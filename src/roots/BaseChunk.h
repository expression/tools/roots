/*
    This file is part of Roots.

    Roots is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Roots is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Roots.  If not, see <http://www.gnu.org/licenses/>.

    Copyright (c) 2013, 2014 IRISA
*/


#ifndef BASECHUNK_H
#define BASECHUNK_H

#include "common.h"
#include "Base.h"
#include "Utterance.h"

namespace roots {

  /**
   * @brief Represents an abstract group of layers, independently of its implementation
   **/
  class BaseChunk : public virtual Base
  {
  public:
    virtual ~BaseChunk();
    
    virtual BaseChunk* clone_to_basechunk() const = 0;
    
  public:
    /**
     * @brief Returns the name of all the layers described in the group of UtteranceSets
     */
    virtual std::vector<std::string> get_all_layer_names() const = 0;

    /**
     * @brief Clears temporary data from the group of layers
     * @note This method is used to handle caching
     */
    virtual void unload_all() = 0;

    /**
     * @brief Loads all the layers in memory
     * @note This method is used to handle caching
     */
    virtual void load_all() = 0;

   /**
     * @brief Test if everything is loaded to get an utterance
     */
    virtual bool is_loaded_for_utterance(const int id) = 0;

    /**
     * @brief Returns the number of utterances
     * @note Complexity is O(1)
     */
    virtual int count_utterances() const = 0;
    
    /**
     * @brief Returns the number of layers
     **/
    virtual int count_layers() const = 0;

    /**
     * @brief Update the number of utterances
     * @note may loads files
     */
    virtual void update_counts() = 0;

    /**
     * @brief Deletes an utterance (all layers)
     * @param id ID of the utterance to be removed. Default ID is 0 (first utterance).
     */
    virtual void delete_utterance(int id = 0) = 0;
    
    /**
     * @brief Removes all utterances and cleans the chunk
     */
    virtual void clear() = 0;

    /**
     * @brief Returns an utterance where all layers are merged
     * @warning Allocates a new full utterance, requires to free memory at some point
     * @param id ID of the utterance. Default is 0 (first utterance).
     * @return pointer of the loaded utterance
     */
    virtual roots::Utterance* get_utterance(int id = 0) = 0;

    /**
     * @brief Returns an utterance where only some layers are considered and merged
     * @warning Allocates a new full utterance, requires to free memory at some point
     * @param layerToLoad List of layers to consider (others are ignored)
     * @param id ID of the utterance. Default is 0 (first utterance).
     * @return pointer of the loaded utterance
     */
    virtual roots::Utterance* get_utterance(const std::vector<std::string> & layerToLoad,
					    int id = 0) = 0;

    /**
     * @brief Adds a new utterance in the current file group, assuming there is no layer
     * @param utt pointer to the utterance
     */
    virtual void add_utterance(const Utterance& utt, const std::string layerName=ANONYMOUS_LAYER) = 0; // throw(RootsException) = 0;

    /**
     * @brief Adds a new (sliced) utterance in the current file group
     * @param utt map giving layer names to utterance (one utterance per layer)
     */
    virtual void add_utterance(const std::map<std::string, Utterance>& utt) = 0; // throw(RootsException) = 0;

    /**
     * @brief Adds new utterances in the current file group, assuming there is no layer
     * @param utt pointer to the utterance
     */
    virtual void add_utterances(const std::vector<Utterance>& utt, const std::string layerName=ANONYMOUS_LAYER) = 0; // throw(RootsException) = 0;

    /**
     * @brief Adds a new (sliced) utterance in the current file group
     * @param utt map giving layer names to utterances
     */
    virtual void add_utterances(const std::map<std::string, std::vector<Utterance> >& utt) = 0; // throw(RootsException) = 0;
    
    /**
     * @brief Replace a given utterance by another
     * @param id ID of the utterance to be replaced
     * @param utt the new utterance
     **/
    virtual void update_utterance(int id, const Utterance& utt) = 0;
    
    /**
     * @brief Replace a given utterance by another
     * @param id ID of the utterance to be replaced
     * @param utt the new utterance split into layers
     **/
    virtual void update_utterance(int id, const std::map<std::string, Utterance> &utt) = 0;
    
    /**
     * @brief Returns a string representation of the chunk
     * @param level the precision level of the output
     * @return string representation of the chunk
     */
    virtual std::string to_string( int level = 0 );

    /**
     * @brief returns the XML tag name value for current class
     * @return string constant representing the XML tag name
     */
    static std::string xml_tag_name() {
      return "chunk";
    };


  protected:
    static const std::string ANONYMOUS_LAYER;
  };

}

#endif // BASECHUNK_H
