/*
    This file is part of Roots.

    Roots is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Roots is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Roots.  If not, see <http://www.gnu.org/licenses/>.

    Copyright (c) 2013, 2014 IRISA
*/


#include "PosSequence.h"

namespace roots
{

namespace sequence
{

	BaseItem *PosSequence::inflate_element(RootsStream * stream, int list_index)
	{
		roots::linguistic::pos::POS *s;

		std::string classname = stream->get_object_classname_no_read(linguistic::pos::POS::xml_tag_name(), list_index);
	
		if(classname.compare(linguistic::pos::Adjective::classname()) == 0)
		{
			s = linguistic::pos::Adjective::inflate_object(stream,list_index);
		} else if(classname.compare(linguistic::pos::Adverb::classname()) == 0)
		{
			s = linguistic::pos::Adverb::inflate_object(stream,list_index);
		} else if(classname.compare(linguistic::pos::Adverb::classname()) == 0)
		{
			s = linguistic::pos::Adverb::inflate_object(stream,list_index);
		} else if(classname.compare(linguistic::pos::Article::classname()) == 0)
		{
			s = linguistic::pos::Article::inflate_object(stream,list_index);
		} else if(classname.compare(linguistic::pos::Conjunction::classname()) == 0)
		{
			s = linguistic::pos::Conjunction::inflate_object(stream,list_index);
		} else if(classname.compare(linguistic::pos::Demonstrative::classname()) == 0)
		{
			s = linguistic::pos::Demonstrative::inflate_object(stream,list_index);
		} else if(classname.compare(linguistic::pos::Determiner::classname()) == 0)
		{
			s = linguistic::pos::Determiner::inflate_object(stream,list_index);
		} else if(classname.compare(linguistic::pos::Indefinite::classname()) == 0)
		{
			s = linguistic::pos::Indefinite::inflate_object(stream,list_index);
		} else if(classname.compare(linguistic::pos::Interjection::classname()) == 0)
		{
			s = linguistic::pos::Interjection::inflate_object(stream,list_index);
		} else if(classname.compare(linguistic::pos::Interrogative::classname()) == 0)
		{
			s = linguistic::pos::Interrogative::inflate_object(stream,list_index);
		} else if(classname.compare(linguistic::pos::Noun::classname()) == 0)
		{
			s = linguistic::pos::Noun::inflate_object(stream,list_index);
		} else if(classname.compare(linguistic::pos::Numeral::classname()) == 0)
		{
			s = linguistic::pos::Numeral::inflate_object(stream,list_index);
		} else if(classname.compare(linguistic::pos::Possessive::classname()) == 0)
		{
			s = linguistic::pos::Possessive::inflate_object(stream,list_index);
		} else if(classname.compare(linguistic::pos::Preposition::classname()) == 0)
		{
			s = linguistic::pos::Preposition::inflate_object(stream,list_index);
		} else if(classname.compare(linguistic::pos::Pronoun::classname()) == 0)
		{
			s = linguistic::pos::Pronoun::inflate_object(stream,list_index);
		} else if(classname.compare(linguistic::pos::Punctuation::classname()) == 0)
		{
			s = linguistic::pos::Punctuation::inflate_object(stream,list_index);
		} else if(classname.compare(linguistic::pos::Verb::classname()) == 0)
		{
			s = linguistic::pos::Verb::inflate_object(stream,list_index);
		} else if(classname.compare(linguistic::pos::ForeignWord::classname()) == 0)
		{
			s = linguistic::pos::ForeignWord::inflate_object(stream,list_index);
		} else if(classname.compare(linguistic::pos::Prefix::classname()) == 0)
		{
			s = linguistic::pos::Prefix::inflate_object(stream,list_index);
		} else if(classname.compare(linguistic::pos::Particle::classname()) == 0)
		{
			s = linguistic::pos::Particle::inflate_object(stream,list_index);
		} else if(classname.compare(linguistic::pos::Symbol::classname()) == 0)
		{
			s = linguistic::pos::Symbol::inflate_object(stream,list_index);
		} else if(classname.compare(linguistic::pos::POS::classname()) == 0)
		  { // Unknown pos 
			s = linguistic::pos::POS::inflate_object(stream,list_index);
		} else {
			 std::stringstream ss;
			ss << classname << " element type is unknown (idx : " << list_index << ") !";
			throw RootsException(__FILE__, __LINE__, ss.str().c_str());
		}

		return s;
	}


	Sequence* PosSequence::inflate_sequence(RootsStream * stream,	const std::string& /* payloadTagName */, int list_index)
	{
		roots::sequence::Sequence *t = new roots::sequence::PosSequence(true);
		t->inflate(stream,true,list_index);//, payloadTagName);
		return t;
	}

}

}
