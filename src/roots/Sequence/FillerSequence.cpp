/*
    This file is part of Roots.

    Roots is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Roots is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Roots.  If not, see <http://www.gnu.org/licenses/>.

    Copyright (c) 2013, 2014 IRISA
*/


#include "FillerSequence.h"

namespace roots
{

namespace sequence
{

	BaseItem *FillerSequence::inflate_element(RootsStream * stream, int list_index)
	{
		roots::linguistic::filler::Filler *s;

		std::string classname = stream->get_object_classname_no_read(linguistic::filler::Filler::xml_tag_name(), list_index);
	
		if(classname.compare(linguistic::filler::Nsa::classname()) == 0)
		{
			s = linguistic::filler::Nsa::inflate_object(stream,list_index);
		} else if(classname.compare(linguistic::filler::Word::classname()) == 0)
		{
			s = linguistic::filler::Word::inflate_object(stream,list_index);
		} else {
			 std::stringstream ss;
			ss << classname << " element type is unknown!";
			throw RootsException(__FILE__, __LINE__, ss.str().c_str());
		}

		return s;
	}


	Sequence* FillerSequence::inflate_sequence(RootsStream * stream,	const std::string& /* payloadTagName */, int list_index)
	{
		roots::sequence::Sequence *t = new roots::sequence::FillerSequence(true);
		t->inflate(stream,true,list_index);//, payloadTagName);
		return t;
	}

}

}
