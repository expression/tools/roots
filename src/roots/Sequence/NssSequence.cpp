/*
    This file is part of Roots.

    Roots is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Roots is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Roots.  If not, see <http://www.gnu.org/licenses/>.

    Copyright (c) 2013, 2014 IRISA
*/


#include "NssSequence.h"

namespace roots
{

namespace sequence
{

	Matrix *NssSequence::make_mapping(sequence::Sequence *targetSequence)
	{
		Matrix *mapping = NULL;
		std::vector<char> sourceSeq, targetSeq;
		char nssSep = '|';
		std::vector<int> sourceMapping, targetMapping;

		char charNssInsertion = '-';
    
		int sourceNssIndex = 0, targetNssIndex = 0;
		char sourceChar = nssSep, targetChar = nssSep; 

		if (dynamic_cast<sequence::NssSequence *>(targetSequence) == NULL)
		{
			throw RootsException(__FILE__, __LINE__, "The target sequence has to be a sequence::NssSequence instance!");
		}
    
		for(unsigned int i=0; i<this->count(); i++)
		{
			std::string nss = this->get_item(i)->get_label();
			const char *str = nss.c_str();

			int j=0;
			while(str[j]!='\0')
			{
				sourceSeq.push_back(str[j]);
				j++;
			}
			sourceSeq.push_back(nssSep);
		}

		for(unsigned int i=0; i<targetSequence->count(); i++)
		{
			std::string nss = targetSequence->get_item(i)->get_label();
			const char	*str = nss.c_str();			

			int j=0;
			while(str[j]!='\0')
			{
				targetSeq.push_back(str[j]);
				j++;
			}
			targetSeq.push_back(nssSep);
		}

		for(unsigned int i=0; i<sourceSeq.size(); i++)
		{
			 std::cout << sourceSeq[i];
		}
		 std::cout<< std::endl <<  std::endl;
		for(unsigned int i=0; i<targetSeq.size(); i++)
		{
			 std::cout << targetSeq[i];
		}
		 std::cout<< std::endl;
		fflush(stdout);


		mapping = new SparseMatrix(this->count(), targetSequence->count());

		if(this->count()==0||targetSequence->count()==0)
		{
			return mapping;
		}

		roots::align::Levenshtein * aligner = new roots::align::Levenshtein(sourceSeq, targetSeq);

		aligner->dpa_standard();

		sourceMapping = aligner->get_source_mapping();
		targetMapping = aligner->get_target_mapping();

		if(sourceMapping.size() != targetMapping.size())
		{
			 std::stringstream ss;
			ss << "source and target mapping of incorrect size!\n";
			throw RootsException(__FILE__, __LINE__, ss.str().c_str());
		}


		sourceNssIndex = 0;
		targetNssIndex = 0;

		for(unsigned int i=0; i<sourceMapping.size(); i++)
		{
        
			if(sourceMapping[i] > 0)
			{
				sourceChar = sourceSeq[sourceMapping[i]];
			}else{
				sourceChar = charNssInsertion;
			}

			if(targetMapping[i] > 0)
			{
				targetChar = targetSeq[targetMapping[i]];
			}else{
				targetChar = charNssInsertion;
			}

			if(targetChar==nssSep || sourceChar==nssSep)
			{
				mapping->set_element(sourceNssIndex, targetNssIndex, 1);

				if(sourceChar==nssSep)
				{
					sourceNssIndex++;
				}
				if(targetChar==nssSep)
				{
					targetNssIndex++;
				}
			}
		}
    
		//mapping->set_element(sourceNssIndex, targetNssIndex, 1);

		delete aligner;

		return mapping;
	}


	Sequence* NssSequence::inflate_sequence(RootsStream * stream,	const std::string& /* payloadTagName */, int list_index)
	  {
		  roots::sequence::Sequence *t = new roots::sequence::NssSequence(true);
		  t->inflate(stream,true,list_index);
		  return t;
	  }


	
}

}
