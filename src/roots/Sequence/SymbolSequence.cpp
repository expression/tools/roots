/*
    This file is part of Roots.

    Roots is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Roots is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Roots.  If not, see <http://www.gnu.org/licenses/>.

    Copyright (c) 2013, 2014 IRISA
*/


#include "SymbolSequence.h"

namespace roots
{

  namespace sequence
  {

    Matrix *SymbolSequence::make_mapping(sequence::Sequence *targetSequence)
    {
      Matrix *mapping = NULL;

      char wordSep = '|';

      std::vector<char> sourceSeq, targetSeq;
      std::vector<int> sourceMapping, targetMapping;

      if (dynamic_cast<sequence::SymbolSequence *>(targetSequence) == NULL)
	{
	  throw RootsException(__FILE__, __LINE__, "The target sequence has to be a sequence::SymbolSequence instance!");
	}

      /* Build vector of UCHAR from this sequence for levenshtein */
      for(unsigned int i=0; i<this->count(); i++)
	{
	  std::string word = this->get_item(i)->get_label();
	  const char *str = word.c_str();

	  int j=0;
	  while(str[j]!='\0')
	    {
	      sourceSeq.push_back(str[j]);
	      j++;
	    }
	  sourceSeq.push_back(wordSep);
	}

      /* Build vector of UCHAR from target sequence for levenshtein */
      for(unsigned int i=0; i<targetSequence->count(); i++)
	{
	  std::string word = targetSequence->get_item(i)->get_label();
	  const char *str = word.c_str();

	  int j=0;
	  while(str[j]!='\0')
	    {
	      targetSeq.push_back(str[j]);
	      j++;
	    }
	  targetSeq.push_back(wordSep);
	}

      /* Prepare mapping */
      mapping = new SparseMatrix(this->count(), targetSequence->count());
      if(this->count()==0||targetSequence->count()==0)
	{
	  return mapping;
	}

      /* Align UCHARs using Levenshtein */
      roots::align::Levenshtein * aligner = new roots::align::Levenshtein(sourceSeq, targetSeq);
      aligner->dpa_standard();

      sourceMapping = aligner->get_source_mapping();
      targetMapping = aligner->get_target_mapping();

      if(sourceMapping.size() != targetMapping.size())
	{
	   std::stringstream ss;
	  ss << "source and target mapping of incorrect size!\n";
	  throw RootsException(__FILE__, __LINE__, ss.str().c_str());
	}

      /* Rebuild word align from uchar align */
      int sourceSymbolIndex = 0;
      int targetSymbolIndex = 0;
      bool currentSourceIsSymbolSep = false;
      bool currentTargetIsSymbolSep = false;

      /* Init alignment	 : shift anlign words */

      if(!sourceMapping.empty())
	{
	  for(int i=0; i<sourceMapping[0]; i++)
	    {
	      if(sourceSeq[i] == wordSep)
		{ sourceSymbolIndex++; }
	    }
	}
      if(!targetMapping.empty())
	{
	  for(int i=0; i<targetMapping[0]; i++)
	    {
	      if(targetSeq[i] == wordSep)
		{ targetSymbolIndex++; }
	    }
	}

      /* Build alignment */
      for(unsigned int i=0; i<sourceMapping.size(); i++)
	{
	  if(sourceMapping[i] > 0)
	    { currentSourceIsSymbolSep = sourceSeq[sourceMapping[i]] == wordSep; }
	  else
	    { currentSourceIsSymbolSep = false; }

	  if(targetMapping[i] > 0)
	    { currentTargetIsSymbolSep = targetSeq[targetMapping[i]] == wordSep; }
	  else
	    { currentTargetIsSymbolSep = false; }

	  if(currentSourceIsSymbolSep || currentTargetIsSymbolSep)
	    {
	      mapping->set_element(sourceSymbolIndex, targetSymbolIndex, 1);
	      if(currentSourceIsSymbolSep)
		{ sourceSymbolIndex++; }
	      if(currentTargetIsSymbolSep)
		{ targetSymbolIndex++; }
	    }
	}

      //mapping->set_element(sourceSymbolIndex, targetSymbolIndex, 1);

      delete aligner;

      return mapping;
    }


    BaseItem *SymbolSequence::inflate_element(RootsStream * stream, int list_index)
    {
      roots::Symbol *s;

      std::string classname = stream->get_object_classname_no_read(Symbol::xml_tag_name(), list_index);

      if(classname.compare(Symbol::classname()) == 0)
	{
	  s = Symbol::inflate_object(stream,list_index);
	} else {
	 std::stringstream ss;
	ss << classname << " element type is unknown!";
	throw RootsException(__FILE__, __LINE__, ss.str().c_str());
      }

      return s;
    }


    Sequence* SymbolSequence::inflate_sequence(RootsStream * stream,	const std::string& /* payloadTagName */, int list_index)
    {
      roots::sequence::Sequence *t = new roots::sequence::SymbolSequence(true);
      t->inflate(stream,true,list_index);//, payloadTagName);
      return t;
    }

  }

}

