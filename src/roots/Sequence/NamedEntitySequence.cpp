/*
    This file is part of Roots.

    Roots is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Roots is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Roots.  If not, see <http://www.gnu.org/licenses/>.

    Copyright (c) 2013, 2014 IRISA
*/


#include "NamedEntitySequence.h"

namespace roots
{

namespace sequence
{

	BaseItem *NamedEntitySequence::inflate_element(RootsStream * stream, int list_index)
	{
		roots::linguistic::namedentity::NamedEntity *s;

		std::string classname = stream->get_object_classname_no_read(linguistic::namedentity::NamedEntity::xml_tag_name(), list_index);
	
		if(classname.compare(linguistic::namedentity::Location::classname()) == 0)
		{
			s = linguistic::namedentity::Location::inflate_object(stream,list_index);
		} else if(classname.compare(linguistic::namedentity::location::Address::classname()) == 0)
		{
			s = linguistic::namedentity::location::Address::inflate_object(stream,list_index);
		} else if(classname.compare(linguistic::namedentity::Number::classname()) == 0)
		{
			s = linguistic::namedentity::Number::inflate_object(stream,list_index);
		} else if(classname.compare(linguistic::namedentity::Organization::classname()) == 0)
		{
			s = linguistic::namedentity::Organization::inflate_object(stream,list_index);
		} else if(classname.compare(linguistic::namedentity::organization::EthnicGroup::classname()) == 0)
		{
			s = linguistic::namedentity::organization::EthnicGroup::inflate_object(stream,list_index);
		} else if(classname.compare(linguistic::namedentity::organization::Other::classname()) == 0)
		{
			s = linguistic::namedentity::organization::Other::inflate_object(stream,list_index);
		} else if(classname.compare(linguistic::namedentity::Period::classname()) == 0)
		{
			s = linguistic::namedentity::Period::inflate_object(stream,list_index);
		} else if(classname.compare(linguistic::namedentity::period::PeriodTime::classname()) == 0)
		{
			s = linguistic::namedentity::period::PeriodTime::inflate_object(stream,list_index);
		} else if(classname.compare(linguistic::namedentity::Person::classname()) == 0)
		{
			s = linguistic::namedentity::Person::inflate_object(stream,list_index);
		} else if(classname.compare(linguistic::namedentity::Time::classname()) == 0)
		{
			s = linguistic::namedentity::Time::inflate_object(stream,list_index);
		} else {
			s = linguistic::namedentity::NamedEntity::inflate_object(stream,list_index);
// 			 std::stringstream ss;
// 			ss << classname << " element type is unknown!";
// 			throw RootsException(__FILE__, __LINE__, ss.str().c_str());
		}

		return s;
	}


	Sequence* NamedEntitySequence::inflate_sequence(RootsStream * stream,	const std::string& /* payloadTagName */, int list_index)
	{
		roots::sequence::Sequence *t = new roots::sequence::NamedEntitySequence(true);
		t->inflate(stream,true,list_index);//, payloadTagName);
		return t;
	}

} }
