/*
    This file is part of Roots.

    Roots is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Roots is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Roots.  If not, see <http://www.gnu.org/licenses/>.

    Copyright (c) 2013, 2014 IRISA
*/


#include "PhonemeSequence.h"
#include "../Acoustic/Allophone.h"


namespace roots
{

namespace sequence
{
  
	PhonemeSequence::PhonemeSequence(bool noInit):roots::sequence::TemplateSequence<roots::phonology::Phoneme>(noInit) {};
	
	PhonemeSequence::PhonemeSequence(const std::string & name, bool noInit):roots::sequence::TemplateSequence<roots::phonology::Phoneme>(name, noInit) {};

	BaseItem *PhonemeSequence::inflate_element(RootsStream * stream, int list_index)
	{
		roots::phonology::Phoneme *s;

		std::string classname = stream->get_object_classname_no_read(phonology::Phoneme::xml_tag_name(), list_index);
		
		if(classname.compare("") == 0)
		{
			classname = stream->get_object_classname_no_read(acoustic::Allophone::xml_tag_name(), list_index);
		}

		if(classname.compare(phonology::Phoneme::classname()) == 0)
		{
			s = phonology::Phoneme::inflate_object(stream,list_index);
		} else if(classname.compare(acoustic::Allophone::classname()) == 0)
		{
			s = acoustic::Allophone::inflate_object(stream,list_index);
		} else {
			 std::stringstream ss;
			ss << classname << " element type is unknown!";
			throw RootsException(__FILE__, __LINE__, ss.str().c_str());
		}

		return s;
	}


	Sequence* PhonemeSequence::inflate_sequence(RootsStream * stream,	const std::string& /* payloadTagName */, int list_index)
	{
		roots::sequence::Sequence *t = new roots::sequence::PhonemeSequence(true);
		t->inflate(stream,true,list_index);//, payloadTagName);
		return t;
	}


} }

