/*
    This file is part of Roots.

    Roots is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Roots is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Roots.  If not, see <http://www.gnu.org/licenses/>.

    Copyright (c) 2013, 2014 IRISA
*/


#ifndef ALLOPHONESEQUENCE_H_
#define ALLOPHONESEQUENCE_H_

#include "../common.h"
#include "../Sequence.h"
#include "./TemplateSequence.h"
#include "./PhonemeSequence.h"
#include "../Acoustic/Allophone.h"

#include "../Align.h"
#include "../Align/Levenshtein.h"


namespace roots
{

  namespace sequence
  {

    class AllophoneSequence: public PhonemeSequence
    {
    public:
    AllophoneSequence(bool noInit = false):PhonemeSequence(noInit){};
    AllophoneSequence(const std::string & name, bool noInit = false):PhonemeSequence(name, noInit){};

      virtual std::string get_classname() const { return classname(); };
      /**
       * @brief returns classname for current class
       * @return string constant representing the classname
       */
      static std::string classname() { return "Sequence::Allophone"; }

      virtual AllophoneSequence *clone() const
      {
	return new AllophoneSequence(*this);
      }

      virtual	AllophoneSequence *clone_empty() const
      {
	return new AllophoneSequence();
      }

      /**
       * @brief Extracts the entity from the stream
       * @param stream the stream from which we inflate the element
       * @param payloadTagName xml tag name used for sequence's contents
       * @return pointer to the inflated sequence
       */
      static Sequence* inflate_sequence(RootsStream * stream,	const std::string& payloadTagName, int list_index=-1);
       
    protected:
      ~AllophoneSequence(){}; // Use Destroy instead
    };
  }

}

#endif /* ALLOPHONESEQUENCE_H_ */
