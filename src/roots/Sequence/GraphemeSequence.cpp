/*
    This file is part of Roots.

    Roots is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Roots is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Roots.  If not, see <http://www.gnu.org/licenses/>.

    Copyright (c) 2013, 2014 IRISA
*/


#include "GraphemeSequence.h"

using namespace icu;

namespace roots
{

  namespace sequence
  {

    std::string GraphemeSequence::to_string(int level) const
    {
      std::string stringResult = std::string();
      int indexItem = 0;
      int	nItem = this->count();

      for(indexItem=0;indexItem<nItem;indexItem++)
	{
	  stringResult = stringResult + this->get_item(indexItem)->to_string(level);
	  if ((indexItem < nItem - 1) && (level != 0) ) { stringResult += " "; }
	}

      return stringResult;
    }

    Matrix *GraphemeSequence::make_mapping(sequence::Sequence *targetSequence)
    {
      Matrix *mapping = NULL;

      UChar wordSep = '|';

      std::vector<UChar> sourceSeq, targetSeq;
      std::vector<int> sourceMapping, targetMapping;

      if ((dynamic_cast<sequence::GraphemeSequence * >(targetSequence) == NULL)
					&& (dynamic_cast<sequence::WordSequence * >(targetSequence) == NULL))
				{
					return Sequence::make_mapping(targetSequence);
				}

      /* Build vector of UCHAR from this sequence for levenshtein */
      for(unsigned int i=0; i<this->count(); i++)
	{
	  UnicodeString cunicode = UnicodeString(this->get_item(i)->to_string(0).c_str());
	  if(cunicode.length() >1)
	    {throw RootsException ( __FILE__,__LINE__, "Grapheme must be one char long!\n" );}

	  UChar cchar = cunicode.getTerminatedBuffer()[0];
	  if(cchar == ' ')
	    { sourceSeq.push_back(wordSep); }
	  else
	    { sourceSeq.push_back(cchar); }
	}
      if(sourceSeq.back() != wordSep)
	{sourceSeq.push_back(wordSep);}

      /* Build vector of UCHAR from target sequence for levenshtein */
      for(unsigned int i=0; i<targetSequence->count(); i++)
	{
	  UnicodeString word = UnicodeString(targetSequence->get_item(i)->get_label().c_str());
	  const UChar	*str = word.getTerminatedBuffer();

	  int j=0;
	  while(str[j]!='\0')
	    {
	      targetSeq.push_back(str[j]);
	      j++;
	    }
	  targetSeq.push_back(wordSep);
	}
      

      // Debug
      /*/
      cout<<endl << "<< " << endl;

      for(unsigned int i=0; i<sourceSeq.size(); i++)
	{
	  cout << UnicodeString(sourceSeq[i]);
	}
      cout<<endl << endl;
      for(unsigned int i=0; i<targetSeq.size(); i++)
	{
	  cout << UnicodeString(targetSeq[i]);
	}
      cout<<endl << " >> " << endl;
      fflush(stdout);
      /*/

      /* Prepare mapping */
      mapping = new SparseMatrix(this->count(), targetSequence->count());
      if(this->count()==0||targetSequence->count()==0)
	{
	  return mapping;
	}

      /* Align UCHARs using Levenshtein */
      roots::align::Levenshtein_Unicode * aligner =
	new roots::align::Levenshtein_Unicode(sourceSeq, targetSeq);
      aligner->dpa_standard();

      sourceMapping = aligner->get_source_mapping();
      targetMapping = aligner->get_target_mapping();

      if(sourceMapping.size() != targetMapping.size())
	{
	   std::stringstream ss;
	  ss << "source and target mapping of incorrect size!\n";
	  throw RootsException(__FILE__, __LINE__, ss.str().c_str());
	}

      /* Rebuild char align from uchar align */
      int targetWordIndex = 0;
      //      int sourceCharIndex = 0;
      int sourcePrevIndex = -1;
      bool targetIsWordSep = false;

      /* Init alignment	 : shift anlign chars */
      if(!targetMapping.empty())
	{
	  for(int i=0; i<targetMapping[0]; i++)
	    {
	      if(targetSeq[i] == wordSep)
		{ targetWordIndex++; }
	    }
	}

      /* Build alignment */
      for(unsigned int i=0; i<sourceMapping.size(); i++)
	{
	  //	  cout << i << "\t" << sourceMapping[i] << " ; "<< targetMapping[i] << endl;
	  if((targetMapping[i] >= 0) &&
	     (targetSeq[targetMapping[i]] == wordSep))
	    {
	      targetWordIndex++;
	      targetIsWordSep = true;
	      // cerr << "jump target -> "<<targetWordIndex <<std::endl;
	    }
	  else
	    {targetIsWordSep = false;}

	  if((!targetIsWordSep)&&
	     //	     (targetMapping[i] >= 0) &&
	     ((sourceMapping[i] < 0) ||
	      (sourceSeq[sourceMapping[i]] != wordSep)))
	    {
	      //	      cout << UnicodeString(sourceSeq[sourceMapping[i]]) << " ; "<< UnicodeString(targetSeq[targetMapping[i]]) << endl;
	      //	      cout << this->get_item(sourceMapping[i])->to_string() << " ; "<< targetSequence->get_item(targetWordIndex)->to_string() << endl;
	      if(sourceMapping[i] >= 0)
		{ sourcePrevIndex = sourceMapping[i];}
	      if(sourcePrevIndex >= 0)
		{ mapping->set_element(sourcePrevIndex, targetWordIndex, 1);}
	      //	      mapping->set_element(sourceCharIndex, targetWordIndex, 1);
	    }
	  //	  if(sourceMapping[i] >= 0)
	  //{sourceCharIndex ++;}

	}

      //      mapping->set_element(sourceCharIndex, targetWordIndex, 1);

      delete aligner;

      return mapping;
    }


    BaseItem *GraphemeSequence::inflate_element(RootsStream * stream, int list_index)
    {
      roots::linguistic::Grapheme *s;

      std::string classname = stream->get_object_classname_no_read(linguistic::Grapheme::xml_tag_name(), list_index);

      if(classname.compare(linguistic::Grapheme::classname()) == 0)
	{
	  s = linguistic::Grapheme::inflate_object(stream,list_index);
	} else {
	 std::stringstream ss;
	ss << classname << " element type is unknown!";
	throw RootsException(__FILE__, __LINE__, ss.str().c_str());
      }

      return s;
    }


    Sequence* GraphemeSequence::inflate_sequence(RootsStream * stream,	const std::string& /* payloadTagName */, int list_index)
    {
      roots::sequence::Sequence *t = new roots::sequence::GraphemeSequence("", true);
      t->inflate(stream,true,list_index);//, payloadTagName);
      return t;
    }

  }

}

