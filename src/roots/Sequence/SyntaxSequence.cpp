/*
    This file is part of Roots.

    Roots is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Roots is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Roots.  If not, see <http://www.gnu.org/licenses/>.

    Copyright (c) 2013, 2014 IRISA
*/

#include "SyntaxSequence.h"

namespace roots {

namespace sequence {

SyntaxSequence::SyntaxSequence(bool noInit)
    : roots::sequence::TemplateSequence<roots::linguistic::syntax::Syntax>(
          noInit) {
  this->set_is_simple_item(false);
}

SyntaxSequence::SyntaxSequence(const SyntaxSequence &seq)
    : TemplateSequence<linguistic::syntax::Syntax>(seq) {
  this->set_is_simple_item(false);
}

Sequence *SyntaxSequence::get_related_sequence() const {
  if (this->count() <= 0) {
    ROOTS_LOGGER_WARN(
        "SyntaxSequence::get_related_sequence : Empty sequence. I will return "
        "NULL but it could be something unexpected!");
    return NULL;
  }
  return this->get_item(0)->get_related_sequence();
}

Relation *SyntaxSequence::get_embedded_relation() {
  if (this->count() <= 0) {
    std::stringstream ss;
    ss << "Syntax sequence is empty, unable to find the linked word sequence !";
    throw RootsException(__FILE__, __LINE__, ss.str().c_str());
  }

  Relation *embedded_relation =
      new Relation(this, this->get_related_sequence());
  Matrix *map = embedded_relation->get_mapping();
  for (unsigned int index = 0; index < this->count(); index++) {
    std::vector<int> wordIndexArray =
        this->get_item(index)->get_word_index_array();
    for (unsigned int i = 0; i < wordIndexArray.size(); i++) {
      map->set_element(index, wordIndexArray[i], 1);
    }
  }
  return embedded_relation;
}

BaseItem *SyntaxSequence::inflate_element(RootsStream *stream, int list_index) {
  roots::linguistic::syntax::Syntax *s;

  std::string classname = stream->get_object_classname_no_read(
      linguistic::syntax::Syntax::xml_tag_name(), list_index);

  if (classname.compare(linguistic::syntax::Syntax::classname()) == 0) {
    s = linguistic::syntax::Syntax::inflate_object(stream, list_index);
  } else {
    std::stringstream ss;
    ss << classname << " element type is unknown!";
    throw RootsException(__FILE__, __LINE__, ss.str().c_str());
  }

  return s;
}

Sequence *SyntaxSequence::inflate_sequence(
    RootsStream *stream, const std::string & /* payloadTagName */,
    int list_index) {
  roots::sequence::Sequence *t = new roots::sequence::SyntaxSequence(true);
  t->inflate(stream, true, list_index);  //, payloadTagName);
  return t;
}
}
}
