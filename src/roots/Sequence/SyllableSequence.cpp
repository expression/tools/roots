/*
    This file is part of Roots.

    Roots is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Roots is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Roots.  If not, see <http://www.gnu.org/licenses/>.

    Copyright (c) 2013, 2014 IRISA
*/

#include "SyllableSequence.h"
#include "../Phonology/SyllableException.h"

namespace roots {
namespace sequence {

SyllableSequence::SyllableSequence(bool noInit)
    : roots::sequence::TemplateSequence<roots::phonology::syllable::Syllable>(
          noInit) {
  this->set_is_simple_item(false);
}

SyllableSequence::SyllableSequence(const SyllableSequence &seq)
    : TemplateSequence<phonology::syllable::Syllable>(seq) {
  this->set_is_simple_item(false);
}

Sequence *SyllableSequence::get_related_sequence() const {
  if (this->count() <= 0) {
    ROOTS_LOGGER_WARN(
        "SyllableSequence::get_related_sequence : Empty sequence. I will "
        "return NULL but it could be something unexpected!");
    return NULL;
  }
  return this->get_item(0)->get_related_sequence();
}

Relation *SyllableSequence::get_embedded_relation() {
  if (this->count() <= 0) {
    std::stringstream ss;
    ss << "Syllable sequence is empty, unable to find the linked phoneme "
          "sequence !";
    throw RootsException(__FILE__, __LINE__, ss.str().c_str());
  }

  Relation *embedded_relation =
      new Relation(this, this->get_related_sequence());

  Matrix *map = embedded_relation->get_mapping();

  for (unsigned int index = 0; index < this->count();
       index++)  // For each syllable of index index
  {
    std::vector<int> vec = this->get_item(index)->get_onset_indices();
    for (unsigned int i = 0; i < vec.size(); i++)
      map->set_element(index, vec[i], 1);
    vec = this->get_item(index)->get_nucleus_indices();
    for (unsigned int i = 0; i < vec.size(); i++)
      map->set_element(index, vec[i], 1);
    vec = this->get_item(index)->get_coda_indices();
    for (unsigned int i = 0; i < vec.size(); i++)
      map->set_element(index, vec[i], 1);
  }

  return embedded_relation;
}

// !!! linking_relation must be in the direction old phonemes => new phonemes
void SyllableSequence::set_embedded_relation(
    const roots::Relation &new_relation,
    roots::sequence::PhonemeSequence *phoneme_sequence) {
  Matrix *map = new_relation.get_mapping();
  std::vector<int> set = *(new std::vector<int>());
  bool error = false;
  unsigned int n_col = map->get_n_col();

  if (this->count() > 0) {
    if (this->count() != (unsigned int)map->get_n_row()) {
      std::stringstream ss;
      ss << "Unable to substitute the embedded relation : New relation has not "
            "the same number of rows !";
      throw RootsException(__FILE__, __LINE__, ss.str().c_str());
    }

    for (unsigned int index = 0; index < this->count(); index++) {
      for (unsigned int i = 0; i < n_col; i++) {
        if (map->get_element(index, i) == 1) {
          set.push_back(i);
        }
      }

      try {
        (this->get_item(index))->from_phoneme_indices(phoneme_sequence, set);
      } catch (phonology::syllable::SyllableMultipleVowelException e) {
        // Remove syllable
        std::stringstream ss;
        ss << index;
        ROOTS_LOGGER_ERROR(
            std::string("set_embedded_relation : more than one vowel in "
                        "syllable - ignoring it (index:") +
            ss.str() + ")");
        new_relation.get_mapping()->remove_row(index);
        this->remove(index);
        index--;
        error = true;
      } catch (phonology::syllable::SyllableNoNucleusException e) {
        // Remove syllable
        std::stringstream ss;
        ss << index;
        ROOTS_LOGGER_ERROR(
            std::string("set_embedded_relation : no nucleus found for syllable "
                        "- ignoring it (index:") +
            ss.str() + ")");
        new_relation.get_mapping()->remove_row(index);
        this->remove(index);
        index--;
        error = true;
      }

      set.clear();
    }

    if (error) {
      std::stringstream ss;
      ss << "Malformed syllables removed from sequence!";
      throw phonology::syllable::SyllableException(__FILE__, __LINE__,
                                                   ss.str().c_str());
    }
  }
}

BaseItem *SyllableSequence::inflate_element(RootsStream *stream,
                                            int list_index) {
  roots::phonology::syllable::Syllable *s;

  std::string classname = stream->get_object_classname_no_read(
      phonology::syllable::Syllable::xml_tag_name(), list_index);

  if (classname.compare(phonology::syllable::Syllable::classname()) == 0) {
    s = phonology::syllable::Syllable::inflate_object(stream, list_index);
  } else {
    std::stringstream ss;
    ss << classname << " element type is unknown!";
    throw RootsException(__FILE__, __LINE__, ss.str().c_str());
  }

  return s;
}

Sequence *SyllableSequence::inflate_sequence(
    RootsStream *stream, const std::string & /* payloadTagName */,
    int list_index) {
  roots::sequence::Sequence *t = new roots::sequence::SyllableSequence(true);
  t->inflate(stream, true, list_index);  //, payloadTagName);
  return t;
}
}
}
