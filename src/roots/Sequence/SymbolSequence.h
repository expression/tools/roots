/*
    This file is part of Roots.

    Roots is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Roots is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Roots.  If not, see <http://www.gnu.org/licenses/>.

    Copyright (c) 2013, 2014 IRISA
*/


#ifndef SYMBOLSEQUENCE_H
#define SYMBOLSEQUENCE_H

#include "../common.h"
#include "../Sequence.h"
#include "TemplateSequence.h"
#include "../Symbol.h"

#include "../Align.h"
#include "../Align/Levenshtein.h"

namespace roots
{
  
  namespace sequence
  {
    
    class SymbolSequence: public TemplateSequence<roots::Symbol>
    {
    public:
      /**
       * @brief Default constructor
       * @param noInit True if attributes should not be initialized, default is false.
       **/
      SymbolSequence(bool noInit = false) : TemplateSequence<roots::Symbol>(noInit) {};
      
      /**
       * @brief Constructor where the name is given
       *
       * @param _label Name of the sequence to be created
       * @param noInit True if attributes should not be initialized, default is false.
       **/
      SymbolSequence(const std::string & _label, bool noInit = false) : TemplateSequence<roots::Symbol>(noInit)
      {
	set_label(_label);
      };
      
      virtual roots::Matrix *make_mapping(roots::sequence::Sequence *targetSequence);
      
      /**
       * @brief Get the class name of the current sequence
       * @return A string
       **/
      virtual std::string get_classname() const { return classname(); };
      
      /**
       * @brief returns classname for current class
       * @return string constant representing the classname
       */
      static std::string classname() { return "Sequence::Symbol"; }
      
      /**
       * @brief Return a clone of the current sequence
       * @return Pointer to the clone
       **/
      virtual SymbolSequence *clone() const
      {
	return new SymbolSequence(*this);
      }
      
      /**
       * @brief Creates a new empty symbol sequence
       * @return Pointer to the new sequence
       **/
      virtual SymbolSequence *clone_empty() const
      {
	return new SymbolSequence();
      }
      
      /**
       * @brief Loads one symbol from a stream
       * @param stream Stream to be read
       * @param list_index Index in the list if reading a list of symbols, -1 otherwise
       * @return Pointer to the loaded item
       **/
      virtual BaseItem *inflate_element(RootsStream * stream, int list_index=-1);
      
      /**
       * @brief Extracts the entity from the stream
       * @param stream the stream from which we inflate the element
       * @param payloadTagName xml tag name used for sequence's contents
       * @return pointer to the inflated sequence
       */
      static Sequence* inflate_sequence(RootsStream * stream, const std::string& payloadTagName, int list_index=-1);
      
    protected:
      /**
       * @brief Private destructor, use method destroy() instead
       **/
      ~SymbolSequence() {};
    };
    
  }
}

#endif // SYMBOLSEQUENCE_H
