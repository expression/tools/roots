/*
    This file is part of Roots.

    Roots is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Roots is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Roots.  If not, see <http://www.gnu.org/licenses/>.

    Copyright (c) 2013, 2014 IRISA
*/


#ifndef PHONEME_BLOCK_SEQUENCE_H
#define PHONEME_BLOCK_SEQUENCE_H

#include "../common.h"
#include "../Sequence.h"
#include "./TemplateSequence.h"
#include "../Phonology/PhonemeBlock.h"

namespace roots {
namespace sequence {

class PhonemeBlockSequence
    : public roots::sequence::TemplateSequence<roots::phonology::PhonemeBlock> {
 public:
  PhonemeBlockSequence(bool noInit = false);
  PhonemeBlockSequence(const std::string &_label, bool noInit = false);
  PhonemeBlockSequence(const PhonemeBlockSequence &seq);

  virtual std::string get_classname() const { return classname(); };
  /**
   * @brief returns classname for current class
   * @return string constant representing the classname
   */
  static std::string classname() { return "Sequence::PhonemeBlock"; }

  virtual PhonemeBlockSequence *clone() const {
    return new PhonemeBlockSequence(*this);
  }

  virtual PhonemeBlockSequence *clone_empty() const {
    return new PhonemeBlockSequence();
  }

  virtual Relation *get_embedded_relation();
  virtual Sequence *get_related_sequence() const;
  virtual void
  set_embedded_relation(const roots::Relation &new_relation,
                        roots::sequence::PhonemeSequence *phoneme_sequence);

  virtual BaseItem *inflate_element(RootsStream *stream, int list_index = -1);

  /**
   * @brief Extracts the entity from the stream
   * @param stream the stream from which we inflate the element
   * @param payloadTagName xml tag name used for sequence's contents
   * @return pointer to the inflated sequence
   */
  static Sequence *inflate_sequence(RootsStream *stream,
                                    const std::string &payloadTagName,
                                    int list_index = -1);

  //	/**
  //	 * @brief returns a string representation for use with PGF/Tikz
  //	 * @param xCoord x coordinate at which the object has to be drawn
  //(default: 0)
  //	 * @param yCoord y coordinate at which the object has to be drawn
  //(default: 0)
  //	 * @param xLength length on the x axis (default: 2)
  //	 * @param yLength length on th y axis (default: 2)
  //	 * @param xScaleMillimeter length on th y axis (default: 2)
  //	 * @param yScaleMillimeter length on th y axis (default: 2)
  //	 * @param level the precision level of the output (default: 0)
  //	 * @param mapping used for complex element to map nested elements
  //(default: empty mapping)
  //	 * @return PGF/Tikz representation of the object
  //	 */
  //	virtual std::string to_pgf(int xCoord=0, int yCoord=0, int xLength=2,
  //int yLength=2, int xScaleMillimeter=2, int yScaleMillimeter=2, int level=0,
  //int start=0, int end=-1, matrix::GridMapping mapping =
  //matrix::GridMapping());
 protected:
  ~PhonemeBlockSequence(){};  // Use Destroy instead
};

}
}

#endif // PHONEME_BLOCK_SEQUENCE_H
