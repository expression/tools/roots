/*
    This file is part of Roots.

    Roots is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Roots is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Roots.  If not, see <http://www.gnu.org/licenses/>.

    Copyright (c) 2013, 2014 IRISA
*/

#ifndef TEMPLATESEQUENCE_H_
#define TEMPLATESEQUENCE_H_

#include "../BaseItem.h"
#include "../Relation.h"
#include "../Sequence.h"
#include "../common.h"

namespace roots {
namespace sequence {
/**
 * @brief This class is the template implementation of sequence. All real
 *sequences inherits this class.
 **/
template <class T, bool virtual_sequence = false>
class TemplateSequence : public roots::sequence::Sequence {
 public:
  /**
   * @brief Default constructor
   * @param noInit By default, value is false.
   **/
  TemplateSequence(bool noInit = false) : Sequence(noInit) {
    elements = std::deque<T *>();
  }

  /**
   * @brief Default constructor
   * @param name Name of the sequence
   * @param noInit By default, value is false.
   **/
  TemplateSequence(const std::string &name, bool noInit = false)
      : Sequence(name, noInit) {
    elements = std::deque<T *>();
  }

  /**
   * @brief Copy constructor
   * @param seq Sequence to be copied
   **/
  TemplateSequence(const TemplateSequence<T, virtual_sequence> &seq)
      : Sequence(seq) {
    clear();
    int sz = seq.get_elements().size();
    for (int i = 0; i < sz; i++) add(seq.get_elements().at(i));
  }

 protected:
  /**
   * @brief Destructor
   **/
  ~TemplateSequence() { clear(); };

 public:
  virtual TemplateSequence<T, virtual_sequence> *clone() const {
    return new TemplateSequence<T, virtual_sequence>(*this);
  }

  /**
   * @brief Get an empty sequence with same template type.
   */
  virtual TemplateSequence<T, virtual_sequence> *clone_empty() const {
    return new TemplateSequence<T, virtual_sequence>();
  }

  /**
   * @brief Affectation operator
   * @param seq Sequence to be copied
   * @return A reference to the current object
   **/
  virtual TemplateSequence<T, virtual_sequence> &operator=(
      const TemplateSequence<T, virtual_sequence> &seq) {
    ((Base) * this).operator=(seq);
    clear();

    int sz = seq.get_elements().size();
    for (int i = 0; i < sz; i++) add(seq.get_elements().at(i));

    return *this;
  }

  virtual bool has_related_sequence() const {
    bool found_related = false;
    for (unsigned int index = 0; index < this->count() && !found_related;
         index++)  // For each phoneme block of index index
    {
      found_related |= this->get_item(index)->has_related_sequence();
    }
    return found_related;
  }

  /**
   * @brief Default implementation of get_embedded_relation()
   * @return NULL (i.e., no embbeded relation)
   **/
  virtual Relation *get_embedded_relation() { return NULL; }

  /**
   * @brief Default implementation of get_related_sequence()
   * @return NULL (i.e., no embbeded relation)
   **/
  virtual Sequence *get_related_sequence() const { return NULL; }

  virtual T *get_item(int index) const {
    // Normal access from the beginning of the sequence
    if (index >= 0) {
      return elements.at(index);
    }
    // Access from the end of the sequence
    else {
      return elements.at(elements.size() + index);
    }
  }

  virtual std::vector<T *> get_all_items() const {
    unsigned int n = this->count();
    std::vector<T *> all_items;
    for (unsigned int i = 0; i < n; i++) {
      all_items.push_back(this->get_item(i));
    }
    return all_items;
  }

  virtual T *first() { return elements.front(); }

  virtual T *last() { return elements.back(); }

  virtual void set_item(unsigned int index, BaseItem *item) {
    check_type(item);
    T *item_t;
    if (!virtual_sequence) {
      item_t = dynamic_cast<T *>(item->clone());
      item_t->set_in_sequence(this, index);
    } else {
      item_t = dynamic_cast<T *>(item);
    }

    if (!virtual_sequence) {
      delete elements.at(index);
    }

    elements.at(index) = item_t;
  }

  virtual unsigned int add(BaseItem *item) {
    // objects are cloned
    check_type(item);

    T *item_t;
    if (!virtual_sequence) {
      item_t = dynamic_cast<T *>(item->clone());

      item_t->set_in_sequence(this, elements.size());
    } else {
      item_t = dynamic_cast<T *>(item);
    }

    elements.push_back(item_t);

    // Adds a column to all linked relations
    std::vector<Relation *> rels = this->get_all_relations();
    for (std::vector<Relation *>::const_iterator it = rels.begin();
         it != rels.end(); ++it) {
      Relation *rel = (*it);
      if (rel->get_source_sequence() == this) {
        rel->insert_source(this->count());
      } else {
        rel->insert_target(this->count());
      }
    }

    return (elements.size() - 1);
  }

  virtual void add(const std::vector<BaseItem *> &items) {
    for (std::vector<BaseItem *>::const_iterator it = items.begin();
         it != items.end(); ++it) {
      add(*it);
    }
  }

  virtual void concatenate(const Sequence *seq) {
    if (seq == NULL) {
      return;
    }

    int shiftIdx = 0;
    bool isRelated = false;
    Sequence *relatedSeq2 = NULL;

    // Manage Embeded relation
    if (!has_related_sequence()) {
      if (seq->has_related_sequence()) {
        // TODO ERROR
      }
      // Concatenate related sequences
      Sequence *relatedSeq1 = get_related_sequence();
      int tmpShiftIdx = relatedSeq1->count();
      relatedSeq2 = seq->get_related_sequence();
      if (relatedSeq1 != relatedSeq2) {
        relatedSeq1->concatenate(relatedSeq2);
        shiftIdx = tmpShiftIdx;
        isRelated = true;
      }
    }

    // Add a raw copie of items in current seq
    size_t seqSize = seq->count();
    for (size_t i = 0; i < seqSize; ++i) {
      BaseItem *item = seq->get_item(i);

      // objects are cloned
      check_type(item);

      T *item_t;
      if (!virtual_sequence) {
        item_t = dynamic_cast<T *>(item->clone());
        // shift items indices for the current sequence
        if (isRelated) {
          item_t->translate_index(shiftIdx);
          item_t->set_related_sequence(relatedSeq2);
        }
        item_t->set_in_sequence(this, elements.size());
      } else {
        item_t = dynamic_cast<T *>(item);
      }
      elements.push_back(item_t);
    }

    // Update relations (only from the original seq)
    std::vector<Relation *> rels = get_all_relations();
    std::vector<Relation *> seqRels = seq->get_all_relations();

    for (std::vector<Relation *>::const_iterator it = rels.begin();
         it != rels.end(); ++it) {
      (*it)->refresh_relation_size();
    }
  }

 virtual void concatenate_no_propagate(const Sequence *seq) {
    if (seq == NULL) {
      return;
    }
   // Add a raw copie of items in current seq
     size_t seqSize = seq->count();
    for (size_t i = 0; i < seqSize; ++i) {
      BaseItem *item = seq->get_item(i);

      // objects are cloned
      check_type(item);

      T *item_t;
      if (!virtual_sequence) {
        item_t = dynamic_cast<T *>(item->clone());
        item_t->set_in_sequence(this, elements.size());
      } else {
        item_t = dynamic_cast<T *>(item);
      }
      elements.push_back(item_t);
    }
   }

 virtual void insert(BaseItem *item, unsigned int index) {
    // objects are cloned
    check_type(item);

    T *item_t;
    if (!virtual_sequence) {
      item_t = dynamic_cast<T *>(item->clone());

      item_t->set_in_sequence(this, index);
    } else {
      item_t = dynamic_cast<T *>(item);
    }

    if (index < elements.size())
      elements.insert(elements.begin() + index, item_t);
    else
      elements.push_back(item_t);

    for (unsigned int i = index; i < elements.size(); i++) {
      elements[i]->set_in_sequence_index(i);
    }

    // Adds a column to all linked relations
    std::vector<Relation *> rels = this->get_all_relations();
    for (std::vector<Relation *>::const_iterator it = rels.begin();
         it != rels.end(); ++it) {
      Relation *rel = (*it);
      if (rel->get_source_sequence() == this) {
        rel->insert_source(index);
      } else {
        rel->insert_target(index);
      }
    }
  }

  /**
   * @brief
   * @todo Delete the element
   */
  virtual void remove(unsigned int index) {
    // Removes a column from all linked relations
    boost::unordered_set<Sequence *> alreadyPerformed;
    std::vector<Relation *> rels = this->get_all_relations();
    for (std::vector<Relation *>::const_iterator it = rels.begin();
         it != rels.end(); ++it) {
      Relation *rel = (*it);
      if (rel->get_source_sequence() == this) {
        rel->remove_source(
            index, alreadyPerformed.insert(rel->get_target_sequence()).second);
      } else {
        rel->remove_target(
            index, alreadyPerformed.insert(rel->get_source_sequence()).second);
      }
    }

    if (!virtual_sequence) {
      T *item_t = elements[index];

      elements.erase(elements.begin() + index);
      item_t->remove_from_sequence();

      delete item_t;

      for (unsigned int i = index; i < elements.size(); i++) {
        elements[i]->set_in_sequence_index(i);
      }
    } else {
      elements.erase(elements.begin() + index);
    }
  }

  virtual T *shift() {
    T *elt;
    if (!virtual_sequence) {
      elt = dynamic_cast<T *>(elements.front()->clone());
    } else {
      elt = dynamic_cast<T *>(elements.front());
    }
    remove(0);
    return elt;
  }

  virtual T *pop() {
    T *elt;
    if (!virtual_sequence) {
      elt = dynamic_cast<T *>(elements.back()->clone());
    } else {
      elt = dynamic_cast<T *>(elements.back());
    }

    remove(elements.size() - 1);

    return elt;
  }

  virtual unsigned int count() const { return elements.size(); }

  /**
   * @brief Clears the sequence
   */
  virtual void clear() {
    if (!virtual_sequence) {
      int sz = elements.size();
      for (int i = 0; i < sz; i++) elements[i]->destroy();
    }
    elements.clear();
  }

  virtual Sequence *get_subsequence(int indexBegin, int indexEnd,
                                    int embeddedOffset = 0) const {
    Sequence *res = this->clone_empty();
    res->set_label(this->get_label());

    for (int i = indexBegin; i <= indexEnd; ++i) {
      res->add(elements[i]);
      ((BaseItem *)res->get_item(res->count() - 1))
          ->translate_index(embeddedOffset);  // Translate not simple items
    }

    return res;
  }

  virtual void check_type(BaseItem *item) {
    if (dynamic_cast<T *>(item) == 0)
      throw RootsException(__FILE__, __LINE__, "Bad type !\n");
  }

  virtual std::string get_content_type() const { return content_type(); };

  static std::string content_type() { return T::classname(); };

  virtual bool linked(const std::string &targetSequence) {
    std::vector<Relation *> rels = get_all_relations();
    Sequence *emb_seq = get_related_sequence();
    if (emb_seq != NULL && emb_seq->get_label() == targetSequence) {
      return true;
    }
    for (std::vector<Relation *>::const_iterator it = rels.begin();
         it != rels.end(); ++it) {
      if (((*it)->get_source_label() == targetSequence) ||
          ((*it)->get_target_label() == targetSequence)) {
        return true;
      }
    }
    return false;
  }

  virtual void insert_related_element(unsigned int index) {
    unsigned int firstElement = 0;
    Sequence *seqRelated = this->get_related_sequence();
    if (seqRelated != NULL) {
      // Remove the linked item
      std::vector<int> relIndices =
          seqRelated->get_related_indices(index, get_label());
      if (relIndices.size() > 0) firstElement = relIndices[0];

      for (unsigned int idx = firstElement; idx < this->count(); idx++) {
        this->get_item(idx)->insert_related_element(index);
      }
    }
  }

  virtual void remove_related_element(unsigned int index) {
    // TODO : don't work in some cases ! do not use it !! (JC)
    unsigned int firstElement = 0;
    Sequence *seqRelated = this->get_related_sequence();
    if (seqRelated != NULL) {
      for (unsigned int idx = firstElement; idx < this->count(); idx++) {
        this->get_item(idx)->remove_related_element(index);
      }

      // Remove the linked item
      std::vector<int> relIndices =
          seqRelated->get_related_indices(index, get_label());
      while (relIndices.size() != 0) {
        this->remove(relIndices.front());
        relIndices = seqRelated->get_related_indices(index, get_label());
      }
    }
  }

  virtual void prepare_save(const std::string &target_base_dir) {
    std::vector<T *> vectItems = get_all_items();
    for (unsigned int i = 0; i < vectItems.size(); i++) {
      vectItems[i]->prepare_save(target_base_dir);
    }
  }

  virtual void inflate(RootsStream *stream, bool is_terminal = true,
                       int list_index = -1) {
    if (virtual_sequence) {
      throw RootsException(
          __FILE__, __LINE__,
          "Not allowed to inflate virtual template sequences !\n");
    }

    unsigned int n_elem = 0;
    std::map<int, BaseItem *> tempList;

    Base::inflate(stream, false, list_index);

    // stream->open_children();
    this->set_label(stream->get_unicodestring_content("label"));
    n_elem = stream->get_uint_content("n_elem");

    for (unsigned int index = 0; index < n_elem; index++) {
      // Get the rank from the node
      // int rank = stream->get_int_attribute("rank");

      // Inflate the element
      BaseItem *elt = inflate_element(stream, index);

      // Add the pair to the map
      // tempList[rank] = elt;
      tempList[index] = elt;

      stream->next_child();
    }

    // Add the elements to the sequence
    for (unsigned int index = 0; index < n_elem; index++) {
      add(tempList[index]);
      delete tempList[index];
    }

    if (is_terminal) stream->close_children();
  }

  virtual BaseItem *inflate_element(RootsStream *stream, int list_index) {
    return T::inflate_object(stream, list_index);
  }

 private:
  std::deque<T *> elements;
  const std::deque<T *> &get_elements() const { return elements; };
};
}
}

#endif /* TEMPLATESEQUENCE_H_ */
