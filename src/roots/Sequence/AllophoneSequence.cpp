/*
    This file is part of Roots.

    Roots is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Roots is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Roots.  If not, see <http://www.gnu.org/licenses/>.

    Copyright (c) 2013, 2014 IRISA
*/


#include "AllophoneSequence.h"

namespace roots
{

namespace sequence
{

	Sequence* AllophoneSequence::inflate_sequence(RootsStream * stream,	const std::string& /* payloadTagName */, int list_index)
	{
		roots::sequence::Sequence *t = new roots::sequence::AllophoneSequence(true);
		t->inflate(stream,true,list_index);//, payloadTagName);
		return t;
	}

	
} }
