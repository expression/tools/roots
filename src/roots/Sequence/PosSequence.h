/*
    This file is part of Roots.

    Roots is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Roots is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Roots.  If not, see <http://www.gnu.org/licenses/>.

    Copyright (c) 2013, 2014 IRISA
*/


#ifndef POSSEQUENCE_H
#define POSSEQUENCE_H

#include "../common.h"
#include "../Sequence.h"
#include "./TemplateSequence.h"
#include "../Linguistic/POS.h"
#include "../Linguistic/POS/Adjective.h"
#include "../Linguistic/POS/Adverb.h"
#include "../Linguistic/POS/Conjunction.h"
#include "../Linguistic/POS/Determiner.h"
#include "../Linguistic/POS/Interjection.h"
#include "../Linguistic/POS/Noun.h"
#include "../Linguistic/POS/Preposition.h"
#include "../Linguistic/POS/Pronoun.h"
#include "../Linguistic/POS/Punctuation.h"
#include "../Linguistic/POS/Verb.h"
#include "../Linguistic/POS/ForeignWord.h"
#include "../Linguistic/POS/Prefix.h"
#include "../Linguistic/POS/Particle.h"
#include "../Linguistic/POS/Symbol.h"
#include "../Linguistic/POS/Determiner/Article.h"
#include "../Linguistic/POS/Determiner/Demonstrative.h"
#include "../Linguistic/POS/Determiner/Indefinite.h"
#include "../Linguistic/POS/Determiner/Interrogative.h"
#include "../Linguistic/POS/Determiner/Numeral.h"
#include "../Linguistic/POS/Determiner/Possessive.h"

namespace roots
{

  namespace sequence
  {

    class PosSequence: public roots::sequence::TemplateSequence<roots::linguistic::pos::POS>
      {
      public:
      PosSequence(bool noInit = false):roots::sequence::TemplateSequence<roots::linguistic::pos::POS>(noInit) {};
      
      PosSequence(const std::string & _label, bool noInit = false):roots::sequence::TemplateSequence<roots::linguistic::pos::POS>(noInit)
      {
	set_label(_label);
      };

	virtual std::string get_classname() const { return classname(); };
	/**
	 * @brief returns classname for current class
	 * @return string constant representing the classname
	 */
	static std::string classname() { return "Sequence::POS"; }

	virtual PosSequence *clone() const
	{
	  return new PosSequence(*this);
	}

	virtual PosSequence *clone_empty() const
	{
	  return new PosSequence();
	}

	virtual BaseItem *inflate_element(RootsStream * stream, int list_index=-1);

	/**
	 * @brief Extracts the entity from the stream
	 * @param stream the stream from which we inflate the element
	 * @param payloadTagName xml tag name used for sequence's contents
	 * @return pointer to the inflated sequence
	 */
	static Sequence* inflate_sequence(RootsStream * stream,	const std::string& payloadTagName, int list_index=-1);

      protected:
	~PosSequence(){}; // Use Destroy instead
      };
  }
}

#endif // POSSEQUENCE_H
