/*
    This file is part of Roots.

    Roots is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Roots is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Roots.  If not, see <http://www.gnu.org/licenses/>.

    Copyright (c) 2013, 2014 IRISA
*/


#include "WordSequence.h"

namespace roots
{

  namespace sequence
  {

    Matrix *WordSequence::make_mapping(sequence::Sequence *targetSequence)
    {
      Matrix *mapping = NULL;

      char wordSep = '|';

      std::vector<char> sourceSeq, targetSeq;
      std::vector<int> sourceMapping, targetMapping;

      if (dynamic_cast<sequence::WordSequence * >(targetSequence) == NULL)
				{
					return Sequence::make_mapping(targetSequence);
				}

      /* Build vector of UCHAR from this sequence for levenshtein */
      for(unsigned int i=0; i<this->count(); i++)
	{
	  std::string word = this->get_item(i)->get_label();
	  const char *str = word.c_str();

	  int j=0;
	  while(str[j]!='\0')
	    {
	      sourceSeq.push_back(str[j]);
	      j++;
	    }
	  sourceSeq.push_back(wordSep);
	}

      /* Build vector of UCHAR from target sequence for levenshtein */
      for(unsigned int i=0; i<targetSequence->count(); i++)
	{
	  std::string word = targetSequence->get_item(i)->get_label();
	  const char *str = word.c_str();

	  int j=0;
	  while(str[j]!='\0')
	    {
	      targetSeq.push_back(str[j]);
	      j++;
	    }
	  targetSeq.push_back(wordSep);
	}

      // Debug
      /*
	cout<<endl << "<< " << endl;

	for(unsigned int i=0; i<sourceSeq.size(); i++)
	{
	cout << std::string(sourceSeq[i]);
	}
	cout<<endl << endl;
	for(unsigned int i=0; i<targetSeq.size(); i++)
	{
	cout << std::string(targetSeq[i]);
	}
	cout<<endl << " >> " << endl;
	fflush(stdout);
      */

      /* Prepare mapping */
      mapping = new SparseMatrix(this->count(), targetSequence->count());
      if(this->count()==0||targetSequence->count()==0)
	{
	  return mapping;
	}

      /* Align UCHARs using Levenshtein */
      roots::align::Levenshtein * aligner = new roots::align::Levenshtein(sourceSeq, targetSeq);
      aligner->dpa_standard();

      sourceMapping = aligner->get_source_mapping();
      targetMapping = aligner->get_target_mapping();

      if(sourceMapping.size() != targetMapping.size())
	{
	   std::stringstream ss;
	  ss << "source and target mapping of incorrect size!\n";
	  throw RootsException(__FILE__, __LINE__, ss.str().c_str());
	}

      /* Rebuild word align from uchar align */
      int sourceWordIndex = 0;
      int targetWordIndex = 0;
      bool currentSourceIsWordSep = false;
      bool currentTargetIsWordSep = false;

      /* Init alignment	 : shift anlign words */

      if(!sourceMapping.empty())
	{
	  for(int i=0; i<sourceMapping[0]; i++)
	    {
	      if(sourceSeq[i] == wordSep)
		{ sourceWordIndex++; }
	    }
	}
      if(!targetMapping.empty())
	{
	  for(int i=0; i<targetMapping[0]; i++)
	    {
	      if(targetSeq[i] == wordSep)
		{ targetWordIndex++; }
	    }
	}

      /* Build alignment */
      for(unsigned int i=0; i<sourceMapping.size(); i++)
	{
	  if(sourceMapping[i] > 0)
	    { currentSourceIsWordSep = sourceSeq[sourceMapping[i]] == wordSep; }
	  else
	    { currentSourceIsWordSep = false; }

	  if(targetMapping[i] > 0)
	    { currentTargetIsWordSep = targetSeq[targetMapping[i]] == wordSep; }
	  else
	    { currentTargetIsWordSep = false; }

	  if(currentSourceIsWordSep || currentTargetIsWordSep)
	    {
	      mapping->set_element(sourceWordIndex, targetWordIndex, 1);
	      if(currentSourceIsWordSep)
		{ sourceWordIndex++; }
	      if(currentTargetIsWordSep)
		{ targetWordIndex++; }
	    }
	}

      delete aligner;

      return mapping;
    }


    BaseItem *WordSequence::inflate_element(RootsStream * stream, int list_index)
    {
      roots::linguistic::Word *s;

      std::string classname = stream->get_object_classname_no_read(linguistic::Word::xml_tag_name(), list_index);

      if(classname.compare(linguistic::Word::classname()) == 0)
	{
	  s = linguistic::Word::inflate_object(stream,list_index);
	} else {
	 std::stringstream ss;
	ss << classname << " element type is unknown!";
	throw RootsException(__FILE__, __LINE__, ss.str().c_str());
      }

      return s;
    }


    Sequence* WordSequence::inflate_sequence(RootsStream * stream,	const std::string& /* payloadTagName */, int list_index)
    {
      roots::sequence::Sequence *t = new roots::sequence::WordSequence(true);
      t->inflate(stream,true,list_index);//, payloadTagName);
      return t;
    }

  }

}

