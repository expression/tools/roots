/*
    This file is part of Roots.

    Roots is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Roots is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Roots.  If not, see <http://www.gnu.org/licenses/>.

    Copyright (c) 2013, 2014 IRISA
*/


#include "PhonemeBlockSequence.h"


namespace roots
{

  namespace sequence
  {

    PhonemeBlockSequence::PhonemeBlockSequence(bool noInit):roots::sequence::TemplateSequence<roots::phonology::PhonemeBlock>(noInit) {
      this->set_is_simple_item(false);
    }
    
    PhonemeBlockSequence::PhonemeBlockSequence(const std::string & _label, bool noInit):roots::sequence::TemplateSequence<roots::phonology::PhonemeBlock>(noInit) {
      this->set_label(_label);
      this->set_is_simple_item(false);
    }

    PhonemeBlockSequence::PhonemeBlockSequence ( const PhonemeBlockSequence &seq ) : TemplateSequence<phonology::PhonemeBlock>(seq)
    {
      this->set_is_simple_item(false);
    }

    Sequence *PhonemeBlockSequence::get_related_sequence() const
    {
      if(this->count()<=0)
	{
	  ROOTS_LOGGER_WARN("SyllableSequence::get_related_sequence : Empty sequence. I will return NULL but it could be something unexpected!");
	  return NULL;
	}
      return this->get_item(0)->get_related_sequence();
    }

    Relation *PhonemeBlockSequence::get_embedded_relation()
    {
      if(this->count()<=0){
	   std::stringstream ss;
	  ss << "PhonemeBlock sequence is empty, unable to find the linked phoneme sequence !";
	  throw RootsException(__FILE__, __LINE__, ss.str().c_str());
	}

      Relation *embedded_relation = new Relation(this, get_related_sequence());

      Matrix *map = embedded_relation->get_mapping();

      for(unsigned int index=0; index < this->count(); index++){
	// For each phoneme block of index index
	unsigned int start = this->get_item(index)->get_start_index();
	unsigned int end = this->get_item(index)->get_end_index();
	for(unsigned int phIdx=start; phIdx<=end; phIdx++) {
	  map->set_element(index, phIdx, 1);
	}
      }

      return embedded_relation;
    }

    // !!! linking_relation must be in the direction old phonemes => new phonemes
    void PhonemeBlockSequence::set_embedded_relation(const roots::Relation & new_relation,
						     roots::sequence::PhonemeSequence *phoneme_sequence)
    {
      Matrix *map = new_relation.get_mapping();
      std::vector<int> set;

      unsigned int n_col = map->get_n_col();

      if (this->count() > 0)
	{
	  if (this->count() != (unsigned int) map->get_n_row())
	    {
	       std::stringstream ss;
	      ss << "Unable to substitute the embedded relation : New relation has not the same number of rows !";
	      throw RootsException(__FILE__, __LINE__, ss.str().c_str());
	    }

	  for(unsigned int index=0; index < this->count(); index++)
	    {
	      for (unsigned int i=0; i<n_col; i++)
		{
		  if (map->get_element(index, i) == 1)
		    {
		      set.push_back(i);
		    }
		}

	      this->get_item(index)->set_phoneme_sequence(phoneme_sequence);
	      this->get_item(index)->set_start_index(set.front());
	      this->get_item(index)->set_end_index(set.back());

	      set.clear();
	    }
	}
    }

    BaseItem *PhonemeBlockSequence::inflate_element(RootsStream * stream, int list_index)
    {
      roots::phonology::PhonemeBlock *p;

      std::string classname = stream->get_object_classname_no_read(phonology::PhonemeBlock::xml_tag_name(), list_index);

      if(classname.compare(phonology::PhonemeBlock::classname()) == 0)
	{
	  p = phonology::PhonemeBlock::inflate_object(stream,list_index);
	} else {
	 std::stringstream ss;
	ss << classname << " element type is unknown!";
	throw RootsException(__FILE__, __LINE__, ss.str().c_str());
      }

      return p;
    }


    Sequence* PhonemeBlockSequence::inflate_sequence(RootsStream * stream,	const std::string& payloadTagName __attribute__((unused)), int list_index)
    {
      roots::sequence::Sequence *t = new roots::sequence::PhonemeBlockSequence(true);
      t->inflate(stream,true,list_index);//, payloadTagName);
      return t;
    }


  } }

