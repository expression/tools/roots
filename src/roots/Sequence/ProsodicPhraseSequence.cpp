/*
    This file is part of Roots.

    Roots is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Roots is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Roots.  If not, see <http://www.gnu.org/licenses/>.

    Copyright (c) 2013, 2014 IRISA
*/


#include "ProsodicPhraseSequence.h"

namespace roots
{

namespace sequence
{

  ProsodicPhraseSequence::ProsodicPhraseSequence(bool noInit): roots::sequence::TemplateSequence<roots::phonology::prosodicphrase::ProsodicPhrase>(noInit)
{
	this->set_is_simple_item(false);
}

ProsodicPhraseSequence::ProsodicPhraseSequence ( const ProsodicPhraseSequence &seq ) : TemplateSequence<phonology::prosodicphrase::ProsodicPhrase>(seq)
{
	this->set_is_simple_item(false);
}

Relation *ProsodicPhraseSequence::get_embedded_relation()
{
	Relation *embedded_relation = new Relation(this, this->get_item(0)->get_phoneme_sequence());
	if(this->count()>0)
	{	
		Matrix *map = embedded_relation->get_mapping();

		for(unsigned int index=0; index < this->count(); index++)
		{
			std::vector<int> phonemeIndexArray = this->get_item(index)->get_phoneme_index_array();
			for(unsigned int i=0; i<phonemeIndexArray.size(); i++) map->set_element(index, phonemeIndexArray[i], 1);
		}

		//return embedded_relation;
	}
	return embedded_relation;
}

	BaseItem *ProsodicPhraseSequence::inflate_element(RootsStream * stream, int list_index)
	{
		roots::phonology::prosodicphrase::ProsodicPhrase *s;

		std::string classname = stream->get_object_classname_no_read(phonology::prosodicphrase::ProsodicPhrase::xml_tag_name(), list_index);

		if(classname.compare(phonology::prosodicphrase::ProsodicPhrase::classname()) == 0)
		{
			s = phonology::prosodicphrase::ProsodicPhrase::inflate_object(stream,list_index);
		} else {
			 std::stringstream ss;
			ss << classname << " element type is unknown!";
			throw RootsException(__FILE__, __LINE__, ss.str().c_str());
		}


		return s;
	}


	Sequence* ProsodicPhraseSequence::inflate_sequence(RootsStream * stream,	const std::string& /* payloadTagName */, int list_index)
	{
		roots::sequence::Sequence *t = new roots::sequence::ProsodicPhraseSequence(true);
		t->inflate(stream,true,list_index);//, payloadTagName);
		return t;
	}

} }

