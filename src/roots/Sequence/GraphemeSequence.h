/*
    This file is part of Roots.

    Roots is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Roots is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Roots.  If not, see <http://www.gnu.org/licenses/>.

    Copyright (c) 2013, 2014 IRISA
*/


#ifndef GRAPHEMESEQUENCE_H
#define GRAPHEMESEQUENCE_H

#include "../common.h"
#include "../Sequence.h"
#include "./TemplateSequence.h"
#include "./WordSequence.h"
#include "../Linguistic/Grapheme.h"

#include "../Align.h"
#include "../Align/Levenshtein.h"

namespace roots
{

  namespace sequence
  {

    class GraphemeSequence: public TemplateSequence<roots::linguistic::Grapheme>
    {
    public:
    GraphemeSequence(const std::string &lab="", bool noInit = false):TemplateSequence<roots::linguistic::Grapheme>(noInit){ set_label(lab); };

      /**
       * @brief return a string of the sequence
       * @param level level of display :
       *          0 : concatenation of each grapheme (so it return the raw text)
       *          1 : each grapheme are separetaed by a blanks
       **/
      virtual std::string to_string(int level=0) const;

      virtual roots::Matrix *make_mapping(roots::sequence::Sequence *targetSequence);

      virtual std::string get_classname() const { return classname(); };
      /**
       * @brief returns classname for current class
       * @return string constant representing the classname
       */
      static std::string classname() { return "Sequence::Grapheme"; }

      virtual GraphemeSequence *clone() const
      {
	return new GraphemeSequence(*this);
      }

      virtual GraphemeSequence *clone_empty() const
      {
	return new GraphemeSequence();
      }

      virtual BaseItem *inflate_element(RootsStream * stream, int list_index=-1);

      /**
       * @brief Extracts the entity from the stream
       * @param stream the stream from which we inflate the element
       * @param payloadTagName xml tag name used for sequence's contents
       * @return pointer to the inflated sequence
       */
      static Sequence* inflate_sequence(RootsStream * stream,	const std::string& payloadTagName, int list_index=-1);

    protected:
      ~GraphemeSequence(){}; // Use Destroy instead

    };

  }

}

#endif // GRAPHEMESEQUENCE_H
