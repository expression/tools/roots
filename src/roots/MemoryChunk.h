/*
    This file is part of Roots.

    Roots is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Roots is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Roots.  If not, see <http://www.gnu.org/licenses/>.

    Copyright (c) 2013, 2014 IRISA
*/


#ifndef MEMORYCHUNK_H
#define MEMORYCHUNK_H

#include "BaseChunk.h"

namespace roots {

  /**
   * @todo fonction to get a full utterance set (need a merge function)
   * @todo documentation
   **/
  class MemoryChunk :  public BaseChunk
  {

    /* Constructor / Destructor / Copy */
  public:
    MemoryChunk();
    MemoryChunk(const std::vector<std::string> & layerNames);

    MemoryChunk(const MemoryChunk& mchunk);
    MemoryChunk& operator= (const MemoryChunk& mchunk);
    MemoryChunk * clone() const;
    BaseChunk * clone_to_basechunk() const;

    virtual ~MemoryChunk();

  public:
    
    /**
     * @brief Returns the number of layers
     **/
    virtual int count_layers() const;
    
    /**
     * @brief Returns the name of all the layers described in the group of UtteranceSets
     */
    virtual std::vector<std::string> get_all_layer_names() const;
    
    /**
     * @brief Clears temporary data from the group of layers
     * @note This method is used to handle caching
     */
    virtual void unload_all();
    
    /**
     * @brief Loads all the layers in memory
     * @note This method is used to handle caching
     */
    virtual void load_all();

    /**
     * @brief Test if everything is loaded to get an utterance
     * @note return true (do not test id range)
     */
    virtual bool is_loaded_for_utterance(const int id);

    /**
     * @brief Returns the number of utterances
     * @note Complexity is O(1)
     */
    virtual int count_utterances() const;
    
    /**
     * @brief Update the number of utterances
     * @note may loads files
     */
    virtual void update_counts();
    
    /**
     * @brief Deletes an utterance (all layers)
     * @param id ID of the utterance to be removed. Default ID is 0 (first utterance).
     */
    virtual void delete_utterance(int id = 0);
    
    
    /**
     * @brief Removes all utterances and cleans the chunk
     */
    virtual void clear();

    /**
     * @brief Returns an utterance where all layers are merged
     * @warning Allocates a new full utterance, requires to free memory at some point
     * @return pointer of the loaded utterance
     */
    virtual roots::Utterance* get_utterance(int id = 0);

    /**
     * @brief Returns an utterance where all layers are merged
     * @warning Allocates a new full utterance, requires to free memory at some point
     * @return pointer of the loaded utterance
     */
    virtual roots::Utterance* get_utterance(const std::vector<std::string> & layerToLoad,
					    int id = 0);

   /**
     * @brief Returns all utterances by layers 
     */
    virtual const std::map<std::string, std::vector<Utterance> > & get_utterances() const;

    /**
     * @brief Adds a new utterance in the current file group, assuming there is no layer
     * @param utt pointer to the utterance
     */
    virtual void add_utterance(const Utterance& utt, 
							   const std::string layerName=ANONYMOUS_LAYER) ;
		//  throw(RootsException);
      
    /**
     * @brief Adds a new (sliced) utterance in the current file group
     * @param utt map giving names to utterance pointers (one utterance per layer)
     */
    virtual void add_utterance( const std::map< std::string, Utterance >& utt ); // throw(RootsException);
    
    /**
     * @brief Adds new utterances in the current file group, assuming there is no layer 
     * @param utt pointer to the utterance
     */
    virtual void add_utterances(const std::vector<Utterance>& utt, 
								const std::string layerName=ANONYMOUS_LAYER);
    //  throw(RootsException);

     /**
     * @brief Adds a new (sliced) utterance in the current file group
     * @param utt map giving layer names to utterances 
     */
    virtual void add_utterances(const std::map<std::string, std::vector<Utterance> >& utt);
    //  throw(RootsException);
    
    /**
     * @brief Replace a given utterance by another
     * @param id ID of the utterance to be replaced
     * @param utt the new utterance
     **/
    virtual void update_utterance(int id, const Utterance& utt);
    
    /**
     * @brief Replace a given utterance by another
     * @param id ID of the utterance to be replaced
     * @param utt the new utterance split into layers
     **/
    virtual void update_utterance(int id, const std::map<std::string, Utterance> &utt);

    /**
     * @brief Generate the XML part of the entity
     * @param stream
     * @param is_terminal
     */
    virtual void deflate(RootsStream * stream, bool is_terminal=true,
			 int list_index=-1);

    /**
     * @brief Extracts the entity from the stream
     * @param stream the stream from which we inflate the element
     */
    virtual void inflate(RootsStream * stream, bool is_terminal=true,
			 int list_index=-1);

    /**
     * @brief returns the XML tag name value for current object
     * @return std::string constant representing the XML tag name
     */
    virtual std::string get_xml_tag_name() const {
      return xml_tag_name();
    };

    /**
     * @brief returns classname for current object
     * @return std::string constant representing the classname
     */
    virtual std::string get_classname() const {
      return classname();
    };
    
    /**
     * @brief returns classname for current class
     * @return std::string constant representing the classname
     */
    static std::string classname() {
      return "MemoryChunk";
    };
 
    /**
     * @brief Prepare a chunk to be saved in a directory. Basicaly, a signal segment items will copy the associated wav file if target_base_dir change.
     * @Warning items in sequences may be changed (for instance the base directory of signal segment items)!
     */
    virtual void prepare_save(const std::string & target_base_dir);


  protected:
    /**
     * @brief Load with fusion of utterances in each files
     * @return pointer of the loaded utterance
     * @todo Doc
     */
    virtual void load_in_utterance(const std::string & fileToAdd,
				   int index,
				   roots::Utterance *& utterance
				   );

    /**
     * @brief Correspondances between each layer name (string) and its respective list of utterances
     */
    std::map<std::string, std::vector<Utterance> > uttLayers;

  };

}

#endif // LOCALGROUP_H
