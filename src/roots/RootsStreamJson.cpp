/*
    This file is part of Roots.

    Roots is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Roots is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Roots.  If not, see <http://www.gnu.org/licenses/>.

    Copyright (c) 2013, 2014 IRISA
*/

#include "RootsStreamJson.h"
#include <boost/filesystem.hpp>

namespace bfs = boost::filesystem;

namespace roots {

RootsStreamJson::RootsStreamJson() { init(); }

RootsStreamJson::~RootsStreamJson() {}

void RootsStreamJson::init() {
  RootsStream::init();
  rootNode = Json::Value();
  currentNode = &rootNode;
  this->parentNodes = std::stack<Json::Value*>();
}

void RootsStreamJson::set_current_node(Json::Value* currentNode) {
  this->currentNode = currentNode;
}

void RootsStreamJson::set_root_node(const Json::Value& rootNode) {
  this->rootNode = rootNode;
}

Json::Value& RootsStreamJson::get_current_node() { return *currentNode; }

Json::Value& RootsStreamJson::get_root_node() { return rootNode; }

// Public
void RootsStreamJson::clear() { init(); }

// Private
void RootsStreamJson::clear_doc() { init(); }

void RootsStreamJson::load(const std::string& _filename) {
  std::string baseDirName;
  std::ifstream instream;

  init();
  baseDirName = bfs::path(_filename).parent_path().string();

  // Setting the instance attribute containing the path to the files' directory
  // cout << "===================== " << baseDirName <<  std::endl;
  set_base_dir_name(baseDirName);

  instream.open(_filename.c_str());
  if (instream.fail()) {
    fflush(stderr);
    std::stringstream ss;
    ss << "Error in \"" << _filename << "\" : ";
    if (instream.bad()) {
      ss << " Because of bad io operation ! ";
    }
    std::cerr << ss.str() << std::endl;
    throw RootsException(__FILE__, __LINE__, ss.str().c_str());
  }

  std::locale utfFile("en_US.UTF-8");
  instream.imbue(utfFile);

  Json::Reader parser;

  if (!parser.parse(instream, rootNode)) {
    throw RootsException(__FILE__, __LINE__,
                         "Impossible to parse file, check its format.\n");
  }
  instream.close();

  // cout << rootNode << std::endl;
  // show_member_names();

  currentNode = &rootNode;
  parentNodes = std::stack<Json::Value*>();
  parentNodes.push(&rootNode);
}

void RootsStreamJson::from_string(const std::string& _json,
                                  const std::string _baseDirName) {
  std::stringstream ss;
  ss << _json;

  set_base_dir_name(_baseDirName);

  Json::Reader parser;

  if (!parser.parse(ss, rootNode)) {
    throw RootsException(__FILE__, __LINE__,
                         "Impossible to parse file, check its format.\n");
  }

  currentNode = &rootNode;
  parentNodes = std::stack<Json::Value*>();
  parentNodes.push(&rootNode);
}

void RootsStreamJson::save(const std::string& _filename) {
  std::ofstream ofile;
  std::stringstream ss;
  Json::FastWriter fastWriter;

  ss << _filename;

  ofile.open(ss.str().c_str());
  if (ofile.fail()) {
    throw RootsException(__FILE__, __LINE__,
                         "Impossible to open file " + ss.str() + "\n");
  } else {
    // ofile << rootNode.toStyledString();
    ofile << fastWriter.write(rootNode);
    ofile.close();
  }
}

std::string RootsStreamJson::to_string() {
  return std::string(rootNode.toStyledString().c_str());
}

void RootsStreamJson::append_object(const char* name, int list_index) {
  Json::Value& pNode = get_current_node();
  this->parentNodes.push(&pNode);

#if ROOTS_STREAM_JSON_DEBUG == 1
  cout << "APPEND_OBJECT: " << name << " with index " << list_index
       << std::endl;
#endif
  if (list_index > -1) {
    if (!pNode.isMember(name)) {
      pNode[name] = Json::Value::null;
    }
    Json::Value element;
    pNode[name][list_index] = element;
    set_current_node(&pNode[name][list_index]);
  } else {
    Json::Value element;
    pNode[name] = element;
    set_current_node(&pNode[name]);
  }
}

void RootsStreamJson::open_object(const char* name, int list_index) {
  Json::Value& pNode = get_current_node();

  this->parentNodes.push(&pNode);
  if (list_index > -1) {
    set_current_node(&((pNode)[name][list_index]));
  } else {
    set_current_node(&((pNode)[name]));
  }
}

void RootsStreamJson::close_object() {
  if (this->parentNodes.size() > 0) {
    Json::Value* pNode = this->parentNodes.top();
    this->parentNodes.pop();
    set_current_node(pNode);
  } else {
    set_current_node(NULL);
  }
}

void RootsStreamJson::set_object_classname(const std::string& classname) {
  set_unicodestring_content("class", classname);
}

std::string RootsStreamJson::get_object_classname() {
  std::string classname;
  classname = get_unicodestring_content("class");
  return classname;
}

// go to first child node
void RootsStreamJson::open_children() {}

// go back to parent node
void RootsStreamJson::close_children() {
#if ROOTS_STREAM_JSON_DEBUG == 1
  cout << "CLOSE_CHILDREN" << std::endl;
#endif
  close_object();
}

void RootsStreamJson::next_child() {}

std::string RootsStreamJson::get_object_classname_no_read(
    const std::string& name, int list_index) {
  Json::Value& pNode = get_current_node();
  std::string str = "";

  if (pNode.isMember(name)) {
    if (list_index > -1) {
      str = pNode[name][list_index]["class"].asCString();
    } else {
      str = pNode[name]["class"].asCString();
    }

#if ROOTS_STREAM_JSON_DEBUG == 1
    cout << "OBJECT_CLASSNAME_NO_READ: " << str << std::endl;
#endif
  }
#if ROOTS_STREAM_JSON_DEBUG == 1
  else {
    cout << "OBJECT_CLASSNAME_NO_READ: not found : " << name << std::endl;
  }
#endif
  return str;
}

bool RootsStreamJson::has_child(const std::string& name) {
  return get_current_node().isMember(name.c_str());
}

bool RootsStreamJson::has_n_children(const std::string& name,
                                     const unsigned int n) {
  Json::Value& pNode = get_current_node();

  if (n == 1) {
    return pNode.isMember(name) && !pNode[name].isArray();
  } else {
    return ((pNode.isMember(name)) && pNode[name].isArray() &&
            (pNode[name].size() == n));
  }
}

unsigned int RootsStreamJson::get_n_children(const std::string& name) {
  Json::Value& pNode = get_current_node();

  if (pNode.isMember(name) && !pNode[name].isArray()) {
    return 1;
  } else if ((pNode.isMember(name)) && pNode[name].isArray()) {
    return pNode[name].size();
  } else {
    return 0;
  }
}

//////////////////////////////////////////

#define IMPLEMENT_DEFLATE_TYPE(tna, tva)                          \
  const tva& RootsStreamJson::deflate_##tna(const tva& content) { \
    return content;                                               \
  }

/*#define IMPLEMENT_INFLATE_TYPE(tna, tva, tmisa, tmasa)
\
void RootsStreamJson::inflate_ ## tna (const Json::Value& node, tva & ret) \
{ \
        if(node.tmisa ())           \
        { \
        ret = node.tmasa ();        \
        }else{                \
               std::stringstream ss;        \
               ss << "value <" << node << "> for node is not convertible to " <<
#tva ;\
               throw RootsException(__FILE__,__LINE__, ss.str()); \
        } \
}*/

#define IMPLEMENT_INFLATE_TYPE(tna, tva, tmisa, tmasa)                     \
  void RootsStreamJson::inflate_##tna(const Json::Value& node, tva& ret) { \
    std::stringstream ss;                                                  \
    ss << node;                                                            \
    ss >> ret;                                                             \
  }

#define IMPLEMENT_APPEND(tna, tva)                                   \
  void RootsStreamJson::append_##tna##_content(const char* name,     \
                                               const tva& content) { \
    Json::Value& pNode = get_current_node();                         \
    (pNode)[name] = deflate_##tna(content);                          \
  }

#define IMPLEMENT_SET_CONTENT_FOR_NODE(tna, tva)                  \
  void RootsStreamJson::set_##tna##_content(const char* name,     \
                                            const tva& content) { \
    Json::Value& pNode = get_current_node();                      \
    (pNode)[name] = deflate_##tna(content);                       \
  }

#if ROOTS_STREAM_JSON_DEBUG == 1
#define DISP_CONTENT_FOR_NODE \
  cout << "get content for node " << name << std::endl;
#else
#define DISP_CONTENT_FOR_NODE
#endif

/*
  #define IMPLEMENT_GET_CONTENT_FOR_NODE(tna, tva)      \
  tva get_ ## tna ## _content (const char * name)   \
  {               \
  DISP_CONTENT_FOR_NODE         \
  tva value;            \
  Json::Value &node = get_current_node();     \
  if(!node.isMember(name))          \
  {             \
   std::stringstream ss;          \
  ss << "expecting <" << name << "> but not found!";  \
  throw RootsException(__FILE__,__LINE__, ss.str());  \
  }             \
  value = extract_ ## tna ## _node(node[name]);   \
  next_child();           \
  return value;           \
  }*/
#define IMPLEMENT_GET_CONTENT_FOR_NODE(tna, tva)               \
  tva RootsStreamJson::get_##tna##_content(const char* name) { \
    DISP_CONTENT_FOR_NODE                                      \
    tva value;                                                 \
    Json::Value& node = get_current_node();                    \
    if (!node.isMember(name)) {                                \
      std::stringstream ss;                                    \
      ss << "expecting <" << name << "> but not found!";       \
      throw RootsException(__FILE__, __LINE__, ss.str());      \
    }                                                          \
    inflate_##tna(node[name], value);                          \
    next_child();                                              \
    return value;                                              \
  }

#define IMPLEMENT_APPEND_VECTOR(tna, tva, cva)                       \
  void RootsStreamJson::append_##tna##_content(const char* name,     \
                                               const tva& content) { \
    set_##tna##_content(name, content);                              \
  }

#define IMPLEMENT_SET_CONTENT_VECTOR_FOR_NODE(tna, tva, cva)             \
  void RootsStreamJson::set_vector_##tna##_content(const char* name,     \
                                                   const tva& content) { \
    append_object(name);                                                 \
    Json::Value& node = get_current_node();                              \
    (node)["n_elem"] = (unsigned int)content.size();                     \
    (node)["elements"] = Json::Value::null;                              \
    for (unsigned int index = 0; index < content.size(); ++index) {      \
      (node)["elements"].append(deflate_##tna(content[index]));          \
    }                                                                    \
    close_object();                                                      \
  }

#define IMPLEMENT_GET_CONTENT_VECTOR_FOR_NODE(tna, tva, cva)          \
  tva RootsStreamJson::get_vector_##tna##_content(const char* name) { \
    DISP_CONTENT_FOR_NODE                                             \
    Json::Value& node = get_current_node();                           \
    if (!(node).isMember(name)) {                                     \
      std::stringstream ss;                                           \
      ss << "expecting <" << name << "> but not found!";              \
      throw RootsException(__FILE__, __LINE__, ss.str());             \
    }                                                                 \
    tva vec = tva();                                                  \
    unsigned int n_elem = (node)[name]["n_elem"].asUInt();            \
    for (unsigned int index = 0; index < n_elem; ++index) {           \
      cva value;                                                      \
      inflate_##tna(node[name]["elements"][index], value);            \
      vec.push_back(value);                                           \
    }                                                                 \
    return vec;                                                       \
  }

#define IMPLEMENT_APPEND_SET(tna, tva, cva)                          \
  void RootsStreamJson::append_##tna##_content(const char* name,     \
                                               const tva& content) { \
    set_##tna##_content(name, content);                              \
  }

#define IMPLEMENT_SET_CONTENT_SET_FOR_NODE(tna, tva, cva)                   \
  void RootsStreamJson::set_set_##tna##_content(const char* name,           \
                                                const tva& content) {       \
    append_object(name);                                                    \
    Json::Value& node = get_current_node();                                 \
    (node)["n_elem"] = (unsigned int)content.size();                        \
    (node)["elements"] = Json::Value::null;                                 \
    for (tva::const_iterator iter = content.begin(); iter != content.end(); \
         ++iter) {                                                          \
      (node)["elements"].append(deflate_##tna(*iter));                      \
    }                                                                       \
    close_object();                                                         \
  }

#define IMPLEMENT_GET_CONTENT_SET_FOR_NODE(tna, tva, cva)          \
  tva RootsStreamJson::get_set_##tna##_content(const char* name) { \
    DISP_CONTENT_FOR_NODE                                          \
    Json::Value& node = get_current_node();                        \
    if (!(node).isMember(name)) {                                  \
      std::stringstream ss;                                        \
      ss << "expecting <" << name << "> but not found!";           \
      throw RootsException(__FILE__, __LINE__, ss.str());          \
    }                                                              \
    tva vec = tva();                                               \
    unsigned int n_elem = (node)[name]["n_elem"].asUInt();         \
    for (unsigned int index = 0; index < n_elem; ++index) {        \
      cva value;                                                   \
      inflate_##tna(node[name]["elements"][index], value);         \
      vec.insert(value);                                           \
    }                                                              \
    return vec;                                                    \
  }

//////////////////////////////////////////

// std::string
/*
const char* RootsStreamJson::deflate_unicodestring (const std::string & content)
{
  throw RootsException (__FILE__, __LINE__,
                        "This function do not work ! don't use it !\n");
}
*/

void RootsStreamJson::inflate_unicodestring(const Json::Value& node,
                                            std::string& ret) {
  ret = std::string(node.asCString());
  //  std::cout << ret << std:: std::endl;
}

void RootsStreamJson::set_unicodestring_content(const char* name,
                                                const std::string& content) {
  Json::Value& pNode = get_current_node();
  (pNode)[name] = content.c_str();
}

void RootsStreamJson::append_unicodestring_content(const char* name,
                                                   const std::string& content) {
  Json::Value& pNode = get_current_node();
  (pNode)[name] = content.c_str();
}

// IMPLEMENT_APPEND(unicodestring, std::string);
// IMPLEMENT_SET_CONTENT_FOR_NODE(unicodestring, std::string);
IMPLEMENT_GET_CONTENT_FOR_NODE(unicodestring, std::string);

// char *
const char*& RootsStreamJson::deflate_cstr(const char*& content) {
  return content;
}
void RootsStreamJson::inflate_cstr(const Json::Value& node, char*& ret) {
  const char* strRef = node.asCString();
  ret = new char[strlen(strRef)];
  strcpy(ret, strRef);
}
IMPLEMENT_APPEND(cstr, char*);
IMPLEMENT_SET_CONTENT_FOR_NODE(cstr, char*);
IMPLEMENT_GET_CONTENT_FOR_NODE(cstr, char*);

// float
IMPLEMENT_DEFLATE_TYPE(float, float);
IMPLEMENT_INFLATE_TYPE(float, float, isDouble, asFloat);
IMPLEMENT_APPEND(float, float);
IMPLEMENT_SET_CONTENT_FOR_NODE(float, float);
IMPLEMENT_GET_CONTENT_FOR_NODE(float, float);

// double
IMPLEMENT_DEFLATE_TYPE(double, double);
IMPLEMENT_INFLATE_TYPE(double, double, isDouble, asDouble);
IMPLEMENT_APPEND(double, double);
IMPLEMENT_SET_CONTENT_FOR_NODE(double, double);
IMPLEMENT_GET_CONTENT_FOR_NODE(double, double);

// long double

double RootsStreamJson::deflate_long_double(const long double& content) {
  return (double)content;
}
IMPLEMENT_INFLATE_TYPE(long_double, long double, isDouble, asDouble);
IMPLEMENT_APPEND(long_double, long double);
IMPLEMENT_SET_CONTENT_FOR_NODE(long_double, long double);
IMPLEMENT_GET_CONTENT_FOR_NODE(long_double, long double);

// int
IMPLEMENT_DEFLATE_TYPE(int, int);
IMPLEMENT_INFLATE_TYPE(int, int, isIntegral, asInt);
IMPLEMENT_APPEND(int, int);
IMPLEMENT_SET_CONTENT_FOR_NODE(int, int);
IMPLEMENT_GET_CONTENT_FOR_NODE(int, int);

// unsigned int
IMPLEMENT_DEFLATE_TYPE(uint, unsigned int);
IMPLEMENT_INFLATE_TYPE(uint, unsigned int, isIntegral, asUInt);
IMPLEMENT_APPEND(uint, unsigned int);
IMPLEMENT_SET_CONTENT_FOR_NODE(uint, unsigned int);
IMPLEMENT_GET_CONTENT_FOR_NODE(uint, unsigned int);

// bool
IMPLEMENT_DEFLATE_TYPE(bool, bool);
void RootsStreamJson::inflate_bool(const Json::Value& node, bool& ret) {
  ret = node.asBool();
}
IMPLEMENT_APPEND(bool, bool);
IMPLEMENT_SET_CONTENT_FOR_NODE(bool, bool);
IMPLEMENT_GET_CONTENT_FOR_NODE(bool, bool);

// char
IMPLEMENT_DEFLATE_TYPE(char, char);
void RootsStreamJson::inflate_char(const Json::Value& node, char& ret) {
  std::stringstream ss;
  ss << ((char)node.asInt());
  ss >> ret;
}
IMPLEMENT_APPEND(char, char);
IMPLEMENT_SET_CONTENT_FOR_NODE(char, char);
IMPLEMENT_GET_CONTENT_FOR_NODE(char, char);

// UChar
IMPLEMENT_DEFLATE_TYPE(uchar, UChar);
void RootsStreamJson::inflate_uchar(const Json::Value& node, UChar& ret) {
  std::stringstream ss;
  ss << node;
  unsigned short int v;
  ss >> v;
  ret = v;
  /*UnicodeString s;
  ss >> s;
  ret = s[0];*/
}
IMPLEMENT_APPEND(uchar, UChar);
IMPLEMENT_SET_CONTENT_FOR_NODE(uchar, UChar);
IMPLEMENT_GET_CONTENT_FOR_NODE(uchar, UChar);

// std::vector<float>
IMPLEMENT_APPEND_VECTOR(vector_float, std::vector<float>, float);
IMPLEMENT_SET_CONTENT_VECTOR_FOR_NODE(float, std::vector<float>, float);
IMPLEMENT_GET_CONTENT_VECTOR_FOR_NODE(float, std::vector<float>, float);

// std::vector<int>
IMPLEMENT_APPEND_VECTOR(vector_int, std::vector<int>, int);
IMPLEMENT_SET_CONTENT_VECTOR_FOR_NODE(int, std::vector<int>, int);
IMPLEMENT_GET_CONTENT_VECTOR_FOR_NODE(int, std::vector<int>, int);

// std::vector<bool>
IMPLEMENT_APPEND_VECTOR(vector_bool, std::vector<bool>, bool);
IMPLEMENT_SET_CONTENT_VECTOR_FOR_NODE(bool, std::vector<bool>, bool);
IMPLEMENT_GET_CONTENT_VECTOR_FOR_NODE(bool, std::vector<bool>, bool);

// std::vector<UChar>
IMPLEMENT_APPEND_VECTOR(vector_uchar, std::vector<UChar>, UChar);
IMPLEMENT_SET_CONTENT_VECTOR_FOR_NODE(uchar, std::vector<UChar>, UChar);
IMPLEMENT_GET_CONTENT_VECTOR_FOR_NODE(uchar, std::vector<UChar>, UChar);

// std::set<int>
IMPLEMENT_APPEND_SET(set_int, std::set<int>, int);
IMPLEMENT_SET_CONTENT_SET_FOR_NODE(int, std::set<int>, int);
IMPLEMENT_GET_CONTENT_SET_FOR_NODE(int, std::set<int>, int);

// std::set<UChar>
IMPLEMENT_APPEND_SET(set_uchar, std::set<UChar>, UChar);
IMPLEMENT_SET_CONTENT_SET_FOR_NODE(uchar, std::set<UChar>, UChar);
IMPLEMENT_GET_CONTENT_SET_FOR_NODE(uchar, std::set<UChar>, UChar);

// std::vector<std::string>
IMPLEMENT_APPEND_VECTOR(vector_unicodestring, std::vector<std::string>,
                        std::string);
void RootsStreamJson::set_vector_unicodestring_content(
    const char* name, const std::vector<std::string>& content) {
  append_object(name);
  Json::Value& node = get_current_node();
  (node)["n_elem"] = (unsigned int)content.size();
  (node)["elements"] = Json::Value::null;
  for (unsigned int index = 0; index < content.size(); ++index) {
    (node)["elements"].append(content[index].c_str());
  }
  close_object();
}
IMPLEMENT_GET_CONTENT_VECTOR_FOR_NODE(unicodestring, std::vector<std::string>,
                                      std::string);

// boost::numeric::ublas::mapped_matrix<int>
void RootsStreamJson::append_matrix_int_content(
    const char* name,
    const boost::numeric::ublas::mapped_matrix<int>& content) {
  boost::numeric::ublas::mapped_matrix<int>::const_iterator1 iter1 =
      content.begin1();
  boost::numeric::ublas::mapped_matrix<int>::const_iterator2 iter2;

  append_object(name);
  Json::Value& pNode = get_current_node();

  (pNode)["n_row"] = (unsigned int)content.size1();
  (pNode)["n_col"] = (unsigned int)content.size2();

  unsigned int n_elem = 0;
  while (iter1 != content.end1()) {
    iter2 = iter1.begin();
    while (iter2 != iter1.end()) {
      if (*iter2 != 0) n_elem++;
      // n_elem++;
      iter2++;
    }
    iter1++;
  }
  (pNode)["n_elem"] = n_elem;

  (pNode)["elements"] = Json::Value::null;

  iter1 = content.begin1();
  while (iter1 != content.end1()) {
    iter2 = iter1.begin();
    // Use iterators to access only non-null elements
    while (iter2 != iter1.end()) {
      Json::Value curNode;
      /*curNode["row_index"] = (unsigned int) iter2.index1();
        curNode["column_index"] = (unsigned int) iter2.index2();
        curNode["value"] = (*iter2);*/
      if (*iter2 != 0) {
        curNode[0] = (unsigned int)iter2.index1();
        curNode[1] = (unsigned int)iter2.index2();
        curNode[2] = (*iter2);

        (pNode)["elements"].append(curNode);
      }
      iter2++;
    }

    iter1++;
  }
  close_object();
}

boost::numeric::ublas::mapped_matrix<int>
RootsStreamJson::get_matrix_int_content(const char* name) {
  boost::numeric::ublas::mapped_matrix<int> mat;
  unsigned int n_row = 0;
  unsigned int n_col = 0;
  unsigned int n_elem = 0;
  unsigned int row_index, col_index;
  int value;
  Json::Value& pNode = get_current_node();

  n_col = (pNode)[name]["n_col"].asUInt();
  n_elem = (pNode)[name]["n_elem"].asUInt();
  n_row = (pNode)[name]["n_row"].asUInt();

#if ROOTS_STREAM_JSON_DEBUG == 1
  cout << "MATRIX " << n_row << " " << n_col << " " << n_elem << std::endl;
#endif
  mat.resize(n_row, n_col, false);

  for (unsigned int index = 0; index < n_elem; ++index) {
    Json::Value curNode = (pNode)[name]["elements"][index];

    row_index = curNode[0].asUInt();
    col_index = curNode[1].asUInt();
    value = curNode[2].asUInt();

#if ROOTS_STREAM_JSON_DEBUG == 1
    cout << "MATRIX ELEM " << row_index << " " << col_index << " " << value
         << std::endl;
#endif
    mat(row_index, col_index) = value;
  }

  return mat;
}

//    void append_vector_unistr_content(const char * name, const
//std::vector<std::string>& content)
//   {
//     std::vector<std::string>::const_iterator iter;
//      std::stringstream convert;
//
//     append_object(name);
//     Json::Value & pNode = get_current_node();
//
//     (pNode)["n_elem"] = (unsigned int) content.size();
//     (pNode)["elements"] = Json::Value::null;
//
//     iter = content.begin();
//     while(iter != content.end())
//  {
//    std::string value = (*iter);
//    convert.str("");
//    convert << value;
//    std::string stringvalue = convert.str();
//
//    pNode["elements"].append(stringvalue);
//
//    iter++;
//  }
//     close_object();
//   }
//
//
//   std::vector<std::string> get_vector_unistr_content(const char * name)
//     {
//  unsigned int n_elem=0;
//  std::string key, value;
//  std::map<std::string, std::string> content;
//
//  Json::Value & pNode = get_current_node();
//
//   std::stringstream ss;
//  ss.str("");
//  ss << (pNode)[name]["n_elem"];
//  ss >> n_elem;
//
//  Json::Value::Members members =
//(pNode)[name]["elements"].getMemberNames();
//  assert(members.size() == n_elem);
//  for(unsigned int index=0; index<n_elem; ++index)
//    {
//      Json::Value curNode = (pNode)[name]["elements"][members[index]];
//
//      ss.str("");
//      ss << curNode.asCString();
//      content[std::string(members[index].c_str())] =
//std::string(ss.str().c_str());
//    }
//
//  return content;
//     }

// std::map<std::string, std::string>
void RootsStreamJson::append_map_unistr_unistr_content(
    const char* name, const std::map<std::string, std::string>& content) {
  std::map<std::string, std::string>::const_iterator iter;

  append_object(name);
  Json::Value& pNode = get_current_node();

  (pNode)["n_elem"] = (unsigned int)content.size();
  (pNode)["elements"] = Json::Value::null;

  iter = content.begin();
  while (iter != content.end()) {
    std::string value = (*iter).second;
    std::string key = (*iter).first;

    pNode["elements"][key] = value;

    iter++;
  }
  close_object();
}

std::map<std::string, std::string>
RootsStreamJson::get_map_unistr_unistr_content(const char* name) {
  unsigned int n_elem = 0;
  std::string key, value;
  std::map<std::string, std::string> content;

  Json::Value& pNode = get_current_node();

  n_elem = (pNode)[name]["n_elem"].asUInt();

  Json::Value::Members members = (pNode)[name]["elements"].getMemberNames();
  assert(members.size() == n_elem);
  for (unsigned int index = 0; index < n_elem; ++index) {
    Json::Value curNode = (pNode)[name]["elements"][members[index]];

    content[members[index]] = curNode.asString();
  }

  return content;
}
}  // End Namespace Roots
