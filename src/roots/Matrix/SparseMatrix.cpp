/*
  This file is part of Roots.

  Roots is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Roots is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with Roots.  If not, see <http://www.gnu.org/licenses/>.

  Copyright (c) 2013, 2014 IRISA
*/

#include "SparseMatrix.h"

namespace roots {

SparseMatrix::SparseMatrix() {
  mat = boost::numeric::ublas::mapped_matrix<int>();
}

SparseMatrix::SparseMatrix(int n_row, int n_col) {
  mat = boost::numeric::ublas::mapped_matrix<int>(n_row, n_col);
}

SparseMatrix::SparseMatrix(const Matrix& matrix) {
  const SparseMatrix* sp = dynamic_cast<const SparseMatrix*>(&matrix);
  if (sp != NULL) {
    mat = boost::numeric::ublas::mapped_matrix<int>(sp->mat);
  } else {
    mat = boost::numeric::ublas::mapped_matrix<int>(matrix.get_n_row(),
                                                    matrix.get_n_col());

    for (int i = 0; i < this->get_n_row(); i++) {
      for (int j = 0; j < this->get_n_col(); j++) {
        int val = matrix.get_element(i, j);
        if (val != 0) {
          this->set_element(i, j, val);
        }
      }
    }
  }
}

SparseMatrix::SparseMatrix(const SparseMatrix& sparseMatrix) {
  mat = boost::numeric::ublas::mapped_matrix<int>(sparseMatrix.mat);
}

SparseMatrix::~SparseMatrix() {
  // TODO Auto-generated destructor stub
}

SparseMatrix* SparseMatrix::clone() const { return new SparseMatrix(*this); }

int SparseMatrix::get_n_col() const { return mat.size2(); }

int SparseMatrix::get_n_row() const { return mat.size1(); }

void SparseMatrix::set_n_col(int ncol) { resize(get_n_row(), ncol); }

void SparseMatrix::set_n_row(int nrow) { resize(nrow, get_n_col()); }

void SparseMatrix::resize(int nrow, int ncol) {
  // Note(JC): Resize is not implemented for sparse matrix
  //  mat.resize(nrow, ncol, true);
  boost::numeric::ublas::mapped_matrix<int> m(nrow, ncol);

  boost::numeric::ublas::mapped_matrix<int>::const_iterator1 iter1;
  boost::numeric::ublas::mapped_matrix<int>::const_iterator2 iter2;
  for (iter1 = mat.begin1(); iter1 != mat.end1(); ++iter1) {
    // Use iterators to access only non-null elements
    for (iter2 = iter1.begin(); iter2 != iter1.end(); ++iter2) {
      size_t newRow = iter2.index1();
      size_t newCol = iter2.index2();
      if ((newRow < static_cast<size_t>(nrow)) &&
          (newCol < static_cast<size_t>(ncol))) {
        m(newRow, newCol) = *iter2;
      }
    }
  }

  mat.swap(m);
}

void SparseMatrix::shift(int row, int col) {
  size_t nRow = mat.size1();
  size_t nCol = mat.size2();

  boost::numeric::ublas::mapped_matrix<int> m(nRow, nCol);

  boost::numeric::ublas::mapped_matrix<int>::const_iterator1 iter1;
  boost::numeric::ublas::mapped_matrix<int>::const_iterator2 iter2;
  for (iter1 = mat.begin1(); iter1 != mat.end1(); ++iter1) {
    // Use iterators to access only non-null elements
    for (iter2 = iter1.begin(); iter2 != iter1.end(); ++iter2) {
      size_t newRow = iter2.index1() + row;
      size_t newCol = iter2.index2() + col;
      if ((newRow < nRow) && (newCol < nCol)) {
        m(newRow, newCol) = *iter2;
      }
    }
  }

  mat.swap(m);
}

void SparseMatrix::set_element(int row, int col, int elem) {
  if (col >= this->get_n_col()) {
    std::stringstream ss;
    ss << "SparseMatrix::set_element : out of range col = " << col
       << " range is [0," << this->get_n_col() - 1 << "]" << std::endl;
    throw RootsException(__FILE__, __LINE__, ss.str().c_str());
  }

  if (row >= this->get_n_row()) {
    std::stringstream ss;
    ss << "SparseMatrix::set_element : out of range row = " << row
       << " range is [0," << this->get_n_row() - 1 << "]" << std::endl;
    throw RootsException(__FILE__, __LINE__, ss.str().c_str());
  }

  mat(row, col) = elem;
}

int SparseMatrix::get_element(int row, int col) const { return mat(row, col); }

std::vector<int>* SparseMatrix::get_column_index(int rowIndex) {
  std::vector<int>* vec = new std::vector<int>();

  if (rowIndex >= 0 && rowIndex < this->get_n_row()) {
    boost::numeric::ublas::matrix_row<
        boost::numeric::ublas::mapped_matrix<int> >
        aRow(mat, rowIndex);
    boost::numeric::ublas::matrix_row<
        boost::numeric::ublas::mapped_matrix<int> >::const_iterator it;
    for (it = aRow.begin(); it != aRow.end(); ++it) {
      if (*it != 0) vec->push_back(it.index());
    }
  }

  return vec;
}

std::vector<int>* SparseMatrix::get_row_index(int columnIndex) {
  std::vector<int>* vec = new std::vector<int>();

  if (columnIndex >= 0 && columnIndex < this->get_n_col()) {
    boost::numeric::ublas::matrix_column<
        boost::numeric::ublas::mapped_matrix<int> >
        aColumn(mat, columnIndex);
    boost::numeric::ublas::matrix_column<
        boost::numeric::ublas::mapped_matrix<int> >::const_iterator it;
    for (it = aColumn.begin(); it != aColumn.end(); ++it) {
      if (*it != 0) vec->push_back(it.index());
    }
  }

  return vec;
}

int SparseMatrix::get_nb_element() {
  // Note : Do not use the method nnz() (returns the number of STRUCTURAL -
  // allocated values - non zeroes, not the actual number of zeroes).

  boost::numeric::ublas::mapped_matrix<int>::const_iterator1 iter1 =
      mat.begin1();
  boost::numeric::ublas::mapped_matrix<int>::const_iterator2 iter2;
  int count = 0;

  while (iter1 != mat.end1()) {
    iter2 = iter1.begin();
    while (iter2 != iter1.end()) {
      if (*iter2 != 0) count++;
      iter2++;
    }
    iter1++;
  }

  return count;
}

void SparseMatrix::remove_row(int row) {
  unsigned int nRow = 0, nCol = 0;

  nRow = mat.size1();
  nCol = mat.size2();

  if (row < 0) {
    throw RootsException(__FILE__, __LINE__,
                         "Index of the row to remove is negative!");
  }

  if ((unsigned int)row >= nRow) {
    throw RootsException(__FILE__, __LINE__,
                         "Index for the row to remove is bigger than the "
                         "number of rows in the matrix !");
  }

  boost::numeric::ublas::mapped_matrix<int> m(nRow - 1, nCol);

  boost::numeric::ublas::mapped_matrix<int>::const_iterator1 iter1;
  boost::numeric::ublas::mapped_matrix<int>::const_iterator2 iter2;
  for (iter1 = mat.begin1(); iter1 != mat.end1(); ++iter1) {
    // Use iterators to access only non-null elements
    for (iter2 = iter1.begin(); iter2 != iter1.end(); ++iter2) {
      if (iter2.index1() < (unsigned int)row) {
        m(iter2.index1(), iter2.index2()) = *iter2;
      } else if (iter2.index1() > (unsigned int)row) {
        m(iter2.index1() - 1, iter2.index2()) = *iter2;
      }
    }
  }

  mat.swap(m);
}

void SparseMatrix::remove_column(int column) {
  unsigned int nRow = 0, nCol = 0;

  nRow = mat.size1();
  nCol = mat.size2();

  if (column < 0) {
    throw RootsException(__FILE__, __LINE__,
                         "Index of the column to remove is negative!");
  }

  if ((unsigned int)column >= nCol) {
    throw RootsException(__FILE__, __LINE__,
                         "Index for the column to remove is bigger than the "
                         "number of columns in the matrix !");
  }

  boost::numeric::ublas::mapped_matrix<int> m(nRow, nCol - 1);

  boost::numeric::ublas::mapped_matrix<int>::const_iterator1 iter1;
  boost::numeric::ublas::mapped_matrix<int>::const_iterator2 iter2;
  for (iter1 = mat.begin1(); iter1 != mat.end1(); ++iter1) {
    // Use iterators to access only non-null elements
    for (iter2 = iter1.begin(); iter2 != iter1.end(); ++iter2) {
      if (iter2.index2() < (unsigned int)column) {
        m(iter2.index1(), iter2.index2()) = *iter2;
      } else if (iter2.index2() > (unsigned int)column) {
        m(iter2.index1(), iter2.index2() - 1) = *iter2;
      }
    }
  }
  mat.swap(m);
}

void SparseMatrix::insert_row(int row) {
  unsigned int nRow = 0, nCol = 0;

  nRow = mat.size1();
  nCol = mat.size2();

  if (row < 0) {
    throw RootsException(__FILE__, __LINE__,
                         "Index of the row to remove is negative!");
  }

  boost::numeric::ublas::mapped_matrix<int> m(nRow + 1, nCol);

  boost::numeric::ublas::mapped_matrix<int>::const_iterator1 iter1;
  boost::numeric::ublas::mapped_matrix<int>::const_iterator2 iter2;
  for (iter1 = mat.begin1(); iter1 != mat.end1(); ++iter1) {
    // Use iterators to access only non-null elements
    for (iter2 = iter1.begin(); iter2 != iter1.end(); ++iter2) {
      if (iter2.index1() < (unsigned int)row) {
        m(iter2.index1(), iter2.index2()) = *iter2;
      } else if (iter2.index1() >= (unsigned int)row) {
        m(iter2.index1() + 1, iter2.index2()) = *iter2;
      }
    }
  }
  mat.swap(m);
}

void SparseMatrix::insert_column(int column) {
  unsigned int nRow = 0, nCol = 0;

  nRow = mat.size1();
  nCol = mat.size2();

  if (column < 0) {
    throw RootsException(__FILE__, __LINE__,
                         "Index of the column to remove is negative!");
  }

  boost::numeric::ublas::mapped_matrix<int> m(nRow, nCol + 1);

  boost::numeric::ublas::mapped_matrix<int>::const_iterator1 iter1;
  boost::numeric::ublas::mapped_matrix<int>::const_iterator2 iter2;
  for (iter1 = mat.begin1(); iter1 != mat.end1(); ++iter1) {
    // Use iterators to access only non-null elements
    for (iter2 = iter1.begin(); iter2 != iter1.end(); ++iter2) {
      if (iter2.index2() < (unsigned int)column) {
        m(iter2.index1(), iter2.index2()) = *iter2;
      } else if (iter2.index2() >= (unsigned int)column) {
        m(iter2.index1(), iter2.index2() + 1) = *iter2;
      }
    }
  }

  mat.swap(m);
}

Matrix* SparseMatrix::transpose() {
  SparseMatrix* transp = new SparseMatrix(this->get_n_col(), this->get_n_row());

  boost::numeric::ublas::mapped_matrix<int>::const_iterator1 iter1;
  boost::numeric::ublas::mapped_matrix<int>::const_iterator2 iter2;
  for (iter1 = mat.begin1(); iter1 != mat.end1(); ++iter1) {
    // Use iterators to access only non-null elements
    for (iter2 = iter1.begin(); iter2 != iter1.end(); ++iter2) {
      transp->set_element(iter2.index2(), iter2.index1(), *iter2);
    }
  }
  return transp;
}

void SparseMatrix::sum(const Matrix& matrix) {
  const SparseMatrix* sp = dynamic_cast<const SparseMatrix*>(&matrix);
  // TODO : check dimensions
  if (sp != NULL) {
    if (get_n_row() > 0 && get_n_col() > 0 && matrix.get_n_col() > 0 &&
        matrix.get_n_row() > 0) {
      //  mat += sp->mat; // Note(JC): not working but I don't know why ...
      boost::numeric::ublas::mapped_matrix<int>::const_iterator1 iter1;
      boost::numeric::ublas::mapped_matrix<int>::const_iterator2 iter2;
      for (iter1 = sp->mat.begin1(); iter1 != sp->mat.end1(); ++iter1) {
        // Use iterators to access only non-null elements
        for (iter2 = iter1.begin(); iter2 != iter1.end(); ++iter2) {
          mat(iter2.index1(), iter2.index2()) += *iter2;
        }
      }
    }
  } else {
    for (int i = 0; i < get_n_row(); i++) {
      for (int j = 0; j < get_n_col(); j++) {
        mat(i, j) += matrix.get_element(i, j);
      }
    }
  }
}

Matrix* SparseMatrix::multiply_right(const Matrix& matrix) {
  SparseMatrix* result = NULL;

  const SparseMatrix* sp = dynamic_cast<const SparseMatrix*>(&matrix);

  if (sp != NULL) {
    result = new SparseMatrix(this->get_n_row(), matrix.get_n_col());
    if (this->get_n_row() > 0 && this->get_n_col() > 0 &&
        matrix.get_n_col() > 0 && matrix.get_n_row() > 0) {
      boost::numeric::ublas::sparse_prod(mat, sp->mat, result->mat);
    }
  } else {
    int i = 0, j = 0, k = 0;

    if (this->get_n_col() != matrix.get_n_row()) {
      std::stringstream ss;
      ss << "Error left matrix column number <" << this->get_n_col()
         << "> should equals right matrix column number <" << matrix.get_n_row()
         << ">!\n";
      throw RootsException(__FILE__, __LINE__, ss.str().c_str());
    }

    result = new SparseMatrix(this->get_n_row(), matrix.get_n_col());

    for (i = 0; i < result->get_n_row(); i++) {
      for (j = 0; j < result->get_n_col(); j++) {
        int sumproduct = 0;

        for (k = 0; k < this->get_n_col(); k++) {
          int valueLeft = this->get_element(i, k);
          if (valueLeft > 1) {
            valueLeft = 1;
          }

          int valueRight = matrix.get_element(k, j);
          if (valueRight > 1) {
            valueRight = 1;
          }
          sumproduct += valueLeft * valueRight;
        }

        result->set_element(i, j, sumproduct);
      }
    }
  }
  return result;
}

void SparseMatrix::deflate(RootsStream* stream, bool is_terminal,
                           int list_index) {
  Matrix::deflate(stream, false, list_index);

  stream->append_matrix_int_content("matrix", mat);

  if (is_terminal) stream->close_object();
}

void SparseMatrix::inflate(RootsStream* stream, bool is_terminal,
                           int list_index) {
  Matrix::inflate(stream, false, list_index);

  mat = stream->get_matrix_int_content("matrix");

  if (is_terminal) stream->close_children();
}

SparseMatrix* SparseMatrix::inflate_object(RootsStream* stream,
                                           int list_index) {
  SparseMatrix* t = new SparseMatrix();
  t->inflate(stream, true, list_index);
  return t;
}

std::ostream& operator<<(std::ostream& out, SparseMatrix& mat) {
  /*out<< "Matrix:" <<  std::endl;
    out<< "\t nb_elements: "<< mat.get_nb_element() <<  std::endl;*/

  for (int i = 0; i < mat.get_n_row(); i++) {
    for (int j = 0; j < mat.get_n_col(); j++) {
      std::cout << mat.get_element(i, j) << " ";
    }
    out << std::endl;
  }

  return out;
}
}
