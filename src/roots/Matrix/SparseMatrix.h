/*
    This file is part of Roots.

    Roots is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Roots is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Roots.  If not, see <http://www.gnu.org/licenses/>.

    Copyright (c) 2013, 2014 IRISA
*/

#ifndef SPARSEMATRIX_H_
#define SPARSEMATRIX_H_

#include <boost/numeric/ublas/io.hpp>
#include <boost/numeric/ublas/matrix_proxy.hpp>
#include <boost/numeric/ublas/matrix_sparse.hpp>
#include <boost/numeric/ublas/operation_sparse.hpp>
#include "../Matrix.h"
#include "../common.h"

namespace roots {

/**
 * @brief Sparse matrix representation
 * @details
 * This class provides an implementation of the Matrix class using a sparse
 * representation.
 *
 * @author Cordial Group
 * @version 0.1
 * @date 2011
 */
class SparseMatrix : public Matrix {
 protected:
  SparseMatrix();

 public:
  SparseMatrix(int n_row, int n_col);
  SparseMatrix(const Matrix&);
  SparseMatrix(const SparseMatrix&);
  virtual ~SparseMatrix();
  /**
   * @brief clone the current object
   * @return a copy of the object
   */
  virtual SparseMatrix* clone() const;

  virtual int get_n_col() const;
  virtual int get_n_row() const;
  virtual void set_n_col(int ncol);
  virtual void set_n_row(int nrow);

  virtual void set_element(int row, int col, int elem);
  virtual int get_element(int row, int col) const;

  virtual void remove_row(int row);
  virtual void remove_column(int column);

  virtual void insert_row(int row);
  virtual void insert_column(int column);
  virtual void resize(int nrow, int ncol);

  virtual void shift(int row, int col);

  /**
   * @brief sum a matrix with the current matrix
   * @param matrix the matrix to sum with
   */
  virtual void sum(const Matrix& matrix);

  virtual std::vector<int>* get_column_index(int row);
  virtual std::vector<int>* get_row_index(int col);
  virtual int get_nb_element();
  /**
   * @brief Returns the transposed matrix of the current one
   * @return Matrix*
   */
  virtual Matrix* transpose();
  /**
   * @brief Returns the matrix multiplication result of (this x matrix)
   * @param matrix the matrix to multiply with
   */
  virtual Matrix* multiply_right(const roots::Matrix& matrix);
  /**
   * @brief Write the entity into a RootsStream
   * @param stream the stream from which we inflate the element
   * @param is_terminal indicates that the node is terminal (true by default)
   * @param parentNode
   */
  virtual void deflate(RootsStream* stream, bool is_terminal = true,
                       int list_index = -1);
  /**
   * @brief Extracts the entity from the stream
   * @param stream the stream from which we inflate the element
   */
  virtual void inflate(RootsStream* stream, bool is_terminal = true,
                       int list_index = -1);
  /**
   * @brief Extracts the entity from the stream
   * @param stream the stream from which we inflate the element
   */
  static SparseMatrix* inflate_object(RootsStream* stream, int list_index = -1);
  /**
   * @brief returns the XML tag name value for current object
   * @return string constant representing the XML tag name
   */
  virtual std::string get_xml_tag_name() const { return xml_tag_name(); };
  /**
   * @brief returns the XML tag name value for current class
   * @return string constant representing the XML tag name
   */
  static std::string xml_tag_name() { return "sparse_matrix"; };
  /**
   * @brief returns classname for current object
   * @return string constant representing the classname
   */
  virtual std::string get_classname() const { return classname(); };
  /**
   * @brief returns classname for current class
   * @return string constant representing the classname
   */
  static std::string classname() { return "Matrix::Sparse"; };

  /**
   * Serialize the object into a stream as a string
   *
   * @param out
   * @param mat
   * @return the modified stream
   */
  friend std::ostream& operator<<(std::ostream& out, SparseMatrix& mat);

 private:
  boost::numeric::ublas::mapped_matrix<int> mat;
};
}

#endif /* SPARSEMATRIX_H_ */
