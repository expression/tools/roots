/*
    This file is part of Roots.

    Roots is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Roots is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Roots.  If not, see <http://www.gnu.org/licenses/>.

    Copyright (c) 2013, 2014 IRISA
*/


#include "Number.h"

namespace roots
{
  Number::Number(const bool noInit) : BaseItem(noInit)
  {
    // Nothing to do
  }
  
  Number::Number(const double _value) : BaseItem()
  {
    set_value(_value);
  }
  
  Number::Number(const std::string & str_value) : BaseItem()
  {
    set_value(atof(str_value.c_str()));
  }
  
  Number::Number(const Number & to_be_copied) : BaseItem(to_be_copied)
  {
    set_value(to_be_copied.get_value());
  }
  
  Number::~Number() {
    // Nothing to do
  }
  
  Number* Number::clone() const
  {
    return new Number(*this);
  }
  
  Number& Number::operator=(const Number & to_be_copied) {
    BaseItem::operator=(to_be_copied);
    return *this;
  }

  double Number::get_value() const
  {
    return value;
  }
  
  void Number::set_value(const double _value)
  {
    value = _value;
  }
  
  // IO methods
  std::string Number::to_string(int /*level*/) const
  {
    std::stringstream ss;
    ss << get_value();
    return ss.str();
  }
  
  void Number::deflate(RootsStream * stream, bool is_terminal, int list_index) {
    BaseItem::deflate(stream,false,list_index);
    
    stream->append_double_content("value", this->get_value());
    
    if(is_terminal)
      stream->close_object();
  }
  
  void Number::inflate(RootsStream * stream, bool is_terminal, int list_index)
  {
    BaseItem::inflate(stream,false,list_index);
    
    this->set_value(stream->get_double_content("value"));
    
    if(is_terminal) stream->close_children();
  }
  
  Number * Number::inflate_object(RootsStream * stream, int list_index)
  {
    Number *n = new Number(true);
    n->inflate(stream,true,list_index);
    return n; 
  }
}
