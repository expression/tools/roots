/*
    This file is part of Roots.

    Roots is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Roots is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Roots.  If not, see <http://www.gnu.org/licenses/>.

    Copyright (c) 2013, 2014 IRISA
*/


#include "SequenceLinkedObject.h"
#include "Sequence.h"

namespace roots
{
  
  SequenceLinkedObject::SequenceLinkedObject(){
    sequenceRank = -1;
    sequenceLink = BaseLink<sequence::Sequence >();
  };
  
  SequenceLinkedObject::SequenceLinkedObject(const SequenceLinkedObject& seq_link){
    sequenceRank = seq_link.sequenceRank;
    sequenceLink = seq_link.sequenceLink;
  };
  
  SequenceLinkedObject::~SequenceLinkedObject(){};
  
  SequenceLinkedObject& SequenceLinkedObject::operator=(const SequenceLinkedObject& seq_link){
    sequenceRank = seq_link.sequenceRank;
    sequenceLink = seq_link.sequenceLink;
    return (*this);
  };
 
  int SequenceLinkedObject::get_in_sequence_index() const
  {
    return sequenceRank;
  }
  
  void SequenceLinkedObject::set_in_sequence_index(int rank)
  {
    this->sequenceRank = rank;
  }
  
  void SequenceLinkedObject::remove_in_sequence_index()
  {
    sequenceRank = -1;
  }
  
  bool SequenceLinkedObject::is_in_sequence() const
  {
    return (sequenceLink.get_link() != NULL);
  }
  
  sequence::Sequence* SequenceLinkedObject::get_in_sequence() const
  {
    return (sequenceLink.get_link());
  }
  
  void SequenceLinkedObject::set_in_sequence(sequence::Sequence* _sequenceLink, unsigned int rank)
  {
    sequenceLink.set_link(_sequenceLink);
    set_in_sequence_index(rank);
  }
  
  void SequenceLinkedObject::remove_from_sequence()
  {
    sequenceLink.set_link(NULL);
    remove_in_sequence_index();
  }
  
  bool SequenceLinkedObject::is_first_in_sequence()
  {
    return sequenceRank == 0;
  }

  bool SequenceLinkedObject::is_last_in_sequence()
  {
    return sequenceRank+1 == (sequenceLink.get_link()->count());
  }

}
