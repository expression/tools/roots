/*
    This file is part of Roots.

    Roots is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Roots is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Roots.  If not, see <http://www.gnu.org/licenses/>.

    Copyright (c) 2013, 2014 IRISA
*/

//
//  UtteranceHelper.h
//
//
//  Created by Damien Lolive on 09/06/2015.
//
//

#ifndef _UtteranceHelper_h
#define _UtteranceHelper_h

#include "Phonology/Ipa.h"
#include "Phonology/Ipa/Alphabet.h"
#include "Phonology/Nsa/NsAlphabet.h"
#include "Phonology/Nsa/NsaCommon.h"
#include "Sequence/AllophoneSequence.h"
#include "Sequence/NamedEntitySequence.h"
#include "Sequence/NssSequence.h"
#include "Sequence/PosSequence.h"
#include "Sequence/SegmentSequence.h"
#include "Sequence/SyllableSequence.h"
#include "Sequence/SyntaxSequence.h"
#include "Sequence/WordSequence.h"
#include "Utterance.h"

#include "File/HTKLab.h"
#include "File/TextGrid.h"

#include "common.h"
#include "utils.h"

namespace roots {
const int MASK_SEQ_NONE = 0x0000;
const int MASK_SEQ_SEGMENT = 0x0001;
const int MASK_SEQ_PHONEME = 0x0002;
const int MASK_SEQ_SYLLABLE = 0x0004;
const int MASK_SEQ_WORD = 0x0008;
const int MASK_SEQ_F0 = 0x0010;
const int MASK_SEQ_SYNTAX = 0x0020;
const int MASK_SEQ_NAMED_ENTITY = 0x0040;
const int MASK_SEQ_ALLOPHONE = 0x0080;
const int MASK_SEQ_NSS = 0x0100;
const int MASK_SEQ_POS = 0x0200;
const int MASK_SEQ_INTENSITY = 0x0400;
const int MASK_SEQ_ALL = 0xFFFF;

//  TMP
/*
const std::string LABEL_SEQ_SEGMENT = "Segment";
const std::string LABEL_SEQ_ALLOPHONE = "Allophone";
const std::string LABEL_SEQ_NSS = "NonSpeechSound";
const std::string LABEL_SEQ_SYLLABLE = "Syllable";
const std::string LABEL_SEQ_WORD = "Word Text";
const std::string LABEL_SEQ_GRAPHEME = "Grapheme Text";
const std::string LABEL_SEQ_F0 = "F0";
const std::string LABEL_SEQ_INTENSITY = "Intensity";
const std::string LABEL_SEQ_SYNAPSE_WORD = "Word Synapse";
const std::string LABEL_SEQ_SYNAPSE_LEMMA = "Lemma Synapse";
const std::string LABEL_SEQ_SYNAPSE_SYNTAX = "Syntax Synapse";
const std::string LABEL_SEQ_SYNAPSE_POS = "POS Synapse";
const std::string LABEL_SEQ_SYNAPSE_NAMED_ENTITY = "Named Entity Synapse";
const std::string LABEL_SEQ_LIAPHON_WORD = "Word Liaphon";
const std::string LABEL_SEQ_LIAPHON_PHONEME = "Phoneme Liaphon";
*/

class Labels {
 public:
  Labels() {
    // Acco
    SEQ_SEGMENT = "Segment";
    SEQ_ALLOPHONE = "Allophone";
    SEQ_NSS = "NonSpeechSound";
    SEQ_F0 = "F0";
    SEQ_MFCC = "MFCC";
    SEQ_PMK = "Pitchmark";
    SEQ_SIGNAL = "Signal";

    // Phono
    SEQ_PHONEME = "Phoneme";
    SEQ_SYLLABLE = "Syllable";

    // Raw text
    SEQ_WORD = "Word Text";
    SEQ_GRAPHEME = "Grapheme Text";

    // Linguistic
    SEQ_LING_WORD = "Word Linguistic";
    SEQ_LEMMA = "Lemma Synapse";
    SEQ_SYNTAX = "Syntax Synapse";
    SEQ_POS = "POS Synapse";
    SEQ_NAMED_ENTITY = "Named Entity Synapse";
  };

  std::string SEQ_SEGMENT;
  std::string SEQ_ALLOPHONE;
  std::string SEQ_NSS;
  std::string SEQ_F0;
  std::string SEQ_MFCC;
  std::string SEQ_PMK;
  std::string SEQ_SIGNAL;

  std::string SEQ_PHONEME;
  std::string SEQ_SYLLABLE;

  std::string SEQ_WORD;
  std::string SEQ_GRAPHEME;

  std::string SEQ_LING_WORD;
  std::string SEQ_LEMMA;
  std::string SEQ_SYNTAX;
  std::string SEQ_POS;
  std::string SEQ_NAMED_ENTITY;
};

template <class NA>
void get_all_nss(
    const std::string &label, bool &is_nss, unsigned int &n_nss,
    sequence::NssSequence *nssSequence,
    sequence::SegmentSequence *segmentSequence,
    std::vector<std::pair<unsigned int, unsigned int> > &segNssRelationElts) {
  try {
    NA &nsAlph = NA::get_instance();
    roots::phonology::nsa::NsaCommon nsa(nsAlph, label);
    roots::acoustic::NonSpeechSound nss(nsa);
    nssSequence->add(&nss);
    is_nss = true;
    segNssRelationElts.push_back(std::pair<int, int>(
        segmentSequence->count() - 1, nssSequence->count() - 1));
    n_nss++;
  } catch (roots::phonology::nsa::NsaException e) {
    // Do nothing
  }
};

template <>
void get_all_nss<void>(
    const std::string &label, bool &is_nss, unsigned int &n_nss,
    sequence::NssSequence *nssSequence,
    sequence::SegmentSequence *segmentSequence,
    std::vector<std::pair<unsigned int, unsigned int> > &segNssRelationElts);


template <class A>
void get_all_allophone(
    std::string &label, bool &is_allophone, unsigned int &n_allophone,
    sequence::AllophoneSequence *allophoneSequence,
    sequence::SegmentSequence *segmentSequence,
    std::vector<std::pair<unsigned int, unsigned int> > &segAllRelationElts) {
  try {
    A &alph = A::get_instance();
    std::vector<roots::phonology::ipa::Ipa *> vec_ipas = alph.extract_ipas(label);

    for (std::vector<roots::phonology::ipa::Ipa *>::iterator it_ipas =
             vec_ipas.begin();
         it_ipas != vec_ipas.end(); ++it_ipas) {
      roots::acoustic::Allophone all(*(*it_ipas), &alph);
      allophoneSequence->add(&all);
      is_allophone = true;
      segAllRelationElts.push_back(std::pair<int, int>(
          segmentSequence->count() - 1, allophoneSequence->count() - 1));
      n_allophone++;
    }
  } catch (roots::phonology::ipa::IpaException e) {
    // std::cerr<< "exception occured 1" << endl;
  }
};


template <>
void get_all_allophone<void>(
    std::string &label, bool &is_allophone, unsigned int &n_allophone,
    sequence::AllophoneSequence *allophoneSequence,
    sequence::SegmentSequence *segmentSequence,
    std::vector<std::pair<unsigned int, unsigned int> > &segAllRelationElts);

template <class A, class NA>
void import_htklab(Utterance &utt, const std::string &filename,
                   const Labels &labels, int seqTypes = MASK_SEQ_ALL,
                   std::locale _locale = std::locale("")) {
  std::stringstream ss;

  // UErrorCode status = U_ZERO_ERROR;
  bool is_allophone = false, is_nss = false;

  sequence::SegmentSequence *segmentSequence = NULL;
  sequence::AllophoneSequence *allophoneSequence = NULL;
  sequence::NssSequence *nssSequence = NULL;
  sequence::WordSequence *wordSequence = NULL;

  Relation *segmentAllophoneRelation = NULL;
  Relation *segmentNssRelation = NULL;
  Relation *segmentWordRelation = NULL;
  Relation *allophoneWordRelation = NULL;
  Relation *nssWordRelation = NULL;

  unsigned int n_seg = 0, n_allophone = 0, n_nss = 0, n_word = 0;
  std::vector<std::pair<unsigned int, unsigned int> > segAllRelationElts,
      segNssRelationElts, segWordRelationElts, allWordRelationElts,
      nssWordRelationElts;

  roots::file::htk::HTKLab file(filename, _locale);
  std::vector<roots::file::htk::HTKLabLine> *lines = file.load();
  
  icu::UnicodeString currentWord = "";

  if ((seqTypes & MASK_SEQ_SEGMENT) != 0) {
    segmentSequence = new sequence::SegmentSequence();
    segmentSequence->set_label(labels.SEQ_SEGMENT);
  }
  if ((seqTypes & MASK_SEQ_ALLOPHONE) != 0) {
    allophoneSequence = new sequence::AllophoneSequence();
    allophoneSequence->set_label(labels.SEQ_ALLOPHONE);
  }
  if ((seqTypes & MASK_SEQ_NSS) != 0) {
    nssSequence = new sequence::NssSequence();
    nssSequence->set_label(labels.SEQ_NSS);
  }
  if ((seqTypes & MASK_SEQ_WORD) != 0) {
    wordSequence = new sequence::WordSequence();
    wordSequence->set_label(labels.SEQ_WORD);
  }
  if (((seqTypes & MASK_SEQ_SEGMENT) != 0) &&
      ((seqTypes & MASK_SEQ_ALLOPHONE) != 0)) {
    segmentAllophoneRelation = new Relation(segmentSequence, allophoneSequence);
  }
  if (((seqTypes & MASK_SEQ_SEGMENT) != 0) &&
      ((seqTypes & MASK_SEQ_NSS) != 0)) {
    segmentNssRelation = new Relation(segmentSequence, nssSequence);
  }
  if (((seqTypes & MASK_SEQ_SEGMENT) != 0) &&
      ((seqTypes & MASK_SEQ_WORD) != 0)) {
    segmentWordRelation = new Relation(segmentSequence, wordSequence);
  }
  if (((seqTypes & MASK_SEQ_ALLOPHONE) != 0) &&
      ((seqTypes & MASK_SEQ_WORD) != 0)) {
    allophoneWordRelation = new Relation(allophoneSequence, wordSequence);
  }
  if (((seqTypes & MASK_SEQ_NSS) != 0) && ((seqTypes & MASK_SEQ_WORD) != 0)) {
    nssWordRelation = new Relation(nssSequence, wordSequence);
  }

  for (std::vector<roots::file::htk::HTKLabLine>::const_iterator
           iter = lines->begin();
           iter != lines->end();
       ++iter) {

    is_allophone = false;
    is_nss = false;
    std::string labelStr = unistr2stdstr(iter->label);

    if ((seqTypes & MASK_SEQ_SEGMENT) != 0) {
      // Add a new segment to sequence
      roots::acoustic::TimeSegment tmpTimeSeg(
          iter->timeStart, iter->timeEnd,
          roots::acoustic::Segment::TIME_UNIT_FACTOR_HTK);
      segmentSequence->add(&tmpTimeSeg);  // item is cloned
    }

    if ((seqTypes & MASK_SEQ_ALLOPHONE) != 0) {
      // Try to get a Allophones
      get_all_allophone<A>(labelStr, is_allophone, n_allophone,
                           allophoneSequence, segmentSequence,
                           segAllRelationElts);
    }

    //
    if (((seqTypes & MASK_SEQ_NSS) != 0) && !is_allophone) {
      // Try to get a NonSpeechSound
      get_all_nss<NA>(labelStr, is_nss, n_nss, nssSequence, segmentSequence,
                     segNssRelationElts);
   }

    /*
        if (!is_allophone && !is_nss) {
          if ((seqTypes & MASK_SEQ_ALLOPHONE) != 0) {
            // Not a french symbol, try with unicode
           get_all_allophone<A>(iter->label, is_allophone, n_allophone,
                               allophoneSequence, segAllRelationElts, true);
            try {
              roots::acoustic::Allophone all(
                  (roots::phonology::ipa::Ipa::from_unicode(
                      (*iter).label)));  // * enlevée
              allophoneSequence->add(&all);
              segAllRelationElts.push_back(std::pair<int, int>(
                  segmentSequence->count() - 1, allophoneSequence->count() -
       1));
              n_allophone++;
              is_allophone = true;
            } catch (roots::phonology::ipa::IpaException e) {
              // Not unicode, do nothing
            }
          }
        }
        */

    if ((!is_allophone) && (!is_nss)) {
      std::stringstream ss;
      ss << unistr2stdstr((*iter).label)
         << " is not a known symbol or is ignored !";
      ROOTS_LOGGER_WARN(ss.str().c_str());
    }

    if ((seqTypes & MASK_SEQ_WORD) != 0) {
      // traiter les mots lorsqu'ils se présentent dans label2
      if ((*iter).label2 != "") {
        if ((*iter).label2 != currentWord) {
          currentWord = (*iter).label2;
          wordSequence->add(
              new roots::linguistic::Word(unistr2stdstr(currentWord)));
          n_word++;
        }

        if (((seqTypes & MASK_SEQ_SEGMENT) != 0)) {
          segWordRelationElts.push_back(std::pair<int, int>(
              segmentSequence->count() - 1, wordSequence->count() - 1));
        }
        if (((seqTypes & MASK_SEQ_ALLOPHONE) != 0) && is_allophone) {
          allWordRelationElts.push_back(std::pair<int, int>(
              allophoneSequence->count() - 1, wordSequence->count() - 1));
        }
        if (((seqTypes & MASK_SEQ_NSS) != 0) && is_nss) {
          nssWordRelationElts.push_back(std::pair<int, int>(
              nssSequence->count() - 1, wordSequence->count() - 1));
        }
      }
    }

    n_seg++;
  }

  // Build the relations mappings
  if (((seqTypes & MASK_SEQ_SEGMENT) != 0) &&
      ((seqTypes & MASK_SEQ_ALLOPHONE) != 0)) {
    segmentAllophoneRelation->set_mapping(new SparseMatrix(n_seg, n_allophone));
    for (unsigned int i = 0; i < segAllRelationElts.size(); i++) {
      segmentAllophoneRelation->get_mapping()->set_element(
          segAllRelationElts[i].first, segAllRelationElts[i].second, 1);
    }
  }

  if (((seqTypes & MASK_SEQ_SEGMENT) != 0) &&
      ((seqTypes & MASK_SEQ_NSS) != 0)) {
    segmentNssRelation->set_mapping(new SparseMatrix(n_seg, n_nss));

    for (unsigned int i = 0; i < segNssRelationElts.size(); i++) {
      segmentNssRelation->get_mapping()->set_element(
          segNssRelationElts[i].first, segNssRelationElts[i].second, 1);
    }
  }

  if (((seqTypes & MASK_SEQ_SEGMENT) != 0) &&
      ((seqTypes & MASK_SEQ_WORD) != 0)) {
    segmentWordRelation->set_mapping(new SparseMatrix(n_seg, n_word));

    for (unsigned int i = 0; i < segWordRelationElts.size(); i++) {
      segmentWordRelation->get_mapping()->set_element(
          segWordRelationElts[i].first, segWordRelationElts[i].second, 1);
    }
  }

  if (((seqTypes & MASK_SEQ_ALLOPHONE) != 0) &&
      ((seqTypes & MASK_SEQ_WORD) != 0)) {
    //		std::cerr << "size = (" << n_allophone << ", " << n_word << ")"
    //<<
    // endl;
    allophoneWordRelation->set_mapping(new SparseMatrix(n_allophone, n_word));

    for (unsigned int i = 0; i < allWordRelationElts.size(); i++) {
      //			std::cerr << "elem = (" <<
      // allWordRelationElts[i].first << ", " << allWordRelationElts[i].second
      // <<
      //")" << endl;
      allophoneWordRelation->get_mapping()->set_element(
          allWordRelationElts[i].first, allWordRelationElts[i].second, 1);
    }
  }

  if (((seqTypes & MASK_SEQ_NSS) != 0) && ((seqTypes & MASK_SEQ_WORD) != 0)) {
    // std::cerr << "size = (" << n_nss << ", " << n_word << ")" << endl;
    nssWordRelation->set_mapping(new SparseMatrix(n_nss, n_word));

    for (unsigned int i = 0; i < nssWordRelationElts.size(); i++) {
      // std::cerr << "elem = (" << nssWordRelationElts[i].first << ", " <<
      // nssWordRelationElts[i].second << ")" << endl;
      nssWordRelation->get_mapping()->set_element(
          nssWordRelationElts[i].first, nssWordRelationElts[i].second, 1);
    }
  }

  // Add the sequences and the relation to the Utterance
  if ((seqTypes & MASK_SEQ_SEGMENT) != 0) {
    utt.add_sequence(segmentSequence);
  }
  if ((seqTypes & MASK_SEQ_ALLOPHONE) != 0) {
    utt.add_sequence(allophoneSequence);
  }
  if ((seqTypes & MASK_SEQ_NSS) != 0) {
    utt.add_sequence(nssSequence);
  }
  if ((seqTypes & MASK_SEQ_WORD) != 0) {
    utt.add_sequence(wordSequence);
  }
  if (((seqTypes & MASK_SEQ_SEGMENT) != 0) &&
      ((seqTypes & MASK_SEQ_ALLOPHONE) != 0)) {
    utt.add_relation(segmentAllophoneRelation);
  }
  if (((seqTypes & MASK_SEQ_SEGMENT) != 0) &&
      ((seqTypes & MASK_SEQ_NSS) != 0)) {
    utt.add_relation(segmentNssRelation);
  }
  if (((seqTypes & MASK_SEQ_SEGMENT) != 0) &&
      ((seqTypes & MASK_SEQ_WORD) != 0)) {
    utt.add_relation(segmentWordRelation);
  }
  if (((seqTypes & MASK_SEQ_ALLOPHONE) != 0) &&
      ((seqTypes & MASK_SEQ_WORD) != 0)) {
    utt.add_relation(allophoneWordRelation);
  }
  if (((seqTypes & MASK_SEQ_NSS) != 0) && ((seqTypes & MASK_SEQ_WORD) != 0)) {
    utt.add_relation(nssWordRelation);
  }
}

template <class A, class I, class NA, class N>
void import_textgrid(Utterance &utt, const std::string &filename,
                     const Labels &labels, int seqTypes = MASK_SEQ_ALL,
                     const std::string &phonemeSeqLabel = "",
                     std::locale _locale = std::locale("")) {
  std::string segtiername = "phones";
  std::string wordtiername = "words";
  std::string syltiername = "syll";
  std::string deltiername = "delivery";
  int segtierindex = -1, wordtierindex = -1, syltierindex = -1,
      deltierindex = -1;

  bool is_allophone = false, is_nss = false;

  sequence::SegmentSequence *segmentSequence = NULL;
  sequence::AllophoneSequence *allophoneSequence = NULL;
  sequence::NssSequence *nssSequence = NULL;
  sequence::WordSequence *wordSequence = NULL;
  sequence::SyllableSequence *syllableSequence = NULL;

  Relation *segmentAllophoneRelation = NULL;
  Relation *segmentNssRelation = NULL;
  Relation *segmentWordRelation = NULL;
  // Relation *allophoneWordRelation = NULL;
  // Relation *nssWordRelation = NULL;

  unsigned int n_seg = 0, n_allophone = 0, n_nss = 0;
  std::vector<std::pair<unsigned int, unsigned int> > segAllRelationElts,
      segNssRelationElts, segWordRelationElts, allWordRelationElts,
      nssWordRelationElts;

  A &alph = A::get_instance();

  int segmentIndex = 0, firstIndex = 0, lastIndex = 0;

  // Load TextGrid file
  roots::file::praat::TextGrid textGridFile(filename);
  roots::file::praat::TextGridFile *tgridStructure = textGridFile.load();

  // items: phones, syll, delivery (prominence), words
  if ((seqTypes & MASK_SEQ_SEGMENT) != 0 ||
      (seqTypes & MASK_SEQ_ALLOPHONE) != 0 || (seqTypes & MASK_SEQ_NSS) != 0) {
    segtierindex = roots::file::praat::TextGrid::get_tier_index(tgridStructure,
                                                                segtiername);
    if (segtierindex == -1) {
      throw RootsException(__FILE__, __LINE__, "phoneme tier not found!");
    }
  }
  if ((seqTypes & MASK_SEQ_WORD) != 0) {
    wordtierindex = roots::file::praat::TextGrid::get_tier_index(tgridStructure,
                                                                 wordtiername);
    if (wordtierindex == -1) {
      throw RootsException(__FILE__, __LINE__, "word tier not found!");
    }
  }
  if ((seqTypes & MASK_SEQ_SYLLABLE) != 0) {
    syltierindex = roots::file::praat::TextGrid::get_tier_index(tgridStructure,
                                                                syltiername);
    if (syltierindex == -1) {
      throw RootsException(__FILE__, __LINE__, "syllable tier not found!");
    }

    deltierindex = roots::file::praat::TextGrid::get_tier_index(tgridStructure,
                                                                deltiername);
    if (deltierindex == -1) {
      ROOTS_LOGGER_WARN("delivery tier not found!");
    }
  }

  if ((seqTypes & MASK_SEQ_SEGMENT) != 0) {
    segmentSequence = new sequence::SegmentSequence();
    segmentSequence->set_label(labels.SEQ_SEGMENT);
    utt.add_sequence(segmentSequence);
  }

  if ((seqTypes & MASK_SEQ_ALLOPHONE) != 0) {
    allophoneSequence = new sequence::AllophoneSequence();
    allophoneSequence->set_label(labels.SEQ_ALLOPHONE);
    utt.add_sequence(allophoneSequence);
  }

  if ((seqTypes & MASK_SEQ_NSS) != 0) {
    nssSequence = new sequence::NssSequence();
    nssSequence->set_label(labels.SEQ_NSS);
    utt.add_sequence(nssSequence);
  }

  // TODO : build the relations
  if (((seqTypes & MASK_SEQ_SEGMENT) != 0) &&
      ((seqTypes & MASK_SEQ_ALLOPHONE) != 0)) {
    segmentAllophoneRelation = new Relation(segmentSequence, allophoneSequence);
  }
  if (((seqTypes & MASK_SEQ_SEGMENT) != 0) &&
      ((seqTypes & MASK_SEQ_NSS) != 0)) {
    segmentNssRelation = new Relation(segmentSequence, nssSequence);
  }

  /*if(((seqTypes & MASK_SEQ_ALLOPHONE) != 0) && ((seqTypes & MASK_SEQ_WORD) !=
   0))
   {
   allophoneWordRelation = new Relation(allophoneSequence, wordSequence);
   }
   if(((seqTypes & MASK_SEQ_NSS) != 0) && ((seqTypes & MASK_SEQ_WORD) != 0))
   {
   nssWordRelation = new Relation(nssSequence, wordSequence);
   }*/

  if ((seqTypes & MASK_SEQ_SEGMENT) != 0 ||
      (seqTypes & MASK_SEQ_ALLOPHONE) != 0 || (seqTypes & MASK_SEQ_NSS) != 0) {
    // get the tier and build the sequences : allophone, segment, nss

    for (unsigned int elementIndex = 0;
         elementIndex < tgridStructure->items[segtierindex]->elements.size();
         ++elementIndex) {
      is_allophone = false;
      is_nss = false;

      double start =
          tgridStructure->items[segtierindex]->elements[elementIndex]->xmin;
      double end =
          tgridStructure->items[segtierindex]->elements[elementIndex]->xmax;
      icu::UnicodeString label =
          *(tgridStructure->items[segtierindex]->elements[elementIndex]->text);

      if ((seqTypes & MASK_SEQ_SEGMENT) != 0) {
        // Add a new segment to sequence
        roots::acoustic::TimeSegment timeSegment(
            start, end, roots::acoustic::Segment::TIME_UNIT_FACTOR_HTK);
        segmentSequence->add(&timeSegment);
      }

      if ((seqTypes & MASK_SEQ_ALLOPHONE) != 0) {
        // Try to get an allophone
        try {
          roots::acoustic::Allophone *all;
          std::string label_str = unistr2stdstr(label);
          std::vector<roots::phonology::ipa::Ipa *> vec_ipas =
              alph.extract_ipas(label_str);
          for (std::vector<roots::phonology::ipa::Ipa *>::iterator it_ipas =
                   vec_ipas.begin();
               it_ipas != vec_ipas.end(); ++it_ipas) {
            all = new roots::acoustic::Allophone(*(*it_ipas), &alph);
            allophoneSequence->add(all);
            is_allophone = true;
            segAllRelationElts.push_back(std::pair<int, int>(
                segmentSequence->count() - 1, allophoneSequence->count() - 1));
            n_allophone++;
          }
        } catch (roots::phonology::ipa::IpaException e) {
          // std::cerr<< "exception occured 1" << endl;
        }
      }

      //
      if (((seqTypes & MASK_SEQ_NSS) != 0) && !is_allophone) {
        // Try to get a NonSpeechSound
        try {
          roots::acoustic::NonSpeechSound nss(
              N(NA::get_instance(), unistr2stdstr(label)));
          nssSequence->add(&nss);
          is_nss = true;
          segNssRelationElts.push_back(std::pair<int, int>(
              segmentSequence->count() - 1, nssSequence->count() - 1));
          n_nss++;
        } catch (roots::phonology::nsa::NsaException e) {
          // Do nothing
        }
      }

      if (!is_allophone && !is_nss) {
        if ((seqTypes & MASK_SEQ_ALLOPHONE) != 0) {
          // Not a french symbol, try with unicode
          try {
            roots::acoustic::Allophone all((
                roots::phonology::ipa::Ipa::from_unicode(label)));  // * enlevée
            allophoneSequence->add(&all);
            segAllRelationElts.push_back(std::pair<int, int>(
                segmentSequence->count() - 1, allophoneSequence->count() - 1));
            n_allophone++;
          } catch (roots::phonology::ipa::IpaException e) {
            /* Not unicode, do nothing */
          }
        }
      }

      if ((!is_allophone) && (!is_nss)) {
        std::stringstream ss;
        ss << unistr2stdstr(label) << " is not a known symbol or is ignored !";
        ROOTS_LOGGER_WARN(ss.str().c_str());
      }

      n_seg++;
    }

    // Build the relations mappings
    if (((seqTypes & MASK_SEQ_SEGMENT) != 0) &&
        ((seqTypes & MASK_SEQ_ALLOPHONE) != 0)) {
      segmentAllophoneRelation->set_mapping(
          new SparseMatrix(n_seg, n_allophone));
      for (unsigned int i = 0; i < segAllRelationElts.size(); i++) {
        segmentAllophoneRelation->get_mapping()->set_element(
            segAllRelationElts[i].first, segAllRelationElts[i].second, 1);
      }
      utt.add_relation(segmentAllophoneRelation);
    }

    if (((seqTypes & MASK_SEQ_SEGMENT) != 0) &&
        ((seqTypes & MASK_SEQ_NSS) != 0)) {
      segmentNssRelation->set_mapping(new SparseMatrix(n_seg, n_nss));

      for (unsigned int i = 0; i < segNssRelationElts.size(); i++) {
        segmentNssRelation->get_mapping()->set_element(
            segNssRelationElts[i].first, segNssRelationElts[i].second, 1);
      }

      utt.add_relation(segmentNssRelation);
    }
  }

  if ((seqTypes & MASK_SEQ_WORD) != 0) {
    wordSequence = new sequence::WordSequence();
    wordSequence->set_label(labels.SEQ_WORD);
    utt.add_sequence(wordSequence);

    for (unsigned int elementIndex = 0;
         elementIndex < tgridStructure->items[wordtierindex]->elements.size();
         ++elementIndex) {
      double start =
          tgridStructure->items[wordtierindex]->elements[elementIndex]->xmin;
      double end =
          tgridStructure->items[wordtierindex]->elements[elementIndex]->xmax;
      UErrorCode status = U_ZERO_ERROR;

      icu::UnicodeString label =
          *(tgridStructure->items[wordtierindex]->elements[elementIndex]->text);
      label = icu::RegexMatcher("\\|", label, 0, status).replaceAll(" ", status);
      label = icu::RegexMatcher("-$", label, 0, status)
                  .replaceAll("", status);  // remove - from hesitations

      if (label != "_" && label != "")  // If text is a word and not a pause
      {
        ROOTS_LOGGER_DEBUG("WORD LABEL = '" + label + "'");
        roots::linguistic::Word word(unistr2stdstr(label));
        wordSequence->add(&word);

        if ((seqTypes & MASK_SEQ_SEGMENT) != 0) {
          // Find first segment
          segmentIndex = 0;
          while (segmentIndex < (int)segmentSequence->count() &&
                 segmentSequence->get_item(segmentIndex)->get_segment_start() <
                     start) {
            ++segmentIndex;
          }
          firstIndex = segmentIndex;

          // Find last segment
          // segmentIndex = ;
          double segstart;
          double segend;
          while (segmentIndex < ((int)segmentSequence->count()) &&
                 segmentSequence->get_item(segmentIndex)->get_segment_end() <=
                     end) {
            segstart =
                segmentSequence->get_item(segmentIndex)->get_segment_start();
            segend = segmentSequence->get_item(segmentIndex)->get_segment_end();

#if ROOTS_LOGGER_LEVEL <= ROOTS_LOGGER_DEBUG_LEVEL
            std::stringstream ss;
            ss << segmentIndex << " - segstart = " << segstart
               << " - segend = " << segend << " / "
               << segmentSequence->get_item(segmentIndex)->to_string();
            ROOTS_LOGGER_DEBUG(ss.str());
#endif
            ++segmentIndex;
          }
          lastIndex = segmentIndex;

#if ROOTS_LOGGER_LEVEL <= ROOTS_LOGGER_DEBUG_LEVEL
          std::stringstream ss;
          ss << "found word begin at "
             << segmentSequence->get_item(firstIndex)->get_segment_start();
          ROOTS_LOGGER_DEBUG(ss.str());
          ss.str("");
          ss << "found word end at "
             << segmentSequence->get_item(lastIndex - 1)->get_segment_end();
          ROOTS_LOGGER_DEBUG(ss.str());
#endif

          // Add the (word, allophone) couple in segWordRelationElts
          for (int lidx = firstIndex; lidx < lastIndex; ++lidx) {
            segWordRelationElts.push_back(
                std::pair<int, int>(lidx, wordSequence->count() - 1));
          }
        }
      }
    }

    if ((seqTypes & MASK_SEQ_SEGMENT) != 0) {
      segmentWordRelation = new Relation(segmentSequence, wordSequence);
      segmentWordRelation->set_mapping(
          new SparseMatrix(n_seg, wordSequence->count()));
      for (unsigned int i = 0; i < segWordRelationElts.size(); i++) {
        segmentWordRelation->get_mapping()->set_element(
            segWordRelationElts[i].first, segWordRelationElts[i].second, 1);
      }
      utt.add_relation(segmentWordRelation);
    }
  }

  if ((seqTypes & MASK_SEQ_SYLLABLE) != 0) {
    syllableSequence = new sequence::SyllableSequence();
    syllableSequence->set_label(labels.SEQ_SYLLABLE);

    sequence::AllophoneSequence *sylPhonemeSequence = NULL;
    if (phonemeSeqLabel != "") {
      sylPhonemeSequence =
          utt.get_sequence(phonemeSeqLabel)->as<sequence::AllophoneSequence>();
    } else {
      if ((seqTypes & MASK_SEQ_ALLOPHONE) != 0) {
        sylPhonemeSequence = allophoneSequence;
      }
    }

    if (sylPhonemeSequence == NULL) {
      throw RootsException(
          __FILE__, __LINE__,
          "no phoneme sequence given for building syllable sequence!");
    }

    for (unsigned int elementIndex = 0;
         elementIndex < tgridStructure->items[syltierindex]->elements.size();
         ++elementIndex) {
      double start =
          tgridStructure->items[syltierindex]->elements[elementIndex]->xmin;
      double end =
          tgridStructure->items[syltierindex]->elements[elementIndex]->xmax;

      icu::UnicodeString label =
          *(tgridStructure->items[syltierindex]->elements[elementIndex]->text);

#if ROOTS_LOGGER_LEVEL <= ROOTS_LOGGER_DEBUG_LEVEL
      std::stringstream ss;
      ss << "START = " << start << " - END = " << end;
      ROOTS_LOGGER_DEBUG(ss.str());
#endif
      if (label != "_" && label != "_*_" && label != "%" && label != "#") {
        // retrouver les phonèmes de la syllabe à partir de début et fin avec
        // leur indices
        std::vector<int> phonemeIndices, segmentIndices;
        int segTmpIdx = -1;
        int realSegIdx = -1;
        double segstart;
        double segend;

        do {
          segTmpIdx++;
          segstart =
              tgridStructure->items[segtierindex]->elements[segTmpIdx]->xmin;
          segend =
              tgridStructure->items[segtierindex]->elements[segTmpIdx]->xmax;

          icu::UnicodeString seglabel =
              *(tgridStructure->items[segtierindex]->elements[segTmpIdx]->text);
          if (seglabel != "_" && seglabel != "*" && seglabel != "%" &&
              seglabel != "#") {
            ++realSegIdx;
          }

        } while (segstart < start &&
                 segTmpIdx <
                     (int)tgridStructure->items[segtierindex]->elements.size());

        while (segend <= end &&
               segTmpIdx <
                   (int)tgridStructure->items[segtierindex]->elements.size()) {
          segstart =
              tgridStructure->items[segtierindex]->elements[segTmpIdx]->xmin;
          segend =
              tgridStructure->items[segtierindex]->elements[segTmpIdx]->xmax;

          if (segend <= end) {
#if ROOTS_LOGGER_LEVEL <= ROOTS_LOGGER_DEBUG_LEVEL
            std::stringstream ss;
            ss << segTmpIdx << " - segstart = " << segstart
               << " - segend = " << segend << " / "
               << sylPhonemeSequence->get_item(realSegIdx)->to_string();
            //*(tgridStructure->items[segtierindex]->elements[segTmpIdx]->text);
            ROOTS_LOGGER_DEBUG(ss.str());
#endif
            phonemeIndices.push_back(realSegIdx);
            segmentIndices.push_back(segTmpIdx);

            segTmpIdx++;
            realSegIdx++;
          }
        }
#if ROOTS_LOGGER_LEVEL <= ROOTS_LOGGER_DEBUG_LEVEL
        std::stringstream ss;
        ss << "found syllable begin at "
           << tgridStructure->items[segtierindex]
                  ->elements[segmentIndices[0]]
                  ->xmin;
        ROOTS_LOGGER_DEBUG(ss.str());
        ss.str("");
        ss << "found syllable end at "
           << tgridStructure->items[segtierindex]
                  ->elements[segmentIndices[segmentIndices.size() - 1]]
                  ->xmax;
        ROOTS_LOGGER_DEBUG(ss.str());
#endif

        try {
          roots::phonology::syllable::Syllable syllable =
              roots::phonology::syllable::Syllable::from_phoneme_indices(
                  sylPhonemeSequence, phonemeIndices);
          syllableSequence->add(&syllable);
        } catch (roots::phonology::syllable::SyllableNoNucleusException e) {
          std::stringstream ss;
          ss << "syllable without nucleus between "
             << tgridStructure->items[segtierindex]
                    ->elements[segmentIndices[0]]
                    ->xmin;
          ss << " and "
             << tgridStructure->items[segtierindex]
                    ->elements[segmentIndices[segmentIndices.size() - 1]]
                    ->xmax;
          ROOTS_LOGGER_WARN(ss.str());
        }
      }
    }

    utt.add_sequence(syllableSequence);
  }

  // Deletes variables
}
}

#endif
