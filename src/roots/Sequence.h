/*
    This file is part of Roots.

    Roots is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Roots is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Roots.  If not, see <http://www.gnu.org/licenses/>.

    Copyright (c) 2013, 2014 IRISA
*/

#ifndef Sequence_H_
#define Sequence_H_

#include "Base.h"
#include "BaseItem.h"
#include "RelationLinkedObject.h"
#include "UtteranceLinkedObject.h"
#include "common.h"

namespace roots {

class Relation;

namespace sequence {

/**
 * @brief Generic sequence
 * @details
 * This class models generic sequences of Base items.
 * @author Cordial Group
 * @version 0.1
 * @date 2011
 */
class Sequence : public roots::Base,
                 public roots::RelationLinkedObject,
                 public roots::UtteranceLinkedObject {
 public:
  /**
   * @brief Default constructor
   */
  Sequence(bool noInit = false);

  /**
   * @brief Default constructor
   * @param name Name of the sequence
   */
  Sequence(const std::string& name, bool noInit = false);

  /**
   * @brief Copy constructor
   * @warning Do not copy backlinks
   */
  Sequence(const Sequence& s);

  /**
   * @brief clone the current sequence object
   * @return a copy of the object
   * @warning Do not copy backlinks
   * @note items are also cloned
   */
  virtual Sequence* clone() const = 0;

  /**
   * @brief Return an empty sequence with the same type
   * @return a copy of the object
   * @warning Do not copy backlinks. You will have to handly set label and
   * is_simple_item !
   */
  virtual Sequence* clone_empty() const = 0;

  /**
   * @brief delete the current sequence if not in any utterance.
   */
  virtual void destroy();

 protected:
  /**
   * @brief Protected
   * @warning Protected : use destroy !
   */
  ~Sequence();

 public:
  virtual void set_label(const std::string& alabel);

  /**
   * @brief Returns an item the sequence
   * @param index Position of the target item in the sequence. Use a negative
   * index to access from the end of the sequence, e.g., -1 to access the last
   * item.
   * @return a pointer to the item
   */
  virtual BaseItem* get_item(int index) const = 0;

  //	/**
  //	 * @brief Return all items from the current sequence
  //	 * @return A vector of items
  //	 **/
  //	virtual std::vector< BaseItem* > get_all_items() const;

  /**
   * @brief Returns the first item
   * @return pointer to the first item
   */
  virtual BaseItem* first() = 0;

  /**
   * @brief Returns the last item
   * @return pointer to the last item
   */
  virtual BaseItem* last() = 0;

  /**
   * @brief Modifies an item in the sequence
   * @param index position of the item to modify
   * @param item new item value
   */
  virtual void set_item(unsigned int index, BaseItem* item) = 0;

  /**
   * @brief Adds an item clone at the end of the sequence
   * @param item new item value
   * @return the index of the added item
   */
  virtual unsigned int add(BaseItem* item) = 0;

  /**
   * @brief Adds a list of (cloned) items at the end of the sequence
   * @param item Vector of items to be cloned and added
   * @note Not efficient
   */
  virtual void add(const std::vector<BaseItem*>& items) = 0;

  /**
   * @brief Concatenate two sequences (and update associated relations)
   * @param seq The sequence to concatenate with
   */
  virtual void concatenate(const Sequence* seq) = 0;

  /**
   * @brief Concatenate two sequences but do not update relations or related sequence
   * @param seq The sequence to concatenate with
   * @note For internal usage only
   */
  virtual void concatenate_no_propagate(const Sequence* seq) = 0;

  /**
   * @brief Inserts an item clone at the position specified in the sequence
   * @param item new item value
   * @param index position to insert element
   */
  virtual void insert(BaseItem* item, unsigned int index) = 0;

  /**
   * @brief Removes (delete object) the item at the given index
   * @param index index of the item to remove
   */
  virtual void remove(unsigned int index) = 0;

  /**
   * @brief Removes the first item and returns it
   * @return the first item
   */
  virtual BaseItem* shift() = 0;

  /**
   * @brief Removes the last item and returns it
   * @return the last item
   */
  virtual BaseItem* pop() = 0;

  /**
   * @brief Returns the number of items in the sequence
   * @returns number of items
   */
  virtual unsigned int count() const = 0;

  /**
   * @brief Clears the sequence
   */
  virtual void clear() = 0;

  /**
   * @brief return the subSequence where all items are cloned
   * @note index are shift from n to n-beginIndex
   * @param indexBegin
   * @param indexEnd (included)
   * @return the sub sequence
   */
  virtual Sequence* get_subsequence(int indexBegin, int indexEnd,
                                    int embeddedOffset = 0) const = 0;

  /**
   * @brief Tests if the current sequence is directly linked to another sequence
   *
   * @param targetSequence Label of the other sequence
   * @return True if the links exists, false otherwise
   **/
  virtual bool linked(const std::string& targetSequence) = 0;

  /**
   * @brief Checks the type of an item is valid
   * @throw RootsException if the type of the item is different or does not
   * inherit from the one of the items in the sequence
   * @param item to item of which we want to check the type
   */
  virtual void check_type(BaseItem* item) = 0;

  /**
   * @brief Gives the type of the elements in the sequence
   * @return type of the elements as a string
   */
  virtual std::string get_content_type() const = 0;

  /**
   * @brief Returns true if the items contained in the sequence are simple
   * @return true if the items contained in the sequence are simple
   */
  virtual bool get_is_simple_item() const;

  /**
   * @brief sets the is_simple_item attribute
   */
  virtual void set_is_simple_item(bool b);

  /**
   * @brief Test if the current sequence is related to another sequence through
   *an embedded relation (sequence of complex items)
   * @return True if a related sequence exists, false otherwise
   **/
  virtual bool has_related_sequence() const;

  /**
   * @brief Modifies the current sequence to take into account the insertion of
   *an element into the related sequence
   * @param index index of the element added in the related sequence
   **/
  virtual void insert_related_element(unsigned int index) = 0;

  /**
   * @brief Modifies the current sequence to take into account the deletion of
   *an element into the related sequence
   * @param index index of the element removed from the related sequence
   **/
  virtual void remove_related_element(unsigned int index) = 0;

  /**
   * @brief return the embedded relation if the item is not simple
   */
  virtual Relation* get_embedded_relation();

  /**
   * @brief return the embedded sequence if the item is not simple
   */
  virtual Sequence* get_related_sequence() const;

  /**
   * @brief Find items from a target sequence which are related to the given
   *item from the current sequence
   * @param index Index of the source item in the current sequence
   * @param targetLabel Name of the target sequence
   * @return Vector of the related items (potentially empty)
   **/
  virtual std::vector<BaseItem*> get_related_items(
      unsigned int index, const std::string& targetLabel) const;

  /**
   * @brief Find items indices from a target sequence which are related to the
   *given item from the current sequence
   * @param index Index of the source item in the current sequence
   * @param targetLabel Name of the target sequence
   * @return Vector of the related items indices (potentially empty)
   **/
  virtual std::vector<int> get_related_indices(
      unsigned int index, const std::string& targetLabel) const;

  /**
   * @brief returns the display height for the current object
   * @return display height for the current object
   */
  virtual int get_pgf_height() const;

  /**
   * @brief Serialize the object into a stream as a string
   * @param out
   * @param seq
   * @return the modified stream
   */
  friend std::ostream& operator<<(std::ostream& out, sequence::Sequence& seq);

  /**
   * @brief Prepare a sequence to be saved in a directory. Basicaly, a signal
   * segment items will copy the associated wav file if target_base_dir change.
   * @Warning items in sequences may be changed (for instance the base directory
   * of signal segment items)!
   */
  virtual void prepare_save(const std::string& target_base_dir) = 0;

  /**
   * @brief Generate the XML part of the entity
   * @param stream
   * @param currentTagName
   * @param parentNode
   */
  virtual void deflate(RootsStream* stream, bool is_terminal = true,
                       int list_index = -1);

  /**
   * @brief Extracts the entity from the stream
   * @param stream the stream from which we inflate the element
   */
  virtual void inflate(RootsStream* stream, bool is_terminal = true,
                       int list_index = -1) = 0;

  /**
   * @brief returns the XML tag name value for current object
   * @return string constant representing the XML tag name
   */
  virtual std::string get_xml_tag_name() const;
  /**
   * @brief returns the XML tag name value for current class
   * @return string constant representing the XML tag name
   */
  static std::string xml_tag_name();

  /**
   * @brief returns the mapping between the current sequence and a target
   * sequence
   * @param targetSequence the target sequence
   * @return the mapping
   * @throws RootsException if this method is not overloaded
   */
  virtual roots::Matrix* make_mapping(
      roots::sequence::Sequence* /*targetSequence*/);

  virtual roots::Relation* align_1_to_1(
      roots::sequence::Sequence* targetSequence);

  virtual roots::Relation* align_m_to_n(
      roots::sequence::Sequence* targetSequence);

  virtual std::vector<int>* find_longest_common(
      roots::sequence::Sequence* findSequence);

  virtual std::string to_string(int level = 0) const;

  template <class U>
  U* as() {
    return static_cast<U*>(this);
  }

 protected:
  /**
   * @brief Find items from a target sequence which are directly linked to the
   *given item from the current sequence
   * @remarks This method is meant to be called only when the current sequence
   *is not part of an utterance
   * @param index Index of the source item in the current sequence
   * @param targetLabel Name of the target sequence
   * @return Vector of the related items (potentially empty)
   **/
  virtual std::vector<BaseItem*> get_linked_items(
      unsigned int index, const std::string& targetLabel) const;

  /**
   * @brief Find items from a target sequence which are related to the given
   *item from the current sequence, including through transitive links
   * @warning This method can only be called if the current sequence is part of
   *at least one utterance
   * @param index Index of the source item in the current sequence
   * @param targetLabel Name of the target sequence
   * @return Vector of the related items (potentially empty)
   **/
  virtual std::vector<BaseItem*> get_related_utterance_items(
      unsigned int index, const std::string& targetLabel) const;

  /**
   * @brief Find items indices from a target sequence which are directly linked
   *to the given item from the current sequence
   * @remarks This method is meant to be called only when the current sequence
   *is not part of an utterance
   * @param index Index of the source item in the current sequence
   * @param targetLabel Name of the target sequence
   * @return Vector of the related items indices (potentially empty)
   **/
  virtual std::vector<int> get_linked_indices(
      unsigned int index, const std::string& targetLabel) const;

  /**
   * @brief Find items indices from a target sequence which are related to the
   *given item from the current sequence, including through transitive links
   * @warning This method can only be called if the current sequence is part of
   *at least one utterance
   * @param index Index of the source item in the current sequence
   * @param targetLabel Name of the target sequence
   * @return Vector of the related items indices (potentially empty)
   **/
  virtual std::vector<int> get_related_utterance_indices(
      unsigned int index, const std::string& targetLabel) const;

 private:
  bool is_simple_item; /**< indicates whether the items contained in the
                          sequence are simple or not */
};
}
}  // End of namespace

#endif /* Sequence_H_ */
