/*
    This file is part of Roots.

    Roots is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Roots is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Roots.  If not, see <http://www.gnu.org/licenses/>.

    Copyright (c) 2013, 2014 IRISA
*/


#ifndef ALIGNMENT_H_
#define ALIGNMENT_H_

#include "../common.h"
#include "../RootsException.h"
#include "../Align.h"
#include "../Base.h"
#include "../Sequence.h"

namespace roots
{
  namespace align
  {

    /**
     * @brief Alignment of items using the Levenshtein distance
     * @details
     * Give an implementation to the Align class by giving values to the different
     * costs.
     */
    class Alignment : public roots::Align< roots::Base * >
    {
    public:
      /**
       * @brief Constructor
       * @param source_sequence Pointer to the vector of source elements
       * @param target_sequence Pointer to the vector of target elements
       */
      Alignment(std::vector<Base *> & source_vector, std::vector< Base *> & target_vector);

      /**
       * @brief Constructor
       * @param source_sequence Pointer to the source sequence
       * @param target_sequence Pointer to the target sequence
       */
      Alignment(roots::sequence::Sequence & source_sequence, roots::sequence::Sequence & target_sequence);
      
      /**
       * @brief Destructor
       */
      virtual ~Alignment();
      
      /**
       * @brief Returns the local cost of a source and target element
       * @details This function is called during the alignment algorithm for each
       * pair of source and target values. These function is purely virtual and has
       * to be implemented in subclasses.
       * @param rowElement pointer to the source element
       * @param colElement pointer to the target element
       * @return the local cost for the source and target pair
       */
      virtual double local_cost_function( roots::Base*& rowElement, roots::Base*& colElement );

      /**
       * @brief Return a human readable string of the alignment.
       * @param level Level of detail: 0 is one line per sequence (horizontal display), >0 is one line per token (vertical display).
       * @return A string of all aligned items.
       */
      
      virtual std::string to_string(int level=0);
      
    };

  }
}

#endif /* ALIGNMENT_H_ */
