/*
  This file is part of Roots.

  Roots is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Roots is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	See the
  GNU Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with Roots.  If not, see <http://www.gnu.org/licenses/>.

  Copyright (c) 2013, 2014 IRISA
*/


#include "Levenshtein.h"

namespace roots{
  namespace align{

    Levenshtein::Levenshtein(std::vector<char>& source_sequence,
			     std::vector<char>& target_sequence) :
      Align<char>(source_sequence, target_sequence)
    {
      // warping coefficients for the Levenshtein distance,
      // takes only local cost for the diagonal path
      warpingMultiplier[0] = 0.0; // 0 -1
      warpingMultiplier[1] = 0.0; //-1	0
      warpingMultiplier[2] = 1.0; //-1 -1

      // warping offsets for the Levenshtein distance,
      // 1 for the deletion cost and 1 for insertion cost
      warpingOffset[0] = 1.0; // 0 -1
      warpingOffset[1] = 1.0; //-1  0
      warpingOffset[2] = 0.0; //-1 -1

      std::pair<int,int> dir;
      dir.first = -1; dir.second =  0; warpingDirection.push_back(dir);
      dir.first =  0; dir.second = -1; warpingDirection.push_back(dir);
      dir.first = -1; dir.second = -1; warpingDirection.push_back(dir);
    }

    Levenshtein::~Levenshtein()
    {
    }

    double Levenshtein::local_cost_function(char& rowValue, char& colValue)
    {
      double cost = 0.0;

      if(rowValue != colValue)
	{
	  bool substProhibited = false;
	  for(unsigned int i=0; i<get_constraint().size();i++ && !substProhibited) {
	    substProhibited = (rowValue == constraint[i]) || (colValue == constraint[i]);
	  }

	  if(substProhibited) {
	    cost = 10000.0;
	  } else {
	    cost = 1.0;
	  }
	}

      return cost;
    }

    std::string Levenshtein::to_string(int level){
      std::stringstream ss, s1, s2;
      int pos1, pos2;
      std::string str1, str2;
      size_t n_alg;

      switch(level) {
      case 0:
	n_alg = get_source_mapping().size();
	for (size_t i = 0; i < n_alg; i++) {
	  pos1 = get_source_mapping().at(i);
	  pos2 = get_target_mapping().at(i);
	  if (pos1 < 0) {
	    s1 << get_target_vector().at(pos2);
	    s2 << "*";
	  }
	  else if (pos2 < 0) {
	    s1 << get_source_vector().at(pos1);
	    s2 << "*";
	  }
	  else {
	    s1 << get_source_vector().at(pos1);
	    s2 << get_target_vector().at(pos2);
	  }
	  s1 << "\t";
	  s2 << "\t";
	}
	s1 << std::endl;
	s2 << std::endl;
	ss << s1.str() << s2.str();
	break;

      default:
	n_alg = get_source_mapping().size();
	for (size_t i = 0; i < n_alg; i++) {
	  pos1 = get_source_mapping().at(i);
	  pos2 = get_target_mapping().at(i);
	  if (pos1 < 0) {
	    ss << "*\t" << get_target_vector().at(pos2) << std::endl;
	  }
	  else if (pos2 < 0) {
	    ss << get_source_vector().at(pos1) << "\t*" << std::endl;
	  }
	  else {
	    ss << get_source_vector().at(pos1) << "\t" << get_target_vector().at(pos2) << std::endl;
	  }
	}
	break;

      }

      return ss.str();
    }


    Levenshtein_Unicode::Levenshtein_Unicode(std::vector<UChar>& source_sequence,
					     std::vector<UChar>& target_sequence) : Align<UChar>(source_sequence, target_sequence)
    {
      // warping coefficients for the Levenshtein distance,
      // takes only local cost for the diagonal path
      warpingMultiplier[0] = 0.0; // 0 -1
      warpingMultiplier[1] = 0.0; //-1	0
      warpingMultiplier[2] = 1.0; //-1 -1

      // warping offsets for the Levenshtein distance,
      // 1 for the deletion cost and 1 for insertion cost
      warpingOffset[0] = 1.0; // 0 -1
      warpingOffset[1] = 1.0; //-1  0
      warpingOffset[2] = 0.0; //-1 -1

      std::pair<int,int> dir;
      dir.first = -1; dir.second =  0; warpingDirection.push_back(dir);
      dir.first =	 0; dir.second = -1; warpingDirection.push_back(dir);
      dir.first = -1; dir.second = -1; warpingDirection.push_back(dir);
    }

    Levenshtein_Unicode::~Levenshtein_Unicode()
    {
    }

    double Levenshtein_Unicode::local_cost_function(UChar& rowValue, UChar& colValue)
    {
      double cost = 0.0;

      if(rowValue != colValue)
	{
	  bool substProhibited = false;
	  for(unsigned int i=0; i<get_constraint().size();i++ && !substProhibited)
	    {
	      substProhibited = (rowValue == constraint[i]) || (colValue == constraint[i]);
	    }

	  if(substProhibited)
	    {
	      cost = 10000.0;
	    } else {
	    cost = 1.0;
	  }
	}

      return cost;
    }

    std::string Levenshtein_Unicode::to_string(int level){
      std::stringstream ss, s1, s2;
      int pos1, pos2;
      std::string str1, str2;
      size_t n_alg;
      switch(level) {

      case 0:
	n_alg = get_source_mapping().size();
	for (size_t i = 0; i < n_alg; i++) {
	  pos1 = get_source_mapping().at(i);
	  pos2 = get_target_mapping().at(i);
	  if (pos1 < 0) {
	    s1 << get_target_vector().at(pos2);
	    s2 << "*";
	  }
	  else if (pos2 < 0) {
	    s1 << get_source_vector().at(pos1);
	    s2 << "*";
	  }
	  else {
	    s1 << get_source_vector().at(pos1);
	    s2 << get_target_vector().at(pos2);
	  }
	  s1 << "\t";
	  s2 << "\t";
	}
	s1 << std::endl;
	s2 << std::endl;
	ss << s1.str() << s2.str();
	break;

      default:
	n_alg = get_source_mapping().size();
	for (size_t i = 0; i < n_alg; i++) {
	  pos1 = get_source_mapping().at(i);
	  pos2 = get_target_mapping().at(i);
	  if (pos1 < 0) {
	    ss << "*\t" << get_target_vector().at(pos2) << std::endl;
	  }
	  else if (pos2 < 0) {
	    ss << get_source_vector().at(pos1) << "\t*" << std::endl;
	  }
	  else {
	    ss << get_source_vector().at(pos1) << "\t" << get_target_vector().at(pos2) << std::endl;
	  }
	}
	break;

      }

      return ss.str();
    }


  } // End namespace align
} // End namespace roots
