/*
    This file is part of Roots.

    Roots is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Roots is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Roots.  If not, see <http://www.gnu.org/licenses/>.

    Copyright (c) 2013, 2014 IRISA
*/


#include "Alignment.h"

namespace roots
{
  
  namespace align
  {
    
    Alignment:: Alignment(std::vector<Base *> & source_vector, std::vector< Base *> & target_vector) : Align<Base *>(source_vector, target_vector)
    {
      // warping coefficients for the Alignment distance,
      // takes only local cost for the diagonal path
      warpingMultiplier[0] = 0.0; // 0 -1
      warpingMultiplier[1] = 0.0; //-1  0
      warpingMultiplier[2] = 1.0; //-1 -1
      
      // warping offsets for the Alignment distance,
      // 1 for the deletion cost and 1 for insertion cost
      warpingOffset[0] = Base::MAX_DISSIMILARITY; // 0 -1
      warpingOffset[1] = Base::MAX_DISSIMILARITY; //-1  0
      warpingOffset[2] = 0.0; //-1 -1
      
      std::pair<int,int> dir;
      dir.first = -1; dir.second =  0; warpingDirection.push_back(dir);
      dir.first =  0; dir.second = -1; warpingDirection.push_back(dir);
      dir.first = -1; dir.second = -1; warpingDirection.push_back(dir);
    }

    Alignment::Alignment(roots::sequence::Sequence & source_sequence, roots::sequence::Sequence & target_sequence): Align<Base*>()
    {
      unsigned int n = 0;
      std::vector< Base* > all_items;

      n= source_sequence.count();      
      for (unsigned int i = 0; i < n; i++) {
	all_items.push_back(source_sequence.get_item(i));
      }
      this->set_source_vector(all_items);
      
      n = target_sequence.count();
      all_items.clear();
      for (unsigned int i = 0; i < n; i++) {
	all_items.push_back(target_sequence.get_item(i));
      }
      this->set_target_vector(all_items);

      // warping coefficients for the Alignment distance,
      // takes only local cost for the diagonal path
      warpingMultiplier[0] = 0.0; // 0 -1
      warpingMultiplier[1] = 0.0; //-1  0
      warpingMultiplier[2] = 1.0; //-1 -1
      
      // warping offsets for the Alignment distance,
      // 1 for the deletion cost and 1 for insertion cost
      warpingOffset[0] = Base::MAX_DISSIMILARITY; // 0 -1
      warpingOffset[1] = Base::MAX_DISSIMILARITY; //-1  0
      warpingOffset[2] = 0.0; //-1 -1
      
      std::pair<int,int> dir;
      dir.first = -1; dir.second =  0; warpingDirection.push_back(dir);
      dir.first =  0; dir.second = -1; warpingDirection.push_back(dir);
      dir.first = -1; dir.second = -1; warpingDirection.push_back(dir);      
    }
    
    Alignment::~Alignment()
    {
    }
    
    double Alignment::local_cost_function(Base *& rowElement, Base *& colElement)
    {
      double cost = 0.0;
      double dist = rowElement->compute_dissimilarity( colElement );
      if( rowElement != colElement )
      {
	bool substProhibited = false;
	for(unsigned int i=0; i<get_constraint().size();i++ && !substProhibited)
	{
	  substProhibited = ( rowElement == constraint[i]) || ( colElement == constraint[i]);
	}
	
	if(substProhibited)
	{
	  cost = Base::MAX_DISSIMILARITY;
	} else {
	  cost = dist;
	}
      }
      return cost;
    }
    
    
    /**
     * @brief Return a human readable string of the alignment.
     * 
     * @param level Level of detail: 0 is one line per sequence (horizontal display), >0 is one line per token (vertical display).
     * @return A string of all aligned items.
     */
    std::string Alignment::to_string(int level)
    {
      std::stringstream ss, s1, s2;
      int pos1, pos2;
      int len1, len2;
      std::string str1, str2;
      size_t n_alg;
      switch(level) {

	case 0:
	  n_alg = get_source_mapping().size();
	  for (size_t i = 0; i < n_alg; i++) {
	    pos1 = get_source_mapping().at(i);
	    pos2 = get_target_mapping().at(i);
	    if (pos1 < 0) {
	      str2 = get_target_vector().at(pos2)->to_string();
	      len2 = str2.size();
	      str1 = std::string(len2, '*');
	      len1 = len2;
	    }
	    else if (pos2 < 0) {
	      str1 = get_source_vector().at(pos1)->to_string();
	      len1 = str1.size();
	      str2 = std::string(len1, '*');
	      len2 = len1;
	    }
	    else {
	      str1 = get_source_vector().at(pos1)->to_string();
	      len1 = str1.size();
	      str2 = get_target_vector().at(pos2)->to_string();
	      len2 = str2.size();
	    }
	    s1 << str1 << std::string(std::max(len1, len2) - len1, ' ') << "\t";
	    s2 << str2 << std::string(std::max(len1, len2) - len2, ' ') << "\t";
	  }
	  s1 << std::endl;
	  s2 << std::endl;
	  ss << s1.str() << s2.str();
	  break;
	  
	default:
	  n_alg = get_source_mapping().size();
	  for (size_t i = 0; i < n_alg; i++) {
	    pos1 = get_source_mapping().at(i);
	    pos2 = get_target_mapping().at(i);
	    if (pos1 < 0) {
	      str2 = get_target_vector().at(pos2)->to_string();
	      len2 = str2.size();
	      str1 = std::string(len2, '*');
	      len1 = len2;
	    }
	    else if (pos2 < 0) {
	      str1 = get_source_vector().at(pos1)->to_string();
	      len1 = str1.size();
	      str2 = std::string(len1, '*');
	      len2 = len1;
	    }
	    else {
	      str1 = get_source_vector().at(pos1)->to_string();
	      len1 = str1.size();
	      str2 = get_target_vector().at(pos2)->to_string();
	      len2 = str2.size();
	    }
	    ss << str1 << std::string(std::max(0, 40 - len1), ' ') << "\t" << str2 << std::string(std::max(0, 40  - len2), ' ');
	  }
	  ss << std::endl;
	  break;
	  
      }
      
      return ss.str();
    }
    
  }
}
