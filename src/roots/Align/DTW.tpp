/*
  This file is part of Roots.

  Roots is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Roots is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with Roots.  If not, see <http://www.gnu.org/licenses/>.

  Copyright (c) 2013, 2014 IRISA
*/

#ifndef DTW_TPP_
#define DTW_TPP_

#include <cmath>
#include <vector>

namespace roots {
namespace align {

/* Cost functions */
namespace distances {
/**
 * Returns Euclidean distance between two vectors.
 *
 * @param v1 first vector
 * @param v2 second vector
 * @return Euclidean distance
 */
template <typename T>
T euclideanDistance(const std::vector<T>& v1, const std::vector<T>& v2) {
  T d = 0.0;
  for (unsigned int i = 0; i < v1.size(); i++) {
    d += (v1[i] - v2[i]) * (v1[i] - v2[i]);
  }
  return std::sqrt(d);
}

/**
 * Returns Manhattan (taxicab) distance between two vectors.
 *
 * @param v1 first vector
 * @param v2 second vector
 * @return Manhattan distance
 */
template <typename T>
T manhattanDistance(const std::vector<T>& v1, const std::vector<T>& v2) {
  T d = 0.0;
  for (unsigned int i = 0; i < v1.size(); i++) {
    d += std::abs(v1[i] - v2[i]);
  }
  return d;
}

/**
 * Returns Chebyshev distance between two vectors.
 *
 * @param v1 first vector
 * @param v2 second vector
 * @return Chebyshev distance
 */
template <typename T>
T chebyshevDistance(const std::vector<T>& v1, const std::vector<T>& v2) {
  T d = 0.0, max = 0.0;
  for (unsigned int i = 0; i < v1.size(); i++) {
    d = std::abs(v1[i] - v2[i]);
    if (d > max) max = d;
  }
  return max;
}

/**
 * Returns Minkowski distance (with p = 0.33) between two vectors.
 *
 * @param v1 first vector
 * @param v2 second vector
 * @return Minkowski distance
 */
template <typename T>
T minkowskiDistance(const std::vector<T>& v1, const std::vector<T>& v2) {
  T d = 0.0, p = 0.33;
  for (unsigned int i = 0; i < v1.size(); i++) {
    d += std::pow(std::abs(v1[i] - v2[i]), p);
  }

  return std::pow(d, 1.0 / p);
}
}

template <typename T>
DTW<T>::DTW(std::vector<std::vector<T> >& source_sequence,
            std::vector<std::vector<T> >& target_sequence)
    : Align<std::vector<T> >(source_sequence, target_sequence) {
  // warping coefficients for the DTW distance,
  this->warpingMultiplier[0] = 1.0;  // 0 -1
  this->warpingMultiplier[1] = 1.0;  //-1 0
  this->warpingMultiplier[2] = 2.0;  //-1 -1

  // warping offsets for the DTW distance,
  // 1 for the deletion cost and 1 for insertion cost
  this->warpingOffset[0] = 0.0;  // 0 -1
  this->warpingOffset[1] = 0.0;  //-1 0
  this->warpingOffset[2] = 0.0;  //-1 -1

  std::pair<int, int> dir;
  dir.first = -1;
  dir.second = 0;
  this->warpingDirection.push_back(dir);
  dir.first = 0;
  dir.second = -1;
  this->warpingDirection.push_back(dir);
  dir.first = -1;
  dir.second = -1;
  this->warpingDirection.push_back(dir);
}

template <typename T>
DTW<T>::~DTW<T>() {}

template <typename T>
double DTW<T>::local_cost_function(std::vector<T>& rowValue,
                                   std::vector<T>& colValue) {
  double cost =
      static_cast<double>(distances::euclideanDistance<T>(rowValue, colValue));
  return cost;
}

template <typename T>
std::string DTW<T>::to_string(int) {
  return "DTW::to_string() : Not Applicable here!";
}

}  // End namespace Align
}  // End namespace roots

#endif /* DTW_H_ */
