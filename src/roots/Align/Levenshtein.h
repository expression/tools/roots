/*
  This file is part of Roots.

  Roots is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Roots is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	See the
  GNU Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with Roots.  If not, see <http://www.gnu.org/licenses/>.

  Copyright (c) 2013, 2014 IRISA
*/


#ifndef LEVENSHTEIN_H_
#define LEVENSHTEIN_H_

#include "../common.h"
#include "../RootsException.h"

#include "../Align.h"

namespace roots
{
  namespace align
  {

    /**
     * @brief Levenshtein algorithm implementation
     * @details
     * Give an implementation to the Align class by giving values to the different
     * costs.
     * @author Cordial Group
     * @version 0.1
     * @date 2011
     */
    class Levenshtein : public Align<char>
    {
    public:
      /**
       * @brief Constructor
       * @param source_sequence source sequence of chars
       * @param target_sequence target sequence of chars
       */
      Levenshtein(std::vector<char>& source_sequence, std::vector<char>& target_sequence);
      /**
       * @brief Destructor
       */
      virtual ~Levenshtein();
      /**
       * @brief Returns the local cost of a source and target element
       * @details This function is called during the alignment algorithm for each
       * pair of source and target values. These function is purely virtual and has
       * to be implemented in subclasses.
       * @param rowValue source value
       * @param colValue target value
       * @return the local cost for the source and target pair
       */
      virtual double local_cost_function(char& rowValue, char& colValue);

      /**
       * @brief Return a human readable string of the alignment.
       * @param level Level of detail: 0 is one line per sequence (horizontal display), >0 is one line per token (vertical display).
       * @return A string of all aligned items.
       */
      virtual std::string to_string(int level=0);
    };

    /**
     * @brief Levenshtein_Unicode algorithm implementation
     * @details
     * Give an implementation to the Align class by giving values to the different
     * costs.
     * @author Cordial Group
     * @version 0.1
     * @date 2011
     */
    class Levenshtein_Unicode : public Align<UChar>
    {
    public:
      /**
       * @brief Constructor
       * @param source_sequence source sequence of UChars
       * @param target_sequence target sequence of UChars
       */
      Levenshtein_Unicode(std::vector<UChar>& source_sequence, std::vector<UChar>& target_sequence);
      /**
       * @brief Destructor
       */
      virtual ~Levenshtein_Unicode();
      /**
       * @brief Returns the local cost of a source and target element
       * @details This function is called during the alignment algorithm for each
       * pair of source and target values. These function is purely virtual and has
       * to be implemented in subclasses.
       * @param rowValue source value
       * @param colValue target value
       * @return the local cost for the source and target pair
       */
      virtual double local_cost_function(UChar& rowValue, UChar& colValue);

      /**
       * @brief Return a human readable string of the alignment.
       * @param level Level of detail: 0 is one line per sequence (horizontal display), >0 is one line per token (vertical display).
       * @return A string of all aligned items.
       */
      virtual std::string to_string(int level=0);
    };

  } // End namespace align
} // End namespace roots
#endif /* LEVENSHTEIN_H_ */
