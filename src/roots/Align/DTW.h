/*
  This file is part of Roots.

  Roots is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Roots is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	See the
  GNU Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with Roots.  If not, see <http://www.gnu.org/licenses/>.

  Copyright (c) 2013, 2014 IRISA
*/


#ifndef DTW_H_
#define DTW_H_

#include "../common.h"
#include "../RootsException.h"

#include "../Align.h"

namespace roots{
  namespace align{

    /**
     * @brief DTW algorithm implementation
     * @details
     * Give an implementation to the Align class by giving values to the different
     * costs.
     * @author Cordial Group
     * @version 0.1
     * @date 2011
     */
    template<typename T> class DTW: public Align<std::vector<T> >
    {
    public:
      /**
       * @brief Constructor
       * @param source_sequence source sequence of vectors of coeffecients
       * @param target_sequence target sequence of vectors of coeffecients
       */
      DTW<T>(std::vector<std::vector<T> >& source_sequence, 
	     std::vector<std::vector<T> >& target_sequence);
      /**
       * @brief Destructor
       */
      virtual ~DTW<T>();
      /**
       * @brief Returns the local cost of a source and target element
       * @details This function is called during the alignment algorithm for each
       * pair of source and target values.
       * Distance between two vectors is the euclidian distance here.
       * @param rowValue source value
       * @param colValue target value
       * @return the local cost for the source and target pair
       */
      virtual double local_cost_function(std::vector<T>& rowValue, std::vector<T>& colValue);

      virtual std::string to_string(int level=0);

    };
  }
}

// Include template implementation
#include "DTW.tpp"

#endif /* DTW_H_ */
