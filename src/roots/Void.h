/*
    This file is part of Roots.

    Roots is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Roots is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Roots.  If not, see <http://www.gnu.org/licenses/>.

    Copyright (c) 2013, 2014 IRISA
*/


#ifndef VOID_H_
#define VOID_H_

#include "common.h"
#include "BaseItem.h"
#include "SequenceLinkedObject.h"
#include "RelationLinkedObject.h"


namespace roots
{

  /**
   * @brief Represents a void element (with no meaning or content)
   * @details
   * This class provides a a representation of an element which has no content
   * and no particular meaning. It is usually used as an intermediate elemnts
   * to link elements of different nature together.
   *
   * @author Cordial Group
   * @version 0.1
   * @date 2011
   */
  class Void: public BaseItem
  {
  public:
    /**
     * @brief Default constructor
     * Initialize label to the empty string and also compute the timestamp and generate an ID.
     */
    Void(const bool noInit = false);
    /**
     * @brief Copy constructor
     * Initialize label and ID from the given object but compute a new timestamp.
     */
    Void(const Void&);
    /**
     * @brief Destructor
     */
    virtual ~Void();
    /**
     * @brief clone the current object
     * @return a copy of the object
     */
    virtual Void *clone() const;
    /**
     * @brief Copy the given object into the current one except for the timestamp that is recomputed.
     * @param aVoid source object
     * @return reference to the current object
     */
    Void& operator= (const Void& aVoid);
    /**
     * @brief returns a string representation of any Roots object
     * @param level the precision level of the output
     * @return string representation of the object
     */
    virtual std::string to_string(int level=0) const;

 	/**
	 * @brief Write the entity into a RootsStream
	 * @param stream the stream from which we inflate the element
	 * @param is_terminal indicates that the node is terminal (true by default)
	 */
	virtual void deflate(RootsStream * stream, bool is_terminal=true, int list_index=-1);
	/**
	 * @brief Extracts the entity from the stream
	 * @param stream the stream from which we inflate the element
	 */
	virtual void inflate(RootsStream * stream, bool is_terminal=true, int list_index=-1);
	/**
	 * @brief Extracts the entity from the stream
	 * @param stream the stream from which we inflate the element
	 */
	static Void * inflate_object(RootsStream * stream, int list_index=-1);
    /**
     * @brief returns the XML tag name value for current object
     * @return string constant representing the XML tag name
     */
    virtual std::string get_xml_tag_name() const
    {
      return xml_tag_name();
    };
    /**
     * @brief returns the XML tag name value for current class
     * @return string constant representing the XML tag name
     */
    static std::string xml_tag_name()
    {
      return "void";
    };
    /**
     * @brief returns classname for current object
     * @return string constant representing the classname
     */
    virtual std::string get_classname() const
    {
      return classname();
    };
    /**
     * @brief returns classname for current class
     * @return string constant representing the classname
     */
    static std::string classname()
    {
      return "Void";
    };
    /**
     * @brief returns display color for current object
     * @return string constant representing the pgf display color
     */
    virtual std::string get_pgf_display_color() const { return pgf_display_color();};
    /**
     * @brief returns display color for current class
     * @return string constant representing the pgf display color
     */
    static std::string pgf_display_color() { return "blue!25"; };
    /**
     * Serialize the object into a stream as a string
     *
     * @param out
     * @param aVoid
     * @return the modified stream
     */
    friend std::ostream& operator<<(std::ostream& out, Void &aVoid);

  private:

  };

}

#endif /* VOID_H_ */
