/*
    This file is part of Roots.

    Roots is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Roots is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Roots.  If not, see <http://www.gnu.org/licenses/>.

    Copyright (c) 2013, 2014 IRISA
*/

#ifndef COMMON_H_
#define COMMON_H_

#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/time.h>
#include <unistd.h>
#include <wchar.h>
#include <exception>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <map>
#include <sstream>
#include <string>
#include <typeinfo>
#include <vector>

#include <unicode/decimfmt.h>
#include <unicode/fmtable.h>
#include <unicode/gregocal.h>
#include <unicode/numfmt.h>
#include <unicode/regex.h>
#include <unicode/smpdtfmt.h>
#include <unicode/uchar.h> /* char names	      */
#include <unicode/ucnv.h>  /* C   Converter API    */
#include <unicode/uloc.h>
#include <unicode/unistr.h>
#include <unicode/ustream.h>
#include <unicode/ustring.h> /* some more string fcns*/
#include <unicode/utypes.h>  /* Basic ICU data types */

// Add extra methods
inline bool endsWith(std::string str, std::string substr) {
  if (str.length() >= substr.length()) {
    return (
        0 ==
        str.compare(str.length() - substr.length(), substr.length(), substr));
  } else {
    return false;
  }
}

inline void findAndReplace(std::string& str, const std::string& from,
                           const std::string& to) {
  size_t start_pos = 0;
  while ((start_pos = str.find(from, start_pos)) != std::string::npos) {
    str.replace(start_pos, from.length(), to);
    start_pos += to.length();
  }
}

#include <boost/config.hpp>
#include <boost/static_assert.hpp>
#include <boost/type_traits.hpp>

#include <boost/unordered_map.hpp>
#include <boost/unordered_set.hpp>

#ifdef USE_STREAM_XML
#include <xercesc/dom/DOM.hpp>
#include <xercesc/util/OutOfMemoryException.hpp>
#include <xercesc/util/PlatformUtils.hpp>
#include <xercesc/util/XMLString.hpp>
#endif

/* Logger Macrio definitions */
const int ROOTS_LOGGER_FATAL_LEVEL = 50000;
const int ROOTS_LOGGER_ERROR_LEVEL = 40000;
const int ROOTS_LOGGER_WARN_LEVEL = 30000;
const int ROOTS_LOGGER_INFO_LEVEL = 20000;
const int ROOTS_LOGGER_DEBUG_LEVEL = 10000;
const int ROOTS_LOGGER_TRACE_LEVEL = 0;

// TODO : move to a class
typedef int ROOTS_LOGGER_LEVEL_TYPE;
extern ROOTS_LOGGER_LEVEL_TYPE ROOTS_LOGGER_LEVEL;

#define ROOTS_LOGGER_FUNCTION(level, levelText, text)    \
  if (level >= ROOTS_LOGGER_LEVEL) {                     \
    std::clog << levelText << ": " << text << std::endl; \
  }

#define ROOTS_LOGGER_FATAL(text) \
  ROOTS_LOGGER_FUNCTION(ROOTS_LOGGER_FATAL_LEVEL, "fatal", text)
#define ROOTS_LOGGER_ERROR(text) \
  ROOTS_LOGGER_FUNCTION(ROOTS_LOGGER_ERROR_LEVEL, "error", text)
#define ROOTS_LOGGER_WARN(text) \
  ROOTS_LOGGER_FUNCTION(ROOTS_LOGGER_WARN_LEVEL, "warn", text)
#define ROOTS_LOGGER_INFO(text) \
  ROOTS_LOGGER_FUNCTION(ROOTS_LOGGER_INFO_LEVEL, "info", text)
#define ROOTS_LOGGER_DEBUG(text) \
  ROOTS_LOGGER_FUNCTION(ROOTS_LOGGER_DEBUG_LEVEL, "debug", text)
#define ROOTS_LOGGER_TRACE(text) \
  ROOTS_LOGGER_FUNCTION(ROOTS_LOGGER_TRACE_LEVEL, "trace", text)

//#define ROOTS_DEFAULT_REVERSE_EPSILON 65536 // More than 44100
#define ROOTS_DEFAULT_REVERSE_EPSILON 131072  // More than 128k

namespace roots {
enum Approx {
  APPROX_UNDEF,
  APPROX_FLOOR,
  APPROX_CEIL,
  APPROX_ROUND,
  APPROX_STRICT_FLOOR,
  APPROX_STRICT_CEIL,
  APPROX_STRICT_ROUND,
};

/* TODO : move this into a class */
namespace TimeUtils {
template <int REVERSE_EPSILON>
class TimeUtils_template {
 public:
  static double round(double time) { return std::floor(time + 0.5); };

  static double approx_time(double timeRef, double time) {
    if (TimeUtils_template<REVERSE_EPSILON>::equal_to(timeRef, time)) {
      return timeRef;
    }
    return time;
  };

  static double approx_time(double time) {
    return TimeUtils_template<REVERSE_EPSILON>::approx_time(
        TimeUtils_template<REVERSE_EPSILON>::round(time), time);
  };

  static bool equal_to(double time1, double time2) {
    return (std::abs((time1 * REVERSE_EPSILON) - (time2 * REVERSE_EPSILON)) <
            1.0);
  };

  static bool not_equal_to(double time1, double time2) {
    return !TimeUtils_template<REVERSE_EPSILON>::equal_to(time1, time2);
  };

  static bool less(double time1, double time2) {
    return (TimeUtils_template<REVERSE_EPSILON>::not_equal_to(time1, time2) &&
            (time1 < time2));
  };

  static bool less_equal(double time1, double time2) {
    return ((TimeUtils_template<REVERSE_EPSILON>::equal_to(time1, time2)) ||
            (time1 < time2));
  };

  static bool greater(double time1, double time2) {
    return (TimeUtils_template<REVERSE_EPSILON>::not_equal_to(time1, time2) &&
            (time1 > time2));
  };

  static bool greater_equal(double time1, double time2) {
    return ((TimeUtils_template<REVERSE_EPSILON>::equal_to(time1, time2)) ||
            (time1 > time2));
  };

  static double get_epsilon() { return (1 / ((double)REVERSE_EPSILON)); };
};

class TimeUtils : public TimeUtils_template<ROOTS_DEFAULT_REVERSE_EPSILON> {};

struct less {
  bool operator()(double t1, double t2) const {
    return TimeUtils_template<ROOTS_DEFAULT_REVERSE_EPSILON>::less(t1, t2);
  }
};
}
}

#endif /* COMMON_H_ */
