/*
    This file is part of Roots.

    Roots is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Roots is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Roots.  If not, see <http://www.gnu.org/licenses/>.

    Copyright (c) 2013, 2014 IRISA
*/

#include "Audio.h"
//#include <endian.h>

namespace roots {
namespace file {
namespace audio {

Audio::Audio() {
  set_file_name("");
  set_file_format(FILE_FORMAT_AUDIO_UNDEF);

  set_format(NULL);

  set_is_opened(false);
  set_is_loaded(false);

  startSampleLoaded = 0;
  set_precision(16);
  set_channels(1);
  set_rate(16000);
}

Audio::Audio(const std::string &aFileName, const file_format_t aFormat) {
  set_file_name(aFileName);
  set_file_format(aFormat);

  set_format(NULL);

  set_is_opened(false);
  set_is_loaded(false);

  startSampleLoaded = 0;

  set_precision(16);
  set_channels(1);
  set_rate(16000);
}

Audio::Audio(const Audio &au) {
  set_file_name(au.get_file_name());
  set_file_format(au.get_file_format());
  set_format(au.get_format());
  set_is_opened(false);
  set_is_loaded(au.is_loaded());

  samples = au.samples;
  startSampleLoaded = au.startSampleLoaded;

  set_precision(au.get_precision());
  set_rate(au.get_rate());
  set_channels(au.get_channels());
}

Audio::~Audio() { close(); }

Audio *Audio::clone() const { return new Audio(*this); }

Audio &Audio::operator=(const Audio &au) {
  set_file_name(au.get_file_name());
  set_file_format(au.get_file_format());
  set_format(au.get_format());
  set_is_opened(false);
  set_is_loaded(au.is_loaded());

  samples = au.samples;
  startSampleLoaded = au.startSampleLoaded;

  set_precision(au.get_precision());
  set_rate(au.get_rate());
  set_channels(au.get_channels());

  return *this;
}

void Audio::set_file_name(const std::string &aFileName) {
  fileName = aFileName;
}

const std::string &Audio::get_file_name() const { return fileName; }

void Audio::set_file_format(const file_format_t aFormat) {
  fileFormat = aFormat;
}

file_format_t Audio::get_file_format() const { return fileFormat; }

const std::string &Audio::get_file_format_name() const {
  return FILE_FORMAT_AUDIO[fileFormat];
}

int Audio::get_precision() const { return precision; }

void Audio::set_precision(int p) { precision = p; }

int Audio::get_channels() const { return channels; }

void Audio::set_channels(int c) { channels = c; }

int Audio::get_rate() const { return rate; }

void Audio::set_rate(int r) { rate = r; }

void Audio::set_format(sox_format_t *aSoxFormat) { format = aSoxFormat; }

sox_format_t *Audio::get_format() const { return format; }

void Audio::set_is_opened(bool flagIsOpened) { isOpened = flagIsOpened; }

bool Audio::is_opened() const { return isOpened; }

void Audio::set_is_loaded(bool flagIsLoaded) { isLoaded = flagIsLoaded; }

bool Audio::is_loaded() const { return isLoaded; }

void Audio::set_samples(const std::vector<int32_t> &samplesVect) {
  samples = samplesVect;
}

void Audio::open() {
  close();
  if (get_file_format() == FILE_FORMAT_AUDIO_RAW) {
    sox_signalinfo_t info;

    info.channels = get_channels();
    info.rate = get_rate();
    ;
    info.precision = get_precision();

    format = sox_open_read(get_file_name().c_str(), &info, NULL, NULL);
    if (format == NULL) {
      std::stringstream ss;
      ss << "Unable to open file <" << get_file_name() << ">";
      throw RootsException(__FILE__, __LINE__, ss.str());
    }
  } else {
    format = sox_open_read(get_file_name().c_str(), NULL, NULL, NULL);
    if (format == NULL) {
      std::stringstream ss;
      ss << "Unable to open file <" << get_file_name() << ">";
      throw RootsException(__FILE__, __LINE__, ss.str());
    }

    std::string soxFileFormatName = std::string(format->filetype);
    if ((get_file_format() != FILE_FORMAT_AUDIO_UNDEF) &&
        (get_file_format_name() != soxFileFormatName)) {
      std::stringstream ss;
      ss << "File format for audio is not compatible with sox intuition : "
            "audio <"
         << get_file_name() << ">  sox <" << soxFileFormatName << ">";
      throw RootsException(__FILE__, __LINE__, ss.str());
    }
  }

  set_is_opened(true);

  /* Set correct data according to stream */
  set_precision(get_format_bit_per_sample());
  set_rate(get_format_rate());
  set_channels(get_format_n_channel());
}

void Audio::unload() {
  samples.clear();
  startSampleLoaded = -1;
}

void Audio::close() {
  if (is_opened()) {
    if (sox_close(get_format()) != SOX_SUCCESS) {
      std::stringstream ss;
      ss << "Error on sox_close()";
      throw RootsException(__FILE__, __LINE__, ss.str());
    }
  }
  set_format(NULL);
  set_is_opened(false);
}

long Audio::load(long startSample, long endSample, int channelIndex) {
  if (!is_opened()) {
    open();
  }

  unload();

  if ((get_format_bit_per_sample() % 8) != 0) {
    std::stringstream ss;
    ss << "Sample size in bits <" << get_format_bit_per_sample()
       << " is not an octet multiple";
    throw RootsException(__FILE__, __LINE__, ss.str());
  }

  if ((channelIndex < 0) ||
      ((unsigned int)channelIndex > (get_format()->signal.channels))) {
    std::stringstream ss;
    ss << "channel index " << channelIndex << " is out of range [0,"
       << get_format()->signal.channels << ")";
    throw RootsException(__FILE__, __LINE__, ss.str());
  }

  if (endSample == -1) {
    endSample = (get_format_n_sample() * get_format_n_channel()) - 1;
  }

  long bufferLength =
      (endSample - startSample + 1) * get_format_n_channel() * sizeof(int32_t);
  sox_sample_t *buffer = (sox_sample_t *)malloc(bufferLength);
  if (buffer == NULL) {
    std::stringstream ss;
    ss << "Failed to allocate buffer (" << bufferLength << ")";
    throw RootsException(__FILE__, __LINE__, ss.str());
  }

  long seekPosition = startSample * get_format_n_channel();
  if (sox_seek(get_format(), seekPosition, SOX_SEEK_SET) != SOX_SUCCESS) {
    std::stringstream ss;
    ss << "sox_seek error (" << seekPosition << ")";
    throw RootsException(__FILE__, __LINE__, ss.str());
  }

  long nSoxSample =
      sox_read(get_format(), buffer, (endSample - startSample + 1));

  if (nSoxSample != (endSample - startSample + 1)) {
    std::stringstream ss;
    ss << "sox_read error, number of read samples <" << nSoxSample
       << "> is different from requested number <"
       << (endSample - startSample + 1) << ">";
    throw RootsException(__FILE__, __LINE__, ss.str());
  }

  for (int i = 0; i < nSoxSample; i += get_format_n_channel()) {
    SOX_SAMPLE_LOCALS;
    switch (get_format_bit_per_sample()) {
      case 16:
        samples.push_back((int32_t)SOX_SAMPLE_TO_SIGNED_16BIT(
            buffer[i + channelIndex], get_format()->clips));
        break;
      case 24:
        samples.push_back((int32_t)SOX_SAMPLE_TO_SIGNED_24BIT(
            buffer[i + channelIndex], get_format()->clips));
        break;
      case 32:
        samples.push_back((int32_t)SOX_SAMPLE_TO_SIGNED_32BIT(
            buffer[i + channelIndex], get_format()->clips));
        break;
      default: {
        std::stringstream ss;
        ss << "sample size in bits is unknown (" << get_format_bit_per_sample()
           << ")";
        throw RootsException(__FILE__, __LINE__, ss.str());
      }
    }
  }

  startSampleLoaded = startSample;

  set_is_loaded(true);

  free(buffer);

  return samples.size();
}

/*****************************************/

long Audio::check_convert_index(long valueIndex) {
  if (!is_loaded()) {
    load();
  }

  long localValueIndex = valueIndex - startSampleLoaded;
  localValueIndex =
      std::max(std::min(localValueIndex,
                        (long)(startSampleLoaded + get_n_values_loaded() - 1)),
               (long)0);
  /*
  if((localValueIndex<0)||
     (localValueIndex >= (startSampleLoaded + get_n_values_loaded())))
    {
      std::stringstream ss;
      ss << "sample index " << localValueIndex << " is out of range [" <<
  startSampleLoaded << "," << (startSampleLoaded+get_n_values_loaded()) << ")";
      throw RootsException(__FILE__,__LINE__, ss.str());
    }
  */

  return localValueIndex;
}

/*****************************************/

std::vector<int> Audio::get_samples(long startIndex, long endIndex) {
  if (!is_loaded()) {
    load();
  }

  int localStart = check_convert_index(startIndex);
  if (endIndex == -1) {
    endIndex = get_n_values_loaded() + localStart - 1;
  }
  int localEnd = check_convert_index(endIndex + 1);

  return std::vector<int>(samples.begin() + localStart,
                          samples.begin() + localEnd);
}

int Audio::get_sample(long sampleIndex) {
  if (!is_loaded()) {
    load();
  }
  return samples[check_convert_index(sampleIndex)];
}

/*****************************************/

double Audio::convert_index_to_time(long idx) const {
  return idx / (double)get_rate();
}

long Audio::convert_time_to_index(double time, roots::Approx approxPolicy) {
  double virtualIndex = std::max(time * (double)get_rate(), 0.0);

  if (roots::TimeUtils::TimeUtils::equal_to(
          std::floor(virtualIndex) / (double)get_rate(), time)) {
    virtualIndex = std::floor(virtualIndex);
  } else if (roots::TimeUtils::TimeUtils::equal_to(
                 std::ceil(virtualIndex) / (double)get_rate(), time)) {
    virtualIndex = std::ceil(virtualIndex);
  }

  std::stringstream ss;
  switch (approxPolicy) {
    case APPROX_FLOOR:
      return std::floor(virtualIndex);
      break;
    case APPROX_CEIL:
      return std::min(std::ceil(virtualIndex),
                      (double)(startSampleLoaded + get_n_values_loaded() - 1));
      break;
    case APPROX_ROUND:
      return std::floor(virtualIndex + 0.5);
      break;
    case APPROX_STRICT_FLOOR:
      return std::max(std::ceil(virtualIndex - 1.0), (double)0.0);
      break;
    case APPROX_STRICT_CEIL:
      return std::min(std::floor(virtualIndex + 1.0),
                      (double)(startSampleLoaded + get_n_values_loaded() - 1));
      break;
    case APPROX_STRICT_ROUND:
      return std::min(std::max(std::floor(virtualIndex + 0.5), (double)0.0),
                      (double)(startSampleLoaded + get_n_values_loaded() - 1));
      break;
    case APPROX_UNDEF:
    default:
      ss << "Audio::convert_time_to_index: Unknow or undef approximation "
            "policy!";
      throw RootsException(__FILE__, __LINE__, ss.str().c_str());
  }
}

std::vector<int> Audio::get_samples_from_time(double startTime, double endTime,
                                              roots::Approx approxStart,
                                              roots::Approx approxEnd) {
  if (!is_loaded()) {
    load();
  }
  return get_samples(convert_time_to_index(startTime, approxStart),
                     convert_time_to_index(endTime, approxEnd));
}

int Audio::get_sample_from_time(double sampleTime, roots::Approx approxPolicy) {
  if (!is_loaded()) {
    load();
  }
  return get_sample(convert_time_to_index(sampleTime, approxPolicy));
}

/*****************************************/

double Audio::get_format_rate() const {
  if (is_opened()) {
    return (double)format->signal.rate;
  } else {
    std::stringstream ss;
    ss << "Audio in not opened, related file is <" << get_file_name() << ">";
    throw RootsException(__FILE__, __LINE__, ss.str());
  }
}

int Audio::get_format_bit_per_sample() const {
  if (is_opened()) {
    return (int)format->signal.precision;
  } else {
    std::stringstream ss;
    ss << "Audio is not opened, related file is <" << get_file_name() << ">";
    throw RootsException(__FILE__, __LINE__, ss.str());
  }
}

int Audio::get_format_n_channel() const {
  if (is_opened()) {
    return (int)format->signal.channels;
  } else {
    std::stringstream ss;
    ss << "Audio in not opened, related file is <" << get_file_name() << ">";
    throw RootsException(__FILE__, __LINE__, ss.str());
  }
}

int Audio::get_format_n_sample() const {
  if (is_opened()) {
    if (!((format->signal.length % format->signal.channels) == 0)) {
      std::stringstream ss;
      ss << "get_format_n_sample(): total sample number <"
         << format->signal.length
         << "> should be a multiple of the number of channels <"
         << format->signal.channels << ">";
      throw RootsException(__FILE__, __LINE__, ss.str());
    }
    return (int)format->signal.length / (int)format->signal.channels;
  } else {
    std::stringstream ss;
    ss << "Audio in not opened, related file is <" << get_file_name() << ">";
    throw RootsException(__FILE__, __LINE__, ss.str());
  }
}

long Audio::get_n_values_loaded() const { return samples.size(); }

void Audio::export_to_file(const std::string &exportFilename, int startSample,
                           int length) {
  sox_signalinfo_t signalinfo;
  signalinfo.rate = get_rate();
  signalinfo.channels = 1;
  signalinfo.precision = get_precision();
  signalinfo.length = get_n_values_loaded();
  signalinfo.mult = NULL;

  sox_format_t *outformat = sox_open_write(exportFilename.c_str(), &signalinfo,
                                           NULL, NULL, NULL, NULL);
  if (outformat == NULL) {
    std::stringstream ss;
    ss << "Unable to open file <" << exportFilename << ">";
    throw RootsException(__FILE__, __LINE__, ss.str());
  }
  if (length > 0 && length <= get_n_values_loaded()) {
    int localSampleIndex = startSample - startSampleLoaded;
    if ((localSampleIndex < 0) ||
        (localSampleIndex >= (startSampleLoaded + get_n_values_loaded()))) {
      std::stringstream ss;
      ss << "sample index " << startSample << " is out of range ["
         << startSampleLoaded << ","
         << (startSampleLoaded + get_n_values_loaded()) << ")";
      throw RootsException(__FILE__, __LINE__, ss.str());
    }

    std::vector<sox_sample_t> tmpArray(get_n_values_loaded());

    for (int i = 0; i < length; ++i) {
      SOX_SAMPLE_LOCALS;
      switch (get_precision()) {
        case 16:
          tmpArray[i] = SOX_SIGNED_16BIT_TO_SAMPLE(samples[i + startSample],
                                                   get_format()->clips);
          break;
        case 24:
          tmpArray[i] = SOX_SIGNED_24BIT_TO_SAMPLE(samples[i + startSample],
                                                   get_format()->clips);
          break;
        case 32:
          tmpArray[i] = SOX_SIGNED_32BIT_TO_SAMPLE(samples[i + startSample],
                                                   get_format()->clips);
          break;
        default: {
          std::stringstream ss;
          ss << "sample size in bits is unknown (" << get_precision() << ")";
          throw RootsException(__FILE__, __LINE__, ss.str());
        }
      }
    }

    // long nSamplesWritten =
    sox_write(outformat, &(tmpArray[0]), length);
    //      if(nSamplesWritten != get_n_values_loaded())
    //        {
    //    std::stringstream ss;
    //    ss << "sox_write error, number of written samples <" <<
    // nSamplesWritten
    // << "> is different from requested number <" << (get_n_values_loaded()) <<
    // ">";
    //    throw RootsException(__FILE__, __LINE__,ss.str());
    //        }
  }

  if (sox_close(outformat) != SOX_SUCCESS) {
    std::stringstream ss;
    ss << "Error on sox_close()";
    throw RootsException(__FILE__, __LINE__, ss.str());
  }
}
}
}
}
