/*
    This file is part of Roots.

    Roots is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Roots is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Roots.  If not, see <http://www.gnu.org/licenses/>.

    Copyright (c) 2013, 2014 IRISA
*/


#ifndef PRAATPITCH_H
#define PRAATPITCH_H

#include "../RootsException.h"

namespace roots { namespace file { namespace praat {

/**
 * @brief Structure of a line in a PraatPitch file
 */
struct PraatPitchCandidate {
	double frequency; 
	double strength;
};

struct PraatPitchFrame {
	double intensity; 
	int nCandidates;
	std::vector<PraatPitchCandidate*> candidate;
};

struct PraatPitchFile {
	icu::UnicodeString text_type;
	double xmin; 
	double xmax;  
	int nx;
	double dx; 
	double x1; 
	int ceiling;
	int maxnCandidates;
	std::vector<PraatPitchFrame*> frames;
};

/**
 * @brief PraatPitch file
 * @details
 * Provides access to PraatPitch files
 *
 * @author Cordial Group
 * @version 0.1
 * @date 2011
 * @todo all
 */
class PraatPitch {

public:
	PraatPitch(const std::string& filename = "", std::locale _locale = std::locale(""));
	virtual ~PraatPitch();

/**
	 * @brief Loads praat pitch file data
	 * @param filename the name of the file to load
	 * @param _locale
	 * @return the content of the line in a structure
	 */
	PraatPitchFile *load(const std::string& filename = "", std::locale _locale = std::locale(""));
	
	/**
	 * @brief Saves praat pitch file data
	 * @param the content of the file as a structure
	 * @param filename the name of the file to load
	 * @param _locale
	 */	
	void save(PraatPitchFile *content, const std::string& filename = "", std::locale _locale = std::locale(""));
	
	static PraatPitchFile *extract_info_between_start_and_end(PraatPitchFile *content, double start, double end, bool removeOffset=false);

	static void delete_structure(PraatPitchFile **structure);
	
private:
	std::string filename;
	std::locale locale;	
};

} } }

#endif // PRAATPITCH_H
