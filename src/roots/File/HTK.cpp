/*
    This file is part of Roots.

    Roots is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Roots is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Roots.  If not, see <http://www.gnu.org/licenses/>.

    Copyright (c) 2013, 2014 IRISA
*/


#include "HTK.h"

namespace roots { namespace file { namespace htk {

HTK::HTK()
{
	set_file_name("");
	set_file_descriptor(NULL);
	set_vector_n(0);
	set_vector_d(0);
	set_parameter_kind_htk(HTK_PARAMETER_KIND_UNDEF);
	set_parameter_kind("");
	set_vector_loaded_start(0);
	set_has_energy(false);
	set_is_relative_energy(false);
	set_has_delta(false);
	set_has_acceleration(false);
	set_is_compressed(false);
	set_is_zero_mean(false);
	set_has_checksum(false);
	set_has_c0(false);
	set_is_opened(false);
	set_is_loaded(false);
}


HTK::HTK(const std::string& aFileName)
{
	set_file_name(aFileName);
	set_file_descriptor(NULL);
	set_vector_n(0);
	set_vector_d(0);
	set_parameter_kind_htk(HTK_PARAMETER_KIND_UNDEF);
	set_parameter_kind("");
	set_vector_loaded_start(0);
	set_has_energy(false);
	set_is_relative_energy(false);
	set_has_delta(false);
	set_has_acceleration(false);
	set_is_compressed(false);
	set_is_zero_mean(false);
	set_has_checksum(false);
	set_has_c0(false);
	set_is_opened(false);
	set_is_loaded(false);
}


HTK::~HTK()
{
	if(this->is_loaded())
	{
		unload();
	}

	close();
}

HTK::HTK(const HTK &htk)
{
	if(is_loaded())
	{
		unload();
	}

	set_file_name(htk.get_file_name());
	set_file_descriptor(NULL);
	set_parameter_kind_htk(htk.get_parameter_kind_htk());
	set_parameter_kind(htk.get_parameter_kind());
	set_has_energy(htk.has_energy());
	set_is_relative_energy(htk.is_relative_energy());
	set_has_delta(htk.has_delta());
	set_has_acceleration(htk.has_acceleration());
	set_is_compressed(htk.is_compressed());
	set_is_zero_mean(htk.is_zero_mean());
	set_has_checksum(htk.has_checksum());
	set_has_c0(htk.has_c0());
	set_vector_n(htk.get_vector_n());
	set_vector_d(htk.get_vector_d());
	set_vector_loaded_start(htk.get_vector_loaded_start());
	set_is_opened(false);
	set_is_loaded(htk.is_loaded());

  values = htk.values;
}

HTK& HTK::operator=(const HTK& htk)
{
	if(is_loaded())
	{
		unload();
	}

	set_file_name(htk.get_file_name());
	set_file_descriptor(NULL);
	set_parameter_kind_htk(htk.get_parameter_kind_htk());
	set_parameter_kind(htk.get_parameter_kind());
	set_has_energy(htk.has_energy());
	set_is_relative_energy(htk.is_relative_energy());
	set_has_delta(htk.has_delta());
	set_has_acceleration(htk.has_acceleration());
	set_is_compressed(htk.is_compressed());
	set_is_zero_mean(htk.is_zero_mean());
	set_has_checksum(htk.has_checksum());
	set_has_c0(htk.has_c0());
	set_vector_n(htk.get_vector_n());
	set_vector_d(htk.get_vector_d());
	set_vector_loaded_start(htk.get_vector_loaded_start());
	set_is_opened(false);
	set_is_loaded(htk.is_loaded());

	values = htk.values;

	return *this;
}

void HTK::set_file_descriptor(FILE *fd)
{
	this->fileDescriptor = fd;
}

FILE *HTK::get_file_descriptor() const
{
	return this->fileDescriptor;
}

void HTK::set_is_opened(bool flagIsOpened)
{
	this->isOpened = flagIsOpened;
}

bool HTK::is_opened() const
{
	return this->isOpened;
}

void HTK::set_is_loaded(bool flagIsLoaded)
{
	this->isLoaded = flagIsLoaded;
}

bool HTK::is_loaded() const
{
	return this->isLoaded;
}

const std::string &HTK::get_file_name() const
{
	return this->fileName;
}

void HTK::set_file_name(const std::string& aFileName)
{
	this->fileName = aFileName;
}

long HTK::get_vector_n() const
{
	return this->nVector;
}

void HTK::set_vector_n(long n)
{
	this->nVector = n;
}

int HTK::get_vector_d() const
{
	return this->dVector;
}

void HTK::set_vector_d(int d)
{
	this->dVector = d;
}

void HTK::set_period(float aPeriod)
{
	this->period = aPeriod;
}

float HTK::get_period() const
{
	return this->period;
}

htk_parameter_kind_t HTK::get_parameter_kind_htk() const
{
	return this->parameterKindHtk;
}

void HTK::set_parameter_kind_htk(htk_parameter_kind_t aParameterKindHtk)
{
	this->parameterKindHtk = aParameterKindHtk;
}

const std::string &HTK::get_parameter_kind() const
{
	return this->parameterKind;
}

void HTK::set_parameter_kind(const std::string & aParameterKind)
{
	this->parameterKind = aParameterKind;
}


long HTK::get_n_vector_loaded() const
{
	return this->values.size();
}

long HTK::get_vector_loaded_start() const
{
	return this->startVectorLoaded;
}

void HTK::set_vector_loaded_start(long nStartIndex)
{
	this->startVectorLoaded = nStartIndex;
}

bool HTK::has_energy() const
{
	return this->hasEnergy;
}

void HTK::set_has_energy(bool hasEnergy)
{
	this->hasEnergy = hasEnergy;
}

bool HTK::is_relative_energy() const
{
	return this->isRelativeEnergy;
}

void HTK::set_is_relative_energy(bool isRelativeEnergy)
{
	this->isRelativeEnergy = isRelativeEnergy;
}

bool HTK::has_delta() const
{
	return this->hasDeltaCoefficient;
}

void HTK::set_has_delta(bool hasDelta)
{
	this->hasDeltaCoefficient = hasDelta;
}

bool HTK::has_acceleration() const
{
	return this->hasAccelerationCoefficient;
}

void HTK::set_has_acceleration(bool hasAcceleration)
{
	 this->hasAccelerationCoefficient = hasAcceleration;
}

bool HTK::is_compressed() const
{
	return this->isCompressed;
}

void HTK::set_is_compressed(bool isCompressed)
{
	this->isCompressed = isCompressed;
}

bool HTK::is_zero_mean() const
{
	return this->isZeroMean;
}

void HTK::set_is_zero_mean(bool isZeroMean)
{
	this->isZeroMean = isZeroMean;
}

bool HTK::has_checksum() const
{
	return this->hasChecksum;
}

void HTK::set_has_checksum(bool hasChecksum)
{
	this->hasChecksum = hasChecksum;
}

bool HTK::has_c0() const
{
	return this->hasC0Coefficient;
}

void HTK::set_has_c0(bool hasC0)
{
	this->hasC0Coefficient = hasC0;
}


void HTK::open()
{
	std::stringstream fileName;

	this->open("r");
}

void HTK::open(std::string mode)
{
	std::stringstream fileName;
	FILE *fs;

	if(!this->is_opened())
	{
		fileName << this->get_file_name();

		if((fs=fopen(fileName.str().c_str(),mode.c_str()))==NULL)
		{
			std::stringstream ss;
			ss << "Error on fopen() file = " << fileName.str().c_str() << " mode = " << mode.c_str();
			throw RootsException(__FILE__,__LINE__, ss.str());
		}

		this->set_file_descriptor(fs);
		this->set_is_opened(true);
	}
}

void HTK::close()
{
	if(this->is_opened())
	{
		if(fclose(get_file_descriptor())!=0)
		{
			std::stringstream ss;
			ss << "Error on fclose(), file " << get_file_name();
			throw RootsException(__FILE__,__LINE__, ss.str());
		}

		this->set_is_opened(false);
	}
}

void HTK::read_header()
{
	int header_nsample;
	int header_period;
	short header_size;
	short header_kind;

	if(this->is_opened())
	{
		if(fseek(get_file_descriptor(),0, SEEK_SET)!=0)
		{
			std::stringstream ss;
			ss << "Error on fseek(), offset 0, file " << get_file_name();
			throw RootsException(__FILE__,__LINE__, ss.str());
		}

		if(sizeof(int)!=4)
		{
			std::stringstream ss;
			ss << " HTK::read_header(), sizeof int should be 4 bytes" ;
			throw RootsException(__FILE__,__LINE__, ss.str());
		}

		// 4 bytes
		if(fread(&header_nsample,sizeof(int),1,get_file_descriptor())!=1)
		{
			std::stringstream ss;
			ss << " HTK::read_header(), could not read nsample ";
			throw RootsException(__FILE__,__LINE__, ss.str());
		}

		// 4 bytes
		if(fread(&header_period,sizeof(int),1,get_file_descriptor())!=1)
		{
			std::stringstream ss;
			ss << " HTK::read_header(), could not read period ";
			throw RootsException(__FILE__,__LINE__, ss.str());
		}

		if(fread(&header_size,sizeof(short),1,get_file_descriptor())!=1)
		{
			std::stringstream ss;
			ss << " HTK::read_header(), could not read vector size ";
			throw RootsException(__FILE__,__LINE__, ss.str());
		}

		if(fread(&header_kind,sizeof(short),1,get_file_descriptor())!=1)
		{
			std::stringstream ss;
			ss << " HTK::read_header(), could not read vector kind";
			throw RootsException(__FILE__,__LINE__, ss.str());
		}

		this->set_vector_n(header_nsample);
		this->set_vector_d((int)header_size/(int)sizeof(float));
		this->set_period(header_period/10000000.0);

		std::stringstream codingString;

		switch(header_kind & 0x003F)
		{
			case 9 :  set_parameter_kind_htk(HTK_PARAMETER_KIND_USER);
						codingString << "USER";
					break;
			default :
					{
						std::stringstream ss;
						ss << " HTK::read_header(), HTK kind of file unknown (" << (header_kind & 0x003F) << ")";
						throw RootsException(__FILE__,__LINE__, ss.str());
					}
		}

		set_has_energy(header_kind & 0x0040);
		set_is_relative_energy(header_kind & 0x0080);
		set_has_delta(header_kind & 0x0100);
		set_has_acceleration(header_kind & 0x0200);
		set_is_compressed(header_kind & 0x0400);
		set_is_zero_mean(header_kind & 0x0800);
		set_has_checksum(header_kind & 0x1000);
		set_has_c0(header_kind & 0x2000);

		if(has_energy())
		{
			codingString << "_E";
		}

		if(has_delta())
		{
			codingString << "_D";
		}

		if(has_acceleration())
		{
			codingString << "_A";
		}

		if(is_relative_energy())
		{
			codingString << "_N";
		}

		if(is_zero_mean())
		{
			codingString << "_Z";
		}

		if(has_checksum())
		{
			codingString << "_K";
		}

		if(is_compressed())
		{
			codingString << "_C";
		}

		if(has_c0())
		{
			codingString << "_O";
		}

		//fprintf(stdout,"!!! %s\n",codingString.str().c_str());
	}
}

      void HTK::unload()
      {
				values.clear();
				set_vector_loaded_start(0);
				set_is_loaded(false);
      }

long HTK::load(long startVectorIndex,long endVectorIndex)
{
	long 	nVectorLoaded;
	long	dVector,nVector;
	long	nByteToRead,nByte;

	if(!this->is_opened())
	{
		this->open();
	}

	read_header();

	nVector = this->get_vector_n();
	dVector = this->get_vector_d();

	if(endVectorIndex==-1)
	{
		endVectorIndex = nVector-1;
	}

	if((startVectorIndex<0)||(startVectorIndex>=nVector))
	{
		std::stringstream ss;
		ss << "HTK::load startVectorIndex " << startVectorIndex << " is out of range [0," << nVector-1 << "]";
		throw RootsException(__FILE__,__LINE__, ss.str());
	}

	if((endVectorIndex<0)||(endVectorIndex>=nVector))
	{
		std::stringstream ss;
		ss << "HTK::load endVectorIndex " << endVectorIndex << " is out of range [0," << nVector-1 << "]";
		throw RootsException(__FILE__,__LINE__, ss.str());
	}

	if(endVectorIndex<startVectorIndex)
	{
		std::stringstream ss;
		ss << "HTK::load endVectorIndex (" << endVectorIndex << ") should be > to startVectorIndex (" << startVectorIndex << ")";
		throw RootsException(__FILE__,__LINE__, ss.str());
	}

	if(this->is_loaded())
	{
		unload();
	}

	nVectorLoaded = endVectorIndex-startVectorIndex+1;

	float *vectorArray = new float[nVectorLoaded*dVector];

	if(fseek(get_file_descriptor(),HTK_HEADER_SIZE+startVectorIndex*dVector*sizeof(float), SEEK_SET)!=0)
	{
		std::stringstream ss;
		ss << "HTK::load error on fseek(), offset " <<  HTK_HEADER_SIZE+startVectorIndex*dVector*sizeof(float) << ", file " << get_file_name();
		throw RootsException(__FILE__,__LINE__, ss.str());
	}

	nByteToRead = nVectorLoaded*dVector*sizeof(float);

	if((nByte = fread((void *)vectorArray,1,nByteToRead,get_file_descriptor())) != nByteToRead)
	{
		std::stringstream ss;
		ss << "HTK::load, number of red bytes <" << nByte << "> is different from the requested number of bytes <" << nByteToRead << ">";
		throw RootsException(__FILE__, __LINE__,ss.str());
	}

  values.resize(nVectorLoaded);
	for(int i=0; i<nVectorLoaded; ++i)
		{
		  values[i].assign(vectorArray+i*dVector,
												vectorArray+(i+1)*dVector);
		}

	delete[] vectorArray;

	set_vector_loaded_start(startVectorIndex);
	set_is_loaded(true);

	return nVectorLoaded;
}

      /*****************************************/
      long HTK::check_convert_index(long valueIndex)
      {
				if(!this->is_loaded())
					{ load(); }

				long localValueIndex = valueIndex - startVectorLoaded;
				localValueIndex = std::max(std::min( localValueIndex, (long) (startVectorLoaded + get_n_vector_loaded() -1)), (long) 0);

				return localValueIndex;
      }

      /*******************************************/

      std::vector<std::vector<float> > HTK::get_values(long startSample, long endSample)
      {
				if(!this->is_loaded())
					{ load(); }

				int localStart = check_convert_index(startSample);
				if(endSample==-1)
					{ endSample = get_n_vector_loaded() + localStart -1 ; }
				int localEnd = check_convert_index(endSample+1);

				return std::vector<std::vector<float> >(values.begin() + localStart, values.begin() + localEnd);
      }

			std::vector<float> HTK::get_value(long valueIndex)
      {
				if(!this->is_loaded())
					{ load(); }

				return values[check_convert_index(valueIndex)];
      }



} } }
