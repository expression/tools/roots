/*
    This file is part of Roots.

    Roots is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Roots is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Roots.  If not, see <http://www.gnu.org/licenses/>.

    Copyright (c) 2013, 2014 IRISA
*/


#ifndef PITCHMARK_H
#define PITCHMARK_H

#include "../common.h"
#include "../Acoustic/Segment.h"
#include "../RootsException.h"

#include <boost/assign/list_of.hpp> // for 'map_list_of()'
#include <boost/assign/list_inserter.hpp>

namespace roots {
  namespace file {
    namespace pitchmark {

      enum file_format_t
      {
	FILE_FORMAT_PITCHMARK_UNDEF		= 0,
	FILE_FORMAT_PITCHMARK_RAW_SAMPLE	= 1,
	FILE_FORMAT_PITCHMARK_RAW_TIME		= 2
      };


      /**
       * @brief Pitchmark file
       * @details
       * Provides access to Pitchmarks text files
       *
       * @author Talma Group
       * @version 0.1
       * @date 2012
       */
      class Pitchmark
      {

      public:
	Pitchmark(const std::string& filename = "",
		  const file_format_t aFormat=FILE_FORMAT_PITCHMARK_UNDEF,
		  std::locale _locale = std::locale(""));

	Pitchmark(const Pitchmark&);

	virtual ~Pitchmark();

	Pitchmark& operator=(const Pitchmark& pitchmark);

	void set_file_name(const std::string& aFileName);
	const std::string &get_file_name() const;

	void set_file_format(file_format_t aFormat);
	file_format_t get_file_format() const;
	const std::string &get_file_format_name() const;

	void open();
	void open(std::locale _locale);
	void close();
	long load(long startSample=0, long endSample=-1, int samplingFrequency=-1);
	void unload();
	bool is_opened() const;
	bool is_loaded() const;

	/**
	 * @brief Return value for a pitchmark index.
	 * @Note  complexity in O(valueIndex)
	 **/
	double get_value(long valueIndex) ;

	/**
	 * @brief Return time for a pitchmark index.
	 * @Note complexity in O(valueIndex)
	 **/
	double get_time(long valueIndex) ;

	/**
	 * @brief Return time for a pitchmark index.
	 * @Note complexity in O(valueIndex)
	 **/
	std::pair<double, double> get_time_and_value(long valueIndex) ;

	/**
	 * @brief Return time values for pitchmark interval
	 * @Note complexity in O(endSample)
	 **/
	std::vector<double> get_times(long startSample=0, long endSample=-1) ;

	/**
	 * @brief Return values for pitchmark interval
	 * @Note complexity in O(endSample)
	 **/
	std::vector<double> get_values(long startSample=0, long endSample=-1) ;

	/**
	 * @brief Return values for pitchmark interval
	 * @Note complexity in O(endSample)
	 **/
	std::vector<std::pair<double, double> > get_times_and_values(long startSample=0, long endSample=-1) ;

	/**
	 * @brief Return value for a pitchmark time.
	 * @Note complexity in O(Ln(get_n_values_loaded()) + endSample-startSample )
	 **/
	double get_value_from_time(double time, roots::Approx approx = APPROX_ROUND) ;

	/**
	 * @brief Return time for a pitchmark index.
	 * @Note complexity in O(Ln(get_n_values_loaded()) + endSample-startSample )
	 **/
	double get_time_from_time(double time, roots::Approx approx = APPROX_ROUND) ;

	/**
	 * @brief Return time for a pitchmark index.
	 * @Note complexity in O(Ln(get_n_values_loaded()) + endSample-startSample )
	 **/
	std::pair<double, double>
	  get_time_and_value_from_time(double time, 
				       roots::Approx approx = APPROX_ROUND) ;

	/**
	 * @brief Return time values for pitchmark interval
	 * @Note complexity in O(Ln(get_n_values_loaded()) + endSample-startSample )
	 **/
	std::vector<double> get_times_from_time(double startTime=0,
						double endTime=-1,
						roots::Approx approxStart = APPROX_FLOOR,
						roots::Approx approxEnd = APPROX_CEIL) ;

	/**
	 * @brief Return values for pitchmark interval
	 * @Note complexity in O(Ln(get_n_values_loaded()) + endSample-startSample )
	 **/
	std::vector<double> get_values_from_time(double startTime=0,
						 double endTime=-1,
						 roots::Approx approxStart = APPROX_FLOOR,
						 roots::Approx approxEnd = APPROX_CEIL) ;

	/**
	 * @brief Return values for pitchmark interval
	 * @Note complexity in O(Ln(get_n_values_loaded()) + endSample-startSample )
	 **/
	std::vector<std::pair<double,double> > 
	  get_times_and_values_from_time(double startTime=0,
					 double endTime=-1,
					 roots::Approx approxStart = APPROX_FLOOR,
					 roots::Approx approxEnd = APPROX_CEIL) ;

	/**
	 * @brief Return the index of the latter pitchmark befor timevalue.
	 * @Note complexity in O(ln(get_n_values_loaded()))
	 * @return a pair with the pmk index and its time value
	 **/
	std::pair<long, double> get_lower_bound_index(double timeValue) ;

	void set_locale(const std::locale& locale);
	const std::locale& get_locale() const;
	long get_n_values_loaded() const;

      protected:
	/**
	 * @brief Apply function extraction on sub part of the pmk set.
	 * @Note complexity in O(Ln(get_n_values_loaded()) + endSample-startSample )
	 **/
	template<class Inserter>
	  void extract_from_time_range(Inserter & insert,
				       double startTime=0,
				       double endTime=-1,
				       roots::Approx approxStart = APPROX_ROUND,
				       roots::Approx approxEnd = APPROX_ROUND
				       ) ;

	/**
	 * @brief Apply function extraction on sub part of the pmk set.
	 * @Note complexity in O(Ln(get_n_values_loaded()) + endSample-startSample )
	 **/
	template<class Inserter>
	  void extract_from_index_range(Inserter & insert,
					long startIndex=0,
					long endIndex=-1
					) ;


      private:
	long check_convert_index(long valueIndex) ;

	long load_rawsample_file(long startSample=0, 
				 long endSample=-1, 
				 int samplingFrequency=44100);
	long load_rawtime_file(long startSample=0,
			       long endSample=-1,
			       double normParameter=1.0);

	void set_is_opened(bool flagIsOpened);

	void set_is_loaded(bool flagIsLoaded);

	/* Members */

    std::ifstream	infile;
	std::string	fileName;
	file_format_t	fileFormat;

	std::map<double, double, roots::TimeUtils::less> values; /* Map time -> value */

	long startSampleLoaded;

	bool		isOpened;
	bool		isLoaded;

	std::locale	locale;
      public:
    static std::map<file_format_t, std::string>	fileFormatToString; ///< Static map to transform a file format to a string
	static std::map<std::string, file_format_t>	stringToFileFormat; ///< Static map to transform a string to a file format

      };
    }
  }
}

#endif // PITCHMARK_H
