/*
    This file is part of Roots.

    Roots is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Roots is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Roots.  If not, see <http://www.gnu.org/licenses/>.

    Copyright (c) 2013, 2014 IRISA
*/


#ifndef HTK_H
#define HTK_H

#include "../RootsException.h"
#include <sox.h>

namespace roots { namespace file { namespace htk {

enum htk_parameter_kind_t
{
	HTK_PARAMETER_KIND_UNDEF	 		= -1,
	HTK_PARAMETER_KIND_WAVEFORM	 		= 0,
	HTK_PARAMETER_KIND_LPC		 		= 1,
	HTK_PARAMETER_KIND_MFCC		 		= 6,
	HTK_PARAMETER_KIND_USER		 		= 9,
};

 static const int HTK_PARAMETER_KIND_LENGTH = 5;
 static const std::string HTK_PARAMETER_KIND[] = {
	 "undef", "waveform", "lpc", "mfcc", "user"
 };


#define HTK_HEADER_SIZE			(int)(sizeof(int)+sizeof(int)+sizeof(short)+sizeof(short))

class HTK {

public:
	HTK();
	HTK(const std::string& aFileName);
	HTK(const HTK &htk);
	virtual ~HTK();

	HTK& operator=(const HTK& htk);

	const std::string &get_file_name() const;
	void set_file_name(const std::string& aFileName);

	long get_vector_n() const;
	void set_vector_n(long n);

	int get_vector_d() const;
	void set_vector_d(int d);

	float get_period() const;
	void set_period(float aPeriod);

	htk_parameter_kind_t get_parameter_kind_htk() const;
	void set_parameter_kind_htk(htk_parameter_kind_t aParameterKind);

	const std::string &get_parameter_kind() const;
	void set_parameter_kind(const std::string &aParameterKind);

	long get_n_vector_loaded() const;

	long get_vector_loaded_start() const;
	void set_vector_loaded_start(long nStartIndex);

	bool has_energy() const;
	void set_has_energy(bool hasEnergy);

	bool is_relative_energy() const;
	void set_is_relative_energy(bool isRelativeEnergy);

	bool has_delta() const;
	void set_has_delta(bool hasDelta);

	bool has_acceleration() const;
	void set_has_acceleration(bool hasAcceleration);

	bool is_compressed() const;
	void set_is_compressed(bool isCompressed);

	bool is_zero_mean() const;
	void set_is_zero_mean(bool isZeroMean);

	bool has_checksum() const;
	void set_has_checksum(bool hasChecksum);

	bool has_c0() const;
	void set_has_c0(bool hasC0);

	void open();
	void open(std::string mode);
	void close();

	void read_header();

	long load(long startVectorIndex=0,long endVectorIndex=-1);
	void unload();

	/////

	/**
	 * @brief Return value for a spectrum index.
	 **/
	std::vector<float> get_value(long valueIndex) ;

	/**
	 * @brief Return values for spectrum interval
	 **/
	std::vector<std::vector<float> > get_values(long startSample=0, long endSample=-1) ;

	/////
 public:
	bool is_opened() const;
	bool is_loaded() const;

 private:
	long check_convert_index(long valueIndex);

 private:

	void set_file_descriptor(FILE *fd);
	FILE *get_file_descriptor() const;
	void set_is_opened(bool flagIsOpened);
	void set_is_loaded(bool flagIsLoaded);


	FILE					*fileDescriptor;
	std::string			fileName;
	long					nVector;	// number of vectors
	int						dVector;	// dimension of a vector
	float					period;
	htk_parameter_kind_t	parameterKindHtk;	// parameter kind (HTK value)
	std::string			parameterKind;		// parameter kind, to extend htk "user" kind
	bool					hasEnergy; // _E
	bool					isRelativeEnergy; // _N
	bool					hasDeltaCoefficient; // _D
	bool					hasAccelerationCoefficient; // _A
	bool					isCompressed; // _C
	bool					isZeroMean; // _Z
	bool					hasChecksum; // _K
	bool					hasC0Coefficient; // _O
	std::vector<std::vector<float> > values;
	long					startVectorLoaded;
	bool					isOpened;
	bool					isLoaded;
};

} } }

#endif // HTK_H
