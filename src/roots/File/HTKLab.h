/*
    This file is part of Roots.

    Roots is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Roots is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Roots.  If not, see <http://www.gnu.org/licenses/>.

    Copyright (c) 2013, 2014 IRISA
*/


#ifndef HTKLAB_H
#define HTKLAB_H

#include "../common.h"
#include "../RootsException.h"


namespace roots { namespace file { namespace htk {

/**
 * @brief Structure of a line in a lab file
 */
struct HTKLabLine {
	double timeStart;
	double timeEnd;
	icu::UnicodeString label;
	double likelihood;
	icu::UnicodeString label2;
	icu::UnicodeString comment;
	bool is_word;
};

/**
 * @brief HTK lab file
 * @details
 * Provides access to HTK formatted lab files
 *
 * @author Cordial Group
 * @version 0.1
 * @date 2011
 */
class HTKLab {

public:
	HTKLab(const std::string& filename = "", std::locale _locale = std::locale(""));
	virtual ~HTKLab();
	
	/**
	 * @brief Loads htk file data
	 * @details A structure is built for each line (one field for one column) 
	 * of the file and all the structures are stored in an array.
	 * @param filename the name of the file to load
	 * @param _locale
	 * @return set of lines contained in file as a structure for each line
	 */
	std::vector<HTKLabLine> * load(const std::string& filename = "", std::locale _locale = std::locale(""));
	
	/**
	 * @brief Saves htk file data
	 * @details It needs an array of struture with the following fieds : time_start,
	 * time_end, label and comment (optional).
	 * @param lines set of lines contained in file as a structure for each line
	 * @param filename the name of the file to load
	 * @param _locale
	 */	
	void save(std::vector<HTKLabLine> *lines, const std::string& filename = "", std::locale _locale = std::locale(""));
	
private:
	void filter_line(HTKLabLine*);

private:
	std::string filename;
	std::locale locale;	

};

} } }

#endif // HTKLAB_H
