/*
    This file is part of Roots.

    Roots is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Roots is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Roots.  If not, see <http://www.gnu.org/licenses/>.

    Copyright (c) 2013, 2014 IRISA
*/


#ifndef F0_H
#define F0_H

#include "../common.h"
#include "../Acoustic/Segment.h"
#include "../RootsException.h"

#include <boost/assign/list_of.hpp> // for 'map_list_of()'
#include <boost/assign/list_inserter.hpp>

namespace roots {
  namespace file {
    namespace f0 {


      enum file_format_t
      {
	FILE_FORMAT_F0_UNDEF		= 0,
	FILE_FORMAT_F0_RAW		= 1,
	FILE_FORMAT_F0_WAVESURFER	= 2
      };

      enum f0_unit_t
      {
	F0_UNIT_HZ			= 0,
	F0_UNIT_LOG			= 1
      };


      /**
       * @brief F0 file
       * @details
       * Provides access to Fundamental frequency text files
       *
       * @author Cordial Group
       * @version 0.1
       * @date 2011
       */
      class F0
      {

      public:
	F0(const std::string& filename = "",
	   const file_format_t aFormat=FILE_FORMAT_F0_UNDEF,
	   std::locale _locale = std::locale(""));

	F0(const F0&);
	F0& operator=(const F0& f0);

	virtual ~F0();

	void set_file_name(const std::string& aFileName);
	const std::string &get_file_name() const;

	void set_file_format(file_format_t aFormat);
	file_format_t get_file_format() const;
	const std::string &get_file_format_name() const;

	void open();
	void open(std::locale _locale);
	void open(int samplingFrequency);
	void open(int samplingFrequency, std::locale _locale);
	void close();
	long load(long startSample=0, long endSample=-1);
	void unload();
	bool is_opened() const;
	bool is_loaded() const;

	/**
	 * @brief Return value for a f0 index.
	 **/
	double get_value(long valueIndex) ;

	/**
	 * @brief Return values for f0 interval
	 **/
	std::vector<double> get_values(long startSample=0, long endSample=-1) ;

	/**
	 * @brief Return value for a f0 time.
	 **/
	double get_value_from_time(double time, roots::Approx approx = APPROX_ROUND) ;

	/**
	 * @brief Return values for f0 interval
	 **/
	std::vector<double> get_values_from_time(double startTime=0,
						 double endTime=-1,
						 roots::Approx approxStart = APPROX_FLOOR,
						 roots::Approx approxEnd = APPROX_CEIL) ;

	long convert_time_to_index( double time, roots::Approx approxPolicy=APPROX_ROUND);
	double convert_index_to_time( long idx) const;

	void set_sampling_frequency(int samplingFrequency);
	void set_locale(const std::locale& locale);
	void set_time_offset(double timeOffset);
	void set_time_scaling_factor(double timeScalingFactor);
	void set_f0_unit(f0_unit_t f0Unit);
	int get_sampling_frequency() const;
	const std::locale& get_locale() const;
	double get_time_offset() const;
	double get_time_scaling_factor() const;
	long get_n_values_loaded() const;
	f0_unit_t get_f0_unit() const;

      private:
	long check_convert_index(long valueIndex);

	long load_raw_file(long startSample=0,
			   long endSample=-1,
			   unsigned int nbExpectedFields = 1,
			   unsigned int idF0field = 0);

    std::ifstream			infile;
	std::string			fileName;
	file_format_t			fileFormat;
	std::locale			locale;

	std::vector<double>		values;

	long				startSampleLoaded;

	bool				isOpened;
	bool				isLoaded;

	int				samplingFrequency;
	double				timeOffset;
	f0_unit_t			f0Unit;
	double				timeScalingFactor;	/**< time scaling factor */

      public:
    static std::map<f0_unit_t, std::string>	f0unitToString; ///< Static map to transform f0 unit to a string
	static std::map<std::string, f0_unit_t>	stringToF0Unit; ///< Static map to transform a string to an f0 unit
	static std::map<file_format_t, std::string>	fileFormatToString; ///< Static map to transform a file format to a string
	static std::map<std::string, file_format_t>	stringToFileFormat; ///< Static map to transform a string to a file format

      };

    } } }

#endif // F0_H
