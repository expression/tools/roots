/*
    This file is part of Roots.

    Roots is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Roots is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Roots.  If not, see <http://www.gnu.org/licenses/>.

    Copyright (c) 2013, 2014 IRISA
*/

#ifndef AUDIO_H
#define AUDIO_H

#include <sox.h> /* Sound eXchange lib : http://sourceforge.net/projects/sox/ */
#include "../RootsException.h"
#include "../common.h"

namespace roots {
namespace file {
namespace audio {

enum file_format_t { FILE_FORMAT_AUDIO_UNDEF = 0, FILE_FORMAT_AUDIO_RAW = 1 };

static const std::string FILE_FORMAT_AUDIO[] = {"", "raw"};

class Audio {
 public:
  Audio();
  Audio(const std::string &aFileName,
        file_format_t aFormat = FILE_FORMAT_AUDIO_UNDEF);

  Audio(const Audio &audio);
  virtual Audio &operator=(const Audio &audio);
  virtual Audio *clone() const;

  virtual ~Audio();

  void set_file_name(const std::string &aFileName);
  const std::string &get_file_name() const;

  void set_file_format(file_format_t aFormat);
  file_format_t get_file_format() const;
  const std::string &get_file_format_name() const;

  int get_precision() const;
  void set_precision(int p);
  int get_channels() const;
  void set_channels(int c);
  int get_rate() const;
  void set_rate(int r);

  void set_samples(const std::vector<int> &samples);

  void open();
  void close();
  long load(long startSample = 0, long endSample = -1, int channelIndex = 0);
  void unload();
  bool is_opened() const;
  bool is_loaded() const;

  std::vector<int> get_samples(long startIndex = 0, long endIndex = -1);
  int get_sample(long sampleIndex);

  std::vector<int> get_samples_from_time(
      double startTime = 0, double endTime = -1,
      roots::Approx approxStart = APPROX_FLOOR,
      roots::Approx approxEnd = APPROX_CEIL);
  int get_sample_from_time(double sampleTime,
                           roots::Approx approxPolicy = APPROX_ROUND);

  long convert_time_to_index(double time,
                             roots::Approx approxPolicy = APPROX_ROUND);
  double convert_index_to_time(long idx) const;

  long get_n_values_loaded() const;

  void export_to_file(const std::string &exportFilename, int startSample = 0,
                      int length = -1);

  /* Should not be used ! it need file open */
  double get_format_rate() const;
  int get_format_bit_per_sample() const;
  int get_format_n_channel() const;
  int get_format_n_sample() const;

 private:
  long check_convert_index(long valueIndex);

  void set_format(sox_format_t *aSoxFormat);
  sox_format_t *get_format() const;

  void set_is_opened(bool flagIsOpened);

  void set_is_loaded(bool flagIsLoaded);

  std::string fileName;
  file_format_t fileFormat;
  sox_format_t *format;
  std::vector<int> samples;
  long startSampleLoaded;
  bool isOpened;
  bool isLoaded;

  int precision;
  int channels;
  int rate;
};
}
}
}

#endif  // WAV_H
