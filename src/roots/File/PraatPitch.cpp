/*
    This file is part of Roots.

    Roots is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Roots is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Roots.  If not, see <http://www.gnu.org/licenses/>.

    Copyright (c) 2013, 2014 IRISA
*/


#include "PraatPitch.h"

namespace roots { namespace file { namespace praat {


PraatPitch::PraatPitch(const std::string& filename, std::locale _locale)
{
	this->filename = filename;
	this->locale = _locale;
}

PraatPitch::~PraatPitch()
{
}

PraatPitchFile *PraatPitch::load(const std::string& filename, std::locale _locale)
{
	std::ifstream infile;
	std::stringstream ss;
	UErrorCode status = U_ZERO_ERROR;
	icu::RegexMatcher m("\\s", 0, status);
	//const int maxFields = 6;
	icu::UnicodeString unicodeLine; //, unicodeLine2;
	icu::Formattable fmtable;
	UErrorCode success = U_ZERO_ERROR;
	icu::NumberFormat* form = icu::NumberFormat::createInstance(icu::Locale(icu::Locale::getEnglish()), success);
	char line[512];
	PraatPitchFile *result = new PraatPitchFile();
	PraatPitchCandidate *element = NULL;
			
	if(filename != "") this->filename = filename;
	if(_locale != std::locale("")) this->locale = _locale;
	
	ss << this->filename;

	infile.open(ss.str().c_str());
	if(infile.fail())
	{
		ROOTS_LOGGER_ERROR("file not found!");
		std::stringstream ss2;
		ss2 << "file \"" << ss.str() << "\" not found! \n";
		throw RootsException(__FILE__, __LINE__, ss2.str().c_str());
	}
	
	infile.imbue(this->locale);
	
	result->xmin = 0.0; 
	result->xmax = 0.0;  
	result->nx = 0;
	result->dx = 0.0; 
	result->x1 = 0.0; 
	result->ceiling = 0;
	result->maxnCandidates = 0;

	//read header
	//read xmin, xmax, size
	bool endOfHeader = false;
	while(!endOfHeader && !infile.eof())
	{
		infile.getline(line,512);
		
		// split the line into fields
		unicodeLine = icu::UnicodeString(line);
		unicodeLine = icu::RegexMatcher("^\\s", unicodeLine, 0, status).replaceAll("", status);
		unicodeLine = icu::RegexMatcher("\\s$", unicodeLine, 0, status).replaceAll("", status);
		
		
		if(unicodeLine.startsWith(icu::UnicodeString("File type")) == true)
		{
		        unicodeLine = icu::RegexMatcher("\\s", unicodeLine, 0, status).replaceAll("", status);
			unicodeLine.remove(0, unicodeLine.indexOf("=")+1);
			unicodeLine = icu::RegexMatcher("\"", unicodeLine, 0, status).replaceAll("", status);
			result->text_type = unicodeLine;
		}
		if(unicodeLine.startsWith(icu::UnicodeString("xmin")) == true)
		{
		        unicodeLine = icu::RegexMatcher("\\s", unicodeLine, 0, status).replaceAll("", status);
			unicodeLine.remove(0, unicodeLine.indexOf("=")+1);
			
			form->parse(unicodeLine, fmtable, success);
			result->xmin = fmtable.getDouble(success);
			if (U_FAILURE(success))
			{
				ROOTS_LOGGER_ERROR("xmin value is not a valid number!");
				std::stringstream ss2;
				ss2 << "xmin value is not a valid number (" << unicodeLine << ")! \n";
				throw RootsException(__FILE__, __LINE__, ss2.str().c_str());
			}
		}
		if(unicodeLine.startsWith(icu::UnicodeString("xmax")) == true)
		{
		        unicodeLine = icu::RegexMatcher("\\s", unicodeLine, 0, status).replaceAll("", status);
			unicodeLine.remove(0, unicodeLine.indexOf("=")+1);
			
			form->parse(unicodeLine, fmtable, success);
			result->xmax = fmtable.getDouble(success);
			if (U_FAILURE(success))
			{
				ROOTS_LOGGER_ERROR("xmax value is not a valid number!");
				std::stringstream ss2;
				ss2 << "xmax value is not a valid number (" << unicodeLine << ")! \n";
				throw RootsException(__FILE__, __LINE__, ss2.str().c_str());
			}
		}
		
		if(unicodeLine.startsWith(icu::UnicodeString("nx")) == true)
		{
		        unicodeLine = icu::RegexMatcher("\\s", unicodeLine, 0, status).replaceAll("", status);
			unicodeLine.remove(0, unicodeLine.indexOf("=")+1);
			
			form->parse(unicodeLine, fmtable, success);
			result->nx = fmtable.getLong(success);
			if (U_FAILURE(success))
			{
				ROOTS_LOGGER_ERROR("nx value is not a valid number!");
				std::stringstream ss2;
				ss2 << "nx value is not a valid number (" << unicodeLine << ")! \n";
				throw RootsException(__FILE__, __LINE__, ss2.str().c_str());
			}
		}
		
		if(unicodeLine.startsWith(icu::UnicodeString("dx")) == true)
		{
		        unicodeLine = icu::RegexMatcher("\\s", unicodeLine, 0, status).replaceAll("", status);
			unicodeLine.remove(0, unicodeLine.indexOf("=")+1);
			
			form->parse(unicodeLine, fmtable, success);
			result->dx = fmtable.getDouble(success);
			if (U_FAILURE(success))
			{
				ROOTS_LOGGER_ERROR("dx value is not a valid number!");
				std::stringstream ss2;
				ss2 << "dx value is not a valid number (" << unicodeLine << ")! \n";
				throw RootsException(__FILE__, __LINE__, ss2.str().c_str());
			}
		}
		
		if(unicodeLine.startsWith(icu::UnicodeString("x1")) == true)
		{
		        unicodeLine = icu::RegexMatcher("\\s", unicodeLine, 0, status).replaceAll("", status);
			unicodeLine.remove(0, unicodeLine.indexOf("=")+1);
			
			form->parse(unicodeLine, fmtable, success);
			result->x1 = fmtable.getDouble(success);
			if (U_FAILURE(success))
			{
				ROOTS_LOGGER_ERROR("x1 value is not a valid number!");
				std::stringstream ss2;
				ss2 << "x1 value is not a valid number (" << icu::UnicodeString(unicodeLine) << ")! \n";
				throw RootsException(__FILE__, __LINE__, ss2.str().c_str());
			}
		}
		
		if(unicodeLine.startsWith(icu::UnicodeString("ceiling")) == true)
		{
		        unicodeLine = icu::RegexMatcher("\\s", unicodeLine, 0, status).replaceAll("", status);
			unicodeLine.remove(0, unicodeLine.indexOf("=")+1);
			
			form->parse(unicodeLine, fmtable, success);
			result->ceiling = fmtable.getLong(success);
			if (U_FAILURE(success))
			{
				ROOTS_LOGGER_ERROR("ceiling value is not a valid number!");
				std::stringstream ss2;
				ss2 << "ceiling value is not a valid number (" << icu::UnicodeString(unicodeLine) << ")! \n";
				throw RootsException(__FILE__, __LINE__, ss2.str().c_str());
			}
		}
		
		if(unicodeLine.startsWith(icu::UnicodeString("maxnCandidates")) == true)
		{
		        unicodeLine = icu::RegexMatcher("\\s", unicodeLine, 0, status).replaceAll("", status);
			unicodeLine.remove(0, unicodeLine.indexOf("=")+1);
			
			form->parse(unicodeLine, fmtable, success);
			result->maxnCandidates = fmtable.getLong(success);
			if (U_FAILURE(success))
			{
				ROOTS_LOGGER_ERROR("maxnCandidates value is not a valid number!");
				std::stringstream ss2;
				ss2 << "maxnCandidates value is not a valid number (" << icu::UnicodeString(unicodeLine) << ")! \n";
				throw RootsException(__FILE__, __LINE__, ss2.str().c_str());
			}
		}		
		
		if(unicodeLine.startsWith(icu::UnicodeString("frame")) == true)
		{
			endOfHeader = true;
		}
	}
	
	
	if(result->text_type == "ooTextFile")
	{
		while(!infile.eof())
		{
			icu::UnicodeString regexp = "frame\\s*\\[\\d+]";
			icu::RegexMatcher *matcher = new icu::RegexMatcher(regexp, 0, status);
			if(U_FAILURE(success))
			{
				ROOTS_LOGGER_ERROR("the regular expression is not valid!");
				std::stringstream ss2;
				ss2 << "the regular expression is not valid (" << regexp << ")! \n";
				throw RootsException(__FILE__, __LINE__, ss2.str().c_str());
			}
			matcher->reset(unicodeLine);
			while(!infile.eof() && !matcher->find())
			{
				infile.getline(line,512);	
				unicodeLine = icu::UnicodeString(line);
				matcher->reset(unicodeLine);
			}
			delete matcher;
			matcher = NULL;
			
			if(!infile.eof())
			{						
				// lire le numero de la frame
			        unicodeLine = icu::RegexMatcher("\\s", unicodeLine, 0, status).replaceAll("", status);
				unicodeLine.remove(0, unicodeLine.indexOf("[")+1);
				unicodeLine.remove(unicodeLine.indexOf("]"), unicodeLine.length());
								
				form->parse(unicodeLine, fmtable, success);
				(void) fmtable.getLong(success); /* Ignore the number */
				if (U_FAILURE(success))
				{
					ROOTS_LOGGER_ERROR("size value is not a valid number!");
					std::stringstream ss2;
					ss2 << "size value is not a valid number (" << unicodeLine << ")! \n";
					throw RootsException(__FILE__, __LINE__, ss2.str().c_str());
				}
				
				
				// Build a new item
				PraatPitchFrame *frame = new PraatPitchFrame();	
				frame->intensity = 0.0;
				frame->nCandidates = 0;	
				
				// read intensity // not a standard value: treat it if exists
				infile.getline(line,512);
				unicodeLine = icu::UnicodeString(line);
				unicodeLine = icu::RegexMatcher("^\\s*", unicodeLine, 0, status).replaceAll("", status);
				if(unicodeLine.startsWith(icu::UnicodeString("intensity")) == true)
				{					
					unicodeLine.remove(0, unicodeLine.indexOf("=")+1);
					unicodeLine = icu::RegexMatcher("\\s", unicodeLine, 0, status).replaceAll("", status);
					form->parse(unicodeLine, fmtable, success);
					frame->intensity = fmtable.getDouble(success);
					
					infile.getline(line,512);
					unicodeLine = icu::UnicodeString(line);
				}
				
				// read nCandidates				
				unicodeLine = icu::RegexMatcher("^\\s*", unicodeLine, 0, status).replaceAll("", status);
				if(unicodeLine.startsWith(icu::UnicodeString("nCandidates")) == true)
				{					
					unicodeLine.remove(0, unicodeLine.indexOf("=")+1);
					unicodeLine = icu::RegexMatcher("\\s", unicodeLine, 0, status).replaceAll("", status);
					form->parse(unicodeLine, fmtable, success);
					frame->nCandidates = fmtable.getLong(success);
				}
				else
				{									
					std::stringstream ss2;
					ss2 << "expected nCandidates=\"<value>\" and found: " << unicodeLine << " ! \n";
					ROOTS_LOGGER_ERROR(ss2.str().c_str());
					throw RootsException(__FILE__, __LINE__, ss2.str().c_str());
				}
				
								
				if(frame->nCandidates != -1)
				{
					frame->candidate.reserve(frame->nCandidates);
					
					bool endOfCandidates = false;
					int candidateNumber = -1;
					infile.getline(line,512); // skip the line "candidate []:"		
					infile.getline(line,512);
					unicodeLine = icu::UnicodeString(line);
					
					while(!endOfCandidates && !infile.eof())
					{
					        unicodeLine = icu::RegexMatcher("^\\s*", unicodeLine, 0, status).replaceAll("", status);
						
						if(unicodeLine.startsWith(icu::UnicodeString("frame")) == true)
						{
							endOfCandidates = true;
						}
						else
						{							
							// read candidate number						
							if(unicodeLine.startsWith(icu::UnicodeString("candidate")) == true)
							{	
							        unicodeLine = icu::RegexMatcher("\\s", unicodeLine, 0, status).replaceAll("", status);				
								unicodeLine.remove(0, unicodeLine.indexOf("[")+1);
								unicodeLine.remove(unicodeLine.indexOf("]"), unicodeLine.length());
								
								form->parse(unicodeLine, fmtable, success);
								candidateNumber = fmtable.getLong(success);
								if (U_FAILURE(success))
								{
									ROOTS_LOGGER_ERROR("candidate index value is not a valid number!");
									std::stringstream ss2;
									ss2 << "candidate index value is not a valid number (" << unicodeLine << ")! \n";
									throw RootsException(__FILE__, __LINE__, ss2.str().c_str());
								}
								
								if(element!=NULL) 
								{
									frame->candidate.push_back(element);
									//delete element;
								}
								element = new PraatPitchCandidate();
								element->frequency = 0.0;
								element->strength = 0.0;								
							}
							
							if(candidateNumber == -1)
							{
								ROOTS_LOGGER_ERROR("missing candidate index!");
								std::stringstream ss2;
								ss2 << "missing candidate index! \n";
								throw RootsException(__FILE__, __LINE__, ss2.str().c_str());
							}
		
							
							// read frequency
							if(unicodeLine.startsWith(icu::UnicodeString("frequency")) == true)
							{				
							        unicodeLine = icu::RegexMatcher("\\s", unicodeLine, 0, status).replaceAll("", status);					
								unicodeLine.remove(0, unicodeLine.indexOf("=")+1);
								form->parse(unicodeLine, fmtable, success);
								element->frequency = fmtable.getDouble(success);
								
								if (U_FAILURE(success))
								{
									ROOTS_LOGGER_ERROR("frequency value is not a valid number!");
									std::stringstream ss2;
									ss2 << "frequency value is not a valid number (" << unicodeLine << ")! \n";
									throw RootsException(__FILE__, __LINE__, ss2.str().c_str());
								}
							}
							
							// read stength
							if(unicodeLine.startsWith(icu::UnicodeString("strength")) == true)
							{					
							        unicodeLine = icu::RegexMatcher("\\s", unicodeLine, 0, status).replaceAll("", status);
								unicodeLine.remove(0, unicodeLine.indexOf("=")+1);
								form->parse(unicodeLine, fmtable, success);
								element->strength = fmtable.getDouble(success);
								
								if (U_FAILURE(success))
								{
									ROOTS_LOGGER_ERROR("strength value is not a valid number!");
									std::stringstream ss2;
									ss2 << "strength value is not a valid number (" << icu::UnicodeString(unicodeLine) << ")! \n";
									throw RootsException(__FILE__, __LINE__, ss2.str().c_str());
								}
							}									
							
							infile.getline(line,512);
							unicodeLine = icu::UnicodeString(line);
						}
					}
					if(element!=NULL) 
					{
						frame->candidate.push_back(element);
						element = NULL;
					}			
				}
				
				result->frames.push_back(frame);
			} // if not eof			
		}
	}
	else
	{
		ROOTS_LOGGER_ERROR("type of file unknown or not implemented!");
		std::stringstream ss2;
		ss2 << "type of file unknown or not implemented! \n";
		throw RootsException(__FILE__, __LINE__, ss2.str().c_str());
	}
	
	infile.close();
	
	return result;	
}
	

void PraatPitch::save(PraatPitchFile *content, const std::string& filename, std::locale _locale)
{	
	std::ofstream outfile;
	std::stringstream ss;
	UErrorCode success = U_ZERO_ERROR;
	icu::NumberFormat* form = icu::NumberFormat::createInstance(icu::Locale(icu::Locale::getEnglish()), success);
	icu::UnicodeString number;
			
	if(filename != "") this->filename = filename;
	if(_locale != std::locale("")) this->locale = _locale;
	
	ss << this->filename;

	outfile.open(ss.str().c_str());
	if(outfile.fail())
	{		
		std::stringstream ss2;
		ss2 << "can't open file \"" << ss.str() << "\" in write mode! \n";
		ROOTS_LOGGER_ERROR(ss2.str().c_str());
		throw RootsException(__FILE__, __LINE__, ss2.str());
	}
		
	form->setGroupingUsed(false);
	
	outfile.imbue(this->locale);
	
	outfile.setf(std::ios::fixed,std::ios::floatfield);
	
	outfile << "File type = \"" << content->text_type << "\"" << std::endl;
	outfile << "Object class = \"" << "Pitch" << "\"" << std::endl;
	outfile << std::endl;

	outfile << "xmin = " << form->format(content->xmin, number) << std::endl;
	number = "";
	outfile << "xmax = " << form->format(content->xmax, number) << std::endl;
	number = "";
	outfile << "nx = " << form->format(content->nx, number) << std::endl;
	number = "";
	outfile << "dx = " << form->format(content->dx, number) << std::endl;
	number = "";
	outfile << "x1 = " << form->format(content->x1, number) << std::endl;
	number = "";
	outfile << "ceiling = " << form->format(content->ceiling, number) << std::endl;
	number = "";
	outfile << "maxnCandidates = " << form->format(content->maxnCandidates, number) << std::endl;
	number = "";
	outfile << "frame []: " << std::endl;
	for(unsigned int itemIndex=0; itemIndex < content->frames.size(); ++itemIndex)
	{
		outfile << "    frame [" << form->format((int)(itemIndex+1), number) << "]:" << std::endl;
		number = "";
		outfile << "        intensity = " << form->format(content->frames[itemIndex]->intensity, number) << std::endl;
		number = "";
		outfile << "        nCandidates = " << form->format(content->frames[itemIndex]->nCandidates, number) << std::endl;
		number = "";
		outfile << "        candidate []:" << std::endl;
		for(unsigned int elementIndex=0; elementIndex < content->frames[itemIndex]->candidate.size(); ++elementIndex)
		{
			outfile << "            candidate [" << form->format((int)(elementIndex+1), number) << "]:" << std::endl;
			number = "";
			outfile << "                frequency = " << form->format(content->frames[itemIndex]->candidate[elementIndex]->frequency, number) << std::endl;
			number = "";
			outfile << "                strength = " << form->format(content->frames[itemIndex]->candidate[elementIndex]->strength, number) << std::endl;
			number = "";
		}
	}
	 
	outfile.close();
}


PraatPitchFile *PraatPitch::extract_info_between_start_and_end(PraatPitchFile *content, double start, double end, bool removeOffset)
{
	PraatPitchFile *output;
	
	if(start < 0.0) start = 0.0;
	
	if(start>=end)
	{
		return NULL;
	}
	
	output = new PraatPitchFile();
	output->text_type = content->text_type;
	output->dx = content->dx;
	output->ceiling = content->ceiling;
	output->maxnCandidates = content->maxnCandidates;
	output->x1 = 0.0;
	if(removeOffset)
	{	
		output->xmin = 0.0;
		output->xmax = end - start + output->x1;
	}
	else
	{
		output->xmin = start;
		output->xmax = end + output->x1;		 		
	}
	
	unsigned int startIndex = (unsigned int)((start - content->x1) / content->dx);
	unsigned int endIndex = (unsigned int)((end - content->x1) / content->dx);
	
	for(unsigned int itemIndex=startIndex; itemIndex <= endIndex && itemIndex < content->frames.size(); ++itemIndex)
	{
		PraatPitchFrame *frame = new PraatPitchFrame();
		frame->intensity = content->frames[itemIndex]->intensity;
		frame->nCandidates = content->frames[itemIndex]->nCandidates;
		
		PraatPitchCandidate *element = NULL;
		
		for(unsigned int elementIndex=0; elementIndex < content->frames[itemIndex]->candidate.size(); ++elementIndex)
		{	
				element = new PraatPitchCandidate();
				element->frequency = content->frames[itemIndex]->candidate[elementIndex]->frequency;
				element->strength = content->frames[itemIndex]->candidate[elementIndex]->strength;
				frame->candidate.push_back(element);
		}

		output->frames.push_back(frame);
	}
	
	output->nx = output->frames.size();
	
	return output;
}

void PraatPitch::delete_structure(PraatPitchFile **content)
{
	PraatPitchFile *structure = *content;
	for(unsigned int itemIndex=0; itemIndex < structure->frames.size(); ++itemIndex)
	{
		for(unsigned int elementIndex=0; elementIndex < structure->frames[itemIndex]->candidate.size(); ++elementIndex)
		{
			delete structure->frames[itemIndex]->candidate[elementIndex];
		}
		delete structure->frames[itemIndex];
	}
	delete structure;
	*content = NULL;
}


} } }

