/*
    This file is part of Roots.

    Roots is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Roots is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Roots.  If not, see <http://www.gnu.org/licenses/>.

    Copyright (c) 2013, 2014 IRISA
*/


#ifndef TEXTGRID_H
#define TEXTGRID_H

#include "../RootsException.h"

namespace roots { namespace file { namespace praat {

/**
 * @brief Structure of a line in a TextGrid file
 */
struct TextGridElement {
	int interval;
	double xmin;
	double xmax;
	icu::UnicodeString *text;
};

struct TextGridItem {
	icu::UnicodeString *classname;
	icu::UnicodeString *name;
	double xmin;
	double xmax;
	std::vector<TextGridElement*> elements; 
};

struct TextGridFile {
	icu::UnicodeString text_type;
	double xmin;
	double xmax;
	std::vector<TextGridItem*> items;
};

class TextGrid {

public:
	TextGrid(const std::string& filename = "", std::locale _locale = std::locale(""));
	virtual ~TextGrid();
	
	/**
	 * @brief Loads textgrid file data
	 * @param filename the name of the file to load
	 * @param _locale
	 * @return the content of the line in a structure
	 */
	TextGridFile *load(const std::string& filename = "", std::locale _locale = std::locale(""));
	
	/**
	 * @brief Saves textgrid file data
	 * @param the content of the file as a structure
	 * @param filename the name of the file to load
	 * @param _locale
	 */	
	void save(TextGridFile *content, const std::string& filename = "", std::locale _locale = std::locale(""));
	
	static TextGridFile *extract_info_between_start_and_end(TextGridFile *content, double start, double end, bool removeOffset=false);
	
	static int get_tier_index(TextGridFile *content, std::string tiername);
	
	static void delete_structure(TextGridFile **content);
	
private:
	std::string filename;
	std::locale locale;	
};

} } }

#endif // TEXTGRID_H
