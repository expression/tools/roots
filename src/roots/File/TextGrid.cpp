/*
    This file is part of Roots.

    Roots is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Roots is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Roots.  If not, see <http://www.gnu.org/licenses/>.

    Copyright (c) 2013, 2014 IRISA
*/


#include "TextGrid.h"

namespace roots { namespace file { namespace praat {


TextGrid::TextGrid(const std::string& filename, std::locale _locale)
{
	this->filename = filename;
	this->locale = _locale;
}

TextGrid::~TextGrid()
{
}

TextGridFile *TextGrid::load(const std::string& filename, std::locale _locale)
{
	std::ifstream infile;
	std::stringstream ss;
	UErrorCode status = U_ZERO_ERROR;
	icu::RegexMatcher m("\\s", 0, status);
	//const int maxFields = 6;
	icu::UnicodeString unicodeLine; //, unicodeLine2;
	icu::Formattable fmtable;
	UErrorCode success = U_ZERO_ERROR;
	icu::NumberFormat* form = icu::NumberFormat::createInstance(icu::Locale(icu::Locale::getEnglish()), success);
	char line[512];
	TextGridFile *result = new TextGridFile();
	TextGridElement *element = NULL;
			
	if(filename != "") this->filename = filename;
	if(_locale != std::locale("")) this->locale = _locale;
	
	ss << this->filename;

	infile.open(ss.str().c_str());
	if(infile.fail())
	{
		ROOTS_LOGGER_ERROR("file not found!");
		std::stringstream ss2;
		ss2 << "file \"" << ss.str() << "\" not found! \n";
		throw RootsException(__FILE__, __LINE__, ss2.str().c_str());
	}
	
	infile.imbue(this->locale);
	
	result->xmin = -1;
	result->xmax = -1;

	//read header
	//read xmin, xmax, size
	bool endOfHeader = false;
	while(!endOfHeader && !infile.eof())
	{
		infile.getline(line,512);
		
		// split the line into fields
		unicodeLine = icu::UnicodeString(line);
		unicodeLine = icu::RegexMatcher("^\\s", unicodeLine, 0, status).replaceAll("", status);
		unicodeLine = icu::RegexMatcher("\\s$", unicodeLine, 0, status).replaceAll("", status);
		
		
		if(unicodeLine.startsWith(icu::UnicodeString("File type")) == true)
		{
		        unicodeLine = icu::RegexMatcher("\\s", unicodeLine, 0, status).replaceAll("", status);
			unicodeLine.remove(0, unicodeLine.indexOf("=")+1);
			unicodeLine = icu::RegexMatcher("\"", unicodeLine, 0, status).replaceAll("", status);
			result->text_type = unicodeLine;
// 			result->text_type = unicodeLine;
		}
		if(unicodeLine.startsWith(icu::UnicodeString("xmin")) == true)
		{
		        unicodeLine = icu::RegexMatcher("\\s", unicodeLine, 0, status).replaceAll("", status);
			unicodeLine.remove(0, unicodeLine.indexOf("=")+1);
			
			form->parse(unicodeLine, fmtable, success);
			result->xmin = fmtable.getDouble(success);
			if (U_FAILURE(success))
			{
				ROOTS_LOGGER_ERROR("xmin value is not a valid number!");
				std::stringstream ss2;
				ss2 << "xmin value is not a valid number (" << unicodeLine << ")! \n";
				throw RootsException(__FILE__, __LINE__, ss2.str().c_str());
			}
		}
		if(unicodeLine.startsWith(icu::UnicodeString("xmax")) == true)
		{
		        unicodeLine = icu::RegexMatcher("\\s", unicodeLine, 0, status).replaceAll("", status);
			unicodeLine.remove(0, unicodeLine.indexOf("=")+1);
			
			form->parse(unicodeLine, fmtable, success);
			result->xmax = fmtable.getDouble(success);
			if (U_FAILURE(success))
			{
				ROOTS_LOGGER_ERROR("xmax value is not a valid number!");
				std::stringstream ss2;
				ss2 << "xmax value is not a valid number (" << unicodeLine << ")! \n";
				throw RootsException(__FILE__, __LINE__, ss2.str().c_str());
			}
		}
		if(unicodeLine.startsWith(icu::UnicodeString("item")) == true)
		{
			endOfHeader = true;
		}
	}
	
	
	if(result->text_type == "ooTextFile")
	{
		while(!infile.eof())
		{
			icu::UnicodeString regexp = "item\\s*\\[\\d+]";
			icu::RegexMatcher *matcher = new icu::RegexMatcher(regexp, 0, status);
			if(U_FAILURE(success))
			{
				ROOTS_LOGGER_ERROR("the regular expression is not valid!");
				std::stringstream ss2;
				ss2 << "the regular expression is not valid (" << regexp << ")! \n";
				throw RootsException(__FILE__, __LINE__, ss2.str().c_str());
			}
			matcher->reset(unicodeLine);
			while(!infile.eof() && !matcher->find())
			{
				infile.getline(line,512);	
				unicodeLine = icu::UnicodeString(line);
				matcher->reset(unicodeLine);
			}
			delete matcher;
			matcher = NULL;
			
			if(!infile.eof())
			{						
				// lire le numero de l'item
			        unicodeLine = icu::RegexMatcher("\\s", unicodeLine, 0, status).replaceAll("", status);
				unicodeLine.remove(0, unicodeLine.indexOf("[")+1);
				unicodeLine.remove(unicodeLine.indexOf("]"), unicodeLine.length());
				
				form->parse(unicodeLine, fmtable, success);
				fmtable.getLong(success); /* Ignore the number */
				if (U_FAILURE(success))
				{
					ROOTS_LOGGER_ERROR("size value is not a valid number!");
					std::stringstream ss2;
					ss2 << "size value is not a valid number (" << unicodeLine << ")! \n";
					throw RootsException(__FILE__, __LINE__, ss2.str().c_str());
				}
				
				// Build a new item
				TextGridItem *item = new TextGridItem();	
				item->classname = NULL;
				item->name = NULL;	
				
				// read class
				infile.getline(line,512);
				unicodeLine = icu::UnicodeString(line);
				unicodeLine = icu::RegexMatcher("^\\s*", unicodeLine, 0, status).replaceAll("", status);
				if(unicodeLine.startsWith(icu::UnicodeString("class")) == true)
				{					
					unicodeLine.remove(0, unicodeLine.indexOf("=")+1);
					unicodeLine = icu::RegexMatcher("\"", unicodeLine, 0, status).replaceAll("", status);
					unicodeLine = icu::RegexMatcher("^\\s*", unicodeLine, 0, status).replaceAll("", status);
					unicodeLine = icu::RegexMatcher("\\s*$", unicodeLine, 0, status).replaceAll("", status);
					item->classname = new icu::UnicodeString(unicodeLine);
				}
				else
				{									
					std::stringstream ss2;
					ss2 << "expected class=\"<value>\" and found: " << unicodeLine << " ! \n";
					ROOTS_LOGGER_ERROR(ss2.str().c_str());
					throw RootsException(__FILE__, __LINE__, ss2.str().c_str());
				}
			
				// read name
				infile.getline(line,512);		
				ROOTS_LOGGER_DEBUG(line);
				unicodeLine = icu::UnicodeString(line);
				unicodeLine = icu::RegexMatcher("^\\s*", unicodeLine, 0, status).replaceAll("", status);
				if(unicodeLine.startsWith(icu::UnicodeString("name")) == true)
				{					
					unicodeLine.remove(0, unicodeLine.indexOf("=")+1);
					unicodeLine = icu::RegexMatcher("\"", unicodeLine, 0, status).replaceAll("", status);
					unicodeLine = icu::RegexMatcher("^\\s*", unicodeLine, 0, status).replaceAll("", status);
					unicodeLine = icu::RegexMatcher("\\s*$", unicodeLine, 0, status).replaceAll("", status);
					item->name = new icu::UnicodeString(unicodeLine);
				}
				else
				{									
					std::stringstream ss2;
					ss2 << "expected name=\"<value>\" and found: " << unicodeLine << " ! \n";
					ROOTS_LOGGER_ERROR(ss2.str().c_str());
					throw RootsException(__FILE__, __LINE__, ss2.str().c_str());
				}
				
				// read xmin
				infile.getline(line,512);		
				ROOTS_LOGGER_DEBUG(line);
				unicodeLine = icu::UnicodeString(line);
				unicodeLine = icu::RegexMatcher("\\s", unicodeLine, 0, status).replaceAll("", status);
				if(unicodeLine.startsWith(icu::UnicodeString("xmin")) == true)
				{					
					unicodeLine.remove(0, unicodeLine.indexOf("=")+1);
					form->parse(unicodeLine, fmtable, success);
					item->xmin = fmtable.getDouble(success);
					
					if (U_FAILURE(success))
					{
						ROOTS_LOGGER_ERROR("xmin value is not a valid number!");
						std::stringstream ss2;
						ss2 << "xmin value is not a valid number (" << unicodeLine << ")! \n";
						throw RootsException(__FILE__, __LINE__, ss2.str().c_str());
					}
				}
				else
				{									
					std::stringstream ss2;
					ss2 << "expected xmin=\"<value>\" and found: " << unicodeLine << " ! \n";
					ROOTS_LOGGER_ERROR(ss2.str().c_str());
					throw RootsException(__FILE__, __LINE__, ss2.str().c_str());
				}
				
				// read xmax
				infile.getline(line,512);		
				ROOTS_LOGGER_DEBUG(line);
				unicodeLine = icu::UnicodeString(line);
				unicodeLine = icu::RegexMatcher("\\s", unicodeLine, 0, status).replaceAll("", status);
				if(unicodeLine.startsWith(icu::UnicodeString("xmax")) == true)
				{					
					unicodeLine.remove(0, unicodeLine.indexOf("=")+1);
					form->parse(unicodeLine, fmtable, success);
					item->xmax = fmtable.getDouble(success);
					
					if (U_FAILURE(success))
					{
						ROOTS_LOGGER_ERROR("xmax value is not a valid number!");
						std::stringstream ss2;
						ss2 << "xmax value is not a valid number (" << unicodeLine << ")! \n";
						throw RootsException(__FILE__, __LINE__, ss2.str().c_str());
					}
				}
				else
				{									
					std::stringstream ss2;
					ss2 << "expected xmax=\"<value>\" and found: " << unicodeLine << " ! \n";
					ROOTS_LOGGER_ERROR(ss2.str().c_str());
					throw RootsException(__FILE__, __LINE__, ss2.str().c_str());
				}
				
				// intervals size
				int intervalsSize = -1;
				infile.getline(line,512);
				unicodeLine = icu::UnicodeString(line);
				unicodeLine = icu::RegexMatcher("\\s", unicodeLine, 0, status).replaceAll("", status);
				if(unicodeLine.startsWith(icu::UnicodeString("intervals")) == true)
				{					
					unicodeLine.remove(0, unicodeLine.indexOf("=")+1);
					form->parse(unicodeLine, fmtable, success);
					intervalsSize = fmtable.getLong(success);
					
					if (U_FAILURE(success))
					{
						ROOTS_LOGGER_ERROR("intervals size value is not a valid number!");
						std::stringstream ss2;
						ss2 << "intervals size value is not a valid number (" << unicodeLine << ")! \n";
						throw RootsException(__FILE__, __LINE__, ss2.str().c_str());
					}
				}
                else if (unicodeLine.startsWith(icu::UnicodeString("points")) == true)
                {
					unicodeLine.remove(0, unicodeLine.indexOf("=")+1);
					form->parse(unicodeLine, fmtable, success);
					intervalsSize = fmtable.getLong(success);
					
					if (U_FAILURE(success))
					{
						ROOTS_LOGGER_ERROR("points size value is not a valid number!");
						std::stringstream ss2;
						ss2 << "intervals size value is not a valid number (" << unicodeLine << ")! \n";
						throw RootsException(__FILE__, __LINE__, ss2.str().c_str());
					}
                }
				else
				{									
					std::stringstream ss2;
					ss2 << "expected intervals: size=\"<value>\" and found: " << unicodeLine << " ! \n";
					ROOTS_LOGGER_ERROR(ss2.str().c_str());
					throw RootsException(__FILE__, __LINE__, ss2.str().c_str());
				}
				
				if(intervalsSize != -1)
				{
					item->elements.reserve(intervalsSize);
					
					bool endOfInterval = false;
					int intervalNumber = -1;
					infile.getline(line,512);
					unicodeLine = icu::UnicodeString(line);
					
					while(!endOfInterval && !infile.eof())
					{
					        unicodeLine = icu::RegexMatcher("^\\s*", unicodeLine, 0, status).replaceAll("", status);
						
						if(unicodeLine.startsWith(icu::UnicodeString("item")) == true)
						{
							endOfInterval = true;
						}
						else
						{							
							// read interval number						
                            if ((unicodeLine.startsWith(icu::UnicodeString("intervals")) == true) ||
                                (unicodeLine.startsWith(icu::UnicodeString("points")) == true))
							{	
							        unicodeLine = icu::RegexMatcher("\\s", unicodeLine, 0, status).replaceAll("", status);				
								unicodeLine.remove(0, unicodeLine.indexOf("[")+1);
								unicodeLine.remove(unicodeLine.indexOf("]"), unicodeLine.length());
								
								form->parse(unicodeLine, fmtable, success);
								intervalNumber = fmtable.getLong(success);
								if (U_FAILURE(success))
								{
									ROOTS_LOGGER_ERROR("interval or point index value is not a valid number!");
									std::stringstream ss2;
									ss2 << "interval or point index value is not a valid number (" << unicodeLine << ")! \n";
									throw RootsException(__FILE__, __LINE__, ss2.str().c_str());
								}
								
								if(element!=NULL) 
								{
									item->elements.push_back(element);
									//delete element;
								}
								element = new TextGridElement();
								element->xmin = -1;
								element->xmax = -1;
								element->text = NULL;
							}
							
							if(intervalNumber == -1)
							{
								ROOTS_LOGGER_ERROR("missing interval index!");
								std::stringstream ss2;
								ss2 << "missing interval index! \n";
								throw RootsException(__FILE__, __LINE__, ss2.str().c_str());
							}
		
							
							// read xmin
							if(unicodeLine.startsWith(icu::UnicodeString("xmin")) == true)
							{				
							        unicodeLine = icu::RegexMatcher("\\s", unicodeLine, 0, status).replaceAll("", status);					
								unicodeLine.remove(0, unicodeLine.indexOf("=")+1);
								form->parse(unicodeLine, fmtable, success);
								element->xmin = fmtable.getDouble(success);
								
								if (U_FAILURE(success))
								{
									ROOTS_LOGGER_ERROR("xmin value is not a valid number!");
									std::stringstream ss2;
									ss2 << "xmin value is not a valid number (" << icu::UnicodeString(unicodeLine) << ")! \n";
									throw RootsException(__FILE__, __LINE__, ss2.str().c_str());
								}
							}
							
							// read xmax
							if(unicodeLine.startsWith(icu::UnicodeString("xmax")) == true)
							{					
							        unicodeLine = icu::RegexMatcher("\\s", unicodeLine, 0, status).replaceAll("", status);
								unicodeLine.remove(0, unicodeLine.indexOf("=")+1);
								form->parse(unicodeLine, fmtable, success);
								element->xmax = fmtable.getDouble(success);
								
								if (U_FAILURE(success))
								{
									ROOTS_LOGGER_ERROR("xmax value is not a valid number!");
									std::stringstream ss2;
									ss2 << "xmax value is not a valid number (" << unicodeLine << ")! \n";
									throw RootsException(__FILE__, __LINE__, ss2.str().c_str());
								}
							}


							// read time (points)
							if(unicodeLine.startsWith(icu::UnicodeString("time")) == true)
							{					
							        unicodeLine = icu::RegexMatcher("\\s", unicodeLine, 0, status).replaceAll("", status);
								unicodeLine.remove(0, unicodeLine.indexOf("=")+1);
								form->parse(unicodeLine, fmtable, success);
								element->xmin = fmtable.getDouble(success);
								element->xmax = fmtable.getDouble(success);
								
								if (U_FAILURE(success))
								{
									ROOTS_LOGGER_ERROR("time value is not a valid number!");
									std::stringstream ss2;
									ss2 << "time value is not a valid number (" << unicodeLine << ")! \n";
									throw RootsException(__FILE__, __LINE__, ss2.str().c_str());
								}
							}
					
							// read text
							if(unicodeLine.startsWith(icu::UnicodeString("text")) == true)
							{					
								unicodeLine.remove(0, unicodeLine.indexOf("=")+1);
								unicodeLine = icu::RegexMatcher("\"", unicodeLine, 0, status).replaceAll("", status);
								unicodeLine = icu::RegexMatcher("^\\s*", unicodeLine, 0, status).replaceAll("", status);
								unicodeLine = icu::RegexMatcher("\\s*$", unicodeLine, 0, status).replaceAll("", status);
								//unicodeLine = icu::RegexMatcher("\\|", unicodeLine, 0, status).replaceAll(" ", status);
								element->text = new icu::UnicodeString(unicodeLine);
							}
					
							// read mark
							if(unicodeLine.startsWith(icu::UnicodeString("mark")) == true)
							{					
								unicodeLine.remove(0, unicodeLine.indexOf("=")+1);
								unicodeLine = icu::RegexMatcher("\"", unicodeLine, 0, status).replaceAll("", status);
								unicodeLine = icu::RegexMatcher("^\\s*", unicodeLine, 0, status).replaceAll("", status);
								unicodeLine = icu::RegexMatcher("\\s*$", unicodeLine, 0, status).replaceAll("", status);
								//unicodeLine = icu::RegexMatcher("\\|", unicodeLine, 0, status).replaceAll(" ", status);
								element->text = new icu::UnicodeString(unicodeLine);
							}
							
							infile.getline(line,512);
							unicodeLine = icu::UnicodeString(line);
						}
					}
					if(element!=NULL) 
					{
						item->elements.push_back(element);
						element = NULL;
					}			
				}
				
				result->items.push_back(item);
			} // if not eof			
		}
	}
	else
	{
		ROOTS_LOGGER_ERROR("type of file unknown or not implemented!");				
		std::stringstream ss2;
		ss2 << "type of file unknown or not implemented! \n";
		throw RootsException(__FILE__, __LINE__, ss2.str().c_str());
	}
	
	infile.close();
	
	return result;	
}
	

void TextGrid::save(TextGridFile *content, const std::string& filename, std::locale _locale)
{	
	std::ofstream outfile;
	std::stringstream ss;
	UErrorCode success = U_ZERO_ERROR;
	icu::NumberFormat* form = icu::NumberFormat::createInstance(icu::Locale(icu::Locale::getEnglish()), success);
	icu::UnicodeString number;
				
	if(filename != "") this->filename = filename;
	if(_locale != std::locale("")) this->locale = _locale;
	
	ss << this->filename;

	outfile.open(ss.str().c_str());
	if(outfile.fail())
	{		
		std::stringstream ss2;
		ss2 << "can't open file \"" << ss.str() << "\" in write mode! \n";
		ROOTS_LOGGER_ERROR(ss2.str().c_str());
		throw RootsException(__FILE__, __LINE__, ss2.str());
	}
	
	form->setGroupingUsed(false);
	
	outfile.imbue(this->locale);
	
	outfile.setf(std::ios::fixed,std::ios::floatfield);

	
	outfile << "File type = \"" << content->text_type << "\"" << std::endl;
	outfile << "Object class = \"" << "TextGrid" << "\"" << std::endl;
	outfile << std::endl;
	outfile << "xmin = " << form->format(content->xmin, number) << std::endl;
	number = "";
	outfile << "xmax = " << form->format(content->xmax, number) << std::endl;
	number = "";
	outfile << "tiers? <exists>" << std::endl;
	outfile << "size = " << form->format((int) content->items.size(), number) << std::endl;
	number = "";
	outfile << "item []: " << std::endl;
	for(unsigned int itemIndex=0; itemIndex < content->items.size(); ++itemIndex)
	{
		outfile << "    item [" << form->format((int)(itemIndex+1), number)<< "]:" << std::endl;
		number = "";
		outfile << "        classname = \"" << *(content->items[itemIndex]->classname) << "\"" << std::endl;
		outfile << "        name = \"" << *(content->items[itemIndex]->name) << "\"" << std::endl;
		outfile << "        xmin = " << form->format(content->items[itemIndex]->xmin, number) << std::endl;
		number = "";
		outfile << "        xmax = " << form->format(content->items[itemIndex]->xmax, number) << std::endl;
		number = "";
		outfile << "        intervals: size = " << form->format((int) (content->items[itemIndex]->elements.size()), number) << std::endl;
		number = "";
		for(unsigned int elementIndex=0; elementIndex < content->items[itemIndex]->elements.size(); ++elementIndex)
		{
			outfile << "            intervals [" << form->format((int)(elementIndex+1), number) << "]:" << std::endl;
			number = "";
			outfile << "                xmin = " << form->format(content->items[itemIndex]->elements[elementIndex]->xmin, number) << std::endl;
			number = "";
			outfile << "                xmax = " << form->format(content->items[itemIndex]->elements[elementIndex]->xmax, number) << std::endl;
			number = "";
			outfile << "                text = \"" << *(content->items[itemIndex]->elements[elementIndex]->text) << "\"" << std::endl;			
		}
	}
	 
	outfile.close();	
}

TextGridFile *TextGrid::extract_info_between_start_and_end(TextGridFile *content, double start, double end, bool removeOffset)
{
	TextGridFile *output;
	
	if(start < 0.0) start = 0.0;
	
	if(start>=end)
	{
		return NULL;
	}
	
	output = new TextGridFile();
	output->text_type = content->text_type;
	if(removeOffset)
	{
		output->xmin = 0;
		output->xmax = end - start;
	}
	else
	{
		output->xmin = start;
		output->xmax = end;	
	}
	for(unsigned int itemIndex=0; itemIndex < content->items.size(); ++itemIndex)
	{
		TextGridItem *item = new TextGridItem();
		item->classname = new icu::UnicodeString(*(content->items[itemIndex]->classname));
		item->name = new icu::UnicodeString(*(content->items[itemIndex]->name));
		if(removeOffset)
		{	
			item->xmin = 0;
			item->xmax = end - start;
		}
		else
		{
			item->xmin = start;
			item->xmax = end;
		}
		TextGridElement *element = NULL;
		
		for(unsigned int elementIndex=0; elementIndex < content->items[itemIndex]->elements.size(); ++elementIndex)
		{
			if(content->items[itemIndex]->elements[elementIndex]->xmin >= start && 
				content->items[itemIndex]->elements[elementIndex]->xmax <= end)
			{	
				element = new TextGridElement();
				if(removeOffset)
				{	
					element->xmin = content->items[itemIndex]->elements[elementIndex]->xmin - start;
					element->xmax = content->items[itemIndex]->elements[elementIndex]->xmax - start;
				}
				else
				{
					element->xmin = content->items[itemIndex]->elements[elementIndex]->xmin;
					element->xmax = content->items[itemIndex]->elements[elementIndex]->xmax;	
				}
				element->text = new icu::UnicodeString(*(content->items[itemIndex]->elements[elementIndex]->text));
				item->elements.push_back(element);
			}
		}
		
		if(item->elements.size() == (unsigned)0)
		{
			delete item->classname;
			delete item->name;
			delete item;
			item = NULL;
		}
		else
		{
			output->items.push_back(item);
		}
	}
	
	return output;
}

int TextGrid::get_tier_index(TextGridFile *content, std::string tiername)
{
	unsigned int tierindex = 0;
	while((tierindex < content->items.size()) 
		&& (icu::UnicodeString(tiername.c_str()) != *(content->items[tierindex]->name)))
	{	
		++tierindex;
	}
	if(tierindex >= content->items.size())
	{
		ROOTS_LOGGER_ERROR("tier name not found in structure!");
		return -1;
	}
	
	return tierindex;
}

void TextGrid::delete_structure(TextGridFile **content)
{
	TextGridFile *structure = *content;
	for(unsigned int itemIndex=0; itemIndex < structure->items.size(); ++itemIndex)
	{
		for(unsigned int elementIndex=0; elementIndex < structure->items[itemIndex]->elements.size(); ++elementIndex)
		{
			delete structure->items[itemIndex]->elements[elementIndex]->text;
			delete structure->items[itemIndex]->elements[elementIndex];
		}
		delete structure->items[itemIndex]->classname;
		delete structure->items[itemIndex]->name;
		delete structure->items[itemIndex];
	}
	delete structure;	
	*content = NULL;
}

	

} } }

