/*
    This file is part of Roots.

    Roots is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Roots is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Roots.  If not, see <http://www.gnu.org/licenses/>.

    Copyright (c) 2013, 2014 IRISA
*/


#include "Pitchmark.h"

#include <boost/regex.hpp>
#include <boost/lexical_cast.hpp>

using namespace roots;

namespace roots {
  namespace file {
    namespace pitchmark {

      std::map<file_format_t, std::string>	Pitchmark::fileFormatToString = boost::assign::map_list_of
	(FILE_FORMAT_PITCHMARK_UNDEF, "undef")
	(FILE_FORMAT_PITCHMARK_RAW_SAMPLE, "rawsample")
	(FILE_FORMAT_PITCHMARK_RAW_TIME, "rawtime")
	;

      std::map<std::string, file_format_t>	Pitchmark::stringToFileFormat = boost::assign::map_list_of
	("undef", FILE_FORMAT_PITCHMARK_UNDEF)
	("rawsample", FILE_FORMAT_PITCHMARK_RAW_SAMPLE)
	("rawtime", FILE_FORMAT_PITCHMARK_RAW_TIME)
	;

      Pitchmark::Pitchmark(const std::string& filename, const file_format_t aFormat, std::locale _locale)
      {
	set_file_name(filename);
	set_file_format(aFormat);

	set_is_opened(false);
	set_is_loaded(false);

	this->locale = _locale;

	unload();
      }

      Pitchmark::Pitchmark(const Pitchmark &pmk)
      {

	set_file_name(pmk.get_file_name());
	set_file_format(pmk.get_file_format());

	set_is_opened(false);
	set_is_loaded(pmk.is_loaded());

	this->values = pmk.values;

	this->locale = pmk.locale;
	this->startSampleLoaded= pmk.startSampleLoaded;
      }

      Pitchmark::~Pitchmark()
      {
	close();
      }

      Pitchmark& Pitchmark::operator=(const Pitchmark& pmk)
      {
	set_file_name(pmk.get_file_name());
	set_file_format(pmk.get_file_format());

	set_is_opened(false);
	set_is_loaded(pmk.is_loaded());

	this->values = pmk.values;

	this->locale = pmk.locale;
	this->startSampleLoaded= pmk.startSampleLoaded;
	return *this;
      }

      void Pitchmark::set_file_name(const std::string& aFileName)
      {
	this->fileName = aFileName;
      }

      const std::string &Pitchmark::get_file_name() const
      {
	return this->fileName;
      }

      void Pitchmark::set_file_format(file_format_t aFormat)
      {
	this->fileFormat = aFormat;
      }

      file_format_t Pitchmark::get_file_format() const
      {
	return this->fileFormat;
      }

      const std::string &Pitchmark::get_file_format_name() const
      {
	return fileFormatToString[this->fileFormat];
      }

      void Pitchmark::set_is_opened(bool flagIsOpened)
      {
	this->isOpened = flagIsOpened;
      }

      bool Pitchmark::is_opened() const
      {
	return this->isOpened;
      }

      void Pitchmark::set_is_loaded(bool flagIsLoaded)
      {
	this->isLoaded = flagIsLoaded;
      }

      bool Pitchmark::is_loaded() const
      {
	return this->isLoaded;
      }

      void Pitchmark::open()
      {
	open(std::locale(""));
      }

      void Pitchmark::open(std::locale _locale)
      {
	close();

	if(_locale != std::locale(""))
	  { this->locale = _locale; }

	infile.open(get_file_name().c_str());
	if(infile.fail())
	  {
	    std::cerr<<"opening file failed!"<<std::endl;
	    fflush(stderr);
	    std::stringstream ss2;
	    ss2 << "opening file \"" << fileName << "\" failed! \n";
	    throw RootsException(__FILE__, __LINE__, ss2.str().c_str());
	  }

	infile.imbue(this->locale);

	set_is_opened(true);
      }

      void Pitchmark::close()
      {
	if(is_opened())
	  {
	    infile.close();
	  }
      }

      long Pitchmark::load_rawsample_file(long startSample,
					  long endSample,
					  int samplingFrequency)
      {
	return load_rawtime_file(startSample, endSample, (double)samplingFrequency);
      }

      long Pitchmark::load_rawtime_file(long startSample, long endSample, double normParameter)
      {
	bool finished = false;
	std::string line;
	long nValues = 0;
	boost::regex re("\\s+");

	// skip the first samples until startSample
	long skipIndex = 0;
	while(skipIndex < startSample && !infile.eof())
	  {
	    getline(infile, line, '\n');
	    skipIndex++;
	  }

	// read the samples requested in the range [startSample,endSample)
	while(!infile.eof() && !finished)
	  {
	    std::vector<std::string> linesplit;
	    getline(infile, line, '\n');

	    if(line != "")
	      {
		// split the line into fields
		boost::sregex_token_iterator i(line.begin(), line.end(), re, -1);
		boost::sregex_token_iterator j;
		while (i != j) {
		  linesplit.push_back(*i++);
		}

		if(linesplit.size() == 2)
		  {
		    values[boost::lexical_cast<long double>(linesplit[0]) / (long double)normParameter] =
		      boost::lexical_cast<long double>(linesplit[1]);
		  }

		// if all samples have been trated
		if(nValues == endSample)
		  {
		    finished = true;
		  }

		nValues++;
	      }
	  }

	this->startSampleLoaded= startSample;
	return values.size();
      }

      void Pitchmark::unload()
      {
	values.clear();
	startSampleLoaded=-1;
      }

      long Pitchmark::load(long startSample, long endSample, int samplingFrequency)
      {
	long nValues = 0;

	if(!this->is_opened())
	  { this->open(); }

	unload();

	if(this->get_file_format() == FILE_FORMAT_PITCHMARK_RAW_SAMPLE)
	  {
	    if(samplingFrequency == -1)
	      {
		std::stringstream ss;
		ss << "Sampling frequency is not defined for <" << this->get_file_name() << "> in raw format";
		throw RootsException(__FILE__,__LINE__, ss.str());
	      }
	    nValues = load_rawsample_file(startSample, endSample, samplingFrequency);
	  }
	else if(this->get_file_format() == FILE_FORMAT_PITCHMARK_RAW_TIME)
	  {
	    if(samplingFrequency != -1)
	      {
		ROOTS_LOGGER_WARN("Pitchmark::load, sampling frequency set but unused for raw time type file");
	      }
	    nValues = load_rawtime_file(startSample, endSample);
	  }
	else
	  {
	    std::stringstream ss;
	    ss << "unknown file format <" << this->get_file_format() << ">";
	    throw RootsException(__FILE__,__LINE__, ss.str());
	  }

	set_is_loaded(true);

	this->close();

	return nValues;
      }

      /*****************************************/
      template<class Inserter, class Key, class Value>
      class FirstInserter
      {
      public:
	FirstInserter(Inserter insert):insert(insert){};
	void operator=(std::pair<Key, Value> p){insert = p.first;}
	Inserter & insert;
      };

      template<class Inserter, class Key, class Value>
      class SecondInserter
      {
      public:
	SecondInserter(Inserter insert):insert(insert){};
	void operator=(std::pair<Key, Value> p){insert = p.second;}
	Inserter & insert;
      };

      long Pitchmark::check_convert_index(long valueIndex)
      {
	if(!this->is_loaded())
	  { load(); }

	long localValueIndex = valueIndex - startSampleLoaded;
	localValueIndex = std::max(std::min( localValueIndex, (long) (startSampleLoaded + get_n_values_loaded() -1)), (long) 0);
	/*
	  if((localValueIndex<0)||
	  (localValueIndex >= (this->startSampleLoaded + this->get_n_values_loaded())))
	  {
	  std::stringstream ss;
	  ss << "sample index " << localValueIndex << " is out of range [" << this->startSampleLoaded << "," << (this->startSampleLoaded+this->get_n_values_loaded()) << ")";
	  throw RootsException(__FILE__,__LINE__, ss.str());
	  }
	*/
	return localValueIndex;
      }


      /*******************************************/

      template<class Inserter>
      void Pitchmark::extract_from_index_range(Inserter & insert,
					       long startIndex,
					       long endIndex
					       )
      {
	int localStart = check_convert_index(startIndex);
	if(endIndex==-1)
	  { endIndex = get_n_values_loaded() + localStart -1; }
	int localEnd   = check_convert_index(endIndex);

	std::map<double,double>::const_iterator it = values.begin();
	for(int i = 0; i <= localEnd; ++i, ++it)
	  {
	    if(i >= localStart)
	      {insert = *it;}
	  }
      }

      std::vector<double> Pitchmark::get_times(long startSample, long endSample)
      {
	if(!this->is_loaded())
	  { load(); }

	std::vector<double> res;
	// the C++11 auto keyword is expected here !
	FirstInserter<std::back_insert_iterator<std::vector<double> >, double, double >
	  fi(std::back_inserter(res));
	extract_from_index_range(fi, startSample, endSample);
	return res;
      }

      std::vector<double> Pitchmark::get_values(long startSample, long endSample)
      {
	if(!this->is_loaded())
	  { load(); }

	std::vector<double> res;
	// the C++11 auto keyword is expected here !
	SecondInserter<std::back_insert_iterator<std::vector<double> >, double, double >
	  si(std::back_inserter(res));
	extract_from_index_range(si, startSample, endSample);
	return res;
      }

      std::vector<std::pair<double, double> >
      Pitchmark::get_times_and_values(long startSample, long endSample)
      {
	if(!this->is_loaded())
	  { load(); }

	std::vector<std::pair<double, double> > res;
	std::insert_iterator<std::vector<std::pair<double, double> > > inserter =
	  std::inserter(res, res.end());
	extract_from_index_range(inserter , startSample, endSample);
	return res;
      }

      double Pitchmark::get_value(long valueIndex)
      {
	if(!this->is_loaded())
	  { load(); }

	return get_values(valueIndex, valueIndex).front();
      }

      double Pitchmark::get_time(long valueIndex)
      {
	if(!this->is_loaded())
	  { load(); }

	return get_times(valueIndex, valueIndex).front();
      }

      std::pair<double, double> Pitchmark::get_time_and_value(long valueIndex)
      {
	if(!this->is_loaded())
	  { load(); }

	return get_times_and_values(valueIndex, valueIndex).front();
      }

      /*******************************************/

      template<class Inserter>
      void Pitchmark::extract_from_time_range(Inserter & insert,
					      double startTime,
					      double endTime,
					      roots::Approx approxStart,
					      roots::Approx approxEnd
					      )
      {
	if(endTime==-1)
	  { endTime = values.rbegin()->first + 1;}

	/* Set start iterator according to approximation policy */
	std::map<double, double, roots::TimeUtils::less>::const_iterator it = values.lower_bound(startTime);
	std::map<double, double, roots::TimeUtils::less>::const_iterator tmp;
	std::stringstream ss;
	switch(approxStart)
	  {
	  case APPROX_FLOOR:
	    if(TimeUtils::TimeUtils::greater(it->first,startTime) &&
	       (it != values.begin()))
	      {--it;}
	    break;
	  case APPROX_CEIL : // Nothing to do !
	    break;
	  case APPROX_ROUND :
	    if(it != values.begin())
	      {
		tmp = it; tmp--;
		if(TimeUtils::TimeUtils::less_equal((startTime - tmp->first) ,
					 (it->first - startTime)))
		  {--it;}
	      }
	    break;
	  case APPROX_STRICT_FLOOR :
	    if(TimeUtils::TimeUtils::greater_equal(it->first, startTime)
	       && (it != values.begin()))
	      {--it;}
	    break;
	  case APPROX_STRICT_CEIL :
	    if(TimeUtils::TimeUtils::equal_to(it->first, startTime) &&
	       (it != values.end()))
	      {++it;}
	    break;
	  case APPROX_UNDEF :
	  default:
	    ss << "Pitchmark::extract_from_time_range: Unknow or undef approximation policy!";
	    throw RootsException(__FILE__, __LINE__, ss.str().c_str());
	  }

	/* Insert safe value */
	double diff = 0;
	while((it != values.end()) && 
	      TimeUtils::TimeUtils::less(it->first, endTime))
	  {
	    insert = *it;
	    diff = endTime - it->first; // needed if APPROX_ROUND end policy
	    ++it;
	  }

	/* Look at problematic end values according to the approximation end policy */
	if(it != values.end())
	  {
	    switch(approxEnd)
	      {
	      case APPROX_FLOOR:
		if(TimeUtils::TimeUtils::equal_to(it->first, endTime))
		  { insert = *it; }
		break;
	      case APPROX_CEIL :
		insert = *it;  // Insert in all case (safe since it is not .end())
		break;
	      case APPROX_ROUND :
		if(TimeUtils::TimeUtils::greater_equal(diff , (it->first - endTime)))
		  { insert = *it; }
		break;
	      case APPROX_STRICT_FLOOR : // Nothing to do !
		break;
	      case APPROX_STRICT_CEIL :
		insert = *it; // Insert in all case (safe since it is not .end())
		if(TimeUtils::TimeUtils::equal_to(it->first, endTime))
		  {
		    ++it;
		    if(it != values.end())
		      { insert = *it; }
		  }
		break;
	      case APPROX_UNDEF :
	      default:
		ss << "Pitchmark::extract_from_time_range: Unknow or undef approximation policy!";
		throw RootsException(__FILE__, __LINE__, ss.str().c_str());
	      }
	  }
      }

      double Pitchmark::get_value_from_time(double time, roots::Approx approx)
      {
	if(!this->is_loaded())
	  { load(); }

	return get_values_from_time(time, time, approx, approx).front();
      }

      double Pitchmark::get_time_from_time(double time, roots::Approx approx)
      {
	if(!this->is_loaded())
	  { load(); }

	return get_times_from_time(time, time, approx, approx).front();
      }

      std::pair<double, double>
      Pitchmark::get_time_and_value_from_time(double time,
					      roots::Approx approx)
      {
	if(!this->is_loaded())
	  { load(); }

	return get_times_and_values_from_time(time, time, approx, approx).front();
      }

      std::vector<double> Pitchmark::get_times_from_time(double startTime,
							 double endTime,
							 roots::Approx approxStart,
							 roots::Approx approxEnd)
      {
	std::vector<double> res;
	if(!this->is_loaded())
	  { load(); }

	// the C++11 auto keyword is expected here !
	FirstInserter<std::back_insert_iterator<std::vector<double> >, double, double >
	  fi(std::back_inserter(res));
	extract_from_time_range(fi, startTime, endTime, approxStart, approxEnd);
	return res;
      }

      std::vector<double> Pitchmark::get_values_from_time(double startTime,
							  double endTime,
							  roots::Approx approxStart,
							  roots::Approx approxEnd)
      {
	if(!this->is_loaded())
	  { load(); }

	std::vector<double> res;
	// the C++11 auto keyword is expected here !
	SecondInserter<std::back_insert_iterator<std::vector<double> >, double, double >
	  si(std::back_inserter(res));
	extract_from_time_range(si, startTime, endTime, approxStart, approxEnd);
	return res;
      }

      std::vector<std::pair<double, double> >
      Pitchmark::get_times_and_values_from_time(double startTime,
						double endTime,
						roots::Approx approxStart,
						roots::Approx approxEnd)
      {
	if(!this->is_loaded())
	  { load(); }

	std::vector<std::pair<double, double> > res;
	std::insert_iterator<std::vector<std::pair<double, double> > > inserter =
	  std::inserter(res, res.end());
	extract_from_time_range(inserter, startTime, endTime, approxStart, approxEnd);
	return res;
      }


      std::pair<long, double> Pitchmark::get_lower_bound_index(double timeValue)
      {
	if(!this->is_loaded())
	  { load(); }

	std::map<double, double, roots::TimeUtils::less>::const_iterator it = values.lower_bound(timeValue);

	return std::make_pair((it->second) +  startSampleLoaded, it->first);
      }

      /*******************************************/

      void Pitchmark::set_locale(const std::locale& locale)
      {
	this->locale = locale;
      }

      const std::locale& Pitchmark::get_locale() const
      {
	return locale;
      }

      long Pitchmark::get_n_values_loaded() const
      {
	return values.size();
      }

    }
  }
}

