/*
    This file is part of Roots.

    Roots is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Roots is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Roots.  If not, see <http://www.gnu.org/licenses/>.

    Copyright (c) 2013, 2014 IRISA
*/


#include "F0.h"

#include <boost/regex.hpp>
#include <boost/lexical_cast.hpp>

namespace roots
{
  namespace file
  {
    namespace f0
    {

      std::map<file_format_t, std::string>	F0::fileFormatToString = boost::assign::map_list_of
	(FILE_FORMAT_F0_UNDEF, "undef")
	(FILE_FORMAT_F0_RAW, "raw")
	(FILE_FORMAT_F0_WAVESURFER, "wavesurfer")
	;

      std::map<std::string, file_format_t>	F0::stringToFileFormat = boost::assign::map_list_of
	("undef", FILE_FORMAT_F0_UNDEF)
	("raw", FILE_FORMAT_F0_RAW)
	("wavesurfer", FILE_FORMAT_F0_WAVESURFER)
	;


      std::map<f0_unit_t, std::string>	F0::f0unitToString = boost::assign::map_list_of
	(F0_UNIT_HZ, "hertz")
	(F0_UNIT_LOG, "log")
	;

      std::map<std::string, f0_unit_t>	F0::stringToF0Unit = boost::assign::map_list_of
	("hertz", F0_UNIT_HZ)
	("log", F0_UNIT_LOG)
	;

      F0::F0(const std::string& filename, const file_format_t aFormat, std::locale _locale)
      {
	set_file_name(filename);
	set_file_format(aFormat);

	isOpened = false;
	isLoaded = false;

	startSampleLoaded = 0;
	locale = _locale;
	samplingFrequency = -1;
	timeOffset = 0;
	timeScalingFactor = roots::acoustic::Segment::TIME_UNIT_FACTOR_METRIC;
	f0Unit = F0_UNIT_HZ;
      }

      F0::F0(const F0 &f0)
      {
	set_file_name(f0.get_file_name());
	set_file_format(f0.get_file_format());

	isOpened = false;
	isLoaded = f0.isLoaded;
	values = f0.values;

	startSampleLoaded = f0.startSampleLoaded;
	locale = f0.locale;
	samplingFrequency = f0.samplingFrequency;
	timeOffset = f0.timeOffset;
	timeScalingFactor = f0.timeScalingFactor;
	f0Unit = f0.f0Unit;
      }

      F0::~F0()
      {
	close();
      }

      F0& F0::operator=(const F0& f0)
      {
	set_file_name(f0.get_file_name());
	set_file_format(f0.get_file_format());

	isOpened = false;
	isLoaded = f0.isLoaded;
	values = f0.values;

	startSampleLoaded = f0.startSampleLoaded;
	locale = f0.locale;
	samplingFrequency = f0.samplingFrequency;
	timeOffset = f0.timeOffset;
	timeScalingFactor = f0.timeScalingFactor;
	f0Unit = f0.f0Unit;
	return *this;
      }

      void F0::set_file_name(const std::string& aFileName)
      {
	this->fileName = aFileName;
      }

      const std::string &F0::get_file_name() const
      {
	return fileName;
      }

      void F0::set_file_format(file_format_t aFormat)
      {
	fileFormat = aFormat;
      }

      file_format_t F0::get_file_format() const
      {
	return fileFormat;
      }

      const std::string &F0::get_file_format_name() const
      {
	return fileFormatToString[fileFormat];
      }

      bool F0::is_opened() const
      {
	return isOpened;
      }

      bool F0::is_loaded() const
      {
	return isLoaded;
      }

      void F0::open()
      {
	open(std::locale(""));
      }

      void F0::open(std::locale _locale)
      {
	open(100, _locale);
      }

      void F0::open(int samplingFrequenc)
      {
	open(samplingFrequenc, std::locale(""));
      }

      void F0::open(int samplingFrequency, std::locale _locale)
      {
	close();

	if(_locale != std::locale(""))
	  {locale = _locale;}
	set_sampling_frequency(samplingFrequency);

	infile.open(get_file_name().c_str());
	if(infile.fail())
	  {
	    std::cerr<<"file not found!"<<std::endl;
	    fflush(stderr);
	    std::stringstream ss2;
	    ss2 << "opening file \"" << fileName << "\" failed! \n";
	    throw RootsException(__FILE__, __LINE__, ss2.str().c_str());
	  }

	infile.imbue(this->locale);
	isOpened = true;
      }

      void F0::close()
      {
	if(is_opened())
	  { infile.close(); }
      }

      long F0::load_raw_file(long startSample,
			     long endSample,
			     unsigned int nbExpectedFields,
			     unsigned int idF0field)
      {
	unload();

	bool finished = false;
	std::string line;
	long nValues = 0;
	boost::regex re("\\s+");

	if(this->get_sampling_frequency()==-1)
	  {
	    std::stringstream ss;
	    ss << "Sampling frequency is not defined for <" << this->get_file_name() << ">";
	    throw RootsException(__FILE__,__LINE__, ss.str());
	  }

	// skip the first samples until startSample
	long skipIndex = 0;
	while(skipIndex < startSample && !infile.eof())
	  {
	    getline(infile, line, '\n');
	    skipIndex++;
	  }

	// read the samples requested in the range [startSample,endSample)
	while(!infile.eof() && !finished)
	  {
	    std::vector<std::string> linesplit;
	    getline(infile, line, '\n');

	    if(line != "")
	      {
		// split the line into fields
		boost::sregex_token_iterator i(line.begin(), line.end(), re, -1);
		boost::sregex_token_iterator j;
		while (i != j)
		  {
		    linesplit.push_back(*i++);
		  }

		if(linesplit.size() == nbExpectedFields)
		  {
		    values.push_back(boost::lexical_cast<double>(linesplit[idF0field]));
		  }

		// if all samples have been trated
		if(nValues == endSample)
		  {
		    finished = true;
		  }

		nValues++;
	      }
	  }
	this->startSampleLoaded= startSample;

	return values.size();
      }

      void F0::unload()
      {
	values.clear();
	startSampleLoaded=-1;
      }

      long F0::load(long startSample, long endSample)
      {
	long nValues = 0;

	if(!this->is_opened())
	  { this->open(); }

	unload();

	try{
	  if(this->get_file_format() == FILE_FORMAT_F0_RAW)
	    {
	      nValues = load_raw_file(startSample, endSample, 1 , 0);
	    }
	  else if(this->get_file_format() == FILE_FORMAT_F0_WAVESURFER)
	    {
	      nValues = load_raw_file(startSample, endSample, 4, 0);
	    }
	  else
	    {
	      std::stringstream ss;
	      ss << "unknown file format <" << this->get_file_format() << ">";
	      throw RootsException(__FILE__,__LINE__, ss.str());
	    }
	}
	catch(RootsException e)
	  {
	    std::stringstream ss;
	    ss << e.what() << std::endl
	       << "Failed to load file <" << this->get_file_name()
	       << "> with format <" << this->get_file_format()	<<">";
	    throw RootsException(__FILE__, __LINE__,ss.str());
	  }

	isLoaded = true;

	this->close();

	return nValues;
      }

      /*****************************************/
      long F0::check_convert_index(long valueIndex)
      {
	if(!this->is_loaded())
	  { load(); }

	long localValueIndex = valueIndex - startSampleLoaded;
	localValueIndex = std::max(std::min( localValueIndex, (long) (startSampleLoaded + get_n_values_loaded() -1)), (long) 0);
	/*
	if((localValueIndex<0)||
	   (localValueIndex >= (this->startSampleLoaded + this->get_n_values_loaded())))
	  {
	    std::stringstream ss;
	    ss << "sample index " << localValueIndex << " is out of range [" << this->startSampleLoaded << "," << (this->startSampleLoaded+this->get_n_values_loaded()) << ")";
	    throw RootsException(__FILE__,__LINE__, ss.str());
	  }
	*/
	return localValueIndex;
      }


      long F0::convert_time_to_index( double time, roots::Approx approxPolicy)
      {
	double virtualIndex =  std::max((time - get_time_offset()) * 
					(double) get_sampling_frequency(), 0.0);

	if(roots::TimeUtils::TimeUtils::equal_to(std::floor(virtualIndex)/ (double) get_sampling_frequency(), time))
	  {virtualIndex = std::floor(virtualIndex);}
	else if(roots::TimeUtils::TimeUtils::equal_to(std::ceil(virtualIndex)/ (double) get_sampling_frequency(), time))
	  {virtualIndex = std::ceil(virtualIndex);}

	std::stringstream ss;
	switch(approxPolicy)
	  {
	  case APPROX_FLOOR:
	    return std::floor(virtualIndex);
	    break;
	  case APPROX_CEIL :
	    return std::ceil(virtualIndex);
	    break;
	  case APPROX_ROUND :
	    return std::floor(virtualIndex + 0.5);
	    break;
	  case APPROX_STRICT_FLOOR :
	    return std::max(std::ceil(virtualIndex - 1 ), 0.0);
	    break;
	  case APPROX_STRICT_CEIL :
	    return std::min(std::floor(virtualIndex + 1 ),
			    (double)(this->startSampleLoaded + this->get_n_values_loaded()));
	    break;
	  case APPROX_UNDEF :
	  default:
	    ss << "F0::convert_time_to_index: Unknow or undef approximation policy!";
	    throw RootsException(__FILE__, __LINE__, ss.str().c_str());
	  }
      }
      
     double F0::convert_index_to_time( long idx) const
      {
	return get_time_offset() + ((idx + startSampleLoaded) / get_sampling_frequency());
      }

      /*******************************************/

      std::vector<double> F0::get_values(long startSample, long endSample)
      {
	if(!this->is_loaded())
	  { load(); }

	int localStart = check_convert_index(startSample);
	if(endSample==-1)
	  { endSample = get_n_values_loaded() + localStart -1 ; }
	int localEnd = check_convert_index(endSample+1);

	return std::vector<double>(values.begin() + localStart, values.begin() + localEnd);
      }

      double F0::get_value(long valueIndex)
      {
	if(!this->is_loaded())
	  { load(); }

	return values[check_convert_index(valueIndex)];
      }

      /*******************************************/

      double F0::get_value_from_time(double time, roots::Approx approx)
      {
	if(!this->is_loaded())
	  { load(); }

	return get_value(convert_time_to_index(time, approx));
      }


      std::vector<double> F0::get_values_from_time(double startTime,
						   double endTime,
						   roots::Approx approxStart,
						   roots::Approx approxEnd)
      {
	if(!this->is_loaded())
	  { load(); }

	return get_values(convert_time_to_index(startTime, approxStart),
			  convert_time_to_index(endTime, approxEnd));
      }

      /*******************************************/

      void F0::set_sampling_frequency(int samplingFrequency)
      {
	unload();
	this->samplingFrequency = samplingFrequency;
      }

      void F0::set_locale(const std::locale& locale)
      {
	this->locale = locale;
      }

      void F0::set_time_offset(double timeOffset)
      {
	unload();
	this->timeOffset = timeOffset;
      }

      void F0::set_time_scaling_factor(double timeScalingFactor)
      {
	this->timeScalingFactor = timeScalingFactor;
      }

      void F0::set_f0_unit(f0_unit_t f0Unit)
      {
	this->f0Unit = f0Unit;
      }

      int F0::get_sampling_frequency() const
      {
	return samplingFrequency;
      }

      const std::locale& F0::get_locale() const
      {
	return locale;
      }

      double F0::get_time_offset() const
      {
	return timeOffset;
      }

      double F0::get_time_scaling_factor() const
      {
	return timeScalingFactor;
      }

      long F0::get_n_values_loaded() const
      {
	return values.size();
      }

      f0_unit_t F0::get_f0_unit() const
      {
	return f0Unit;
      }

    }
  }
}

