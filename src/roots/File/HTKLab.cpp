/*
    This file is part of Roots.

    Roots is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Roots is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Roots.  If not, see <http://www.gnu.org/licenses/>.

    Copyright (c) 2013, 2014 IRISA
*/


#include "HTKLab.h"


namespace roots { namespace file { namespace htk {

HTKLab::HTKLab(const std::string& filename, std::locale _locale)
{
	this->filename = filename;
	this->locale = _locale;
}

HTKLab::~HTKLab()
{
}

std::vector<HTKLabLine> * HTKLab::load(const std::string& filename, std::locale _locale)
{
	std::ifstream infile;
	std::stringstream ss;
	UErrorCode status = U_ZERO_ERROR;
	icu::RegexMatcher m("\\s", 0, status);
	const int maxFields = 6;
	icu::UnicodeString unicodeLine; //, unicodeLine2;
	icu::Formattable fmtable;
	UErrorCode success = U_ZERO_ERROR;
	icu::NumberFormat* form = icu::NumberFormat::createInstance(icu::Locale(icu::Locale::getEnglish()), success);

	std::vector<HTKLabLine> * resultVector = new std::vector<HTKLabLine>();
	
	if(filename != "") this->filename = filename;
	if(_locale != std::locale("")) this->locale = _locale;
	
	ss << this->filename;

	infile.open(ss.str().c_str());
	if(infile.fail())
	{
		ROOTS_LOGGER_ERROR("file not found!");
		std::stringstream ss2;
		ss2 << "file \"" << ss.str() << "\" not found! \n";
		throw RootsException(__FILE__, __LINE__, ss2.str().c_str());
	}
	
	
	infile.imbue(this->locale);
	
	char line[512];
	while(!infile.eof())
	{
		HTKLabLine current;
		icu::UnicodeString fields[maxFields];
		
		infile.getline(line,512);
		
		ROOTS_LOGGER_DEBUG(line);
		
		// split the line into fields
		unicodeLine = icu::UnicodeString(line);
		unicodeLine = icu::RegexMatcher("^\\s+", unicodeLine, 0, status)
				.replaceAll("", status);
		unicodeLine = icu::RegexMatcher("\\s+$", unicodeLine, 0, status)
						.replaceAll("", status);
		int nfields = icu::RegexMatcher("\\s+", 0, status).split(unicodeLine, fields, maxFields, status);
		if(nfields>2) // At least 3 fields on the line (start end label)
		{		
			// get values
			form->parse(icu::UnicodeString(fields[0]), fmtable, success);
			current.timeStart = fmtable.getDouble(success);
			if (U_FAILURE(success))
			{
				ROOTS_LOGGER_ERROR("start value is not a valid number!");
				std::stringstream ss2;
				ss2 << "start value is not a valid number (" << fields[0] << ")! \n";
				throw RootsException(__FILE__, __LINE__, ss2.str().c_str());
			}

			form->parse(fields[1], fmtable, success);
			current.timeEnd = fmtable.getDouble(success);
            if (U_FAILURE(success))
			{
				ROOTS_LOGGER_ERROR("end value is not a valid number!");
				std::stringstream ss2;
				ss2 << "end value is not a valid number! \n";
				throw RootsException(__FILE__, __LINE__, ss2.str().c_str());
			}
			
			current.label = fields[2];

			if(fields[4].isEmpty() && fields[5].isEmpty())
			{
				current.comment = fields[3];
			}
			else
			{
				form->parse(fields[3], fmtable, success);
				current.likelihood = fmtable.getDouble(success);
				if(success != U_ZERO_ERROR)
				{
					ROOTS_LOGGER_ERROR("likelihood value is not a valid number!");
					std::stringstream ss2;
					ss2 << "likelihood value is not a valid number! \n";
					throw RootsException(__FILE__, __LINE__, ss2.str().c_str());
				}
				current.label2 = fields[4];	
				current.comment = fields[5];
			}

			current.is_word = true;
			this->filter_line(&current);

			if(fields[2].compare("") != 0)
			{
				resultVector->push_back(current);
			}else{
				ROOTS_LOGGER_WARN("line skipped due to missing info!");
			}
		}
	}
	
	infile.close();
	
	return resultVector;	
}
	
void HTKLab::save(std::vector<HTKLabLine> *lines, const std::string& filename, std::locale _locale)
{
	std::ofstream outfile;
	std::stringstream ss;
	
	std::vector<HTKLabLine>::const_iterator iter = lines->begin();
	std::vector<HTKLabLine>::const_iterator end = lines->end();
	
	if(filename != "") this->filename = filename;
	if(_locale != std::locale("")) this->locale = _locale;
	
	ss << this->filename;
	outfile.open(ss.str().c_str());
	outfile.imbue(this->locale);
	
	while(iter != end)
	{
		outfile << (*iter).timeStart << " ";
		outfile << (*iter).timeEnd << " ";
		outfile << (*iter).label << " ";
		//outfile << (*iter).likelihood << " ";
		//outfile << (*iter).label2 << " ";
		outfile << (*iter).comment;
		outfile << std::endl;
		iter++;
	}
	
	outfile.close();
}
	
void HTKLab::filter_line(HTKLabLine *line)
{
	if((line->label == "start") || (line->label == "end") ||
		(line->label == "META_INSP") || (line->label == "META_I") ||
		(line->label == "META_PAUSE") )
	{
		line->is_word = false;
	}

	if((line->label2 == "START") || (line->label2 == "END") || (line->label2 == "_pause_") )
	{
		line->is_word = false;
	}
}

} } }

