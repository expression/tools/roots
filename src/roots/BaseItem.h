/*
    This file is part of Roots.

    Roots is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Roots is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Roots.  If not, see <http://www.gnu.org/licenses/>.

    Copyright (c) 2013, 2014 IRISA
*/

#ifndef BASE_ITEM_H_
#define BASE_ITEM_H_

#include "Base.h"                  // Base class: roots::Base
#include "SequenceLinkedObject.h"  // Base class: roots::SequenceLinkedObject

// #include "Sequence.h"

namespace roots {

namespace sequence {
class Sequence;
}

/**
 * @brief This is the base class of all items in ROOTS
 * @details
 * This class provides commons informations for all items.
 *
 * @author Cordial Group
 * @version 0.1
 * @date 2011
 */
class BaseItem : public roots::Base, public roots::SequenceLinkedObject {
 public:
  /**
   * @brief Default constructor
   *
   */
  BaseItem(const bool noInit = false);

  /**
   * @brief Default constructor
   * Initialize label to the empty string and also compute the timestamp and
   * generate an ID.
   * @param aLabel the label of the object
   */
  BaseItem(const std::string& aLabel);

  /**
   * @brief Copy constructor
   * @warning Do not copy backlinks
   */
  BaseItem(const BaseItem& baseI);

  /**
   * @brief Destructor
   */
  virtual ~BaseItem();

  /**
   * @brief clone the current default object
   * @warning Do not copy backlinks
   * @return a copy of the object
   */
  virtual BaseItem* clone() const;

  /**
   * @brief copy the given object into the current one
   * @param base source baseItem object
   * @return reference to the current object
   */
  BaseItem& operator=(const BaseItem& baseI);

  /**
   * @brief Returns the timestamp
   * @return the timestamp
   */
  virtual const std::string get_timestamp() const;

  /**
   * @brief Says if the item has a related sequence
   * @return Is the item has a related sequence ?
   */
  virtual bool has_related_sequence() const;

  /**
   * @brief Set the related sequence of the base item.
   *	  Do nothing if the item does not have related sequence.
   * @param A pointer to the related sequence.
   */
  virtual void set_related_sequence(sequence::Sequence* _relatedSequence);

  /**
   * @brief Get the item related sequence (if any)
   * @return A pointer to the related sequence if any, NULL elsewhere.
   */
  virtual sequence::Sequence* get_related_sequence() const;

  /**
   * @brief Modifies the current syllable to take into account the insertion of
   *an element into the related sequence
   * @param index index of the element added in the related sequence
   **/
  virtual void insert_related_element(int index);

  /**
   * @brief Modifies the current syllable to take into account the deletion of
   *an element into the related sequence
   * @param index index of the element removed from the related sequence
   **/
  virtual void remove_related_element(int index);

  /**
   * @brief Translate all internal indexes linked with embeded sequence
   * Use when the embeded sequence change size (example when the embeded
   * sequence is concatenated with another)
   * Nothing if item is simple
   */
  virtual void translate_index(int offset);

  /**
   * @brief Find items from a target sequence which are related to the current
   *item
   * @param targetLabel Label of the target sequence
   * @return Vector of related items (potentially none)
   **/
  std::vector<BaseItem*> get_related_items(
      const std::string& targetLabel) const;

  /**
   * @brief Find indices of items from a target sequence which are related to
   *the current item
   * @param targetLabel Label of the target sequence
   * @return Vector of related indices (potentially none)
   **/
  std::vector<int> get_related_indices(const std::string& targetLabel) const;

  /**
   * @brief Prepare an item to be saved in a directory. Basicaly, a signal
   * segment items will copy the associated wav file if target_base_dir change.
   * @Warning items may be changed (for instance the base directory of signal
   * segment items)!
   */
  virtual void prepare_save(const std::string& target_base_dir);

  /**
   * @brief Write the entity into a RootsStream
   * @param stream the stream from which we inflate the element
   * @param is_terminal indicates that the node is terminal (true by default)
   */
  virtual void deflate(RootsStream* stream, bool is_terminal = true,
                       int list_index = -1);

  /**
   * @brief Extracts the entity from the stream
   * @param stream the stream from which we inflate the element
   */
  virtual void inflate(RootsStream* stream, bool is_terminal = true,
                       int list_index = -1);

  /**
   * @brief Casts a BaseItem instance into one of his child class type
   * @return A pointer to cast object
   **/
  template <class U>
  U* as() {
    return static_cast<U*>(this);
  }

 protected:
 private:
};

}  // END OF NAMESPACE
#endif /* BASE_ITEM_H_ */
