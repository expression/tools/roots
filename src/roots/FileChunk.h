/*
    This file is part of Roots.

    Roots is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Roots is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Roots.  If not, see <http://www.gnu.org/licenses/>.

    Copyright (c) 2013, 2014 IRISA
*/


#ifndef FILECHUNK_H
#define FILECHUNK_H

#include "BaseChunk.h"
#include "Corpus.h"
#include "MemoryChunk.h"

#include "RootsStreamJson.h"

#ifdef USE_STREAM_XML
#include "RootsStreamXML.h"
#endif

namespace roots {

//   class Corpus;

  /**
   * @todo fonction to get a full utterance set (need a merge function)
   * @todo documentation
   **/
  class FileChunk : public BaseChunk
  {

    /* Constructor / Destructor / Copy */
  public:
    FileChunk();

    /**
     * @param[in] chunk_rel_path relative path from the current chunk to the target corpus. 
     * @param[in] corpus_abs_dir may be an absolute path of where is the current corpus containing this chunk.
     * @param[in] nbUtterances number of utterance in the target corpus. Optional but in this case, the target corpus will be loaded wich could be expensive.
     **/
    FileChunk(const std::string chunk_rel_path,
	      const std::string corpus_abs_dir = "",
	      const int nbUtterances = 0
	      );
    FileChunk(const std::map<std::string, std::string>& layersToFiles,
	      const std::string corpus_abs_dir = "",
	      const int nbUtterances = 0
	      );
    FileChunk(const MemoryChunk& chunk,
	      const std::map<std::string, std::string>& layersToFiles,
	      const std::string corpus_abs_dir = ""
	      );


    /**
     * @brief Destructor. Changes will be saved here to one or several temporary files.
     **/
    virtual ~FileChunk();
    virtual void destroy();

    FileChunk(const FileChunk& fchunk);
    FileChunk& operator= (const FileChunk& fchunk);
    FileChunk *clone() const;
    BaseChunk *clone_to_basechunk() const;

    /* Getters / Setters */
  public:

    void set_base_dir(const std::string& baseDir);

    const std::string& get_base_dir() const;

    /**
     * @brief Returns the number of utterances
     * @note Loads a file if needed
     */
    virtual int count_utterances() const;

    /**
     * @brief Update the number of utterances
     * @note may loads files
     */
    virtual void update_counts();

  protected:
    /**
     * @brief Update the number of utterances using a given layer
     * @note The layer must be loaded.
     */
    virtual void update_counts(int layerId);
    
    /* Utterance managment */
  public:
    /**
     * @brief Deletes an utterance (all layers)
     * @note Load all layers. Update utterance count.
     * @param id ID of the utterance to be removed. Default ID is 0 (first utterance).
     */
    virtual void delete_utterance(int id = 0);
    
    
    /**
     * @brief Removes all utterances and cleans the chunk
     */
    virtual void clear();

    /**
     * @brief Returns an utterance where all layers are merged
     * @warning Allocates a new full utterance, requires to free memory at some point
     * @return pointer of the loaded utterance
     */
    virtual roots::Utterance* get_utterance(int id = 0);

    /**
     * @brief Returns an utterance where all layers are merged
     * @warning Allocates a new full utterance, requires to free memory at some point
     * @return pointer of the loaded utterance
     */
    virtual roots::Utterance* get_utterance(const std::vector<std::string> & layerToLoad,
					    int id = 0);

    /**
     * @brief Adds a new utterance in the current file group, assuming there is no layer
     * @param utt pointer to the utterance
     */
    virtual void add_utterance(const Utterance &utt, 
							   const std::string layerName=BaseChunk::ANONYMOUS_LAYER); 
		//  throw(RootsException);

    /**
     * @brief Adds a new (sliced) utterance in the current file group
     * @param utt map giving names to utterance pointers (one utterance per layer)
     */
    virtual void add_utterance(const std::map<std::string, Utterance> & utt);
    //  throw(RootsException);

   /**
     * @brief Adds new utterances in the current file group, assuming there is no layer 
     * @param utt pointer to the utterance
     */
    virtual void add_utterances(const std::vector<Utterance>& utt,
								const std::string layerName=ANONYMOUS_LAYER); 
		//  throw(RootsException);

     /**
     * @brief Adds a new (sliced) utterance in the current file group
     * @param utt map giving layer names to utterances 
     */
    virtual void add_utterances(const std::map<std::string, std::vector<Utterance> >& utt);
		//  throw(RootsException);
    
    /**
     * @brief Replace a given utterance by another
     * @param id ID of the utterance to be replaced
     * @param utt the new utterance
     **/
    virtual void update_utterance(int id, const Utterance& utt);
    
    /**
     * @brief Replace a given utterance by another
     * @param id ID of the utterance to be replaced
     * @param utt the new utterance split into layers
     **/
    virtual void update_utterance(int id, const std::map<std::string, Utterance> &utt);

  protected:
    void load_in_utterance(const std::string & layerToAdd,
			   int index,
			   roots::Utterance *& utterance);
    
  
    /* Loading/Save functions */
    
  protected :

    void load_layer(const std::string& layerName);
    void load_layer(const int& id);
    void unload_layer(const std::string& layerName);
    void unload_layer(const int& id);
    bool is_loaded_layer(const std::string& layerName) const;
    bool is_loaded_layer(const int& layerId) const ;

    /**
     * @brief Prepare the file chunk to be saved into one or several temporary file chunks when autosaving. Precisely, generate a temporary filename, memorize it and memorize the "being backed up" status.
     * @note By default, backup is done is the same directory as the original file chunk.
     * @param id ID of the layer to be prepared for backup. By default, all layers are prepared.
     **/
    void prepare_backup(const int & id=-1);
    
    /**
     * @brief Remove or move temporary file chunks.
     * @param id Optional, id of the layer whose temporary file should be cleaned
     * @param dir_to_move Optional, directory where temporary file chunks should be moved. By default, temporary files are deleted.
     **/
    void clean_backup(const int & id=-1, const std::string& dir_to_move = "");

  public:
    
    /**
     * @brief Check is the current file chunk (all layers) is ready for backup.
     * @param id Optional, ID of the layer to be tested. By default, all layers are tested.
     * @return True if ready, false otherwise
     **/
    virtual bool ready_for_backup(int id=-1) const;
    
    /**
     * @brief Change the alternate temporary directory used in case the default temporary directory cannot be used.
     * @param new_alt_dir New directory
     **/
    void change_alternate_temp_dir(const std::string & new_alt_dir);
    
    /**
     * @brief Save the current file chunk is necessary
     * @return True if current file chunk has been saved, false otherwise
     */
    virtual bool save(const std::string & target_base_dir);

    /**
     * @brief Loads all the layers in memory
     * @note This method is used to handle caching
     */
    virtual void load_all();

    /**
     * @brief Test if everything is loaded to get an utterance
     */
    virtual bool is_loaded_for_utterance(const int id);

    /**
     * @brief Clears temporary data from the group of layers
     * @note This method is used to handle caching
     */
    virtual void unload_all();

    /**
     * @brief Generate the XML part of the entity
     * @param stream
     * @param is_terminal
     */
    virtual void deflate(RootsStream * stream, bool is_terminal=true, int list_index=-1);
    /**
     * @brief Extracts the entity from the stream
     * @param stream the stream from which we inflate the element
     */
    virtual void inflate(RootsStream * stream, bool is_terminal=true, int list_index=-1);
    /**
     * @brief returns the XML tag name value for current object
     * @return std::string constant representing the XML tag name
     */
    virtual std::string get_xml_tag_name() const {
      return xml_tag_name();
    };
    /**
     * @brief returns classname for current object
     * @return std::string constant representing the classname
     */
    virtual std::string get_classname() const {
      return classname();
    };
    /**
     * @brief returns classname for current class
     * @return std::string constant representing the classname
     */
    static std::string classname() {
      return "FileChunk";
    };

    /* Layer management */
    // TODO

  public:
    /**
     * @brief Returns the number of layers
     **/
    virtual int count_layers() const;
    
    /**
     * @brief Returns the name of all the layers described in the group of Utterances
     */
    virtual std::vector<std::string> get_all_layer_names() const;

    /**
     * @brief Remove a layer from the chunk. Layer must exist.
     * @note Not efficient
     */
    void remove_layer(const std::string& layerName);

  protected:
    int get_layer_id(const std::string& layerName) const; // throw(RootsException);

    /* File name managment */
  public:

    /**
     * @brief add some empty layers. Layers must not exist.
     */
    void add_filenames(const std::map<std::string, std::string>& fileNames);

    /**
     * @brief add an empty layer. Layer must not exist.
     */
    void add_filename(const std::string& layerName, const std::string& fileName);
    
    /**
     * @brief Adds a filename for the anonymous layer
     * @param fileName Path to the file to be added
     */
    void add_filename(const std::string& fileName);

    /**
     * @brief get the filename associated to a layer. Layer must exist.
     */
    const std::string &  get_filename(const std::string& layerName) const;

    /**
     * @brief get the filename associated to a layer. Layer must exist.
     */
    const std::string &  get_filename(const int& id) const;

    /**
     * @brief get the filename for each layer.
     */
    std::map< std::string, std::string> * get_filenames() const;

    /**
     * @brief Set a layer file name. Layer must exist.
     */
    void set_filename(const std::string& layerName, const std::string& filename);

  protected:


    /* Members */
  private:
    std::string		   	baseDir;
    std::map<std::string, int> 	layersId;
    std::vector<std::string>	files;
    std::vector<bool>		loadedLayers;
    std::vector<bool>	   	readyForBackup;
    std::vector<bool>		currentlyBackedUp;
    std::vector<std::string>	tempFiles;
    std::vector<roots::Corpus> 	layers;
    int 			nbUtterances;
    
    /**
     * @brief Temporary directory which is used to store unsaved file chunks if the directory of original file chunks cannot be written.
     **/
    std::string alternateBaseDir;
    
    /**
     * @brief Name of the directory created to store temporary files.
     **/
    std::string tempDir;
    
    /**
     * @brief Default alternate directory for unsaved file chunks
     **/
    static const std::string DEFAULT_ALTERNATE_TEMP_DIR;
  };

}

#endif // FILECHUNK_H
