/*
    This file is part of Roots.

    Roots is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Roots is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Roots.  If not, see <http://www.gnu.org/licenses/>.

    Copyright (c) 2013, 2014 IRISA
*/


#ifndef SYMBOL_H
#define SYMBOL_H
#include "BaseItem.h"

namespace roots
{
    /**
     * @brief Generic class to store a symbol. The symbol is modeled by the string label.
     **/
    class Symbol : public roots::BaseItem
    {
    public:
      // Constructors, destructor, etc.
      Symbol(const bool noInit = false);
      Symbol(const std::string & symbol_str);
      Symbol(const Symbol & to_be_copied);
      virtual ~Symbol();
      virtual Symbol* clone() const;
      Symbol& operator=(const Symbol & to_be_copied);
      // IO methods
      virtual std::string to_string(int level = 0) const;
      virtual void deflate(RootsStream * stream, bool is_terminal=true, int list_index=-1);
      virtual void inflate(RootsStream * stream, bool is_terminal=true, int list_index=-1);
      static Symbol * inflate_object(RootsStream * stream, int list_index=-1);
      // Metadata methods
      static std::string xml_tag_name() { return "symbol"; };
      static std::string classname() { return "Symbol"; };
      static std::string pgf_display_color() { return "grey!25"; };
      virtual std::string get_xml_tag_name() const { return xml_tag_name(); };
      virtual std::string get_classname() const { return classname(); };
      virtual std::string get_pgf_display_color() const { return pgf_display_color();};
    private:
      // No specific attribute
    };
}

#endif // SYMBOL_H
