/*
    This file is part of Roots.

    Roots is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Roots is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Roots.  If not, see <http://www.gnu.org/licenses/>.

    Copyright (c) 2013, 2014 IRISA
*/


#ifndef ROOTS_STREAM_JSON_H
#define ROOTS_STREAM_JSON_H

#include "common.h"

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include <string.h>
#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <string>
#include <wchar.h>
#include <libgen.h>

#include <exception>

#include <unicode/utypes.h>	/* Basic ICU data types */
#include <unicode/ucnv.h>	/* C   Converter API	*/
#include <unicode/ustring.h>	/* some more string fcns */
#include <unicode/uchar.h>	/* char names		*/
#include <unicode/uloc.h>
#include <unicode/unistr.h>
#include <unicode/decimfmt.h>
#include <unicode/ustream.h>
#include <unicode/fmtable.h>
#include <unicode/numfmt.h>
#include <unicode/regex.h>

#include <json/json.h>

#include <boost/numeric/ublas/matrix_sparse.hpp>

#include "RootsException.h"
#include "RootsStream.h"

#ifndef ROOTS_STREAM_JSON_DEBUG
#define ROOTS_STREAM_JSON_DEBUG 0
#endif

namespace roots
{

  /**
   * @brief Class that provides a generic interfarce for roots input/output
   * @details
   * This class provides a set of convenient methods and functions to manipulate
   * streams in input and output.
   *
   * @author damien.lolive@irisa.fr
   * @version 0.1
   * @date 2013
   */
  class RootsStreamJson: public RootsStream
  {

  public:
    /**
     * @brief Default constructor
     */
    RootsStreamJson ();
    /**
     * @brief Destructor
     */
    virtual ~RootsStreamJson ();
    
    /**
     * @brief Flush and clean the stream
     */
    virtual void clear();
    
  private:
    /**
     * @brief Helper method used to initialized the document. Called by constructor
     */
    void init ();

    /**
     * @brief modifies the current node
     * @param currentNode the new node
     */
    void set_current_node (Json::Value * currentNode);
    /**
     * @brief Modifies the root node
     * @param rootNode the new root node
     */
    void set_root_node (const Json::Value & rootNode);
    /**
     * @brief gives the current node
     * @return current node
     */
    Json::Value& get_current_node ();
    /**
     * @brief Returns the root node
     * @return the root node
     */
    Json::Value& get_root_node ();
    /**
     * @brief clears the document
     */
    void clear_doc ();

  public:
    /**
     * @brief loads the file content
     * @param _filename
     */
    void load (const std::string & _filename);

    /**
     * @brief loads the string content
     * @param _json
     */
    void from_string(const std::string& _json,
		     const std::string  _baseDirName = "");

    /**
     * @brief saves the Json file content
     * @param _filename
     */
    void save (const std::string & _filename);

    /**
     * @brief returns a string representation of the document
     * @return string representation of the object
     */
    std::string to_string ();

    /**
     * @brief Appends a child element to a node
     * @param name the child name
     */
    void append_object (const char * name, int list_index=-1);

    void open_object(const char * name, int list_index=-1);
    void close_object ();

    void set_object_classname(const std::string& classname);
    std::string get_object_classname();

    // go to first child node
    void open_children();

    // go back to parent node
    void close_children();
    void next_child();

    std::string get_object_classname_no_read(const std::string& name, int list_index=-1);
    
    /**
     * @brief Test if the current node has a child (field) of a given name
     * @param name Name of the searched child
     * @return True if the child exists, false otherwise
     **/
    virtual bool has_child(const std::string& name);

    
    /**
     * @brief Test if the current node has a child (field) of a given name which is an array of n elements
     * @param name Name of the searched child
     * @param n Number of element to test    
     * @return True if the child exists and has n elements, false otherwise
     **/
    virtual bool has_n_children(const std::string& name, const unsigned int n);

    /**
     * @brief Returns the number of children with a given name
     * @param name Name of the searched child
     * @return n the number of elements
     **/
    virtual unsigned int get_n_children(const std::string& name);

    //////////////////////////////////////////
#if ROOTS_STREAM_JSON_DEBUG == 1
#define DISP_CONTENT_FOR_NODE  cout << "get content for node " << name << endl;
#else
#define DISP_CONTENT_FOR_NODE
#endif
    //////////////////////////////////////////

    // std::string
    //    virtual const char* deflate_unicodestring (const std::string &  content);
    virtual void inflate_unicodestring (const Json::Value& node, std::string & ret);
    virtual void set_unicodestring_content (const char * name, const std::string& content);
    virtual void append_unicodestring_content(const char * name, const std::string& content);
    virtual std::string get_unicodestring_content (const char * name);

    // char *
    virtual const char* & deflate_cstr (const char* & content);
    virtual void inflate_cstr (const Json::Value& node, char* & ret);
    virtual void set_cstr_content (const char * name, const char*& content);
    virtual void append_cstr_content(const char * name, const char*& content);
    virtual char* get_cstr_content (const char * name);

    // float
    virtual const float & deflate_float (const float & content);
    virtual void inflate_float (const Json::Value& node, float & ret);
    virtual void set_float_content (const char * name, const float& content);
    virtual void append_float_content(const char * name, const float& content);
    virtual float get_float_content (const char * name);

    // double
    virtual const double & deflate_double (const double & content);
    virtual void inflate_double (const Json::Value& node, double & ret);
    virtual void set_double_content (const char * name, const double& content);
    virtual void append_double_content(const char * name, const double& content);
    virtual double get_double_content (const char * name);

    // long double
    virtual double deflate_long_double (const long double & content);
    virtual void inflate_long_double (const Json::Value& node, long double & ret);
    virtual void set_long_double_content (const char * name, const long double& content);
    virtual void append_long_double_content(const char * name, const long double& content);
    virtual long double get_long_double_content (const char * name);

    // int
    virtual const int & deflate_int (const int & content);
    virtual void inflate_int (const Json::Value& node, int & ret);
    virtual void set_int_content (const char * name, const int& content);
    virtual void append_int_content(const char * name, const int& content);
    virtual int get_int_content (const char * name);

    // unsigned int
    virtual const unsigned int & deflate_uint (const unsigned int & content);
    virtual void inflate_uint (const Json::Value& node, unsigned int & ret);
    virtual void set_uint_content (const char * name, const unsigned int& content);
    virtual void append_uint_content(const char * name, const unsigned int& content);
    virtual unsigned int get_uint_content (const char * name);

    // bool
    virtual const bool & deflate_bool (const bool & content);
    virtual void inflate_bool (const Json::Value& node, bool & ret);
    virtual void set_bool_content (const char * name, const bool& content);
    virtual void append_bool_content(const char * name, const bool& content);
    virtual bool get_bool_content (const char * name);

    // char
    virtual const char & deflate_char (const char & content);
    virtual void inflate_char (const Json::Value& node, char & ret);
    virtual void set_char_content (const char * name, const char& content);
    virtual void append_char_content(const char * name, const char& content);
    virtual char get_char_content (const char * name);

    // UChar
    virtual const UChar & deflate_uchar (const UChar & content);
    virtual void inflate_uchar (const Json::Value& node, UChar & ret);
    virtual void set_uchar_content (const char * name, const UChar& content);
    virtual void append_uchar_content(const char * name, const UChar& content);
    virtual UChar get_uchar_content (const char * name);

    // std::vector<float>
    virtual void append_vector_float_content(const char * name, const std::vector<float> & content);
    virtual void set_vector_float_content(const char * name, const std::vector<float> & content);
    virtual std::vector<float> get_vector_float_content (const char * name);

    // std::vector<int>
    virtual void append_vector_int_content(const char * name, const std::vector<int> & content);
    virtual void set_vector_int_content(const char * name, const std::vector<int> & content);
    virtual std::vector<int> get_vector_int_content (const char * name);
    
    // std::vector<bool>
    virtual void append_vector_bool_content(const char * name, const std::vector<bool> & content);
    virtual void set_vector_bool_content(const char * name, const std::vector<bool> & content);
    virtual std::vector<bool> get_vector_bool_content (const char * name);

    // std::vector<UChar>
    virtual void append_vector_uchar_content(const char * name, const std::vector<UChar> & content);
    virtual void set_vector_uchar_content(const char * name, const std::vector<UChar> & content);
    virtual std::vector<UChar> get_vector_uchar_content (const char * name);

    // std::vector<std::string>
    virtual void append_vector_unicodestring_content(const char * name, const std::vector<std::string> & content);
    virtual void set_vector_unicodestring_content(const char * name, const std::vector<std::string> & content);
    virtual std::vector<std::string> get_vector_unicodestring_content (const char * name);

    // std::set<int>
    virtual void append_set_int_content(const char * name, const std::set<int> & content);
    virtual void set_set_int_content(const char * name, const std::set<int> & content);
    virtual std::set<int> get_set_int_content (const char * name);
  
    // std::set<UChar>
    virtual void append_set_uchar_content(const char * name, const std::set<UChar> & content);
    virtual void set_set_uchar_content(const char * name, const std::set<UChar> & content);
    virtual std::set<UChar> get_set_uchar_content (const char * name);
  

    // boost::numeric::ublas::mapped_matrix<int>
    void append_matrix_int_content(const char * name, const boost::numeric::ublas::mapped_matrix<int>& content);

    boost::numeric::ublas::mapped_matrix<int> get_matrix_int_content(const char * name);

    // std::map<std::string, std::string>
    virtual void append_map_unistr_unistr_content(const char * name, const std::map<std::string, std::string>& content);
    std::map<std::string, std::string> get_map_unistr_unistr_content(const char * name);

  private:
    Json::Value rootNode;			/**< The root node of the document */
    Json::Value * currentNode;		/**< The current node, always a child of the root node */
    std::stack < Json::Value * >parentNodes;		/**< The parent nodes, always a child of the root node */
  };

}

#endif // ROOTS_STREAM_JSON_H
