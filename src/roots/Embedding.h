/*
    This file is part of Roots.

    Roots is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Roots is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Roots.  If not, see <http://www.gnu.org/licenses/>.

    Copyright (c) 2013, 2014 IRISA
*/


#ifndef EMBEDDING_H
#define EMBEDDING_H
#include "BaseItem.h"

namespace roots
{
    /**
     * @brief Generic class to store an embedding. The embedding is modeled by a vector of floats.
     **/
    class Embedding : public roots::BaseItem
    {
    private:
      /**
       * @brief Value of the embedding
       **/
      std::vector<float> value;
    public:
      // Constructors, destructor, etc.
      Embedding(const bool noInit = false);
      Embedding(const std::vector<float>& _value);
//      Embedding(const std::string & str_value);
      Embedding(const Embedding & to_be_copied);
      virtual ~Embedding();
      virtual Embedding* clone() const;
      Embedding& operator=(const Embedding & to_be_copied);
      // IO methods
      virtual std::string to_string(int level = 0) const;
      virtual void deflate(RootsStream * stream, bool is_terminal=true, int list_index=-1);
      virtual void inflate(RootsStream * stream, bool is_terminal=true, int list_index=-1);
      static Embedding * inflate_object(RootsStream * stream, int list_index=-1);
      // Specific methods
      virtual std::vector<float> get_value() const;
      virtual void set_value(const std::vector<float>& _value);
      // Metadata methods
      static std::string xml_tag_name() { return "number"; };
      static std::string classname() { return "Embedding"; };
      static std::string pgf_display_color() { return "grey!40"; };
      virtual std::string get_xml_tag_name() const { return xml_tag_name(); };
      virtual std::string get_classname() const { return classname(); };
      virtual std::string get_pgf_display_color() const { return pgf_display_color();};
    };
}

#endif // EMBEDDING_H
