/*
    This file is part of Roots.

    Roots is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Roots is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Roots.  If not, see <http://www.gnu.org/licenses/>.

    Copyright (c) 2013, 2014 IRISA
*/


#ifndef TreeNode_H_
#define TreeNode_H_

#include "../common.h"
#include "../BaseItem.h"
#include "../RelationLinkedObject.h"
#include "../Relation.h"


namespace roots
{

  namespace tree
  {

    /**
     * @brief Generic tree node
     * @details
     * This class models a generic tree node.
     * @author Cordial Group
     * @version 0.1
     * @date 2012
     */
    class TreeNode: public roots::BaseItem
      {
      public:
	/**
	 * @brief Default constructor
	 */
      TreeNode(const bool noInit = false):BaseItem(noInit)
	  {
	    parent = NULL;
	    children = std::vector<TreeNode*>();
	  };

	/**
	 * @brief Copy constructor
	 */
	TreeNode(const TreeNode& node);

	/**
	 * @brief Destructor
	 */
	virtual ~TreeNode();

	/**
	 * @brief clone the current sequence object
	 * @return a copy of the object
	 */
	virtual TreeNode* clone() const;

	/**
	 * @brief Copy a node on the current one
	 * @param node
	 * @return reference on the current node
	 */
	TreeNode& operator= (const TreeNode& node);

	/**
	 * @brief clone the current sequence object and its children recursively
	 * @return a copy of the object
	 */
	virtual TreeNode* clone_tree() const;

	/**
	 * @brief Returns the child at given index
	 * @param index index of the child node (default: 0)
	 * @return pointer to the child
	 */
	virtual TreeNode* get_child(int index=0) const;

	/**
	 * @brief Returns vector of pointers to children
	 * @return pointer to the children as a vector
	 */
	virtual const std::vector<TreeNode*>& get_children() const { return children;};

	/**
	 * @brief Returns vector of pointers to children
	 * @return pointer to the children as a vector
	 */
	virtual std::vector<TreeNode*>& get_children() { return children;};

	/**
	 * @brief Returns the parent node (self if no parent)
	 * @return pointer to the parent
	 */
	virtual TreeNode* get_parent() const;

	/**
	 * @brief Set the parent node pointer
	 * @param ptr pointer to the parent node
	 */
	virtual void set_parent(TreeNode* ptr) { parent = ptr; }

	/**
	 * @brief Sets the vector of pointers to children
	 * @param c a vector of pointers
	 */
	virtual void set_children(const std::vector<TreeNode*>& c) { children = c;};

	/**
	 * @brief Adds a child to the current node
	 * @details Adds child to the current node and place it at given
	 * index in the list of children. This method does not copy the
	 * child node. By default, if index is not given, the node is added
	 * after last position.
	 * @param node
	 * @param index default: -1
	 * @return pointer to the parent
	 */
	virtual void add_child(TreeNode *node, int index=-1);

	virtual TreeNode* new_child(const bool noInit = false) const;


	/**
	 * @brief Removes the child at given index and adopts its children
	 * @param index
	 */
	virtual void remove_child(int index);

	/**
	 * @brief Removes the child at given index and also removes all its subtree
	 * @param index
	 */
	virtual void remove_child_and_subtree(int index);

	/**
	 * @brief Deletes of the subtrees and children linked to this node
	 */
	virtual void delete_children_and_subtrees();

	/**
	 * @brief Returns number of children
	 * @return the number of children nodes
	 */
	virtual int get_number_of_children() const { return children.size();}

	/**
	 * @brief Returns the depth of the tree
	 * @return the depth of the tree from the current node
	 */
	virtual int get_max_depth() const;

	/**
	 * @brief Modifies the current syllable to take into account the insertion of an element into the related sequence
	 * @param index index of the element added in the related sequence
	 **/
	virtual void insert_related_element(int index);

	/**
	 * @brief Modifies the current syllable to take into account the deletion of an element into the related sequence
	 * @param index index of the element removed from the related sequence
	 **/
	virtual void remove_related_element(int index);


	/**
	 * @brief returns the display height for the current object
	 * @return display height for the current object
	 */
	virtual int get_pgf_height() const;

	void get_nodes(std::vector<TreeNode*>& leaves);
	void get_leaf_nodes(std::vector<TreeNode*>& leaves);
	TreeNode* get_first_leaf();
	TreeNode* get_last_leaf();

	int get_number_of_leaves() const;
	virtual int get_first_target_index() const;
	virtual int get_last_target_index() const;

	/**
	 * @brief Build a dot representation of the tree
	 * @param level
	 * @param rank prefix counter used for each node
	 * @return
	 */
	virtual std::string output_dot(unsigned int level=0, unsigned int rank=0) const;

	/**
	 * @brief returns the XML tag name value for current object
	 * @return string constant representing the XML tag name
	 */
	virtual std::string get_xml_tag_name() const { return xml_tag_name(); };

	/**
	 * @brief returns the XML tag name value for current class
	 * @return string constant representing the XML tag name
	 */
	static std::string xml_tag_name()
	{
	  return "treenode";
	}

	/**
	 * @brief Recursively deflates the tree
	 * @param stream
	 */
	virtual void deflate_tree(RootsStream * stream);
	/**
	 * @brief Recursively inflates the tree
	 * @param stream
	 */
	virtual void inflate_tree(RootsStream * stream, bool is_terminal=true);
	/**
	 * @brief Write the entity into a RootsStream
	 * @param stream the stream from which we inflate the element
	 * @param is_terminal indicates that the node is terminal (true by default)
	 */
	virtual void deflate(RootsStream * stream, bool is_terminal=true, int list_index=-1);
	/**
	 * @brief Extracts the entity from the stream
	 * @param stream the stream from which we inflate the element
	 */
	virtual void inflate(RootsStream * stream, bool is_terminal=true, int list_index=-1);

      protected:
	/**
	 * @brief Deflates the specific content of the current node
	 * @param stream
	 */
	virtual void deflate_content(RootsStream * stream, int list_index=-1);
	/**
	 * @brief Inflates the specific content of the current node
	 * @param stream
	 */
	virtual void inflate_content(RootsStream * stream, bool is_terminal=true, int list_index=-1);


      private:
	TreeNode *parent;
	std::vector<TreeNode*> children;
      };

  } } // End of namespace

#endif /* TreeNode_H_ */
