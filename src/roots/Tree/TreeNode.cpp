/*
    This file is part of Roots.

    Roots is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Roots is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Roots.  If not, see <http://www.gnu.org/licenses/>.

    Copyright (c) 2013, 2014 IRISA
*/


#include "TreeNode.h"

namespace roots
{

  namespace tree
  {

    TreeNode::~TreeNode()
    {
      // The node is not responsible for the memory allocation of its children
      // -> nothing to do
    }

    TreeNode::TreeNode(const TreeNode& node): BaseItem(node)
    {
      this->parent = node.get_parent();
      this->children = node.get_children();
    }

    TreeNode *TreeNode::clone() const
    {
      return new TreeNode(*this);
    }

    TreeNode& TreeNode::operator= (const TreeNode& node)
    {
      BaseItem::operator=(node);

      this->parent = node.get_parent();
      this->children = node.get_children();

      return *this;
    }


    TreeNode *TreeNode::clone_tree() const
    {
      TreeNode *node = this->clone();
      node->set_children(std::vector<TreeNode*>());

      // copies children recursively
      int childIndex = 0;
      int numChildren = get_number_of_children();
      while(childIndex < numChildren)
	{
	  node->add_child(children[childIndex]->clone_tree());
	  ++childIndex;
	}

      return node;
    }

    TreeNode* TreeNode::get_child(int index) const
    {
      if(index<0 || ((unsigned int)index>=children.size()))
	return NULL;

      return children[index];
    }

    TreeNode* TreeNode::get_parent() const
    {
      return this->parent;
    }


    void TreeNode::add_child(TreeNode *node, int index)
    {
      if(index<-1 || index>get_number_of_children())
	{
	  std::stringstream ss;
	  ss << "TreeNode::add_child: index="<< index << " is not a valid value!";
	  throw RootsException(__FILE__, __LINE__, ss.str().c_str());
	}

      if(index == -1)
	{
	  children.push_back(node);
	}
      else
	{
	  children.insert(children.begin()+index, node);
	}
    }


    void TreeNode::remove_child(int index)
    {
      if(index<0 || index>=get_number_of_children())
	{
	  std::stringstream ss;
	  ss << "TreeNode::remove_child: index="<< index << " is not a valid value!";
	  throw RootsException(__FILE__, __LINE__, ss.str().c_str());
	}


      TreeNode *nodeToRemove = children[index];

      children.erase(children.begin() + index);
      std::vector<TreeNode*> nodesToAdd = nodeToRemove->get_children();

      int childIndex = 0;
      int numChildren = nodesToAdd.size();
      while(childIndex < numChildren)
	{
	  nodesToAdd[childIndex]->set_parent(this);
	  ++childIndex;
	}

      children.insert(children.begin() + index, nodesToAdd.begin(), nodesToAdd.end());

      delete nodeToRemove;
    }


    void TreeNode::remove_child_and_subtree(int index)
    {
      if(index<0 || index>=get_number_of_children())
	{
	  std::stringstream ss;
	  ss << "TreeNode::remove_child: index="<< index << " is not a valid value!";
	  throw RootsException(__FILE__, __LINE__, ss.str().c_str());
	}

      TreeNode *nodeToRemove = children[index];

      children.erase(children.begin() + index);

      nodeToRemove->delete_children_and_subtrees();
      delete nodeToRemove;
    }

    void TreeNode::delete_children_and_subtrees()
    {
      TreeNode *nodeToRemove = NULL;
      int childIndex = 0;
      int numChildren = get_number_of_children();
      while(childIndex < numChildren)
	{
	  nodeToRemove = children[childIndex];
	  nodeToRemove->delete_children_and_subtrees();
	  delete nodeToRemove;
	  ++childIndex;
	}
      children.clear();
    }

    int TreeNode::get_pgf_height() const
    {
      return 1;
    }

    int TreeNode::get_max_depth() const
    {
      int childIndex = 0;
      int maxDepth = 0;
      int numChildren = get_number_of_children();
      while(childIndex < numChildren)
	{
	  int curMaxDepth = children[childIndex]->get_max_depth();
	  if(curMaxDepth > maxDepth) maxDepth = curMaxDepth;
	  ++childIndex;
	}
      return 1 + maxDepth;
    }

    void TreeNode::insert_related_element(int /*index*/)
    {
      // Nothing
      // Has to be overriden in subclasses
    }

    void TreeNode::remove_related_element(int /*index*/)
    {
      //std::cerr << " **** I should not arrive here ! " << std::endl;
      // Nothing
      // Has to be overriden in subclasses
    }

    std::string TreeNode::output_dot(unsigned int level, unsigned int rank) const
    {
      std::stringstream ss, ss2;
      std::string str;

      ss << "\"" << rank << " " << this->get_label() << "\"";

      if(get_number_of_children() == 0)
	{
	  str = std::string(ss.str().c_str());
	  ss2 << str << " ;" << std::endl;
	  str = std::string(ss2.str().c_str());
	}else{
	str = std::string(ss.str().c_str());

	int childIndex = 0;
	while(childIndex < get_number_of_children())
	  {
	    ss2 << str << " -> " << children[childIndex]->output_dot(level+1, ++rank);
	    ++childIndex;
	  }
	str = std::string(ss2.str().c_str());
      }

      return str;
    }

    void TreeNode::get_leaf_nodes(std::vector<TreeNode*>& leaves)
    {
      int childIndex = 0;
      int numChildren = get_number_of_children();

      if(numChildren == 0)
	{
	  leaves.push_back(this);
	}
      else
	{
	  while(childIndex < numChildren)
	    {
	      children[childIndex]->get_leaf_nodes(leaves);
	      ++childIndex;
	    }
	}
    }

    void TreeNode::get_nodes(std::vector<TreeNode*>& leaves)
    {
      int childIndex = 0;
      int numChildren = get_number_of_children();

      leaves.push_back(this);
      while(childIndex < numChildren)
	{
	  children[childIndex]->get_nodes(leaves);
	  ++childIndex;
	}
    }

    TreeNode* TreeNode::get_first_leaf()
    {
      if(get_number_of_children() == 0)
	{
	  return this;
	}
      else
	{
	  return children[0]->get_first_leaf();
	}
    }

    TreeNode* TreeNode::get_last_leaf()
    {
      if(get_number_of_children() == 0)
	{
	  return this;
	}
      else
	{
	  return children[children.size()-1]->get_last_leaf();
	}
    }

    int TreeNode::get_number_of_leaves() const
    {
      int numberOfLeaves = 0;

      int childIndex = 0;
      int numChildren = get_number_of_children();
      if(numChildren == 0)
	{
	  numberOfLeaves = 1;
	}
      else
	{
	  while(childIndex < numChildren)
	    {
	      numberOfLeaves += children[childIndex]->get_number_of_leaves();
	      ++childIndex;
	    }
	}

      return numberOfLeaves;
    }

    int TreeNode::get_first_target_index() const
    {
      return 0;
    }

    int TreeNode::get_last_target_index() const
    {
      return get_number_of_leaves();
    }

    void TreeNode::deflate_content(RootsStream * /* stream */, int /* list_index */)
    {
      // Nothing to do here
    }


    void TreeNode::deflate_tree(RootsStream * stream)
    {
      this->deflate_content(stream);

      int childIndex = 0;
      int numChildren = get_number_of_children();

      stream->append_int_content("n_children", numChildren);

      while(childIndex < numChildren)
	{
	  stream->append_object("tree", childIndex);
	  children[childIndex]->deflate_tree(stream);
	  stream->close_object();
	  ++childIndex;
	}
    }

    void TreeNode::inflate_content(RootsStream *  /* stream */, bool /* is_terminal */, int /* list_index */)
    {
      // Nothing to do here
    }

    TreeNode* TreeNode::new_child(const bool noInit) const
    {
      //cout << "NEW CHILD TREENODE" << std::endl;
      return new TreeNode(noInit);
    }


    void TreeNode::inflate_tree(RootsStream * stream, bool /* is_terminal */)
    {
      //	stream->open_children();

      this->inflate_content(stream, true);
      stream->next_child();


      int childIndex = 0;
      int numChildren = stream->get_int_content("n_children");

      while(childIndex < numChildren)
	{
	  TreeNode *child = this->new_child(true);
	  this->add_child(child,childIndex);

	  stream->open_object("tree", childIndex);
	  stream->open_children();
	  child->inflate_tree(stream);
	  stream->close_children();
	  stream->next_child();
	  ++childIndex;
	}

      //	if(is_terminal) stream->close_children();
    }


    void TreeNode::deflate(RootsStream * stream, bool is_terminal, int list_index)
    {
      BaseItem::deflate(stream,false,list_index);

      this->deflate_tree(stream);

      if(is_terminal)
	stream->close_object();
    }


    void TreeNode::inflate(RootsStream * stream, bool is_terminal, int list_index)
    {
      BaseItem::inflate(stream,false,list_index); // Inflates super class

      this->inflate_tree(stream);
      stream->next_child();

      if(is_terminal)
	stream->close_children();
    }

  }
}

