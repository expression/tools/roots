/*
    This file is part of Roots.

    Roots is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Roots is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Roots.  If not, see <http://www.gnu.org/licenses/>.

    Copyright (c) 2013, 2014 IRISA
*/


/*
 * RootsDisplay.cpp
 *
 *  Created on: Sep 2, 2013
 *	Author: zene
 */
#include "RootsDisplay.h"
#include "Sequence/SyntaxSequence.h"
double pgf_scaling_factor=1.0;
#define MAX_LATEX_WIDTH 1400
#define MINIMUM_WIDTH 4
#define MINIMUM_HEIGHT 5
#define TEXTWIDTH_FACTOR 0.7
#define INNER_SEP_WIDTH 0.1
//0.1 is minimum. we can not achieve a smaller LINE_WIDTH by scaling
#define LINE_WIDTH 0.01
#define VERTICAL_DISTANCE 2*pgf_scaling_factor
#define PATTERN_SIZE 10*pgf_scaling_factor
#define PATTERN_INSET 0.1
#define PATTERN_UNIT "pt"
#define UNIT "em"
#define NO_PATTERN ""
#define STAR_PATTERN_NAME "roots star"
#define SHARP_PATTERN_NAME "roots sharp"
#define PGF_PATTERNS_HEADER_BEGIN "\\makeatletter\n"
#define PGF_PATTERNS_HEADER_END					\
  "\\makeatother\n"						\
  << "\\newdimen\\LineSpace\n"					\
  << "\\tikzset{\n"						\
  << "line space/.code={\\LineSpace=#1},\n"			\
  << "line space=" << 60 * pgf_scaling_factor << "pt" << "\n"	\
  << "}\n"

#define STAR_PATTERN_DEF "\\pgfdeclarepatternformonly[\\LineSpace,\\tikz@pattern@color]{roots star}{\\pgfqpoint{"<< -PATTERN_SIZE << PATTERN_UNIT << "}{"<< -PATTERN_SIZE << PATTERN_UNIT << "}}{\\pgfqpoint{\\LineSpace}{\\LineSpace}}{\\pgfqpoint{\\LineSpace}{\\LineSpace}}%\n" \
  << "{\n"								\
  << "\\pgfsetlinewidth{" << PATTERN_SIZE << PATTERN_UNIT << "}\n"		\
  << "\\pgfsetcolor{\\tikz@pattern@color}\n"				\
  << "\\pgfpathmoveto{\\pgfqpoint{0" << UNIT << "}{\\LineSpace +" << PATTERN_INSET << PATTERN_UNIT << "}}\n" \
  << "\\pgfpathlineto{\\pgfqpoint{\\LineSpace + " << PATTERN_SIZE << PATTERN_UNIT << "}{0" << UNIT << "}}\n" \
  << "\\pgfpathmoveto{\\pgfqpoint{" << -PATTERN_SIZE << PATTERN_UNIT << "}{" << -PATTERN_SIZE << UNIT << "}}\n" \
  << "\\pgfpathlineto{\\pgfqpoint{\\LineSpace + " << PATTERN_INSET << PATTERN_UNIT << "}{\\LineSpace +" << PATTERN_INSET << PATTERN_UNIT << "}}\n" \
  << "\\pgfusepath{draw}\n"						\
  << "}\n"

#define SHARP_PATTERN_DEF "\\pgfdeclarepatternformonly[\\LineSpace,\\tikz@pattern@color]{roots sharp}{\\pgfqpoint{"<< -PATTERN_SIZE << PATTERN_UNIT << "}{"<< -PATTERN_SIZE << PATTERN_UNIT << "}}{\\pgfqpoint{\\LineSpace}{\\LineSpace}}{\\pgfqpoint{\\LineSpace}{\\LineSpace}}%\n" \
  << "{\n"								\
  << "\\pgfsetlinewidth{" << PATTERN_SIZE << PATTERN_UNIT << "}\n"		\
  << "\\pgfsetcolor{\\tikz@pattern@color}\n"				\
  << "\\pgfpathmoveto{\\pgfqpoint{0" << UNIT << "}{0" << PATTERN_UNIT << "}}\n"	\
  << "\\pgfpathlineto{\\pgfqpoint{\\LineSpace + " << PATTERN_INSET << PATTERN_UNIT << "}{\\LineSpace +" << PATTERN_INSET << PATTERN_UNIT << "}}\n" \
  << "\\pgfpathmoveto{\\pgfqpoint{" << -PATTERN_SIZE << PATTERN_UNIT << "}{" << -PATTERN_SIZE << PATTERN_UNIT << "}}\n" \
  << "\\pgfpathlineto{\\pgfqpoint{\\LineSpace +" << PATTERN_INSET << PATTERN_UNIT << "}{\\LineSpace +" << PATTERN_INSET << PATTERN_UNIT << "}}\n" \
  << "\\pgfusepath{draw}\n"						\
  << "}\n"

// It is in texlive-latex-extra ubuntu package since texlive 2012
#define PGF_HEADER_BEGIN "\\documentclass{standalone}\n"	\
  << "\\usepackage[mathletters]{ucs}\n"				\
  << "\\usepackage[utf8x]{luainputenc}\n"			\
  << "\\usepackage[T1]{fontenc}\n"				\
  << "\\usepackage{lmodern}"					\
  << "\\usepackage{fontspec}"					\
  << "\\setmainfont{LinBiolinumO}"				\
  << "\\usepackage{tikz}\n"					\
  << "\\usepackage{scalefnt}\n"					\
  << "\\usetikzlibrary{positioning}\n"				\
  << "\\usetikzlibrary{patterns}\n"				\
  << "\\setmainfont{unifont}\n"

// It is in texlive-latex-extra ubuntu package since texlive 2012
// To use with Xelatex
/*
#define PGF_HEADER_BEGIN "\\documentclass{standalone}\n"	\
  << "\\usepackage{fontspec}"					\
  << "\\setmainfont{Linux Libertine O}\n"			\
  << "\\usepackage{tikz}\n"					\
  << "\\usepackage{scalefnt}\n"					\
  << "\\usetikzlibrary{positioning}\n"				\
  << "\\usetikzlibrary{patterns}\n"
*/

#define PGF_HEADER_END ""
#define PGF_DOCUMENT_HEADER_BEGIN "\\begin{document}\n\\scalefont{" << pgf_scaling_factor << "}\\begin{tikzpicture}\n"
#define PGF_DOCUMENT_HEADER_END "\\end{tikzpicture}\n\\end{document}\n"
#define PGF_STYLES_HEADER_BEGIN "[node distance=" << VERTICAL_DISTANCE << UNIT << " and 0,"
#define PGF_STYLES_HEADER_BEGIN_NO_SPACE "[node distance=0" << UNIT << " and 0,"
#define PGF_STYLE_DEF(stream, name, color) stream << name << "style/.style={" \
  << "rectangle,\n"							\
  << "minimum height=" << MINIMUM_HEIGHT * pgf_scaling_factor << UNIT << ",\n" \
  << "inner sep=" << INNER_SEP_WIDTH * pgf_scaling_factor << UNIT << ",\n" \
  << "line width=0.0,\n"						\
  << "text badly centered,\n"						\
  << "font=\\itshape}"
#define PGF_STYLES_DEF_SEP ","
#define PGF_STYLES_HEADER_END "]"
#define PGF_ITEM(stream, style, seq, column, lastItem, color, width, label, pattern) \
  stream << "\\node (" << seq << "seq" << column << ") ["		\
  << style << "style,"							\
  << "right=of " << seq << "seq" << lastItem << ","			\
  << "fill=" << color << ","						\
  << "text width=" << width * pgf_scaling_factor << UNIT << "]";	\
  if(sizeof(pattern) > 1) {						\
    stream << "[postaction={"						\
	   << "pattern color=" << make_color_darker(color) << ","	\
	   << "pattern=" << pattern << "}]";}				\
  stream << "{" << label << "};\n";

#define PGF_ITEM_SEP_BEFORE(stream, seq, col, dashed)			\
  stream << "\\draw[line width=" << LINE_WIDTH << UNIT;			\
  if(dashed) { stream <<",dashed,";}					\
  stream << "] (" << seq << "seq" << col << ".north west)--(" << seq << "seq" << col << ".south west);\n";
#define PGF_ITEM_SEP_AFTER(stream, seq, col, dashed)			\
  stream << "\\draw[line width=" << LINE_WIDTH << UNIT;			\
  if(dashed) { stream <<",dashed,";}					\
  stream << "] (" << seq << "seq" << col << ".north east)--(" << seq << "seq" << col << ".south east);\n";
#define PGF_TABLE_HEAD(stream, style, seq, prevSeq, width, label)	\
  stream << "\\node (" << seq << "seq-1) ["				\
  << style << "style, ";						\
  if(prevSeq !="") {stream << " below=of " << prevSeq << "seq-1";}	\
  stream << ", text width=" << width * pgf_scaling_factor << UNIT << "] {" << label << "};\n" \
  << "\\draw[line width=" << LINE_WIDTH << UNIT << "] (" << seq << "seq-1.north west)--(" << seq << "seq-1.south west)" \
  << "(" << seq << "seq-1.north east)--(" << seq << "seq-1.south east);\n";

#define PGF_DRAW_ARROW(stream, seq1, item1, seq2, item2)		\
  stream << "\\draw[line width=" << LINE_WIDTH << UNIT			\
  << "] (" << seq1 << "seq" << item1 << ".north)--(" << seq2 << "seq" << item2 << ");\n";

namespace roots
{

  RootsDisplay::RootsDisplay()
  {
    sharpsNr = -1;
    aligned_matrix = NULL;
    matrix_length = matrix_height = 0;
    clean_mem() ;
  }

  RootsDisplay::~RootsDisplay()
  {
    clean_mem();
  }

  std::string RootsDisplay::make_color_darker(std::string color) {

    std::string color_name;
    size_t lastIndex;
    int color_percentage = 0;
    if( (lastIndex = color.find_last_of("!")) != std::string::npos) {
      color_name = color.substr(0, lastIndex);
      std::string color_pct = color.substr(lastIndex+1, color.length());
      for(size_t i = 0; i < color_pct.length(); i++) {
	color_percentage = color_percentage * 10 + (color_pct[i] - '0');
      }
    }
    else {
      color_name = color;
    }

    color_percentage += 20;

     std::stringstream ss;
    ss << color_name << "!" << color_percentage;

    return ss.str();
  }

  void RootsDisplay::draw_arrows(std::ostream &stream,
				 UtteranceDisplay &utt,
				 const std::string &source_seq,
				 const std::string &target_seq,
				 int item_index)
  {
    Relation *r = utt.get_relation(source_seq, target_seq);
    if(r != NULL) {
      std::vector<int> relEls = r->get_related_elements(item_index);
      for(size_t i = 0; i < relEls.size(); i++)
	{
	  PGF_DRAW_ARROW(stream, source_seq, item_index, target_seq, relEls[i]);
	}
    }
  }

  void RootsDisplay::make_label_pgf_compatible(std::string& label)
  {
    findAndReplace(label, "_", ":");
    findAndReplace(label, "}", "\\}");
    findAndReplace(label, "{", "\\{");
    findAndReplace(label, "#", "\\#");
  }

  double RootsDisplay::get_item_width(const roots::sequence::Sequence & seq,
				      int item_index,
				      int level)
  {
    Base* item;
    if(item_index == NIL)
      {return 0.0;}

    if(IS_SHARP(item_index)) {
      sharp* sh = sharps_ht[HASHINDEX_TO_INDEX(item_index)];
      double maxWidth = MINIMUM_WIDTH;
      for(int i = sh->firstElem; i <= sh->lastElem; i++) {
	item = seq.get_item(i);
	maxWidth = std::max(maxWidth, TEXTWIDTH_FACTOR * get_item_label(item, level).length());
      }
      return maxWidth * (sh->lastElem - sh->firstElem + 1);
    }
    else {
      item = seq.get_item(item_index);
      double maxWidth = TEXTWIDTH_FACTOR*get_item_label(item, level).length();
      maxWidth = std::max(maxWidth, (double)MINIMUM_WIDTH);
      return maxWidth;
    }
  }

  std::string RootsDisplay::get_item_label(Base* item, int level)
  {
    if(item->get_classname() != linguistic::syntax::Syntax::classname()) {
      return item->to_string(level);
    }
    else {
      linguistic::syntax::Syntax *st = static_cast<linguistic::syntax::Syntax*>(item);
      return st->get_root_node()->to_string(level);
    }
  }



  std::string RootsDisplay::to_pgf(Utterance & utt,
				     const std::vector<std::string> &seqLabels,
				     int level,
				     bool draw_relations)
  {
    std::vector<std::string> hiddensSeqLabels ;
    std::vector<int> displayLevels(seqLabels.size(), level);
    return to_pgf(utt, seqLabels, hiddensSeqLabels, displayLevels, draw_relations);
  }

  std::string RootsDisplay::to_pgf(Utterance & utt,
				     const std::vector<std::string> & seqLabels,
				     const std::vector<std::string> & hiddensSeqLabels,
				     const std::vector<int> & displayLevels,
				     bool draw_relations)
  {
    UtteranceDisplay utt_display(utt, seqLabels, hiddensSeqLabels, displayLevels);
    const std::vector<std::string>& new_seqLabels = utt_display.get_all_sequence_labels();
    toAlignedMatrix(utt_display, new_seqLabels);

    std::vector<int> itemRunStart(matrix_height, 0);
    std::vector<double> colWidth(matrix_length, 0);

    for(int j = 0; j <= matrix_length; j++) {
      for(int i = 0; i < matrix_height; i++) {
	bool itemFinished = (j==matrix_length) || (j > 0 && aligned_matrix[i*matrix_length+ j-1] != aligned_matrix[i*matrix_length+j]);
	roots::sequence::Sequence * seq = utt_display.get_sequence(new_seqLabels[i]);
	if(itemFinished) {
	  int level = utt_display.sequenceDisplayLevel(new_seqLabels[i]);
	  //update column maximum for all columns that were in a sharp
	  double itemWidth = get_item_width(*seq, aligned_matrix[i*matrix_length+j-1], level);
	  int colsNeeded = j - itemRunStart[i];
	  for(int k = itemRunStart[i]; k < j; k++) {
	    colWidth[k] = std::max(colWidth[k], itemWidth / colsNeeded );
	  }
	  itemRunStart[i] = j;
	}
	if(j == 0) {
	  itemRunStart[i] = 0;
	}
      }
    }

    double total_width = 0.0;
    for(int i =0; i < matrix_length; i++)
      {
	total_width += colWidth[i] + 2 * INNER_SEP_WIDTH + LINE_WIDTH;
      }
    //	  cout << "Total width: " << total_width <<  std::endl;
    pgf_scaling_factor = std::min( MAX_LATEX_WIDTH / total_width, pgf_scaling_factor);
    //	  cout << "Scaling factor: " << pgf_scaling_factor <<  std::endl;
    //scaling_factor is a global variable
    //following lines will be affected by scaling_factor


     std::stringstream ss;
    ss << PGF_HEADER_BEGIN;
    ss << PGF_PATTERNS_HEADER_BEGIN << STAR_PATTERN_DEF << SHARP_PATTERN_DEF << PGF_PATTERNS_HEADER_END;
    ss << PGF_DOCUMENT_HEADER_BEGIN;

    //print sequence styles and labels;
    if(draw_relations)
      { ss << PGF_STYLES_HEADER_BEGIN; }
    else
      { ss << PGF_STYLES_HEADER_BEGIN_NO_SPACE; }
    for(size_t i = 0; i < new_seqLabels.size(); i++) {
      PGF_STYLE_DEF(ss, new_seqLabels[i], "white") << PGF_STYLES_DEF_SEP <<  std::endl;

    }
    //define also NIL style
    PGF_STYLE_DEF(ss, "blank", "white") << PGF_STYLES_HEADER_END <<  std::endl;


    double widthHeader = MINIMUM_WIDTH;
    for(size_t i = 0; i < new_seqLabels.size(); i++) 
      { widthHeader = std::max(TEXTWIDTH_FACTOR*new_seqLabels[i].length(), widthHeader); }

    std::string prevSeq = "";
    for(size_t i = 0; i < new_seqLabels.size(); i++) {
      PGF_TABLE_HEAD(ss, new_seqLabels[i], new_seqLabels[i], prevSeq, widthHeader, new_seqLabels[i]);
      prevSeq = new_seqLabels[i];
    }

    for(int i = 0; i < matrix_height; i++)
      {
	if(!utt_display.sequenceIsHidden(new_seqLabels[i]))
	  {
	    sequence::Sequence* crtSequence = utt_display.get_sequence(new_seqLabels[i]);
	    int level = utt_display.sequenceDisplayLevel(new_seqLabels[i]);
	    double itemWidth = colWidth[0];
	    int NIL_count = -2;
	    int lastItem = -1;
	    for(int j = 1; j <= matrix_length; j++) {
	      if(j < matrix_length && aligned_matrix[i*matrix_length + j] == aligned_matrix[i*matrix_length + j-1]) {
		itemWidth += colWidth[j] + 2 * INNER_SEP_WIDTH;
		continue;
	      }
	      else {
		Base* item;
		if(!IS_SHARP(aligned_matrix[i* matrix_length + j-1]))
		  {
		    if(aligned_matrix[i* matrix_length + j-1] != NIL)
		      {
			int item_index = aligned_matrix[i* matrix_length + j-1];
			item = crtSequence->get_item(item_index);
			std::string item_label = get_item_label(item, level);
			make_label_pgf_compatible(item_label);
			PGF_ITEM(ss, new_seqLabels[i], new_seqLabels[i], item_index, lastItem, item->get_pgf_display_color(), itemWidth, item_label, NO_PATTERN);
			if(i > 0 && draw_relations)
			  {
			    int elem_above, prev_seq = i-1;
			    while( prev_seq >= 0 &&
				   (
				    ((elem_above = aligned_matrix[(prev_seq)* matrix_length + j-1]) == NIL) ||
				    (utt_display.sequenceIsHidden(new_seqLabels[prev_seq])))
				   )
			      { prev_seq--; }
			    if(prev_seq >= 0)
			      {draw_arrows(ss, utt_display, new_seqLabels[i], new_seqLabels[prev_seq], item_index);}
			  }
			lastItem = item_index;
		      }
		    else {
		      PGF_ITEM(ss, "blank", new_seqLabels[i], NIL_count, lastItem, "white", itemWidth, "", NO_PATTERN);
		      lastItem = NIL_count--;
		    }
		    PGF_ITEM_SEP_BEFORE(ss, new_seqLabels[i], lastItem, false);
		    PGF_ITEM_SEP_AFTER(ss, new_seqLabels[i], lastItem, false);
		  }
		else
		  {
		    sharp *sh = sharps_ht[HASHINDEX_TO_INDEX(aligned_matrix[i* matrix_length + j-1])];
		    double sharpColWidth = (itemWidth + 2 * INNER_SEP_WIDTH) / (sh->lastElem - sh->firstElem + 1) - 2 * INNER_SEP_WIDTH;
		    std::string pattern = is_star(sh, i, utt_display, new_seqLabels)? STAR_PATTERN_NAME : SHARP_PATTERN_NAME;

		    for(int k = sh->firstElem; k <= sh->lastElem; k++) {
		      item = crtSequence->get_item(k);
		      std::string item_label = get_item_label(item, level);
		      make_label_pgf_compatible(item_label);
		      PGF_ITEM(ss, new_seqLabels[i], new_seqLabels[i], k, lastItem, item->get_pgf_display_color(), sharpColWidth, item_label, pattern);
		      if(i > 0 && draw_relations)
			{
			  int elem_above, prev_seq = i-1;
			  while( prev_seq >= 0 &&
				 (
				  ((elem_above = aligned_matrix[(prev_seq)* matrix_length + j-1]) == NIL) ||
				  (utt_display.sequenceIsHidden(new_seqLabels[prev_seq])))
				 )
			    { prev_seq--; }
			  if(prev_seq >= 0)
			    {draw_arrows(ss, utt_display, new_seqLabels[i], new_seqLabels[prev_seq], k);}
			}
		      PGF_ITEM_SEP_BEFORE(ss, new_seqLabels[i], k, (k != sh->firstElem));
		      PGF_ITEM_SEP_AFTER(ss, new_seqLabels[i], k, (k != sh->lastElem));
		      lastItem = k;
		    }
		  }
		if(j < matrix_length)
		  {
		    itemWidth = colWidth[j];
		  }
	      }
	    }
	  }
      }

    ss << PGF_DOCUMENT_HEADER_END << PGF_HEADER_END <<  std::endl;

    return ss.str().c_str();

  }

  inline int matches(const std::vector<int>* c1, const std::vector<int>* c2)
  {
    bool firstCombWild = true;
    size_t size = c1->size();
    if(size != c2->size()) {
      return false;
    }
    else {
      size_t i = 0;
      while(i < size && (*c1)[i] == (*c2)[i] ) {
	i++;
      }
      if(i == size) {
	return true;
      }
      firstCombWild = ((*c1)[i] == NIL);
      while(i < size) {
	if( (firstCombWild && (*c1)[i] != NIL) || (!firstCombWild && (*c2)[i] != NIL) ) {
	  if( (*c1)[i] != (*c2)[i] ) {
	    return 0;
	  }
	}
	i++;
      }
    }
    if( firstCombWild ) {
      return -1;
    }
    else {
      return 1;
    }
  }

  void RootsDisplay::genCompatibleCombinations(const std::list< std::vector<int>* > & alignedMatrix,
					       const std::vector<boost::unordered_set<int> > & relatedElements,
					       std::vector< std::pair<int, bool> > & matched_col,
					       std::vector< std::vector<int>* > & combinations,
					       std::vector<bool> & usedColumns
					       )
  {
    // Create the compatible history for each row
    size_t height = relatedElements.size();

    std::list<std::vector<int>* >::const_reverse_iterator col_it = alignedMatrix.rbegin();
    for(int col_index = alignedMatrix.size() - 1;
	col_it != alignedMatrix.rend();
	col_it++, col_index--)
      {
	std::vector<int> compCol(height, NIL);
	bool usedThisColumn = true;
	for(size_t row_index = 0; row_index < height; row_index++)
	  {
	    int b = (**col_it)[row_index];
	    if(IS_SHARP(b))
	      {
		sharp* sharpItem = sharps_ht[HASHINDEX_TO_INDEX(b)];

		bool isRelated = false;
		for(int i = sharpItem->firstElem; i <= sharpItem->lastElem; ++i)
		  {
		    boost::unordered_set<int>::const_iterator f = relatedElements[row_index].find(i);
		    if(f != relatedElements[row_index].end())
		      {isRelated=true;break;}
		  }
		if(isRelated) {compCol[row_index] = b;}
	      }
	    else {
	      if(b != NIL)
		{
		  boost::unordered_set<int>::const_iterator f = relatedElements[row_index].find(b);
		  if(f != relatedElements[row_index].end())
		    { compCol[row_index] = b; }
		}
	    }
	    if(b != NIL && compCol[row_index] == NIL)
	      {
		usedThisColumn = false;
	      }
	  }

	usedColumns[col_index] = usedColumns[col_index] | usedThisColumn;

	// check if it was not generated before already (or one that is included or includes this comb)

	int itMatched = 0;
	size_t positionMatched = 0;
	for(; positionMatched < combinations.size(); ++positionMatched)
	  {
	    itMatched = matches(combinations[positionMatched], &compCol);
	    if(itMatched != 0)
	      { break; }
	  }

	if(itMatched <= 0)
	  {
	    std::vector<int>* solution = new std::vector<int>(compCol);

	    if(itMatched == 0)
	      {
		combinations.push_back(solution);
		matched_col.push_back( std::make_pair(col_index, usedThisColumn) );
	      }
	    else
	      {
		delete combinations[positionMatched];
		combinations[positionMatched] = solution;
		matched_col[positionMatched] = std::make_pair(col_index, usedThisColumn);
	      }
	  }

      }
  }
  /**
   * initialises the matrix with the first sequence
   */
  void RootsDisplay::initMatrix(std::list<std::vector<int>* > &matrix, sequence::Sequence &firstSequence)
  {
    int seqSize = firstSequence.count();
    if(seqSize == 0) {
      // if the sequence has no items put a NIL in the matrix
      std::vector<int>* newColumn = new std::vector<int>(1, NIL);
      matrix.push_back(newColumn);
    }
    else {
      for(int i = 0; i < seqSize; i++) {
	std::vector<int>* newColumn = new std::vector<int>(1, i);
	matrix.push_back(newColumn);
      }
    }

  }


  // Note : utt is not constant since we use the relation cache !
  /**
   * aligns the sequences to a matrix
   * a utteranceDisplay is a proxy that mimics the behavior of a utterance
   * providing the posibility to see each level of a sequence that contain trees
   * as a different sequence
   */
  void RootsDisplay::toAlignedMatrix(UtteranceDisplay & utt,
				     const std::vector<std::string> & seqLabels)
  {
    //Call clear function
    clean_mem();

    std::vector<std::string> sequencesInMatrix; //the sequences that are already aligned to the matrix
    std::list< std::vector<int>* > alignedMatrix;
    std::list<std::vector<int>* >::reverse_iterator col_it;

    //init matrix with first sequence
    std::string firstSequenceLabel = seqLabels[0];
    sequence::Sequence* firstSequence = utt.get_sequence(firstSequenceLabel);
    initMatrix(alignedMatrix, *firstSequence);
    sequencesInMatrix.push_back(firstSequenceLabel);

    //init sharpstable for first sequence
    sharps_table.resize(seqLabels.size());
    sharps_table[0].resize(firstSequence->count(), 0);

    for(size_t seq_index = 1; seq_index < seqLabels.size(); seq_index++) {
      std::string crtSeqLabel = seqLabels[seq_index];
      sequence::Sequence* crtSeq = utt.get_sequence( crtSeqLabel );

      int seqSize = crtSeq->count();
      sharps_table[seq_index].resize(seqSize, 0);

      std::vector< std::vector<int>* > combinations;
      std::vector< std::pair<int, bool> > matched_col;
      std::vector<bool> usedColumns(alignedMatrix.size(), false);
      std::multimap< int, std::vector<int>* > sortedCombinations;
      std::multimap< int, std::vector<int>* > exactlyMatchedCombinations;

      std::vector<boost::unordered_set<int> > relatedElements1(sequencesInMatrix.size());
      std::vector<boost::unordered_set<int> > relatedElements2(sequencesInMatrix.size());
      std::vector<boost::unordered_set<int> > * currentRelatedElements = & relatedElements1;
      bool isFirst = true;

      int crtItem = 0;
      for( ; crtItem < seqSize; crtItem++)
	{

	  for(size_t j = 0; j < sequencesInMatrix.size(); j++)
	    {
	      Relation *rel = const_cast<UtteranceDisplay&>(utt).get_relation(crtSeqLabel,
									      sequencesInMatrix[j]);
	      if(rel != NULL)
		{
		  std::vector<int> relEls = rel->get_related_elements(crtItem);
		  (*currentRelatedElements)[j] = boost::unordered_set<int>(relEls.begin(),
									   relEls.end());
		  rel->destroy();
		}
	      else
		{(*currentRelatedElements)[j] = boost::unordered_set<int>();}
	    }
	  if( !isFirst && (relatedElementsEqual(relatedElements1,relatedElements2)))
	    {
	      for(size_t i = 0; i < combinations.size(); i++)
		{
		  std::vector<int>* newCombination = new std::vector<int>(*combinations[i]);
		  newCombination->back() = crtItem;
		  if(matched_col[i].second)  //if exactly matched the column
		    {
		      //first el of matched_col[i] is the index of the matched column
		      exactlyMatchedCombinations.insert( std::make_pair( matched_col[i].first, newCombination) );
		    }
		  else
		    {
		      sortedCombinations.insert( std::make_pair( matched_col[i].first, newCombination) );
		    }
		}
	    }
	  else
	    {
	      combinations.clear();
	      matched_col.clear();

	      genCompatibleCombinations(alignedMatrix, *currentRelatedElements, matched_col, combinations, usedColumns);

	      for(size_t i = 0; i < combinations.size(); i++)
		{
		  combinations[i]->push_back(crtItem);
		  if(matched_col[i].second) //if exactly matched the column
		    {
		      //first el of matched_col[i] is the index of the matched column
		      exactlyMatchedCombinations.insert( std::make_pair( matched_col[i].first, combinations[i]) );
		    }
		  else
		    {
		      sortedCombinations.insert( std::make_pair( matched_col[i].first, combinations[i]) );
		    }
		}
	    }
	  // swap current Relatedelements
	  if(currentRelatedElements == &relatedElements1)
	    {currentRelatedElements = &relatedElements2;}
	  else
	    {currentRelatedElements = &relatedElements1;}

	  isFirst = false;
	}


      col_it = alignedMatrix.rbegin();
      for(size_t col_index = alignedMatrix.size() - 1 ;
	  col_it != alignedMatrix.rend(); col_it++, col_index--)
	{
	  if(!usedColumns[col_index])
	    {
	      (*col_it)->push_back(NIL);
	      exactlyMatchedCombinations.insert( std::make_pair( col_index, *col_it ) );
	    }
	  else
	    {
	      usedColumns[col_index] = false;
	      delete *col_it;
	    }
	}

      alignedMatrix.clear();

      //first insert combinations that exactly matched
      std::multimap< int, std::vector<int>* >::reverse_iterator map_it = exactlyMatchedCombinations.rbegin();
      for(; map_it != exactlyMatchedCombinations.rend(); map_it++)
	{
	  insertColumn(alignedMatrix, map_it->second);
	}

      //then insert rest of the combinations
      map_it = sortedCombinations.rbegin();
      for(; map_it != sortedCombinations.rend(); map_it++)
	{
	  insertColumn(alignedMatrix, map_it->second);
	}

      sequencesInMatrix.push_back(crtSeqLabel);
    }

    postProcessMatrix(alignedMatrix, sequencesInMatrix.size());

    //free memory
    for( col_it = alignedMatrix.rbegin(); col_it != alignedMatrix.rend(); col_it++) {
      delete (*col_it);
    }
  }

  void RootsDisplay::printMatrix(const std::list<std::vector<int>* > &matrix, int height, std::ostream &strout)
  {
    std::cout << "Current Matrix  : "<<  std::endl;
    std::list<std::vector<int>*>::const_iterator col_it;
    for(int row_index = 0; row_index < height; row_index++) {
      col_it = matrix.begin();
      for( ; col_it != matrix.end(); col_it++) {
	if((*col_it)->at(row_index) != NIL) {
	  strout << std::setw(4) << (*col_it)->at(row_index);
	}
	else {
	  strout << std::setw(4) << "N";
	}
      }
      strout <<  std::endl;
    }

    strout <<  std::endl;
  }

  /**
   * DEBUG ONLY
   */
  void RootsDisplay::printSharps()
  {
    std::cout << "All sharps : "<<  std::endl;
    for(size_t i = 0; i< sharps_ht.size(); ++i)
      {
	sharp* sh = sharps_ht[i];
	if(sh != NULL)
	  {
	    std::cout << "\t-"<< i <<" sharp : ";
	    for(int i = sh->firstElem; i <= sh->lastElem; ++i)
	      {	std::cout << i<< " " ; }
	    std::cout <<  std::endl;
	  }
      }
    std::cout <<  std::endl;
  }

  /**
   * checks if the column c2 breaks an item from c1 and c3 if inserted between them
   * an column breaks an item from other two columns if:
   *	there is a row where: item = c1[row] = c3[row] and c2[row] is not equal to item
   *	in other words "item is broken in two by the value on c2[row]"
   */
  bool RootsDisplay::breaksItem(const std::vector<int>* c1, const std::vector<int>* c2, const std::vector<int>* c3) {
    for(size_t i = 0; i < c1->size(); i++) {
      if((*c1)[i] != NIL && (*c1)[i] == (*c3)[i] && (*c2)[i] == NIL) {
	return true;
      }
    }
    return false;

  }


  /**
   * returns -1 if c1 <= c2,
   *	   0 if c1 and c2 are not orderable
   *	   1 if c1 > c2
   */
  int RootsDisplay::orderable(const std::vector<int>* c1, const std::vector<int>* c2)
  {
    bool order = true;
    size_t i = 0;
    while(i < c1->size() && ((*c1)[i] == NIL || (*c2)[i] == NIL || (*c1)[i] == (*c2)[i]) )
      {	i++; }

    if(i < c1->size())
      {
	int a =	 IS_SHARP( (*c1)[i] ) ? sharps_ht[HASHINDEX_TO_INDEX((*c1)[i])]->lastElem : (*c1)[i];
	int b =	 IS_SHARP( (*c2)[i] ) ? sharps_ht[HASHINDEX_TO_INDEX((*c2)[i])]->firstElem  : (*c2)[i];
	order = a < b;
	while(++i < c1->size() )
	  {
	    if( (*c1)[i] != NIL && (*c2)[i] != NIL && (*c1)[i] != (*c2)[i] )
	      {
		a =  IS_SHARP( (*c1)[i] ) ? sharps_ht[HASHINDEX_TO_INDEX((*c1)[i])]->lastElem : (*c1)[i];
		b =  IS_SHARP( (*c2)[i] ) ? sharps_ht[HASHINDEX_TO_INDEX((*c2)[i])]->firstElem	: (*c2)[i];
		if( (a < b) != order) {
		  return 0; //vectors are not orderable
		}
	      }
	  }
      }
    return (order?-1:1);
  }

  /**
   *  inserts a column in the matrix in it's right place to keep the matrix sorted (insertion sort)
   */
  void RootsDisplay::insertColumn(std::list<std::vector<int>* > & matrix, std::vector<int>* column)
  {
    //replace every value in the column by its coresponding sharp (if there is one)
    for(size_t i = 0; i < column->size(); i++) {
      if( !IS_SHARP((*column)[i]) && (*column)[i] != NIL && sharps_table[i][(*column)[i]] ) {
	int sharpKey = sharps_table[i][(*column)[i]];
	(*column)[i] = sharpKey;
      }
    }

    //find where we should insert the new column
    bool isOrderable = true;
    int contBreaker = 0;
    std::list<std::vector<int>* >::iterator first = matrix.begin();
    std::list<std::vector<int>* >::iterator pos = searchPlaceToInsert(first, matrix.end(), column, isOrderable, contBreaker);

    //if the column can be inserted without breaking the order or the continuity we insert it
    if(isOrderable && !contBreaker) {
      matrix.insert(pos, column);
    }
    else {
      //we try to solve the orderability and continuity problems and to insert again
      //until we can insert the column without problems
      do {
	if(!isOrderable) {
	  solveOrder(matrix, first, pos, column, column->size() - 1);
	}
	else {
	  solveContinuity(matrix, first, pos, column, contBreaker); //i am outside the range
	}
	isOrderable = true; contBreaker = 0;
	if( orderable(column, *pos) > 0) {
	  first = pos;
	  pos = searchPlaceToInsert(first, matrix.end(), column, isOrderable, contBreaker);
	}
	else {
	  pos = searchPlaceToInsert(first, pos, column, isOrderable, contBreaker);
	}
      }
      while(!isOrderable || contBreaker);
      matrix.insert(pos, column);
    }

    //remove duplicates
    pos = my_unique(matrix.begin(), matrix.end(), matches);
    while(pos != matrix.end()) {
      matrix.resize( std::distance(matrix.begin(), pos) );
      pos = my_unique(matrix.begin(), matrix.end(), matches);
    }
  }

  /**
   * checks if a sharp is a star
   *   a sharp is a star if it fulfils the following conditions:
   *	 - it does not contain NILS
   *	 - the related elements of every item in the sharp are continuous (there are no gaps)
   *	 - all items in the sharp have the same related elements on every row from the matrix
   */
  bool RootsDisplay::is_star(sharp* sharpRef,
			     size_t row,
			     UtteranceDisplay &utt,
			     std::vector<std::string> sequencesInMatrix)
  {
      //cout << "Is this sharp: " << sharpRef->firstElem << ", " << sharpRef->lastElem << " a star?" <<  std::endl;
    if(sharpRef->containsNILS) {
	//std::cout << "Sharp contains NILS" <<  std::endl;
      return false;
    }

    std::vector<Relation*> relations;
    relations.reserve(sequencesInMatrix.size() - 1);
    std::string crtSeqLabel = sequencesInMatrix[row];
    
    for(size_t j = 0; j < sequencesInMatrix.size(); j++)
      {
	if(j != row) {
	  Relation *rel = const_cast<UtteranceDisplay&>(utt).get_relation(crtSeqLabel,
									  sequencesInMatrix[j]);
	  if(rel != NULL) {
	      relations.push_back(rel);
	  }
	}
      }

    std::vector< std::vector<int> > first_relatedElements;
    for(size_t j = 0; j < relations.size(); j++)
      {
	if(j != row)
	  {
	    first_relatedElements.push_back(relations[j]->get_related_elements(sharpRef->firstElem));
	  }
      }

    //check if elements are continuous in vecRef1
    for(size_t j = 0; j < first_relatedElements.size(); j++) {
      if(first_relatedElements[j].size() == 0) {
	continue;
      }
      std::vector<int >::const_iterator it = first_relatedElements[j].begin();
      int lastNumber = (*it);
      while( ++it != first_relatedElements[j].end())  {
	if( *it - lastNumber != 1) {
	    //std::cout << "Elements are not continuous" <<  std::endl;
	    //std::cout << "\t" << *it << "-" << lastNumber << " != " << 1 <<  std::endl;
	  return false; //is not continuous
	}
	lastNumber = *it;
      }
    }
    //cout << "elements are continuous" <<  std::endl;
    for(int i = sharpRef->firstElem + 1; i <= sharpRef->lastElem; i++) {
      std::vector< std::vector<int> > crt_relatedElements;
      for(size_t j = 0; j < relations.size(); j++)
	{
	  if(j != row)
	    {
	      crt_relatedElements.push_back(relations[j]->get_related_elements(i));
	    }
	}
      if(relatedElementsEqual(first_relatedElements, crt_relatedElements) == false) {
	  //cout << "Related elements are not equal" <<  std::endl;
	  return false;
      }
    }
    
    for(std::vector<Relation *>::iterator it = relations.begin();
	it != relations.end();
	++it)
      {(*it)->destroy();}

    //cout << "Found star" <<  std::endl;
    return true;
  }

  /**
   * modifies the representation of the matrix from list of vectors to a matrix that can
   * be accessed by index and column
   */
  void RootsDisplay::postProcessMatrix(std::list<std::vector<int>* > & matrix, int height)
  {
    std::list<std::vector<int>*>::iterator col_it;

    matrix_length = matrix.size();
    matrix_height = height;
    aligned_matrix = new int[matrix_length*matrix_height];

    for(int row_index = 0; row_index < matrix_height; row_index++) {
      col_it = matrix.begin(); int col_index = 0;
      for( ; col_it != matrix.end(); col_it++, col_index++) {
	aligned_matrix[row_index * matrix_length + col_index] = (*col_it)->at(row_index);
      }
    }
  }

  /**
   * clean the memory after each run of toAlignedMatrix
   */
  void RootsDisplay::clean_mem()
  {
    if(aligned_matrix != NULL)
      { 
	delete[] aligned_matrix;
	aligned_matrix = NULL;
      }

    for(int i = -2; i > sharpsNr; --i)
      { deleteSharp(i); }

    sharps_ht.clear();
    sharps_ht.push_back(NULL); // -0 is not a valid sharp
    sharps_ht.push_back(NULL); // -1 is not a valid sharp

    sharps_table.clear();
    sharpsNr = -1;
  }

  /**
   * delete a sharp from the memory
   */
  void RootsDisplay::deleteSharp(int key)
  {
    sharp *sh = sharps_ht[HASHINDEX_TO_INDEX(key)];

    if(sh != NULL)
      {
	delete sh;
	sharps_ht[HASHINDEX_TO_INDEX(key)] = NULL;
      }
  }

  /**
   * DEBUG ONLY
   */
  void RootsDisplay::printColumn(std::vector<int>* c)
  {
    for(size_t i = 0; i < c->size(); i++) {
      std::cout << (*c)[i] << " ";
    }
    std::cout <<  std::endl;
  }


  /**
   *  UtteranceDisplay constructor
   */
  RootsDisplay::UtteranceDisplay::UtteranceDisplay(Utterance &utt,
						   const std::vector<std::string>& seqLabels,
						   const std::vector<std::string> & hiddensSeqLabels,
						   const std::vector<int> & displayLevels)
  {
    this->utt = &utt;
    // = new std::vector<std::string>();

    for(size_t i = 0; i < seqLabels.size(); i++)
      {
	new_seq_labels.push_back(seqLabels[i]);

	bool isHidden =
	  std::find(hiddensSeqLabels.begin(), hiddensSeqLabels.end(), seqLabels[i]) !=
	  hiddensSeqLabels.end();
	seqIsHidden[seqLabels[i]] = isHidden;
	seqDisplayLevels[seqLabels[i]] = displayLevels[i];

	if(utt.get_sequence(seqLabels[i])->get_classname() ==
	   sequence::SyntaxSequence::classname() )
	  {
	    create_virtual_sequences(seqLabels[i], isHidden, displayLevels[i]);
	  }
      }
    previousCacheUtt = utt.get_use_cached_relation();
    utt.set_use_cached_relation(true);
  }

  /**
   *  UtteranceDisplay destructor
   */
  RootsDisplay::UtteranceDisplay::~UtteranceDisplay()
  {
    //delete new_seq_labels;
    typedef boost::unordered_map<std::string, std::pair<sequence::Sequence*, sequence::Sequence*> > virt_seq_map;
    typedef std::map<std::string, Relation* > virt_rel_map;

    //delete virtual relations
    for(virt_rel_map::iterator it = virtual_relations.begin();
	it != virtual_relations.end();
	++it)
      {	
	it->second->remove_from_utterance(NULL,false); // FIXME TODO WARNING
	it->second->destroy(); 
      }

    //delete virtual sequences
    for(virt_seq_map::iterator it = virtual_sequences.begin();
	it != virtual_sequences.end();
	++it)
      { 
	it->second.first->remove_from_utterance(NULL,false); // FIXME TODO WARNING
	it->second.first->destroy(); 
      }

    //	virtual_sequences.clear();

    // Restore the previous relation cache state
    utt->set_use_cached_relation(previousCacheUtt);
  }

  /**
   *  returns a relation between source_sequence and target_sequence
   */
  Relation* RootsDisplay::UtteranceDisplay::get_relation(const std::string& source_sequence, const std::string& target_sequence)
  {
    typedef boost::unordered_map<std::string, std::pair<sequence::Sequence*, sequence::Sequence*> > virt_seq_map;
    typedef std::map<std::string, Relation* > virt_rel_map;

    virt_seq_map::const_iterator ss = virtual_sequences.find(source_sequence);
    virt_seq_map::const_iterator ts = virtual_sequences.find(target_sequence);

    if(ss == virtual_sequences.end() && ts == virtual_sequences.end())
      {
	return utt->get_relation(source_sequence, target_sequence);
      }
    else
      {
	//search for relation in virtual_relations
	std::string relation_label = source_sequence + "->" + target_sequence;
	virt_rel_map::const_iterator vr_it = virtual_relations.find(relation_label);
	if(vr_it != virtual_relations.end())
	  {
	    return vr_it->second;
	  }

	bool r1_is_virt = false, r2_is_virt = false;
	Relation *r1, *r2;
	if(ss != virtual_sequences.end()) {
	  r1 =	create_virtual_relation((ss->second).first, (ss->second).second);
	  r1_is_virt = true;
	}
	else {
	  r1 =	utt->get_relation(source_sequence, ((ts->second).second)->get_label());
	}
	if(ts != virtual_sequences.end()) {
	  r2 =	create_virtual_relation( (ts->second).first, (ts->second).second, false);
	  r2_is_virt = true;
	}
	else {
	  r2 =	utt->get_relation( ((ss->second).second)->get_label(), target_sequence);
	}
	if(r1 == NULL || r2 == NULL) {
	  return NULL;
	}
	else {
	  Relation *r3 = r1->compound(r2);
	  //TODO: delete virtual relations or save in a map
	  if(r1_is_virt) {
	    r1->destroy();
	  }
	  if(r2_is_virt) {
	    r2->destroy();
	  }
	  r3->add_in_utterance(NULL); // FIXME TODO WARNING
	  virtual_relations.insert(std::make_pair(relation_label,r3));
	  return r3;
	}
      }
  }

  /**
   *  returns the sequence for a sequence label given
   */
  sequence::Sequence* RootsDisplay::UtteranceDisplay::get_sequence(const std::string& label, const std::string& contentType) const
  {
    using namespace sequence;
    typedef boost::unordered_map<std::string, std::pair<sequence::Sequence*, sequence::Sequence*> > virt_seq_map;

    virt_seq_map::const_iterator it = virtual_sequences.find(label);

    //if sequence is in virtual sequences map return it from the map
    if(it != virtual_sequences.end())
      { return (it->second).first; }
    else
      { return utt->get_sequence(label, contentType); }
  }

  bool RootsDisplay::UtteranceDisplay::sequenceIsHidden(const std::string & label)
  {
    // Warning : may return false value if label does not exist !
    return seqIsHidden[label];
  }

  int RootsDisplay::UtteranceDisplay::sequenceDisplayLevel(const std::string & label)
  {
    // Warning : may return false value if label does not exist !
    return seqDisplayLevels[label];
  }

  /**
   *  returns a vector with the labels of all the sequences contained
   */
  const std::vector<std::string>& RootsDisplay::UtteranceDisplay::get_all_sequence_labels() const
  {
    return new_seq_labels;
  }

  /**
   *  creates virtual sequences for each level of SyntaxSequence Tree and adds the virtual
   *  sequences to self
   */
  void RootsDisplay::UtteranceDisplay::create_virtual_sequences(const std::string& label,
								bool isHidden,
								int displayLevel)
  {
    using namespace linguistic::syntax;
    using namespace sequence;

     std::stringstream ss;
    SyntaxSequence *source_seq =
      static_cast<SyntaxSequence*>(utt->get_sequence(label));
    TemplateSequence<SyntaxNode, true> *virt_seq;
    virt_seq = new TemplateSequence<SyntaxNode, true>();
    std::string virt_seq_label;
    int level = 1;
    int seq_size = source_seq->count();
    for(int item = 0; item < seq_size; item++)
      {
	Syntax* st = source_seq->get_item(item);
	virt_seq->add(st->get_root_node());
      }

    while( (seq_size = virt_seq->count()) )
      {
	TemplateSequence<SyntaxNode, true> *new_virt_seq;
	new_virt_seq = new TemplateSequence<SyntaxNode, true>();
	new_virt_seq->add_in_utterance(NULL); // FIXME TODO WARNING
	for(int item = 0; item < seq_size; item++)
	  {
	    SyntaxNode* node = virt_seq->get_item(item);
	    std::vector<tree::TreeNode*> children =
	      node->get_children();

	    for(size_t i = 0; i < children.size(); i++)
	      {
		new_virt_seq->add(children[i]);
	      }
	  }

	if(new_virt_seq->count() > 0)
	  {
	    ss << label << " " << level;
	    level++;
	    virt_seq_label = ss.str().c_str();
	    new_virt_seq->set_label(virt_seq_label);
	    ss.clear();//clear any bits set
	    ss.str(std::string());
	    virtual_sequences.insert(
				     std::make_pair(
						    virt_seq_label,
						    std::make_pair(
								   new_virt_seq,
								   source_seq->get_related_sequence()
								   )
						    )
				     );
	    new_seq_labels.push_back(virt_seq_label);
	    seqIsHidden[virt_seq_label] = isHidden;
	    seqDisplayLevels[virt_seq_label] = displayLevel;
	  }

	virt_seq = new_virt_seq;
      }
  }

  /**
   *  creates a virtual virtual relation between a virtual sequence and it's related sequence
   */
  Relation* RootsDisplay::UtteranceDisplay::create_virtual_relation(
								    sequence::Sequence *virt_seq,
								    sequence::Sequence *related_seq,
								    bool virt_is_source
								    )
  {
    SparseMatrix* mapping;
    if(virt_is_source) {
      mapping = new SparseMatrix(virt_seq->count(), related_seq->count());
    }
    else {
      mapping = new SparseMatrix(related_seq->count(), virt_seq->count());
    }
    Relation *virtual_relation;

    for(size_t i = 0; i < virt_seq->count(); i++) {
      linguistic::syntax::SyntaxNode *item = static_cast<linguistic::syntax::SyntaxNode*>(virt_seq->get_item(i)); //WHAT TODO?
      for(int j = item->get_first_target_index(); j <= item->get_last_target_index(); j++)
	{
	  if(virt_is_source) {
	    mapping->set_element(i, j, 1);
	  }
	  else {
	    mapping->set_element(j, i, 1);
	  }
	}
    }
    if(virt_is_source)
      {
	virtual_relation = new Relation(virt_seq, related_seq, mapping);
      }
    else {
      virtual_relation = new Relation(related_seq, virt_seq, mapping);
    }
    virtual_relation->set_is_composite(true);
    return virtual_relation;
  }

}

