/*
    This file is part of Roots.

    Roots is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Roots is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Roots.  If not, see <http://www.gnu.org/licenses/>.

    Copyright (c) 2013, 2014 IRISA
*/


#ifndef ROOTS_TREE_H_
#define ROOTS_TREE_H_

#include <boost/static_assert.hpp>

#include "common.h"
#include "BaseItem.h"
#include "Tree/TreeNode.h"

namespace roots
{

  namespace tree
  {

    /**
     * @brief Generic tree
     * @details
     * This class models generic trees. The only constraint is that the
     * template type must derive from T.
     * @author Cordial Group
     * @version 0.1
     * @date 2012
     */
    template <class T>
      class Tree: public BaseItem
    {
    public:
      // The template type must derive from T
#ifndef SWIG
      BOOST_STATIC_ASSERT((boost::is_base_of<TreeNode, T >::value));
#endif

      Tree(const bool noInit = false);
      Tree(const Tree<T> &Tree);

      virtual ~Tree();
      /**
       * @brief clone the current object
       * @return a copy of the object
       */
      virtual Tree<T> *clone() const;

      Tree& operator= (const Tree&);

      T* get_root_node() { return rootNode;}

      std::vector<T*> get_leaf_nodes() const;
      std::vector<T*> get_nodes() const;

    /**
     * @brief Modifies the current syllable to take into account the insertion of an element into the related sequence
     * @param index index of the element added in the related sequence
     **/
    virtual void insert_related_element(int index);

    /**
     * @brief Modifies the current syllable to take into account the deletion of an element into the related sequence
     * @param index index of the element removed from the related sequence
     **/
    virtual void remove_related_element(int index);

    public:
      /**
       * @brief returns a string representation of any Roots object
       * @param level the precision level of the output
       * @return string representation of the object
       */
      virtual std::string to_string(int level=0) const;
     
      /**
       * @brief returns the display height for the current object
       * @return display height for the current object
       */
      virtual int get_pgf_height() const;

      /**
       * @brief Write the entity into a RootsStream
       * @param stream the stream from which we inflate the element
       * @param is_terminal indicates that the node is terminal (true by default)
       */
      virtual void deflate (RootsStream * stream, bool is_terminal=true, int list_index=-1);
      /**
       * @brief Extracts the entity from the stream
       * @param stream the stream from which we inflate the element
       */
      virtual void inflate(RootsStream * stream, bool is_terminal=true, int list_index=-1);
      /**
       * @brief Extracts the entity from the stream
       * @param stream the stream from which we inflate the element
       */
      static Tree<T> * inflate_object(RootsStream * stream, int list_index=-1);
      /**
       * @brief returns the XML tag name value for current object
       * @return string constant representing the XML tag name
       */
      virtual std::string get_xml_tag_name() const
      {
	return xml_tag_name();
      };
      /**
       * @brief returns the XML tag name value for current class
       * @return string constant representing the XML tag name
       */
      static std::string xml_tag_name()
      {
	return "tree";
      };
      /**
       * @brief returns classname for current object
       * @return string constant representing the classname
       */
      virtual std::string get_classname() const
      {
	return classname();
      };
      /**
       * @brief returns classname for current class
       * @return string constant representing the classname
       */
      static std::string classname()
      {
	return "Tree";
      };
      /**
       * @brief returns display color for current object
       * @return string constant representing the pgf display color
       */
      virtual std::string get_pgf_display_color() const { return pgf_display_color();};
      /**
       * @brief returns display color for current class
       * @return string constant representing the pgf display color
       */
      static std::string pgf_display_color() { return "red!25"; };
      /**
       * Serialize the object into a stream as a string
       *
       * @param out
       * @param ProsodicPhrase
       * @return the modified stream
       */
      //friend std::ostream& operator<<(std::ostream& out, Tree<T>& tree);


    protected:
      T* rootNode;				/**< the root of the tree */
    };

    
    /**
     * Returns whether the two objects are identical (attributes) or not
     *
     * @param a, first Tree<T> instance
     * @param b, second Tree<T> instance
     * @return true if the objects are identical, false otherwise
     */
    template <class T> bool operator==(Tree<T> const& a, Tree<T> const& b) 
    {
        if (*(a.get_root_node()) == *(b.get_root_node())) 
            return true;
        else
            return false;
    }

    template <class T> Tree<T>::Tree(const bool noInit):BaseItem(noInit)
      {
	rootNode = new T(noInit);
      }

    template <class T> Tree<T>::Tree(const Tree<T> &tree) : BaseItem(tree)
    {
      rootNode = static_cast<T*>(const_cast<Tree<T>&>(tree).get_root_node()->clone_tree());
    }


    template <class T> Tree<T>::~Tree()
      {
	if(rootNode!=NULL)
	  {
	    rootNode->delete_children_and_subtrees();
	    delete rootNode;
	  }
      }

    template <class T> Tree<T> *Tree<T>::clone() const
      {
	return new Tree<T>(*this);
      }


    template <class T> Tree<T>& Tree<T>::operator=(const Tree<T> &node)
      {
	BaseItem::operator=(node);

	// content of the Tree is copied
	if(rootNode!=NULL)
	  delete rootNode;
	rootNode = static_cast<T*>(const_cast<Tree<T>&>(node).get_root_node()->clone_tree()); //=> operator= of T lust duplicate node and children recursively

	return *this;
      }


    template <class T> std::vector<T*> Tree<T>::get_leaf_nodes() const
      {
	std::vector<TreeNode*> leaves;
	std::vector<TreeNode*>::const_iterator iter;
	std::vector<T*> leaves2;
	rootNode->get_leaf_nodes(leaves);

	iter = leaves.begin();
	while(iter != leaves.end())
	  {
	    leaves2.push_back(static_cast<T*>(*iter));
	    ++iter;
	  }

	return leaves2;
      }

    template <class T> std::vector<T*> Tree<T>::get_nodes() const
      {
	std::vector<TreeNode*> leaves;
	std::vector<TreeNode*>::const_iterator iter;
	std::vector<T*> leaves2;
	rootNode->get_nodes(leaves);

	iter = leaves.begin();
	while(iter != leaves.end())
	  {
	    leaves2.push_back(static_cast<T*>(*iter));
	    ++iter;
	  }

	return leaves2;
      }


  template <class T> void Tree<T>::insert_related_element(int index)
  {
    // Modify all the nodes to remove word index and move
    // the other values
    std::vector<T*> nodes = this->get_nodes();

    for(typename std::vector<T*>::iterator it = nodes.begin();
        it != nodes.end();
        ++it)
    {         
        if(index>=(*it)->get_first_target_index())
          (*it)->insert_related_element(index);
    }
  }

  template <class T> void Tree<T>::remove_related_element(int index)
  {   
    //std::cerr << "** remove from tree " << index << std::endl;
    // Modify all the nodes to remove word index and move
    // the other values
    T* target = NULL;
    std::vector<T*> nodes = this->get_nodes();

    for(typename std::vector<T* >::iterator it = nodes.begin();
      it != nodes.end();
      ++it)
    {         
      (*it)->remove_related_element(index);
      if((index>=(*it)->get_first_target_index())
	 && (index<=(*it)->get_last_target_index())) 
	{target = (*it);}
      
    }

    // Remove empty nodes along the path
    if(target != NULL)
      {
	T* parent = dynamic_cast<T*>(target->get_parent());
	
	while(parent!=NULL && (target->get_first_target_index()==-1))
	  {
	    const std::vector<TreeNode*>& children = parent->get_children();
	    unsigned int i=0;     
	    while(i!=children.size() && dynamic_cast<T*>(children[i])!=target) {++i;}      
	    parent->remove_child(i);      
	    target = parent;
	    parent = dynamic_cast<T*>(target->get_parent());
	  }
      }
  }

    template <class T> std::string Tree<T>::to_string( int level) const
      {
    std::stringstream ss;

	switch (level)
	  {
	  case 0:
	  default:
	    ss << "digraph Tree {\n";
	    ss << "ordering=out\n";
	    ss << "node [shape=none]\n";
	    ss << rootNode->output_dot();
	    ss << "}\n";
	    break;
	  }
	return std::string(ss.str().c_str());
      }

    template <class T> int Tree<T>::get_pgf_height() const
      {
	if(rootNode == NULL)
	  return 1;

	return rootNode->get_max_depth(); // + 1 ?
      }


    template <class T> void Tree<T>::deflate (RootsStream * stream, bool is_terminal, int list_index)
      {
	BaseItem::deflate(stream, false, list_index);

	rootNode->deflate_tree(stream);

	if(is_terminal)
	  stream->close_object();
      }

    template <class T> void Tree<T>::inflate(RootsStream * stream, bool is_terminal, int list_index)
      {
	BaseItem::inflate(stream,false,list_index);

	//stream->open_children();

	if(rootNode!=NULL)
	  delete rootNode;
	rootNode = new T(true);
	rootNode->inflate_tree(stream);

	if(is_terminal) stream->close_children();
      }

    template <class T> Tree<T>* Tree<T>::inflate_object(RootsStream * stream, int list_index)
      {
	Tree<T> *t = new Tree<T>(true);
	t->inflate(stream, true, list_index);
	return t;
      }

    

  }
} // End of namespace

#endif /* ROOTS_TREE_H_ */
