/*
    This file is part of Roots.

    Roots is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Roots is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Roots.  If not, see <http://www.gnu.org/licenses/>.

    Copyright (c) 2013, 2014 IRISA
*/

#include "Utterance.h"
#include "Relation.h"
#include "Sequence.h"

namespace roots {

Relation::Relation(sequence::Sequence *source, sequence::Sequence *target)
    : Base(), UtteranceLinkedObject() {
  sourceSequence = source;
  targetSequence = target;
  sourceType = source->get_classname();  // std::string(typeid(*source).name());
  targetType = target->get_classname();  // std::string(typeid(*target).name());
  sourceLabel = source->get_label();
  targetLabel = target->get_label();

  this->set_label(generic_label());
  this->set_is_composite(false);

  mapping = new SparseMatrix(source->count(), target->count());

  propagate_link();
}

Relation::Relation(sequence::Sequence *source, sequence::Sequence *target,
                   Matrix *mapping)
    : Base(), UtteranceLinkedObject() {
  sourceSequence = source;
  targetSequence = target;
  sourceType = std::string(source->get_classname());
  targetType = std::string(target->get_classname());
  sourceLabel = source->get_label();
  targetLabel = target->get_label();

  this->mapping = mapping;

  this->set_label(generic_label());
  this->set_is_composite(false);

  propagate_link();
}

Relation::Relation(bool noInit) : Base(noInit), UtteranceLinkedObject() {
  mapping = NULL;
  sourceSequence = NULL;
  targetSequence = NULL;
  is_composite = false;
}

Relation::~Relation() {
  propagate_remove_link();

  if (mapping != NULL) {
    delete mapping;
  }
}

void Relation::destroy() {
  if (!in_utterance()) {
    delete this;
  }
}

void Relation::merge(const Relation *rel) {
  // TODO : check dimensions 
  mapping->sum(*(rel->get_mapping())); 
}

const std::string &Relation::get_source_label() const { return sourceLabel; }

const std::string &Relation::get_target_label() const { return targetLabel; }

const std::string Relation::get_source_type() const { return sourceType; }

const std::string Relation::get_target_type() const { return targetType; }

void Relation::set_source_label(const std::string &new_label) {
  sourceLabel = new_label;
  this->set_label(generic_label());

  if (in_utterance()) {
       std::vector<Utterance*> all_utts = get_all_utterances();
       for (std::vector<Utterance*>::iterator it = all_utts.begin();
            it != all_utts.end(); ++it) {
         (*it)->update_relation(this);
       }
  }
}

void Relation::set_target_label(const std::string &new_label) {
  targetLabel = new_label;
  this->set_label(generic_label());

  if (in_utterance()) {
       std::vector<Utterance*> all_utts = get_all_utterances();
       for (std::vector<Utterance*>::iterator it = all_utts.begin();
            it != all_utts.end(); ++it) {
         (*it)->update_relation(this);
       }
  }
}

void Relation::set_source_sequence(sequence::Sequence *seq) {
  // Clean before set
  propagate_remove_link();

  sourceSequence = seq;
  sourceType = std::string(seq->get_classname());
  set_source_label(seq->get_label());

  propagate_link();

  ROOTS_LOGGER_DEBUG(std::string("Rel set src: ") +
                     sourceSequence->get_label());
}

void Relation::set_target_sequence(sequence::Sequence *seq) {
  // Clean before set
  propagate_remove_link();

  targetSequence = seq;
  targetType = std::string(seq->get_classname());
  set_target_label(seq->get_label());

  propagate_link();

  ROOTS_LOGGER_DEBUG(std::string("Rel set trg: ") +
                     targetSequence->get_label());
}

sequence::Sequence *Relation::get_source_sequence() const {
  return sourceSequence;
}

sequence::Sequence *Relation::get_target_sequence() const {
  return targetSequence;
}

Matrix *Relation::get_mapping() const { return mapping; }

std::string Relation::generic_label() const {
  return generic_label(this->get_source_label(), this->get_target_label());
}

std::string Relation::generic_label(const std::string &sourceLabel,
                                    const std::string &targetLabel) {
  std::string str = sourceLabel;
  str += " => ";
  str += targetLabel;
  return str;
}

void Relation::no_more_linked(RelationLinkedObject *rlo) {
  if (rlo == (RelationLinkedObject *)sourceSequence) {
    delete this;
  } else if (rlo == (RelationLinkedObject *)targetSequence) {
    delete this;
  }
}

bool Relation::linked(int source_index, int target_index) const {
  return mapping->get_element(source_index, target_index) != 0;
}

void Relation::link(int source_index, int target_index) {
  return mapping->set_element(source_index, target_index, 1);
}

void Relation::unlink(int source_index, int target_index) {
  return mapping->set_element(source_index, target_index, 0);
}

void Relation::fill() {
  int n_source = get_source_sequence()->count();
  int n_target = get_target_sequence()->count();
  for (int i_source = 0; i_source < n_source; i_source++) {
    for (int i_target = 0; i_target < n_target; i_target++) {
      link(i_source, i_target);
    }
  }
}

void Relation::clear() {
  int n_source = get_source_sequence()->count();
  int n_target = get_target_sequence()->count();
  for (int i_source = 0; i_source < n_source; i_source++) {
    for (int i_target = 0; i_target < n_target; i_target++) {
      unlink(i_source, i_target);
    }
  }
}

bool Relation::in_relation(int source_index, int target_index) const {
  return mapping->get_element(source_index, target_index) != 0;
}

std::vector<int> Relation::get_related_elements(int index,
                                                bool is_row_index) const {
  return get_related_indices(index, is_row_index);
}

std::vector<int> Relation::get_related_elements(int beginIndex, int endIndex,
                                                bool is_row_index) const {
  return get_related_indices(beginIndex, endIndex, is_row_index);
}

std::vector<int> Relation::get_related_indices(int index,
                                               bool is_row_index) const {
  std::vector<int> *ptElementIndex;

  if (is_row_index) {
    ptElementIndex = mapping->get_column_index(index);
  } else {
    ptElementIndex = mapping->get_row_index(index);
  }

  std::vector<int> elementIndex(*ptElementIndex);
  /*
    for(int i=0; i< (int)ptElementIndex->size(); i++)
    {
    elementIndex.push_back(ptElementIndex->at(i));
    }
  */

  delete ptElementIndex;

  return elementIndex;
}

std::vector<int> Relation::get_related_indices(int beginIndex, int endIndex,
                                               bool is_row_index) const {
  std::vector<int> *ptElementIndex;
  int minIndex = std::numeric_limits<int>::max();
  int maxIndex = -1;
  for (int currentIndex = beginIndex; currentIndex <= endIndex;
       ++currentIndex) {
    if (is_row_index) {
      ptElementIndex = mapping->get_column_index(currentIndex);
    } else {
      ptElementIndex = mapping->get_row_index(currentIndex);
    }
    for (int i = 0; i < (int)ptElementIndex->size(); i++) {
      int ci = ptElementIndex->at(i);
      //  std::cout<< "I see " << ci << " for " << currentIndex <<
      //  std::endl; // DEBUG
      minIndex = std::min(minIndex, ci);
      maxIndex = std::max(maxIndex, ci);
    }
    delete ptElementIndex;
  }

  std::vector<int> elementIndex;
  if (maxIndex != -1) {
    elementIndex.push_back(minIndex);
    elementIndex.push_back(maxIndex);
  }

  return elementIndex;
}

std::vector<BaseItem *> Relation::get_related_items(int index,
                                                    bool is_row_index) const {
  std::vector<BaseItem *> res;
  std::vector<int> indices = get_related_indices(index, is_row_index);

  for (std::vector<int>::const_iterator it = indices.begin();
       it != indices.end(); ++it) {
    res.push_back(is_row_index ? get_target_sequence()->get_item(*it)
                               : get_source_sequence()->get_item(*it));
  }

  return res;
}

std::vector<BaseItem *> Relation::get_related_items(int beginIndex,
                                                    int endIndex,
                                                    bool is_row_index) const {
  std::vector<BaseItem *> res;
  std::vector<int> indices =
      get_related_indices(beginIndex, endIndex, is_row_index);
  if (!indices.empty()) {
    int target_start = indices[0];
    int target_end = indices[1];
    for (int i = target_start; i <= target_end; i++) {
      res.push_back(is_row_index ? get_target_sequence()->get_item(i)
                                 : get_source_sequence()->get_item(i));
    }
  }
  return res;
}

Relation *Relation::get_reduced_subrelation(sequence::Sequence *source_subseq,
                                            int source_seq_offset,
                                            sequence::Sequence *target_subseq,
                                            int target_seq_offset) const {
  Relation *res = new Relation(source_subseq, target_subseq);

  /* copy sub relation */
  Matrix *newMapping = res->mapping;
  int endIndexSource = source_seq_offset + source_subseq->count();
  int endIndexTarget = target_seq_offset + target_subseq->count();
  int maxColumnIndex = endIndexTarget - target_seq_offset;
  for (int i = source_seq_offset; i < endIndexSource; ++i) {
    std::vector<int> *columnIndex = mapping->get_column_index(i);
    for (std::vector<int>::iterator it = columnIndex->begin();
         it != columnIndex->end(); ++it) {
      int newCi = *it - target_seq_offset;
      /* // DEBUG
          std::cout<< "[ADD] [" <<i <<"-"<<source_seq_offset << " ; " << *it <<
         "-"<<target_seq_offset << "] = ["
         << i - source_seq_offset << " ; " << newCi << "]" << flush;
          std::cout<< " \t = " << mapping->get_element(i,*it) <<  std::endl;
      */
      if ((newCi >= 0) && (newCi < maxColumnIndex)) {
        newMapping->set_element(i - source_seq_offset, newCi,
                                mapping->get_element(i, *it));
      }
    }
  }

  return res;
}

void Relation::set_mapping(Matrix *mapping) {
  if (mapping->get_n_row() != (int)sourceSequence->count() ||
      mapping->get_n_col() != (int)targetSequence->count()) {
    std::stringstream serr;
    serr << "Relation::set_mapping_from: incorrect mapping size : "
         << mapping->get_n_row() << " vs " << (int)sourceSequence->count()
         << " and " << mapping->get_n_col() << " vs "
         << (int)targetSequence->count() << " for " << get_label() << " at "
         << this << std::endl;
    throw RootsException(__FILE__, __LINE__, serr.str().c_str());
    //  throw RootsException(__FILE__, __LINE__, "set_mapping failed due to
    // incorrect mapping size! \n");
  }

  if (this->mapping != NULL) delete this->mapping;
  this->mapping = mapping;
}

void Relation::set_mapping_from_source(const std::vector<int> &mapping) {
  Matrix *newMapping = NULL;
  int index = 0;

  if ((int)mapping.size() != this->get_mapping()->get_n_row()) {
    throw RootsException(__FILE__, __LINE__,
                         "set_mapping failed due to incorrect mapping size!\n");
  }

  newMapping = new SparseMatrix(this->get_mapping()->get_n_row(),
                                this->get_mapping()->get_n_col());

  index = 0;
  while (index < this->get_mapping()->get_n_row()) {
    if (mapping[index] >= 0) {
      newMapping->set_element(index, mapping[index], 1);
    }
    index++;
  }

  if (this->mapping != NULL) delete this->mapping;
  this->mapping = newMapping;
}

void Relation::set_mapping_from_target(const std::vector<int> &mapping) {
  Matrix *newMapping = NULL;
  int index = 0;

  if ((int)mapping.size() != this->get_mapping()->get_n_col()) {
    std::stringstream serr;
    serr << "Relation::set_mapping_from_target: mapping size ("
         << mapping.size() << ") is not equal to the number of columns ("
         << this->get_mapping()->get_n_col() << ")";
    throw RootsException(__FILE__, __LINE__, serr.str().c_str());
  }

  newMapping = new SparseMatrix(this->get_mapping()->get_n_row(),
                                this->get_mapping()->get_n_col());

  index = 0;
  while (index < this->get_mapping()->get_n_col()) {
    if (mapping[index] >= 0) {
      newMapping->set_element(mapping[index], index, 1);
    }
    index++;
  }

  if (this->mapping != NULL) delete this->mapping;
  this->mapping = newMapping;
}

Relation *Relation::clone() const {
  Relation *cloneRel;
  Matrix *map = new SparseMatrix(*(this->get_mapping()));
  cloneRel = new Relation(this->get_source_sequence(),
                          this->get_target_sequence(), map);

  return cloneRel;
}

void Relation::inverse() {
  Matrix *transp = this->get_mapping()->transpose();
  sequence::Sequence *nsource = this->get_target_sequence();
  sequence::Sequence *ntarget = this->get_source_sequence();
  this->set_source_sequence(nsource);
  this->set_target_sequence(ntarget);
  delete this->get_mapping();
  this->mapping = NULL;
  this->set_mapping(transp);
}

void Relation::compound_inplace(Relation *relation, bool rightComposition) {
  Relation *compoundRelation = this->compound(relation, rightComposition);
  //    Matrix *newMapping = new
  // SparseMatrix(*compoundRelation->get_mapping());
  this->set_source_sequence(compoundRelation->get_source_sequence());
  this->set_target_sequence(compoundRelation->get_target_sequence());

  delete this->get_mapping();
  this->mapping = NULL;
  // this->set_mapping(newMapping);
  mapping = compoundRelation->mapping;
  compoundRelation->mapping = NULL;
  delete compoundRelation;

  this->set_is_composite(true);
}

Relation *Relation::compound(Relation *relation, bool rightComposition) {
  sequence::Sequence *sourceSequenceA = NULL, *targetSequenceA = NULL;
  std::string sourceSequenceLabelA, targetSequenceLabelA;
  sequence::Sequence *sourceSequenceB = NULL, *targetSequenceB = NULL;
  sequence::Sequence *sourceSequence = NULL, *targetSequence = NULL;
  std::string sourceSequenceLabelB, targetSequenceLabelB;

  Relation *newRelation;
  Matrix *matrixLeft, *matrixRight, *newMatrix;

  if (relation == NULL) {
    throw RootsException(__FILE__, __LINE__, "relation undefined!\n");
  }

  sourceSequenceA = this->get_source_sequence();
  sourceSequenceLabelA = sourceSequenceA->get_label();
  targetSequenceA = this->get_target_sequence();
  targetSequenceLabelA = targetSequenceA->get_label();

  sourceSequenceB = relation->get_source_sequence();
  sourceSequenceLabelB = sourceSequenceB->get_label();
  targetSequenceB = relation->get_target_sequence();
  targetSequenceLabelB = targetSequenceB->get_label();

  if (rightComposition && (targetSequenceLabelA != sourceSequenceLabelB)) {
    std::stringstream ss;
    ss << "Error on right side of composition: \"" + targetSequenceLabelA +
              "\" != \"" + sourceSequenceLabelB + "\"!\n";
    throw RootsException(__FILE__, __LINE__, ss.str().c_str());
  }

  if (!rightComposition && (sourceSequenceLabelA != targetSequenceLabelB)) {
    std::stringstream ss;
    ss << "Error on left side of composition: \"" + sourceSequenceLabelA +
              "\" != \"" + targetSequenceLabelB + "\"!\n";
    throw RootsException(__FILE__, __LINE__, ss.str().c_str());
  }

  if (rightComposition) {
    sourceSequence = sourceSequenceA;
    targetSequence = targetSequenceB;
    matrixLeft = this->get_mapping();
    matrixRight = relation->get_mapping();
  } else {
    sourceSequence = sourceSequenceB;
    targetSequence = targetSequenceA;
    matrixLeft = relation->get_mapping();
    matrixRight = this->get_mapping();
  }

  SparseMatrix *tmpMatrix = new SparseMatrix(*(matrixLeft));
  newMatrix = tmpMatrix->multiply_right(*matrixRight);
  delete tmpMatrix;

  newRelation = new Relation(sourceSequence, targetSequence, newMatrix);
  newRelation->set_is_composite(true);

  return newRelation;
}

void Relation::insert_source(int index) {
  if (mapping != NULL) {
    mapping->insert_row(index);
    if (get_target_sequence()->get_related_sequence() !=
        get_source_sequence()) {
      get_source_sequence()->insert_related_element(index);
    }
  }
}

void Relation::insert_target(int index) {
  if (mapping != NULL) {
    mapping->insert_column(index);
    if (get_source_sequence()->get_related_sequence() !=
        get_target_sequence()) {
      get_target_sequence()->insert_related_element(index);
    }
  }
}

void Relation::refresh_relation_size() {
  if (mapping != NULL) {
    mapping->resize(sourceSequence->count(), targetSequence->count());
  } else {
    mapping =
        new SparseMatrix(sourceSequence->count(), targetSequence->count());
  }
}

void Relation::remove_source(int index, bool propagateEmbeded) {
  if (mapping != NULL) {
    if ((propagateEmbeded) && (get_target_sequence()->get_related_sequence() ==
                               get_source_sequence())) {
      get_target_sequence()->remove_related_element(index);
    }
    mapping->remove_row(index);
  }
}

void Relation::remove_target(int index, bool propagateEmbeded) {
  if (mapping != NULL) {
    if ((propagateEmbeded) && (get_source_sequence()->get_related_sequence() ==
                               get_target_sequence())) {
      get_source_sequence()->remove_related_element(index);
    }
    mapping->remove_column(index);
  }
}

void Relation::propagate_link() {
  BaseLink<Relation> link(this);
  if (sourceSequence != NULL) {
    sourceSequence->add_link(link);
  }
  if (targetSequence != NULL) {
    targetSequence->add_link(link);
  }
}

void Relation::propagate_remove_link() {
  BaseLink<Relation> link(this);
  if (sourceSequence != NULL) {
    sourceSequence->remove_link(link, false);
  }
  if (targetSequence != NULL) {
    targetSequence->remove_link(link, false);
  }
}

std::ostream &operator<<(std::ostream &out, roots::Relation &rel) {
  out << "Relation:" << std::endl;
  out << "\tlabel: " << rel.get_label() << std::endl;
  out << "\tsource_label:" << rel.get_source_label() << std::endl;
  out << "\ttarget_label:" << rel.get_target_label() << std::endl;
  out << "\tcomposite:" << rel.get_is_composite() << std::endl;
  out << "\tmapping:" << std::endl;
  out << *(rel.get_mapping());
  return out;
}

std::string Relation::to_string(int level) const {
  std::stringstream ss;
  switch (level) {
    case 0:
      ss << get_label() << std::endl;
      break;
    case 1:
      ss << get_label() << std::endl;
      ss << "\tcomposite:" << get_is_composite() << std::endl;
      ss << "\tmapping:" << std::endl;
      ss << "\t" << get_mapping()->to_string(level);
      ss << std::endl;
      break;
    default:
      ss << get_label() << std::endl;
      ss << "\tcomposite:" << get_is_composite() << std::endl;
      ss << "\tmapping:" << std::endl;
      ss << "\t" << get_mapping()->to_string(level);
      ss << std::endl;
      sequence::Sequence *src_seq = get_source_sequence();
      sequence::Sequence *tgt_seq = get_target_sequence();
      size_t src_n = src_seq->count();
      size_t tgt_n = tgt_seq->count();
      ss << "\t" << src_seq->get_label() << " -> " << tgt_seq->get_label()
         << ":" << std::endl;
      for (size_t i = 0; i < src_n; i++) {
        ss << "\t\t" << src_seq->get_item(i)->to_string() << " -> "
           << std::endl;
        std::vector<int> indices = get_related_indices(i);
        for (std::vector<int>::iterator j = indices.begin(); j != indices.end();
             ++j) {
          ss << "\t\t\t" << tgt_seq->get_item(*j)->to_string() << std::endl;
        }
      }
      ss << "\t" << tgt_seq->get_label() << " -> " << src_seq->get_label()
         << ":" << std::endl;
      for (size_t i = 0; i < tgt_n; i++) {
        ss << "\t\t" << tgt_seq->get_item(i)->to_string() << " -> "
           << std::endl;
        std::vector<int> indices = get_related_indices(i, false);
        for (std::vector<int>::iterator j = indices.begin(); j != indices.end();
             ++j) {
          ss << "\t\t\t" << src_seq->get_item(*j)->to_string() << std::endl;
        }
      }
  }
  return std::string(ss.str().c_str());
}

void Relation::deflate(RootsStream *stream, bool is_terminal, int list_index) {
  Base::deflate(stream, false, list_index);

  stream->append_unicodestring_content("source_sequence_id",
                                       this->get_source_sequence()->get_id());
  stream->append_unicodestring_content("target_sequence_id",
                                       this->get_target_sequence()->get_id());
  // stream->append_unicodestring_content("source_type",
  // this->get_source_type());
  // stream->append_unicodestring_content("target_type",
  // this->get_target_type());
  // stream->append_unicodestring_content("source_label",
  // this->get_source_label());
  // stream->append_unicodestring_content("target_label",
  // this->get_target_label());
  stream->append_bool_content("is_composite", this->get_is_composite());

  if (this->get_mapping() == NULL) {
    throw RootsException(__FILE__, __LINE__, "Mapping matrix is not defined!");
  }
  this->get_mapping()->deflate(stream);

  if (is_terminal) stream->close_object();
}

void Relation::inflate(RootsStream *stream, bool is_terminal, int list_index) {
  std::string id;
  roots::sequence::Sequence *seq = NULL;

  Base::inflate(stream, false, list_index);

  // stream->open_children();

  id = stream->get_unicodestring_content("source_sequence_id");
  seq = (roots::sequence::Sequence *)stream->get_object_ptr(id);
  if (seq == NULL) {
    std::stringstream ss;
    ss << "cannot find sequence with id=" << id
       << " while inflating Relation object!";
    std::cerr << ss.str().c_str() << std::endl;
    throw RootsException(__FILE__, __LINE__, ss.str().c_str());
  }
  this->set_source_sequence(seq);

  id = stream->get_unicodestring_content("target_sequence_id");
  seq = (roots::sequence::Sequence *)stream->get_object_ptr(id);
  if (seq == NULL) {
    std::stringstream ss;
    ss << "cannot find sequence with id=" << id
       << " while inflating Relation object!";
    std::cerr << ss.str().c_str() << std::endl;
    throw RootsException(__FILE__, __LINE__, ss.str().c_str());
  }
  this->set_target_sequence(seq);

  this->set_is_composite(stream->get_bool_content("is_composite"));

  Matrix *mat = (Matrix *)SparseMatrix::inflate_object(stream);
  this->set_mapping(mat);

  if (is_terminal) stream->close_children();

  this->propagate_link();
}

Relation *Relation::inflate_relation(RootsStream *stream, int list_index) {
  Relation *t = new Relation(true);
  t->inflate(stream, true, list_index);
  return t;
}
}
