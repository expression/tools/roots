/*
    This file is part of Roots.

    Roots is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Roots is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Roots.  If not, see <http://www.gnu.org/licenses/>.

    Copyright (c) 2013, 2014 IRISA
*/

#ifndef RELATIONGRAPH_H_
#define RELATIONGRAPH_H_

#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/astar_search.hpp>
#include <boost/utility.hpp>  // for boost::tie

#include "Relation.h"
#include "Sequence.h"
#include "common.h"

namespace roots {

/**
 * @brief Represent a graph of relations
 * @details
 * @author Talma research group
 * @version 0.1
 * @date 2012
 */
class RelationGraph {
 public:
  /**
   * @brief Vertex Structure for the Relation Graph
   */
  struct RelationGraphVertex {
    sequence::Sequence *sequence_reference; /**< Pointer to the sequence */
  };

  /**
   * @brief Edge Structure for the Relation Graph
   */
  struct RelationGraphEdge {
    int weight;                   /**< weight of the edge */
    Relation *relation_reference; /**< Pointer to the relation associated to the
                                     edge */
    bool relation_is_composite;   /**< true if the relation is composite */
    bool relation_is_inverse; /**< true if the relation is the inverse of the
                                 arc */
  };

  /**
   *  @brief type that describes a node in a path through relation
   *
   */
  struct RelationPathNode {
    sequence::Sequence *sourceSequence; /**< Pointer to the source sequence */
    sequence::Sequence *targetSequence; /**< Pointer to the target sequence */
    Relation *relation;                 /**< Pointer to the relation */
    bool is_inverse; /**< true if the relation has to be inverted */
  };

  typedef boost::adjacency_list<boost::listS, boost::vecS, boost::directedS,
                                RelationGraphVertex, RelationGraphEdge>
      Graph;
  typedef boost::adjacency_list<boost::listS, boost::vecS, boost::directedS,
                                RelationGraphVertex,
                                RelationGraphEdge>::vertex_descriptor VertexID;
  typedef boost::adjacency_list<boost::listS, boost::vecS, boost::directedS,
                                RelationGraphVertex,
                                RelationGraphEdge>::edge_descriptor EdgeID;
  typedef boost::adjacency_list<
      boost::listS, boost::vecS, boost::directedS, RelationGraphVertex,
      RelationGraphEdge>::vertex_iterator VertexIterator;
  typedef boost::adjacency_list<
      boost::listS, boost::vecS, boost::directedS, RelationGraphVertex,
      RelationGraphEdge>::out_edge_iterator OutEdgeIterator;

  /**
   * @brief Default constructor
   */
  RelationGraph();
  ~RelationGraph();
  RelationGraph *clone() const;

  void free_managed_relations();

  /**
   * @brief Removes all edges and all vertices
   **/
  void clear();

  bool exists_relation(const std::string &srcSeqLabel,
                       const std::string &trgSeqLabel);
  /**
  * @note Just search unsing label, content is not checked.
  **/
  bool exists_relation(Relation *relation);
  /**
  * @warning If the relation is composite, only one edge will be added in the relation graph, else, both direction are inserted !
  **/
  void add_relation(Relation *relation);
  std::vector<roots::Relation *> get_out_related_relation(
      const std::string &seqLabel);
  /**
   * @brief remove a relation from the graph. Memory is not free.
   * @param relation retlation to remove
   **/
  void remove_relation(Relation *relation);
  std::vector<Relation *> get_relation_path(const std::string &source_sequence,
                                            const std::string &target_sequence,
                                            std::vector<bool> &inverseRelation);

  bool exists_sequence(const std::string &seq);
  /**
  * @note Just search unsing label, content is not checked.
  **/
  bool exists_sequence(sequence::Sequence *seq);
  void remove_sequence(sequence::Sequence *seq);
  std::vector<sequence::Sequence *> get_related_sequence(
      const std::string &seqLabel);

  std::string to_dot(void);

  RelationGraph::VertexID get_vertex_id(const std::string &seqLabel);
  RelationGraph::EdgeID get_edge_id(const std::string &srcSeqLabel,
                                    const std::string &trgSeqLabel);
  RelationGraph::EdgeID get_edge_id(Relation *relation, bool direct);
  RelationGraph::RelationGraphEdge get_edge(
      const std::string &srcSeqLabel,
      const std::string &trgSeqLabel);

 protected:
 private:
  Graph relationGraph;

  /**
   * @brief Exception launched when goal is found (a_star search path in the
   * relation graph).
   */
  struct found_goal {};

  class astar_goal_visitor : public boost::default_astar_visitor {
   public:
    astar_goal_visitor(VertexID goal) : m_goal(goal) {}

    void examine_vertex(VertexID u, const Graph &g __attribute__((unused))) {
      if (u == m_goal) throw found_goal();
    }

   private:
    VertexID m_goal;
  };
};
}

#endif /* RELATIONGRAPH_H_ */
