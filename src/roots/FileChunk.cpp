/*
    This file is part of Roots.

    Roots is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Roots is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Roots.  If not, see <http://www.gnu.org/licenses/>.

    Copyright (c) 2013, 2014 IRISA
*/


#include "Corpus.h"
#include "FileChunk.h"
#include <boost/filesystem.hpp>
#include <boost/lexical_cast.hpp>


namespace bfs = boost::filesystem;

namespace roots
{

  const std::string FileChunk::DEFAULT_ALTERNATE_TEMP_DIR = "/tmp";

  /*************** Constructor / Destructor / Copy ********************/
  FileChunk::FileChunk(): Base()
  {
    baseDir	  = "";
    nbUtterances  = 0;
    layersId	  = std::map<std::string, int>();
    files	  = std::vector<std::string>();
    loadedLayers  = std::vector<bool>();
    layers	  = std::vector<roots::Corpus>();
    readyForBackup = std::vector<bool>();
    currentlyBackedUp = std::vector<bool>();
    tempFiles = std::vector<std::string>();
    alternateBaseDir = DEFAULT_ALTERNATE_TEMP_DIR;
    tempDir = "";
  }

  FileChunk::FileChunk(const std::string chunk_rel_path,
		       const std::string corpus_abs_dir,
		       const int nbUtterances): Base()
  {
    baseDir	  = "";
    this->nbUtterances	= nbUtterances;
    layersId	  = std::map<std::string, int>();
    files	  = std::vector<std::string>();
    loadedLayers  = std::vector<bool>();
    layers	  = std::vector<roots::Corpus>();
    readyForBackup = std::vector<bool>();
    currentlyBackedUp = std::vector<bool>();
    tempFiles = std::vector<std::string>();
    alternateBaseDir = DEFAULT_ALTERNATE_TEMP_DIR;
    tempDir = "";

    set_base_dir(corpus_abs_dir);
    add_filename(chunk_rel_path);
  }

  FileChunk::FileChunk(const std::map<std::string, std::string>& layersToFiles,
		       const std::string corpus_abs_dir,
		       const int nbUtterances): Base()
  {
    baseDir	  = "";
    this->nbUtterances	= nbUtterances;
    layersId	  = std::map<std::string, int>();
    files	  = std::vector<std::string>();
    loadedLayers  = std::vector<bool>();
    layers	  = std::vector<roots::Corpus>();
    readyForBackup = std::vector<bool>();
    currentlyBackedUp = std::vector<bool>();
    tempFiles = std::vector<std::string>();
    alternateBaseDir = DEFAULT_ALTERNATE_TEMP_DIR;
    tempDir = "";

    set_base_dir(corpus_abs_dir);
    add_filenames(layersToFiles);
  }

  FileChunk::FileChunk(const MemoryChunk& chunk,
		       const std::map<std::string, std::string>& layersToFiles,
		       const std::string corpus_abs_dir
		       ): Base()
  {
    baseDir	  = "";
    nbUtterances  = 0;
    layersId	  = std::map<std::string, int>();
    files	  = std::vector<std::string>();
    loadedLayers  = std::vector<bool>();
    layers	  = std::vector<roots::Corpus>();
    readyForBackup = std::vector<bool>();
    currentlyBackedUp = std::vector<bool>();
    tempFiles = std::vector<std::string>();
    alternateBaseDir = DEFAULT_ALTERNATE_TEMP_DIR;
    tempDir = "";

    set_base_dir(corpus_abs_dir);
    add_filenames(layersToFiles);
    add_utterances(chunk.get_utterances());
  }

  FileChunk::~FileChunk()
  {
    // Remove unsaved temp files
    clean_backup();
  }
  
  void FileChunk::destroy()
  {
    delete this;
  }

  FileChunk::FileChunk(const FileChunk& fchunk) : Base(fchunk)
  {
    baseDir	  = fchunk.get_base_dir();
    nbUtterances  = fchunk.count_utterances();
    layersId	  = fchunk.layersId;
    files	  = fchunk.files;
    loadedLayers  = fchunk.loadedLayers;
    layers	  = fchunk.layers;
    readyForBackup = fchunk.readyForBackup;
    // Generate temp file names if fchunk had such files
    int n = fchunk.count_layers();
    for (int i = 0; i < n; i++) {
      tempDir = "";
      if (fchunk.ready_for_backup(i)) {
	prepare_backup(i);
	if (fchunk.currentlyBackedUp[i]) {
	  bfs::copy_file(fchunk.tempFiles[i], tempFiles[i], bfs::copy_option::overwrite_if_exists);
	}
      }
    }
    currentlyBackedUp = fchunk.currentlyBackedUp;
    tempFiles = fchunk.tempFiles;
    alternateBaseDir = fchunk.alternateBaseDir;
  }

  FileChunk& FileChunk::operator= (const FileChunk& fchunk)
  {
    if(this != &fchunk)
      {
	Base::operator=(fchunk);
	baseDir	     = fchunk.get_base_dir();
	nbUtterances = fchunk.count_utterances();
	layersId     = fchunk.layersId;
	files	     = fchunk.files;
	loadedLayers = fchunk.loadedLayers;
	layers	     = fchunk.layers;
	readyForBackup = fchunk.readyForBackup;
	// Generate temp file names if fchunk had such files
	int n = fchunk.count_layers();
	for (int i = 0; i < n; i++) {
	  tempDir = "";
	  if (fchunk.ready_for_backup(i)) {
	    prepare_backup(i);
	    if (fchunk.currentlyBackedUp[i]) {
	      bfs::copy_file(fchunk.tempFiles[i], tempFiles[i], bfs::copy_option::overwrite_if_exists);
	    }
	  }
	}
	currentlyBackedUp = fchunk.currentlyBackedUp;
	tempFiles = fchunk.tempFiles;
	alternateBaseDir = fchunk.alternateBaseDir;
      }

    return *this;
  }

  FileChunk *FileChunk::clone() const
  {
    return new FileChunk(*this);
  }

  BaseChunk *FileChunk::clone_to_basechunk() const
  {
    return new FileChunk(*this);
  }

  /*************** Getters / Setters ********************/
  void FileChunk::set_base_dir(const std::string& baseDir)
  {
    this->baseDir = baseDir;
  }

  const std::string& FileChunk::get_base_dir() const
  {
    return baseDir;
  }

  int FileChunk::count_utterances() const
  {
    //TODO Think about the consistency of the value (need to load something before asking the count?).
    return nbUtterances;
  }

  void FileChunk::update_counts()
  {
    nbUtterances = 0;
    if(!layersId.empty())
      {
	int id = 0;
	load_layer(id);
	update_counts(id);
      }
  }

  void FileChunk::update_counts(int layerId)
  {
    nbUtterances = layers[layerId].count_utterances();
  }

  /*************** Utterance management ********************/

  void FileChunk::delete_utterance(int id)
  {
    load_all();
    update_counts();
    for(std::vector<roots::Corpus>::iterator it = layers.begin();
	it != layers.end();
	++it)
      {it->delete_utterance(id);}
    update_counts();
    if (count_utterances() == 0) {
      clear();
    }
    if (!ready_for_backup()) { prepare_backup(); }
  }

  void FileChunk::clear()
  {
    baseDir	  = "";
    nbUtterances  = 0;
    layersId.clear();
    files.clear();
    loadedLayers.clear();
    layers.clear();
  }

  roots::Utterance* FileChunk::get_utterance(int id)
  {
    roots::Utterance* res = NULL;
    for(std::map<std::string, int>::const_iterator it = layersId.begin();
	it != layersId.end();
	++it) {
      load_in_utterance(it->first, id, res);
    }

    return res;
  }

  roots::Utterance* FileChunk::get_utterance(const std::vector<std::string> & layerToLoad,
					     int id)
  {
    roots::Utterance* res = NULL;
    for(std::vector<std::string>::const_iterator it = layerToLoad.begin();
	it != layerToLoad.end();
	++it){
      load_in_utterance(*it, id, res);
    }

    return res;
  }

  void FileChunk::load_in_utterance(const std::string & layerToAdd,
				    int index,
				    roots::Utterance *& utterance)
  {
    int layerId = get_layer_id(layerToAdd);

    load_layer(layerId);
    if(utterance == NULL)
      { utterance = layers[layerId].get_utterance(index); } // get_utterance produce a copy
    else
      {
	roots::Utterance* tmp  = layers[layerId].get_utterance(index); // get_utterance produce a copy
	utterance->merge_utterance(tmp);
	delete tmp;
      }
  }


  void FileChunk::add_utterance(const Utterance &utt, const std::string layerName)
  {
    size_t nb_layers = layersId.size();
    // Not the first utterance nor the correct number of layers
    if (nb_layers != 1 || layersId.begin()->first != layerName)
      {
	std::stringstream ss;
	ss << "FileChunk::add_utterance: Uncompatible layer <" << layerName << ">!";
	throw RootsException(__FILE__, __LINE__, ss.str().c_str());
      }

    int id = get_layer_id(layerName);
    load_layer(id);
    update_counts(id);
    layers[id].add_utterance(utt,layerName);
    if (!ready_for_backup(id)) { prepare_backup(id); }
    update_counts(id);
  }

  void FileChunk::add_utterance(const std::map<std::string, Utterance> & utt)
  {
    size_t nb_layers = layersId.size();
    // Not the first utterance nor the correct number of layers
    if (utt.size() != nb_layers) {
      throw RootsException(__FILE__,__LINE__, "add_utterance: Wrong number of layers");
    }

    std::map<std::string, Utterance>::const_iterator it;
    for ( it = utt.begin(); it != utt.end(); ++it)
      {
	if (layersId.find(it->first) == layersId.end())
	  {
	    std::stringstream ss;
	    ss << "FileChunk::add_utterance: Unknown layer <" << it->first << ">!";
	    throw RootsException(__FILE__, __LINE__, ss.str().c_str());
	  }
      }

    load_all();
    update_counts();

    // Add into each layer
    for (it = utt.begin(); it != utt.end(); ++it) {
      int id = get_layer_id(it->first);
      layers[id].add_utterance(it->second, it->first);
      if (!ready_for_backup(id)) { prepare_backup(id); }
    }

    update_counts();
  }

  void FileChunk::add_utterances(const std::vector<Utterance>& utt,
				 const std::string layerName)
  {
    size_t nb_layers = layersId.size();
    // Not the first utterance nor the correct number of layers
    if (nb_layers != 1 || layersId.begin()->first != layerName)
      {
	std::stringstream ss;
	ss << "FileChunk::add_utterances: Uncompatible layer <" << layerName << ">!";
	throw RootsException(__FILE__, __LINE__, ss.str().c_str());
      }
    int id = get_layer_id(layerName);
    load_layer(id);
    update_counts(id);
    layers[id].add_utterances(utt,layerName);
    if (!ready_for_backup(id)) { prepare_backup(id); }
    update_counts(id);
  }

  void FileChunk::add_utterances(const std::map<std::string, std::vector<Utterance> >& utt)
  {
    size_t nb_layers = layersId.size();
    // Not the first utterance nor the correct number of layers
    if (utt.size() != nb_layers) {
      throw RootsException(__FILE__,__LINE__, "add_utterance: Wrong number of layers");
    }

    std::map<std::string, std::vector<Utterance> >::const_iterator it;

    for (it = utt.begin(); it != utt.end(); ++it)
      {
	if (layersId.find(it->first) == layersId.end())
	  {
	    std::stringstream ss;
	    ss << "FileChunk::add_utterances: Unknown layer <" << it->first << ">!";
	    throw RootsException(__FILE__, __LINE__, ss.str().c_str());
	  }
      }

    load_all();
    update_counts();

    // Add into each layer
    for (it = utt.begin(); it != utt.end(); ++it) {
      int id = get_layer_id(it->first);
      layers[id].add_utterances(it->second, it->first);
      if (!ready_for_backup(id)) { prepare_backup(id); }
    }

    update_counts();
  }


  void FileChunk::update_utterance(int id, const Utterance& utt)
  {
    size_t nb_layers = files.size();
    // Check if considered layer is implicit
    if (nb_layers > 1)
      {
	std::stringstream ss;
	ss << "FileChunk::update_utterance: current chunk is made of multiple layers! Please use a map to specify considered layer.";
	throw RootsException(__FILE__, __LINE__, ss.str().c_str());
      }

    // Load the only layer
    load_layer(0);

    // Call update_utterance on the referenced corpus
    layers[0].update_utterance(id, utt);
    if (!ready_for_backup(0)) { prepare_backup(0); }
  }


  void FileChunk::update_utterance(int id, const std::map<std::string, Utterance> &utt)
  {
    std::map<std::string, Utterance>::const_iterator it;
    for ( it = utt.begin(); it != utt.end(); ++it)
      {
	if (layersId.find(it->first) == layersId.end())
	  {
	    std::stringstream ss;
	    ss << "FileChunk::update_utterance: Unknown layer <" << it->first << ">!";
	    throw RootsException(__FILE__, __LINE__, ss.str().c_str());
	  }
      }

    for ( it = utt.begin(); it != utt.end(); ++it)
      {
	load_layer(it->first);
      }

    update_counts();

    // Add into each layer
    for (it = utt.begin(); it != utt.end(); ++it)
      {
	int layer_id = get_layer_id(it->first);
	layers[layer_id].update_utterance(id, it->second);
	if (!ready_for_backup(layer_id)) { prepare_backup(layer_id); }
      }
  }



  /*************** Loading/Save functions ********************/

  void FileChunk::load_all()
  {
    for(unsigned int id = 0; id < layers.size(); ++id)
      { load_layer(id); }
  }

  bool FileChunk::is_loaded_for_utterance(const int utt_id)
  {
    for(unsigned int id = 0; id < layers.size(); ++id)
      { 
	if( (! is_loaded_layer(id)) || 
	    (!layers[id].is_loaded_for_utterance(utt_id)))
	  {return false;}
      }  
    return true;
  }

  void FileChunk::load_layer(const std::string& layerName)
  {
    load_layer(get_layer_id(layerName));
  }

  void FileChunk::load_layer(const int& id)
  {
    if(!is_loaded_layer(id))
      {
	const std::string & filename = (currentlyBackedUp[id]?tempFiles[id]:files[id]);
	roots::RootsStream * ptRootsStreamLoader = NULL;
	if (endsWith(filename,".json"))
	  {
	    ptRootsStreamLoader = new RootsStreamJson();
	  }
#ifdef USE_STREAM_XML
	else if(endsWith(filename,".xml"))
	  {
	    ptRootsStreamLoader = new RootsStreamXML();
	  }
#endif
	else
	  {
	    std::stringstream ss;
	    ss << "FileChunk::load_layer: unknown file extension for file <" << filename << ">!";
	    throw RootsException(__FILE__, __LINE__, ss.str().c_str());
	  }

	if(baseDir != "" && !currentlyBackedUp[id])
	  {ptRootsStreamLoader->load(baseDir +"/"+ filename);}
	else
	  {ptRootsStreamLoader->load(filename);}
	layers[id].inflate(ptRootsStreamLoader);
	delete ptRootsStreamLoader;
	loadedLayers[id] = true;
	currentlyBackedUp[id] = false;
      }
  }

  void FileChunk::unload_all()
  {
    for(unsigned int id = 0; id < layers.size(); ++id)
      { unload_layer(id); }
  }

  void FileChunk::unload_layer(const std::string& layerName)
  {
    unload_layer(get_layer_id(layerName));
  }

  void FileChunk::unload_layer(const int& id)
  {
    if(is_loaded_layer(id))
      {
	// Save to a temporary file if modified
	if (ready_for_backup(id)) {
	  layers[id].save(tempFiles[id]);
	  currentlyBackedUp[id] = true;
	}
	layers[id] = roots::Corpus();
	loadedLayers[id] = false;
      }
  }

  bool FileChunk::is_loaded_layer(const std::string& layerName) const
  {
    int id = get_layer_id(layerName);
    return is_loaded_layer(id);
  }

  bool FileChunk::is_loaded_layer(const int& id) const
  {
    return loadedLayers[id];
  }

  // Files will be stored in base / dirname(files[i]) / temp_dir / key + filename(files[i])
  // where temp_dir is unique and base is the base_dir of
  // the file chunk unless write access is not permitted.
  void FileChunk::prepare_backup(const int & id)
  {
    int start, end;
    if (id == -1) { start = 0; end = count_layers(); }
    else { start = id; end = id+1; }

    // Create a unique dirname if not defined yet
    bfs::path temp_dir;
    if (tempDir == "") {
      
      temp_dir = bfs::path(boost::lexical_cast<std::string>(getpid()));//string_format("%i",getpid()));
      tempDir = temp_dir.string();
    }
    else {
      temp_dir = bfs::path(tempDir);
    }

    // Check for write permission of base dir and, potentially, change the dir
    bfs::path base(get_base_dir());
    // WARNING: always write into the temp directory if permission cannot be tested (Boost <1.47.0)
    bool writable = true;
#if BOOST_VERSION < 104700
    writable = false;
#else
    for (int i = start; i < end; i++)
      {
	bfs::path original_path(files[i]);
	bfs::path temp_path = base / original_path.parent_path();
	bfs::file_status s = status(temp_path);
	if (!(s.permissions()&bfs::others_write))
	{
	  writable = false;
	  break;
	}
      }
#endif
    if (!writable) {
      base = bfs::path(alternateBaseDir);
    }

    // Generate new file name for each layer
    for (int i = start; i < end; i++) {
      bfs::path original_path(files[i]);
      bfs::path key = bfs::unique_path("#temp_%%%%_");
      bfs::path file_name(key.string() + original_path.filename().string());
      bfs::path temp_path = base / original_path.parent_path() / temp_dir / file_name;
      tempFiles[i] = temp_path.string();
      readyForBackup[i] = true;
    }
  }

  void FileChunk::clean_backup(const int& id, const std::string& dir_to_move)
  {
    int start, end;
    if (id == -1) { start = 0; end = count_layers(); }
    else { start = id; end = id+1; }
    bool move = (dir_to_move != "");
    for (int i = start; i < end; i++) {
      // If temp file exists
      if (tempFiles[i] != "") {
	bfs::path from(tempFiles[i]);
	// Move if backup is the latest version (ie, layer is currently in memory)
	if (currentlyBackedUp[i] && move) {
	  bfs::path to(dir_to_move);
	  to /= bfs::path(files[i]);
	  // Check if target dirname must be created
	  if (!boost::filesystem::exists(to.parent_path())) {
	    boost::filesystem::create_directories(to.parent_path());
	  }
	  try {
	    bfs::rename(from, to);
	  }
	  catch (bfs::filesystem_error e) {
	    bfs::copy_file(from, to, bfs::copy_option::overwrite_if_exists);
	    bfs::remove(from);
	  }
	}
	// Remove otherwise
	else {
	  bfs::remove(from);
	}
	currentlyBackedUp[i] = false;
	if (bfs::exists(from.parent_path()) && bfs::is_empty(from.parent_path())) {
	  bfs::remove(from.parent_path());
	}
      }
      readyForBackup[i] = false;
      tempFiles[i] = "";
    }
    tempDir = "";
  }

  bool FileChunk::ready_for_backup(int id) const
  {
    if (id == -1) {
      int n = count_layers();
      bool ret = true;
      for (int i = 0; i < n; i++) {
	ret &= readyForBackup[i];
      }
      return ret;
    }
    else {
      return readyForBackup[id];
    }
  }

  void FileChunk::change_alternate_temp_dir(const std::string & new_alt_dir)
  {
    alternateBaseDir = new_alt_dir;
  }

  bool FileChunk::save(const std::string & target_base_dir)
  {
    bool ret = false;
    
    bfs::path source_path(get_base_dir());
    bfs::path target_path(target_base_dir);
#if BOOST_VERSION < 104800
    	  bfs::path canonical_source = bfs::canonical(source_path);
    	  bfs::path canonical_target = bfs::canonical(target_path);
#else
    //WARNING: to be checked
    bfs::path canonical_source = source_path.normalize();
    bfs::path canonical_target = target_path.normalize();
#endif
    // If difference between target basedir and current basedir
    if (canonical_source != canonical_target) {
      // Then, change basedir
      set_base_dir(target_base_dir);
      // If backup is on, then move temp file to the target dir
      // Otherwise, save each layer to the target dir
      for (size_t i = 0; i < layers.size(); i++) {
	if (currentlyBackedUp[i]) {
	  clean_backup(i, target_base_dir);
	}
	else {
	  layers[i].save(target_base_dir+"/"+files[i]);
	}
      }
      ret = true;
    }
    // Otherwise, save only if file chunk has been modified
    // or move if file chunk has been backed up
    else {
      for (size_t i = 0; i < layers.size(); i++) {
	if (is_loaded_layer(i) && layers[i].unsaved_changes()) {
	  layers[i].save(target_base_dir+"/"+files[i]);
	  ret = true;
	}
	else if (currentlyBackedUp[i]) {
	  clean_backup(i, target_base_dir);
	  ret = true;
	}
      }
    }
    return ret;
  }

  void FileChunk::deflate(RootsStream * stream, bool is_terminal, int list_index)
  {
    Base::deflate(stream,false,list_index);

    stream->append_unicodestring_content("chunk_name", this->get_label());
    stream->append_int_content("n_utterances", count_utterances());
    std::map< std::string, std::string> * filenames = get_filenames();
    stream->append_map_unistr_unistr_content("layers",*filenames);
    delete filenames;

    if(is_terminal)
      stream->close_object();
  }

  void FileChunk::inflate(RootsStream * stream, bool is_terminal, int list_index)
  {
    clear();

    Base::inflate(stream,false,list_index);

    set_label(stream->get_unicodestring_content("chunk_name"));
    nbUtterances = stream->get_int_content("n_utterances"); // may not be up to date !
    set_base_dir(stream->get_base_dir_name());

    add_filenames(stream->get_map_unistr_unistr_content("layers"));

    if(is_terminal) stream->close_children();
  }

  /* Layer managment */

  int FileChunk::count_layers() const
  {
    return layersId.size();
  }

  std::vector<std::string> FileChunk::get_all_layer_names() const
  {
    std::vector<std::string> layersNames;
    for(std::map<std::string, int>::const_iterator it = layersId.begin();
	it != layersId.end();
	++it)
      { layersNames.push_back(it->first); }
    return layersNames;
  }

  int FileChunk::get_layer_id(const std::string & layerName) const
  {
    std::map<std::string, int>::const_iterator it = layersId.find(layerName);
    if(it == layersId.end())
      {
	std::stringstream ss;
	ss << "FileChunk::get_layer_id: unknown layer name <"<< layerName<< "> ";
	throw RootsException(__FILE__, __LINE__, ss.str().c_str());
      }

    return it->second;
  }

  void FileChunk::remove_layer(const std::string& layerName)
  {
    int id = get_layer_id(layerName);
    files.erase(files.begin() + id);
    loadedLayers.erase(loadedLayers.begin() + id);
    layers.erase(layers.begin() + id);
    layersId.erase(layerName);
  }

  /* File name managment */

  std::map< std::string, std::string> * FileChunk::get_filenames() const
  {
    std::map< std::string, std::string> * res =
      new std::map< std::string, std::string>();
    for(std::map<std::string, int>::const_iterator it = layersId.begin();
	it != layersId.end();
	++it)
      { (*res)[it->first] = get_filename(it->second); }
    return res;
  }

  const std::string &	 FileChunk::get_filename(const std::string& layerName) const
  {
    int id = get_layer_id(layerName);
    return get_filename(id);
  }

  const std::string &	 FileChunk::get_filename(const int& id)const
  {
    return files[id];
  }

  void FileChunk::set_filename(const std::string& layerName, const std::string& filename)
  {
    int id = get_layer_id(layerName);
    files[id] = filename;
  }

  void FileChunk::add_filenames(const std::map<std::string, std::string>& fileNames)
  {
    for(std::map<std::string, std::string>::const_iterator it = fileNames.begin();
	it != fileNames.end();
	++it)
      { add_filename(it->first, it->second); }
  }

  void FileChunk::add_filename(const std::string& layerName, const std::string& fileName)
  {
    int id = layers.size();
    layersId.insert(std::make_pair(layerName,id));
    loadedLayers.push_back(false);
    readyForBackup.push_back(false);
    currentlyBackedUp.push_back(false);
    layers.push_back(roots::Corpus());
    files.push_back(fileName);
    tempFiles.push_back("");
  }

  void FileChunk::add_filename(const std::string& fileName)
  {
    int id = layers.size();
    layersId.insert(std::make_pair(ANONYMOUS_LAYER,id));
    loadedLayers.push_back(false);
    readyForBackup.push_back(false);
    currentlyBackedUp.push_back(false);
    layers.push_back(roots::Corpus());
    files.push_back(fileName);
    tempFiles.push_back("");
  }


} // END OF NAMESPACE
