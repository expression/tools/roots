/*
    This file is part of Roots.

    Roots is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Roots is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Roots.  If not, see <http://www.gnu.org/licenses/>.

    Copyright (c) 2013, 2014 IRISA
*/


#include "BaseChunk.h"

namespace roots
{
  const std::string BaseChunk::ANONYMOUS_LAYER = "__ANONYMOUS_LAYER__";

  BaseChunk::~BaseChunk() {}

  std::string BaseChunk::to_string(int level)
  {
    std::stringstream ss;

    for (int i = 0; i < count_utterances(); ++i)
      {
	ss << "################################################" << std::endl;
	ss << "# Utterance " << i << std::endl;
	ss << "################################################" << std::endl;
	ss << get_utterance(i)->to_string(level);
	if (i < count_utterances() - 1) 
	  { ss << std::endl; }
      }

    return std::string(ss.str().c_str());
  }

} // END OF NAMESPACE

