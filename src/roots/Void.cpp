/*
    This file is part of Roots.

    Roots is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Roots is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Roots.  If not, see <http://www.gnu.org/licenses/>.

    Copyright (c) 2013, 2014 IRISA
*/


#include "Void.h"

roots::Void::Void(const bool noInit) : BaseItem(noInit)
{
}

roots::Void::Void(const Void& aVoid) : BaseItem(aVoid)
{
}

roots::Void::~Void()
{

}

roots::Void *roots::Void::clone() const
{
  return new Void(*this);
}

roots::Void& roots::Void::operator= (const Void& aVoid)
{
  BaseItem::operator=(aVoid);

  return *this;

}

std::string roots::Void::to_string(int level) const
{
  std::stringstream ss;

  switch (level)
    {
    case 0:
    default:
      ss << ".";
      break;
    }
  return std::string(ss.str().c_str());
}

void roots::Void::deflate(RootsStream * stream, bool is_terminal, int list_index)
{
	BaseItem::deflate(stream,false,list_index);
	

	if(is_terminal)
		stream->close_object();
}

void roots::Void::inflate(RootsStream * stream, bool is_terminal, int list_index)
{
	BaseItem::inflate(stream,false,list_index);

	if(is_terminal) stream->close_children();
}

roots::Void * roots::Void::inflate_object(RootsStream * stream, int list_index)
{
	Void *t = new Void(true);
	t->inflate(stream,true,list_index);
	return t;	
}
