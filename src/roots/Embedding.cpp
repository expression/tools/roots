/*
    This file is part of Roots.

    Roots is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Roots is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Roots.  If not, see <http://www.gnu.org/licenses/>.

    Copyright (c) 2013, 2014 IRISA
*/


#include "Embedding.h"

namespace roots
{
  Embedding::Embedding(const bool noInit) : BaseItem(noInit)
  {
    // Nothing to do
  }
  
  Embedding::Embedding(const std::vector<float>& _value) : BaseItem()
  {
    set_value(_value);
  }
  
//  Embedding::Embedding(const std::string & str_value) : BaseItem()
//  {
//    set_value(x);
//  }
  
  Embedding::Embedding(const Embedding & to_be_copied) : BaseItem(to_be_copied)
  {
    //set_value(to_be_copied.get_value());
    value.reserve(to_be_copied.value.size());
    for (const auto& i:to_be_copied.value )
    {
      value.emplace_back(i);
    }
  }
  
  Embedding::~Embedding() {
    // Nothing to do
  }
  
  Embedding* Embedding::clone() const
  {
    return new Embedding(*this);
  }
  
  Embedding& Embedding::operator=(const Embedding & to_be_copied) {
    BaseItem::operator=(to_be_copied);
    return *this;
  }

  std::vector<float> Embedding::get_value() const
  {
    return value;
  }
  
  void Embedding::set_value(const std::vector<float>& _value)
  {
    //value = _value;
    value.clear();
    value.reserve(_value.size());
    for (const auto& i:_value )
    {
      value.emplace_back(i);
    }
  }
  
  // IO methods
  std::string Embedding::to_string(int /*level*/) const
  {
    std::stringstream ss;
    //ss << get_value();
    for (const auto& i:value )
    {
      ss<<i<<" ";
    }
    return ss.str();
  }
  
  void Embedding::deflate(RootsStream * stream, bool is_terminal, int list_index) {
    BaseItem::deflate(stream,false,list_index);
  
    stream->append_vector_float_content("value", value);

    if(is_terminal)
      stream->close_object();
  }
  
  void Embedding::inflate(RootsStream * stream, bool is_terminal, int list_index)
  {
    BaseItem::inflate(stream,false,list_index);

    value.clear();
    value = stream->get_vector_float_content("value");
    
    if(is_terminal) stream->close_children();
  }
  
  Embedding * Embedding::inflate_object(RootsStream * stream, int list_index)
  {
    Embedding *n = new Embedding(true);
    n->inflate(stream,true,list_index);
    return n; 
  }
}
