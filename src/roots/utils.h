/*
    This file is part of Roots.

    Roots is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Roots is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Roots.  If not, see <http://www.gnu.org/licenses/>.

    Copyright (c) 2013, 2014 IRISA
*/


#ifndef ROOTS_UTILS_UTILS_H_
#define ROOTS_UTILS_UTILS_H_

#include "common.h"

#include <unicode/utypes.h>   /* Basic ICU data types */
#include <unicode/ucnv.h>     /* C   Converter API    */
#include <unicode/ustring.h>  /* some more string fcns*/
#include <unicode/uchar.h>    /* char names           */
#include <unicode/uloc.h>
#include <unicode/unistr.h>
#include <unicode/decimfmt.h>
#include <unicode/ustream.h>
#include <unicode/fmtable.h>
#include <unicode/numfmt.h>
#include <unicode/regex.h>
#include <unicode/smpdtfmt.h>
#include <unicode/gregocal.h>


inline std::wstring* ustring_to_wstring(const icu::UnicodeString &str)
{
	wchar_t* str2 = NULL;
	UChar* dest = new UChar[str.length() + 1];
	int32_t destlength = 0;

	UErrorCode status = U_ZERO_ERROR;
	str.extract (dest, str.length(), status);

	str2 = new wchar_t[str.length()+1];
	u_strToWCS(str2, str.length()+1, &destlength, dest, str.length(),&status);

	std::wstring *str3 =  new std::wstring(str2);
	return str3;
}

inline icu::UnicodeString* wstring_to_ustring(const std::wstring &str)
{
	UChar* dest = new UChar[str.length() + 1];
	int32_t destlength = 0;

	UErrorCode status = U_ZERO_ERROR;

	dest = new UChar[str.length()+1];
	u_strFromWCS(dest, str.length()+1, &destlength, str.c_str(), -1, &status);

	icu::UnicodeString *str3 =  new icu::UnicodeString(dest);
	return str3;
}

inline std::string unistr2stdstr(const icu::UnicodeString & unistr)
{
  std::stringstream ss;
  ss << unistr;
  return ss.str();
    /*std::string str;
    unistr.toUTF8String(str);
    return str;*/
}

inline icu::UnicodeString stdstr2unistr(const std::string & stdstr)
{
  return icu::UnicodeString(stdstr.c_str());
}

inline std::string string_format(const std::string & fmt, ...) {
    int size = 100;
    std::string str;
    va_list ap;
    while (1) {
        str.resize(size);
        va_start(ap, fmt);
        int n = vsnprintf((char *)str.c_str(), size, fmt.c_str(), ap);
        va_end(ap);
        if (n > -1 && n < size) {
            str.resize(n);
            return str;
        }
        if (n > -1)
            size = n + 1;
        else
            size *= 2;
    }
    return str;
}


#endif // ROOTS_UTILS_UTILS_H_
