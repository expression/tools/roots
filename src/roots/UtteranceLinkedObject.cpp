/*
    This file is part of Roots.

    Roots is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Roots is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Roots.  If not, see <http://www.gnu.org/licenses/>.

    Copyright (c) 2013, 2014 IRISA
*/


#include "UtteranceLinkedObject.h"
#include "Utterance.h"

namespace roots
{

  UtteranceLinkedObject::UtteranceLinkedObject()
  {
    linkHash = boost::unordered_set<BaseLink<Utterance> >();
  }


  UtteranceLinkedObject::UtteranceLinkedObject(const UtteranceLinkedObject& utt_link)
  {
    this->linkHash.clear();
    this->linkHash = utt_link.linkHash;
  }


  UtteranceLinkedObject::~UtteranceLinkedObject()
  {
    for(boost::unordered_set<BaseLink<Utterance> >::const_iterator it = linkHash.begin();
	it != linkHash.end();
	++it)
      {	
	if(it->get_link() != NULL)
	  {it->get_link()->no_more_linked(this); }
      }
  }


  void UtteranceLinkedObject::add_in_utterance(Utterance *utt)
  {
    linkHash.insert(BaseLink<Utterance>(utt));
  }


  bool UtteranceLinkedObject::remove_from_utterance(Utterance *utt, bool informUtt)
  {

    if(informUtt && utt != NULL){utt->no_more_linked(this);}
    int nbErased = linkHash.erase(BaseLink<Utterance>(utt));
    if(nbErased != 0)
      {return true;}
    return false;
  }


  int UtteranceLinkedObject::count_utterances() const
  {
    return linkHash.size();
  }


  bool UtteranceLinkedObject::in_utterance() const
  {
    return (linkHash.size() > 0);
  }

  std::vector< BaseLink< Utterance > > UtteranceLinkedObject::get_all_utterance_links() const
  {
    std::vector<BaseLink<Utterance> > vec;

    for(boost::unordered_set<BaseLink<Utterance> >::const_iterator it = linkHash.begin();
	it != linkHash.end();
	++it)
      { 
	if(it->get_link() != NULL)
	  {vec.push_back(*it); }
      }

    return vec;
  }


  std::vector< Utterance* > UtteranceLinkedObject::get_all_utterances() const
  {
    std::vector< Utterance* > res;

    for (boost::unordered_set<BaseLink<Utterance> >::const_iterator it = linkHash.begin();
	 it != linkHash.end();
	 ++it)
      {	
	if(it->get_link() != NULL)
	  {res.push_back(it->get_link());}
      }

    return res;
  }

  UtteranceLinkedObject& UtteranceLinkedObject::operator=(const UtteranceLinkedObject& utt_link)
  {
    this->linkHash = utt_link.linkHash;
    return *this;
  }

  boost::unordered_set<BaseLink<Utterance> >& UtteranceLinkedObject::get_hash()
  {
    return linkHash;
  }

  void UtteranceLinkedObject::set_hash(const boost::unordered_set<BaseLink<Utterance> >& hash)
  {
    linkHash = hash;
  }


}
