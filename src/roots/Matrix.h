/*
  This file is part of Roots.

  Roots is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Roots is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with Roots.  If not, see <http://www.gnu.org/licenses/>.

  Copyright (c) 2013, 2014 IRISA
*/


#ifndef MATRIX_H_
#define MATRIX_H_
#include "common.h"
#include "RootsException.h"
#include "RootsStream.h"

#include <boost/numeric/ublas/matrix.hpp>
#include <boost/numeric/ublas/io.hpp>

namespace roots {

  // forward declaration of the class Base
  class Base;
  namespace sequence {
    // forward declaration of the class sequence
    class Sequence;
  }

  /**
   * @brief Generic matrix
   * @details
   * This class is abstract and provides a common interface to Matrix inplementations.
   * It has to be overloaded
   *
   * @author Cordial Group
   * @version 0.1
   * @date 2011
   */
  class Matrix {
  protected:
    /**
     * @brief Default constructor
     */
    Matrix();

  public:
    /**
     * @brief Destructor
     */
    virtual ~Matrix();

    /**
     * @brief Returns the number of columns
     * @return number of columns in the matrix
     */
    virtual int get_n_col() const = 0 ;

    /**
     * @brief Returns the number of rows
     * @return number of rows in the matrix
     */
    virtual int get_n_row() const = 0 ;

    /**
     * @brief Modifies the value of an element in the matrix
     * @param row row index, starts from 0
     * @param col columns index, starts from 0
     * @param elem the new value of the element
     */
    virtual void set_element( int row,  int col, int elem) = 0;

    /**
     * @brief Returns the value of an element in the matrix
     * @param row row index, starts from 0
     * @param col columns index, starts from 0
     * @return pointer to the linked object
     */
    virtual int get_element( int row,  int col) const = 0;

    /**
     * @brief Returns the column indices that are present in the matrix for a given row.
     * This method finds the non-zero values on a given row and returns the corresponding
     * column indices.
     * @param row the row index to examine
     * @return a vector containing the column indices if any
     */
    virtual std::vector<int>* get_column_index(int row) = 0;

    /**
     * @brief Returns the row indices that are present in the matrix for a given column.
     * This method finds the non-zero values on a given column and returns the corresponding
     * row indices.
     * @param col the column index to examine
     * @return a vector containing the row indices if any
     */
    virtual std::vector<int>* get_row_index(int col) = 0;

    /**
     * @brief Removes the row in the matrix at a given index
     * @param row the row index to examine
     */
    virtual void remove_row(int row) = 0;

    /**
     * @brief Removes the column in the matrix at a given index.
     * @param col the column index to examine
     */
    virtual void remove_column(int column) = 0;

    /**
     * @brief Add nul rows and columns at the bottom and right of the matrix.
     * @param nrow new number of row in the matrix
     * @param ncol new number of collumn in the matrix
     */
    virtual void resize(int nrow, int ncol) = 0;
    
    /**
     * @brief Inserts the row in the matrix at a given index.
     * If row is bigger than the number of rows, then appends the row
     * @param row the row index to examine
     */
    virtual void insert_row(int row) = 0;

    /**
     * @brief Inserts the column in the matrix at a given index.
     * If column is bigger than the number of columns, then appends the column
     * @param col the column index to examine
     */
    virtual void insert_column(int column) = 0;

    /**
     * @brief shift the content of the matrix by i column and j row. 
     * @note Content that goes outside of the matrix size is deleted.
     * @param row shift by this number of row
     * @param col new shift by this number of column
     */
    virtual void shift(int row, int col) = 0;

    /**
     * @brief Returns the matrix sum result of (this + matrix)
     * @param matrix the matrix to sum with
     */
    virtual void sum(const Matrix& matrix) = 0;

    /**
     * @brief Returns the number of non-zero values in the matrix
     * @return number of non-zero values in the matrix
     */
    virtual int get_nb_element() = 0;

    /**
     * @brief Returns the transposed matrix of the current one
     * @return Matrix*
     */
    virtual Matrix* transpose() = 0;

    /**
     * @brief Returns the matrix multiplication result of (this x matrix)
     * @param matrix the matrix to multiply with
     */
    virtual Matrix* multiply_right(const Matrix& matrix) = 0;

    /**
     * @brief Returns a vector containing the sum of each row
     * @return vector<int>
     */
    virtual std::vector<int>* sum_rows();

    /**
     * @brief Returns a vector containing the sum of each column
     * @return vector<int>
     */
    virtual std::vector<int>* sum_columns();

    /**
     * @brief Write the entity into a RootsStream
     * @param stream the stream from which we inflate the element
     * @param is_terminal indicates that the node is terminal (true by default)
     * @param parentNode
     */
    virtual void deflate(RootsStream * stream, bool is_terminal=true, int list_index=-1);
    /**
     * @brief Extracts the entity from the stream
     * @param stream the stream from which we inflate the element
     */
    virtual void inflate(RootsStream * stream, bool is_terminal=true, int list_index=-1);

    /**
     * @brief returns the XML tag name value for current object
     * @return string constant representing the XML tag name
     */
    virtual std::string get_xml_tag_name() const { return xml_tag_name(); };

    /**
     * @brief returns the XML tag name value for current class
     * @return string constant representing the XML tag name
     */
    static std::string xml_tag_name() { return "matrix"; };

    /**
     * @brief returns classname for current object
     * @return string constant representing the classname
     */
    virtual std::string get_classname() const { return classname(); };

    /**
     * @brief returns classname for current class
     * @return string constant representing the classname
     */
    static std::string classname() { return "Matrix"; };

    /**
     * @brief Returns a raw text string representation of the current matrix
     * @param level precision level of the output (default: 0)
     * @return A unicode string
     */
    std::string to_string(int level = 0) const;

    friend std::ostream& operator<<(std::ostream& out, const Matrix& mat);
  };
}
#endif /* MATRIX_H_ */
