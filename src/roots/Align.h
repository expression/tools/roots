/*
  This file is part of Roots.

  Roots is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Roots is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	See the
  GNU Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with Roots.  If not, see <http://www.gnu.org/licenses/>.

  Copyright (c) 2013, 2014 IRISA
*/


#ifndef ALIGN_H_
#define ALIGN_H_

#include "common.h"
#include "RootsException.h"
#include "Base.h"

#include <boost/numeric/ublas/matrix.hpp>
#include <boost/numeric/ublas/io.hpp>
#include <limits>


namespace roots {

  namespace align {
    /**
     * @brief Element of the alignement path
     */
    struct PathElement
    {
      int i;			/**< index of the element in the source sequence */
      int j;			/**< index of the element in the target sequence */
      bool rowInsert;	/**< true if an element is inserted in the source sequence */
      bool colInsert;	/**< true if an element is inserted in the target sequence */
    };
  }

  /**
   * @brief This class provides generic methods for aligning sequences
   * @details
   * This is an abstract class. In derived class the local_cost_function must be
   * defined. See Roots::Aligne::Levenshtein for an example.
   *
   * @author Cordial Group
   * @version 0.1
   * @date 2011
   */
  template <class T>
    class Align {
  public:

    /**
     * @brief Empty constructor
     */
    Align()
      {
	rowArray = std::vector<T>();
	colArray = std::vector<T>();
	cost = -1;
	edit_distance = 0;
	dissimilarity = Base::MAX_SIMILARITY;
      }

    /**
     * @brief Default constructor
     * @param source_sequence source sequence of Ts
     * @param target_sequence target sequence of Ts
     */
    Align(std::vector<T>& source_sequence, std::vector<T>& target_sequence)
      {
	rowArray = source_sequence;
	colArray = target_sequence;
	cost = -1;
	edit_distance = 0;
	dissimilarity = Base::MAX_SIMILARITY;
      }

    /**
     * @brief Destructor
     */
    virtual ~Align(){};

    /**
     * @brief Modifies the vector of source elements
     * @param array New source elements
     */
    virtual void set_source_vector(std::vector<T> array) { rowArray = array;} ;

    /**
     * @brief Modifies the vector of target elements
     * @param array New target elements
     */
    virtual void set_target_vector(std::vector<T> array) { colArray = array;} ;

    /**
     * @brief Returns the vector of source elements
     * @return A vector
     */
    virtual std::vector<T> get_source_vector() const { return rowArray;};

    /**
     * @brief Returns the vector of target elements
     * @return A vector
     */
    virtual std::vector<T> get_target_vector() const { return colArray;};

    /**
     * @brief Returns the warping window coefficients
     * @details Each element of the map corresponds to a direction specified by the
     * algorithm which is implemented. The directions are available in the
     * warpingDirection vector.
     * @return map of coefficients
     */
    virtual std::map<int, double> get_warping_window() const { return warpingMultiplier;};

    /**
     * @brief Returns the source sequence
     * @details Each element of the map corresponds to a direction specified by the
     * algorithm which is implemented. The directions are available in the
     * warpingDirection vector.
     * @return map of coefficients
     */
    virtual std::map<int, double> get_warping_offset() const { return warpingOffset;};

    /**
     * @brief Modifies the vector of constraints
     * @details the elements of the vector are not substituted during alignement.
     * @param array new constraints
     */
    virtual void set_constraint(std::vector<T> array) { constraint = array;};

    /**
     * @brief Returns the vector of constraints
     * @return constrained elements as a vector
     */
    virtual std::vector<T> get_constraint() const { return constraint;};

    /**
     * @brief Returns the source mapping
     * @details The source mapping returned is the sequence of elements indices
     * obtained after alignement. In the resultant vector, inserted elements are
     * marked with a negative index.
     * @return source mapping
     */
    virtual std::vector<int> get_source_mapping() const { return sourceMapping;};

    /**
     * @brief Returns the target mapping
     * @details The source mapping returned is the sequence of elements indices
     * obtained after alignement. In the resultant vector, inserted elements are
     * marked with a negative index.
     * @return target mapping
     */
    virtual std::vector<int> get_target_mapping() const { return targetMapping;};

    /**
     * @brief Synonymous of method \ref Align::align .
     */
    virtual void dpa_standard()
    {
      align();
    }

    /**
     * @brief Align source and target sequences with a dynamic programming algorithm.
     * @details This algorithm is parameterized by the warpingWindow, warpingOffset,
     * warpingDirection and constraint attributes. The three first attributes
     * are of equal length, and elements they contain have to correspond, i.e.
     * to one direction has to be associated a window coefficient and an offset
     * value. The result of the alignment is stored in two vectors accessible
     * via the get_source_mapping and get_target_mapping accessors.
     */
    virtual void align()
    {
      int nrow = rowArray.size()+1;
      int ncol = colArray.size()+1;
      int k, l;
      double *cumulatedCost = new double[nrow*ncol];
      std::pair<int, int> *backtrackArray = new std::pair<int, int>[nrow*ncol];
      std::map<int, double> warpingWindow = this->get_warping_window();
      std::map<int, double> warpingOffset = this->get_warping_offset();

      sourceMapping = std::vector<int>();
      targetMapping = std::vector<int>();

      for(int i=0; i<nrow; i++)
	{
	  for(int j=0; j<ncol; j++)
	    {
	      cumulatedCost[i*ncol+j] = 0.0;
	      backtrackArray[i*ncol+j].first = 0;
	      backtrackArray[i*ncol+j].second = 0;
	    }
	}
      for (unsigned int w=0; w<warpingWindow.size(); w++) {
	if ((warpingDirection[w].first == -1) && (warpingDirection[w].second == 0)) {
	  for(int i=1; i<nrow; i++) {
	    cumulatedCost[i*ncol+0] = i*warpingOffset[w];
	    backtrackArray[i*ncol+0] = warpingDirection[w];
	  }
	}
	if ((warpingDirection[w].first == 0) && (warpingDirection[w].second == -1)) {
	  for(int i=1; i<ncol; i++) {
	    cumulatedCost[i] = i*warpingOffset[w];
	    backtrackArray[i] = warpingDirection[w];
	  }
	}
      }


      if(warpingWindow.size() != warpingOffset.size() ||
	 warpingDirection.size() != warpingOffset.size() ||
	 warpingWindow.size() != warpingDirection.size())
	{
	  std::stringstream ss;
	  ss << "warpingDirection, warpingWindow or warpingOffset are not initialized correctly!";
	  throw RootsException(__FILE__, __LINE__, ss.str().c_str());
	}


      for(k=1; k<nrow; k++)
	{
	  for(l=1; l<ncol; l++)
	    {
	      double localCost = this->local_cost_function(rowArray[k-1], colArray[l-1]);

	      std::pair<int, int> mindir;
	      double cost, mincost;

	      mincost = std::numeric_limits<double>::max();

	      for(unsigned int i=0; i<warpingWindow.size(); i++)
		{
		  // cost = cumulated_cost[previous case] + new_cost
		  //	    where new_cost = A*localcost + B
		  cost = cumulatedCost[(k + warpingDirection[i].first)*ncol+(l + warpingDirection[i].second)]
		    + warpingWindow[i]*localCost + warpingOffset[i];
		  if(cost <= mincost)
		    {
		      mincost = cost;
		      mindir = warpingDirection[i];
		      //	      printf("New min for (k, l) = (%i, %i): cost = %f ; (dk, dl) = (%i, %i) ; local_cost = %f ; cum_cost(%i, %i) = %f\n", k, l, mincost, mindir.first, mindir.second, localCost, k + mindir.first, l + mindir.second, cumulatedCost[(k + mindir.first)*ncol+(l + mindir.second)]);
		    }
		}

	      cumulatedCost[k*ncol+l] = mincost;
	      backtrackArray[k*ncol+l] = mindir;
	    }
	}

      // Let's start backtracking with the most bottom right element
      k=nrow-1;
      l=ncol-1;

      std::vector<int> tmpSourceMapping = std::vector<int> ();
      std::vector<int> tmpTargetMapping = std::vector<int> ();

      edit_distance = 0;
      while((k>0) || (l>0))
	{
	  int kOffset = backtrackArray[k*ncol+l].first;
	  int lOffset = backtrackArray[k*ncol+l].second;
	  if(kOffset == 0)
	    {
	      tmpSourceMapping.push_back(-l);
	    }else{
	    tmpSourceMapping.push_back(k-1);
	  }

	  if(lOffset == 0)
	    {
	      tmpTargetMapping.push_back(-k);
	    }else{
	    tmpTargetMapping.push_back(l-1);
	  }

	  // Increment edit_distance if not diagonal and dissimilarity == 0
	  if (!(kOffset == -1 && lOffset == -1 && cumulatedCost[k*ncol+l] == cumulatedCost[(k-1)*ncol+(l-1)])) {
	    edit_distance++;
	  }

	  k += kOffset;
	  l += lOffset;
	}

      this->sourceMapping = std::vector<int> ();
      for(int i=tmpSourceMapping.size()-1;i>=0;i--)
	{
	  sourceMapping.push_back(tmpSourceMapping[i]);
	}
      this->targetMapping = std::vector<int> ();
      for(int i=tmpTargetMapping.size()-1;i>=0;i--)
	{
	  targetMapping.push_back(tmpTargetMapping[i]);
	}

      cost = cumulatedCost[nrow*ncol-1];
      dissimilarity = (cumulatedCost[nrow*ncol-1]/((double) std::max(nrow, ncol)));
      delete[] cumulatedCost;
      delete[] backtrackArray;
    }

    /**
     * @brief Return the edit distance between the two input lists of elements
     * @warning This methods should NOT be called before calling the method align().
     * @return A positive or null integer
     */
    virtual unsigned int get_edit_distance() const {
      return edit_distance;
    }

    /**
     * @brief Return the optimal cost between the two input lists of elements
     * @warning This methods should NOT be called before calling the method align().
     * @return The cost between the two sequences (not normalized)
     */
    virtual double get_cost() const {
      return cost;
    }

    /**
     * @brief Return the dissimilarity between the two input lists of elements
     * @warning This methods should NOT be called before calling the method align().
     * @return A floating number ranging between Base::MIN_SIMILARITY and Base::MAX_SIMILARITY
     */
    virtual double get_dissimilarity() const {
      return dissimilarity;
    }

    /**
     * @brief Return the dissimilarity between the two input lists of elements
     * @warning This methods should NOT be called before calling the method align().
     * @return A floating number ranging between Base::MIN_SIMILARITY and Base::MAX_SIMILARITY
     */
    virtual double get_similarity() const {
      return Base::MAX_SIMILARITY - get_dissimilarity();
    }

    /**
     * @brief Returns the local cost of a source and target element
     * @details This function is called during the alignment algorithm for each
     * pair of source and target values. These function is purely virtual and has
     * to be implemented in subclasses.
     * @param rowValue source value
     * @param colValue target value
     * @return the local cost for the source and target pair
     */
    virtual double local_cost_function(T& rowValue, T& colValue) = 0;

    /**
     * @brief Return a human readable string of the alignment.
     * @param level Level of detail: 0 is one line per sequence (horizontal display), >0 is one line per token (vertical display).
     * @return A string of all aligned items.
     */
    virtual std::string to_string(int level=0) = 0;

  protected:
    // The cost of of a new alignment between to elements is defined as A*C + B
    // where C is a distance between the 2 aligned elements
    //	     A is a multiplicative factor as stored in warpingMultiplier
    //	     B is an additive factor as stored in warpingOffset
    std::map<int, double> warpingMultiplier;	/**< The warping window coefficient for each direction */
    std::map<int, double> warpingOffset; /**< The warping offset value for each direction */
    std::vector<std::pair<int, int> > warpingDirection; /**< The warping directions represented as a vector of (source,target) moves */ // (-1,0)(0,-1)(-1,-1)
    std::vector<T> constraint;	/**< Vector of values for which substitution is prohibited */


    double cost;
    double dissimilarity;
    int edit_distance;

  private:

    std::vector<T> rowArray;	/**< Vector row values */
    std::vector<T> colArray;		/**< Vector column values */

    std::vector<int> sourceMapping; /**< Contains the source mapping indices after alignment */
    std::vector<int> targetMapping;	/**< Contains the source mapping indices after alignment */
  };

}
#endif /* ALIGN_H_ */

