/*
    This file is part of Roots.

    Roots is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Roots is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Roots.  If not, see <http://www.gnu.org/licenses/>.

    Copyright (c) 2013, 2014 IRISA
*/

#ifndef UTTERANCE_H_
#define UTTERANCE_H_

#include "Base.h"
#include "RelationGraph.h"
#include "common.h"

#include "File/F0.h"  // TODO Remove when utils methods will be moved out of Utterance

namespace roots {

namespace sequence {
class Sequence;
}
class Relation;
class BaseItem;

typedef std::string t_label;
typedef std::string t_content_type;
typedef std::map<t_label, sequence::Sequence*> t_sequence_hash_label;
typedef std::map<t_content_type, t_sequence_hash_label> t_sequence_hash;
typedef std::map<t_label, Relation*> t_relation_hash;

/**
 * @brief An utterance
 * @details An utterance if the main object of roots and contains sequences of
 * items and relations.
 * @author Cordial Group
 * @version 0.1
 * @date 2011
 */
class Utterance : public roots::Base {
  /***************************************************/
  /* Const & Members :         */
 private:
 protected:
  bool use_cached_relation;
  t_relation_hash cached_relation;

 public:
  /***************************************************/
  /* Constructor/Destructor/Copy :       */

  /**
   * @brief Default constructor
   */
  Utterance();
  /**
   * @brief Destructor
   * @todo check
   */
  virtual ~Utterance();

  /**
   * @brief copy constructor
   */
  Utterance(const Utterance& utt);

  /**
   * @brief clone the current utterance object
   * @return a copy of the object
   */
  virtual Utterance* clone() const;

  virtual Utterance& operator=(const Utterance& utt);

  virtual void clear();

  void Debug();  // DEBUG

  /***************************************************/
  /* Manage Utterance :       */
 public:
  /**
   * @brief Concatenate an utterance after this
   * @param Utterance to concatenate
   * @warning Utterance to concatenate must be compatible with the current
   *Utterance :
   *    - Two sequences with same label and an embedded relation must be
   *related with the same label sequence.
   */
  void concatenate_utterance(const Utterance* concatenatingUtterance);

  /**
   * @brief merge an utterance with this
   * @param Utterance to merge
   * @warning Utterance to merge must be compatible with the current Utterance :
   *    - sequence and relation labels must be identical
   */
  void merge_utterance(const Utterance* utterance);

  /**
   * @brief build a sub utterance from a sub sequence
   * @param sourceSeqLabel
   * @param sourceBeginIndex
   * @param sourceEndIndex
   * @warning see .cpp
   */
  Utterance* extract_from_sequence(const std::string& sourceSeqLabel,
                                   int sourceBeginIndex, int sourceEndIndex);

 protected:
  /***************************************************/
  /* Manage Sequence :         */
 public:
  /**
   * @brief Returns the number of sequences in the utterance
   * @param contentType Optional, the type of sequences to be counted. Default
   * is all types.
   * @return unsigned int, size of the sequence map, id. number of sequences in
   * the utterance
   * @todo Should change the name to homogenize with other count_XXX methods
   * (for instance, rename to count_utterances())
   */
  unsigned int get_n_sequence(const std::string& contentType = "") const;

  /**
   * @brief Returns the label of each sequence in the current utterance
   * @param contentType optional parameter used to filter the content type of
   * sequences
   * @return vector of labels
   */
  std::vector<std::string>* get_all_sequence_labels(
      const std::string& contentType = "") const;

  /**
   * @brief Returns all the sequences in the current utterance
   * @param contentType optional parameter used to filter the content type of
   * sequences
   * @return vector of sequences
   */
  std::vector<sequence::Sequence*>* get_all_sequences(
      const std::string& contentType = "") const;

  /**
   * @brief Assert the validity of a sequence
   * @remarks Currently, the method just checks the existence of the sequence.
   *Further checkings may be achieved in the future to guarantee a safer
   *behaviour.
   * @param label Name of the sequence to be tested
   * @param contentType optional parameter used to filter the content type of
   *sequences
   * @return True if the tested sequence is valid, false otherwise
   **/
  bool is_valid_sequence(const std::string& label,
                         const std::string& contentType = "") const;

  /**
   * @brief Look for a given sequence and return it if found
   * @param label Name of the sequence
   * @param contentType Optional parameter used to filter the content type of
   * sequences
   * @return Pointer to the matching sequence or NULL if none was found
   * @warning The returned pointer points to the original sequence in the
   * utterance (not a copy).
   */
  sequence::Sequence* get_sequence(const std::string& label,
                                   const std::string& contentType = "") const;

  /**
   * @brief Adds a sequence to the utterance
   * Side-effect: change the label of the sequence,
   * Also add relation if sequence items are not simple.
   * @param sequence the sequence to be added
   * @param label label of sequence
   */
  void add_sequence(sequence::Sequence* sequence, const std::string& label);

  /**
   * @brief Adds a sequence to the utterance
   * Surface copy of the sequence.
   * Also add relation if sequence items are not simplee and
   * the related sequence (without copy) if not in the Utterance.
   * @param sequence the sequence to be added
   * @return pointer to the copy of the sequence
   */
  sequence::Sequence* add_sequence_copy(const sequence::Sequence& sequence);

  /**
   * @brief Adds a sequence to the utterance
   * No copy.
   * Also add relation if sequence items are not simple and
   * the related sequence if not in the Utterance.
   * @param sequence the sequence to be added
   */
  void add_sequence(sequence::Sequence* sequence);

  /**
   * @brief Adds a sequence to the utterance
   * Side-effect: change the label of the sequence,
   * if label parameter is given.
   * Do not add relation for not simple sequence.
   * @param sequence
   * @param label optional parameter
   */
  void add_sequence_no_propagate(sequence::Sequence* seq,
                                 const std::string& label = "");

  /**
   * @brief Update a sequence in the current utterance, or add it if not already
   * present
   * @param sequence Sequence to be updated
   * @param label Optional, name of the sequence. The name embedded in the
   * sequence instance if used if no explicit label is provided.
   */
  void update_sequence(sequence::Sequence* sequence,
                       const std::string& label = "");

  /**
   * @brief Change the name of one or several sequences in the current utterance
   * @note If the new sequence name already exists in the current utterance,
   *former sequences are removed from the utterance and returned.
   * @warning Returned removed utterances have not been deallocated.
   * @param old_name Current name of sequence(s)
   * @param new_name New name of sequence(s)
   * @param contentType Optional, type of the target sequence to be changed.
   *Useful if several sequences share a same name.
   * @return The list of removed (overwritten) sequences
   **/
  std::vector<sequence::Sequence*> rename_sequence(
      const std::string& old_name, const std::string& new_name,
      const std::string& contentType = "");

  /**
   * @brief Change the name of a sequence in the current utterance
   * @note If the new sequence name already exists in the current utterance,
   *former sequences are removed from the utterance and returned.
   * @warning Returned removed utterances have not been deallocated.
   * @param sequence Sequence to be renamed
   * @param new_name New name of the sequence
   * @return The list of removed (overwritten) sequences
   **/
  std::vector<sequence::Sequence*> rename_sequence(
      roots::sequence::Sequence* sequence, const std::string& new_name);

  /**
   * @brief Informe the utterance that an object is no more link to it (like it
   *will be destroyed)
   **/
  void no_more_linked(UtteranceLinkedObject* ulo);

  /**
   * @brief Deletes the sequence from the utterance. Do nothing if the sequence
   * is not found.
   * @warning The memory is not freed.
   * @param label Label of the sequence
   * @param contentType optional parameter used to filter the content type of
   * sequences
   * @return pointer to the removed sequence, NULL otherwise.
   */
  sequence::Sequence* remove_sequence(const std::string& label,
                                      const std::string& contentType = "");

  /**
   * @brief Deletes the sequence from the utterance. Do nothing if the sequence
   * is not found.
   * @warning The memory is not free.
   * @param label Pointer to the sequence
   * @return True is the relation is effectively remove (if it was in the
   * utterance), false otherwise.
   */
  bool remove_sequence(const roots::sequence::Sequence* seq);

 protected:
  /***************************************************/
  /* Manage Relation :         */
 public:
  /**
   * @brief Returns the number of relations in the utterance
   * @return unsigned int, size of the relation map, id. number of relations in
   * the utterance
   * @todo Should change the name to homogenize with other count_XXX methods
   * (for instance, rename to count_relations())
   */
  unsigned int get_n_relation() const;

  /**
   * @brief Assert the validity of a relation (i.e. it exist a path)
   * @remarks Currently, the method just checks the existence of the relation.
   *Further checkings may be achieved in the future to guarantee a safer
   *behaviour.
   * @param label Name of the relation to be tested
   * @return True if the tested relation is valid, false otherwise
   **/
  bool is_valid_relation(const std::string& label);

  /**
   * @brief Assert the validity of a relation (i.e. it exist a path)
   * @remarks Currently, the method just checks the existence of the relation.
   * Further checkings may be achieved in the future to guarantee a safer
   * behaviour.
   * @param srcSeqLabel Source sequence label
   * @param trgSeqLabel Target sequence label
   * @return True if the utterance contains a relation between the two
   * sequences, false otherwise
   */
  bool is_valid_relation(const std::string& srcSeqLabel,
                         const std::string& trgSeqLabel);

  /**
   * @brief Assert the validity of a relation (i.e. it exist a path)
   * @remarks Currently, the method just checks the existence of the relation.
   * Further checkings may be achieved in the future to guarantee a safer
   * behaviour.
   * @param srcSeq Source sequence
   * @param trgSeq Target sequence
   * @return True if the utterance contains a relation between the two
   * sequences, false otherwise
   */
  bool is_valid_relation(const sequence::Sequence* srcSeq,
                         const sequence::Sequence* trgSeq);

  /**
   * @brief Adds a relation copy into the utterance
   * @param relation pointer to the relation
   * @return pointer to the relation copy
   */
  Relation* add_relation_copy(const Relation& relation);

  /**
   * @brief Adds a relation to the utterance. No copy.
   * @param relation pointer to the relation
   */
  void add_relation(Relation* rel);

  /**
   * @brief Remove a relation from the utterance. No memory free.
   * @param label label of the relation
   * @return pointer to the removed relation
   */
  Relation* remove_relation(const std::string& label);

  /**
   * @brief Remove a relation from the utterance. No memory free.
   * @param label Pointer to the relation
   * @return is the relation is effectively remove (if it was in the utterance)
   */
  bool remove_relation(const Relation* rel);

  /**
   * @brief Return a direct known relation (no transitivity)
   * @param label Name of the searched relation
   * @return Pointer to the original relation (not a copy)
   * @note The returned relation should not be freed by the user.
   */
  Relation* get_relation(const std::string& label) const;

  /**
   * @brief Updates a relation in the current utterance, or add it if not
   * already present
   * @param relation Relation to be updated
   */
  void update_relation(roots::Relation* rel);

  /**
   * @brief Find items from a target sequence which are related to a given item
   *from a source sequence
   * @param index Index of the item in the source sequence
   * @param sourceLabel Name of the source sequence
   * @param targetLabel Name of the target sequence
   * @return Vector of the related items (potentially empty)
   **/
  std::vector<BaseItem*> get_related_items(unsigned int index,
                                           const std::string& sourceLabel,
                                           const std::string& targetLabel);

  /**
   * @brief Find items from a target sequence which are related to a given item
   *from a source sequence
   * @param index Index of the item in the source sequence
   * @param sourceSequence Pointer to the source sequence
   * @param targetSequence Pointer to the target sequence
   * @return Vector of the related items (potentially empty)
   **/
  std::vector<BaseItem*> get_related_items(
      unsigned int index, const sequence::Sequence* sourceSequence,
      const sequence::Sequence* targetSequence);

  /**
   * @brief Find items indices from a target sequence which are related to a
   *given item from a source sequence
   * @param index Index of the item in the source sequence
   * @param sourceLabel Name of the source sequence
   * @param targetLabel Name of the target sequence
   * @return Vector of the related items indices (potentially empty)
   **/
  std::vector<int> get_related_indices(unsigned int index,
                                       const std::string& sourceLabel,
                                       const std::string& targetLabel);

  /**
   * @brief Return a direct known relation between two sequences
   * @param srcSeqLabel Source sequence label
   * @param trgSeqLabel Target sequence label
   * @return Pointer to a relation if the relation exists, NULL otherwise
   */
  Relation* get_relation_between_sequence(
      const std::string& srcSeqLabel,
      const std::string& trgSeqLabel);

  /**
   * @brief Checks if two sequences share a direct relation (no transitivity)
   * within the utterance
   * @warning Relations that may exist outside the utterance are NOT considered
   * in the method.
   * @param srcSeqLabel Source sequence label
   * @param trgSeqLabel Target sequence label
   * @return True if the utterance contains a relation directly linking the two
   * sequences, false otherwise
   */
  bool linked(const std::string& srcSeqLabel,
              const std::string& trgSeqLabel);

  /**
   * @brief Checks if two sequences share a direct relation (no transitivity)
   * within the utterance
   * @warning Relations that may exist outside the utterance are NOT considered
   * in the method.
   * @param srcSeq Source sequence
   * @param trgSeq Target sequence
   * @return True if the utterance contains a relation directly linking the two
   * sequences, false otherwise
   */
  bool linked(const sequence::Sequence* srcSeq,
              const sequence::Sequence* trgSeq);

  /**
   * @brief Return a copy of the relation built from the relation graph
   * @warning Deallocation of the returned relation MUST be achieved by calling
   * the method destroy() of the relation, NOT using delete.
   * @param srcSeq Source sequence label
   * @param trgSeq Target sequence label
   * @return Pointer to a copy of relation if the relation exists, NULL
   * otherwise
   */
  virtual Relation* get_relation(const std::string& srcSeq,
                                 const std::string& trgSeq);

  /**
   * @brief Return a copy of the relation built from the relation graph
   * @warning Deallocation of the returned relation MUST be achieved by calling
   * the method destroy() of the relation, NOT using delete.
   * @param srcSeq Source sequence
   * @param trgSeq Target sequence
   * @return Pointer to a relation if the relation exists, NULL otherwise
   */
  virtual Relation* get_relation(const sequence::Sequence* srcSeq,
                                 const sequence::Sequence* trgSeq);

  /**
   * @brief When use_cached_relation is set to true, a call to get_relation will
   * cache the result in order to fasten further calls.
   * @param use_it New value for the cache flag
   * @note If a sequence or a relation is changed, cache relation buffer must be
   * flushed or use_cached_relation should be turned off.
   */
  virtual void set_use_cached_relation(bool use_it);

  /**
   * @brief return the use_cached_relation state
   * @return use_cached_relation boolean
   * @detail When use_cached_relation is set to true usage of get_relation will
   * cached the result wich increase futher call.
   * @note if a sequence or a relation is changed, cache relation buffer must be
   * flush or use_cached_relation should be turn off.
   * @todo doc
   */
  virtual bool get_use_cached_relation();

  /**
   * @brief flush all cached relations (if any). Composed relations will be
   * deleted.
   * @todo doc
   */
  virtual void flush_cached_relation();

  /**
   * @brief Returns each label of the relations in the current utterance
   * @return vector of strings
   */
  std::vector<std::string>* get_all_relation_labels() const;

  /**
   * @brief Returns all the relations in the current utterance
   * @return vector of relations
   */
  std::vector<Relation*>* get_all_relations() const;

  /**
   * @brief Returns all the relations linking a given sequence to another in the
   *current utterance
   * @param label label of the sequence
   * @return vector of relations
   * @warning Relations are returned as stored in the utterance, i.e., the given
   *input sequence
   *    may be the source or the target of the relation.
   */
  std::vector<Relation*>* get_all_relations(const std::string& label) const;

  /**
   * @brief return the graph of relations in dot format
   * @todo all
   */
  std::string relation_graph_to_dot();

 protected:
  /***************************************************/
  /* IO :            */
 public:
  /**
 * @brief Prepare an utterance to be saved in a directory. Basicaly, a signal
 * segment items will copy the associated wav file if target_base_dir change.
 * @Warning items in sequences may be changed (for instance the base directory
 * of signal segment items)!
 */
  virtual void prepare_save(const std::string& target_base_dir);

  /**
   * @brief Returns a raw text string representation of the current utterance
   * @param level precision level of the output (default: 0)
   * @return A unicode string
   */
  std::string to_string(int level = 0) const;

  /**
   * @brief returns a string representation for use with PGF/Tikz
   * @param level the precision level of the output (default: 0)
   * @param draw_relations indicates if relations(arrows) between consecutive
   * levels should be drawn or not (default: false)
   * @return PGF/Tikz representation of the object
   */
  std::string to_pgf(const int level = 0, const bool draw_relations = false);

  /**
   * @brief returns a string representation for use with PGF/Tikz
   * @param sequencesOrder labels of the sequences to be represented in the
   * order they should appear in the pgf
   * @param level the precision level of the output (default: 0)
   * @param draw_relations indicates if relations(arrows) between consecutive
   * levels should be drawn or not (default: false)
   * @return PGF/Tikz representation of the object
   */
  std::string to_pgf(const std::vector<std::string>& sequencesOrder,
                     const int level = 0, const bool draw_relations = false);

  /**
   * @brief Generate the XML part of the entity
   * @param stream
   * @param currentTagName
   * @param parentNode
   */
  virtual void deflate(RootsStream* stream, bool is_terminal = true,
                       int list_index = -1);
  /**
   * @brief Extracts the entity from the stream
   * @param stream the stream from which we inflate the element
   */
  virtual void inflate(RootsStream* stream, bool is_terminal = true,
                       int list_index = -1);
  /**
   * @brief returns the XML tag name value for current object
   * @return string constant representing the XML tag name
   */
  virtual std::string get_xml_tag_name() const { return xml_tag_name(); };

  /**
   * @brief returns the XML tag name value for current class
   * @return string constant representing the XML tag name
   */
  static std::string xml_tag_name() { return "utterance"; };

  /**
   * @brief returns classname for current object
   * @return string constant representing the classname
   */
  virtual std::string get_classname() const { return classname(); };

  /**
   * @brief returns classname for current class
   * @return string constant representing the classname
   */
  static std::string classname() { return "Utterance"; };

  /***************************************************/
  /* Import :          */
  // TODO : move into an other File/Class/Factory

 private:
  t_sequence_hash sequenceHash; /**< The sequences */
  t_relation_hash relationHash; /**< The relations */
  RelationGraph relationGraph;  /**< The graph of relations */
};
}

#endif /* UTTERANCE_H_ */
