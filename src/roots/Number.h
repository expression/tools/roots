/*
    This file is part of Roots.

    Roots is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Roots is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Roots.  If not, see <http://www.gnu.org/licenses/>.

    Copyright (c) 2013, 2014 IRISA
*/


#ifndef NUMBER_H
#define NUMBER_H
#include "BaseItem.h"

namespace roots
{
    /**
     * @brief Generic class to store a number. The number is modeled by a double.
     **/
    class Number : public roots::BaseItem
    {
    private:
      /**
       * @brief Value of the number
       **/
      double value;
    public:
      // Constructors, destructor, etc.
      Number(const bool noInit = false);
      Number(const double _value);
      Number(const std::string & str_value);
      Number(const Number & to_be_copied);
      virtual ~Number();
      virtual Number* clone() const;
      Number& operator=(const Number & to_be_copied);
      // IO methods
      virtual std::string to_string(int level = 0) const;
      virtual void deflate(RootsStream * stream, bool is_terminal=true, int list_index=-1);
      virtual void inflate(RootsStream * stream, bool is_terminal=true, int list_index=-1);
      static Number * inflate_object(RootsStream * stream, int list_index=-1);
      // Specific methods
      virtual double get_value() const;
      virtual void set_value(const double value);
      // Metadata methods
      static std::string xml_tag_name() { return "number"; };
      static std::string classname() { return "Number"; };
      static std::string pgf_display_color() { return "grey!40"; };
      virtual std::string get_xml_tag_name() const { return xml_tag_name(); };
      virtual std::string get_classname() const { return classname(); };
      virtual std::string get_pgf_display_color() const { return pgf_display_color();};
    };
}

#endif // NUMBER_H
