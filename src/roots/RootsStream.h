/*
    This file is part of Roots.

    Roots is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Roots is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Roots.  If not, see <http://www.gnu.org/licenses/>.

    Copyright (c) 2013, 2014 IRISA
*/


#ifndef ROOTS_STREAM_H
#define ROOTS_STREAM_H

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include <string.h>
#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <string>
#include <wchar.h>
#include <libgen.h>

#include <exception>

#include <unicode/utypes.h>   /* Basic ICU data types */
#include <unicode/ucnv.h>     /* C   Converter API    */
#include <unicode/ustring.h>  /* some more string fcns*/
#include <unicode/uchar.h>    /* char names	      */
#include <unicode/uloc.h>
#include <unicode/unistr.h>
#include <unicode/decimfmt.h>
#include <unicode/ustream.h>
#include <unicode/fmtable.h>
#include <unicode/numfmt.h>
#include <unicode/regex.h>


#ifdef USE_STREAM_XML
#include <xercesc/util/PlatformUtils.hpp>
#include <xercesc/framework/XMLFormatter.hpp>
#include <xercesc/framework/LocalFileFormatTarget.hpp>
#include <xercesc/util/XMLString.hpp>
#include <xercesc/dom/DOM.hpp>
#include <xercesc/util/OutOfMemoryException.hpp>
#include <xercesc/parsers/XercesDOMParser.hpp>
#include <xercesc/util/XMLUni.hpp>
#include <xercesc/util/XMLDouble.hpp>
#include <xercesc/util/TranscodingException.hpp>
#endif

#include <boost/numeric/ublas/matrix_sparse.hpp>
#include <boost/numeric/ublas/io.hpp>

namespace roots {

  /**
   * @brief Class that provides a generic interface for roots input/output
   * @details
   * This class provides a set of convenient methods and functions to manipulate
   * streams in input and output.
   *
   * @author damien.lolive@irisa.fr
   * @version 0.1
   * @date 2013
   */
  class RootsStream {

  public:
    /**
     * @brief Default constructor
     */
    RootsStream() {elementIndex=-1;isInList=false;contentName=NULL;}
    /**
     * @brief Destructor
     */
    virtual ~RootsStream() {}

  public:
    /**
     * @brief inits the local attribute
     */
    virtual  void init ()
    {
      elementIndex=-1;
      isInList=false;
      contentName=NULL;
      loadedObjectsList.clear();
      baseDirName = "";
    }
    
    virtual void destroy()
    {
      delete this;
    }
    
    /**
     * @brief Flush and clean the stream
     */
    virtual void clear()=0;
    
    /**
     * @brief loads the file content
     * @param _filename
     */
    virtual void load(const std::string& _filename)=0;
    /**
     * @brief loads the string content
     * @param _json
     */
    virtual void from_string(const std::string& _json,
			     const std::string _baseDirName = "")=0;
    /**
     * @brief saves the file content
     * @param _filename
     */
    virtual void save(const std::string& _filename)=0;
    /**
     * @brief returns a string representation of the document
     * @return string representation of the object
     */
    virtual std::string to_string()=0;
    /**
     * @brief Appends a child element to a node
     * @param name the child name
     */
    virtual void append_object(const char * name, int list_index=-1)=0;

    virtual void close_object()=0;

    virtual void open_object(const char * name, int list_index=-1)=0;

    virtual void set_object_classname(const std::string& classname)=0;

    virtual std::string get_object_classname()=0;

    virtual void open_children()=0;

    virtual void close_children()=0;

    virtual void next_child()=0;

    virtual std::string get_object_classname_no_read(const std::string& name, int list_index=-1)=0;
    
    /**
     * @brief Test if the current node has a child (field) of a given name
     * @param name Name of the searched child
     * @return True if the child exists, false otherwise
     **/
    virtual bool has_child(const std::string& name)=0;

    /**
     * @brief Test if the current node has a child (field) of a given name which is an array of n elements
     * @param name Name of the searched child
     * @param n Number of element to test    
     * @return True if the child exists and has n elements, false otherwise
     **/
    virtual bool has_n_children(const std::string& name, const unsigned int n) =0;

    /**
     * @brief Returns the number of children with a given name
     * @param name Name of the searched child
     * @return n the number of elements
     **/
    virtual unsigned int get_n_children(const std::string& name)=0;

    /* // Produce a swig warning
#ifndef COMMA
#define COMMA ,
#endif
    */

    /*
      All functions as get_XX_content(std::string) move to the next child after reading. When get_XX_content() is used, next_child must be used.
    */

#define DECLARE_FOR_SIMPLE_TYPE(tna,itva,tva)				\
    virtual void append_ ## tna ## _content(const char *name, const itva & content)=0; \
    virtual tva get_ ## tna ## _content(const char * name)=0;	\

#if 0
    virtual void set_ ## tna ## _content(const itva & content)=0; \
    virtual tva get_ ## tna ## _content()=0;								\

#endif

#define DECLARE_FOR_COMPLEX_TYPE(tna,itva,tva)				\
    virtual void append_ ## tna ## _content(const char * name, const itva & content)=0; \
    virtual tva get_ ## tna ## _content(const char * name)=0;	\


#if 0
    virtual void set_ ## tna ## _content(const itva & content)=0;              \
    virtual tva get_ ## tna ## _content()=0;								\

#endif

    DECLARE_FOR_SIMPLE_TYPE(unicodestring,std::string,std::string);
    DECLARE_FOR_SIMPLE_TYPE(cstr,char *,char*);
    DECLARE_FOR_SIMPLE_TYPE(float,float,float);
    DECLARE_FOR_SIMPLE_TYPE(double,double,double);
    DECLARE_FOR_SIMPLE_TYPE(long_double,long double,long double);
    DECLARE_FOR_SIMPLE_TYPE(int,int,int);
    DECLARE_FOR_SIMPLE_TYPE(uint,unsigned int,unsigned int);
    DECLARE_FOR_SIMPLE_TYPE(bool,bool,bool);
    DECLARE_FOR_SIMPLE_TYPE(char,char,char);
    DECLARE_FOR_SIMPLE_TYPE(uchar,UChar,UChar);


    DECLARE_FOR_COMPLEX_TYPE(vector_float, std::vector<float>,std::vector<float>);
    DECLARE_FOR_COMPLEX_TYPE(vector_int, std::vector<int>,std::vector<int>);
    DECLARE_FOR_COMPLEX_TYPE(vector_bool, std::vector<bool>,std::vector<bool>);
    DECLARE_FOR_COMPLEX_TYPE(vector_uchar, std::vector<UChar>,std::vector<UChar> );
    DECLARE_FOR_COMPLEX_TYPE(vector_unicodestring, std::vector<std::string>, std::vector<std::string>);
    DECLARE_FOR_COMPLEX_TYPE(matrix_int,boost::numeric::ublas::mapped_matrix<int>,boost::numeric::ublas::mapped_matrix<int>);

    DECLARE_FOR_COMPLEX_TYPE(set_int, std::set<int>,std::set<int> );
    DECLARE_FOR_COMPLEX_TYPE(set_uchar, std::set<UChar>,std::set<UChar> );

    //    DECLARE_FOR_COMPLEX_TYPE(map_unistr_unistr,std::map<std::string COMMA std::string>,std::map<std::string COMMA std::string>);
    virtual void append_map_unistr_unistr_content(const char *name, const std::map<std::string , std::string> & content)=0;
    virtual std::map<std::string , std::string> get_map_unistr_unistr_content(const char * name)=0;	

    /**
     * @brief Adds an object to the list of currently loaded objects
     * @param ID ID of the object
     * @param ptr Pointer to the object to keep track of
     */
    virtual void add_object_to_list(const std::string& ID, void *ptr)
    {
      //loadedObjectsList[ID] = ptr;
      loadedObjectsList.insert(std::make_pair(ID, ptr));
    }
    /**
     * @brief Returns an object pointer from its ID
     * @param ID ID of the object
     * @return the generic pointer or NULL
     */
    virtual void *get_object_ptr(const std::string& ID)
    {
      boost::unordered_map<std::string, void *>::const_iterator it =  loadedObjectsList.find(ID);
      if(it != loadedObjectsList.end())
	return it->second;
      //	return loadedObjectsList[ID];
      return NULL;
    }

    /**
     * @brief Removes an object from the list based on its ID
     * @param ID ID of the object to forget
     */
    virtual void remove_object_ptr(const std::string& ID)
    {
      if(loadedObjectsList.find(ID) != loadedObjectsList.end())
	loadedObjectsList.erase(ID);
    }

    virtual const std::string& get_base_dir_name()
    {
      return this->baseDirName;
    }

    virtual void set_base_dir_name(const std::string& name)
    {
      this->baseDirName = name;
    }

  private:

    boost::unordered_map<std::string, void*> loadedObjectsList; /**< List of objects already loaded from the XML file */
    // Remettre à zéro au début du load, contient (ID,@)
    std::string baseDirName;
    int elementIndex;
    bool isInList;
    char * contentName;
  };

}

#endif // ROOTS_STREAM_H
