/*
    This file is part of Roots.

    Roots is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Roots is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Roots.  If not, see <http://www.gnu.org/licenses/>.

    Copyright (c) 2013, 2014 IRISA
*/


#include "RelationLinkedObject.h"
#include "Relation.h"

namespace roots
{
  RelationLinkedObject::RelationLinkedObject()
  {
    linkHash = boost::unordered_set<BaseLink<Relation> >();
  }

  RelationLinkedObject::RelationLinkedObject(const RelationLinkedObject& rel_link)
  {
    this->linkHash.clear();
    this->linkHash = rel_link.linkHash;
  }

  RelationLinkedObject::~RelationLinkedObject()
  {
    // Note : "wile" bc the iterator is invalidate by no_more_linked !!
    boost::unordered_set<BaseLink<Relation> >::const_iterator it = linkHash.begin();
    while(it != linkHash.end())
      {
	unsigned nbHash = linkHash.size();
	it->get_link()->no_more_linked(this); 
	if(nbHash == linkHash.size())
	  {++it;}
	else
	  {it = linkHash.begin();}
      }
  }

  void RelationLinkedObject::add_link( BaseLink<Relation>& blink)
  {
    linkHash.insert(blink);
  }

  void RelationLinkedObject::remove_link(Relation& rel, bool informRelation)
  {
    if(informRelation){rel.no_more_linked(this);}
    linkHash.erase(BaseLink<Relation>(&rel));
  }
  
  void RelationLinkedObject::remove_link(const BaseLink<Relation>& link, bool informRelation)
  {
    if(informRelation){link.get_link()->no_more_linked(this);}
    linkHash.erase(link);
  }

  std::vector<BaseLink<Relation> > RelationLinkedObject::get_link(const std::string& label) const
  {
    std::vector<BaseLink<Relation> > res;
    for( boost::unordered_set< BaseLink<Relation> >::const_iterator it = linkHash.begin();
	 it != linkHash.end();
	 ++it)
      {
	if(it->get_link()->get_label() == label)
	  {res.push_back(*it);}
      }

    return res;
  }

  std::vector<BaseLink<Relation> > RelationLinkedObject::get_links() const
  {
    std::vector<BaseLink<Relation> > vec;
 
    for(boost::unordered_set<BaseLink<Relation> >::const_iterator it = linkHash.begin();
	it != linkHash.end();
	++it)
      { vec.push_back(*it); }

    return vec;
  }

  std::vector<Relation*> RelationLinkedObject::get_all_relations() const
  {
    std::vector<Relation*> vec;
    for(boost::unordered_set<BaseLink<Relation> >::const_iterator it = linkHash.begin();
	it != linkHash.end();
	++it)
      {
	vec.push_back(it->get_link()); 
      }

    return vec;
  }

  std::vector<std::string > RelationLinkedObject::get_links_labels() const
  {
    std::vector<std::string> vec;
    for(boost::unordered_set<BaseLink<Relation> >::const_iterator it = linkHash.begin();
	it != linkHash.end();
	++it)
      { vec.push_back(it->get_link()->get_id()); }

    return vec;
  }

  RelationLinkedObject& RelationLinkedObject::operator=(const RelationLinkedObject& rel_link)
  {
    this->linkHash = rel_link.linkHash;
    return *this;
  }

  boost::unordered_set<BaseLink<Relation> >& RelationLinkedObject::get_hash()
  {
    return linkHash;
  }

  void RelationLinkedObject::set_hash(const boost::unordered_set<BaseLink<Relation> >& hash)
  {
    linkHash = hash;
  }

}
