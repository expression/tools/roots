/*
    This file is part of Roots.

    Roots is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Roots is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Roots.  If not, see <http://www.gnu.org/licenses/>.

    Copyright (c) 2013, 2014 IRISA
*/


#ifndef RELATIONLINKEDOBJECT_H_
#define RELATIONLINKEDOBJECT_H_


#include "common.h"
#include "BaseLink.h"

namespace roots
{
  
  class BaseItem;
  class Relation;

  /**
   * @brief Represent a link between an object and a relation
   * @details
   * @author Cordial Group
   * @version 0.1
   * @date 2011
   */
  class RelationLinkedObject
  {
  public:
    /**
     * @brief Default constructor
     */
    RelationLinkedObject();

    /**
     * @brief Default constructor
     */
    RelationLinkedObject(const RelationLinkedObject& rel_link);

    /**
     * @brief Default constructor
     * @todo check
     */
    virtual ~RelationLinkedObject();

    /**
     * @brief Adds a link to a relation
     * This method verifies that the link is not already in the hash of
     * links before adding it.
     * @param blink the link to the relation
     */
    void add_link( BaseLink<Relation>& blink);

    /**
     * @brief Removes a link to a relation
     * The linked relation and the link itself
     * are not deleted. It use the relation Id 
     * to find the correct link.
     * @param relation to remove
     */
    void remove_link(Relation& rel, bool informRelation=true);

    /**
     * @brief Removes a link to a relation
     * This method verifies that the link is in the hash of
     * links before deleting it. The linked relation and the link itself
     * are not deleted. It use the relation Id 
     * to find the correct link.
     * @param link a reference to the link to delete
     */
    void remove_link(const BaseLink<Relation>& link, bool informRelation=true);
    /**
     * @brief Returns the link associated to the given label
     * @param label the label of the link we search for
     */
    std::vector<BaseLink<Relation> > get_link(const std::string& label) const;

    /**
     * @brief Returns a vector of pointers to the links
     * @return vector of pointer which contains the links to the relations
     * @todo Should be renamed into get_all_relation_links() or get_all_links()
     */
    std::vector<BaseLink<Relation> > get_links() const;

    /**
     * @brief Lists all the relations to which the current object is linked
     * @return a pointer to a vector of relation pointers
     */
    std::vector<Relation*> get_all_relations() const;

    /**
     * @brief Returns a vector which contains the labels of the links
     * @return vector which contains the labels of the links
     */
    std::vector<std::string > get_links_labels() const;
    
    RelationLinkedObject& operator=(const RelationLinkedObject& rel_link);

  protected:
    /**
     * @brief Returns the hash containing the links
     * Be carefull, this hash is part of the class internals.
     * @return a reference to the hash
     */
    boost::unordered_set< BaseLink<Relation> >& get_hash();

    /**
     * @brief Assigns the hash containing the links
     * Be carefull, this hash is part of the class internals.
     * @param hash a reference to the hash
     */
    void set_hash(const boost::unordered_set< BaseLink<Relation> >& hash);

  private:
    
    /**
     * Hash of the relation links. Key is the label of the link.
     */
    boost::unordered_set< BaseLink<Relation> > linkHash;
  };

}

#endif /* RELATIONLINKEDOBJECT_H_ */
