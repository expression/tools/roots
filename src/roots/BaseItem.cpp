/*
    This file is part of Roots.

    Roots is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Roots is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Roots.  If not, see <http://www.gnu.org/licenses/>.

    Copyright (c) 2013, 2014 IRISA
*/


#include "BaseItem.h"
#include "Sequence.h"
#include "Align/Levenshtein.h"

namespace roots
{

  BaseItem::BaseItem(const bool noInit) : Base(noInit)
  {
  }

  BaseItem::BaseItem(const std::string& aLabel) : Base(aLabel)
  {
  }

  BaseItem::BaseItem(const BaseItem & baseI) : Base(baseI), SequenceLinkedObject()
					       //, SequenceLinkedObject(baseI), RelationLinkedObject(baseI) // Note : no backlink for a clone !
  {
  }

  BaseItem::~BaseItem()
  {
  }

  BaseItem *BaseItem::clone() const
  {
    return new BaseItem(*this);
  }

  BaseItem& BaseItem::operator= (const BaseItem & baseI)
  {
    Base::operator=(baseI);
    //	  SequenceLinkedObject::operator=(baseI); // Do not propagate links

    return *this;
  }
  
  const std::string BaseItem::get_timestamp() const {
    if (get_raw_timestamp() == 0) {
      ((Base )(*this)).set_timestamp();
    }
    return Base::get_timestamp();
  }

  bool BaseItem::has_related_sequence() const
  {
    return false;
  }

  void BaseItem::set_related_sequence(sequence::Sequence *)
  {
  }

  sequence::Sequence * BaseItem::get_related_sequence() const
  {
    return NULL;
  }

  void BaseItem::insert_related_element(int /* index */)
  {
    // Nothing to do
    // Override in subclass if needed
  }

  void BaseItem::remove_related_element(int /* index */)
  {
    // Nothing to do
    // Override in subclass if needed
  }

  void BaseItem::translate_index(int /* offset */)
  {

  }

  std::vector< BaseItem* > BaseItem::get_related_items(const std::string & targetLabel) const
  {
    sequence::Sequence* seq = get_in_sequence();
    if(seq != NULL)
      { return seq->get_related_items(get_in_sequence_index(), targetLabel);}
    return std::vector< BaseItem* >();
  }

  std::vector< int > BaseItem::get_related_indices(const std::string & targetLabel) const
  {
    sequence::Sequence* seq = get_in_sequence();
    if(seq != NULL)
      { return seq->get_related_indices(get_in_sequence_index(), targetLabel);}
    return std::vector< int >();
  }

  void BaseItem::prepare_save(const std::string & /*target_base_dir*/)
  {
    // Nothing to do in the general case
  }

  void BaseItem::deflate (RootsStream * stream, bool is_terminal, int list_index)
  {
    // Do not output ID for items
    std::stringstream convert;
    convert << this->get_xml_tag_name();

    stream->append_object(convert.str().c_str(), list_index); // Creates a new node and set it to current node
    stream->set_object_classname(this->get_classname());

    const std::string * ptDescription = this->get_description();
    if(ptDescription != NULL && *ptDescription != "")
      {stream->append_unicodestring_content("description", *ptDescription);}

    if(is_terminal)
      stream->close_object();
  }

  void BaseItem::inflate(RootsStream * stream, bool is_terminal, int list_index)
  {
    std::stringstream convert;
    convert << this->get_xml_tag_name();

    stream->open_object(convert.str().c_str(), list_index);
    stream->open_children();

    std::string classname = stream->get_object_classname();
    if (stream->has_child("description")) {
      std::string tmpDescr =stream->get_unicodestring_content("description");
      if (tmpDescr != "")
	{this->set_description(tmpDescr);}
      else
	{this->set_description(NULL);}
    }
    else {
      this->set_description(NULL);
    }

    if(is_terminal) stream->close_children();
  }

}
