/*
    This file is part of Roots.

    Roots is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Roots is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Roots.  If not, see <http://www.gnu.org/licenses/>.

    Copyright (c) 2013, 2014 IRISA
*/


#include "Symbol.h"

namespace roots
{
  Symbol::Symbol(const bool noInit) : BaseItem(noInit)
  {
    // Nothing to do
  }
  
  Symbol::Symbol(const std::string & symbol_str) : BaseItem(symbol_str)
  {
  }
  
  Symbol::Symbol(const Symbol & to_be_copied) : BaseItem(to_be_copied)
  {
    // Nothing to do
  }
  
  Symbol::~Symbol() {
    // Nothing to do
  }
  
  Symbol* Symbol::clone() const 
  {
    return new Symbol(*this);
  }
  
  Symbol& Symbol::operator=(const Symbol & to_be_copied) 
  {
    BaseItem::operator=(to_be_copied);
    return *this;
  }
  
  // IO methods
  std::string Symbol::to_string(int /*level*/) const
  {
    return get_label();
  }
  
  void Symbol::deflate(RootsStream * stream, bool is_terminal, int list_index) {
    BaseItem::deflate(stream,false,list_index);
    
    stream->append_unicodestring_content("text", this->get_label());
    
    if(is_terminal)
      stream->close_object();
  }
  
  void Symbol::inflate(RootsStream * stream, bool is_terminal, int list_index)
  {
    BaseItem::inflate(stream,false,list_index);
    
    this->set_label(stream->get_unicodestring_content("text"));
    
    if(is_terminal) stream->close_children();
  }
  
  Symbol * Symbol::inflate_object(RootsStream * stream, int list_index)
  {
    Symbol *s = new Symbol(true);
    s->inflate(stream,true,list_index);
    return s; 
  }
}
