/*
    This file is part of Roots.

    Roots is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Roots is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Roots.  If not, see <http://www.gnu.org/licenses/>.

    Copyright (c) 2013, 2014 IRISA
*/


#ifndef PHONEME_H_
#define PHONEME_H_

#include "../common.h"
#include "../BaseItem.h"
#include "Ipa.h"
#include "Ipa/Alphabet.h"


namespace roots
{
  namespace phonology
  {

    class Phoneme: public roots::BaseItem
      {
      protected:
      public:
	/**
	 * @brief Default constructor
	 */
	Phoneme(const bool noInit = false);

	/**
	 * @brief Constructor
	 * @note ipa are copied
	 * @param ipa The IPA phonological description of the phoneme
	 * @param alphabet used for the phoneme
	 */
	Phoneme(const ipa::Ipa & ipa, ipa::Alphabet* alphabet=NULL);

	/**
	 * @brief Constructor
	 * @note ipa are copied
	 * @param ipa The IPA phonological description of the phoneme
	 * @param ipa2 second IPA in case of diphthong
	 * @param alphabet used for the phoneme
	 */
	Phoneme(const ipa::Ipa & ipa, const ipa::Ipa & ipa2, ipa::Alphabet* alphabet=NULL);

	/**
	 * @brief Constructor
	 * @note ipa are copied
	 * @param ipas The IPA phonological description of the phoneme as a vector of IPA objects
	 * @param alphabet used for the phoneme
	 */
	Phoneme(const std::vector<ipa::Ipa*> & ipas, ipa::Alphabet* alphabet=NULL);

	/**
	 * @brief Constructor
	 * @note ipa are copied
	 * @param ipa The IPA phonological description of the phoneme as a vector of IPA objects
	 * @param alphabet used for the phoneme
	 */
	Phoneme(const std::vector<ipa::Ipa*> * ipa, ipa::Alphabet* alphabet=NULL);

	/**
	 * @brief Copy constructor
	 * @details This constructor does not copy links to relations or sequence.
	 * @param ph
	 */
	Phoneme(const Phoneme& ph);

	/**
	 * @brief Destructor
	 * @todo check
	 */
	virtual ~Phoneme();

	/**
	 * @brief Duplicates the phoneme
	 * @return A clone of the phoneme
	 */
	virtual Phoneme *clone() const;

	/**
	 * @brief Copy allophone features into the current allphone
	 * @details Links to relations or sequence are copied.
	 * @param ph
	 * @return reference to the current allophone
	 */
	Phoneme& operator= (const Phoneme &ph);
	
	/**
	 * @brief Compute the distance between the current phoneme and another
	 * @warning This method does NOT allow flexibility on the position of diacritics
	 * @param ph Phoneme to compute the distance with.
	 * @return A distance, ie, a positive number which is 0 is the phonemes are equivalent.
	 */
	virtual double compute_dissimilarity( const roots::Base * b ) const;
	
	/**
	 * @brief Compute the distance between the current phoneme and another
	 * @warning This method allows flexibility on the position of diacritics
	 * @param ph Phoneme to compute the distance with.
	 * @return A distance, ie, a positive number which is 0 is the phonemes are equivalent.
	 */
	virtual double compute_tolerant_dissimilarity ( const roots::Base* b ) const;

	/**
	 * @brief Splits a phoneme into its IPA components
	 * @return A vector of IPA pointers (presented as Base pointers because of inheritance)
	 */
	virtual std::vector< Base *> * split();


	/**
	 * @brief Returns if the phoneme has an alphabet
	 * @return if the phoneme has an alphabet
	 */
	virtual bool has_alphabet() const;

	/**
	 * @brief Returns the alphabet of the phoneme
	 * @return the label
	 */
	virtual ipa::Alphabet* get_alphabet() const;

	/**
	 * @brief Modifies the alphabet of the phoneme
	 * @param alphabet the new alphabet (could be NULL and in this case it will be IPA)
	 */
	virtual void set_alphabet(ipa::Alphabet* alphabet, bool updateLabel = true);

	/**
	 * @brief Returns true if phoneme is a monophthong
	 * @return true if phoneme is composed of one sound
	 */
	bool is_monophthong() const { return ipa.size() == 1;};
	/**
	 * @brief Returns true if phoneme is a n-phthong
	 * @param n position of the Ipa
	 * @return true if phoneme is composed of n sounds
	 */
	bool is_nphthong(unsigned int n) const { return ipa.size() == n;};

	/**
	 * @brief Modifies the Ipa ref associated to this allophone (second part of phoneme in case of diphthong)
	 * @param i the new Ipa reference
	 * @param n place of the ipa in phoneme
	 * @param updateLabel true to make an update of the label
	 */
	void set_ipa(const ipa::Ipa& i, unsigned int n, bool updateLabel = true);
	
	/**
	 * @brief Add a new Ipa instance at the end of the Ipa list for the current phoneme
	 * @note The inserted Ipa is copied.
	 * @param new_ipa The Ipa instance to be copied and inserted
	 * @param updateLabel true to make an update of the label
	 */
	void add_ipa(const ipa::Ipa & new_ipa, bool updateLabel = true);
	
	/**
	 * @brief Clear (make empty) the list of IPAs for the current phoneme
	 * @note Cleared IPAs are destroyed.
	 * @param updateLabel true to make an update of the label
	 */
	void clear(bool updateLabel = true);

	/**
	 * @brief Remove n-phtong if any
	 * @param n position of the Ipa to remove
	 * @param pack pack the Ipa vector when removing one element
	 */
	void delete_nphtong(unsigned int n, bool pack=true);

	int get_n_ipa() const { return ipa.size(); }

	/**
	 * @brief Returns reference to the Ipa object (n-th part of phoneme in case of n-phthong)
	 * @param n optional, position of the Ipa. Use a negative index to access from the end of the phoneme, e.g., -1 to access the last IPA. Default is 0.
	 * @return reference on Ipa
	 */
	const ipa::Ipa& get_ipa(unsigned int n=0) const;
	
	/**
	 * @brief Return the list of all IPAs for the current phoneme
	 * @notice This is a copy of the internal list in the current phoneme.
	 * @return A vector of Ipa pointers
	 */
	std::vector<ipa::Ipa *> get_all_ipas() const;

	/**
	 * @brief Gets the unicode char associated to the Ipa object (n-th part of phoneme in case of n-phthong)
	 * @param n optional, position of the Ipa. Default is 0.
	 * @return  Unicode character
	 */
	UChar get_ipa_unicode(unsigned int n=0) const;

	/**
	 * @brief Tests if the phoneme is of given Ipa category
	 * @return true if the phoneme is of given category
	 */
	bool is_a(const ipa::Category category) const;

	/**
	 * @brief Tests if the phoneme is one of given Ipa category
	 * @return true if the phoneme is one of given category
	 */
	bool is_one_of(const std::vector<ipa::Category>& categories) const;

	/**
	 * @brief Tests if the phoneme is all of given Ipa category
	 * @return true if the phoneme is all of given category
	 */
	bool is_all_of(const std::vector<ipa::Category>& categories) const;
	
	/**
	 * @brief Test if the phoneme is stressed, whatever the type of stress is.
	 * @return True is stressed, 
	 */
	bool is_stressed() const;
	
	/**
	 * @brief Return the level of stress of the current phoneme.
	 * @return 0 if no stress, a positive integer otherwise. The greater, the more stressed.
	 */
	ipa::StressLevel get_stress_level() const;
	
	/**
	 * @brief Remove the stress information from the current phoneme.
	 */
	void unset_stress();

	/**
	 * @brief convert a phoneme into a string.
	 * @param level 0 => convert code in the associated alphabet (ipa if no alphabet); else return to_string(level-1) of the phoneme ipa(s).
	 **/
	virtual std::string to_string(int level=0) const;

	/**
	 * @brief Write the entity into a RootsStream
	 * @param stream the stream from which we inflate the element
	 * @param is_terminal indicates that the node is terminal (true by default)
	 * @param parentNode
	 */
	virtual void deflate (RootsStream * stream, bool is_terminal=true, int list_index=-1);

	/**
	 * @brief Extracts the entity from the stream
	 * @param stream the stream from which we inflate the element
	 */
	virtual void inflate(RootsStream * stream, bool is_terminal=true, int list_index=-1);

	/**
	 * @brief Extracts the entity from the stream
	 * @param stream the stream from which we inflate the element
	 */
	static Phoneme * inflate_object(RootsStream * stream, int list_index=-1);

	/**
	 * @brief returns the XML tag name value for current object
	 * @return string constant representing the XML tag name
	 */
	virtual std::string get_xml_tag_name() const { return xml_tag_name(); };

	/**
	 * @brief returns the XML tag name value for current class
	 * @return string constant representing the XML tag name
	 */
	static std::string xml_tag_name() { return "phoneme"; };

	/**
	 * @brief returns classname for current object
	 * @return string constant representing the classname
	 */
	virtual std::string get_classname() const { return classname(); };

	/**
	 * @brief returns classname for current class
	 * @return string constant representing the classname
	 */
	static std::string classname() { return "Phonology::Phoneme"; };

	/**
	 * @brief returns display color for current object
	 * @return string constant representing the pgf display color
	 */
	virtual std::string get_pgf_display_color() const { return pgf_display_color();};

	/**
	 * @brief returns display color for current class
	 * @return string constant representing the pgf display color
	 */
	static std::string pgf_display_color() { return "red!25"; };

      protected:

	/**
	 * @brief update label from ipa labels.
	 **/
	void update_label();

	/**
	 * @brief as get_ipan but may return NULL
	 * @param n optional, position of the Ipa. Default is 0.
	 * @return Ipa pointer to requested element
	 **/
	ipa::Ipa * unsafe_get_ipa(unsigned int n=0) const;

	/**
	 * @brief as set_ipan but accept NULL
	 * @param ipa Ipa to set (note that it is cloned)
	 * @param n optional, position of the Ipa. Default is 0.
	 * @param updateLabel optional, true to update the label. Default is true.
	 **/
	void unsafe_set_ipa(const ipa::Ipa* ipa, unsigned int n=0, bool updateLabel = true);

	std::vector<ipa::Ipa*> ipa;

	ipa::Alphabet * alphabet;
      };

  }
} // END OF NAMESPACES

#endif /* PHONEME_H_ */
