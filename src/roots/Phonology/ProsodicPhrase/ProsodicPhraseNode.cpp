/*
    This file is part of Roots.

    Roots is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Roots is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Roots.  If not, see <http://www.gnu.org/licenses/>.

    Copyright (c) 2013, 2014 IRISA
*/


#include "ProsodicPhraseNode.h"

namespace roots
{

namespace phonology
{

namespace prosodicphrase
{

  ProsodicPhraseNode::ProsodicPhraseNode(const bool noInit): TreeNode(noInit)
{
  start = -1;
  end = -1;
  prosodicGroup = PROSODIC_GROUP_UNKNOWN;
  phonemeIndexArray = std::vector<int>();
}

ProsodicPhraseNode::ProsodicPhraseNode(const ProsodicPhraseNode& node) : TreeNode(node)
{
	set_prosodic_group(node.get_prosodic_group());
	set_phoneme_index_array(node.get_phoneme_index_array());
	set_start(node.get_start());
	set_end(node.get_end());
}

ProsodicPhraseNode::~ProsodicPhraseNode()
{
}

ProsodicPhraseNode *ProsodicPhraseNode::clone() const
{
	return new ProsodicPhraseNode(*this);
}

ProsodicPhraseNode& ProsodicPhraseNode::operator=(const ProsodicPhraseNode &node)
{
	TreeNode::operator=(node);	
	set_prosodic_group(node.get_prosodic_group());
	set_phoneme_index_array(node.get_phoneme_index_array());
	set_start(node.get_start());
	set_end(node.get_end());
	
	return *this;
}

bool ProsodicPhraseNode::operator!=(const ProsodicPhraseNode& node) const
{
	return not (*this == node);
}

bool ProsodicPhraseNode::operator==(const ProsodicPhraseNode& node) const
{
	return (this->start == node.get_start()) 
		&& (this->end == node.get_end()) 
		&& (this->prosodicGroup == node.get_prosodic_group()) 
		&& (this->phonemeIndexArray == node.get_phoneme_index_array());
}

void ProsodicPhraseNode::set_phoneme_index_array(const std::vector<int>& aphonemeIndexArray) 
{ 
	phonemeIndexArray = aphonemeIndexArray;
	if(phonemeIndexArray.size()>0)
	{
		set_start(phonemeIndexArray[0]);
		set_end(phonemeIndexArray[phonemeIndexArray.size() - 1]);
	}
	else
	{
		set_start(-1);
		set_end(-1);
	}
}

void ProsodicPhraseNode::insert_related_element(int index)
{
	for(std::vector<int>::iterator itw=phonemeIndexArray.begin();
		itw!=phonemeIndexArray.end(); ++itw)
	{
		(*itw) = ((*itw)>=index) ? (*itw)+1 : (*itw);
	}
	
	set_start(phonemeIndexArray[0]);
	set_end(phonemeIndexArray.back());
}

void ProsodicPhraseNode::remove_related_element(int index)
{
	for(std::vector<int>::iterator itw=phonemeIndexArray.begin();
		itw!=phonemeIndexArray.end(); ++itw)
	{
		(*itw) = ((*itw)>=index) ? (*itw)-1 : (*itw);
	}	

	if(phonemeIndexArray.size()>0)
	{
		set_start(phonemeIndexArray[0]);
		set_end(phonemeIndexArray.back());	
	}else{
		set_start(-1);
		set_end(-1);		
	}
}

std::string ProsodicPhraseNode::to_string(int level) const
{
	std::stringstream ss;

	switch (level)
	{
		// level 0 : precise ProsodicPhraseNode tag
		case 0:
		default:
			ss 	<<  PROSODIC_GROUP_SHORT[get_prosodic_group()] ;
			break;
	}
	return std::string(ss.str().c_str());
}

std::string ProsodicPhraseNode::output_dot(unsigned int level, unsigned int rank) const
{
	std::stringstream ss, ss2;
	std::string str;
			
	ss << "\"" << rank << " ";

	std::vector<int> refPhonemeIndexArray = this->get_phoneme_index_array();
	int startPhonemeIndex = this->get_start();
	int endPhonemeIndex = this->get_end();
	
	if(refPhonemeIndexArray.size() > 0)
	{
		ss << "" << prosodicphrase::PROSODIC_GROUP[this->get_prosodic_group()] << " (";
		for(unsigned int idx=0;idx<refPhonemeIndexArray.size();idx++)
			ss << refPhonemeIndexArray[idx] << " ";
		ss << ")" << "\"";
	}else{
		ss << "" << level << ":" << prosodicphrase::PROSODIC_GROUP[this->get_prosodic_group()] << " [";
		ss << startPhonemeIndex << " " << endPhonemeIndex;
		ss << "]" << "\"";
	}

	if(get_number_of_children() == 0)
	{		
		str = std::string(ss.str().c_str());
		for(unsigned int idx=0;idx<refPhonemeIndexArray.size();idx++)
			ss2 << str << "-> \" " << refPhonemeIndexArray[idx] << "\" ;" << std::endl;
		str = std::string(ss2.str().c_str());
	}else{
		str = std::string(ss.str().c_str());
		
		int childIndex = 0;
		while(childIndex < get_number_of_children())
		{
			ss2 << str << " -> " << get_children()[childIndex]->output_dot(level+1, ++rank);
			++childIndex;
		}
		str = std::string(ss2.str().c_str());
	}

	return str;
}

	void ProsodicPhraseNode::deflate_content(RootsStream * stream)
	{
		stream->append_int_content("start_word_index", this->get_start());
		stream->append_int_content("end_word_index", this->get_end());
		stream->append_int_content("prosodic_group",  this->get_prosodic_group());//PROSODIC_GROUP[this->get_prosodic_group()]);
		stream->append_vector_int_content("phoneme_index_array", this->get_phoneme_index_array());
	}


	roots::tree::TreeNode* ProsodicPhraseNode::new_child(const bool /*noInit*/) const
{
	return new ProsodicPhraseNode();
}

	void ProsodicPhraseNode::inflate_content(RootsStream * stream, bool is_terminal)
	{
			
		//stream->open_children();
		this->set_start(stream->get_int_content("start_word_index"));
		this->set_end(stream->get_int_content("end_word_index"));
		ProsodicGroup prosodicGroup = (ProsodicGroup)stream->get_int_content("prosodic_group");
		if(prosodicGroup>=0 && prosodicGroup<PROSODIC_GROUP_LENGTH)
		{
			this->set_prosodic_group(prosodicGroup);
		}
		else
		{
			this->set_prosodic_group(PROSODIC_GROUP_UNKNOWN);
		}

		this->set_phoneme_index_array(stream->get_vector_int_content("phoneme_index_array"));
		if(is_terminal) stream->close_children();
	}

	ProsodicPhraseNode * ProsodicPhraseNode::inflate_object(RootsStream * stream, int list_index)
	{
		ProsodicPhraseNode *t = new ProsodicPhraseNode(true);
		t->inflate(stream,true,list_index);
		return t;	
	}

std::ostream& operator<<(std::ostream& out, ProsodicPhraseNode& ProsodicPhraseNode)
{
	out << ProsodicPhraseNode.to_string() << std::endl;
	return out;
}

} } } // END OF NAMESPACES
