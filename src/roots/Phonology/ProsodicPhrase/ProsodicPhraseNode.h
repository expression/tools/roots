/*
    This file is part of Roots.

    Roots is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Roots is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Roots.  If not, see <http://www.gnu.org/licenses/>.

    Copyright (c) 2013, 2014 IRISA
*/


#ifndef ProsodicPhraseNode_H_
#define ProsodicPhraseNode_H_

#include "../../common.h"
#include "../../Tree/TreeNode.h"


namespace roots { namespace phonology {

namespace prosodicphrase {

	static const int PROSODIC_GROUP_LENGTH = 4;
	static const std::string PROSODIC_GROUP[] = {
		 "unknown", "Phrase", "Intonation Group", "Accent Group"
	};
	static const std::string PROSODIC_GROUP_SHORT[] = {
		 "unkn.", "P", "IG", "AG"
	};
	/**
	 * @brief The possible syntactic functions
	 */
	enum ProsodicGroup
	{
		PROSODIC_GROUP_UNKNOWN 	= 0,	//!< PROSODIC_GROUP_UNKNOWN
		PROSODIC_GROUP_PHRASE 	= 1,	//!< PROSODIC_GROUP_PHRASE
		PROSODIC_GROUP_IG		= 2,	//!< PROSODIC_GROUP_IG (Intonation Group)
		PROSODIC_GROUP_AG		= 3		//!< PROSODIC_GROUP_AG (Accent Group)
	};

/**
 * @brief The node value in the prosodic phrase
 * @details
 * the node value describes a group in the prosodic phrase structure. 
 * The description is composed by the indexes of the different phonemes 
 * involved in the group and the kind of this group. Furthermore, the 
 * leaf of this tree contains an array of indexed phonemes.
 * The phoneme level has been chosen to be more versatile and possibly
 * take into account other structure than only syllable (for french).
 * @author Cordial Group
 * @version 0.1
 * @date 2012
 * @todo
 */
class ProsodicPhraseNode: public roots::tree::TreeNode
{
public:
	/**
	 * @brief Default constructor
	 */
	ProsodicPhraseNode(const bool noInit = false);
	/**
	 * @brief Constructor
	 */
	ProsodicPhraseNode(const ProsodicPhraseNode&);
	/**
	 * @brief Destructor
	 */
	virtual ~ProsodicPhraseNode();
	/**
	 * @brief clone the current object
	 * @return a copy of the object
	 */
	virtual ProsodicPhraseNode *clone() const;
	/**
	 * @brief Copy a node on the current one
	 * @param node
	 * @return reference on the current node
	 */
	ProsodicPhraseNode& operator= (const ProsodicPhraseNode& node);
	/**
	 * @brief Compares the content of two syntax nodes
	 * @param node
	 * @return true if node are different
	 */
	bool operator!=(const ProsodicPhraseNode& node) const;
	/**
	 * @brief Compares the content of two syntax nodes
	 * @param node
	 * @return true if node are equal
	 */
	bool operator==(const ProsodicPhraseNode& node) const;


	bool is_prosodic_group_unknown() const {return prosodicGroup == PROSODIC_GROUP_UNKNOWN;}
	bool is_prosodic_group_ig() const {return prosodicGroup == PROSODIC_GROUP_IG;}
	bool is_prosodic_group_ag() const {return prosodicGroup == PROSODIC_GROUP_AG;}

	void set_start(int astart) { start = astart;}
	void set_end(int aend) { end = aend;}
	void set_prosodic_group(ProsodicGroup atype) { prosodicGroup = atype;}
	void set_phoneme_index_array(const std::vector<int>& aphonemeIndexArray);

	int get_start() const {return start;};
	int get_end() const {return end;};
	ProsodicGroup get_prosodic_group() const {return prosodicGroup;};
	const std::vector<int>& get_phoneme_index_array() const {return phonemeIndexArray;};

	virtual roots::tree::TreeNode* new_child(const bool noInit = false) const;

	/**
	 * @brief Modifies the current syllable to take into account the insertion of an element into the related sequence
	 * @param index index of the element added in the related sequence
	 **/
	void insert_related_element(int index);

	/**
	 * @brief Modifies the current syllable to take into account the deletion of an element into the related sequence
	 * @param index index of the element removed from the related sequence
	 **/
	void remove_related_element(int index);

	virtual std::string to_string(int level=0) const;

	/**
	 * @brief Build a dot representation of the tree
	 * @param level
	 * @param rank prefix counter used for each node
	 * @return
	 */
	virtual std::string output_dot(unsigned int level=0, unsigned int rank=0) const;
	
	/**
	 * @brief Extracts the entity from the stream
	 * @param stream the stream from which we inflate the element
	 */
	static ProsodicPhraseNode * inflate_object(RootsStream * stream, int list_index=-1);
	/**
	 * @brief returns the XML tag name value for current object
	 * @return string constant representing the XML tag name
	 */
	virtual std::string get_xml_tag_name() const
	{
		return xml_tag_name();
	};
	/**
	 * @brief returns the XML tag name value for current class
	 * @return string constant representing the XML tag name
	 */
	static std::string xml_tag_name()
	{
		return "prosodic_phrase_node";
	};
	/**
	 * @brief returns classname for current object
	 * @return string constant representing the classname
	 */
	virtual std::string get_classname() const
	{
		return classname();
	};
	/**
	 * @brief returns classname for current class
	 * @return string constant representing the classname
	 */
	static std::string classname()
	{
		return "Phonology::ProsodicPhrase::Node";
	};
	/**
	 * @brief returns display color for current object
	 * @return string constant representing the pgf display color
	 */
	virtual std::string get_pgf_display_color() const { return pgf_display_color();};
	/**
	 * @brief returns display color for current class
	 * @return string constant representing the pgf display color
	 */
	static std::string pgf_display_color() { return "red!25"; };

	/**
	 * Serialize the object into a stream as a string
	 *
	 * @param out
	 * @param ProsodicPhraseNode
	 * @return the modified stream
	 */
    friend std::ostream& operator<<(std::ostream& out, ProsodicPhraseNode& ProsodicPhraseNode);

protected:
	/**
	 * @brief Write the entity into a RootsStream
	 * @param stream the stream from which we inflate the element
	 */
	virtual void deflate_content(RootsStream * stream);
	/**
	 * @brief Inflates the specific content of the current node
	 * @param stream
	 */
	virtual void inflate_content(RootsStream * stream, bool is_terminal=true);

private:
	int start; /**< the start index in the phoneme sequence */
	int end; /**< the end index in the phoneme sequence */
	ProsodicGroup prosodicGroup; /**< the kind of group */
	std::vector<int> phonemeIndexArray; /**< array which contains the phonemes' indices */
};

} } }

#endif /* ProsodicPhraseNode_H_ */
