/*
    This file is part of Roots.

    Roots is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Roots is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Roots.  If not, see <http://www.gnu.org/licenses/>.

    Copyright (c) 2013, 2014 IRISA
*/


#ifndef ProsodicPhrase_H_
#define ProsodicPhrase_H_

#include "../common.h"
#include "../Tree.h"
#include "../Sequence.h"
#include "../Sequence/PhonemeSequence.h"
#include "../Phonology/Phoneme.h"
#include "ProsodicPhrase/ProsodicPhraseNode.h"



namespace roots { namespace phonology { namespace prosodicphrase {

/**
 * @brief The ProsodicPhrase tree
 * @details
 * Models the ProsodicPhrase tree of an utterance. Each node
 * corresponds to a ProsodicPhrase level; each leaf is linked to a Phoneme.
 * @author Cordial Group
 * @version 0.1
 * @date 2012
 */
class ProsodicPhrase: public roots::tree::Tree<roots::phonology::prosodicphrase::ProsodicPhraseNode>
{
public:

	ProsodicPhrase(const bool noInit = false);
	ProsodicPhrase(const ProsodicPhrase &ProsodicPhrase);
	
	virtual ~ProsodicPhrase();
	/**
	 * @brief clone the current object
	 * @return a copy of the object
	 */
	virtual ProsodicPhrase *clone() const;

	ProsodicPhrase& operator= (const ProsodicPhrase&);

	sequence::PhonemeSequence* get_phoneme_sequence() const {return _phonemeSequence;} ;
	sequence::Sequence* get_related_sequence() const {return get_phoneme_sequence();} ;
	void set_phoneme_sequence(sequence::PhonemeSequence* wseq) { _phonemeSequence = wseq; }
	void set_related_sequence(sequence::Sequence* _relatedSequence){ set_phoneme_sequence(_relatedSequence->as<sequence::PhonemeSequence>());}
	bool has_related_sequence() const {return true;}
	
	const std::string get_phoneme_sequence_id() const;
	//const kptree::tree<ProsodicPhraseNode>& get_prosodic_phrase_tree() const { return _prosodicPhraseTree;}
	std::vector<int> get_phoneme_index_array();

	int get_first_target_index() const;
	int get_last_target_index() const;

	//int get_phoneme_depth(int phonemeIndex) const;
	ProsodicPhraseNode* get_phoneme_prosodic_phrase_node(int phonemeIndex) const;
	std::vector<ProsodicPhraseNode*> get_prosodic_phrase_node_path_to_phoneme(int phonemeIndex) const;

private:


public:
	std::string to_string( int level=0 ) const;

	/**
	 * @brief Write the entity into a RootsStream
	 * @param stream the stream from which we inflate the element
	 * @param is_terminal indicates that the node is terminal (true by default)
	 * @param parentNode
	 */
	virtual void deflate(RootsStream * stream, bool is_terminal=true, int list_index=-1);
	/**
	 * @brief Extracts the entity from the stream
	 * @param stream the stream from which we inflate the element
	 */
	virtual void inflate(RootsStream * stream, bool is_terminal=true, int list_index=-1);
	/**
	 * @brief Extracts the entity from the stream
	 * @param stream the stream from which we inflate the element
	 */
	static ProsodicPhrase * inflate_object(RootsStream * stream, int list_index=-1);	
	/**
	 * @brief returns the XML tag name value for current object
	 * @return string constant representing the XML tag name
	 */
	virtual std::string get_xml_tag_name() const
	{
		return xml_tag_name();
	};
	/**
	 * @brief returns the XML tag name value for current class
	 * @return string constant representing the XML tag name
	 */
	static std::string xml_tag_name()
	{
		return "prosodic_phrase";
	};
	/**
	 * @brief returns classname for current object
	 * @return string constant representing the classname
	 */
	virtual std::string get_classname() const
	{
		return classname();
	};
	/**
	 * @brief returns classname for current class
	 * @return string constant representing the classname
	 */
	static std::string classname()
	{
		return "Phonology::ProsodicPhrase";
	};
	/**
	 * @brief returns display color for current object
	 * @return string constant representing the pgf display color
	 */
	virtual std::string get_pgf_display_color() const { return pgf_display_color();};
	/**
	 * @brief returns display color for current class
	 * @return string constant representing the pgf display color
	 */
	static std::string pgf_display_color() { return "red!25"; };
	/**
	 * Serialize the object into a stream as a string
	 *
	 * @param out
	 * @param ProsodicPhrase
	 * @return the modified stream
	 */
    friend std::ostream& operator<<(std::ostream& out, ProsodicPhrase& ProsodicPhrase);

private:
	sequence::PhonemeSequence *_phonemeSequence; /**< reference on the phoneme sequence */
};

} } }

#endif /* ProsodicPhrase_H_ */
