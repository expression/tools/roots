/*
    This file is part of Roots.

    Roots is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Roots is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Roots.  If not, see <http://www.gnu.org/licenses/>.

    Copyright (c) 2013, 2014 IRISA
*/


#ifndef NSA_H_
#define NSA_H_

#include <map>
#include "../common.h"
#include "../BaseItem.h"
#include "NsaException.h"

#include <boost/assign/list_of.hpp> // for 'map_list_of()'
#include <boost/assign/list_inserter.hpp>

namespace roots { namespace phonology { namespace nsa {

static const int LEVEL_LABEL = 0;
static const int LEVEL_CLASS_FINE = 1;
static const int LEVEL_CLASS_COARSE = 2;

enum Origin
{
	ORIGIN_UNDEF = 0,
	ORIGIN_HUMAN = 1,
	ORIGIN_MUSIC = 2,
	ORIGIN_OTHER = 3
};
static const int ORIGINS_LENGTH = 4;
static const std::string ORIGINS[] = {
		"undef", "human", "music", "other"
	};

enum Category
{
	CATEGORY_UNDEF				= 0,
	CATEGORY_SILENCE			= 1,
	CATEGORY_INSPIRATION		= 2,
	CATEGORY_NOISE				= 3,
	CATEGORY_PHONATION			= 4,
	CATEGORY_CPROM_PARAVERBAL	= 5
};
static const int CATEGORIES_LENGTH = 6;
static const std::string CATEGORIES[] = {
		"undef", "silence", "inspiration", "noise", "phonation", "cprom_paraverbal"
};

enum Duration
{
	DURATION_UNDEF	= 0,
	DURATION_NULL 	= 1,
	DURATION_SHORT 	= 2,
	DURATION_MEDIUM = 3,
	DURATION_LONG 	= 4
};
static const int DURATIONS_LENGTH = 5;
static const std::string DURATIONS[] = {
		"undef", "null", "short", "medium", "long"
};


class Nsa: public roots::BaseItem
{
public:
	Nsa(const bool noInit = false);
	Nsa(const Nsa& nsa);
	virtual ~Nsa();

	virtual Nsa *clone() const;

	Nsa& operator=(const Nsa& nsa);

	const std::string& get_tag() const { return tag;};
	Category get_category() const { return category;};
	Duration get_duration() const { return duration;};
	Origin get_origin() const { return origin;};

	void set_tag(const std::string& atag) { tag = atag;};
	void set_category(const Category acategory) { category = acategory;};
	void set_duration(const Duration aduration) { duration = aduration;};
	void set_origin(const Origin aorigin) { origin = aorigin;};

	bool is_silence() const;
	bool is_inspiration() const;
	bool is_noise() const;
	bool is_phonation() const;
	bool is_cprom_paraverbal() const;

	void from_tag();
	static Nsa from_tag(const std::string& atag);

	void clear_feature();

	std::string hash_feature() const;

	static Nsa tag_to_nsa(const std::string& atag);

	static void init();

	virtual std::string to_string(int level=LEVEL_LABEL) const;

	/**
	 * @brief Write the entity into a RootsStream
	 * @param stream the stream from which we inflate the element
	 * @param is_terminal indicates that the node is terminal (true by default)
	 * @param parentNode
	 */
	virtual void deflate(RootsStream * stream, bool is_terminal=true, int list_index=-1);
	/**
	 * @brief Extracts the entity from the stream
	 * @param stream the stream from which we inflate the element
	 */
	virtual void inflate(RootsStream * stream, bool is_terminal=true, int list_index=-1);
	/**
	 * @brief Extracts the entity from the stream
	 * @param stream the stream from which we inflate the element
	 */
	static Nsa * inflate_object(RootsStream * stream, int list_index=-1);
	/**
	 * @brief returns the XML tag name value for current object
	 * @return string constant representing the XML tag name 
	 */
	virtual std::string get_xml_tag_name() const { return xml_tag_name(); };
	/**
	 * @brief returns the XML tag name value for current class
	 * @return string constant representing the XML tag name 
	 */
	static std::string xml_tag_name() { return "nsa"; };
	/**
	 * @brief returns classname for current object
	 * @return string constant representing the classname
	 */
	virtual std::string get_classname() const { return classname(); };
	/**
	 * @brief returns classname for current class
	 * @return string constant representing the classname
	 */
	static std::string classname() { return "Phonology::Nsa"; };

private:
	std::string tag;
	Category category;
	Origin origin;
	Duration duration;

    static std::map<std::string, Nsa> tagToNsaMap;
    static std::map<std::string, std::string> nsaToTagMap;

	Nsa(const std::string& atag, const Category& acategory,
				const Origin& aorigin, const Duration& aduration);

	static void initialize_tag_to_nsa_map();
	static void initialize_nsa_to_tag_map();

};

} } }

#endif /* NSA_H_ */
