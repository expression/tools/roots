/*
    This file is part of Roots.

    Roots is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Roots is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Roots.  If not, see <http://www.gnu.org/licenses/>.

    Copyright (c) 2013, 2014 IRISA
*/


#include "PhonemeBlock.h"

namespace roots
{

  namespace phonology
  {

    std::map<std::string, unsigned int> PhonemeBlock::stringToContextMap = boost::assign::map_list_of
      ("empty", PHONEME_BLOCK_CONTEXT_EMPTY)
      ("consonant", PHONEME_BLOCK_CONTEXT_CONSONANT)
      ("no_consonant", PHONEME_BLOCK_CONTEXT_NO_CONSONANT)
      ("end_of_sentence", PHONEME_BLOCK_CONTEXT_END_OF_SENTENCE)
      ("no_cons_cons", PHONEME_BLOCK_CONTEXT_NO_CONSONANT_CONSONANT)
      ("no_nasal", PHONEME_BLOCK_CONTEXT_NO_NASAL)
      ("nasal", PHONEME_BLOCK_CONTEXT_NASAL)
      ("open_syllable", PHONEME_BLOCK_CONTEXT_OPEN_SYLLABLE)
      ("closed_syllable", PHONEME_BLOCK_CONTEXT_CLOSED_SYLLABLE)
      ("no_cons_no_cons", PHONEME_BLOCK_CONTEXT_NO_CONSONANT_NO_CONSONANT)
      ("no_liaison_after", PHONEME_BLOCK_CONTEXT_NO_LIAISON_AFTER)
      ("all", PHONEME_BLOCK_CONTEXT_ALL);
    std::map<unsigned int, std::string> PhonemeBlock::contextToStringMap = boost::assign::map_list_of
      (PHONEME_BLOCK_CONTEXT_EMPTY, "empty")
      (PHONEME_BLOCK_CONTEXT_CONSONANT, "consonant")
      (PHONEME_BLOCK_CONTEXT_NO_CONSONANT, "no_consonant")
      (PHONEME_BLOCK_CONTEXT_END_OF_SENTENCE, "end_of_sentence")
      (PHONEME_BLOCK_CONTEXT_NO_CONSONANT_CONSONANT, "no_cons_cons")
      (PHONEME_BLOCK_CONTEXT_NO_NASAL, "no_nasal")
      (PHONEME_BLOCK_CONTEXT_NASAL, "nasal")
      (PHONEME_BLOCK_CONTEXT_OPEN_SYLLABLE, "open_syllable")
      (PHONEME_BLOCK_CONTEXT_CLOSED_SYLLABLE, "closed_syllable")
      (PHONEME_BLOCK_CONTEXT_NO_CONSONANT_NO_CONSONANT, "no_cons_no_cons")
      (PHONEME_BLOCK_CONTEXT_NO_LIAISON_AFTER, "no_liaison_after")
      (PHONEME_BLOCK_CONTEXT_ALL, "all");



    PhonemeBlock::PhonemeBlock(const bool noInit):BaseItem(noInit)
    {
      phonemeSequence = NULL;
      startIndex = -1;
      endIndex = -1;
      forbiddenLiaisonBefore = false;
      possibleLiaisonAfter = false;
      this->leftContext = PHONEME_BLOCK_CONTEXT_EMPTY;
      this->rightContext = PHONEME_BLOCK_CONTEXT_EMPTY;
    }

    PhonemeBlock::PhonemeBlock(const PhonemeBlock& ph) : BaseItem(ph)
    {
      phonemeSequence = ph.get_phoneme_sequence();
      startIndex = ph.get_start_index();
      endIndex = ph.get_end_index();
      optional = ph.get_optional();
      forbiddenLiaisonBefore = ph.is_forbidden_liaison_before();
      possibleLiaisonAfter = ph.is_possible_liaison_after();
      this->leftContext = ph.get_left_context();
      this->rightContext = ph.get_right_context();
    }

    PhonemeBlock::~PhonemeBlock()
    {

    }

    PhonemeBlock *PhonemeBlock::clone() const
    {
      return new PhonemeBlock(*this);
    }

    PhonemeBlock& PhonemeBlock::operator= (const PhonemeBlock &ph)
    {
      BaseItem::operator=(ph);

      phonemeSequence = ph.get_phoneme_sequence();
      startIndex = ph.get_start_index();
      endIndex = ph.get_end_index();
      optional = ph.get_optional();
      forbiddenLiaisonBefore = ph.is_forbidden_liaison_before();
      possibleLiaisonAfter = ph.is_possible_liaison_after();
      this->leftContext = ph.get_left_context();
      this->rightContext = ph.get_right_context();

      return *this;
    }

    sequence::PhonemeSequence * PhonemeBlock::get_phoneme_sequence() const
    {
      return this->phonemeSequence;
    }

    void PhonemeBlock::set_phoneme_sequence(sequence::PhonemeSequence *s)
    {
      this->phonemeSequence = s;
    }

    bool PhonemeBlock::has_related_sequence() const
    {
      return (get_phoneme_sequence() != NULL);
    }

    void PhonemeBlock::set_related_sequence(sequence::Sequence *_relatedSequence)
    {
      set_phoneme_sequence(_relatedSequence->as<sequence::PhonemeSequence>());
    }

    sequence::Sequence * PhonemeBlock::get_related_sequence() const
    {
      return get_phoneme_sequence();
    }

    void PhonemeBlock::insert_related_element(int index)
    {
      if(index<this->startIndex) 
      {
        this->startIndex++;
        this->endIndex++;
      }else if(index>this->endIndex)
      {
        // Nothing to do
      }else{
        this->endIndex++;
        int toInsert = index - this->startIndex;
        optional.insert(optional.begin()+toInsert, false);
      }

    }

    void PhonemeBlock::remove_related_element(int index)
    {   
      if(index<this->startIndex) 
      {
        this->startIndex--;
        this->endIndex--;
      }else if(index>this->endIndex)
      {
        // Nothing to do
      }else{
        this->endIndex--;
        int toRemove = index - this->startIndex;
        optional.erase(optional.begin()+toRemove, optional.begin()+toRemove+1);
      }
    }


    int PhonemeBlock::get_start_index() const
    {
      return this->startIndex;
    }

    int PhonemeBlock::get_end_index() const
    {
      return this->endIndex;
    }

    void PhonemeBlock::set_start_index(int i)
    {
      this->startIndex = i;
    }

    void PhonemeBlock::set_end_index(int i)
    {
      this->endIndex = i;
    }

    unsigned int PhonemeBlock::count() const
    {
      if (startIndex == -1 || endIndex == -1) {
	return 0;
      }
      else {
	return std::max(0, endIndex - startIndex + 1);
      }
    }

    const std::vector<bool>& PhonemeBlock::get_optional() const
    {
      return this->optional;
    }

    bool PhonemeBlock::is_optional(int index) const
    {
      if((index-startIndex)<0 || (index-startIndex)>endIndex)
	{
      std::stringstream ss;
	  ss << "Index value " << index << " is out of bounds!";
	  ROOTS_LOGGER_ERROR(ss.str());
	  throw OutOfBoundIndexException(__FILE__, __LINE__, ss.str());
	}

      return optional[index-startIndex];
    }

    void PhonemeBlock::set_optional(const std::vector<bool>& vec)
    {
      this->optional = vec;
    }

    void PhonemeBlock::set_optional(int index, bool value)
    {
      if((index-startIndex)<0 || (index-startIndex)>endIndex)
	{
	  std::stringstream ss;
	  ss << "Index value " << index << " is out of bounds!";
	  ROOTS_LOGGER_ERROR(ss.str());
	  throw OutOfBoundIndexException(__FILE__, __LINE__, ss.str());
	}

      if(index-startIndex == endIndex)
	{
	  optional.push_back(value);
	}
      else
	optional[index-startIndex] = value;
    }

    bool PhonemeBlock::is_forbidden_liaison_before() const
    {
      return this->forbiddenLiaisonBefore;
    }

    void PhonemeBlock::set_forbidden_liaison_before(bool b)
    {
      this->forbiddenLiaisonBefore = b;
    }

    bool PhonemeBlock::is_possible_liaison_after() const
    {
      return this->possibleLiaisonAfter;
    }

    void PhonemeBlock::set_possible_liaison_after(bool b)
    {
      this->possibleLiaisonAfter = b;
    }

    uint32_t PhonemeBlock::get_left_context() const
    {
      return leftContext;
    }

    uint32_t PhonemeBlock::get_right_context() const
    {
      return rightContext;
    }
    
    std::vector<uint32_t> PhonemeBlock::get_left_context_vector() const
    {
      std::vector<uint32_t> contexts;
      if(leftContext&PHONEME_BLOCK_CONTEXT_OPEN_SYLLABLE)
      {
	contexts.push_back(PHONEME_BLOCK_CONTEXT_OPEN_SYLLABLE);
      }
      if(leftContext&PHONEME_BLOCK_CONTEXT_CLOSED_SYLLABLE)
      {
	contexts.push_back(PHONEME_BLOCK_CONTEXT_CLOSED_SYLLABLE);
      }
      return contexts;
    }
    
    std::vector<uint32_t> PhonemeBlock::get_right_context_vector() const
    {
      std::vector<uint32_t> contexts;
      if(rightContext&PHONEME_BLOCK_CONTEXT_CONSONANT)
      {
	contexts.push_back(PHONEME_BLOCK_CONTEXT_CONSONANT);
      }
      if(rightContext&PHONEME_BLOCK_CONTEXT_NO_CONSONANT)
      {
	contexts.push_back(PHONEME_BLOCK_CONTEXT_NO_CONSONANT);
      }
      if(rightContext&PHONEME_BLOCK_CONTEXT_END_OF_SENTENCE)
      {
	contexts.push_back(PHONEME_BLOCK_CONTEXT_END_OF_SENTENCE);
      }
      if(rightContext&PHONEME_BLOCK_CONTEXT_NO_CONSONANT_CONSONANT)
      {
	contexts.push_back(PHONEME_BLOCK_CONTEXT_NO_CONSONANT_CONSONANT);
      }
      if(rightContext&PHONEME_BLOCK_CONTEXT_NO_CONSONANT_NO_CONSONANT)
      {
	contexts.push_back(PHONEME_BLOCK_CONTEXT_NO_CONSONANT_NO_CONSONANT);
      }
      if(rightContext&PHONEME_BLOCK_CONTEXT_NO_NASAL)
      {
	contexts.push_back(PHONEME_BLOCK_CONTEXT_NO_NASAL);
      }
      if(rightContext&PHONEME_BLOCK_CONTEXT_NASAL)
      {
	contexts.push_back(PHONEME_BLOCK_CONTEXT_NASAL);
      }
      if(rightContext&PHONEME_BLOCK_CONTEXT_NO_LIAISON_AFTER)
      {
	contexts.push_back(PHONEME_BLOCK_CONTEXT_NO_LIAISON_AFTER);
      }
      return contexts;
    }

    void PhonemeBlock::set_left_context(uint32_t ctxg)
    {
      this->leftContext = ctxg;
    }

    void PhonemeBlock::set_right_context(uint32_t ctxd)
    {
      this->rightContext = ctxd;
    }

    void PhonemeBlock::add_left_context(uint32_t ctxg_value)
    {
      this->leftContext |= ctxg_value;
    }

    void PhonemeBlock::add_right_context(uint32_t ctxd_value)
    {
      this->rightContext |= ctxd_value;
    }
    
    void PhonemeBlock::add_left_context(const std::string & ctxg_str)
    {
      if (stringToContextMap.find(ctxg_str) != stringToContextMap.end()) {
	u_int32_t ctxg_value = stringToContextMap[ctxg_str];
	this->leftContext |= ctxg_value;
      }
    }

    void PhonemeBlock::add_right_context(const std::string & ctxd_str)
    {
      if (stringToContextMap.find(ctxd_str) != stringToContextMap.end()) {
	u_int32_t ctxd_value = stringToContextMap[ctxd_str];
	this->rightContext |= ctxd_value;
      }
    }

    void PhonemeBlock::remove_left_context(uint32_t ctxg_value)
    {
      this->leftContext &= !(ctxg_value);
    }

    void PhonemeBlock::remove_right_context(uint32_t ctxd_value)
    {
      this->rightContext &= !(ctxd_value);
    }

    std::string PhonemeBlock::contextToString(uint32_t value) const
    {
      std::stringstream ss;
      if(value==PHONEME_BLOCK_CONTEXT_EMPTY) ss << contextToStringMap[value];
      else if(value==PHONEME_BLOCK_CONTEXT_ALL) ss << contextToStringMap[value];
      else
	{
	  if(value&PHONEME_BLOCK_CONTEXT_CONSONANT)
	    {
	      ss << contextToStringMap[PHONEME_BLOCK_CONTEXT_CONSONANT];
	    }
	  if(value&PHONEME_BLOCK_CONTEXT_NO_CONSONANT)
	    {
	      if(ss.str() != "") ss << " ";
	      ss << contextToStringMap[PHONEME_BLOCK_CONTEXT_NO_CONSONANT];
	    }
	  if(value&PHONEME_BLOCK_CONTEXT_END_OF_SENTENCE)
	    {
	      if(ss.str() != "") ss << " ";
	      ss << contextToStringMap[PHONEME_BLOCK_CONTEXT_END_OF_SENTENCE];
	    }
	  if(value&PHONEME_BLOCK_CONTEXT_NO_CONSONANT_CONSONANT)
	    {
	      if(ss.str() != "") ss << " ";
	      ss <<contextToStringMap[PHONEME_BLOCK_CONTEXT_NO_CONSONANT_CONSONANT];
	    }
	  if(value&PHONEME_BLOCK_CONTEXT_NO_NASAL)
	    {
	      if(ss.str() != "") ss << " ";
	      ss << contextToStringMap[PHONEME_BLOCK_CONTEXT_NO_NASAL];
	    }
	  if(value&PHONEME_BLOCK_CONTEXT_NASAL)
	    {
	      if(ss.str() != "") ss << " ";
	      ss << contextToStringMap[PHONEME_BLOCK_CONTEXT_NASAL];
	    }
	  if(value&PHONEME_BLOCK_CONTEXT_OPEN_SYLLABLE)
	    {
	      if(ss.str() != "") ss << " ";
	      ss << contextToStringMap[PHONEME_BLOCK_CONTEXT_OPEN_SYLLABLE];
	    }
	  if(value&PHONEME_BLOCK_CONTEXT_CLOSED_SYLLABLE)
	    {
	      if(ss.str() != "") ss << " ";
	      ss << contextToStringMap[PHONEME_BLOCK_CONTEXT_CLOSED_SYLLABLE];
	    }
	  if(value&PHONEME_BLOCK_CONTEXT_NO_CONSONANT_NO_CONSONANT)
	    {
	      if(ss.str() != "") ss << " ";
	      ss << contextToStringMap[PHONEME_BLOCK_CONTEXT_NO_CONSONANT_NO_CONSONANT];
	    }
	  if(value&PHONEME_BLOCK_CONTEXT_NO_LIAISON_AFTER)
	    {
	      if(ss.str() != "") ss << " ";
	      ss << contextToStringMap[PHONEME_BLOCK_CONTEXT_NO_LIAISON_AFTER];
	    }
	}
      return std::string(ss.str().c_str());
    }



    std::string PhonemeBlock::to_string(int level) const
    {
        std::stringstream ss;
		/*		unsigned int ipaIndex;
		 * 
		 f o*r(ipaIndex=0; ipaIndex<variant.size(); ipaIndex++)
		 {
			 if(get_optional_by_index(ipaIndex) && ((ipaIndex == 0) || (!get_optional_by_index(ipaIndex-1)))) {	ss << "("; }
			 ss << variant[ipaIndex]->to_string(level);
			 if(ipaIndex == (variant.size()-1) && this->is_possible_liaison()) {	ss << "\""; }
			 if(get_optional_by_index(ipaIndex) && ((ipaIndex == variant.size()-1) || (!get_optional_by_index(ipaIndex+1)))) {	ss << ")"; }
			 }
			 */
		switch(level) {
			case 0:
				ss << "(";
				ss << contextToString(this->leftContext);
				ss << ",";
				ss << contextToString(this->rightContext);
				ss << ")";
				break;
			case 1:
				ss.setf ( std::ios::hex, std::ios::basefield );
				ss << "(";
				ss << (int) this->leftContext;
				ss << ",";
				ss << (int) this->rightContext;
				ss << ")";
				break;
			default:
				ss << "range = (" << startIndex << "," << endIndex << ")" << std::endl;
				ss << "Optional = [ ";
				unsigned int n = count();
				for (unsigned int i = 0; i < n; i++) {
					ss << optional[i] << " ";
				}
				ss << "]" << std::endl;
				ss << "forbiddenLiaisonBefore = " << (forbiddenLiaisonBefore?"true":"false") << std::endl;
				ss << "possibleLiaisonAfter = " << (possibleLiaisonAfter?"true":"false") << std::endl;
		}
		return std::string(ss.str().c_str());
	}

    void PhonemeBlock::deflate(RootsStream * stream, bool is_terminal, int list_index)
    {
      BaseItem::deflate(stream,false,list_index);

      stream->append_unicodestring_content("phoneme_sequence_id", this->get_phoneme_sequence()->get_id());

      stream->append_uint_content("start_index", this->get_start_index());
      stream->append_uint_content("end_index", this->get_end_index());
      stream->append_bool_content("forbiddenliaisonbefore", is_forbidden_liaison_before());
      stream->append_bool_content("possibleliaisonafter", is_possible_liaison_after());
      stream->append_vector_bool_content("optional", optional);
      stream->append_int_content("left_context", this->get_left_context());
      stream->append_int_content("right_context", this->get_right_context());

      if(is_terminal)
	stream->close_object();
    }

    void PhonemeBlock::inflate(RootsStream * stream, bool is_terminal, int list_index)
    {
      std::string id;
      roots::sequence::PhonemeSequence *seq=NULL;

      BaseItem::inflate(stream,false,list_index);

      id = stream->get_unicodestring_content("phoneme_sequence_id");
      seq = (roots::sequence::PhonemeSequence*)stream->get_object_ptr(id);
      if(seq == NULL)
	{
	  std::stringstream ss;
	  ss << "cannot find sequence with id="<< id << " while inflating PhonemeBlock object!";
	  std::cerr << ss.str().c_str() << std::endl;
	  throw RootsException(__FILE__, __LINE__, ss.str().c_str());
	}
      this->set_phoneme_sequence(seq);

      this->startIndex = stream->get_uint_content("start_index");
      this->endIndex = stream->get_uint_content("end_index");
      this->forbiddenLiaisonBefore = stream->get_bool_content("forbiddenliaisonbefore");
      this->possibleLiaisonAfter = stream->get_bool_content("possibleliaisonafter");
      this->optional = stream->get_vector_bool_content("optional");
      this->set_left_context(stream->get_int_content("left_context"));
      this->set_right_context(stream->get_int_content("right_context"));

      if(is_terminal) stream->close_children();
    }

    PhonemeBlock * PhonemeBlock::inflate_object(RootsStream * stream, int list_index)
    {
      PhonemeBlock *t = new PhonemeBlock(true);
      t->inflate(stream,true,list_index);
      return t;
    }

  } } // END OF NAMESPACES
