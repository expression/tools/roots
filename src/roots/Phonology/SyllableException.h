/*
    This file is part of Roots.

    Roots is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Roots is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Roots.  If not, see <http://www.gnu.org/licenses/>.

    Copyright (c) 2013, 2014 IRISA
*/


#ifndef SYLLABLE_EXCEPTION_H_
#define SYLLABLE_EXCEPTION_H_

#include "../common.h"
#include "../RootsException.h"
#include <exception>

namespace roots {
namespace phonology { namespace syllable {


class SyllableException : public roots::RootsException
{
public:
    SyllableException(const char* file, const int line, const std::string& msg) :
	roots::RootsException(file, line, msg) {};
	virtual ~SyllableException() throw() {};
};

class SyllableNoNucleusException : public SyllableException
{
public:
	SyllableNoNucleusException(const char* file, const int line, const std::string& msg) :
	SyllableException(file, line, msg) {};
	virtual ~SyllableNoNucleusException() throw() {};
};

class SyllableMultipleVowelException : public SyllableException
{
public:
	SyllableMultipleVowelException(const char* file, const int line, const std::string& msg) :
	SyllableException(file, line, msg) {};
	virtual ~SyllableMultipleVowelException() throw() {};
};
} } }
#endif /* SYLLABLE_EXCEPTION_H_ */
