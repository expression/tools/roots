/*
    This file is part of Roots.

    Roots is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Roots is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Roots.  If not, see <http://www.gnu.org/licenses/>.

    Copyright (c) 2013, 2014 IRISA
*/

#ifndef MAPPED_ALPHABET_H_
#define MAPPED_ALPHABET_H_

#include <algorithm>
#include "../Phoneme.h"
#include "Alphabet.h"
#include "AlphabetException.h"

#include <boost/assign/list_inserter.hpp>
#include <boost/assign/list_of.hpp>  // for 'map_list_of()'

namespace roots {
namespace phonology {
//     class Phoneme; // Forward declaration needed by Alphabet::convert_phoneme
//     function

namespace ipa {

inline std::string string_format(const std::string& fmt, ...) {
  int size = 100;
  std::string str;
  va_list ap;
  while (1) {
    str.resize(size);
    va_start(ap, fmt);
    int n = vsnprintf((char*)str.c_str(), size, fmt.c_str(), ap);
    va_end(ap);
    if (n > -1 && n < size) {
      str.resize(n);
      return str;
    }
    if (n > -1)
      size = n + 1;
    else
      size *= 2;
  }
  return str;
}

typedef std::pair<std::vector<UChar>, std::set<UChar> > t_alphabet_ipas;
typedef std::pair<bool, t_alphabet_ipas> t_alphabet_elt;
typedef std::map<std::string, t_alphabet_elt> t_alphabetMap;
typedef std::map<t_alphabet_elt, std::string> t_reversed_alphabetMap;
typedef std::map<std::string, UChar> t_diacriticMap;
typedef std::map<UChar, std::string> t_reversed_diacriticMap;

template <class A>
class MappedAlphabet : public roots::phonology::ipa::Alphabet, public Base {
 protected:
  MappedAlphabet(const std::string& name, const bool caseSensitive = true)
      : Alphabet(name, caseSensitive) {
    alphabetMapChanged = false;

    alphabetMap = t_alphabetMap();
    reversedAlphabetMap = t_reversed_alphabetMap();
    diacriticMap = t_diacriticMap();
    reversedDiacriticMap = t_reversed_diacriticMap();
  }

 public:
  virtual ~MappedAlphabet() {}

  t_alphabetMap& get_alphabet_map() { return alphabetMap; };
  t_diacriticMap& get_diacritic_map() { return diacriticMap; };

  bool is_label(const std::string& label) const {
    t_alphabetMap::const_iterator iter = alphabetMap.begin();
    bool found = false;

    while (iter != alphabetMap.end() && !found) {
      found = ((*iter).first).compare(label) == 0;
      iter++;
    }

    return found;
  }

  std::vector<UChar> get_symbols(const std::string& label) {
    std::vector<UChar> vec;
    if (alphabetMap.find(label) != alphabetMap.end()) {
      vec = std::vector<UChar>(alphabetMap[label].second.first);
      return vec;
    }

    AlphabetException e(__FILE__, __LINE__, "Unknown label!\n");
    throw(e);
    return vec;
  }

  void add_symbol(const std::string& label) {
    if (!this->is_label(label)) {
      alphabetMap[label] = t_alphabet_elt(
          true, t_alphabet_ipas(std::vector<UChar>(), std::set<UChar>()));
      alphabetMapChanged = true;
    } else {
      AlphabetException e(__FILE__, __LINE__,
                          "Label " + label + " already exists!\n");
      throw(e);
    }
  }

  void add_symbol(const std::string& label, const t_alphabet_elt& symbol) {
    if (!this->is_label(label)) {
      alphabetMap[label] = symbol;
      alphabetMapChanged = true;
    } else {
      AlphabetException e(__FILE__, __LINE__, "Label already exists!\n");
      throw(e);
    }
  }

  /**
   * @brief Test is a given string symbol can be mapped to any diacritic in the
   * current alphabet
   * @param diacritic_str String to tested for parsing
   * @param diac_pos Optional, limit the test of preceding or following
   * diacritics in the alphabet
   * @return bool
   */
  bool is_diacritic(const std::string& diacritic_str,
                    roots::phonology::ipa::DiacriticPosition diac_pos =
                        DIACRITIC_POSITION_ANY) const {
    t_diacriticMap::const_iterator iter = diacriticMap.begin();
    bool found = false;

    while (iter != diacriticMap.end() && !found) {
      // TODO: solve problem here with diacritic 0 -> 0x0000
      if (((*iter).first).compare(diacritic_str) == 0) {
        if ((*iter).second ==
            0x0000)  // just ignore the diacritic (usually no stress)
        {
          found = true;
        } else {
          roots::phonology::ipa::Ipa ipa = Ipa::unicode_to_ipa(iter->second);
          roots::phonology::ipa::Diacritic diac =
              ipa.get_diacritics().at(0);  // Logically, the diacritic exists
          found = (diac_pos == DIACRITIC_POSITION_ANY ||
                   (diac_pos == DIACRITIC_POSITION_PRECEDING &&
                    is_preceding_diacritic(diac)) ||
                   (diac_pos == DIACRITIC_POSITION_FOLLOWING &&
                    is_following_diacritic(diac)));
        }
      }
      iter++;
    }

    return found;
  }

  /**
   * @brief Test if a given diacritic should precede its combined symbol in the
   * current alphabet
   * @param diac The diacritic to be tested
   * @return True if preceding or indifferent, false otherwise
   * @notice By default, no diacritic is supposed to precede its symbol.
   */
  bool is_preceding_diacritic(roots::phonology::ipa::Diacritic /*diac*/) const {
    return false;
  }

  /**
   * @brief Test if a given diacritic should follow its combined symbol in the
   * current alphabet
   * @param diac The diacritic to be tested
   * @return True if following or indifferent, false otherwise
   * * @notice By default, all diacritics are supposed to follow their symbol.
   */
  bool is_following_diacritic(roots::phonology::ipa::Diacritic) const {
    return true;
  }

  UChar get_diacritic(const std::string& diacritic_str) {
    if (diacriticMap.find(diacritic_str) != diacriticMap.end()) {
      return diacriticMap[diacritic_str];
    }

    AlphabetException e(__FILE__, __LINE__, "Unknown diacritic!\n");
    throw(e);
    return 0;
  }

  void add_diacritic(const std::string& diacritic_str,
                     const UChar& diacritic_symbol) {
    if (!this->is_diacritic(diacritic_str)) {
      diacriticMap[diacritic_str] = diacritic_symbol;
    } else {
      AlphabetException e(__FILE__, __LINE__, "Diacritic already exists!\n");
      throw(e);
    }
  }

  std::set<UChar> get_diacritic_symbols(const std::string& label) {
    std::set<UChar> vec;
    if (alphabetMap.find(label) != alphabetMap.end()) {
      vec = alphabetMap[label].second.second;
    }

    return vec;
  }

  /*
   * @brief Search for a primary symbol in the alphabet
   * @param symbols the UChar vector corresponding to the symbol's IPAs
   * @param diacriticSymbols the UChar set of diacritics
   * @return true if the symbols exists in the alphabet
   */
  virtual bool has_symbol(const std::vector<UChar>& symbols,
                          const std::set<UChar>& diacriticSymbols) {
    return search_symbol(symbols, diacriticSymbols) != "";
  }

  virtual std::vector<std::string>* get_alphabet_set() {
    std::vector<std::string>* vec = new std::vector<std::string>();
    t_alphabetMap::const_iterator iter = alphabetMap.begin();

    while (iter != alphabetMap.end()) {
      (*vec).push_back((*iter).first);
      iter++;
    }

    return vec;
  }

  virtual std::vector<std::string>* get_all_diacritics_set() {
    std::vector<std::string>* vec = new std::vector<std::string>();
    t_diacriticMap::iterator iter = diacriticMap.begin();

    while (iter != diacriticMap.end()) {
      (*vec).push_back((*iter).first);

      iter++;
    }

    return vec;
  }

  // static std::vector<std::string>* get_alphabet_set_static()=0;
  // virtual std::vector<std::string>* get_all_diacritics_set_static()=0;

  // 	  std::string translate_in_alphabet(const std::string &label, const
  // MappedAlphabet& alphabet, bool verifyDiacritics);

  friend std::ostream& operator<<(std::ostream& out,
                                  MappedAlphabet<A>& alphabet) {
    t_alphabetMap::const_iterator iter = (alphabet.alphabetMap).begin();
    t_diacriticMap::const_iterator iter2 =
        ((alphabet.get_diacritic_map())).begin();

    while (iter != alphabet.alphabetMap.end()) {
      out << (*iter).first << ":";
      for (std::vector<UChar>::const_iterator ipa_iter =
               (*iter).second.second.first.begin();
           ipa_iter != (*iter).second.second.first.end(); ++ipa_iter) {
        out << " " << std::hex << (*ipa_iter);
      }
      std::set<UChar> dias = (*iter).second.second.second;
      for (std::set<UChar>::const_iterator diter = dias.begin();
           diter != dias.end(); ++diter) {
        out << ", " << std::hex << (*diter);
      }
      out << std::endl;
      iter++;
    }
    while (iter2 != alphabet.get_diacritic_map().end()) {
      out << (*iter2).first << ": " << std::hex << (*iter2).second;
      out << std::endl;
      iter2++;
    }
    return out;
  }

 protected:
  /**
   * @brief Parse a string containing a sequence of alphabet symbols and return
   *the sequence of recognized symbols
   * @param str String containing alphabet symbols
   * @warning The input string is modified by the method (matched symbols are
   *consumed).
   * @return A vector of string symbols
   **/
  virtual bool rec_extract_symbols(std::vector<std::string>& parsed_symbols,
                                   std::string& str) {
    std::string label, current_symbol;
    bool validated = false;

    // If something to be parsed (otherwise return true)
    if (str != "") {
      // Copy the string and try to comsume
      std::string str_copy;

      int max_symbol_len = get_max_symbol_string_length();

      do {
        str_copy = str;
        current_symbol = "";

        // Parse preceding diacritics
        bool found = true;
        // std::set<UChar> uni_diacritics_set;
        std::string prev_str = str_copy;
        while (found && str_copy != "") {
          label =
              extract_first_diacritic(str_copy, DIACRITIC_POSITION_PRECEDING);
          if (label != "") {
            current_symbol.append(label);
            prev_str = str_copy;
          } else if (str_copy == prev_str) {
            found = false;
          }
        }

        // Parse core symbol + its potential embedded diacritics
        label = extract_first_label(str_copy, max_symbol_len);
        if (label == "") {
          // Parsing is wrong, try another segmentation or stop
          return false;
        } else {
          current_symbol.append(label);

          // Parse extra diacritics
          bool found = true;
          // std::set<UChar> uni_diacritics_set;
          std::string prev_str = str_copy;
          while (found && str_copy != "") {
            label =
                extract_first_diacritic(str_copy, DIACRITIC_POSITION_FOLLOWING);
            if (label != "") {
              current_symbol.append(label);
              prev_str = str_copy;
            } else if (str_copy == prev_str) {
              found = false;
            }
          }
        }
        validated = rec_extract_symbols(parsed_symbols, str_copy);

      } while (
          !validated);  // Do not loop, unless the end of the parsing failed

      // If parsing suceeded
      // Then validate the symbol consumption and push back the recognized
      // symbol
      str = str_copy;
      parsed_symbols.push_back(current_symbol);
    }

    return true;
  }

 public:
  /**
   * @brief Parse a string containing a sequence of alphabet symbols and return
   *the sequence of recognized symbols
   * @param str String containing alphabet symbols
   * @warning The input string is modified by the method (matched symbols are
   *consumed).
   * @return A vector of string symbols
   **/
  virtual std::vector<std::string> extract_symbols(std::string& str) {
    std::vector<std::string> symbols;
    if (!rec_extract_symbols(symbols, str)) {
      std::stringstream ss;
      ss << "impossible to parse the string <";
      ss << str << "> for alphabet <" << get_name() << "> !" << std::endl;
      IpaException e(__FILE__, __LINE__, ss.str());
      throw(e);
    }

    std::reverse(symbols.begin(), symbols.end());

    return symbols;
  }

 protected:
  /**
   * @brief Parse a string containing a sequence of alphabet symbols and return
   *the sequence of corresponding IPAs
   * @param str String containing one alphabet symbol
   * @return The corresponding IPAs
   **/
  virtual std::vector<roots::phonology::ipa::Ipa*>* sub_extract_ipas(
      std::string str) {
    std::vector<roots::phonology::ipa::Ipa*>* ipas =
        new std::vector<roots::phonology::ipa::Ipa*>();
    std::string label;
    std::set<UChar> preceding_diacs;

    // Parse preceding diacritics
    bool found = true;
    // std::set<UChar> uni_diacritics_set;
    std::string prev_str = str;
    while (found && str != "") {
      label = extract_first_diacritic(str, DIACRITIC_POSITION_PRECEDING);
      if (label != "") {
        roots::phonology::ipa::Ipa tmp =
            roots::phonology::ipa::Ipa::unicode_to_ipa(get_diacritic(label));
        const std::set<UChar>& vec = tmp.get_unicode_diacritics_set();
        preceding_diacs.insert(vec.begin(), vec.end());
        prev_str = str;
      } else if (str == prev_str) {
        found = false;
      }
    }

    // Parse core symbol + its potential embedded diacritics
    label = extract_first_label(str);
    if (label == "") {
      std::stringstream ss;
      ss << "unknown character sequence in string <";
      ss << str << "> for alphabet <" << get_name() << "> !" << std::endl;
      IpaException e(__FILE__, __LINE__, ss.str());
      throw(e);
    } else {
      // Get all IPA unicode
      std::vector<UChar> syms = get_symbols(label);
      std::set<UChar> diacs = get_diacritic_symbols(label);
      std::set<UChar> empty_vec;

      for (std::vector<UChar>::const_iterator it = syms.begin();
           it != syms.end(); ++it) {
        ipas->push_back(new roots::phonology::ipa::Ipa(
            roots::phonology::ipa::Ipa::from_unicode(*it, empty_vec)));
      }

      // Parse extra diacritics
      bool found = true;
      // std::set<UChar> uni_diacritics_set;
      std::string prev_str = str;
      while (found && str != "") {
        label = extract_first_diacritic(str, DIACRITIC_POSITION_FOLLOWING);
        if (label != "") {
          roots::phonology::ipa::Ipa tmp =
              roots::phonology::ipa::Ipa::unicode_to_ipa(get_diacritic(label));
          const std::set<UChar>& vec = tmp.get_unicode_diacritics_set();
          diacs.insert(vec.begin(), vec.end());
          prev_str = str;
        } else if (str == prev_str) {
          found = false;
        }
      }

      if (ipas->size() > 1) {
        // Set all parsed diacritics (embedded + extra) either on the first or
        // on the last IPA
        std::set<UChar> output_preceding_diacs;
        std::set<UChar> output_following_diacs;
        for (std::set<UChar>::iterator it = preceding_diacs.begin();
             it != preceding_diacs.end(); ++it) {
          if (roots::phonology::ipa::Ipa::is_preceding_diacritic_static(*it)) {
            output_preceding_diacs.insert(*it);
          } else {
            output_following_diacs.insert(*it);
          }
        }
        for (std::set<UChar>::iterator it = diacs.begin(); it != diacs.end();
             ++it) {
          if (roots::phonology::ipa::Ipa::is_preceding_diacritic_static(*it)) {
            output_preceding_diacs.insert(*it);
          } else {
            output_following_diacs.insert(*it);
          }
        }

        ipas->front()->set_unicode_diacritics(output_preceding_diacs);
        ipas->back()->set_unicode_diacritics(output_following_diacs);

        // Update the IPAs
        for (std::vector<roots::phonology::ipa::Ipa*>::iterator it =
                 ipas->begin();
             it != ipas->end(); ++it) {
          (*it)->from_unicode();
        }
      } else {
        diacs.insert(preceding_diacs.begin(), preceding_diacs.end());
        ipas->front()->set_unicode_diacritics(diacs);
        ipas->front()->from_unicode();
      }
    }

    return ipas;
  }

 public:
  /**
   * @brief Parse a string containing a sequence of alphabet symbols and return
   *the sequence of corresponding IPAs
   * @param str String containing alphabet symbols
   * @warning The input string is modified by the method (matched symbols are
   *consumed).
   * @return A vector of IPAs extract from the string
   **/
  virtual std::vector<roots::phonology::ipa::Ipa*> extract_ipas(
      std::string& str) {
    std::vector<std::string> symbols = extract_symbols(str);
    std::vector<roots::phonology::ipa::Ipa*> ipas;  // IPAs to be returned
    for (std::vector<std::string>::iterator it = symbols.begin();
         it != symbols.end(); ++it) {
      std::vector<roots::phonology::ipa::Ipa*>* new_ipas =
          sub_extract_ipas(*it);
      ipas.insert(ipas.end(), new_ipas->begin(), new_ipas->end());
      delete new_ipas;
    }
    return ipas;
  }

  /**
   * @brief Parse a string containing a sequence of alphabet symbols and return
   *the sequence of corresponding IPAs
   * @param c_str A C string containing alphabet symbols
   * @return A vector of IPAs extract from the string
   **/
  virtual std::vector<roots::phonology::ipa::Ipa*> extract_ipas(
      const char* c_str) {
    std::string str(c_str);
    return extract_ipas(str);
  }

  bool test_ipa_extraction(std::string& str) {
    try {
      std::vector<roots::phonology::ipa::Ipa*> result = extract_ipas(str);
      for (std::vector<roots::phonology::ipa::Ipa*>::iterator it =
               result.begin();
           it != result.end(); ++it) {
        (*it)->destroy();
      }
    } catch (IpaException e) {
      return false;
    }
    return true;
  }

  /**
   * @brief Try to consume the first IPA symbol starting from the left the input
   *string and return it
   * @param str String from which an IPA should be shifted
   * @param curlen Maximum size of the tokens to be considered
   * @warning Both arguments can be modified by the method.
   * @return An IPA symbol from the current alphabet
   **/
  std::string extract_first_label(std::string& str, int& curlen) const {
    // Strategy: Find the longest one first
    bool found = false;
    std::string candidate;

    // Test the substring with length from curlen (as passed in argument) to 1
    while (curlen >= 1 && !found) {
      candidate = str.substr(0, curlen);
      found = is_label(candidate);
      --curlen;
    }

    if (!found) {
      candidate = "";
    } else {
      str.erase(0, candidate.size());
    }

    return candidate;
  }

  /**
   * @brief Try to consume the first IPA symbol starting from the left the input
   *string and return it
   * @param str String from which an IPA should be shifted
   * @warning The argument can be modified by the method.
   * @return An IPA symbol from the current alphabet
   **/
  std::string extract_first_label(std::string& str) const {
    // Strategy: Find the longest one first
    int curlen = 0;
    int maxLabelLen = 0;
    bool found = false;
    std::string candidate;

    // Max length of label
    t_alphabetMap::const_iterator iter = alphabetMap.begin();
    while (iter != alphabetMap.end()) {
      int curlen = (*iter).first.size();
      if (curlen > maxLabelLen) maxLabelLen = curlen;
      iter++;
    }

    // Test the substring with length from maxLabelLen to 1
    curlen = maxLabelLen;
    while (curlen >= 1 && !found) {
      candidate = str.substr(0, curlen);
      found = is_label(candidate);
      --curlen;
    }

    if (!found) {
      candidate = "";
    } else {
      str.erase(0, candidate.size());
    }

    return candidate;
  }

  /**
   * @brief Try to consume the first diacritic symbol starting from the left of
   *the input string and return it
   * @param str String from which a diacritric should be shifted
   * @param direction Optional, 0 for any diacritic, 1 for preceding diacritics
   *only, 2 for following diacritics only. Default is 0.
   * @return An diacritic symbol from the current alphabet if any encountered,
   *empty string otherwise
   **/
  virtual std::string extract_first_diacritic(
      std::string& str,
      roots::phonology::ipa::DiacriticPosition diac_pos) const {
    // Strategy: Find the longest one first
    int curlen = 0;
    int maxLabelLen = 0;
    bool found = false;
    std::string candidate;

    // Max length of diacritic
    maxLabelLen = get_max_diacritic_string_length();
    // 	t_diacriticMap::const_iterator iter = ((get_diacritic_map())).begin();
    // 	while(iter != get_diacritic_map().end())
    // 	  {
    // 	    int curlen = (*iter).first.size();
    // 	    if(curlen > maxLabelLen) maxLabelLen = curlen;
    // 	    iter++;
    // 	  }

    // Test the substring with length from maxLabelLen to 1
    curlen = maxLabelLen;
    while (curlen >= 1 && !found) {
      candidate = str.substr(0, curlen);
      found = is_diacritic(candidate, diac_pos);
      --curlen;
    }

    if (!found) {
      candidate = "";
    } else {
      str.erase(0, candidate.size());
    }

    return candidate;
  }

  /**
   * @brief Try to convert an IPA (ot 2 IPAs) into the corresponding symbol in
   *the current alphabet.
   * @param ipa sequence of IPA to be converted
   * @param separator Optional, string separator between alphabet symbols.
   *Default is no separator.
   * @return An alphabet symbol if the correspondence is possible, an empty
   *string otherwise.
   **/
  std::string convert_ipas(const std::vector<roots::phonology::ipa::Ipa*>& ipa,
                           const std::string& separator = "") {
    std::string corresp_label = "";
    std::vector<UChar> symbols;

    // ------------
    // Search for a label containing both IPAs
    if (ipa.size() > 0) {
      std::set<UChar> diacs;

      for (std::vector<roots::phonology::ipa::Ipa*>::const_iterator iter =
               ipa.begin();
           iter != ipa.end(); ++iter) {
        symbols.push_back((*iter)->get_unicode());
        diacs.insert((*iter)->get_unicode_diacritics_set().begin(),
                     (*iter)->get_unicode_diacritics_set().end());
      }

      corresp_label = search_symbol(symbols, diacs);
    }

    if (corresp_label == "" && ipa.size() == 1) {
      std::stringstream ss;
      ss << "cannot convert ipa [" << ipa[0]->to_string(0)
         << "] into alphabet [" << get_name() << "]!" << std::endl;
      throw IpaException(__FILE__, __LINE__, ss.str());
    } else {
      // ------------
      // if not found -> search for separate symbols
      if (corresp_label == "" && ipa.size() > 1) {
        corresp_label = "";
        for (std::vector<roots::phonology::ipa::Ipa*>::const_iterator iter =
                 ipa.begin();
             iter != ipa.end(); ++iter) {
          std::vector<roots::phonology::ipa::Ipa*> vec;
          vec.push_back(*iter);
          if (corresp_label != "") {
            corresp_label += separator;
          }
          corresp_label += convert_ipas(vec, separator);
        }
      }
    }

    return corresp_label;
  }

  /**
   * @brief Approximate an IPA (ot 2 IPAs) into the corresponding symbol in the
   *current alphabet.
   * @param ipa sequence of IPA to be converted
   * @param separator Optional, string separator between alphabet symbols in the
   *output string. Default is no separator.
   * @return An alphabet symbol if the correspondence is possible, an empty
   *string otherwise.
   **/
  std::string approximate_ipas(
      const std::vector<roots::phonology::ipa::Ipa*>& ipa,
      const std::string& separator = "") {
    std::string corresp_label = "";
    try {
      corresp_label = convert_ipas(ipa, separator);
    } catch (IpaException e) {
      std::vector<std::string> vec_label;
      std::vector<UChar> symbols;

      if (ipa.size() > 0) {
        std::set<UChar> diacs;

        for (std::vector<roots::phonology::ipa::Ipa*>::const_iterator iter =
                 ipa.begin();
             iter != ipa.end(); ++iter) {
          symbols.push_back((*iter)->get_unicode());
          diacs.insert((*iter)->get_unicode_diacritics_set().begin(),
                       (*iter)->get_unicode_diacritics_set().end());
        }

        vec_label = search_closest_symbols(symbols, diacs);

        if (vec_label.empty()) {
          std::stringstream ss;
          ss << "cannot approximate symbols [" << ipa[0]->to_string(0)
             << "] with any symbol in alphabet [" << get_name() << "]!"
             << std::endl;
          std::cerr << "error: " << ss.str();
          throw IpaException(__FILE__, __LINE__, ss.str());
        } else {
          for (std::vector<std::string>::iterator it = vec_label.begin();
               it != vec_label.end(); ++it) {
            if (corresp_label != "") {
              corresp_label += separator;
            }
            corresp_label += *it;
          }
        }
      }
    }

    return corresp_label;
  }

  /**
   * @brief Try to convert a phoneme into the corresponding symbol in the
   *current alphabet.
   * @param phoneme Phoneme to be converted
   * @return An alphabet symbol if the correspondence is possible, an empty
   *string otherwise.
   **/
  std::string convert_phoneme(const Phoneme& phoneme) {
    std::vector<roots::phonology::ipa::Ipa*> ipa;
    for (int ipaIdx = 0; ipaIdx < phoneme.get_n_ipa(); ++ipaIdx) {
      ipa.push_back(
          const_cast<roots::phonology::ipa::Ipa*>(&phoneme.get_ipa(ipaIdx)));
    }
    return convert_ipas(ipa);
  }

  /**
   * @brief Approximate a phoneme into the most accurate symbol in the current
   *alphabet.
   * @param phoneme Phoneme to be approximated
   * @return An alphabet symbol.
   **/
  std::string approximate_phoneme(const Phoneme& phoneme) {
    std::vector<roots::phonology::ipa::Ipa*> ipa;
    for (int ipaIdx = 0; ipaIdx < phoneme.get_n_ipa(); ++ipaIdx) {
      ipa.push_back(
          const_cast<roots::phonology::ipa::Ipa*>(&phoneme.get_ipa(ipaIdx)));
    }
    return approximate_ipas(ipa);
  }

  /**
   * @brief List all correspondences between symbols of the current alphabet and
   * IPA symbols.
   * @param level Level of precision. Default is 0.
   * @return A list of string rows, one per symbol.
   */
  virtual std::string to_string(int level = 0) {
    std::stringstream ss;
    ss << get_name() << std::endl;
    ss << "--------------------" << std::endl;
    for (std::map<std::string, t_alphabet_elt>::iterator it =
             alphabetMap.begin();
         it != alphabetMap.end(); ++it) {
      ss << it->first << "\t\t";
      std::string str = it->first;
      std::vector<roots::phonology::ipa::Ipa*> ipas = extract_ipas(str);
      size_t n = ipas.size();
      for (size_t i = 0; i < n; i++) {
        if (level == 0) {
          ss << ipas[i]->to_string(level);
          if (i < n - 1) {
            ss << " + ";
          }
        } else {
          if (i > 0) {
            int m = it->first.length();
            for (int j = 0; j < m; j++) {
              ss << " ";
            }
            ss << "\t\t+ ";
          } else {
            ss << "  ";
          }
          ss << ipas[i]->to_string(level);
          if (i < n - 1) {
            ss << std::endl;
          }
        }
        ipas[i]->destroy();
      }
      ss << std::endl;
    }
    return ss.str();
  }

 protected:
  int get_max_symbol_string_length() const {
    if (max_symbol_string_length == -1) {
      int maxLabelLen = 0;
      t_alphabetMap::const_iterator iter = alphabetMap.begin();
      while (iter != alphabetMap.end()) {
        int curlen = (*iter).first.size();
        if (curlen > maxLabelLen) maxLabelLen = curlen;
        iter++;
      }
      max_symbol_string_length = maxLabelLen;
    }
    return max_symbol_string_length;
  }

  int get_max_diacritic_string_length() const {
    if (max_diacritic_string_length == -1) {
      int maxLabelLen = 0;
      t_diacriticMap::const_iterator iter = diacriticMap.begin();
      while (iter != diacriticMap.end()) {
        int curlen = (*iter).first.size();
        if (curlen > maxLabelLen) maxLabelLen = curlen;
        iter++;
      }
      max_diacritic_string_length = maxLabelLen;
    }
    return max_diacritic_string_length;
  }

  std::string search_symbol(const std::vector<UChar>& symbols,
                            const std::set<UChar>& diacs) {
    std::string corresp_label = "";

    // If reversed map is not consistent with the alphabet map
    // (re-)build it
    if (alphabetMapChanged) {
      build_reversed_alphabet_map();
      alphabetMapChanged = false;
    }
    if (diacriticMap.size() != reversedDiacriticMap.size()) {
      build_reversed_diacritic_map();
    }

    // Eliminate standalone diacritics
    std::set<UChar> standaloneDiacs;
    std::set<UChar> survivingDiacs;

    for (std::set<UChar>::iterator iter = diacs.begin(); iter != diacs.end();
         ++iter) {
      if (reversedDiacriticMap.find(*iter) != reversedDiacriticMap.end()) {
        standaloneDiacs.insert(*iter);
      } else {
        survivingDiacs.insert(*iter);
      }
    }

    // Search the symbols with surviving diacritics
    t_alphabet_elt key = t_alphabet_elt(
        true, t_alphabet_ipas(std::vector<UChar>(), std::set<UChar>()));
    key.second.first = symbols;
    key.second.second.insert(survivingDiacs.begin(), survivingDiacs.end());

    // Search in the map
    t_reversed_alphabetMap::iterator search = reversedAlphabetMap.find(key);
    if (search != reversedAlphabetMap.end()) {
      corresp_label = search->second;
    }

    // If no symbol, try iteratively the diacritics to find one matching symbol
    // with diacritic
    if (corresp_label != "") {
      // Convert diacritics to string
      for (std::set<UChar>::iterator iter = standaloneDiacs.begin();
           iter != standaloneDiacs.end(); ++iter) {
        corresp_label += reversedDiacriticMap[*iter];
      }
    }

    return corresp_label;
  }

  /**
   * @brief Find the most approaching alphabet symbol for a given input sequence
   *of description of IPAs.
   * @param symbols List of unicode IPA symbols to be approached
   * @param diacs List of unicode IPA diacritics attached to the IPA symbols
   * @return A list of alphabet symbols if an approximation is possible, an
   *empty vector otherwise.
   **/
  std::vector<std::string> search_closest_symbols(
      const std::vector<UChar>& symbols, const std::set<UChar>& diacs) {
    float min_distance = 1e100;
    std::vector<std::string> closest_symbols;
    size_t closest_symbols_length = 1000;

    float current_distance = 0;
    std::vector<std::string> current_matching;

    // Build ALL combinations of 1+ symbols + 0+ diacritics
    // NOTE: Change to something more efficient if you know how
    std::vector<std::vector<t_alphabet_ipas> > all_combinations;

    // 	          print_original(symbols, diacs);

    // Build all combinations of symbols
    for (std::vector<UChar>::const_iterator it = symbols.begin();
         it != symbols.end(); ++it) {
      all_combinations = add_combinations(all_combinations, *it);
    }

    // 	    std::cerr << "--" << std::endl;
    // 	    for (std::vector< std::vector< t_alphabet_elt > >::iterator it =
    // all_combinations.begin();
    // 		 it != all_combinations.end();
    // 		 ++it) { print_combination(*it); }
    // 	    std::cerr << "--" << std::endl;

    // Integrate all possible combinations of diacritics
    // -> If N symbols, then compute all partitionnings up to partitions of N
    // subsets.
    for (std::set<UChar>::const_iterator it = diacs.begin(); it != diacs.end();
         ++it) {
      all_combinations = dispatch_diacritics(all_combinations, *it);
    }

    // Sort combinations by descending number of tuples
    std::sort(all_combinations.begin(), all_combinations.end());

    // 	    std::cerr << "--" << std::endl;
    // 	    for (int i = 0; i < all_combinations.size(); i++) {
    // 	      print_combination(all_combinations.at(i));
    // 	    }
    // 	    for (std::vector< std::vector< t_alphabet_elt > >::iterator it =
    // all_combinations.begin(); it != all_combinations.end(); ++it) {
    // 	      print_combination(*it);
    // 	    }
    // 	    std::cerr << "--" << std::endl;

    // Browse all combinations and find the one with the least distance
    for (std::vector<std::vector<t_alphabet_ipas> >::reverse_iterator it =
             all_combinations.rbegin();
         it != all_combinations.rend(); ++it) {
      current_matching.clear();
      // 	    std::cerr << "==" << std::endl;
      // 	      print_combination(*it);
      current_distance = compute_combination_distance(*it, current_matching);
      std::string current_matching_str = "";
      std::string closest_symbols_str = "";
      for (std::vector<std::string>::iterator jt = current_matching.begin();
           jt != current_matching.end(); ++jt) {
        current_matching_str += *jt;
      }
      for (std::vector<std::string>::iterator jt = closest_symbols.begin();
           jt != closest_symbols.end(); ++jt) {
        closest_symbols_str += *jt;
      }
      if ((current_distance < min_distance) ||
          (current_distance == min_distance &&
           it->size() < closest_symbols_length)) {
        // 		std::cerr << string_format("search_closest_symbols: (%s,
        // %i, %f)\treplaces\t(%s, %i, %f)", current_matching_str.c_str(),
        // it->size(), current_distance, closest_symbols_str.c_str(),
        // closest_symbols_length, min_distance) << std::endl;
        min_distance = current_distance;
        closest_symbols = current_matching;
        closest_symbols_length = it->size();
      } else {
        // 		std::cerr << string_format("search_closest_symbols: (%s,
        // %i, %f)\tworse than\t(%s, %i, %f)", current_matching_str.c_str(),
        // it->size(), current_distance, closest_symbols_str.c_str(),
        // closest_symbols_length, min_distance) << std::endl;
      }
      if (min_distance == 0) {
        break;
      }
    }

    return closest_symbols;
  }

  /**
   * @brief Suffix existing combinations of symbols with a new input symbol
   * @param all_sym_comb List of symbol combinations
   * @param new_sym New symbol to be suffixed
   * @return List of symbol combinations after including the new symbol
   **/
  std::vector<std::vector<t_alphabet_ipas> > add_combinations(
      const std::vector<std::vector<t_alphabet_ipas> >& all_sym_comb,
      const UChar& new_sym) {
    std::vector<std::vector<t_alphabet_ipas> > result;
    const int SYMBOL_GROUP_SIZE_LIMIT = 2;
    if (all_sym_comb.empty()) {
      // {} -> {[(a)]}
      std::vector<UChar> ttt;
      ttt.push_back(new_sym);
      t_alphabet_ipas single_sym = t_alphabet_ipas(ttt, std::set<UChar>());
      // t_alphabet_ipas single_sym = t_alphabet_ipas
      // (boost::assign::list_of(new_sym), std::set<UChar>());
      std::vector<t_alphabet_ipas> single_vec;
      single_vec.push_back(single_sym);
      result.push_back(single_vec);
    } else {
      // {[(a),(b)]} -> {[(a),(b),(c)],[(a),(b,c)]}
      for (std::vector<std::vector<t_alphabet_ipas> >::const_iterator it =
               all_sym_comb.begin();
           it != all_sym_comb.end(); ++it) {
        // [(a),(b)] -> [(a),(b),(c)]
        result.push_back(*it);

        std::vector<UChar> ttt;
        ttt.push_back(new_sym);
        result.back().push_back(t_alphabet_ipas(ttt, std::set<UChar>()));

        // result.back().push_back(t_alphabet_ipas
        // (boost::assign::list_of(new_sym), std::set<UChar>()));

        // [(a),(b)] -> [(a),(b,c)] unless (b,c) is too long
        if ((*it).back().first.size() < SYMBOL_GROUP_SIZE_LIMIT) {
          // 		  print_combination(result.back());
          result.push_back(*it);
          result.back().back().first.push_back(new_sym);
        }
      }
    }

    // Exceptions where manual combinations have to be inserted
    std::vector<std::vector<t_alphabet_ipas> > exception_result;
    switch (new_sym) {
      case 0x0272:  // latin n with left hook
        exception_result = add_combinations(all_sym_comb, 0x006E);  // latin n
        exception_result =
            add_combinations(exception_result, 0x006A);  // latin j
        result.insert(result.end(), exception_result.begin(),
                      exception_result.end());
        break;
      case 0x014B:                                                  // latin eng
        exception_result = add_combinations(all_sym_comb, 0x006E);  // latin n
        exception_result =
            add_combinations(exception_result, 0x0261);  // latin script g
        result.insert(result.end(), exception_result.begin(),
                      exception_result.end());
        break;
      default:
        break;
    }
    return result;
  }

  /**
   * @brief Dispatch an input over the input combinations of symbols
   * @param all_comb List of symbol combinations
   * @param new_diac Diactritic to be dispatched
   * @return The combination of symbols after including the input diacritic
   **/
  std::vector<std::vector<t_alphabet_ipas> > dispatch_diacritics(
      const std::vector<std::vector<t_alphabet_ipas> >& all_comb,
      const UChar& new_diac) {
    std::vector<std::vector<t_alphabet_ipas> > result;
    // {[(a),(b)],[(a,b)]} + 1 -> {[(a,1),(b)],[(a),(b,1)],[(a,b,1)]}
    for (std::vector<std::vector<t_alphabet_ipas> >::const_iterator it =
             all_comb.begin();
         it != all_comb.end(); ++it) {
      int n = it->size();
      for (int i = 0; i < n; i++) {
        // Copy
        result.push_back(*it);
        // Dispatch the diactric to the i-th sublist
        result.back().at(i).second.insert(new_diac);
      }
      // Case where the diacritic is separate from symbols
      result.push_back(*it);
      result.back().push_back(
          t_alphabet_ipas(std::vector<UChar>(), std::set<UChar>()));
      result.back().back().second.insert(new_diac);
    }
    return result;
  }

  /**
   * @brief Find the most approaching list of alphabet symbol that can match a
   *given input list of IPA (expressed as unicode alphabet elements)
   * @param combination A list of alphabet elements to be approached
   * @param best_matching Output parameter, list of string symbols which best
   *approach the input
   * @return Distance of the most approaching solution.
   **/
  float compute_combination_distance(std::vector<t_alphabet_ipas> combination,
                                     std::vector<std::string>& best_matching) {
    float global_distance = 0.0;
    // Take all elements of a combination
    // and look for the the least distance equivalent in the alphabet for each
    for (std::vector<t_alphabet_ipas>::iterator it_target = combination.begin();
         it_target != combination.end(); ++it_target) {
      float min_distance = 1e100;
      std::string best_symbol = "";
      // 	      //separate prefix and suffix diacritics
      // 	      std::set<UChar> prefix_diacs_target;
      // 	      std::set<UChar> suffix_diacs_target;
      // 	      for (std::set<UChar>::iterator it_diacs_target =
      // it_target->second.begin(); it_diacs_target != it_target->second.end();
      // ++it_diacs_target) {
      // 		if (Ipa::is_preceding_diacritic_static(*it_diacs_target))
      // {
      // 		  prefix_diacs_target.insert(*it_diacs_target);
      // 		}
      // 		else {
      // 		  suffix_diacs_target.insert(*it_diacs_target);
      // 		}
      // 	      }

      roots::phonology::ipa::Ipa ipa_target;
      roots::phonology::ipa::Ipa ipa_alphabet;
      bool is_a_diacritic = false;
      // If it_target contains at least 1 symbol
      if (it_target->first.size() > 0) {
        // Browse all alphabet elements
        for (t_alphabetMap::iterator it_alphabet = alphabetMap.begin();
             it_alphabet != alphabetMap.end(); ++it_alphabet) {
          size_t n =
              it_alphabet->second.second.first
                  .size();  // Number of symbols in the current alphabet element
          // 		  //separate prefix and suffix diacritics
          // 		  std::set<UChar> prefix_diacs_alphabet;
          // 		  std::set<UChar> suffix_diacs_alphabet;
          //
          // 		  for (std::set<UChar>::iterator it_diacs_alpha =
          // it_alphabet->second.second.second.begin(); it_diacs_alpha !=
          // it_alphabet->second.second.second.end(); ++it_diacs_alpha) {
          // 		    if (Ipa::is_preceding_diacritic_static(*it_diacs_alpha))
          // {
          // 		      prefix_diacs_alphabet.insert(*it_diacs_alpha);
          // 		    }
          // 		    else {
          // 		      suffix_diacs_alphabet.insert(*it_diacs_alpha);
          // 		    }
          // 		  }

          // Only consider the alphabet element if the alphabet element and
          // combination have the same number of symbols
          if (it_target->first.size() == n) {
            for (size_t diacritic_position = 0; diacritic_position < n;
                 diacritic_position++) {
              float current_distance = 0;
              for (size_t i = 0; i < n; i++) {
                // if (i == 0) { // Put prefix diacritics in the last IPA (does
                // not change anything)
                // std::cerr << "First" << std::endl;
                // ipa_target =
                // roots::phonology::ipa::Ipa::from_unicode(it_target->first.at(i),
                // prefix_diacs_target);
                // ipa_alphabet =
                // roots::phonology::ipa::Ipa::from_unicode(it_alphabet->second.second.first.at(i),
                // prefix_diacs_alphabet);
                //}
                // if (i == n-1) { // Put suffix diacritics in the last IPA
                // (does not change anything)
                // std::cerr << "Last" << std::endl;
                // ipa_target =
                // roots::phonology::ipa::Ipa::from_unicode(it_target->first.at(i),
                // suffix_diacs_target);
                // ipa_alphabet =
                // roots::phonology::ipa::Ipa::from_unicode(it_alphabet->second.second.first.at(i),
                // suffix_diacs_alphabet);
                //}
                if (i == diacritic_position) {  // Put suffix diacritics in the
                                                // last IPA (does not change
                                                // anything)
                  ipa_target = roots::phonology::ipa::Ipa::from_unicode(
                      it_target->first.at(i), it_target->second);
                  ipa_alphabet = roots::phonology::ipa::Ipa::from_unicode(
                      it_alphabet->second.second.first.at(i),
                      it_alphabet->second.second.second);
                } else {
                  ipa_target = roots::phonology::ipa::Ipa::from_unicode(
                      it_target->first.at(i), std::set<UChar>());
                  ipa_alphabet = roots::phonology::ipa::Ipa::from_unicode(
                      it_alphabet->second.second.first.at(i),
                      std::set<UChar>());
                }
                // std::cerr << "ipa_target = " << ipa_target.to_string() <<
                // std::endl;
                // std::err << "ipa_alphabet = " << ipa_alphabet.to_string() <<
                // std::endl;
                // std::cerr << "-> " << current_distance << " + \t";
                current_distance +=
                    ipa_target.compute_dissimilarity(&ipa_alphabet);
                // std::cerr << ipa_target.compute_dissimilarity(&ipa_alphabet)
                // << "\t = \t" << current_distance << std::endl;
              }
              if (current_distance < min_distance) {
                min_distance = current_distance;
                // 			std::cerr << "-> Min distance = " <<
                // min_distance << std::endl << std::endl;
                best_symbol = it_alphabet->first;
              }
            }
          }
        }
      }
      // Else simply compare diacritics from the diacritic table
      else {
        for (t_diacriticMap::iterator it_diacritic = diacriticMap.begin();
             it_diacritic != diacriticMap.end(); ++it_diacritic) {
          float current_distance = 0;

          current_distance += (it_target->second.find(it_diacritic->second) ==
                                       it_target->second.end()
                                   ? 1
                                   : 0);
          if (current_distance < min_distance) {
            min_distance = current_distance;
            best_symbol = it_diacritic->first;
          }
        }
        is_a_diacritic = true;
      }
      global_distance += min_distance;
      if (is_a_diacritic && !best_matching.empty()) {
        best_matching.back().append(best_symbol);
      } else {
        best_matching.push_back(best_symbol);
      }
    }
    return global_distance;
  }

  bool is_shorter_alphabet_symbol_sequence(
      const std::vector<t_alphabet_ipas>& comb1,
      const std::vector<t_alphabet_ipas>& comb2) {
    return (comb1.size() < comb2.size());
  }

  void print_combination(std::vector<t_alphabet_ipas> comb) {
    std::cerr << "[ s+d = ";
    size_t n = comb.size();
    for (size_t i = 0; i < n; i++) {
      for (std::vector<UChar>::iterator it_s = comb[i].first.begin();
           it_s != comb[i].first.end(); ++it_s) {
        std::cerr << *it_s << " ";
      }
      for (std::set<UChar>::iterator it_d = comb[i].second.begin();
           it_d != comb[i].second.end(); ++it_d) {
        std::cerr << *it_d << " ";
      }
      if (i < n - 1) {
        std::cerr << ", ";
      }
    }
    std::cerr << "]";
    std::cerr << std::endl;
  }

  void print_original(const std::vector<UChar>& symbols,
                      const std::set<UChar>& diacs) {
    std::cerr << "print_original: ";
    for (size_t i = 0; i < symbols.size(); i++) {
      std::cerr << symbols[i] << " ";
    }
    for (std::set<UChar>::iterator it = diacs.begin(); it != diacs.end();
         ++it) {
      std::cerr << *it << " ";
    }
    std::cerr << std::endl;
  }

  void build_reversed_alphabet_map() {
    // Clear previous map if already built previously
    reversedAlphabetMap.clear();
    for (t_alphabetMap::const_iterator iter = alphabetMap.begin();
         iter != alphabetMap.end(); ++iter) {
      reversedAlphabetMap[(*iter).second] = (*iter).first;
    }
  }

  void build_reversed_diacritic_map() {
    // Clear previous map if already built previously
    reversedDiacriticMap.clear();
    for (t_diacriticMap::const_iterator iter = get_diacritic_map().begin();
         iter != get_diacritic_map().end(); ++iter) {
      reversedDiacriticMap[(*iter).second] = (*iter).first;
    }
  }

  bool alphabetMapChanged;

  /**
   * @brief Map associating alphabet's symbol strings to IPA unicode values
   **/
  t_alphabetMap alphabetMap;
  /**
   * @brief Map associating IPA unicode values to alphabet's symbol strings.
   * @note The construction of this map is delayed until the first call to
   *conversion methods.
   **/
  t_reversed_alphabetMap reversedAlphabetMap;
  /**
   * @brief Map associating diacritic's symbol strings to IPA unicode values
   **/
  t_diacriticMap diacriticMap;

  /**
   * @brief Map associating IPA unicode values to diacritic's symbol strings.
   * @note The construction of this map is delayed until the first call to
   *conversion methods.
   **/
  t_reversed_diacriticMap reversedDiacriticMap;
};

/*
  template <class A> bool MappedAlphabet<A>::alphabetMapChanged = false;
  template <class A> t_alphabetMap MappedAlphabet<A>::alphabetMap =
  t_alphabetMap();
  template <class A> t_reversed_alphabetMap
  MappedAlphabet<A>::reversedAlphabetMap = t_reversed_alphabetMap();
  template <class A> t_diacriticMap MappedAlphabet<A>::diacriticMap =
  t_diacriticMap();
  template <class A> t_reversed_diacriticMap
  MappedAlphabet<A>::reversedDiacriticMap = t_reversed_diacriticMap();
*/
}
}
}

#endif /* MAPPED_ALPHABET_H_ */
