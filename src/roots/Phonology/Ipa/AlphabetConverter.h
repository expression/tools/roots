/*
    This file is part of Roots.

    Roots is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Roots is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Roots.  If not, see <http://www.gnu.org/licenses/>.

    Copyright (c) 2013, 2014 IRISA
*/


#ifndef IPA_ALPHABET_CONVERTER_H_
#define IPA_ALPHABET_CONVERTER_H_


#include "../../common.h"
#include "../../Base.h"
#include "AlphabetException.h"

#include <boost/assign/list_of.hpp> // for 'map_list_of()'
#include <boost/assign/list_inserter.hpp>

namespace roots { 
	namespace phonology { 
		namespace ipa {

			template <class S, class T>
			class AlphabetConverter: public roots::Base
			{
			protected:
				AlphabetConverter(){}
				virtual ~AlphabetConverter(){}
			private:
				AlphabetConverter& operator=(const AlphabetConverter<S,T>&){}

			public:
				std::map<std::string, std::vector<std::string> >& get_conversion_map() { return conversionMap; }

				std::vector<std::string>& translate_label( const std::string& label)
				{
					try
					{
						return conversionMap.at(label);
					}catch(std::out_of_range e)
					 {
						 throw AlphabetException(__FILE__, __LINE__, "translate_label: label not found in conversion map!\n");
					 }
				}

				S& get_source_alphabet() const { return S::get_instance(); }
				T& get_target_alphabet() const { return T::get_instance(); }

			protected:
				std::map<std::string, std::vector<std::string> > conversionMap;

			};

		} 
	}
}

#endif /* IPA_ALPHABET_CONVERTER_H_ */
