/*
    This file is part of Roots.

    Roots is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Roots is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Roots.  If not, see <http://www.gnu.org/licenses/>.

    Copyright (c) 2013, 2014 IRISA
*/


#include "PinyinAlphabet.h"

namespace roots { namespace phonology { namespace ipa {


PinyinAlphabet& PinyinAlphabet::get_instance()
{
	static PinyinAlphabet instance;
	return instance;
}

PinyinAlphabet* PinyinAlphabet::get_instance_ptr()
{
	return &static_cast<PinyinAlphabet&>(get_instance());
}

PinyinAlphabet::PinyinAlphabet() : MappedAlphabet("pinyin")
{
	
	// Consonants
	alphabetMap["b"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x0070),
																													 std::set<UChar>()));
	alphabetMap["p"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x0070),
																													 boost::assign::list_of(0x02B0))); //diacritic
	alphabetMap["m"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x006D),
																													 std::set<UChar>()));
	alphabetMap["f"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x0066),
																													 std::set<UChar>()));
	alphabetMap["d"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x0074),
																													 std::set<UChar>()));
	alphabetMap["t"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x0074),
																													 boost::assign::list_of(0x02B0))); //diacritic
	alphabetMap["n"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x006E),
																													 std::set<UChar>()));
	alphabetMap["l"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x006C),
																													 std::set<UChar>()));
	alphabetMap["g"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x006B),
																													 std::set<UChar>()));
	alphabetMap["k"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x006B),
																													 boost::assign::list_of(0x02B0))); //diacritic
	alphabetMap["h"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x0078),
																													 std::set<UChar>()));
	alphabetMap["j"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x0074)(0x0255),
																													 std::set<UChar>())); //double IPA
	alphabetMap["q"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x0074)(0x0255),
																													 boost::assign::list_of(0x02B0))); //double IPA with diacritic
	alphabetMap["x"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x0255),
																													 std::set<UChar>()));
	alphabetMap["zh"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x0074)(0x0282),
																														std::set<UChar>())); //double IPA
	alphabetMap["ch"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x0074)(0x0282),
																														boost::assign::list_of(0x02B0))); //double IPA with diacritic
	alphabetMap["sh"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x0282),
																														std::set<UChar>()));
	alphabetMap["r"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x027B),
																													 std::set<UChar>()));
	alphabetMap["z"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x0074)(0x0073),
																													 std::set<UChar>())); //double IPA
	alphabetMap["c"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x0074)(0x0073),
																													 boost::assign::list_of(0x02B0))); //double IPA with diacritic
	alphabetMap["s"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x0073),
																													 std::set<UChar>()));
	alphabetMap["w"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x0077),
																													 std::set<UChar>()));
	alphabetMap["y"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x006A),
																													 std::set<UChar>()));
	alphabetMap["yu"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x0265),
																														std::set<UChar>()));
	
	//Vowels	
	alphabetMap["a"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x0061),
																													 boost::assign::list_of(0x0308)));
	alphabetMap["ā"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x0061),
																													 boost::assign::list_of(0x0308)(0x0304)));
	alphabetMap["á"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x0061),
																													 boost::assign::list_of(0x0308)(0x0301)));
	alphabetMap["ǎ"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x0061),
																													 boost::assign::list_of(0x0308)(0x030C)));
	alphabetMap["à"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x0061),
																													 boost::assign::list_of(0x0308)(0x0300)));	
					    			    				    
	alphabetMap["i"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x0069),
																													 std::set<UChar>()));		
	alphabetMap["ī"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x0069),
																													 boost::assign::list_of(0x0304)));	
	alphabetMap["í"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x0069),
																													 boost::assign::list_of(0x0301)));	
	alphabetMap["ǐ"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x0069),
																													 boost::assign::list_of(0x030C)));
	alphabetMap["ì"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x0069),
																													 boost::assign::list_of(0x0300)));
					    				    					    					    					  	    
	alphabetMap["u"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x0075),
																													 std::set<UChar>())); 
	alphabetMap["ū"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x0075),
																													 boost::assign::list_of(0x0304)));		    					    				    					    					    					  	    
	alphabetMap["ú"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x0075),
																													 boost::assign::list_of(0x0301)));			    					    				    					    					    					  	    
	alphabetMap["ǔ"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x0075),
																													 boost::assign::list_of(0x030C)));			    					    				    					    					    					  	    
	alphabetMap["ù"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x0075),
																													 boost::assign::list_of(0x0300)));		    
			    
	alphabetMap["e"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x0259),
																													 std::set<UChar>()));					    
	alphabetMap["ē"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x0259),
																													 boost::assign::list_of(0x0304)));			    			    
	alphabetMap["é"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x0259),
																													 boost::assign::list_of(0x0301)));			    
	alphabetMap["ě"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x0259),
																													 boost::assign::list_of(0x030C)));			    			    
	alphabetMap["è"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x0259),
																													 boost::assign::list_of(0x0300)));		    
		    
	alphabetMap["o"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x0254),
																													 std::set<UChar>()));				    
	alphabetMap["ō"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x0254),
																													 boost::assign::list_of(0x0304)));					    	    
	alphabetMap["ó"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x0254),
																													 boost::assign::list_of(0x0301)));						    	    
	alphabetMap["ǒ"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x0254),
																													 boost::assign::list_of(0x030C)));						    	    
	alphabetMap["ò"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x0254),
																													 boost::assign::list_of(0x0300)));		
					        
	alphabetMap["ü"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x0079),
																													 std::set<UChar>()));	
	alphabetMap["ǖ"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x0079),
																													 boost::assign::list_of(0x0304)));	
	alphabetMap["ǘ"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x0079),
																													 boost::assign::list_of(0x0301)));	
	alphabetMap["ǚ"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x0079),
																													 boost::assign::list_of(0x030C)));	
	alphabetMap["ǜ"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x0079),
																													 boost::assign::list_of(0x0300)));
					    
	//Diphthongs		    
	alphabetMap["ai"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x0061)(0x026A),
																														std::set<UChar>()));					    
	alphabetMap["āi"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x0061)(0x026A),
																														boost::assign::list_of(0x0304)));			    
	alphabetMap["ái"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x0061)(0x026A),
																														boost::assign::list_of(0x0301)));				    
	alphabetMap["ǎi"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x0061)(0x026A),
																														boost::assign::list_of(0x030C)));				    
	alphabetMap["ài"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x0061)(0x026A),
																														boost::assign::list_of(0x0300)));		
					    	    
	alphabetMap["ao"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x0251)(0x028A),
																														std::set<UChar>()));			    
	alphabetMap["āo"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x0251)(0x028A),
																														boost::assign::list_of(0x0304)));		    
	alphabetMap["áo"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x0251)(0x028A),
																														boost::assign::list_of(0x0301)));		    
	alphabetMap["ǎo"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x0251)(0x028A),
																														boost::assign::list_of(0x030C)));			    
	alphabetMap["ào"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x0251)(0x028A),
																														boost::assign::list_of(0x0300)));			    
	
	alphabetMap["ei"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x0065)(0x026A),
																														std::set<UChar>()));			    
	alphabetMap["ēi"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x0065)(0x026A),
																														boost::assign::list_of(0x0304)));		    
	alphabetMap["éi"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x0065)(0x026A),
																														boost::assign::list_of(0x0301)));			    
	alphabetMap["ěi"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x0065)(0x026A),
																														boost::assign::list_of(0x030C)));			    
	alphabetMap["èi"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x0065)(0x026A),
																														boost::assign::list_of(0x0300)));		    
	
	alphabetMap["ou"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x006F)(0x028A),
																														std::set<UChar>()));			    
	alphabetMap["ōu"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x006F)(0x028A),
																														boost::assign::list_of(0x0304)));		    
	alphabetMap["óu"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x006F)(0x028A),
																														boost::assign::list_of(0x0301)));		    
	alphabetMap["ǒu"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x006F)(0x028A),
																														boost::assign::list_of(0x030C)));			    
	alphabetMap["òu"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x006F)(0x028A),
																														boost::assign::list_of(0x0300)));		    
	
	// Nasal vowels   				    
	
	alphabetMap["ang"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x0251)(0x014B),
																														 std::set<UChar>()));			    
	alphabetMap["āng"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x0251)(0x014B),
																														 boost::assign::list_of(0x0304)));		    
	alphabetMap["áng"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x0251)(0x014B),
																														 boost::assign::list_of(0x0301)));		    
	alphabetMap["ǎng"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x0251)(0x014B),
																														 boost::assign::list_of(0x030C)));		    
	alphabetMap["àng"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x0251)(0x014B),
																														 boost::assign::list_of(0x0300)));		    
	
	alphabetMap["eng"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x0264)(0x014B),
																														 std::set<UChar>()));			    
	alphabetMap["ēng"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x0264)(0x014B),
																														 boost::assign::list_of(0x0304)));		    
	alphabetMap["éng"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x0264)(0x014B),
																														 boost::assign::list_of(0x0301)));		    
	alphabetMap["ěng"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x0264)(0x014B),
																														 boost::assign::list_of(0x030C)));		    
	alphabetMap["èng"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x0264)(0x014B),
																														 boost::assign::list_of(0x0300)));		    
	
	alphabetMap["ong"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x028A)(0x014B),
																														 std::set<UChar>()));			    
	alphabetMap["ōng"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x028A)(0x014B),
																														 boost::assign::list_of(0x0304)));			    
	alphabetMap["óng"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x028A)(0x014B),
																														 boost::assign::list_of(0x0301)));			    
	alphabetMap["ǒng"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x028A)(0x014B),
																														 boost::assign::list_of(0x030C)));			    
	alphabetMap["òng"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x028A)(0x014B),
																														 boost::assign::list_of(0x0300)));		    
	
	alphabetMap["ng"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x014B),
																														std::set<UChar>()));			    			    


	//lowercase version of symbols since Arpabet is not case sensitive
    for (std::map<std::string,t_alphabet_elt>::const_iterator it = alphabetMap.begin();
	     it != alphabetMap.end();
	     ++it) {
	      std::string new_key = it->first;
	      std::transform(new_key.begin(), new_key.end(), new_key.begin(), ::toupper);
	      alphabetMap[new_key] = it->second;
				alphabetMap[new_key].first = false;
	}


	// Diacritics
	diacriticMap["0"] = 0x0304; //macron
	diacriticMap["1"] = 0x0301; //acute accent
	diacriticMap["2"] = 0x030C; //caron
	diacriticMap["3"] = 0x0300; //grave accent
	
	alphabetMapChanged = true;
}

PinyinAlphabet::~PinyinAlphabet()
{
}

    std::string PinyinAlphabet::extract_first_diacritic( std::string& str, roots::phonology::ipa::DiacriticPosition diac_pos ) const
{
  std::string d = MappedAlphabet::extract_first_diacritic(str, diac_pos);
  return (d == "0"?"":d);
}

} } }
