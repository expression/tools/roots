/*
    This file is part of Roots.

    Roots is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Roots is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Roots.  If not, see <http://www.gnu.org/licenses/>.

    Copyright (c) 2013, 2014 IRISA
*/


#ifndef LIAPHONALPHABET_H_
#define LIAPHONALPHABET_H_

#include "../MappedAlphabet.h"
#include "../../../common.h"
#include "../../../Base.h"

namespace roots { namespace phonology { namespace ipa {

class LiaphonAlphabet: public MappedAlphabet<LiaphonAlphabet>
{
public:
	static LiaphonAlphabet& get_instance();
    static LiaphonAlphabet* get_instance_ptr();
	/**
	 * @brief returns the XML tag name value for current object
	 * @return string constant representing the XML tag name
	 */
	virtual std::string get_xml_tag_name() const { return xml_tag_name(); };

	/**
	 * @brief returns the XML tag name value for current class
	 * @return string constant representing the XML tag name
	 */
	static std::string xml_tag_name() { return "Liaphon"; };

protected:
	LiaphonAlphabet();
	virtual ~LiaphonAlphabet();

};

} } }

#endif /* LIAPHONALPHABET_H_ */
