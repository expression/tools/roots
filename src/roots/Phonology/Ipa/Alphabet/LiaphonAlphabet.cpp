/*
    This file is part of Roots.

    Roots is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Roots is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Roots.  If not, see <http://www.gnu.org/licenses/>.

    Copyright (c) 2013, 2014 IRISA
*/


#include "LiaphonAlphabet.h"

namespace roots { namespace phonology { namespace ipa {

LiaphonAlphabet& LiaphonAlphabet::get_instance()
{
	static LiaphonAlphabet instance;
	return instance;
}

LiaphonAlphabet* LiaphonAlphabet::get_instance_ptr()
{
	return &static_cast<LiaphonAlphabet&>(get_instance());
}


LiaphonAlphabet::LiaphonAlphabet() : MappedAlphabet("liaphon")
{

	alphabetMap["aa"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x61 ), std::set<UChar>()));
	alphabetMap["ai"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x25B), std::set<UChar>()));
	alphabetMap["oe"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x153), std::set<UChar>()));
	alphabetMap["oo"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x254), std::set<UChar>()));
	alphabetMap["ee"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x259), std::set<UChar>()));
	alphabetMap["ei"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x065), std::set<UChar>()));
	alphabetMap["eu"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x0F8), std::set<UChar>()));
	alphabetMap["au"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x06F), std::set<UChar>()));
	alphabetMap["ii"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x069), std::set<UChar>()));
	alphabetMap["uu"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x079), std::set<UChar>()));
	alphabetMap["ou"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x075), std::set<UChar>()));
	std::set<UChar> vec_diacritics;
	vec_diacritics.insert(0x303);
	alphabetMap["an"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x251), vec_diacritics));
	alphabetMap["in"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x25B), vec_diacritics));
	alphabetMap["on"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x254), vec_diacritics));
	alphabetMap["yy"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x06A), std::set<UChar>()));
	alphabetMap["uy"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x265), std::set<UChar>()));
	alphabetMap["ww"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x077), std::set<UChar>()));
	alphabetMap["pp"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x070), std::set<UChar>()));
	alphabetMap["bb"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x062), std::set<UChar>()));
	alphabetMap["tt"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x074), std::set<UChar>()));
	alphabetMap["dd"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x064), std::set<UChar>()));
	alphabetMap["kk"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x06B), std::set<UChar>()));
	alphabetMap["gg"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x261), std::set<UChar>()));
	alphabetMap["mm"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x06D), std::set<UChar>()));
	alphabetMap["nn"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x06E), std::set<UChar>()));
	alphabetMap["gn"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x272), std::set<UChar>()));
	alphabetMap["ng"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x14B), std::set<UChar>()));
	alphabetMap["rr"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x281), std::set<UChar>()));
	//alphabetMap["rr"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x280), std::set<UChar>()));	
	alphabetMap["ll"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x06C), std::set<UChar>()));
	alphabetMap["ff"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x066), std::set<UChar>()));
	alphabetMap["vv"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x076), std::set<UChar>()));
	alphabetMap["ss"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x073), std::set<UChar>()));
	alphabetMap["zz"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x07A), std::set<UChar>()));
	alphabetMap["ch"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x283), std::set<UChar>()));
	alphabetMap["jj"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x292), std::set<UChar>()));

	alphabetMapChanged = true;
}

LiaphonAlphabet::~LiaphonAlphabet()
{
}

} } }
