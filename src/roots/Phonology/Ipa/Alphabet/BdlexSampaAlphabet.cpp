/*
    This file is part of Roots.

    Roots is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Roots is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Roots.  If not, see <http://www.gnu.org/licenses/>.

    Copyright (c) 2013, 2014 IRISA
*/


#include "BdlexSampaAlphabet.h"

namespace roots { namespace phonology { namespace ipa {


BdlexSampaAlphabet& BdlexSampaAlphabet::get_instance()
{
	static BdlexSampaAlphabet instance;
	return instance;
}

BdlexSampaAlphabet* BdlexSampaAlphabet::get_instance_ptr()
{
	return &static_cast<BdlexSampaAlphabet&>(get_instance());
}


BdlexSampaAlphabet::BdlexSampaAlphabet() : MappedAlphabet("bdlex")
{

	// Vowels and semi-vowels
	alphabetMap["j"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x6A), std::set<UChar>()));
	alphabetMap["w"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x77), std::set<UChar>()));
	alphabetMap["H"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x265), std::set<UChar>()));
	alphabetMap["i"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x69), std::set<UChar>()));
	//alphabetMap["I"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x69), std::set<UChar>()));
	alphabetMap["y"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x79), std::set<UChar>()));
	alphabetMap["u"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x75), std::set<UChar>()));
	alphabetMap["e"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x65), std::set<UChar>()));
	alphabetMap["2"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0xF8), std::set<UChar>()));
	alphabetMap["o"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x6F), std::set<UChar>()));
	alphabetMap["E/"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x25B), std::set<UChar>())); // variant from e to E
	alphabetMap["6"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x259), std::set<UChar>()));
	alphabetMap["@"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x259), std::set<UChar>()));
	alphabetMap["O/"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x254), std::set<UChar>())); // variant from o to O
	alphabetMap["E"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x25B), std::set<UChar>()));
	alphabetMap["O"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x254), std::set<UChar>()));
	alphabetMap["9"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x153), std::set<UChar>()));
	alphabetMap["a"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x61), std::set<UChar>()));
	alphabetMap["e~"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x25B), boost::assign::list_of(0x303)));
	alphabetMap["a~"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x251), boost::assign::list_of(0x303)));
	alphabetMap["o~"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x254), boost::assign::list_of(0x303)));
	alphabetMap["9~"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x153), boost::assign::list_of(0x303))); // TODO: verify diacritics ??

	// Consonants
	alphabetMap["p"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x70), std::set<UChar>()));
	alphabetMap["t"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x74), std::set<UChar>()));
	alphabetMap["k"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x6B), std::set<UChar>()));
	alphabetMap["b"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x62), std::set<UChar>()));
	alphabetMap["d"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x64), std::set<UChar>()));
	alphabetMap["g"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x261), std::set<UChar>()));
	alphabetMap["f"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x66), std::set<UChar>()));
	alphabetMap["s"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x73), std::set<UChar>()));
	alphabetMap["S"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x283), std::set<UChar>()));
	alphabetMap["v"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x76), std::set<UChar>()));
	alphabetMap["z"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x7A), std::set<UChar>()));
	alphabetMap["Z"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x292), std::set<UChar>()));
	alphabetMap["m"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x6D), std::set<UChar>()));
	alphabetMap["n"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x6E), std::set<UChar>()));
	alphabetMap["J"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x272), std::set<UChar>()));
	alphabetMap["N"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x14B), std::set<UChar>()));
	alphabetMap["l"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x6C), std::set<UChar>()));
	alphabetMap["R"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x281), std::set<UChar>()));
	alphabetMap["x"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x78), std::set<UChar>()));

	alphabetMapChanged = true;
}

BdlexSampaAlphabet::~BdlexSampaAlphabet()
{
}

} } }
