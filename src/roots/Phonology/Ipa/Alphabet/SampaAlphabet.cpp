/*
    This file is part of Roots.

    Roots is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Roots is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Roots.  If not, see <http://www.gnu.org/licenses/>.

    Copyright (c) 2013, 2014 IRISA
*/


#include "SampaAlphabet.h"

namespace roots { namespace phonology { namespace ipa {

SampaAlphabet& SampaAlphabet::get_instance()
{
	static SampaAlphabet instance;
	return instance;
}

SampaAlphabet* SampaAlphabet::get_instance_ptr()
{
	return &static_cast<SampaAlphabet&>(get_instance());
}


SampaAlphabet::SampaAlphabet() : MappedAlphabet("sampa")
{

	// this is now X-SAMPA based on http://en.wikipedia.org/wiki/X-SAMPA
	// Lower case symbols
	alphabetMap["a"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x0061), std::set<UChar>()));
	alphabetMap["b"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x0062), std::set<UChar>()));
	alphabetMap["b_<"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x0253), std::set<UChar>()));
	alphabetMap["c"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x0063), std::set<UChar>()));
	alphabetMap["d"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x0064), std::set<UChar>()));
	alphabetMap["d`"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x0256), std::set<UChar>()));
	alphabetMap["d_<"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x0257), std::set<UChar>()));
	alphabetMap["e"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x0065), std::set<UChar>()));
	alphabetMap["f"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x0066), std::set<UChar>()));
	alphabetMap["g"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x0261), std::set<UChar>()));
	alphabetMap["g_<"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x0260), std::set<UChar>()));
	alphabetMap["h"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x0068), std::set<UChar>()));
	alphabetMap["h\\"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x0266), std::set<UChar>()));
	alphabetMap["i"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x0069), std::set<UChar>()));
	alphabetMap["j"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x006A), std::set<UChar>()));
	alphabetMap["j\\"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x029D), std::set<UChar>()));
	alphabetMap["k"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x006B), std::set<UChar>()));
	alphabetMap["l"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x006C), std::set<UChar>()));
	alphabetMap["l`"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x026D), std::set<UChar>()));
	alphabetMap["l\\"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x027A), std::set<UChar>()));
	alphabetMap["m"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x006D), std::set<UChar>()));
	alphabetMap["n"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x006E), std::set<UChar>()));
	alphabetMap["n`"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x0273), std::set<UChar>()));
	alphabetMap["o"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x006F), std::set<UChar>()));
	alphabetMap["p"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x0070), std::set<UChar>()));
	alphabetMap["p\\"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x0278), std::set<UChar>()));
	alphabetMap["q"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x0071), std::set<UChar>()));
	alphabetMap["r"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x0072), std::set<UChar>()));
	alphabetMap["r`"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x027D), std::set<UChar>()));
	alphabetMap["r\\"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x0279), std::set<UChar>()));
	alphabetMap["r\\`"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x027B), std::set<UChar>()));
	alphabetMap["s"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x0073), std::set<UChar>()));
	alphabetMap["s`"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x0282), std::set<UChar>()));
	alphabetMap["s\\"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x0255), std::set<UChar>()));
	alphabetMap["t"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x0074), std::set<UChar>()));
	alphabetMap["t`"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x0288), std::set<UChar>()));
	alphabetMap["u"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x0075), std::set<UChar>()));
	alphabetMap["v"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x0076), std::set<UChar>()));
	alphabetMap["v\\"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x028B), std::set<UChar>())); /* or P */
	alphabetMap["w"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x0077), std::set<UChar>()));
	alphabetMap["x"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x0078), std::set<UChar>()));
	alphabetMap["x\\"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x0267), std::set<UChar>()));
	alphabetMap["y"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x0079), std::set<UChar>()));
	alphabetMap["z"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x007A), std::set<UChar>()));
	alphabetMap["z`"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x0290), std::set<UChar>()));
	alphabetMap["z\\"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x0291), std::set<UChar>()));

	// Capital symbols
	alphabetMap["A"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x0251), std::set<UChar>()));
	alphabetMap["B"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x03B2), std::set<UChar>()));
	alphabetMap["B\\"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x0299), std::set<UChar>()));
	alphabetMap["C"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x00E7), std::set<UChar>()));
	alphabetMap["D"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x00F0), std::set<UChar>()));
	alphabetMap["E"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x025B), std::set<UChar>()));
	alphabetMap["F"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x0271), std::set<UChar>()));
	alphabetMap["G"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x0263), std::set<UChar>()));
	alphabetMap["G\\"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x0262), std::set<UChar>()));
	alphabetMap["G\\_<"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x029B), std::set<UChar>()));
	alphabetMap["H"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x0265), std::set<UChar>()));
	alphabetMap["H\\"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x029C), std::set<UChar>()));
	alphabetMap["I"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x026A), std::set<UChar>()));
	alphabetMap["I\\"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x026A), boost::assign::list_of(0x0308))); /* or 1_o or @\_r */
	alphabetMap["J"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x0272), std::set<UChar>()));
	alphabetMap["J\\"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x025F), std::set<UChar>()));
	alphabetMap["J\\_<"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x0284), std::set<UChar>()));
	alphabetMap["K"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x026C), std::set<UChar>()));
	alphabetMap["K\\"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x026E), std::set<UChar>()));
	alphabetMap["L"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x028E), std::set<UChar>()));
	alphabetMap["L\\"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x029F), std::set<UChar>()));
	alphabetMap["M"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x026F), std::set<UChar>()));
	alphabetMap["M\\"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x0270), std::set<UChar>()));
	alphabetMap["N"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x014B), std::set<UChar>()));
	alphabetMap["N\\"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x0274), std::set<UChar>()));
	alphabetMap["O"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x0254), std::set<UChar>()));
	alphabetMap["O\\"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x0298), std::set<UChar>()));
	alphabetMap["P"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x028B), std::set<UChar>())); /* or v\ */
	alphabetMap["Q"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x0252), std::set<UChar>()));
	alphabetMap["R"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x0281), std::set<UChar>()));
	alphabetMap["R\\"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x0280), std::set<UChar>()));
	alphabetMap["S"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x0283), std::set<UChar>()));
	alphabetMap["T"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x03B8), std::set<UChar>()));
	alphabetMap["U"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x028A), std::set<UChar>()));
	alphabetMap["U\\"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x028A), boost::assign::list_of(0x0308)));
	alphabetMap["V"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x028C), std::set<UChar>()));
	alphabetMap["W"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x028D), std::set<UChar>()));
	alphabetMap["X"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x03C7), std::set<UChar>()));
	alphabetMap["X\\"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x0127), std::set<UChar>()));
	alphabetMap["Y"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x028F), std::set<UChar>()));
	alphabetMap["Z"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x0292), std::set<UChar>()));

	// Other symbols
	//alphabetMap["."] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x), std::set<UChar>()));
	//alphabetMap["\""] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x), std::set<UChar>()));
	//alphabetMap["%"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x02CC), std::set<UChar>())); /* !! 0x02CC unavailable */
	//alphabetMap["'"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x02B2), std::set<UChar>())); /* or _j */
	//alphabetMap[":"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x), std::set<UChar>()));
	//alphabetMap[":\\"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x), std::set<UChar>()));
	//alphabetMap["-"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x), std::set<UChar>()));
	alphabetMap["@"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x0259), std::set<UChar>()));
	alphabetMap["@\\"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x0258), std::set<UChar>()));
	alphabetMap["{"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x00E6), std::set<UChar>()));
	alphabetMap["}"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x0289), std::set<UChar>()));
	alphabetMap["1"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x0268), std::set<UChar>()));
	alphabetMap["2"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x00F8), std::set<UChar>()));
	alphabetMap["3"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x025C), std::set<UChar>()));
	alphabetMap["3\\"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x025E), std::set<UChar>()));
	alphabetMap["4"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x027E), std::set<UChar>()));
	alphabetMap["5"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x006C), std::set<UChar>()));
	alphabetMap["6"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x0250), std::set<UChar>()));
	alphabetMap["7"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x0264), std::set<UChar>()));
	alphabetMap["8"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x0275), std::set<UChar>()));
	alphabetMap["9"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x0153), std::set<UChar>()));
	alphabetMap["&"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x0276), std::set<UChar>()));
	alphabetMap["?"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x0294), std::set<UChar>()));
	alphabetMap["?\\"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x0295), std::set<UChar>()));
	//alphabetMap["*"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x), std::set<UChar>()));
	//alphabetMap["/"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x), std::set<UChar>()));
	//alphabetMap["<"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x), std::set<UChar>()));
	alphabetMap["<\\"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x02A2), std::set<UChar>()));
	//alphabetMap[">"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x), std::set<UChar>()));
	alphabetMap[">\\"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x02A1), std::set<UChar>()));
	//alphabetMap["^"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0xA71B), std::set<UChar>())); /* !! 0xA71B unavailable */
	//alphabetMap["!"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0xA71C), std::set<UChar>())); /* !! 0xA71C unavailable */
	alphabetMap["!\\"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x01C3), std::set<UChar>()));
	//alphabetMap["|"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x), std::set<UChar>()));
	alphabetMap["|\\"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x01C0), std::set<UChar>()));
	//alphabetMap["||"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x), std::set<UChar>()));
	alphabetMap["|\\|\\"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x01C1), std::set<UChar>()));
	alphabetMap["=\\"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x01C2), std::set<UChar>()));
	//alphabetMap["-\\"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x), std::set<UChar>()));

	/* useless
	std::set<UChar> vec_diacritics;
	vec_diacritics.insert(0x303);
	alphabetMap["e~"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x65), vec_diacritics)));
	alphabetMap["a~"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x61), vec_diacritics)));
	alphabetMap["o~"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x6F), vec_diacritics)));
	alphabetMap["9~"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x153), std::set<UChar>()));
	*/
    // _/


    // Diacritic
    diacriticMap["_\""] = 0x0308; //combining diaeresis
    diacriticMap["_+"] = 0x031F; //combining plus sign below
    diacriticMap["_-"] = 0x0320; //combining minus sign below
    // _/ ?
    diacriticMap["_0"] = 0x0325; //combining ring below
    // _< ? not taken into account since separate symbols are used in IPA

    // = or _= ? syllabic
    diacriticMap["="] = 0x0329; //combining vertical line below
    diacriticMap["_="] = 0x0329; //combining vertical line below
    // _> ? ejective, code ? 0x02BC
    // TODO verify
    diacriticMap["_?\\"] = 0x02E4; //modifier letter reverse glottal stop
    // _\ ?
    diacriticMap["_^"] = 0x032F; //combining inverted bref below
    diacriticMap["_}"] = 0x031A; //combining left angle above
    diacriticMap["`"] = 0x02DE; //modifier letter rhotic hook
    diacriticMap["~"] = 0x0303; //combining tilde
    diacriticMap["_~"] = 0x0303; //combining tilde
    diacriticMap["_A"] = 0x0318; //combining left tack below
    diacriticMap["_a"] = 0x033A; //combining inverted bridge below
    // _B : extra low tone 0x030F
    // _B_L : low rising tone
    diacriticMap["_c"] = 0x031C; //combining left half ring below
    diacriticMap["_d"] = 0x032A; //combining bridge below
    diacriticMap["_e"] = 0x0334; //combining tilde overlay
    // <F> 0x2198 global fall
    // _F ?
    diacriticMap["_G"] = 0x02E0; //modifier letter gamma
    // _H high tone 0x0301
    // _H_T high rising tone
    diacriticMap["_h"] = 0x02B0; //modifier letter small h
    diacriticMap["_j"] = 0x02B2; //modifier letter j
    diacriticMap["'"] = 0x02B2; //modifier letter j
    diacriticMap["_k"] = 0x0330; //combining tilde below
    // _L low tone 0x0300
    diacriticMap["_l"] = 0x02E1; //modifier letter small l
    // _M mid tone 0x0304
    diacriticMap["_m"] = 0x033B; //combining square below
    diacriticMap["_N"] = 0x033C; //combining seagull below
    diacriticMap["_n"] = 0x207F; //superscript latin small n
    diacriticMap["_O"] = 0x0339; //combining right half ring below
    diacriticMap["_o"] = 0x031E; //combining down tack below
    diacriticMap["_q"] = 0x0319; //combining right tack below
    // <R> global rise 0x2197
    // _R
    // _R_F
    diacriticMap["_r"] = 0x031D; //combining up tack below
    // _T 0x030B Extra high tone
    diacriticMap["_t"] = 0x0324; //combining diaeresis below
    diacriticMap["_v"] = 0x032C; //combining caron below
    diacriticMap["_w"] = 0x02B7; //modifier letter w
    // _X extra short 0x0306
    diacriticMap["_x"] = 0x033D; //combining x above

    // Added
    diacriticMap[":"] = 0x02D0;
		diacriticMap["\""] = 0x02C8; //primary stress
		diacriticMap["%"] = 0x02CC; //secondary stress

		alphabetMapChanged = true;
}

SampaAlphabet::~SampaAlphabet()
{
}

} } }
