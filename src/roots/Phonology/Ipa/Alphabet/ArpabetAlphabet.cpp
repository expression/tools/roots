/*
    This file is part of Roots.

    Roots is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Roots is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Roots.  If not, see <http://www.gnu.org/licenses/>.

    Copyright (c) 2013, 2014 IRISA
*/


#include "ArpabetAlphabet.h"

namespace roots { namespace phonology { namespace ipa {

bool ArpabetAlphabet::SHOW_UNSTRESSED = true;

ArpabetAlphabet& ArpabetAlphabet::get_instance()
{
	static ArpabetAlphabet instance;
	return instance;
}

ArpabetAlphabet* ArpabetAlphabet::get_instance_ptr()
{
	return &static_cast<ArpabetAlphabet&>(get_instance());
}

ArpabetAlphabet::ArpabetAlphabet() : MappedAlphabet("arpabet", false)
{
	// Vowels
	// Monophtongs

	alphabetMap["AO"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x0254),
																														std::set<UChar>()));
	alphabetMap["AA"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x0251),
																														std::set<UChar>()));
	alphabetMap["IY"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x0069),
																														std::set<UChar>()));
	alphabetMap["UW"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x0075),
																														std::set<UChar>()));
	alphabetMap["EH"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x025B),
																														std::set<UChar>()));
	alphabetMap["IH"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x026A),
																														std::set<UChar>()));
	alphabetMap["UH"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x028A),
																														std::set<UChar>()));
	alphabetMap["AH"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x028C),
																														std::set<UChar>()));
	alphabetMap["AX"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x0259),
																														std::set<UChar>()));
	alphabetMap["AE"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x00E6),
																														std::set<UChar>()));
	alphabetMap["AON"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x0254),
																														 boost::assign::list_of(0x0303))); //diacritic
	alphabetMap["AAN"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x0251),
																														 boost::assign::list_of(0x0303))); //diacritic
	alphabetMap["IYN"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x0069),
																														 boost::assign::list_of(0x0303))); //diacritic
	alphabetMap["UWN"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x0075),
																														 boost::assign::list_of(0x0303))); //diacritic
	alphabetMap["EHN"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x025B),
																														 boost::assign::list_of(0x0303))); //diacritic
	alphabetMap["IHN"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x026A),
																														 boost::assign::list_of(0x0303))); //diacritic
	alphabetMap["UHN"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x028A),
																														 boost::assign::list_of(0x0303))); //diacritic
	alphabetMap["AHN"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x028C),
																														 boost::assign::list_of(0x0303))); //diacritic
	alphabetMap["AEN"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x00E6),
																														 boost::assign::list_of(0x0303))); //diacritic
	// Diphtongs
	alphabetMap["EY"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x0065)(0x026A),
																														std::set<UChar>())); //double IPA
	alphabetMap["AY"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x0061)(0x026A),
																														std::set<UChar>())); //double IPA
	alphabetMap["OW"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x006F)(0x028A),
																														std::set<UChar>())); //double IPA
	alphabetMap["AW"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x0061)(0x028A),
																														std::set<UChar>())); //double IPA
	alphabetMap["OY"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x0254)(0x026A),
																														std::set<UChar>())); //double IPA
	alphabetMap["EYN"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x0065)(0x026A),
																														 boost::assign::list_of(0x0303))); //double IPA and diacritic
	alphabetMap["AYN"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x0061)(0x026A),
																														 boost::assign::list_of(0x0303))); //double IPA and diacritic
	alphabetMap["OWN"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x006F)(0x028A),
																														 boost::assign::list_of(0x0303))); //double IPA and diacritic
	alphabetMap["AWN"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x0061)(0x028A),
																														 boost::assign::list_of(0x0303))); //double IPA and diacritic
	alphabetMap["OYN"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x0254)(0x026A),
																														 boost::assign::list_of(0x0303))); //double IPA and diacritic
	// R-colored vowels
	alphabetMap["ER"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x025C),
																														boost::assign::list_of(0x02DE))); //diacritic
	alphabetMap["AXR"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x0259),
																														 boost::assign::list_of(0x02DE))); //diacritic
	alphabetMap["ERN"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x025C),
																														 boost::assign::list_of(0x02DE)(0x0303))); //double diacritics

	// Consonants
	// Stops
	alphabetMap["P"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x0070),
																													 std::set<UChar>()));
	alphabetMap["B"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x0062),
																													 std::set<UChar>()));
	alphabetMap["T"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x0074),
																													 std::set<UChar>()));
	alphabetMap["D"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x0064),
																													 std::set<UChar>()));
	alphabetMap["K"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x006B),
																													 std::set<UChar>()));
	alphabetMap["G"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x0261),
																													 std::set<UChar>()));
	//Affricates
	alphabetMap["CH"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x0074)(0x0283),
																														std::set<UChar>())); //double IPA
	alphabetMap["JH"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x0064)(0x0292),
																														std::set<UChar>())); //double IPA
	//Fricatives
	alphabetMap["F"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x0066),
																													 std::set<UChar>()));
	alphabetMap["V"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x0076),
																													 std::set<UChar>()));
	alphabetMap["TH"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x03B8),
																														std::set<UChar>()));
	alphabetMap["DH"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x00F0),
																														std::set<UChar>()));
	alphabetMap["S"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x0073),
																													 std::set<UChar>()));
	alphabetMap["Z"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x007A),
																													 std::set<UChar>()));
	alphabetMap["SH"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x0283),
																														std::set<UChar>()));
	alphabetMap["ZH"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x0292),
																														std::set<UChar>()));
	alphabetMap["HH"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x0068),
																														std::set<UChar>()));
	//Nasals
	alphabetMap["M"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x006D),
																													 std::set<UChar>()));
	alphabetMap["EM"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x006D),
																														boost::assign::list_of(0x0329))); //diacritic
	alphabetMap["N"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x006E),
																													 std::set<UChar>()));
	alphabetMap["EN"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x006E),
																			boost::assign::list_of(0x0329))); //diacritic
	alphabetMap["NG"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x014B),
																														std::set<UChar>()));
	alphabetMap["ENG"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x014B),
																														 boost::assign::list_of(0x030D))); //diacritic
	//Liquids
	alphabetMap["L"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x006C),
																													 boost::assign::list_of(0x0334))); //diacritic but NOT considered here
	alphabetMap["EL"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x006C),
																														boost::assign::list_of(0x0334)(0x0329))); //DOUBLE diacritic
	alphabetMap["R"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x0279),
																													 std::set<UChar>()));
	alphabetMap["DX"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x027E),
																														std::set<UChar>()));
	alphabetMap["NX"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x027E),
																														boost::assign::list_of(0x0303))); //diacritic
	//Semivowels
	alphabetMap["Y"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x006A),
																		 std::set<UChar>()));
	alphabetMap["W"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x0077),
																													 std::set<UChar>()));
	alphabetMap["Q"] = t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x0294),
																											std::set<UChar>()));


	//lowercase version of symbols since Arpabet is not case sensitive
    for (std::map<std::string,t_alphabet_elt>::const_iterator it = alphabetMap.begin();
	     it != alphabetMap.end();
	     ++it) {
	      std::string new_key = it->first;
	      std::transform(new_key.begin(), new_key.end(), new_key.begin(), ::tolower);
	      alphabetMap[new_key] = it->second;
				alphabetMap[new_key].first = false;
	}


	// Diacritics
	diacriticMap["0"] = 0x0000; //no stress
	diacriticMap["1"] = 0x02C8; //primary stress
	diacriticMap["2"] = 0x02CC; //secondary stress

	alphabetMapChanged = true;
}

ArpabetAlphabet::~ArpabetAlphabet()
{
}

std::string ArpabetAlphabet::extract_first_diacritic( std::string& str, roots::phonology::ipa::DiacriticPosition diac_pos ) const
{
  std::string d = MappedAlphabet::extract_first_diacritic(str, diac_pos);
  
  return (d == "0"?"":d);
}
      
std::string ArpabetAlphabet::convert_phoneme( const Phoneme & phoneme)
{
  std::vector<roots::phonology::ipa::Ipa*> ipa;
  for(int ipaIdx=0; ipaIdx<phoneme.get_n_ipa(); ++ipaIdx)
    {
      ipa.push_back(const_cast<roots::phonology::ipa::Ipa*>(&phoneme.get_ipa(ipaIdx)));
    }

  std::string result = convert_ipas(ipa);
  if(SHOW_UNSTRESSED && phoneme.is_a(CATEGORY_VOWEL) && !phoneme.is_stressed())
    {
      result += "0";
    }
  return result;
}
            
std::string ArpabetAlphabet::approximate_phoneme( const Phoneme & phoneme)
{
  std::vector<roots::phonology::ipa::Ipa*> ipa;
  for(int ipaIdx=0; ipaIdx<phoneme.get_n_ipa(); ++ipaIdx)
    {
      ipa.push_back(const_cast<roots::phonology::ipa::Ipa*>(&phoneme.get_ipa(ipaIdx)));
    }

  std::string result = approximate_ipas(ipa);
  if(SHOW_UNSTRESSED && phoneme.is_a(CATEGORY_VOWEL) && !phoneme.is_stressed())
    {
      result += "0";
    }  
  return result;
}
      
} } }
