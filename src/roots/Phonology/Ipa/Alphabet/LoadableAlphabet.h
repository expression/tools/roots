/*
    This file is part of Roots.

    Roots is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Roots is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Roots.  If not, see <http://www.gnu.org/licenses/>.

    Copyright (c) 2013, 2014 IRISA
*/


#ifndef LOADABLE_ALPHABET_H_
#define LOADABLE_ALPHABET_H_


#include "../../../common.h"
#include "../../../Base.h"
#include "../MappedAlphabet.h"


namespace roots { namespace phonology { namespace ipa {

class LoadableAlphabet: public MappedAlphabet<LoadableAlphabet>//, public roots::Base
{
public:
	LoadableAlphabet();
	virtual ~LoadableAlphabet();

	/**
	 * @brief returns the XML tag name value for current object
	 * @return string constant representing the XML tag name
	 */
	virtual std::string get_xml_tag_name() const { return xml_tag_name(); };

	/**
	 * @brief returns the XML tag name value for current class
	 * @return string constant representing the XML tag name
	 */
	static std::string xml_tag_name() { return "LoadableAlphabet"; };

	/**
	 * @brief Loads a corpus stored in a file.
	 * @note Push back loaded data if current corpus is not empty.
	 * @param filename File path
	 */
	void load(const std::string & filename);
    
	/**
	 * @brief Saves a corpus into a file.
	 * @note Data contained in the file are erased.
	 * @note Already existing basedirs are not changed.
	 * @param filename File path
	 */
	void save(const std::string & filename);

    /**
     * @brief returns classname for current object
     * @return string constant representing the classname
     */
    virtual std::string get_classname() const { return classname(); };

    /**
     * @brief returns classname for current class
     * @return string constant representing the classname
     */
    static std::string classname() { return "Phonology::Ipa::LoadableAlphabet"; };

 private:
	/**
	 * @brief Write the entity into a RootsStream
	 * @param stream the stream from which we inflate the element
	 * @param is_terminal indicates that the node is terminal (true by default)
	 * @param parentNode
	 */
	virtual void deflate (RootsStream * stream, bool is_terminal=true, int list_index=-1);

	/**
	 * @brief Extracts the entity from the stream
	 * @param stream the stream from which we inflate the element
	 */
	virtual void inflate(RootsStream * stream, bool is_terminal=true, int list_index=-1);

	/**
	 * @brief Extracts the entity from the stream
	 * @param stream the stream from which we inflate the element
	 */
	static LoadableAlphabet * inflate_object(RootsStream * stream, int list_index=-1);


protected:

};

} } }


#endif /* LOADABLE_ALPHABET_H_ */
