/*
    This file is part of Roots.

    Roots is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Roots is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Roots.  If not, see <http://www.gnu.org/licenses/>.

    Copyright (c) 2013, 2014 IRISA
*/


#include "IrisaAlphabet.h"

namespace roots { namespace phonology { namespace ipa {


IrisaAlphabet& IrisaAlphabet::get_instance()
{
	static IrisaAlphabet instance;
	return instance;
}

IrisaAlphabet* IrisaAlphabet::get_instance_ptr()
{
	return &static_cast<IrisaAlphabet&>(get_instance());
}

IrisaAlphabet::IrisaAlphabet() : MappedAlphabet("irisa")
{
	//std::set<UChar> empty_vec = std::set<UChar>();

    alphabetMap = boost::assign::list_of <std::pair<std::string,t_alphabet_elt> >
		("a", t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x61 ), std::set<UChar>())))
		("ai", t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x25B), std::set<UChar>())))
		("oe", t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x153), std::set<UChar>())))
		("o", t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x254), std::set<UChar>())))
		("e", t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x259), std::set<UChar>())))
		("ei", t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x065), std::set<UChar>())))
		("eu", t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x0F8), std::set<UChar>())))
		("au", t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x06F), std::set<UChar>())))
		("i", t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x069), std::set<UChar>())))
		("u", t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x079), std::set<UChar>())))
		("ou", t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x075), std::set<UChar>())))
		("an", t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x251), boost::assign::list_of (0x303))))
		("in", t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x25B), boost::assign::list_of (0x303))))
		("on", t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x254), boost::assign::list_of (0x303))))
		("y", t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x06A), std::set<UChar>())))
		("uy", t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x265), std::set<UChar>())))
		("w", t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x077), std::set<UChar>())))
		("p", t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x070), std::set<UChar>())))
		("b", t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x062), std::set<UChar>())))
		("t", t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x074), std::set<UChar>())))
		("d", t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x064), std::set<UChar>())))
		("k", t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x06B), std::set<UChar>())))
		("g", t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x261), std::set<UChar>())))
		("m", t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x06D), std::set<UChar>())))
		("n", t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x06E), std::set<UChar>())))
		("gn", t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x272), std::set<UChar>())))
		("ng", t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x14B), std::set<UChar>())))
		("r", t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x280), std::set<UChar>())))
		("l", t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x06C), std::set<UChar>())))
		("f", t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x066), std::set<UChar>())))
		("v", t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x076), std::set<UChar>())))
		("s", t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x073), std::set<UChar>())))
		("z", t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x07A), std::set<UChar>())))
		("ch", t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x283), std::set<UChar>())))
		("j", t_alphabet_elt (true, t_alphabet_ipas(boost::assign::list_of(0x292), std::set<UChar>()))).to_container(alphabetMap);

	alphabetMapChanged = true;
}

IrisaAlphabet::~IrisaAlphabet()
{
}


} } } // END OF NAMESPACES
