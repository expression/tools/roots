/*
    This file is part of Roots.

    Roots is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Roots is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Roots.  If not, see <http://www.gnu.org/licenses/>.

    Copyright (c) 2013, 2014 IRISA
*/


#include "LoadableAlphabet.h"
#include "../../../RootsStreamJson.h"

#ifdef USE_STREAM_XML
#include "RootsStreamXML.h"
#endif

#include <boost/filesystem.hpp>

namespace roots { namespace phonology { namespace ipa {

			LoadableAlphabet::LoadableAlphabet() : MappedAlphabet("loadable")
			{
				//std::set<UChar> empty_vec = std::set<UChar>();
				alphabetMapChanged = true;
			}

			LoadableAlphabet::~LoadableAlphabet()
			{
			}

			void LoadableAlphabet::load(const std::string & filename)
			{
				roots::RootsStream * ptRootsStreamLoader = NULL;
				if(endsWith(filename,".json"))
					{
						ptRootsStreamLoader = new RootsStreamJson();
					}
#ifdef USE_STREAM_XML
				else if(endsWith(filename,".xml"))
					{
						ptRootsStreamLoader = new RootsStreamXML();
					}
#endif
				else
					{
                        std::stringstream ss;
						ss << "Corpus::load : unknown file extension for file <" << filename << ">!";
						throw RootsException(__FILE__, __LINE__, ss.str().c_str());
					}
				ptRootsStreamLoader->load(filename);
				this->inflate(ptRootsStreamLoader);
				delete ptRootsStreamLoader;
			}


			void LoadableAlphabet::save(const std::string & filename)
			{
				// Deep save?
				std::stringstream ss;
				ss << filename;
				boost::filesystem::path target_dirname(ss.str().c_str());
				target_dirname.remove_filename();

				// Check if target dirname must be created
				if (!boost::filesystem::exists(target_dirname)) {
					boost::filesystem::create_directories(target_dirname);
				}
    
				// Shallow save
				roots::RootsStream * ptRootsStreamLoader = NULL;
				if(endsWith(filename,".json"))
					{
						ptRootsStreamLoader = new RootsStreamJson();
					}
#ifdef USE_STREAM_XML
				else if(endsWith(filename,".xml"))
					{
						ptRootsStreamLoader = new RootsStreamXML();
					}
#endif
				else
					{
						std::stringstream ss;
						ss << "Corpus::load : unknown file extension for file <" << filename << ">!";
						throw RootsException(__FILE__, __LINE__, ss.str().c_str());
					}
				this->deflate(ptRootsStreamLoader);
				ptRootsStreamLoader->save(filename);
				delete ptRootsStreamLoader;
			}



			void LoadableAlphabet::deflate (RootsStream * stream, bool is_terminal, int list_index)
			{
				Base::deflate(stream,false,list_index);

				alphabetMapChanged = true;

				stream->append_unicodestring_content("name", this->get_name());

				stream->append_object("alphabet_map");
				int cpt=0;
				for(t_alphabetMap::const_iterator iterMap=alphabetMap.begin();
						iterMap != alphabetMap.end(); ++iterMap, ++cpt)
					{
						stream->append_object("element", cpt);
						stream->append_unicodestring_content("label", (*iterMap).first);
						const t_alphabet_elt &alphabetElt = (*iterMap).second;

						stream->append_bool_content("primary", alphabetElt.first);

						const t_alphabet_ipas & alphabetIpas = alphabetElt.second;
						stream->append_vector_uchar_content("ipas", alphabetIpas.first);
						if(alphabetIpas.second.size() != 0)
							stream->append_set_uchar_content("diacritics", alphabetIpas.second);
					
						stream->close_object();
					}
				stream->close_object();

				stream->append_object("diacritic_map");
				cpt=0;
				for(t_diacriticMap::const_iterator iterMap=diacriticMap.begin();
						iterMap != diacriticMap.end(); ++iterMap, ++cpt)
					{
						stream->append_object("element", cpt);
						stream->append_unicodestring_content("label", (*iterMap).first);
						stream->append_uchar_content("value",(*iterMap).second);					
						stream->close_object();
					}

				stream->close_object();

				if(is_terminal)
					{stream->close_object();}
			}


			void LoadableAlphabet::inflate(RootsStream * stream, bool is_terminal, int list_index)
			{
				//std::string ipaClassname;

				Base::inflate(stream,false,list_index);

				alphabetMap = t_alphabetMap();
				diacriticMap = t_diacriticMap();
			
				std::string name = stream->get_unicodestring_content("name");
				this->set_name(name);

				stream->open_object("alphabet_map");
				int nElement = stream->get_n_children("element");
				for(int cpt=0; cpt<nElement; ++cpt)
					{
						stream->open_object("element", cpt);
						std::string label = stream->get_unicodestring_content("label");
						t_alphabet_elt alphabetElt; 
						if(stream->has_child("primary"))
							alphabetElt.first = stream->get_bool_content("primary");
						else
							alphabetElt.first = true;

						t_alphabet_ipas alphabetIpas;
						alphabetIpas.first = stream->get_vector_uchar_content("ipas");
						if(stream->has_child("diacritics"))
							{
								alphabetIpas.second = stream->get_set_uchar_content("diacritics");
							}
						else
							{
								alphabetIpas.second = std::set<UChar>();
							}
					
						alphabetElt.second = alphabetIpas;
						alphabetMap[label] = alphabetElt;

						stream->close_object();
					}
				stream->close_object();

				stream->open_object("diacritic_map");
				nElement = stream->get_n_children("element");
				for(int cpt=0; cpt<nElement; ++cpt)
					{
						stream->open_object("element", cpt);
						std::string label = stream->get_unicodestring_content("label");
						UChar value = stream->get_uchar_content("value");
						diacriticMap[label] = value;
						stream->close_object();
					}
				stream->close_object();

				alphabetMapChanged = true;

				if(is_terminal) stream->close_children();
			}

			LoadableAlphabet * LoadableAlphabet::inflate_object(RootsStream * stream, int list_index)
			{
				LoadableAlphabet *t = new LoadableAlphabet();
				t->inflate(stream,true,list_index);
				return t;
			}

		} } } // END OF NAMESPACES
