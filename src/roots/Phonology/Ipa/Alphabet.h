/*
  This file is part of Roots.

  Roots is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Roots is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with Roots.  If not, see <http://www.gnu.org/licenses/>.

  Copyright (c) 2013, 2014 IRISA
*/

#ifndef ALPHABET_H
#define ALPHABET_H

#include "../../Base.h"
#include "../../common.h"
#include "../IpaConstants.h"
#include "AlphabetException.h"

#include <boost/assign/list_inserter.hpp>
#include <boost/assign/list_of.hpp>  // for 'map_list_of()'

namespace roots {
namespace phonology {

class Phoneme;  // Forward declaration needed by Alphabet::convert_phoneme
                // function

namespace ipa {

class Ipa;  // Forward declaration needed by Alphabet::convert_ipa function

class Alphabet {
 public:
  Alphabet(const std::string& _name, const bool caseSensitive = true);
  virtual ~Alphabet();

  const std::string& get_name() const;
  void set_name(const std::string& n) { name = n; };
  const std::string& get_alphabet_name() const;

  bool is_case_sensitive() const;

  virtual bool is_label(const std::string& label) const = 0;

  virtual std::vector<UChar> get_symbols(const std::string& label) = 0;

  /**
   * @brief Test is a given string symbol can be mapped to any diacritic in the
   * current alphabet
   * @param diacritic_str String to tested for parsing
   * @param diac_pos Optional, limit the test of preceding or following
   * diacritics in the alphabet
   * @return bool
   */
  virtual bool is_diacritic(const std::string& diacritic_str,
                            roots::phonology::ipa::DiacriticPosition diac_pos =
                                DIACRITIC_POSITION_ANY) const = 0;

  /**
   * @brief Test if a given diacritic should precede its combined symbol in the
   * current alphabet
   * @param diac The diacritic to be tested
   * @return True if preceding or indifferent, false otherwise
   * @notice By default, no diacritic is supposed to precede its symbol.
   */
  virtual bool is_preceding_diacritic(Diacritic diac) const = 0;

  /**
   * @brief Test if a given diacritic should follow its combined symbol in the
   * current alphabet
   * @param diac The diacritic to be tested
   * @return True if following or indifferent, false otherwise
   * * @notice By default, all diacritics are supposed to follow their symbol.
   */
  virtual bool is_following_diacritic(
      roots::phonology::ipa::Diacritic) const = 0;

  /**
   * @brief Transform a diacritic string into its unicode representation
   * @param diacritic_str The diacritic string to be transformed
   * @return The corresponding unicode representation (UChar), throws an
   * exception if the input string is invalid (unknown diacritic)
   */
  virtual UChar get_diacritic(const std::string& diacritic_str) = 0;

  /**
   * @brief Return the diacritics associated with a given symbol label
   * @param label The alphabet symbol
   * @return A set of unicode characters standing for all the associated IPA
   * diacritics
   */
  virtual std::set<UChar> get_diacritic_symbols(const std::string& label) = 0;

  virtual std::vector<std::string>* get_alphabet_set() = 0;

  virtual std::vector<std::string>* get_all_diacritics_set() = 0;

  //      virtual std::string translate_in_alphabet(const std::string &label,
  // const Alphabet& alphabet, bool verifyDiacritics) = 0;

  /**
   * @brief Parse a string containing a sequence of alphabet symbols and return
   *the sequence of corresponding IPAs
   * @param str String containing alphabet symbols
   * @warning The input string is modified by the method (matched symbols are
   *consumed).
   * @return A vector of IPAs extract from the string
   **/
  virtual std::vector<roots::phonology::ipa::Ipa*> extract_ipas(
      std::string& str) = 0;

  /**
   * @brief Parse a string containing a sequence of alphabet symbols and return
   *the sequence of corresponding IPAs
   * @param c_str A C string containing alphabet symbols
   * @return A vector of IPAs extract from the string
   **/
  virtual std::vector<roots::phonology::ipa::Ipa*> extract_ipas(
      const char* c_str) = 0;

  /**
   * @brief Try to parse a string containing a sequence of alphabet symbols and
   * return true if the parsing succeeded, false if it failed.
   * @param str String containing alphabet symbols
   * @warning The input string is NOT modified by the method (matched symbols
   * are NOT consumed here), as opposed to method extract_ipas().
   * @return True if parsing is a success, false otherwise
   */
  virtual bool test_ipa_extraction(std::string& str) = 0;

  /**
   * @brief Try to convert an IPA (ot 2 IPAs) into the corresponding symbol in
   *the current alphabet.
   * @param ipa sequence of IPA to be converted
   * @return An alphabet symbol if the correspondence is possible, an empty
   *string otherwise.
   **/
  virtual std::string convert_ipas(
      const std::vector<roots::phonology::ipa::Ipa*>& ipa,
      const std::string& separator = "") = 0;

  /**
   * @brief Approximate an IPA (ot 2 IPAs) into the corresponding symbol in the
   *current alphabet.
   * @warning This method is ABSOLUTELY NOT LINEAR IN TIME NOR IN SPACE. Thus,
   *use it on short vectors of IPAs.
   * @param ipa sequence of IPA to be converted
   * @param separator Optional, string separator between alphabet symbols.
   *Default is no separator.
   * @return An alphabet symbol if the correspondence is possible, an empty
   *string otherwise.
   **/
  virtual std::string approximate_ipas(
      const std::vector<roots::phonology::ipa::Ipa*>& ipa,
      const std::string& separator = "") = 0;

  /**
   * @brief Try to convert a phoneme into the corresponding symbol in the
   *current alphabet.
   * @param phoneme Phoneme to be converted
   * @return An alphabet symbol if the correspondence is possible, an empty
   *string otherwise.
   **/
  virtual std::string convert_phoneme(const Phoneme& phoneme) = 0;

  /**
   * @brief Approximate a phoneme into the most accurate symbol in the current
   *alphabet.
   * @param phoneme Phoneme to be approximated
   * @return An alphabet symbol.
   **/
  virtual std::string approximate_phoneme(const Phoneme& phoneme) = 0;

  /**
   * @brief returns the XML tag name value for current object
   * @return string constant representing the XML tag name
   */
  virtual std::string get_xml_tag_name() const { return xml_tag_name(); };

  /**
   * @brief returns the XML tag name value for current class
   * @return string constant representing the XML tag name
   */
  static std::string xml_tag_name() { return "NoAlphabet"; };

  static std::vector<std::string> get_alphabet_names();

  static std::vector<std::string> get_alphabet_xml_tags();

  static roots::phonology::ipa::Alphabet* get_alphabet(
      const std::string& alphabetName);

  static roots::phonology::ipa::Alphabet* get_alphabet_from_xml_tag(
      const std::string& alphabetName);

  static void add_alphabet(roots::phonology::ipa::Alphabet* alphabet);

  std::map<std::string, std::vector<std::string> >
  list_phonemes_by_categories();
  std::map<std::string, std::vector<std::string> >
  list_categories_by_phonemes();

 private:
  static void static_init();

 protected:
  std::string name;

  bool caseSensitive;

  static int max_symbol_string_length;

  static int max_diacritic_string_length;

  static roots::phonology::ipa::Ipa* ipa_static;

  static std::vector<roots::phonology::ipa::Alphabet*> alphabets;

  static bool isInitialized;
};
}
}
}

#endif  // ALPHABET_H
