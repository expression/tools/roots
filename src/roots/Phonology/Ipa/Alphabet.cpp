/*
  This file is part of Roots.

  Roots is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Roots is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with Roots.  If not, see <http://www.gnu.org/licenses/>.

  Copyright (c) 2013, 2014 IRISA
*/


/*
 * <one line to give the program's name and a brief idea of what it does.>
 * Copyright (C) 2014  Gwénolé Lecorvé <gwenole.lecorve@irisa.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "Alphabet.h"
#include "../Phoneme.h"
#include "../../utils.h"

#include "Alphabet/IrisaAlphabet.h"
#include "Alphabet/LiaphonAlphabet.h"
#include "Alphabet/SampaAlphabet.h"
#include "Alphabet/BdlexSampaAlphabet.h"
#include "Alphabet/ArpabetAlphabet.h"
#include "Alphabet/CmudictAlphabet.h"
#include "Alphabet/PinyinAlphabet.h"
#include "../Ipa.h"

namespace roots { namespace phonology { namespace ipa {
            int Alphabet::max_symbol_string_length = -1;
            int Alphabet::max_diacritic_string_length = -1;
            roots::phonology::ipa::Ipa * Alphabet::ipa_static = new roots::phonology::ipa::Ipa();
            std::vector<roots::phonology::ipa::Alphabet *> Alphabet::alphabets = std::vector<roots::phonology::ipa::Alphabet *>();
            bool Alphabet::isInitialized = false;

            void Alphabet::static_init()
            {
                alphabets.push_back(roots::phonology::ipa::ArpabetAlphabet::get_instance_ptr());
                alphabets.push_back(roots::phonology::ipa::CmudictAlphabet::get_instance_ptr());
                alphabets.push_back(roots::phonology::ipa::BdlexSampaAlphabet::get_instance_ptr());
                //alphabets.push_back(roots::phonology::ipa::VoxygenAlphabet::get_instance_ptr());
                alphabets.push_back(roots::phonology::ipa::IrisaAlphabet::get_instance_ptr());
                alphabets.push_back(roots::phonology::ipa::LiaphonAlphabet::get_instance_ptr());
                alphabets.push_back(roots::phonology::ipa::SampaAlphabet::get_instance_ptr());
                alphabets.push_back(roots::phonology::ipa::PinyinAlphabet::get_instance_ptr());
                alphabets.push_back(ipa_static); // TMP : dirty ?
            }

            Alphabet::Alphabet ( const std::string& name, const bool caseSensitive )
            {
            	// DO NOT intialise static variables in a base class constructor
                this->name = name;
                this->caseSensitive = caseSensitive;
            }

            Alphabet::~Alphabet()
            {
                // Nothing to do
            }

            const std::string& Alphabet::get_name() const { return name; }

            const std::string& Alphabet::get_alphabet_name() const { return name; }

            bool Alphabet::is_case_sensitive() const { return caseSensitive; }

            std::vector<std::string> Alphabet::get_alphabet_names()
            {
                std::vector<std::string> names;

                if(!isInitialized)
                {static_init();isInitialized=true;}

                for(std::vector<roots::phonology::ipa::Alphabet *>::const_iterator iter = alphabets.begin();
                    iter != alphabets.end(); ++iter)
                {
                    names.push_back((*iter)->get_name());
                }

                return names;
            }

            std::vector<std::string> Alphabet::get_alphabet_xml_tags()
            {
                std::vector<std::string> names;

                if(!isInitialized)
                {static_init();isInitialized=true;}

                for(std::vector<roots::phonology::ipa::Alphabet *>::const_iterator iter = alphabets.begin();
                    iter != alphabets.end(); ++iter)
                {
                    names.push_back((*iter)->get_xml_tag_name());
                }

                return names;
            }

            roots::phonology::ipa::Alphabet * Alphabet::get_alphabet(const std::string & alphabetName)
            {
                roots::phonology::ipa::Alphabet *alph = NULL;

                if(!isInitialized)
                {static_init();isInitialized=true;}

                for(std::vector<roots::phonology::ipa::Alphabet *>::const_iterator it=alphabets.begin();
                    it!=alphabets.end() && alph==NULL;
                    ++it)
                {
                    if((*it)->get_name() == alphabetName) alph = *it;
                }

                /*if(alph==NULL)
                  {
                  std::clog << "warning: alphabet " << alphabetName << " is unknown!" << std::endl;
                  }*/

                return alph;
            }

            roots::phonology::ipa::Alphabet * Alphabet::get_alphabet_from_xml_tag(const std::string & alphabetName)
            {
                roots::phonology::ipa::Alphabet *alph = NULL;

                if(!isInitialized)
                {static_init();isInitialized=true;}

                for(std::vector<roots::phonology::ipa::Alphabet *>::const_iterator it=alphabets.begin();
                    it!=alphabets.end() && alph==NULL;
                    ++it)
                {
                    if((*it)->get_xml_tag_name() == alphabetName) alph = *it;
                }

                /*if(alph==NULL)
                  {
                  std::clog << "warning: alphabet " << alphabetName << " is unknown!" << std::endl;
                  }*/

                return alph;
            }

            void Alphabet::add_alphabet(roots::phonology::ipa::Alphabet * alphabet)
            {
                if(get_alphabet(alphabet->get_name()) == NULL)
                {
                    alphabets.push_back(alphabet);
                }
            }


            std::map< std::string, std::vector<std::string> > Alphabet::list_categories_by_phonemes()
            {
                std::vector<std::string> *alphabetSet = this->get_alphabet_set();
                std::map< std::string, std::vector<std::string> > result;

                unsigned int i=0;

                for(i=0; i<(*alphabetSet).size(); i++)
                {
                    std::vector<std::string> categoriesVec = std::vector<std::string>();
                    for(int j=0; j<CATEGORIES_LENGTH; j++)
                    {
                        Ipa tmp = Ipa::from_unicode(this->get_symbols((*alphabetSet)[i])[0],
                                                    this->get_diacritic_symbols((*alphabetSet)[i]));
                        if(tmp.is_a((Category) j))
                        {
                            categoriesVec.push_back(CATEGORIES[j]);
                        }
                        //delete tmp;
                    }

                    result[(*alphabetSet)[i]] = categoriesVec;
                }

                delete alphabetSet;

                return result;
            }

            std::map< std::string, std::vector<std::string> > Alphabet::list_phonemes_by_categories()
            {

                std::vector<std::string> *alphabetSet = this->get_alphabet_set();
                std::map< std::string, std::vector<std::string> > result;

                unsigned int i=0;


                for(int j=0; j<CATEGORIES_LENGTH; j++)
                {
                    std::vector<std::string> phonemesVec = std::vector<std::string>();
                    for(i=0; i<(*alphabetSet).size(); i++)
                    {
                        Ipa tmp = Ipa::from_unicode(this->get_symbols((*alphabetSet)[i])[0],
                                                    this->get_diacritic_symbols((*alphabetSet)[i]));
                        if(tmp.is_a((Category) j))
                        {
                            phonemesVec.push_back((*alphabetSet)[i]);
                        }
                        //delete tmp;
                    }

                    result[CATEGORIES[j]] = phonemesVec;
                }

                delete alphabetSet;

                return result;
            }
        }
    }
}
