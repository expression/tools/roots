/*
    This file is part of Roots.

    Roots is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Roots is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Roots.  If not, see <http://www.gnu.org/licenses/>.

    Copyright (c) 2013, 2014 IRISA
*/

#include "Syllable.h"

namespace roots {

namespace phonology {

namespace syllable {

std::map<syllable::ProminenceTag, std::string> Syllable::prominencetagToString =
    boost::assign::map_list_of(ipa::STRESS_LEVEL_NONE, "notprominent")(
        ipa::STRESS_LEVEL_LOW, "lowprominent")(ipa::STRESS_LEVEL_HIGH,
                                               "prominent");

std::map<syllable::ProminenceTag, ipa::Diacritic>
    Syllable::prominencetagToDiacritic = boost::assign::map_list_of(
        ipa::STRESS_LEVEL_NONE, ipa::DIACRITIC_UNDEF)(
        ipa::STRESS_LEVEL_LOW, ipa::DIACRITIC_SECONDARY_STRESS)(
        ipa::STRESS_LEVEL_HIGH, ipa::DIACRITIC_PRIMARY_STRESS);

std::map<std::string, syllable::ProminenceTag> Syllable::stringToProminencetag =
    boost::assign::map_list_of("notprominent", ipa::STRESS_LEVEL_NONE)(
        "lowprominent", ipa::STRESS_LEVEL_LOW)("prominent",
                                               ipa::STRESS_LEVEL_HIGH);

Syllable::Syllable(const bool noInit) : BaseItem(noInit) {
  _prominent = ipa::STRESS_LEVEL_NONE;
}

Syllable::Syllable(const Syllable& syl) : BaseItem(syl) {
  *this = syl;
  /*
    _onset
    _nucleus
    _coda
    _phonemeSequence
    _phonemeReferenceArray
    _phonemeSequenceId
    _prominent
    _prosodicClass
  */
}

Syllable::~Syllable() {}

Syllable* Syllable::clone() const { return new Syllable(*this); }

Syllable& Syllable::operator=(const Syllable& syl) {
  BaseItem::operator=(syl);

  _onset = syl.get_onset_indices();
  _nucleus = syl.get_nucleus_indices();
  _coda = syl.get_coda_indices();
  _phonemeSequence = const_cast<Syllable&>(syl).get_phoneme_sequence();

  _prominent = syl.get_prominent();
  _prosodicClass = syl.get_prosodic_class();

  return *this;
}

void Syllable::set_coda(const std::vector<Phoneme*>& phonemes) {
  std::vector<int> indices;
  size_t n = phonemes.size();
  for (size_t i = 0; i < n; i++) {
    if (!phonemes[i]->is_in_sequence()) {
      std::stringstream ss;
      ss << "Syllable::set_coda(): Item is not part of a sequence. Item is "
         << phonemes[i]->to_string() << "\n";
      std::cerr << ss.str().c_str() << std::endl;
      throw RootsException(__FILE__, __LINE__, ss.str().c_str());
    } else {
      indices.push_back(phonemes[i]->get_in_sequence_index());
    }
  }
  set_coda_indices(indices);
}

void Syllable::set_nucleus(const std::vector<Phoneme*>& phonemes) {
  std::vector<int> indices;
  size_t n = phonemes.size();
  for (size_t i = 0; i < n; i++) {
    if (!phonemes[i]->is_in_sequence()) {
      std::stringstream ss;
      ss << "Syllable::set_nucleus(): Item is not part of a sequence. Item is "
         << phonemes[i]->to_string() << "\n";
      std::cerr << ss.str().c_str() << std::endl;
      throw RootsException(__FILE__, __LINE__, ss.str().c_str());
    } else {
      indices.push_back(phonemes[i]->get_in_sequence_index());
    }
  }
  set_nucleus_indices(indices);
}

void Syllable::set_onset(const std::vector<Phoneme*>& phonemes) {
  std::vector<int> indices;
  size_t n = phonemes.size();
  for (size_t i = 0; i < n; i++) {
    if (!phonemes[i]->is_in_sequence()) {
      std::stringstream ss;
      ss << "Syllable::set_onset(): Item is not part of a sequence. Item is "
         << phonemes[i]->to_string() << "\n";
      std::cerr << ss.str().c_str() << std::endl;
      throw RootsException(__FILE__, __LINE__, ss.str().c_str());
    } else {
      indices.push_back(phonemes[i]->get_in_sequence_index());
    }
  }
  set_onset_indices(indices);
}

std::vector<Phoneme*> Syllable::get_coda() const {
  if (!has_related_sequence()) {
    std::stringstream ss;
    ss << "Syllable::get_coda(): No phoneme sequence attached to the current "
          "syllable.\n";
    std::cerr << ss.str().c_str() << std::endl;
    throw RootsException(__FILE__, __LINE__, ss.str().c_str());
  }
  std::vector<Phoneme*> phonemes;
  size_t n = _coda.size();
  sequence::PhonemeSequence* phs = get_phoneme_sequence();
  for (size_t i = 0; i < n; i++) {
    phonemes.push_back(phs->get_item(_coda[i]));
  }
  return phonemes;
}

std::vector<Phoneme*> Syllable::get_nucleus() const {
  if (!has_related_sequence()) {
    std::stringstream ss;
    ss << "Syllable::get_nucleus(): No phoneme sequence attached to the "
          "current syllable.\n";
    std::cerr << ss.str().c_str() << std::endl;
    throw RootsException(__FILE__, __LINE__, ss.str().c_str());
  }
  std::vector<Phoneme*> phonemes;
  size_t n = _nucleus.size();
  sequence::PhonemeSequence* phs = get_phoneme_sequence();
  for (size_t i = 0; i < n; i++) {
    phonemes.push_back(phs->get_item(_nucleus[i]));
  }
  return phonemes;
}

std::vector<Phoneme*> Syllable::get_onset() const {
  if (!has_related_sequence()) {
    std::stringstream ss;
    ss << "Syllable::get_onset(): No phoneme sequence attached to the current "
          "syllable.\n";
    std::cerr << ss.str().c_str() << std::endl;
    throw RootsException(__FILE__, __LINE__, ss.str().c_str());
  }
  std::vector<Phoneme*> phonemes;
  size_t n = _onset.size();
  sequence::PhonemeSequence* phs = get_phoneme_sequence();
  for (size_t i = 0; i < n; i++) {
    phonemes.push_back(phs->get_item(_onset[i]));
  }
  return phonemes;
}

const std::string Syllable::get_phoneme_sequence_id() const {
  if (_phonemeSequence == NULL) {
    return "";
  } else {
    return _phonemeSequence->get_id();
  }
}

void Syllable::set_phoneme_sequence(
    sequence::PhonemeSequence* _phonemeSequence) {
  if (_phonemeSequence == NULL) {
    throw RootsException(__FILE__, __LINE__,
                         "NULL pointer for phoneme sequence forbidden!");
  }
  this->_phonemeSequence = _phonemeSequence;
}

void Syllable::set_related_sequence(sequence::Sequence* _relatedSequence) {
  set_phoneme_sequence(_relatedSequence->as<sequence::PhonemeSequence>());
}

void Syllable::translate_index(int offset) {
  for (std::vector<int>::iterator it = _onset.begin(); it != _onset.end();
       ++it) {
    *it += offset;
  }
  for (std::vector<int>::iterator it = _nucleus.begin(); it != _nucleus.end();
       ++it) {
    *it += offset;
  }
  for (std::vector<int>::iterator it = _coda.begin(); it != _coda.end(); ++it) {
    *it += offset;
  }
}

void Syllable::from_phoneme_indices(const std::vector<int>& indexArray) {
  bool flagHaveSeenVowel = false;

  if (_phonemeSequence->count() == 0) {
    throw RootsException(__FILE__, __LINE__,
                         "Phoneme reference array is empty!");
  }

  for (unsigned int i = 0; i < indexArray.size(); i++) {
    Phoneme* ref = _phonemeSequence->get_item(indexArray[i]);

    bool contains_schwa = false;
    for (int ipaIdx = 0; ipaIdx < ref->get_n_ipa() && !contains_schwa;
         ++ipaIdx) {
      ipa::Ipa currentIpa = ref->get_ipa(ipaIdx);
      contains_schwa = contains_schwa || (currentIpa.get_unicode() == 0x259);
    }

    if (ref->is_a(roots::phonology::ipa::CATEGORY_VOWEL) &&
        !(contains_schwa && flagHaveSeenVowel))  // Shwa could be in coda
    {
      if (flagHaveSeenVowel && !contains_schwa) {
        std::stringstream ss;
        ss << "from_phoneme_indices: Trying to add a second vowel \""
           << ref->to_string() << "\" in syllable \"" << this->to_string()
           << "\" when reading phoneme \"" << ref->to_string()
           << "\". Forbidden.";
        throw SyllableMultipleVowelException(__FILE__, __LINE__, ss.str());
      }

      flagHaveSeenVowel = true;
      this->_nucleus.push_back(indexArray[i]);
    } else {
      if (!flagHaveSeenVowel) {
        this->_onset.push_back(indexArray[i]);
      } else {
        this->_coda.push_back(indexArray[i]);
      }
    }
  }

  //	 if(!flagHaveSeenVowel)
  //	 {
  //	 	throw SyllableNoNucleusException(__FILE__, __LINE__, "Syllable must
  //have one nucleus (vowel)... ");
  //	 }
}

Syllable Syllable::from_phoneme_indices(
    sequence::PhonemeSequence* _phonemeSequence,
    const std::vector<int>& indexArray) {
  Syllable syl;
  syl.set_phoneme_sequence(_phonemeSequence);
  syl.from_phoneme_indices(indexArray);
  return syl;
}

void Syllable::from_phonemes(const std::vector<Phoneme*>& phonemes) {
  std::vector<int> ph_indices;
  sequence::PhonemeSequence* phs = NULL;
  size_t n = phonemes.size();
  for (size_t i = 0; i < n; i++) {
    if (!phonemes[i]->is_in_sequence()) {
      std::stringstream ss;
      ss << "from_phonemes: Trying to add a phoneme \""
         << phonemes[i]->to_string()
         << "\" which is not part of a phoneme sequence.\n";
      throw SyllableMultipleVowelException(__FILE__, __LINE__, ss.str());
    } else {
      if (i == 0) {
        phs = phonemes[0]->get_in_sequence()->as<sequence::PhonemeSequence>();
      }
      ph_indices.push_back(phonemes[i]->get_in_sequence_index());
    }
  }
  set_phoneme_sequence(phs);
  from_phoneme_indices(ph_indices);
}

std::vector<int> Syllable::to_phoneme_indices() const {
  std::vector<int> vec;
  for (unsigned int i = 0; i < this->_onset.size(); i++)
    vec.push_back(_onset[i]);
  for (unsigned int i = 0; i < this->_nucleus.size(); i++)
    vec.push_back(_nucleus[i]);
  for (unsigned int i = 0; i < this->_coda.size(); i++) vec.push_back(_coda[i]);
  return vec;
}

std::vector<Phoneme*> Syllable::to_phonemes() const {
  std::vector<Phoneme*> vec;
  if (has_related_sequence()) {
    for (unsigned int i = 0; i < this->_onset.size(); i++) {
      vec.push_back(get_phoneme_sequence()->get_item(_onset[i]));
    }
    for (unsigned int i = 0; i < this->_nucleus.size(); i++) {
      vec.push_back(get_phoneme_sequence()->get_item(_nucleus[i]));
    }
    for (unsigned int i = 0; i < this->_coda.size(); i++) {
      vec.push_back(get_phoneme_sequence()->get_item(_coda[i]));
    }
  }
  return vec;
}

void Syllable::insert_related_element(int index) {
  for (unsigned int i = 0; i < _onset.size(); i++) {
    _onset[i] = (_onset[i] >= index) ? _onset[i] : _onset[i] + 1;
  }
  for (unsigned int i = 0; i < _nucleus.size(); i++) {
    _nucleus[i] = (_nucleus[i] >= index) ? _nucleus[i] : _nucleus[i] + 1;
  }
  for (unsigned int i = 0; i < _coda.size(); i++) {
    _coda[i] = (_coda[i] >= index) ? _coda[i] : _coda[i] + 1;
  }
}

void Syllable::remove_related_element(int index) {
  // Remove from onset
  int oindex = -1;
  for (unsigned int i = 0; i < _onset.size(); i++) {
    if (_onset[i] == index) oindex = i;
    _onset[i] = (_onset[i] >= index) ? _onset[i] : _onset[i] - 1;
  }
  if (oindex != -1)
    _onset.erase(_onset.begin() + index, _onset.begin() + index + 1);

  // Remove from nucleus
  oindex = -1;
  for (unsigned int i = 0; i < _nucleus.size(); i++) {
    if (_nucleus[i] == index) oindex = i;
    _nucleus[i] = (_nucleus[i] >= index) ? _nucleus[i] : _nucleus[i] - 1;
  }
  if (oindex != -1)
    _nucleus.erase(_nucleus.begin() + index, _nucleus.begin() + index + 1);

  // Remove from coda
  oindex = -1;
  for (unsigned int i = 0; i < _coda.size(); i++) {
    if (_coda[i] == index) oindex = i;
    _coda[i] = (_coda[i] >= index) ? _coda[i] : _coda[i] - 1;
  }
  if (oindex != -1)
    _coda.erase(_coda.begin() + index, _coda.begin() + index + 1);
}

std::string Syllable::to_string(int level) const {
  // std::stringstream ss;
  std::string stressStr, onsetStr, nucleusStr, codaStr, str;

  switch (level) {
    // level 0:
    case 0:
    case 1:  // The same as 0 but phoneme will be displayed as level 1 (IPA)
      if (is_prominent()) {
        ipa::Ipa* ipa = ipa::Ipa::get_ipa_from_diacritic(
            prominencetagToDiacritic[_prominent]);
        stressStr = ipa->to_string();
        if (level != 0) {
          stressStr += " ";
        }
        ipa->destroy();
      }
      if (get_onset_indices().size() > 0) {
        for (unsigned int i = 0; i < this->_onset.size(); i++) {
          int onsetIndex = _onset[i];
          Phoneme* refPhoneme = (*(_phonemeSequence)).get_item(onsetIndex);

          // 		    if(i>0) { onsetStr += " "; }
          onsetStr += refPhoneme->to_string(level);
        }
      }
      if (get_nucleus_indices().size() > 0) {
        for (unsigned int i = 0; i < this->_nucleus.size(); i++) {
          int nucleusIndex = _nucleus[i];
          Phoneme* refPhoneme = (*(_phonemeSequence)).get_item(nucleusIndex);

          // 		    if(i>0) { nucleusStr += " "; }
          nucleusStr += refPhoneme->to_string(level);
        }
      }
      if (get_coda_indices().size() > 0) {
        for (unsigned int i = 0; i < this->_coda.size(); i++) {
          int codaIndex = _coda[i];
          Phoneme* refPhoneme = (*(_phonemeSequence)).get_item(codaIndex);

          // 		    if(i>0) { codaStr += " "; }
          codaStr += refPhoneme->to_string(level);
        }
      }
      if (level == 0) {
        str += stressStr + onsetStr + nucleusStr + codaStr;
      } else {
        str +=
            "<" + stressStr + onsetStr + "," + nucleusStr + "," + codaStr + ">";
      }
      break;
    default:
      if (is_prominent()) {
        stressStr = prominencetagToString[_prominent];
      }
      if (get_onset_indices().size() > 0) {
        for (unsigned int i = 0; i < this->_onset.size(); i++) {
          int onsetIndex = _onset[i];
          Phoneme* refPhoneme = (*(_phonemeSequence)).get_item(onsetIndex);

          if (i > 0) {
            onsetStr += " ; ";
          }
          onsetStr += refPhoneme->to_string(level);
        }
      }
      if (get_nucleus_indices().size() > 0) {
        for (unsigned int i = 0; i < this->_nucleus.size(); i++) {
          int nucleusIndex = _nucleus[i];
          Phoneme* refPhoneme = (*(_phonemeSequence)).get_item(nucleusIndex);

          if (i > 0) {
            nucleusStr += " ; ";
          }
          nucleusStr += refPhoneme->to_string(level);
        }
      }
      if (get_coda_indices().size() > 0) {
        for (unsigned int i = 0; i < this->_coda.size(); i++) {
          int codaIndex = _coda[i];
          Phoneme* refPhoneme = (*(_phonemeSequence)).get_item(codaIndex);

          if (i > 0) {
            codaStr += " ; ";
          }
          codaStr += refPhoneme->to_string(level);
        }
      }
      str += "prominence: " + stressStr + " onset:" + onsetStr + " nucleus: " +
             nucleusStr + " coda: " + codaStr;
      break;
  }
  return str;  // std::string(ss.str().c_str());
}

void Syllable::deflate(RootsStream* stream, bool is_terminal, int list_index) {
  BaseItem::deflate(stream, false, list_index);

  stream->append_unicodestring_content("phoneme_sequence_id",
                                       this->get_phoneme_sequence_id());
  stream->append_int_content(
      "prominent",
      this->get_prominent());  // prominencetagToString[this->get_prominent()]);
  stream->append_int_content("prosodic_class", this->get_prosodic_class());

  stream->append_vector_int_content("onset_index_array", this->_onset);
  stream->append_vector_int_content("nucleus_index_array", this->_nucleus);
  stream->append_vector_int_content("coda_index_array", this->_coda);

  if (is_terminal) stream->close_object();
}

bool Syllable::in_coda(int phoneme_index) {
  for (unsigned int i = 0; i < this->_coda.size(); i++)
    if (_coda[i] == phoneme_index) return true;

  return false;
}

bool Syllable::in_nucleus(int phoneme_index) {
  for (unsigned int i = 0; i < this->_nucleus.size(); i++)
    if (_nucleus[i] == phoneme_index) return true;

  return false;
}

bool Syllable::in_onset(int phoneme_index) {
  for (unsigned int i = 0; i < this->_onset.size(); i++)
    if (_onset[i] == phoneme_index) return true;

  return false;
}

bool Syllable::in_coda(const Phoneme* ph) {
  if (!ph->is_in_sequence()) {
    return false;
  } else {
    if (!has_related_sequence()) {
      std::stringstream ss;
      ss << "Syllable::in_coda(): No phoneme sequence attached to the current "
            "syllable.\n";
      std::cerr << ss.str().c_str() << std::endl;
      throw RootsException(__FILE__, __LINE__, ss.str().c_str());
    } else {
      return ((ph->get_in_sequence() == get_phoneme_sequence()) &&
              in_coda(ph->get_in_sequence_index()));
    }
  }
}

bool Syllable::in_nucleus(const Phoneme* ph) {
  if (!ph->is_in_sequence()) {
    return false;
  } else {
    if (!has_related_sequence()) {
      std::stringstream ss;
      ss << "Syllable::in_nucleus(): No phoneme sequence attached to the "
            "current syllable.\n";
      std::cerr << ss.str().c_str() << std::endl;
      throw RootsException(__FILE__, __LINE__, ss.str().c_str());
    } else {
      return ((ph->get_in_sequence() == get_phoneme_sequence()) &&
              in_nucleus(ph->get_in_sequence_index()));
    }
  }
}

bool Syllable::in_onset(const Phoneme* ph) {
  if (!ph->is_in_sequence()) {
    return false;
  } else {
    if (!has_related_sequence()) {
      std::stringstream ss;
      ss << "Syllable::in_onset(): No phoneme sequence attached to the current "
            "syllable.\n";
      std::cerr << ss.str().c_str() << std::endl;
      throw RootsException(__FILE__, __LINE__, ss.str().c_str());
    } else {
      return ((ph->get_in_sequence() == get_phoneme_sequence()) &&
              in_onset(ph->get_in_sequence_index()));
    }
  }
}

bool Syllable::is_stressed() const {
  if (is_prominent()) {
    return true;
  }
  std::vector<int> indices;

  // Onset
  indices = get_onset_indices();
  for (std::vector<int>::iterator it = indices.begin(); it != indices.end();
       ++it) {
    if (get_phoneme_sequence()->get_item(*it)->is_stressed()) {
      return true;
    }
  }

  // Nucleus
  indices = get_nucleus_indices();
  for (std::vector<int>::iterator it = indices.begin(); it != indices.end();
       ++it) {
    if (get_phoneme_sequence()->get_item(*it)->is_stressed()) {
      return true;
    }
  }

  // Coda
  indices = get_coda_indices();
  for (std::vector<int>::iterator it = indices.begin(); it != indices.end();
       ++it) {
    if (get_phoneme_sequence()->get_item(*it)->is_stressed()) {
      return true;
    }
  }

  return false;
}

ipa::StressLevel Syllable::get_stress_level() const {
  if (is_prominent()) {
    return get_prominent();
  }

  ipa::StressLevel stress_level = ipa::STRESS_LEVEL_NONE;
  std::vector<int> indices;

  // Onset
  indices = get_onset_indices();
  for (std::vector<int>::iterator it = indices.begin(); it != indices.end();
       ++it) {
    stress_level =
        std::max(stress_level,
                 get_phoneme_sequence()->get_item(*it)->get_stress_level());
  }

  // Nucleus
  indices = get_nucleus_indices();
  for (std::vector<int>::iterator it = indices.begin(); it != indices.end();
       ++it) {
    stress_level =
        std::max(stress_level,
                 get_phoneme_sequence()->get_item(*it)->get_stress_level());
  }

  // Coda
  indices = get_coda_indices();
  for (std::vector<int>::iterator it = indices.begin(); it != indices.end();
       ++it) {
    stress_level =
        std::max(stress_level,
                 get_phoneme_sequence()->get_item(*it)->get_stress_level());
  }

  return stress_level;
}

void Syllable::fetch_phoneme_stress() {
  ipa::StressLevel stress_level = get_prominent();
  std::vector<int> indices;

  // Onset
  indices = get_onset_indices();
  for (std::vector<int>::iterator it = indices.begin(); it != indices.end();
       ++it) {
    if (get_phoneme_sequence()->get_item(*it)->is_stressed()) {
      stress_level =
          std::max(stress_level,
                   get_phoneme_sequence()->get_item(*it)->get_stress_level());
      get_phoneme_sequence()->get_item(*it)->unset_stress();
    }
  }

  // Nucleus
  indices = get_nucleus_indices();
  for (std::vector<int>::iterator it = indices.begin(); it != indices.end();
       ++it) {
    if (get_phoneme_sequence()->get_item(*it)->is_stressed()) {
      stress_level =
          std::max(stress_level,
                   get_phoneme_sequence()->get_item(*it)->get_stress_level());
      get_phoneme_sequence()->get_item(*it)->unset_stress();
    }
  }

  // Coda
  indices = get_coda_indices();
  for (std::vector<int>::iterator it = indices.begin(); it != indices.end();
       ++it) {
    if (get_phoneme_sequence()->get_item(*it)->is_stressed()) {
      stress_level =
          std::max(stress_level,
                   get_phoneme_sequence()->get_item(*it)->get_stress_level());
      get_phoneme_sequence()->get_item(*it)->unset_stress();
    }
  }

  set_prominent(stress_level);
}

void Syllable::inflate(RootsStream* stream, bool is_terminal, int list_index) {
  BaseItem::inflate(stream, false, list_index);

  // stream->open_children();

  std::string id = stream->get_unicodestring_content("phoneme_sequence_id");
  roots::sequence::PhonemeSequence* phonemeSeq =
      (roots::sequence::PhonemeSequence*)stream->get_object_ptr(id);
  if (phonemeSeq == NULL) {
    std::stringstream ss;
    ss << "cannot find sequence with id=" << id
       << " while inflating ProsodicPhrase object!";
    std::cerr << ss.str().c_str() << std::endl;
    throw RootsException(__FILE__, __LINE__, ss.str().c_str());
  }
  this->set_phoneme_sequence(phonemeSeq);

  ProminenceTag promValue = (ProminenceTag)stream->get_int_content("prominent");
  if (prominencetagToString.find(promValue) != prominencetagToString.end()) {
    this->set_prominent(promValue);
  } else {
    this->set_prominent(ipa::STRESS_LEVEL_NONE);
  }

  this->set_prosodic_class(stream->get_int_content("prosodic_class"));

  this->_onset = stream->get_vector_int_content("onset_index_array");
  this->_nucleus = stream->get_vector_int_content("nucleus_index_array");
  this->_coda = stream->get_vector_int_content("coda_index_array");

  if (is_terminal) stream->close_children();
}

Syllable* Syllable::inflate_object(RootsStream* stream, int list_index) {
  Syllable* t = new Syllable(true);
  t->inflate(stream, true, list_index);
  return t;
}
}
}
}
