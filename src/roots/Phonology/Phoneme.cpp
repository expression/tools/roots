/*
    This file is part of Roots.

    Roots is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Roots is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Roots.  If not, see <http://www.gnu.org/licenses/>.

    Copyright (c) 2013, 2014 IRISA
*/


#include "Phoneme.h"

#include "../Align/Alignment.h"

#include "Ipa/Alphabet.h"

namespace roots
{

  namespace phonology
  {

    Phoneme::Phoneme(const bool noInit):BaseItem(noInit), ipa()
    {
      alphabet = NULL;
    }

    
    Phoneme::Phoneme( const roots::phonology::ipa::Ipa& ipa,
		      ipa::Alphabet* alphabet)
    {
      this->ipa = std::vector<ipa::Ipa*>();
      this->alphabet = NULL;
      set_ipa(ipa, 0, false);
      set_alphabet(alphabet);
    }

    
    Phoneme::Phoneme( const roots::phonology::ipa::Ipa& ipa,
		      const roots::phonology::ipa::Ipa& ipa2,
		      ipa::Alphabet* alphabet)
    {
      this->ipa = std::vector<ipa::Ipa*>();
      this->alphabet = NULL;
      set_ipa(ipa, 0, false);
      set_ipa(ipa2, 1, false);
      set_alphabet(alphabet);
    }

    
    Phoneme::Phoneme( const std::vector<ipa::Ipa*> & ipas, ipa::Alphabet* alphabet)
    {
      unsigned int cpt=0;
      this->ipa = std::vector<ipa::Ipa*>();
      this->alphabet = NULL;

      for(std::vector<ipa::Ipa*>::const_iterator iter=ipas.begin();
	  iter!=ipas.end();
	  ++iter, ++cpt)
	{
	  set_ipa(**iter, cpt, false);
	}

      set_alphabet(alphabet);
    }

    
    Phoneme::Phoneme( const std::vector<ipa::Ipa*> * ipa, ipa::Alphabet* alphabet)
    {
      unsigned int cpt=0;
      this->ipa = std::vector<ipa::Ipa*>();
      this->alphabet = NULL;

      for(std::vector<ipa::Ipa*>::const_iterator iter=ipa->begin();
	  iter!=ipa->end();
	  ++iter, ++cpt)
	{
	  set_ipa(**iter, cpt, false);
	}

      set_alphabet(alphabet);
    }

    
    Phoneme::Phoneme(const Phoneme& ph) : BaseItem(ph)
    {
      this->ipa = std::vector<ipa::Ipa*>();
      alphabet = NULL;

      for(int cpt=0; cpt<ph.get_n_ipa(); ++cpt)
	{
	  set_ipa(ph.get_ipa(cpt), cpt, false);
	}

      set_alphabet(ph.get_alphabet(), false);
      set_label(ph.get_label());
    }

    
    Phoneme::~Phoneme()
    {
      for(std::vector<ipa::Ipa*>::const_iterator iter=ipa.begin(); iter!=ipa.end(); ++iter)
	{
	  delete *iter;
	}
      alphabet = NULL;
    }

    
    Phoneme *Phoneme::clone() const
    {
      return new Phoneme(*this);
    }

    
    Phoneme& Phoneme::operator= (const Phoneme &ph)
    {
      BaseItem::operator=(ph);

      for(int cpt=0; cpt<ph.get_n_ipa(); ++cpt)
	{
	  set_ipa(ph.get_ipa(cpt), cpt, false);
	}

      set_alphabet(ph.get_alphabet());

      return *this;
    }

    
    double Phoneme::compute_dissimilarity ( const roots::Base* b ) const
    {
      if (const Phoneme *ph = dynamic_cast<const Phoneme *>(b)) {
	// This is just a quick levenshtein on vectors of IPAs
	const size_t len1 = ipa.size(), len2 = ph->ipa.size();
	std::vector<std::vector<double> > d(len1 + 1, std::vector<double>(len2 + 1));
	double subst_cost = Base::MAX_DISSIMILARITY;
	double ins_del_cost = Base::MAX_DISSIMILARITY;

	d[0][0] = 0;

	for(unsigned int i = 1; i <= len1; ++i) { d[i][0] = i; }
	for(unsigned int i = 1; i <= len2; ++i) { d[0][i] = i; }

	for(unsigned int i = 1; i <= len1; ++i) {
	  for(unsigned int j = 1; j <= len2; ++j) {
	    subst_cost = ipa[i-1]->compute_dissimilarity(ph->ipa[j-1]);
	    d[i][j] = std::min( std::min(d[i-1][j] + ins_del_cost,d[i][j-1] + ins_del_cost),
				d[i-1][j-1] + subst_cost );
	  }
	}
          return (d[len1][len2]/std::max(len1, len2));
      }
      else {
	return ((Base *)this)->compute_dissimilarity(b);
      }
    }

        
    double Phoneme::compute_tolerant_dissimilarity ( const roots::Base* b ) const
    {
      
      if (const Phoneme *ph = dynamic_cast<const Phoneme *>(b)) {
	Phoneme ph1  = *this;
	Phoneme ph2 = *ph;
	// This is just a quick levenshtein on vectors of IPAs
	size_t len1 = ph1.ipa.size(), len2 = ph2.ipa.size();
	double subst_cost = Base::MAX_DISSIMILARITY;
	double ins_del_cost = Base::MAX_DISSIMILARITY;

// 	// Move all diacritics in separate IPAs
// 	ipa::Ipa diacs1;
// 	for(unsigned int i = 0; i < len1; i++) {
// 	  diacs1.add_diacritics(ph1.get_ipa(i).get_diacritics_set());
// 	  ph1.unsafe_get_ipa(i)->remove_all_diacritics();
// 	}
// 	if (!diacs1.get_diacritics_set().empty()) {
// 	  ph1.add_ipa(diacs1);
// 	  len1++;
// 	}
// 	
// 	ipa::Ipa diacs2;
// 	for(unsigned int i = 0; i < len2; i++) {
// 	  diacs2.add_diacritics(ph2.get_ipa(i).get_diacritics_set());
// 	  ph2.unsafe_get_ipa(i)->remove_all_diacritics();
// 	}
// 	if (!diacs2.get_diacritics_set().empty()) {
// 	  ph2.add_ipa(diacs2);
// 	  len2++;
// 	}
	
	// Shift all diacritics on the last IPAs
	ipa::Ipa diacs1;
	for(unsigned int i = 0; i < len1; i++) {
	  ph1.unsafe_get_ipa(len1-1)->add_diacritics(ph1.get_ipa(i).get_diacritics_set());
	  if (i != len1-1) {
	    ph1.unsafe_get_ipa(i)->remove_all_diacritics();
	  }
	}
	
	ipa::Ipa diacs2;
	for(unsigned int i = 0; i < len2; i++) {
	  ph2.unsafe_get_ipa(len2-1)->add_diacritics(ph2.get_ipa(i).get_diacritics_set());
	  if (i != len2-1) {
	    ph2.unsafe_get_ipa(i)->remove_all_diacritics();
	  }
	}
	
	
	// Initialize the distance matrix
	std::vector<std::vector<double> > d(len1 + 1, std::vector<double>(len2 + 1));
	d[0][0] = 0;
	for(unsigned int i = 1; i <= len1; ++i) { d[i][0] = i; }
	for(unsigned int i = 1; i <= len2; ++i) { d[0][i] = i; }

	// Compute distances
	for(unsigned int i = 1; i <= len1; ++i) {
	  for(unsigned int j = 1; j <= len2; ++j) {
	    subst_cost = ph1.ipa[i-1]->compute_dissimilarity(ph2.ipa[j-1]);
	    d[i][j] = std::min( std::min(d[i-1][j] + ins_del_cost,d[i][j-1] + ins_del_cost),
				d[i-1][j-1] + subst_cost );
	  }
	}
	
	// Return the normalized accumulated distance
	return (d[len1][len2]/std::max(len1, len2));
      }
      else {
	return ((Base *)this)->compute_dissimilarity(b);
      }
    }


    std::vector< Base* >* Phoneme::split()
    {
      std::vector< Base* > *subitems = new std::vector< Base* >();
      size_t n = ipa.size();
      for (size_t i = 0; i < n; i++) {
	subitems->push_back(ipa[i]->clone());
      }
      return subitems;
    }


    void Phoneme::update_label()
    {
      BaseItem::set_label (this->to_string(0));
    }

    void Phoneme::delete_nphtong(unsigned int n, bool pack)
    {
      if(ipa.size()>n && ipa[n]!=NULL)
	{
	  delete ipa[n];
	  ipa[n] = NULL;
	  if(pack)
	    {
	      ipa.erase(ipa.begin() + n);
	    }
	}
    }

    bool Phoneme::has_alphabet() const
    {
      return alphabet != NULL;
    }

    ipa::Alphabet * Phoneme::get_alphabet() const
    {
      return alphabet;
    }

    void Phoneme::set_alphabet(ipa::Alphabet* alphabet, bool updateLabel)
    {
      this->alphabet = alphabet;
      if(updateLabel){update_label();}
    }


    void Phoneme::set_ipa(const ipa::Ipa& i, unsigned int n, bool updateLabel)
    {
      delete_nphtong(n, false);
      ipa::Ipa *ii = i.clone();

      if(ipa.size() > n)
	ipa[n] = ii;
      else
	ipa.push_back(ii);

      if(updateLabel){update_label();}
    }

    void Phoneme::add_ipa(const ipa::Ipa & new_ipa, bool updateLabel)
    {
      ipa.push_back(new_ipa.clone());
      if (updateLabel) { update_label(); }
    }

    void Phoneme::clear(bool updateLabel)
    {
      size_t n = ipa.size();
      for (size_t i = 0; i < n; i++) {
	ipa[i]->destroy();
      }
      ipa.clear();
      if (updateLabel) { update_label(); }
    }

    const ipa::Ipa& Phoneme::get_ipa(unsigned int n) const
    {
      /// TODO : n is unsigned	so useless
      /*
	if (n < 0) {
	n = ipa.size() + n;
	}
      */
      if(ipa.size()<n+1 || ipa[n] == NULL)
	{throw RootsException(__FILE__, __LINE__, "ipa attribute is not initialised!");}
      return *ipa[n];
    }

    std::vector< ipa::Ipa* > Phoneme::get_all_ipas() const
    {
      return ipa;
    }


    ipa::Ipa* Phoneme::unsafe_get_ipa( unsigned int n ) const
    {
      ipa::Ipa *i = NULL;
      if(ipa.size()>n) i = ipa[n];
      return i;
    }

    void Phoneme::unsafe_set_ipa( const roots::phonology::ipa::Ipa* ipa2, unsigned int n, bool updateLabel )
    {
      delete_nphtong(false);
      ipa[n] = ipa2->clone();
      if(updateLabel){update_label();}
    }

    UChar Phoneme::get_ipa_unicode(unsigned int n) const
    {
      if(ipa.size()<n+1 || ipa[n] == NULL)
	{throw RootsException(__FILE__, __LINE__, "ipa2 attribute is not initialised!");}
      return ipa[n]->get_unicode();
    }

    bool Phoneme::is_a(const ipa::Category category) const
    {
      bool result = false;
      for(std::vector<ipa::Ipa*>::const_iterator iter=ipa.begin(); iter!=ipa.end(); ++iter)
	{
	  if(*iter != NULL)
	    {
	      result = result || (*iter)->is_a(category);
	    }
	}

      return result;
    }

    bool Phoneme::is_one_of(const std::vector<ipa::Category>& categories) const
    {
      bool result = false;
      for(std::vector<ipa::Ipa*>::const_iterator iter=ipa.begin(); iter!=ipa.end(); ++iter)
	{
	  if(*iter != NULL)
	    {
	      result = result || (*iter)->is_one_of(categories);
	    }
	}

      return result;
    }

    bool Phoneme::is_all_of(const std::vector<ipa::Category>& categories) const
    {
      bool result = true;
      for(std::vector<ipa::Ipa*>::const_iterator iter=ipa.begin(); iter!=ipa.end(); ++iter)
	{
	  if(*iter != NULL)
	    {
	      result = result && (*iter)->is_all_of(categories);
	    }
	}

      return result && (ipa.size()!=0);
    }


    bool Phoneme::is_stressed() const
    {
      size_t n = get_n_ipa();
      for (size_t i = 0; i < n; i++) {
	if (get_ipa(i).is_stressed()) { return true; }
      }
      return false;
    }

    ipa::StressLevel Phoneme::get_stress_level() const
    {
      size_t n = get_n_ipa();
      ipa::StressLevel stress_level = ipa::STRESS_LEVEL_NONE;
      for (size_t i = 0; i < n; i++) {
	stress_level = std::max(stress_level, get_ipa(i).get_stress_level());
      }
      return stress_level;
    }

    void Phoneme::unset_stress()
    {
      size_t n = get_n_ipa();
      for (size_t i = 0; i < n; i++) {
	ipa[i]->unset_stress();
      }
    }



    std::string Phoneme::to_string(int level) const
    {
      int ipaLevel = std::max(level -1 , 0);

      if(level == 0)
	{
	  if(has_alphabet())
	    { return get_alphabet()->convert_phoneme(*this); }
	  ipaLevel = level;
	}

    std::stringstream ss;
      for(std::vector<ipa::Ipa*>::const_iterator iter=ipa.begin(); iter!=ipa.end(); ++iter)
	{
	  ss << (*iter)->to_string(ipaLevel);
	}

      return ss.str();
    }

    void Phoneme::deflate (RootsStream * stream, bool is_terminal, int list_index)
    {
      BaseItem::deflate(stream,false,list_index);

      if(ipa.size()==0 || ipa[0] == NULL) {throw RootsException(__FILE__, __LINE__, "ipa attribute is not initialised!");}

      if(is_monophthong())
	{ipa[0]->deflate(stream);}
      else
	{
	  unsigned int cpt=0;
	  for(std::vector<ipa::Ipa*>::const_iterator iter=ipa.begin(); iter!=ipa.end(); ++iter)
	    {
	      if(*iter != NULL)
		{
		  (*iter)->deflate(stream, true, cpt);
		  ++cpt;
		}
	    }
	}

      if(has_alphabet())
	{ stream->append_unicodestring_content("alphabet", this->get_alphabet()->get_xml_tag_name()); }

      if(is_terminal)
	{stream->close_object();}
    }


    void Phoneme::inflate(RootsStream * stream, bool is_terminal, int list_index)
    {
      std::string ipaClassname;

      BaseItem::inflate(stream,false,list_index);

      for(std::vector<ipa::Ipa*>::const_iterator iter=ipa.begin(); iter!=ipa.end(); ++iter)
	{
	  delete *iter;
	}


      alphabet = NULL;
      /* Alphabet factory */
      if(stream->has_child("alphabet") )
	{
	  std::string tmpAlph = stream->get_unicodestring_content("alphabet");
	  alphabet = roots::phonology::ipa::Alphabet::get_alphabet_from_xml_tag(tmpAlph);

	  /*if(alphabet==NULL)
	    {
	      //throw RootsException(__FILE__, __LINE__, "Unknown alphabet! <"+tmpAlph+">");
			std::clog << "warning: " << __FILE__ << ":" << __LINE__ << ": Unknown alphabet! <" << tmpAlph << ">";
	    }*/
	}
      /// TODO : Old version for compatibility : must be removed !
      /* Alphabet factory */
      if( stream->has_child("Alphabet") )
	{
	  std::string tmpAlph = stream->get_unicodestring_content("Alphabet");
	  alphabet = roots::phonology::ipa::Alphabet::get_alphabet_from_xml_tag(tmpAlph);

	  /*if(alphabet==NULL)
	    {
	      //throw RootsException(__FILE__, __LINE__, "Unknown alphabet! <"+tmpAlph+">");
			std::clog << "warning: " << __FILE__ << ":" << __LINE__ << ": Unknown alphabet! <" << tmpAlph << ">";
	    }*/
	}

      /* IPA factory */
      unsigned int n_children = stream->get_n_children(roots::phonology::ipa::Ipa::xml_tag_name());
      if(n_children == 1)
	{
	  ipa.push_back(roots::phonology::ipa::Ipa::inflate_object(stream));
	}
      else
	{
	  for(int cpt=0; cpt< (int)n_children; ++cpt)
	    {
	      ipa.push_back(roots::phonology::ipa::Ipa::inflate_object(stream, cpt));
	    }
	}
      if(is_terminal) stream->close_children();
    }

    Phoneme * Phoneme::inflate_object(RootsStream * stream, int list_index)
    {
      Phoneme *t = new Phoneme(true);
      t->inflate(stream,true,list_index);
      return t;
    }

  }
} // END OF NAMESPACES
