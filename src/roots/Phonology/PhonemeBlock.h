/*
    This file is part of Roots.

    Roots is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Roots is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Roots.  If not, see <http://www.gnu.org/licenses/>.

    Copyright (c) 2013, 2014 IRISA
*/

#ifndef PHONEME_BLOCK_H_
#define PHONEME_BLOCK_H_

#include "../BaseItem.h"
#include "../RootsException.h"
#include "../Sequence/PhonemeSequence.h"
#include "../common.h"

namespace roots {
namespace phonology {

const uint32_t PHONEME_BLOCK_CONTEXT_EMPTY = 0x0000;
const uint32_t PHONEME_BLOCK_CONTEXT_CONSONANT = 0x0001;
const uint32_t PHONEME_BLOCK_CONTEXT_NO_CONSONANT = 0x0002;
const uint32_t PHONEME_BLOCK_CONTEXT_END_OF_SENTENCE = 0x0004;
const uint32_t PHONEME_BLOCK_CONTEXT_NO_CONSONANT_CONSONANT = 0x0008;
const uint32_t PHONEME_BLOCK_CONTEXT_NO_NASAL = 0x0010;
const uint32_t PHONEME_BLOCK_CONTEXT_NASAL = 0x0020;
const uint32_t PHONEME_BLOCK_CONTEXT_OPEN_SYLLABLE = 0x0040;
const uint32_t PHONEME_BLOCK_CONTEXT_CLOSED_SYLLABLE = 0x0080;
const uint32_t PHONEME_BLOCK_CONTEXT_NO_CONSONANT_NO_CONSONANT = 0x0100;
const uint32_t PHONEME_BLOCK_CONTEXT_NO_LIAISON_AFTER = 0x0200;
const uint32_t PHONEME_BLOCK_CONTEXT_ALL = 0xFFFFFFFF;

class PhonemeBlock : public roots::BaseItem {
 public:
  /**
   * @brief Default constructor
   */
  PhonemeBlock(const bool noInit = false);

  /**
  * @brief Copy constructor
  * @details This constructor does not copy links to relations or sequence.
  * @param ph
  */
  PhonemeBlock(const PhonemeBlock &ph);

  /**
   * @brief Destructor
   * @todo check
   */
  virtual ~PhonemeBlock();

  /**
   * @brief Duplicates the phoneme
   * @return A clone of the phoneme
   */
  virtual PhonemeBlock *clone() const;

  /**
   * @brief Copy phoneme block features into the current phoneme block
   * @details Links to relations or sequence are copied.
   * @param ph
   * @return reference to the current phoneme block
   */
  PhonemeBlock &operator=(const PhonemeBlock &ph);

  /**
   * @brief Returns the phoneme sequence pointer
   * @return the phoneme sequence pointer
   */
  sequence::PhonemeSequence *get_phoneme_sequence() const;

  /**
   * @brief Sets the phoneme sequence pointer
   * @param s the phoneme sequence pointer
   */
  void set_phoneme_sequence(sequence::PhonemeSequence *s);

  /**
  * @brief Says if the item has a related sequence
  * @return Is the item has a related sequence ?
  */
  virtual bool has_related_sequence() const;

  /**
  * @brief Set the related phoneme sequence of the base item.
  * @param A pointer to the related sequence.
  */
  virtual void set_related_sequence(sequence::Sequence *_relatedSequence);

  /**
  * @brief Get the item related phoneme sequence (if any)
  * @return A pointer to the related phoneme sequence if any, NULL elsewhere.
  */
  virtual sequence::Sequence *get_related_sequence() const;

  /**
* @brief Modifies the current syllable to take into account the insertion of an
*element into the related sequence
* @param index index of the element added in the related sequence
**/
  virtual void insert_related_element(int index);

  /**
   * @brief Modifies the current syllable to take into account the deletion of
   *an element into the related sequence
   * @param index index of the element removed from the related sequence
   **/
  virtual void remove_related_element(int index);

  /**
   * @brief Returns the start index in the target phoneme sequence
   * @return the start index in the phoneme sequence
   */
  virtual int get_start_index() const;

  /**
   * @brief Returns the end index in the target phoneme sequence
   * @return the end index in the phoneme sequence
   */
  virtual int get_end_index() const;

  /**
   * @brief Sets the start index in the target phoneme sequence
   * @param i the new start index in the phoneme sequence
   */
  virtual void set_start_index(int i);

  /**
   * @brief Sets the end index in the target phoneme sequence
   * @param i the new end index in the phoneme sequence
   */
  virtual void set_end_index(int i);

  /**
   * @brief Returns the number of phonemes in the phoneme block
   * @return an integer, 0 if the block is empty
   */
  unsigned int count() const;

  /**
   * @brief Returns the current optional vector
   * @return the optional vector
   */
  virtual const std::vector<bool> &get_optional() const;
  /**
   * @brief Returns true if the phoneme at given index is optional
   * @param index
   * @return true or false
   * @throw OutOfBoundIndexException if index is out of bounds
   */
  virtual bool is_optional(int index) const;
  /**
   * @brief Sets the current optional vector
   * @return the optional vector
   */
  virtual void set_optional(const std::vector<bool> &);
  /**
   * @brief Sets the optional for the corresponding phoneme
   * @param index
   * @param value true or false
   * @throw OutOfBoundIndexException if index is out of bounds
   */
  virtual void set_optional(int index, bool value);

  /**
   * @brief Returns the indicator about forbidden liaison before the current
   * phoneme block
   * @return true or false
   */
  virtual bool is_forbidden_liaison_before() const;

  /**
   * @brief Sets the indicator about forbidden liaison before the current
   * phoneme block
   * @param b true if a liaison is forbidden before the phoneme block. Default
   * argument is true.
   */
  virtual void set_forbidden_liaison_before(bool b = true);

  /**
   * @brief Returns the indicator for potential liaison after the current
   * phoneme block
   * @return true or false
   */
  virtual bool is_possible_liaison_after() const;

  /**
   * @brief Sets the indicator for potential liaison after the current phoneme
   * block
   * @param b true if a liaison is possible after the phoneme block
   */
  virtual void set_possible_liaison_after(bool b);

  /**
   * @brief Returns the left context constraint of the phoneme block
   * @return the left context
   */
  virtual uint32_t get_left_context() const;

  /**
   * @brief Returns the right context constraint of the phoneme block
   * @return the right context
   */
  virtual uint32_t get_right_context() const;

  /**
   * @brief Returns all left context features independtly in one vector
   * @return a vector of context codes (uint32)
   **/
  virtual std::vector<uint32_t> get_left_context_vector() const;

  /**
   * @brief Returns all right context features independtly in one vector
   * @return a vector of context codes (uint32)
   **/
  virtual std::vector<uint32_t> get_right_context_vector() const;

  /**
   * @brief Sets the left context constraint of the phoneme block
   * @param ctxg new left context value
   */
  virtual void set_left_context(uint32_t ctxg);

  /**
   * @brief Sets the right context constraint of the phoneme block
   * @param ctxd new right context value
   */
  virtual void set_right_context(uint32_t ctxd);

  /**
   * @brief Adds the left context constraint to the phoneme block
   * @param ctxg_value new left context value
   */
  virtual void add_left_context(uint32_t ctxg_value);

  /**
   * @brief Adds the right context constraint to the phoneme block
   * @param ctxd_value new right context value
   */
  virtual void add_right_context(uint32_t ctxd_value);

  /**
   * @brief Adds the left context constraint to the phoneme block
   * @param ctxg_value new left context string value
   */
  virtual void add_left_context(const std::string &ctxg_str);

  /**
   * @brief Adds the right context constraint to the phoneme block
   * @param ctxd_value new right context string value
   */
  virtual void add_right_context(const std::string &ctxd_str);

  /**
   * @brief Removes the left context constraint from the phoneme block
   * @param ctxg_value new left context value
   */
  virtual void remove_left_context(uint32_t ctxg_value);

  /**
   * @brief Removes the right context constraint from the phoneme block
   * @param ctxd_value new right context value
   */
  virtual void remove_right_context(uint32_t ctxd_value);

  /**
   * @brief Transforms a context value to a string
   * @param value context value
   */
  std::string contextToString(uint32_t value) const;

  virtual std::string to_string(int level = 0) const;

  /**
   * @brief Write the entity into a RootsStream
   * @param stream the stream from which we inflate the element
   * @param is_terminal indicates that the node is terminal (true by default)
   * @param parentNode
   */
  virtual void deflate(RootsStream *stream, bool is_terminal = true,
                       int list_index = -1);
  /**
   * @brief Extracts the entity from the stream
   * @param stream the stream from which we inflate the element
   */
  virtual void inflate(RootsStream *stream, bool is_terminal = true,
                       int list_index = -1);
  /**
   * @brief Extracts the entity from the stream
   * @param stream the stream from which we inflate the element
   */
  static PhonemeBlock *inflate_object(RootsStream *stream, int list_index = -1);
  /**
   * @brief returns the XML tag name value for current object
   * @return string constant representing the XML tag name
   */
  virtual std::string get_xml_tag_name() const { return xml_tag_name(); };
  /**
   * @brief returns the XML tag name value for current class
   * @return string constant representing the XML tag name
   */
  static std::string xml_tag_name() { return "phonemeblock"; };
  /**
   * @brief returns classname for current object
   * @return string constant representing the classname
   */
  virtual std::string get_classname() const { return classname(); };
  /**
   * @brief returns classname for current class
   * @return string constant representing the classname
   */
  static std::string classname() { return "Phonology::PhonemeBlock"; };
  /**
   * @brief returns display color for current object
   * @return string constant representing the pgf display color
   */
  virtual std::string get_pgf_display_color() const {
    return pgf_display_color();
  };
  /**
   * @brief returns display color for current class
   * @return string constant representing the pgf display color
   */
  static std::string pgf_display_color() { return "red!25"; };

 protected:
  sequence::PhonemeSequence *phonemeSequence;
  int startIndex;
  int endIndex;
  std::vector<bool> optional;
  bool forbiddenLiaisonBefore;
  bool possibleLiaisonAfter;
  uint32_t leftContext;
  uint32_t rightContext;

  static std::map<unsigned int, std::string> contextToStringMap;
  static std::map<std::string, unsigned int> stringToContextMap;
};
}
} // END OF NAMESPACES

#endif /* PHONEME_H_ */
