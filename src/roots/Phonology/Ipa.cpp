/*
    This file is part of Roots.

    Roots is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Roots is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Roots.  If not, see <http://www.gnu.org/licenses/>.

    Copyright (c) 2013, 2014 IRISA
*/

#include "Ipa.h"
#include <set>
#include "Phoneme.h"
#include "md5.h"

using namespace icu;

namespace roots {
namespace phonology {
namespace ipa {

std::map<UChar, Ipa> Ipa::unicodeToIpaMap = std::map<UChar, Ipa>();
std::map<std::string, UChar> Ipa::ipaToUnicodeMap =
    std::map<std::string, UChar>();

/****************************************************************************
 * BaseItem part
 ***************************************************************************/

Ipa::Ipa(const bool noInit)
    : BaseItem(noInit),
      Alphabet("ipa"),
      manner(MANNER_UNDEF),
      place(PLACE_UNDEF),
      aperture(APERTURE_UNDEF),
      shape(SHAPE_UNDEF),
      vowelClassification(CLASSIFICATION_VOWEL_BACKNESS),
      consonantClassification(CLASSIFICATION_CONSONANT_MANNER),
      is_affricate(false),
      is_voiced(false),
      is_rounded(false),
      is_double(false),
      ipa_changed(true),
      unicode(0) {
  set_label("");
  diacritic = std::set<Diacritic>();
  unicode_diacritic = std::set<UChar>();
}

Ipa::Ipa(Manner manner, Place place, Aperture aperture, Shape shape,
         bool is_affricate, bool is_voiced, bool is_rounded, bool is_double,
         const std::vector<Diacritic>& diacritic)
    : Alphabet("ipa"),
      manner(manner),
      place(place),
      aperture(aperture),
      shape(shape),
      is_affricate(is_affricate),
      is_voiced(is_voiced),
      is_rounded(is_rounded),
      is_double(is_double),
      ipa_changed(true) {
  set_label("");
  this->diacritic = std::set<Diacritic>(diacritic.begin(), diacritic.end());
  this->unicode_diacritic = std::set<UChar>();
  clear_unicode();
  set_vowel_classification(CLASSIFICATION_VOWEL_BACKNESS);
  set_consonant_classification(CLASSIFICATION_CONSONANT_MANNER);
}

Ipa::Ipa(Manner manner, Place place, Aperture aperture, Shape shape,
         bool is_affricate, bool is_voiced, bool is_rounded, bool is_double,
         const std::set<Diacritic>& diacritic)
    : Alphabet("ipa"),
      manner(manner),
      place(place),
      aperture(aperture),
      shape(shape),
      is_affricate(is_affricate),
      is_voiced(is_voiced),
      is_rounded(is_rounded),
      is_double(is_double),
      ipa_changed(true) {
  set_label("");
  this->diacritic = diacritic;
  this->unicode_diacritic = std::set<UChar>();
  clear_unicode();
  set_vowel_classification(CLASSIFICATION_VOWEL_BACKNESS);
  set_consonant_classification(CLASSIFICATION_CONSONANT_MANNER);
}

/**
 * Copy constructor
 * @param ipa
 * @return
 */
Ipa::Ipa(const Ipa& ipa) : BaseItem(ipa), Alphabet("ipa") {
  clear_features();
  clear_unicode();

  set_manner(ipa.get_manner());
  set_place(ipa.get_place());
  set_is_affricate(ipa.get_is_affricate());
  set_is_double(ipa.get_is_double());
  set_is_rounded(ipa.get_is_rounded());
  set_is_voiced(ipa.get_is_voiced());
  set_aperture(ipa.get_aperture());
  set_shape(ipa.get_shape());
  set_diacritics(ipa.get_diacritics());
  set_vowel_classification(ipa.get_vowel_classification());
  set_consonant_classification(ipa.get_consonant_classification());

  unicode = ipa.unicode;
  unicode_diacritic = std::set<UChar>(ipa.unicode_diacritic);
  ipa_changed = ipa.ipa_changed;
}

Ipa::~Ipa() {}

Ipa* Ipa::clone() const { return new Ipa(*this); }

Ipa& Ipa::operator=(const Ipa& ipa) {
  BaseItem::operator=(ipa);

  clear_features();
  clear_unicode();

  set_manner(ipa.get_manner());
  set_place(ipa.get_place());
  set_is_affricate(ipa.get_is_affricate());
  set_is_double(ipa.get_is_double());
  set_is_rounded(ipa.get_is_rounded());
  set_is_voiced(ipa.get_is_voiced());
  set_aperture(ipa.get_aperture());
  set_shape(ipa.get_shape());
  set_diacritics(ipa.get_diacritics());
  set_vowel_classification(ipa.get_vowel_classification());
  set_consonant_classification(ipa.get_consonant_classification());

  ipa_changed = true;
  to_unicode();
  return *this;
}

bool Ipa::operator==(const Ipa& ipa) const {
  return this->hash_feature() == ipa.hash_feature();
}

bool Ipa::operator!=(const Ipa& ipa) const {
  return this->hash_feature() != ipa.hash_feature();
}

float compute_manner_distance(const Ipa* ipa1, const Ipa* ipa2) {
  if (ipa1->get_manner() == ipa2->get_manner())
    return 0.0;
  else if (ipa1->get_manner() != MANNER_UNDEF &&
           ipa2->get_manner() != MANNER_UNDEF) {
    // return std::min(1.0, abs( m1-m2 )/10.0);

    double dist = 1.0;
    if ((ipa1->is_pulmonic() && ipa2->is_pulmonic()) ||
        (ipa1->is_non_pulmonic() && ipa2->is_non_pulmonic())) {
      dist -= 0.5;
    }
    if ((ipa1->is_fricative() && ipa2->is_fricative()) ||
        (ipa1->is_approximant() && ipa2->is_approximant()) ||
        (ipa1->is_flap() && ipa2->is_flap())) {
      dist -= 0.25;
    }

    return dist;
  } else {
    return 1.0;
  }
}

float compute_place_distance(Place p1, Place p2) {
  if (p1 == p2)
    return 0.0;
  else if (p1 != PLACE_UNDEF && p2 != PLACE_UNDEF) {
    if (p1 == PLACE_LABIOPALATAL) {
      return (compute_place_distance(PLACE_BILABIAL, p2) +
              compute_place_distance(PLACE_PALATAL, p2)) /
             2.0;
    }
    if (p2 == PLACE_LABIOPALATAL) {
      return (compute_place_distance(p1, PLACE_BILABIAL) +
              compute_place_distance(p1, PLACE_PALATAL)) /
             2.0;
    }
    if (p1 == PLACE_LABIOVELAR) {
      return (compute_place_distance(PLACE_BILABIAL, p2) +
              compute_place_distance(PLACE_VELAR, p2)) /
             2.0;
    }
    if (p2 == PLACE_LABIOVELAR) {
      return (compute_place_distance(p1, PLACE_BILABIAL) +
              compute_place_distance(p1, PLACE_VELAR)) /
             2.0;
    } else {
      return std::min(1.0, abs(p1 - p2) / 16.0);
    }

  } else {
    return 1.0;
  }
}

float compute_aperture_distance(Aperture a1, Aperture a2) {
  if (a1 == a2)
    return 0.0;
  else if (a1 != APERTURE_UNDEF && a2 != APERTURE_UNDEF) {
    if ((a1 == APERTURE_CLOSE && a2 == APERTURE_NEAR_CLOSE) ||
        (a1 == APERTURE_NEAR_CLOSE && a2 == APERTURE_CLOSE) ||
        (a1 == APERTURE_OPEN && a2 == APERTURE_NEAR_OPEN) ||
        (a1 == APERTURE_NEAR_OPEN && a2 == APERTURE_OPEN)) {
      return 0.1;
    } else {
      return std::min(1.0, abs(a1 - a2) / 6.0);
    }
  } else {
    return 1.0;
  }
}

float compute_shape_distance(Shape s1, Shape s2) {
  if (s1 == s2)
    return 0.0;
  else if (s1 != SHAPE_UNDEF && s2 != SHAPE_UNDEF) {
    return std::min(1.0, abs(s1 - s2) / 4.0);
  } else {
    return 1.0;
  }
}

double Ipa::compute_dissimilarity(const Base* b) const {
  if (const Ipa* ipa = dynamic_cast<const Ipa*>(b)) {
    float sym_distance = Base::MIN_DISSIMILARITY;
    float diac_distance = Base::MIN_DISSIMILARITY;
    float max_symbol_distance = 6.1;

    float manner_dist = compute_manner_distance(
        this,
        ipa);  // Manner is judged as a bit more important in case of equality
    float place_dist = compute_place_distance(place, ipa->get_place());
    float affricate_dist = (is_affricate != ipa->get_is_affricate());
    float voiced_dist = (is_voiced != ipa->get_is_voiced());
    float aperture_dist =
        compute_aperture_distance(aperture, ipa->get_aperture());
    float shape_dist = compute_shape_distance(shape, ipa->get_shape());
    float rounded_dist = 0.67 * (is_rounded != ipa->get_is_rounded());
    float double_dist = (is_double != ipa->get_is_double());

    sym_distance = 2.0 * manner_dist + 1.0 * place_dist + affricate_dist +
                   voiced_dist +
                   sqrt(1.0 * pow(aperture_dist, 2) + 0.8 * pow(shape_dist, 2) +
                        pow(rounded_dist, 2)) +
                   double_dist;

    if ((is_nasal() && !ipa->is_nasal()) || (!is_nasal() && ipa->is_nasal()))
      sym_distance += 1;

    for (std::set<Diacritic>::iterator it = diacritic.begin();
         it != diacritic.end(); ++it) {
      diac_distance += (ipa->is_diacritic(*it) ? 0 : 1);
    }
    for (std::set<Diacritic>::iterator it = ipa->get_diacritics_set().begin();
         it != ipa->get_diacritics_set().end(); ++it) {
      diac_distance += (this->is_diacritic(*it) ? 0 : 1);
    }
    float max_diacritics_distance =
        std::max(diacritic.size(), ipa->get_diacritics_set().size());
    float diacritics_weight = 0.5;
    // Ipa * unconst_this = const_cast<Ipa *>(this);
    // Ipa * unconst_ipa = const_cast<Ipa *>(ipa);
    // cerr << "Dist( " << unconst_this->to_string() << " , " <<
    // unconst_ipa->to_string() << " ) = " <<
    // (((sym_distance+diacritics_weight*diac_distance)/(max_symbol_distance+diacritics_weight*max_diacritics_distance))*Base::MAX_DISSIMILARITY)
    // << "( " << max_symbol_distance << " + " << max_diacritics_distance << "
    // )" << std::endl;
    // fprintf(stderr,
    // "man=%.2f\tpla=%.2f\taff=%.2f\tvoi=%.2f\tape=%.2f\tsha=%.2f\trou=%.2f\tdou=%.2f\n",
    // manner_dist, place_dist, affricate_dist, voiced_dist, aperture_dist,
    // shape_dist, rounded_dist, double_dist);
    // cerr << "sym: " << sym_distance << std::endl;
    // cerr << "diac: " << diac_distance << std::endl;
    return ((sym_distance + diacritics_weight * diac_distance) /
            (max_symbol_distance +
             diacritics_weight * max_diacritics_distance)) *
           Base::MAX_DISSIMILARITY;
  } else {
    return ((Base*)this)->compute_dissimilarity(b);
  }
}

Aperture Ipa::get_aperture() const { return this->aperture; }

std::vector<Diacritic> Ipa::get_diacritics() const {
  return std::vector<Diacritic>(diacritic.begin(), diacritic.end());
}

const std::set<Diacritic>& Ipa::get_diacritics_set() const { return diacritic; }

Diacritic Ipa::get_diacritic_from_unicode(UChar diac) {
  Ipa ipa = unicodeToIpaMap[diac];
  std::vector<Diacritic> diacs = ipa.get_diacritics();
  if (diacs.size() > 0) {
    return diacs[0];
  } else {
    std::stringstream ss;
    ss << ": Unicode " << diac << " is not a proper value for a diacritic!\n"
       << std::endl;
    IpaException e(__FILE__, __LINE__, ss.str());
    throw(e);
  }
}

UChar Ipa::get_unicode_from_diacritic(Diacritic diac) {
  Ipa ipa;
  ipa.set_diacritic(diac);
  std::vector<UChar> uchars = ipa.get_unicode_diacritics();
  if (uchars.size() > 0) {
    return uchars.at(0);
  } else {
    std::stringstream ss;
    ss << "Unknown diacritic " << DIACRITICS[diac] << "(value = " << diac
       << ")!" << std::endl;
    IpaException e(__FILE__, __LINE__, ss.str());
    throw e;
  }
}

Ipa* Ipa::get_ipa_from_diacritic(Diacritic diac) {
  Ipa* ipa = new ipa::Ipa();
  ipa->set_diacritic(diac);
  return ipa;
}

bool Ipa::get_is_affricate() const { return is_affricate; }

bool Ipa::get_is_double() const { return is_double; }

bool Ipa::get_is_rounded() const { return is_rounded; }

bool Ipa::get_is_voiced() const {
  return (is_voiced || is_diacritic(DIACRITIC_VOICED));
}

Manner Ipa::get_manner() const { return manner; }

Place Ipa::get_place() const { return place; }

Shape Ipa::get_shape() const { return shape; }

UChar Ipa::get_unicode() {
  to_unicode();
  return unicode;
}

std::vector<UChar> Ipa::get_unicode_diacritics() {
  to_unicode();
  return std::vector<UChar>(unicode_diacritic.begin(), unicode_diacritic.end());
}

const std::set<UChar>& Ipa::get_unicode_diacritics_set() {
  to_unicode();
  return unicode_diacritic;
}

Classification Ipa::get_vowel_classification() const {
  return vowelClassification;
}

Classification Ipa::get_consonant_classification() const {
  return consonantClassification;
}

void Ipa::set_aperture(Aperture aperture) {
  this->aperture = aperture;
  ipa_changed = true;
}

/**
 * Erase the existing diacritics and add the one given as parameter. To add more
 * diacritics @see{add_diacritic()}
 * @param dia the diacritic to take into account
 */
void Ipa::set_diacritic(Diacritic dia) {
  diacritic.clear();
  diacritic.insert(dia);
  ipa_changed = true;
}

/**
 * Erase the existing diacritics and add the given diacritics. To add more
 * diacritics @see{add_diacritic()}
 * @param dia the diacritic vector to take into account
 */
void Ipa::set_diacritics(
    const std::vector<roots::phonology::ipa::Diacritic>& dia) {
  diacritic =
      std::set<roots::phonology::ipa::Diacritic>(dia.begin(), dia.end());
  ipa_changed = true;
}

void Ipa::set_diacritics(
    const std::set<roots::phonology::ipa::Diacritic>& dia) {
  diacritic = dia;
  ipa_changed = true;
}

void Ipa::set_is_affricate(bool is_affricate) {
  this->is_affricate = is_affricate;
  ipa_changed = true;
}

void Ipa::set_is_double(bool is_double) {
  this->is_double = is_double;
  ipa_changed = true;
}

void Ipa::set_is_rounded(bool is_rounded) {
  this->is_rounded = is_rounded;
  ipa_changed = true;
}

void Ipa::set_is_voiced(bool is_voiced) {
  this->is_voiced = is_voiced;
  ipa_changed = true;
}

void Ipa::set_manner(Manner manner) {
  this->manner = manner;
  ipa_changed = true;
}

void Ipa::set_place(Place place) {
  this->place = place;
  ipa_changed = true;
}

void Ipa::set_shape(Shape shape) {
  this->shape = shape;
  ipa_changed = true;
}

void Ipa::set_unicode(const UChar unicode) { this->unicode = unicode; }

void Ipa::set_unicode_diacritics(const std::vector<UChar>& unicode_diacritic) {
  this->unicode_diacritic =
      std::set<UChar>(unicode_diacritic.begin(), unicode_diacritic.end());
}

void Ipa::set_unicode_diacritics(const std::set<UChar>& unicode_diacritic) {
  this->unicode_diacritic = unicode_diacritic;
}

void Ipa::set_vowel_classification(Classification aClass) {
  this->vowelClassification = aClass;
}

void Ipa::set_consonant_classification(Classification aClass) {
  this->consonantClassification = aClass;
}

/**
 * Add a diacritic to the existing ones for the current Ipa object
 * @param dia the diacritic to add
 */
void Ipa::add_diacritic(Diacritic dia) {
  diacritic.insert(dia);
  ipa_changed = true;
}

/**
 * Add diacritics to the existing ones for the current Ipa object
 * @param dia the diacritic vector to add
 */
void Ipa::add_diacritics(const std::vector<Diacritic>& dia) {
  for (std::vector<Diacritic>::const_iterator it = dia.begin(); it != dia.end();
       ++it) {
    add_diacritic(*it);
  }
  ipa_changed = true;
}

void Ipa::add_diacritics(const std::set<Diacritic>& dia) {
  for (std::set<Diacritic>::const_iterator it = dia.begin(); it != dia.end();
       ++it) {
    add_diacritic(*it);
  }
  ipa_changed = true;
}

bool Ipa::remove_diacritic(Diacritic dia) {
  ipa_changed = true;
  return (diacritic.erase(dia) > 0);
}

void Ipa::remove_all_diacritics() {
  ipa_changed = true;
  diacritic.clear();
}

bool Ipa::is_diacritic() const {
  return (diacritic.size() > 0) && (get_manner() == MANNER_UNDEF) &&
         (get_place() == PLACE_UNDEF) && (get_aperture() == APERTURE_UNDEF) &&
         (get_shape() == SHAPE_UNDEF) && (!get_is_affricate()) &&
         (!get_is_voiced()) && (!get_is_rounded()) && (!get_is_double());
}

bool Ipa::is_preceding_diacritic() const {
  return is_diacritic() && is_preceding_diacritic_static(*(diacritic.begin()));
}

bool Ipa::is_following_diacritic() const {
  return is_diacritic() && is_following_diacritic_static(*(diacritic.begin()));
}

bool Ipa::is_diacritic(Diacritic dia) const {
  std::set<Diacritic>::iterator iter = diacritic.find(dia);
  return (iter != diacritic.end());
}

bool Ipa::is_diacritic_static(UChar u) {
  return (Ipa::unicodeToIpaMap.find(u) != Ipa::unicodeToIpaMap.end()) &&
         Ipa::unicodeToIpaMap[u].is_diacritic();
}

bool Ipa::is_preceding_diacritic_static(UChar u) {
  return (Ipa::unicodeToIpaMap.find(u) != Ipa::unicodeToIpaMap.end()) &&
         Ipa::unicodeToIpaMap[u].is_preceding_diacritic();
}

bool Ipa::is_following_diacritic_static(UChar u) {
  return (Ipa::unicodeToIpaMap.find(u) != Ipa::unicodeToIpaMap.end()) &&
         Ipa::unicodeToIpaMap[u].is_following_diacritic();
}

bool Ipa::is_preceding_diacritic_static(Diacritic dia) {
  return ((dia == DIACRITIC_PRIMARY_STRESS) ||
          (dia == DIACRITIC_SECONDARY_STRESS));
}

bool Ipa::is_following_diacritic_static(Diacritic dia) {
  return !((dia == DIACRITIC_PRIMARY_STRESS) ||
           (dia == DIACRITIC_SECONDARY_STRESS));
}

Ipa Ipa::unicode_to_ipa(const UChar& symbol) {
  if (Ipa::unicodeToIpaMap.find(symbol) != Ipa::unicodeToIpaMap.end()) {
    return ((Ipa::unicodeToIpaMap[symbol]));
  } else {
    std::stringstream ss;
    UnicodeString us;
    us.append(symbol);
    ss << "Unknown symbol " << us << " (0x" << std::hex << symbol << ")!" << std::endl;
    IpaException e(__FILE__, __LINE__, ss.str());
    throw e;
  }
}

std::vector<bool> Ipa::in_category(
    const std::vector<Category>& categories) const {
  std::vector<bool> result;

  std::vector<Category>::const_iterator iter = categories.begin();
  while (iter != categories.end()) {
    (result).push_back(is_a(*iter));
    iter++;
  }

  return result;
}

/**
 * Class methods used to test if a symbol is in one of the categories given.
 * @param symbol the unicode symbol to test
 * @param categories a vector of categories
 * @return
 */
std::vector<bool> Ipa::symbol_in_category(
    const UChar& symbol, const std::vector<Category>& categories) {
  std::vector<bool> answer = std::vector<bool>(categories.size(), false);
  Ipa ipa = Ipa::from_unicode(symbol, std::vector<UChar>());

  answer = ipa.in_category(categories);

  return answer;
}

bool Ipa::is_a(const Category category) const {
  bool answer = false;

  switch (category) {
    case CATEGORY_VOWEL:
      answer = (get_shape() != SHAPE_UNDEF) &&
               (get_aperture() != APERTURE_UNDEF) &&
               (get_manner() == MANNER_UNDEF);
      break;
    case CATEGORY_CONSONANT:
      answer = (get_manner() != MANNER_UNDEF) && (get_place() != PLACE_UNDEF);
      break;
    case CATEGORY_SYLLABIC_CONSONANT:
      answer = (get_manner() != MANNER_UNDEF) && (get_place() != PLACE_UNDEF) &&
               is_syllabic();
      break;
    case CATEGORY_NASAL:
      answer = is_nasal();
      break;
    case CATEGORY_FRICATIVE:
      answer = is_fricative();
      break;
    case CATEGORY_PULMONIC:
      answer = is_pulmonic();
      break;
    case CATEGORY_NON_PULMONIC:
      answer = is_non_pulmonic();
      break;
    case CATEGORY_CO_ARTICULATED:
      answer = get_is_double();
      break;
    case CATEGORY_PLOSIVE:
      answer = is_plosive();
      break;
    case CATEGORY_APPROXIMANT:
      answer = is_approximant();
      break;
    case CATEGORY_TRILL:
      answer = is_trill();
      break;
    case CATEGORY_TAP:
      answer = is_flap();  // TODO
      break;
    case CATEGORY_FLAP:
      answer = is_flap();
      break;
    case CATEGORY_LATERAL:
      answer = is_lateral();
      break;
    case CATEGORY_LIQUID:
      answer = (is_trill()) || (is_flap()) || (is_lateral());
      break;
    case CATEGORY_GLIDE:
      answer = is_approximant() &&
               ((get_is_double() ||
                 (is_non_lateral() && (is_palatal() || is_velar()))));
      break;
    case CATEGORY_SEMI_VOWEL:
      answer = (is_approximant() &&
                (get_is_double() ||
                 (is_non_lateral() && (is_palatal() || is_velar()))));
      break;
    case CATEGORY_LABIAL:
      answer = is_bilabial() || is_labiodental();
      break;
    case CATEGORY_CORONAL:
      answer =
          is_dental() || is_alveolar() || is_palatoalveolar() || is_retroflex();
      break;
    case CATEGORY_DORSAL:
      answer = is_palatal() || is_velar() || is_uvular() || is_palatoalveolar();
      break;
    case CATEGORY_RADICAL:
      answer = is_pharyngeal() || is_epiglottal();
      break;
    case CATEGORY_LARYNGEAL:
      answer = is_glottal();
      break;
    case CATEGORY_BILABIAL:
      answer = is_bilabial();
      break;
    case CATEGORY_LABIO_DENTAL:
      answer = is_labiodental();
      break;
    case CATEGORY_DENTAL:
      answer = is_dental();
      break;
    case CATEGORY_ALVEOLAR:
      answer = is_alveolar();
      break;
    case CATEGORY_PALATO_ALVEOLAR:
      answer = is_palatoalveolar();
      break;
    case CATEGORY_RETROFLEX:
      answer = is_retroflex();
      break;
    case CATEGORY_PALATAL:
      answer = is_palatal();
      break;
    case CATEGORY_VELAR:
      answer = is_velar();
      break;
    case CATEGORY_UVULAR:
      answer = is_uvular();
      break;
    case CATEGORY_PHARYNGEAL:
      answer = is_pharyngeal();
      break;
    case CATEGORY_EPIGLOTTAL:
      answer = is_epiglottal();
      break;
    case CATEGORY_GLOTTAL:
      answer = is_glottal();
      break;
    case CATEGORY_CLICK:
      answer = is_click();
      break;
    case CATEGORY_VOICED_IMPLOSIVE:
      answer = is_voiced_implosive();
      break;
    case CATEGORY_FRONT:
      answer = is_front();
      break;
    case CATEGORY_NEAR_FRONT:
      answer = is_near_front();
      break;
    case CATEGORY_CENTRAL:
      answer = is_central();
      break;
    case CATEGORY_BACK:
      answer = is_back();
      break;
    case CATEGORY_NEAR_BACK:
      answer = is_near_back();
      break;
    case CATEGORY_CLOSE:
      answer = is_close();
      break;
    case CATEGORY_NEAR_CLOSE:
      answer = is_near_close();
      break;
    case CATEGORY_MID:
      answer = is_mid();
      break;
    case CATEGORY_CLOSE_MID:
      answer = is_close_mid();
      break;
    case CATEGORY_OPEN_MID:
      answer = is_open_mid();
      break;
    case CATEGORY_OPEN:
      answer = is_open();
      break;
    case CATEGORY_NEAR_OPEN:
      answer = is_near_open();
      break;
    case CATEGORY_ROUNDED:
      answer = get_is_rounded();
      break;
    case CATEGORY_VOICED:
      answer = get_is_voiced();
      break;
    case CATEGORY_UNVOICED:
      answer = !get_is_voiced();
      break;
    case CATEGORY_AFFRICATE:
      answer = get_is_affricate();
      break;
    case CATEGORY_DOUBLE:
      answer = get_is_double();
      break;
    default:
      IpaException e(__FILE__, __LINE__, ": Unknown category!\n");
      throw(e);
      break;
  }

  return answer;
}

bool Ipa::is_one_of(const std::vector<Category>& categories) const {
  bool result = false;
  std::vector<Category>::const_iterator iter = categories.begin();
  while (iter != categories.end() && !result) {
    result = is_a(*iter);
    iter++;
  }
  return result;
}

bool Ipa::is_all_of(const std::vector<Category>& categories) const {
  bool result = true;
  std::vector<Category>::const_iterator iter = categories.begin();
  while (iter != categories.end() && result) {
    result = result && is_a(*iter);
    iter++;
  }
  return result;
}

bool Ipa::is_vowel() const {
  return (get_shape() != SHAPE_UNDEF) && (get_aperture() != APERTURE_UNDEF) &&
         (get_manner() == MANNER_UNDEF);
}

bool Ipa::is_consonant() const {
  return (get_manner() != MANNER_UNDEF) && (get_place() != PLACE_UNDEF);
}

bool Ipa::is_semi_vowel() const {
  return (
      is_approximant() &&
      (get_is_double() || (is_non_lateral() && (is_palatal() || is_velar()))));
}

bool Ipa::is_liquid() const {
  return (is_trill()) || (is_flap()) || (is_lateral());
}

bool Ipa::is_pulmonic() const {
  return ((get_manner() == MANNER_NASAL) ||
          (get_manner() == MANNER_FRICATIVE) ||
          (get_manner() == MANNER_PLOSIVE) ||
          (get_manner() == MANNER_APPROXIMANT) ||
          (get_manner() == MANNER_TRILL) || (get_manner() == MANNER_FLAP) ||
          (get_manner() == MANNER_LATERAL_FRICATIVE) ||
          (get_manner() == MANNER_LATERAL_APPROXIMANT) ||
          (get_manner() == MANNER_LATERAL_FLAP));
}

bool Ipa::is_non_pulmonic() const {
  return ((get_manner() == MANNER_CLICK) ||
          (get_manner() == MANNER_VOICED_IMPLOSIVE) ||
          (get_manner() == MANNER_EJECTIVE));
}

bool Ipa::is_nasal() const {
  return ((get_manner() == MANNER_NASAL) || is_diacritic(DIACRITIC_NASALIZED));
}

bool Ipa::is_plosive() const { return ((get_manner() == MANNER_PLOSIVE)); }

bool Ipa::is_fricative() const {
  return ((get_manner() == MANNER_FRICATIVE) ||
          (get_manner() == MANNER_LATERAL_FRICATIVE));
}

bool Ipa::is_approximant() const {
  return ((get_manner() == MANNER_APPROXIMANT) ||
          (get_manner() == MANNER_LATERAL_APPROXIMANT));
}

bool Ipa::is_trill() const { return ((get_manner() == MANNER_TRILL)); }

bool Ipa::is_flap() const {
  return ((get_manner() == MANNER_FLAP) || (get_manner() == MANNER_TAP) ||
          (get_manner() == MANNER_LATERAL_FLAP));
}

bool Ipa::is_lateral() const {
  return ((get_manner() == MANNER_LATERAL_FRICATIVE) ||
          (get_manner() == MANNER_LATERAL_APPROXIMANT) ||
          (get_manner() == MANNER_LATERAL_FLAP));
}

bool Ipa::is_non_lateral() const {
  return ((get_manner() == MANNER_NASAL) ||
          (get_manner() == MANNER_FRICATIVE) ||
          (get_manner() == MANNER_PLOSIVE) ||
          (get_manner() == MANNER_APPROXIMANT) ||
          (get_manner() == MANNER_TRILL) || (get_manner() == MANNER_FLAP));
}

bool Ipa::is_click() const { return ((get_manner() == MANNER_CLICK)); }

bool Ipa::is_voiced_implosive() const {
  return ((get_manner() == MANNER_VOICED_IMPLOSIVE));
}

bool Ipa::is_ejective() const { return ((get_manner() == MANNER_EJECTIVE)); }

bool Ipa::is_bilabial() const { return ((get_place() == PLACE_BILABIAL)); }

bool Ipa::is_labiodental() const {
  return ((get_place() == PLACE_LABIODENTAL));
}

bool Ipa::is_dental() const { return ((get_place() == PLACE_DENTAL)); }

bool Ipa::is_alveolar() const { return ((get_place() == PLACE_ALVEOLAR)); }

bool Ipa::is_palatoalveolar() const {
  return ((get_place() == PLACE_PALATOALVEOLAR));
}

bool Ipa::is_retroflex() const { return ((get_place() == PLACE_RETROFLEX)); }

bool Ipa::is_palatal() const { return ((get_place() == PLACE_PALATAL)); }

bool Ipa::is_velar() const { return ((get_place() == PLACE_VELAR)); }

bool Ipa::is_uvular() const { return ((get_place() == PLACE_UVULAR)); }

bool Ipa::is_pharyngeal() const { return ((get_place() == PLACE_PHARYNGEAL)); }

bool Ipa::is_epiglottal() const { return ((get_place() == PLACE_EPIGLOTTAL)); }

bool Ipa::is_glottal() const { return ((get_place() == PLACE_GLOTTAL)); }

bool Ipa::is_front() const { return ((get_shape() == SHAPE_FRONT)); }

bool Ipa::is_near_front() const { return ((get_shape() == SHAPE_NEAR_FRONT)); }

bool Ipa::is_central() const { return ((get_shape() == SHAPE_CENTRAL)); }

bool Ipa::is_back() const { return ((get_shape() == SHAPE_BACK)); }

bool Ipa::is_near_back() const { return ((get_shape() == SHAPE_NEAR_BACK)); }

bool Ipa::is_close() const { return ((get_aperture() == APERTURE_CLOSE)); }

bool Ipa::is_near_close() const {
  return ((get_aperture() == APERTURE_NEAR_CLOSE));
}

bool Ipa::is_open() const { return ((get_aperture() == APERTURE_OPEN)); }

bool Ipa::is_near_open() const {
  return ((get_aperture() == APERTURE_NEAR_OPEN));
}

bool Ipa::is_mid() const { return ((get_aperture() == APERTURE_MID)); }

bool Ipa::is_close_mid() const {
  return ((get_aperture() == APERTURE_CLOSE_MID));
}

bool Ipa::is_open_mid() const {
  return ((get_aperture() == APERTURE_OPEN_MID));
}

bool Ipa::is_syllabic() const { return (is_diacritic(DIACRITIC_SYLLABIC)); }

bool Ipa::is_long() const { return (is_diacritic(DIACRITIC_LONG)); }

bool Ipa::is_stressed() const {
  return (is_diacritic(DIACRITIC_PRIMARY_STRESS) ||
          is_diacritic(DIACRITIC_SECONDARY_STRESS));
}

StressLevel Ipa::get_stress_level() const {
  if (is_diacritic(DIACRITIC_PRIMARY_STRESS)) {
    return STRESS_LEVEL_HIGH;
  } else if (is_diacritic(DIACRITIC_SECONDARY_STRESS)) {
    return STRESS_LEVEL_LOW;
  } else {
    return STRESS_LEVEL_NONE;
  }
}

void Ipa::unset_stress() {
  std::set<Diacritic>::iterator iter = diacritic.find(DIACRITIC_PRIMARY_STRESS);
  if (iter != diacritic.end()) {
    diacritic.erase(iter);
    ipa_changed = true;
  }

  iter = diacritic.find(DIACRITIC_SECONDARY_STRESS);
  if (iter != diacritic.end()) {
    diacritic.erase(iter);
    ipa_changed = true;
  }
}

std::string Ipa::hash_feature() const {
/*
  std::string str;
  unsigned char digest[MD5_DIGEST_LENGTH];
  char charDigest[2 * MD5_DIGEST_LENGTH + 1];
  str += "aperture=";
  str += APERTURES[get_aperture()];
  str += ",is_affricate=";
  str += get_is_affricate();
  str += ",is_double=";
  str += get_is_double();
  str += ",is_rounded=";
  str += get_is_rounded();
  str += ",is_voiced=";
  str += get_is_voiced();
  str += ",manner=";
  str += MANNERS[get_manner()];
  str += ",place=";
  str += PLACES[get_place()];
  str += ",shape=";
  str += SHAPES[get_shape()];
  str += ",diacritics=";
  for (std::set<Diacritic>::const_iterator iter = diacritic.begin();
       iter != diacritic.end(); ++iter) {
    str += DIACRITICS[*iter];
  }
  MD5((unsigned char*)str.c_str(), (size_t)str.length() * sizeof(char), digest);
  for (int i = 0; i < MD5_DIGEST_LENGTH; i++)
    sprintf(charDigest + 2 * i, "%02x", digest[i]);
  return std::string(charDigest);
 */
  std::string str;
  str += "aperture=";
  str += APERTURES[get_aperture()];
  str += ",is_affricate=";
  str += get_is_affricate();
  str += ",is_double=";
  str += get_is_double();
  str += ",is_rounded=";
  str += get_is_rounded();
  str += ",is_voiced=";
  str += get_is_voiced();
  str += ",manner=";
  str += MANNERS[get_manner()];
  str += ",place=";
  str += PLACES[get_place()];
  str += ",shape=";
  str += SHAPES[get_shape()];
  str += ",diacritics=";
  for (std::set<Diacritic>::const_iterator iter = diacritic.begin();
       iter != diacritic.end(); ++iter) {
    str += DIACRITICS[*iter];
  }
  MD5 md5;
  
  return std::string(md5.digestString(str.c_str(), (size_t)str.length() * sizeof(char)));
}

/**
 * Test the existence of a code point in the Unicode IPA table.
 * @param code the code point to test existence
 * @return true if the code point is a unicode IPA code point
 */
bool Ipa::is_unicode(const UChar& code) {
  return (Ipa::unicodeToIpaMap.find(code) != Ipa::unicodeToIpaMap.end());
}

/**
 * Test the existence of a code point in the Unicode IPA table.
 * @param code the code point to test existence
 * @return true if the code point is a unicode IPA code point
 */
bool Ipa::is_unicode(const icu::UnicodeString& code) {
  // bool isLabel = false;
  Ipa ipa = from_unicode(code);

  // if(ipa != NULL)
  //{
  //	isLabel = true;
  //	delete ipa;
  //}
  return Ipa::is_unicode(ipa.get_unicode());
}

Ipa Ipa::from_unicode(const UnicodeString& code)  // throw (IpaException)
{
  UErrorCode status = U_ZERO_ERROR;
  std::vector<UChar> vec;
  UChar codepoint;
  int n_char;
  UChar32 label_uchar32[16];
  Ipa ipa;  // = NULL;

  try {
    n_char = code.toUTF32(label_uchar32, 16, status);
    codepoint = (UChar)label_uchar32[0];  // TODO test this
    for (int i = 1; i < n_char; i++) {
      vec.push_back(label_uchar32[i]);
    }
    ipa = Ipa::from_unicode(codepoint, vec);
  } catch (IpaException e) {
  }

  return ipa;
}

Ipa Ipa::from_unicode(
    const UChar& symbol,
    const std::set<UChar>& uni_diacritics)  // throw (IpaException)
{
  Ipa ipa;  // = NULL;

  try {
    ipa = Ipa::unicode_to_ipa(symbol);

    for (std::set<UChar>::const_iterator iter = uni_diacritics.begin();
         iter != uni_diacritics.end(); ++iter) {
      if (Ipa::is_unicode(*iter)) {
        Ipa tmpipa = Ipa::unicode_to_ipa(*iter);
        ipa.add_diacritics((tmpipa).get_diacritics());
      }
    }

    ipa.ipa_changed = true;
  } catch (IpaException e) {
    std::stringstream ss;
    ss << ": Not a unicode value!" << std::endl
       << "\t-> Error message is \"" << e.what() << "\"." << std::endl;
    IpaException e2(__FILE__, __LINE__, ss.str());
    throw(e2);
  }
  return ipa;
}

Ipa Ipa::from_unicode(
    const UChar& symbol,
    const std::vector<UChar>& uni_diacritics)  // throw (IpaException)
{
  Ipa ipa;  // = NULL;

  try {
    ipa = Ipa::unicode_to_ipa(symbol);
    for (std::vector<UChar>::const_iterator iter = uni_diacritics.begin();
         iter != uni_diacritics.end(); ++iter) {
      if (Ipa::is_unicode(*iter)) {
        Ipa tmpipa = Ipa::unicode_to_ipa(*iter);
        ipa.add_diacritics((tmpipa).get_diacritics());
      }
    }

    ipa.ipa_changed = true;
  } catch (IpaException e) {
    std::stringstream ss;
    ss << ": Not a unicode value!" << std::endl
       << "\t-> Error message is \"" << e.what() << "\"." << std::endl;
    IpaException e2(__FILE__, __LINE__, ss.str());
    throw(e2);
  }
  return ipa;
}

void Ipa::from_unicode() {
  try {
    Ipa ipa = Ipa::unicode_to_ipa(unicode);
    clear_features();

    set_manner((ipa).get_manner());
    set_place((ipa).get_place());
    set_is_affricate((ipa).get_is_affricate());
    set_is_double((ipa).get_is_double());
    set_is_rounded((ipa).get_is_rounded());
    set_is_voiced((ipa).get_is_voiced());
    set_aperture((ipa).get_aperture());
    set_shape((ipa).get_shape());
    set_diacritics((ipa).get_diacritics());

    for (std::set<UChar>::const_iterator iter = unicode_diacritic.begin();
         iter != unicode_diacritic.end(); ++iter) {
      if (Ipa::is_unicode(*iter)) {
        Ipa tmpipa = Ipa::unicode_to_ipa(*iter);
        add_diacritics((tmpipa).get_diacritics());
      }
    }
  } catch (IpaException e) {
    IpaException e2(__FILE__, __LINE__, ": Not a unicode value!\n");
    throw(e2);
  }
}

/**
 * Computes unicode and unicode diacritics values for the current object from
 * the features.
 */
void Ipa::to_unicode() {
  if (ipa_changed) {
    std::string hash;
    std::set<UChar> unicodeDiacritics;
    // Gets the unicode value from the map
    Ipa tmpIpa;
    tmpIpa.clear_features();
    tmpIpa.clear_unicode();
    tmpIpa.set_manner(get_manner());
    tmpIpa.set_place(get_place());
    tmpIpa.set_is_affricate(get_is_affricate());
    tmpIpa.set_is_double(get_is_double());
    tmpIpa.set_is_rounded(get_is_rounded());
    tmpIpa.set_is_voiced(get_is_voiced());
    tmpIpa.set_aperture(get_aperture());
    tmpIpa.set_shape(get_shape());
    // tmpIpa.set_diacritics(get_diacritic());
    hash = tmpIpa.hash_feature();
    set_unicode(ipaToUnicodeMap[hash]);

    // Gets the unicode values for diacritics from the map
    for (std::set<Diacritic>::const_iterator iter = diacritic.begin();
         iter != diacritic.end(); ++iter) {
      Ipa tmpIpa;
      tmpIpa.clear_features();
      tmpIpa.set_diacritic(*iter);
      hash = tmpIpa.hash_feature();
      unicodeDiacritics.insert(ipaToUnicodeMap[hash]);
    }
    this->unicode_diacritic = unicodeDiacritics;
    ipa_changed = false;
    // 	  cerr << "to_unicode (" << this << " , true): " << unicode << " / ";
    // 	  for (set<UChar>::iterator it = unicode_diacritic.begin(); it !=
    // unicode_diacritic.end(); ++it) { cerr << *it << " "; }
    // 	  cerr << std::endl;
  }
  // 	else {
  // 	  cerr << "to_unicode (" << this << " , false): " << unicode << " / ";
  // 	  for (set<UChar>::iterator it = unicode_diacritic.begin(); it !=
  // unicode_diacritic.end(); ++it) { cerr << *it << " "; }
  // 	  cerr << std::endl;
  // 	}
}

void Ipa::init() {
  Ipa::initialize_unicode_to_ipa_map();
  Ipa::initialize_ipa_to_unicode_map();
}

void Ipa::clear_features() {
  manner = MANNER_UNDEF;
  place = PLACE_UNDEF;
  is_affricate = false;
  is_voiced = false;
  aperture = APERTURE_UNDEF;
  shape = SHAPE_UNDEF;
  is_rounded = false;
  is_double = false;
  diacritic.clear();
  ipa_changed = true;
}

void Ipa::clear_unicode() {
  unicode = 0;
  unicode_diacritic.clear();
  ipa_changed = true;
}

void Ipa::initialize_unicode_to_ipa_map() {
  unicodeToIpaMap =
      boost::assign::list_of<std::pair<UChar, Ipa> >
      // vowels
      // // affricate, voiced, rounded, double, diacritics
      (0x0061, Ipa(MANNER_UNDEF, PLACE_UNDEF, APERTURE_OPEN, SHAPE_FRONT, false,
                   true, false, false, std::set<Diacritic>()))  // latin a
      (0x0276,
       Ipa(MANNER_UNDEF, PLACE_UNDEF, APERTURE_OPEN, SHAPE_FRONT, false, true,
           true, false, std::set<Diacritic>()))  // latin small capital oe
      (0x0251,
       Ipa(MANNER_UNDEF, PLACE_UNDEF, APERTURE_OPEN, SHAPE_BACK, false, true,
           false, false, std::set<Diacritic>()))  // latin alpha (=cursive a)
      (0x0252,
       Ipa(MANNER_UNDEF, PLACE_UNDEF, APERTURE_OPEN, SHAPE_BACK, false, true,
           true, false, std::set<Diacritic>()))  // latin turned alpha
      (0x00E6,
       Ipa(MANNER_UNDEF, PLACE_UNDEF, APERTURE_NEAR_OPEN, SHAPE_FRONT, false,
           true, false, false, std::set<Diacritic>()))  // latin ae
      (0x0250,
       Ipa(MANNER_UNDEF, PLACE_UNDEF, APERTURE_NEAR_OPEN, SHAPE_CENTRAL, false,
           true, false, false, std::set<Diacritic>()))  // latin turned a
      (0x025B,
       Ipa(MANNER_UNDEF, PLACE_UNDEF, APERTURE_OPEN_MID, SHAPE_FRONT, false,
           true, false, false, std::set<Diacritic>()))  // latin epsilon
      (0x0153,
       Ipa(MANNER_UNDEF, PLACE_UNDEF, APERTURE_OPEN_MID, SHAPE_FRONT, false,
           true, true, false, std::set<Diacritic>()))  // latin oe
      (0x025C, Ipa(MANNER_UNDEF, PLACE_UNDEF, APERTURE_OPEN_MID, SHAPE_CENTRAL,
                   false, true, false, false,
                   std::set<Diacritic>()))  // latin reversed epsilon
      (0x025E, Ipa(MANNER_UNDEF, PLACE_UNDEF, APERTURE_OPEN, SHAPE_CENTRAL,
                   false, true, true, false,
                   std::set<Diacritic>()))  // latin closed reversed epsilon
      (0x028C,
       Ipa(MANNER_UNDEF, PLACE_UNDEF, APERTURE_OPEN_MID, SHAPE_BACK, false,
           true, false, false, std::set<Diacritic>()))  // latin turned v
      (0x0254,
       Ipa(MANNER_UNDEF, PLACE_UNDEF, APERTURE_OPEN_MID, SHAPE_BACK, false,
           true, true, false, std::set<Diacritic>()))  // latin open o
      (0x0259, Ipa(MANNER_UNDEF, PLACE_UNDEF, APERTURE_MID, SHAPE_CENTRAL,
                   false, true, false, false, std::set<Diacritic>()))  // schwa
      (0x0065, Ipa(MANNER_UNDEF, PLACE_UNDEF, APERTURE_CLOSE_MID, SHAPE_FRONT,
                   false, true, false, false, std::set<Diacritic>()))  // e
      (0x00F8,
       Ipa(MANNER_UNDEF, PLACE_UNDEF, APERTURE_CLOSE_MID, SHAPE_FRONT, false,
           true, true, false, std::set<Diacritic>()))  // latin o with stroke
      (0x0258,
       Ipa(MANNER_UNDEF, PLACE_UNDEF, APERTURE_CLOSE_MID, SHAPE_CENTRAL, false,
           true, false, false, std::set<Diacritic>()))  // latin reversed e
      (0x0275,
       Ipa(MANNER_UNDEF, PLACE_UNDEF, APERTURE_CLOSE_MID, SHAPE_CENTRAL, false,
           true, true, false, std::set<Diacritic>()))  // latin barred o
      (0x0264,
       Ipa(MANNER_UNDEF, PLACE_UNDEF, APERTURE_CLOSE_MID, SHAPE_BACK, false,
           true, false, false, std::set<Diacritic>()))  // latin rams horn
      (0x006F,
       Ipa(MANNER_UNDEF, PLACE_UNDEF, APERTURE_CLOSE_MID, SHAPE_BACK, false,
           true, true, false, std::set<Diacritic>()))  // latin small o
      (0x026A, Ipa(MANNER_UNDEF, PLACE_UNDEF, APERTURE_NEAR_CLOSE,
                   SHAPE_NEAR_FRONT, false, true, false, false,
                   std::set<Diacritic>()))  // latin capital i
      (0x028F,
       Ipa(MANNER_UNDEF, PLACE_UNDEF, APERTURE_NEAR_CLOSE, SHAPE_NEAR_FRONT,
           false, true, true, false, std::set<Diacritic>()))  // latin capital y
      (0x028A, Ipa(MANNER_UNDEF, PLACE_UNDEF, APERTURE_NEAR_CLOSE,
                   SHAPE_NEAR_BACK, false, true, true, false,
                   std::set<Diacritic>()))  // latin small upsilon
      (0x0069,
       Ipa(MANNER_UNDEF, PLACE_UNDEF, APERTURE_CLOSE, SHAPE_FRONT, false, true,
           false, false, std::set<Diacritic>()))  // latin i
      (0x0079, Ipa(MANNER_UNDEF, PLACE_UNDEF, APERTURE_CLOSE, SHAPE_FRONT,
                   false, true, true, false, std::set<Diacritic>()))  // latin y
      (0x0268,
       Ipa(MANNER_UNDEF, PLACE_UNDEF, APERTURE_CLOSE, SHAPE_CENTRAL, false,
           true, false, false, std::set<Diacritic>()))  // latin i with stroke
      (0x0289,
       Ipa(MANNER_UNDEF, PLACE_UNDEF, APERTURE_CLOSE, SHAPE_CENTRAL, false,
           true, true, false, std::set<Diacritic>()))  // latin u bar
      (0x026F,
       Ipa(MANNER_UNDEF, PLACE_UNDEF, APERTURE_CLOSE, SHAPE_BACK, false, true,
           false, false, std::set<Diacritic>()))  // latin turned m
      (0x0075, Ipa(MANNER_UNDEF, PLACE_UNDEF, APERTURE_CLOSE, SHAPE_BACK, false,
                   true, true, false, std::set<Diacritic>()))  // latin u
      // Consonant (pulmonic)
      (0x006D,
       Ipa(MANNER_NASAL, PLACE_BILABIAL, APERTURE_UNDEF, SHAPE_UNDEF, false,
           true, false, false, std::set<Diacritic>()))  // latin m
      (0x0271,
       Ipa(MANNER_NASAL, PLACE_LABIODENTAL, APERTURE_UNDEF, SHAPE_UNDEF, false,
           true, false, false, std::set<Diacritic>()))  // latin m with hook
      (0x006E,
       Ipa(MANNER_NASAL, PLACE_ALVEOLAR, APERTURE_UNDEF, SHAPE_UNDEF, false,
           true, false, false, std::set<Diacritic>()))  // latin n
      (0x0273, Ipa(MANNER_NASAL, PLACE_RETROFLEX, APERTURE_UNDEF, SHAPE_UNDEF,
                   false, true, false, false,
                   std::set<Diacritic>()))  // latin n with retroflex hook
      (0x0272, Ipa(MANNER_NASAL, PLACE_PALATAL, APERTURE_UNDEF, SHAPE_UNDEF,
                   false, true, false, false,
                   std::set<Diacritic>()))  // latin n with left hook
      (0x014B,
       Ipa(MANNER_NASAL, PLACE_VELAR, APERTURE_UNDEF, SHAPE_UNDEF, false, true,
           false, false, std::set<Diacritic>()))  // latin eng
      (0x0274,
       Ipa(MANNER_NASAL, PLACE_UVULAR, APERTURE_UNDEF, SHAPE_UNDEF, false, true,
           false, false, std::set<Diacritic>()))  // latin small capital n
      (0x0070,
       Ipa(MANNER_PLOSIVE, PLACE_BILABIAL, APERTURE_UNDEF, SHAPE_UNDEF, false,
           false, false, false, std::set<Diacritic>()))  // latin p
      (0x0062,
       Ipa(MANNER_PLOSIVE, PLACE_BILABIAL, APERTURE_UNDEF, SHAPE_UNDEF, false,
           true, false, false, std::set<Diacritic>()))  // latin b
      (0x0074,
       Ipa(MANNER_PLOSIVE, PLACE_ALVEOLAR, APERTURE_UNDEF, SHAPE_UNDEF, false,
           false, false, false, std::set<Diacritic>()))  // latin t
      (0x0064,
       Ipa(MANNER_PLOSIVE, PLACE_ALVEOLAR, APERTURE_UNDEF, SHAPE_UNDEF, false,
           true, false, false, std::set<Diacritic>()))  // latin d
      (0x0288, Ipa(MANNER_PLOSIVE, PLACE_RETROFLEX, APERTURE_UNDEF, SHAPE_UNDEF,
                   false, false, false, false,
                   std::set<Diacritic>()))  // latin t with retroflex hook
      (0x0256,
       Ipa(MANNER_PLOSIVE, PLACE_RETROFLEX, APERTURE_UNDEF, SHAPE_UNDEF, false,
           true, false, false, std::set<Diacritic>()))  // latin d with tail
      (0x0063,
       Ipa(MANNER_PLOSIVE, PLACE_PALATAL, APERTURE_UNDEF, SHAPE_UNDEF, false,
           false, false, false, std::set<Diacritic>()))  // latin c
      (0x025F, Ipa(MANNER_PLOSIVE, PLACE_PALATAL, APERTURE_UNDEF, SHAPE_UNDEF,
                   false, true, false, false,
                   std::set<Diacritic>()))  // latin dotless j with stroke
      (0x006B,
       Ipa(MANNER_PLOSIVE, PLACE_VELAR, APERTURE_UNDEF, SHAPE_UNDEF, false,
           false, false, false, std::set<Diacritic>()))  // latin k
      (0x0261,
       Ipa(MANNER_PLOSIVE, PLACE_VELAR, APERTURE_UNDEF, SHAPE_UNDEF, false,
           true, false, false, std::set<Diacritic>()))  // latin script g
      (0x0071,
       Ipa(MANNER_PLOSIVE, PLACE_UVULAR, APERTURE_UNDEF, SHAPE_UNDEF, false,
           false, false, false, std::set<Diacritic>()))  // latin q
      (0x0262,
       Ipa(MANNER_PLOSIVE, PLACE_UVULAR, APERTURE_UNDEF, SHAPE_UNDEF, false,
           true, false, false, std::set<Diacritic>()))  // latin g small capital
      (0x02A1, Ipa(MANNER_PLOSIVE, PLACE_EPIGLOTTAL, APERTURE_UNDEF,
                   SHAPE_UNDEF, false, false, false, false,
                   std::set<Diacritic>()))  // latin glottal stop with stroke
      (0x0294,
       Ipa(MANNER_PLOSIVE, PLACE_GLOTTAL, APERTURE_UNDEF, SHAPE_UNDEF, false,
           false, false, false, std::set<Diacritic>()))  // latin glottal stop
      (0x0278,
       Ipa(MANNER_FRICATIVE, PLACE_BILABIAL, APERTURE_UNDEF, SHAPE_UNDEF, false,
           false, false, false, std::set<Diacritic>()))  // latin small phi
      (0x03B2,
       Ipa(MANNER_FRICATIVE, PLACE_BILABIAL, APERTURE_UNDEF, SHAPE_UNDEF, false,
           true, false, false, std::set<Diacritic>()))  // greek small beta
      (0x0066,
       Ipa(MANNER_FRICATIVE, PLACE_LABIODENTAL, APERTURE_UNDEF, SHAPE_UNDEF,
           false, false, false, false, std::set<Diacritic>()))  // latin f
      (0x0076,
       Ipa(MANNER_FRICATIVE, PLACE_LABIODENTAL, APERTURE_UNDEF, SHAPE_UNDEF,
           false, true, false, false, std::set<Diacritic>()))  // latin v
      (0x03B8,
       Ipa(MANNER_FRICATIVE, PLACE_DENTAL, APERTURE_UNDEF, SHAPE_UNDEF, false,
           false, false, false, std::set<Diacritic>()))  // greek small theta
      (0x00F0,
       Ipa(MANNER_FRICATIVE, PLACE_DENTAL, APERTURE_UNDEF, SHAPE_UNDEF, false,
           true, false, false, std::set<Diacritic>()))  // latin small eth
      (0x0073,
       Ipa(MANNER_FRICATIVE, PLACE_ALVEOLAR, APERTURE_UNDEF, SHAPE_UNDEF, false,
           false, false, false, std::set<Diacritic>()))  // latin s
      (0x007A,
       Ipa(MANNER_FRICATIVE, PLACE_ALVEOLAR, APERTURE_UNDEF, SHAPE_UNDEF, false,
           true, false, false, std::set<Diacritic>()))  // latin z
      (0x0283, Ipa(MANNER_FRICATIVE, PLACE_PALATOALVEOLAR, APERTURE_UNDEF,
                   SHAPE_UNDEF, false, false, false, false,
                   std::set<Diacritic>()))  // latin small esh
      (0x0292, Ipa(MANNER_FRICATIVE, PLACE_PALATOALVEOLAR, APERTURE_UNDEF,
                   SHAPE_UNDEF, false, true, false, false,
                   std::set<Diacritic>()))  // latin small ezh
      (0x0282, Ipa(MANNER_FRICATIVE, PLACE_RETROFLEX, APERTURE_UNDEF,
                   SHAPE_UNDEF, false, false, false, false,
                   std::set<Diacritic>()))  // latin s with hook
      (0x0290, Ipa(MANNER_FRICATIVE, PLACE_RETROFLEX, APERTURE_UNDEF,
                   SHAPE_UNDEF, false, true, false, false,
                   std::set<Diacritic>()))  // latin z with retroflex hook
      (0x00E7,
       Ipa(MANNER_FRICATIVE, PLACE_PALATAL, APERTURE_UNDEF, SHAPE_UNDEF, false,
           false, false, false, std::set<Diacritic>()))  // latin c with cedilla
      (0x029D, Ipa(MANNER_FRICATIVE, PLACE_PALATAL, APERTURE_UNDEF, SHAPE_UNDEF,
                   false, true, false, false,
                   std::set<Diacritic>()))  // latin j with cross tail
      (0x0078,
       Ipa(MANNER_FRICATIVE, PLACE_VELAR, APERTURE_UNDEF, SHAPE_UNDEF, false,
           false, false, false, std::set<Diacritic>()))  // latin x
      (0x0263,
       Ipa(MANNER_FRICATIVE, PLACE_VELAR, APERTURE_UNDEF, SHAPE_UNDEF, false,
           true, false, false, std::set<Diacritic>()))  // latin small gamma
      (0x03C7,
       Ipa(MANNER_FRICATIVE, PLACE_UVULAR, APERTURE_UNDEF, SHAPE_UNDEF, false,
           false, false, false, std::set<Diacritic>()))  // greek small chi
      (0x0281, Ipa(MANNER_FRICATIVE, PLACE_UVULAR, APERTURE_UNDEF, SHAPE_UNDEF,
                   false, true, false, false,
                   std::set<Diacritic>()))  // latin small capital inverted R
      (0x0127, Ipa(MANNER_FRICATIVE, PLACE_PHARYNGEAL, APERTURE_UNDEF,
                   SHAPE_UNDEF, false, false, false, false,
                   std::set<Diacritic>()))  // latin h with stroke
      (0x0295, Ipa(MANNER_FRICATIVE, PLACE_PHARYNGEAL, APERTURE_UNDEF,
                   SHAPE_UNDEF, false, true, false, false,
                   std::set<Diacritic>()))  // latin reversed glottal stop
      (0x029C, Ipa(MANNER_FRICATIVE, PLACE_EPIGLOTTAL, APERTURE_UNDEF,
                   SHAPE_UNDEF, false, false, false, false,
                   std::set<Diacritic>()))  // latin small capital h
      (0x02A2,
       Ipa(MANNER_FRICATIVE, PLACE_EPIGLOTTAL, APERTURE_UNDEF, SHAPE_UNDEF,
           false, true, false, false,
           std::set<Diacritic>()))  // latin reversed glottal stop with stroke
      (0x0068,
       Ipa(MANNER_FRICATIVE, PLACE_GLOTTAL, APERTURE_UNDEF, SHAPE_UNDEF, false,
           false, false, false, std::set<Diacritic>()))  // latin h
      (0x0266,
       Ipa(MANNER_FRICATIVE, PLACE_GLOTTAL, APERTURE_UNDEF, SHAPE_UNDEF, false,
           true, false, false, std::set<Diacritic>()))  // latin h with hook
      (0x028B, Ipa(MANNER_APPROXIMANT, PLACE_LABIODENTAL, APERTURE_UNDEF,
                   SHAPE_UNDEF, false, true, false, false,
                   std::set<Diacritic>()))  // latin v with hook
      (0x0279,
       Ipa(MANNER_APPROXIMANT, PLACE_ALVEOLAR, APERTURE_UNDEF, SHAPE_UNDEF,
           false, true, false, false, std::set<Diacritic>()))  // latin turned r
      (0x027B, Ipa(MANNER_APPROXIMANT, PLACE_RETROFLEX, APERTURE_UNDEF,
                   SHAPE_UNDEF, false, true, false, false,
                   std::set<Diacritic>()))  // latin turned r with hook
      (0x006A,
       Ipa(MANNER_APPROXIMANT, PLACE_PALATAL, APERTURE_UNDEF, SHAPE_UNDEF,
           false, true, false, false, std::set<Diacritic>()))  // latin j
      (0x0270, Ipa(MANNER_APPROXIMANT, PLACE_VELAR, APERTURE_UNDEF, SHAPE_UNDEF,
                   false, true, false, false,
                   std::set<Diacritic>()))  // latin turned m with long leg
      (0x0299,
       Ipa(MANNER_TRILL, PLACE_BILABIAL, APERTURE_UNDEF, SHAPE_UNDEF, false,
           true, false, false, std::set<Diacritic>()))  // latin small capital b
      (0x0072, Ipa(MANNER_TRILL, PLACE_ALVEOLAR, APERTURE_UNDEF, SHAPE_UNDEF,
                   false, true, false, false,
                   std::set<Diacritic>()))  // latin r (WARNING : we can find
                                            // \x280 r with long leg!)
      (0x0280,
       Ipa(MANNER_TRILL, PLACE_UVULAR, APERTURE_UNDEF, SHAPE_UNDEF, false, true,
           false, false, std::set<Diacritic>()))  // latin small capital r
      (0x2C71, Ipa(MANNER_FLAP, PLACE_LABIODENTAL, APERTURE_UNDEF, SHAPE_UNDEF,
                   false, true, false, false,
                   std::set<Diacritic>()))  // latin v with right hook
      (0x027E, Ipa(MANNER_FLAP, PLACE_ALVEOLAR, APERTURE_UNDEF, SHAPE_UNDEF,
                   false, true, false, false,
                   std::set<Diacritic>()))  // latin r with fish hook
      (0x027D,
       Ipa(MANNER_FLAP, PLACE_RETROFLEX, APERTURE_UNDEF, SHAPE_UNDEF, false,
           true, false, false, std::set<Diacritic>()))  // latin r with tail
      (0x026C, Ipa(MANNER_LATERAL_FRICATIVE, PLACE_ALVEOLAR, APERTURE_UNDEF,
                   SHAPE_UNDEF, false, false, false, false,
                   std::set<Diacritic>()))  // latin l with belt
      (0x026E, Ipa(MANNER_LATERAL_FRICATIVE, PLACE_ALVEOLAR, APERTURE_UNDEF,
                   SHAPE_UNDEF, false, true, false, false,
                   std::set<Diacritic>()))  // latin small lezh
      (0x006C, Ipa(MANNER_LATERAL_APPROXIMANT, PLACE_ALVEOLAR, APERTURE_UNDEF,
                   SHAPE_UNDEF, false, true, false, false,
                   std::set<Diacritic>()))  // latin l
      (0x026D, Ipa(MANNER_LATERAL_APPROXIMANT, PLACE_RETROFLEX, APERTURE_UNDEF,
                   SHAPE_UNDEF, false, true, false, false,
                   std::set<Diacritic>()))  // latin l with retroflex hook
      (0x028E, Ipa(MANNER_LATERAL_APPROXIMANT, PLACE_PALATAL, APERTURE_UNDEF,
                   SHAPE_UNDEF, false, true, false, false,
                   std::set<Diacritic>()))  // latin turned y
      (0x029F, Ipa(MANNER_LATERAL_APPROXIMANT, PLACE_VELAR, APERTURE_UNDEF,
                   SHAPE_UNDEF, false, true, false, false,
                   std::set<Diacritic>()))  // latin small capital l
      (0x027A,
       Ipa(MANNER_LATERAL_FLAP, PLACE_ALVEOLAR, APERTURE_UNDEF, SHAPE_UNDEF,
           false, true, false, false,
           std::set<Diacritic>()))  // latin small turned r with long leg
      // Consonant (non pulmonic)
      (0x0298,
       Ipa(MANNER_CLICK, PLACE_BILABIAL, APERTURE_UNDEF, SHAPE_UNDEF, false,
           false, false, false, std::set<Diacritic>()))  // bull's eye
      (0x01C0,
       Ipa(MANNER_CLICK, PLACE_DENTAL, APERTURE_UNDEF, SHAPE_UNDEF, false,
           false, false, false, std::set<Diacritic>()))  // vertical bar
      (0x01C3, Ipa(MANNER_CLICK, PLACE_RETROFLEX, APERTURE_UNDEF, SHAPE_UNDEF,
                   false, false, false, false,
                   std::set<Diacritic>()))  // pipe with a subscript dot
      (0x01C2, Ipa(MANNER_CLICK, PLACE_PALATAL, APERTURE_UNDEF, SHAPE_UNDEF,
                   false, false, false, false,
                   std::set<Diacritic>()))  // vertical bar with double stroke
      (0x01C1,
       Ipa(MANNER_CLICK, PLACE_ALVEOLAR, APERTURE_UNDEF, SHAPE_UNDEF, false,
           false, false, false, std::set<Diacritic>()))  // double vertical bar
      (0x0253, Ipa(MANNER_VOICED_IMPLOSIVE, PLACE_BILABIAL, APERTURE_UNDEF,
                   SHAPE_UNDEF, false, true, false, false,
                   std::set<Diacritic>()))  // latin b with hook
      (0x0257, Ipa(MANNER_VOICED_IMPLOSIVE, PLACE_ALVEOLAR, APERTURE_UNDEF,
                   SHAPE_UNDEF, false, true, false, false,
                   std::set<Diacritic>()))  // latin d with hook
      (0x0284,
       Ipa(MANNER_VOICED_IMPLOSIVE, PLACE_PALATAL, APERTURE_UNDEF, SHAPE_UNDEF,
           false, true, false, false,
           std::set<Diacritic>()))  // latin dotless j with stroke and hook
      (0x0260, Ipa(MANNER_VOICED_IMPLOSIVE, PLACE_VELAR, APERTURE_UNDEF,
                   SHAPE_UNDEF, false, true, false, false,
                   std::set<Diacritic>()))  // latin g with hook
      (0x029B, Ipa(MANNER_VOICED_IMPLOSIVE, PLACE_UVULAR, APERTURE_UNDEF,
                   SHAPE_UNDEF, false, true, false, false,
                   std::set<Diacritic>()))  // latin small capital g with hook
      // Consonant (co-articulated)
      (0x028D,
       Ipa(MANNER_APPROXIMANT, PLACE_LABIOVELAR, APERTURE_UNDEF, SHAPE_UNDEF,
           false, false, false, true, std::set<Diacritic>()))  // latin turned w
      (0x0077,
       Ipa(MANNER_APPROXIMANT, PLACE_LABIOVELAR, APERTURE_UNDEF, SHAPE_UNDEF,
           false, true, false, true, std::set<Diacritic>()))  // latin w
      (0x0265,
       Ipa(MANNER_APPROXIMANT, PLACE_LABIOPALATAL, APERTURE_UNDEF, SHAPE_UNDEF,
           false, true, false, true, std::set<Diacritic>()))  // latin turned h
      (0x0255, Ipa(MANNER_FRICATIVE, PLACE_ALVEOLOPALATAL, APERTURE_UNDEF,
                   SHAPE_UNDEF, false, false, false, true,
                   std::set<Diacritic>()))  // latin c with curl
      (0x0291, Ipa(MANNER_FRICATIVE, PLACE_ALVEOLOPALATAL, APERTURE_UNDEF,
                   SHAPE_UNDEF, false, true, false, true,
                   std::set<Diacritic>()))  // latin z with curl
      (0x0267, Ipa(MANNER_FRICATIVE, PLACE_PALATOVELAR, APERTURE_UNDEF,
                   SHAPE_UNDEF, false, false, false, true,
                   std::set<Diacritic>()))  // latin heng with hook
      // Diacritic
      (0x0325, Ipa(MANNER_UNDEF, PLACE_UNDEF, APERTURE_UNDEF, SHAPE_UNDEF,
                   false, false, false, false,
                   boost::assign::list_of<Diacritic>(DIACRITIC_VOICELESS)
                       .convert_to_container<
                           std::set<Diacritic> >()))  // combining ring below
      (0x032C, Ipa(MANNER_UNDEF, PLACE_UNDEF, APERTURE_UNDEF, SHAPE_UNDEF,
                   false, false, false, false,
                   boost::assign::list_of<Diacritic>(DIACRITIC_VOICED)
                       .convert_to_container<
                           std::set<Diacritic> >()))  // combining caron below
      (0x02B0, Ipa(MANNER_UNDEF, PLACE_UNDEF, APERTURE_UNDEF, SHAPE_UNDEF,
                   false, false, false, false,
                   boost::assign::list_of<Diacritic>(DIACRITIC_ASPIRATED)
                       .convert_to_container<
                           std::set<Diacritic> >()))  // modifier letter small h
      (0x0339,
       Ipa(MANNER_UNDEF, PLACE_UNDEF, APERTURE_UNDEF, SHAPE_UNDEF, false, false,
           false, false,
           boost::assign::list_of<Diacritic>(DIACRITIC_MORE_ROUNDED)
               .convert_to_container<std::set<Diacritic> >()))  // combining
                                                                // right half
                                                                // ring below
      (0x031C,
       Ipa(MANNER_UNDEF, PLACE_UNDEF, APERTURE_UNDEF, SHAPE_UNDEF, false, false,
           false, false,
           boost::assign::list_of<Diacritic>(DIACRITIC_LESS_ROUNDED)
               .convert_to_container<std::set<Diacritic> >()))  // combining
                                                                // left half
                                                                // ring below
      (0x031F,
       Ipa(MANNER_UNDEF, PLACE_UNDEF, APERTURE_UNDEF, SHAPE_UNDEF, false, false,
           false, false,
           boost::assign::list_of<Diacritic>(DIACRITIC_ADVANCED)
               .convert_to_container<std::set<Diacritic> >()))  // combining
                                                                // plus sign
                                                                // below
      (0x0320,
       Ipa(MANNER_UNDEF, PLACE_UNDEF, APERTURE_UNDEF, SHAPE_UNDEF, false, false,
           false, false,
           boost::assign::list_of<Diacritic>(DIACRITIC_RETRACTED)
               .convert_to_container<std::set<Diacritic> >()))  // combining
                                                                // minus sign
                                                                // below
      (0x0308, Ipa(MANNER_UNDEF, PLACE_UNDEF, APERTURE_UNDEF, SHAPE_UNDEF,
                   false, false, false, false,
                   boost::assign::list_of<Diacritic>(DIACRITIC_CENTRALIZED)
                       .convert_to_container<
                           std::set<Diacritic> >()))  // combining diaeresis
      (0x033D, Ipa(MANNER_UNDEF, PLACE_UNDEF, APERTURE_UNDEF, SHAPE_UNDEF,
                   false, false, false, false,
                   boost::assign::list_of<Diacritic>(DIACRITIC_MID_CENTRALIZED)
                       .convert_to_container<
                           std::set<Diacritic> >()))  // combining x above
      (0x0329,
       Ipa(MANNER_UNDEF, PLACE_UNDEF, APERTURE_UNDEF, SHAPE_UNDEF, false, false,
           false, false,
           boost::assign::list_of<Diacritic>(DIACRITIC_SYLLABIC)
               .convert_to_container<std::set<Diacritic> >()))  // combining
                                                                // vertical line
                                                                // below
      (0x032F,
       Ipa(MANNER_UNDEF, PLACE_UNDEF, APERTURE_UNDEF, SHAPE_UNDEF, false, false,
           false, false,
           boost::assign::list_of<Diacritic>(DIACRITIC_NON_SYLLABIC)
               .convert_to_container<std::set<Diacritic> >()))  // combining
                                                                // inverted bref
                                                                // below
      (0x02DE,
       Ipa(MANNER_UNDEF, PLACE_UNDEF, APERTURE_UNDEF, SHAPE_UNDEF, false, false,
           false, false,
           boost::assign::list_of<Diacritic>(DIACRITIC_RHOTICITY)
               .convert_to_container<std::set<Diacritic> >()))  // modifier
                                                                // letter rhotic
                                                                // hook
      (0x0324,
       Ipa(MANNER_UNDEF, PLACE_UNDEF, APERTURE_UNDEF, SHAPE_UNDEF, false, false,
           false, false,
           boost::assign::list_of<Diacritic>(DIACRITIC_BREATHY_VOICE)
               .convert_to_container<std::set<Diacritic> >()))  // combining
                                                                // diaeresis
                                                                // below
      (0x0330, Ipa(MANNER_UNDEF, PLACE_UNDEF, APERTURE_UNDEF, SHAPE_UNDEF,
                   false, false, false, false,
                   boost::assign::list_of<Diacritic>(DIACRITIC_CREAKY_VOICE)
                       .convert_to_container<
                           std::set<Diacritic> >()))  // combining tilde below
      (0x033C, Ipa(MANNER_UNDEF, PLACE_UNDEF, APERTURE_UNDEF, SHAPE_UNDEF,
                   false, false, false, false,
                   boost::assign::list_of<Diacritic>(DIACRITIC_LINGUOLABIAL)
                       .convert_to_container<
                           std::set<Diacritic> >()))  // combining seagull below
      (0x02B7,
       Ipa(MANNER_UNDEF, PLACE_UNDEF, APERTURE_UNDEF, SHAPE_UNDEF, false, false,
           false, false, boost::assign::list_of<Diacritic>(DIACRITIC_LABIALIZED)
                             .convert_to_container<
                                 std::set<Diacritic> >()))  // modifier letter w
      (0x02B2, Ipa(MANNER_UNDEF, PLACE_UNDEF, APERTURE_UNDEF, SHAPE_UNDEF,
                   false, false, false, false,
                   boost::assign::list_of<Diacritic>(DIACRITIC_PALATALIZED)
                       .convert_to_container<
                           std::set<Diacritic> >()))  // modifier letter j
      (0x02E0, Ipa(MANNER_UNDEF, PLACE_UNDEF, APERTURE_UNDEF, SHAPE_UNDEF,
                   false, false, false, false,
                   boost::assign::list_of<Diacritic>(DIACRITIC_VELARIZED)
                       .convert_to_container<
                           std::set<Diacritic> >()))  // modifier letter gamma
      (0x02E4,
       Ipa(MANNER_UNDEF, PLACE_UNDEF, APERTURE_UNDEF, SHAPE_UNDEF, false, false,
           false, false,
           boost::assign::list_of<Diacritic>(DIACRITIC_PHARYNGEALIZED)
               .convert_to_container<std::set<Diacritic> >()))  // modifier
                                                                // letter
                                                                // reverse
                                                                // glottal stop
      (0x0334, Ipa(MANNER_UNDEF, PLACE_UNDEF, APERTURE_UNDEF, SHAPE_UNDEF,
                   false, false, false, false,
                   boost::assign::list_of<Diacritic>(
                       DIACRITIC_VELARIZED_OR_PHARYNGEALIZED)
                       .convert_to_container<
                           std::set<Diacritic> >()))  // combining tilde overlay
                                                      // (velarization or
                                                      // pharyngealization)
      (0x031D, Ipa(MANNER_UNDEF, PLACE_UNDEF, APERTURE_UNDEF, SHAPE_UNDEF,
                   false, false, false, false,
                   boost::assign::list_of<Diacritic>(DIACRITIC_RAISED)
                       .convert_to_container<
                           std::set<Diacritic> >()))  // combining up tack below
      (0x031E,
       Ipa(MANNER_UNDEF, PLACE_UNDEF, APERTURE_UNDEF, SHAPE_UNDEF, false, false,
           false, false,
           boost::assign::list_of<Diacritic>(DIACRITIC_LOWERED)
               .convert_to_container<std::set<Diacritic> >()))  // combining
                                                                // down tack
                                                                // below
      (0x0318,
       Ipa(MANNER_UNDEF, PLACE_UNDEF, APERTURE_UNDEF, SHAPE_UNDEF, false, false,
           false, false,
           boost::assign::list_of<Diacritic>(DIACRITIC_ADVANCED_TONGUE_ROOT)
               .convert_to_container<std::set<Diacritic> >()))  // combining
                                                                // left tack
                                                                // below
      (0x0319,
       Ipa(MANNER_UNDEF, PLACE_UNDEF, APERTURE_UNDEF, SHAPE_UNDEF, false, false,
           false, false,
           boost::assign::list_of<Diacritic>(DIACRITIC_RETRACTED_TONGUE_ROOT)
               .convert_to_container<std::set<Diacritic> >()))  // combining
                                                                // right tack
                                                                // below
      (0x032A, Ipa(MANNER_UNDEF, PLACE_UNDEF, APERTURE_UNDEF, SHAPE_UNDEF,
                   false, false, false, false,
                   boost::assign::list_of<Diacritic>(DIACRITIC_DENTAL)
                       .convert_to_container<
                           std::set<Diacritic> >()))  // combining bridge below
      (0x033A,
       Ipa(MANNER_UNDEF, PLACE_UNDEF, APERTURE_UNDEF, SHAPE_UNDEF, false, false,
           false, false,
           boost::assign::list_of<Diacritic>(DIACRITIC_APICAL)
               .convert_to_container<std::set<Diacritic> >()))  // combining
                                                                // inverted
                                                                // bridge below
      (0x033B, Ipa(MANNER_UNDEF, PLACE_UNDEF, APERTURE_UNDEF, SHAPE_UNDEF,
                   false, false, false, false,
                   boost::assign::list_of<Diacritic>(DIACRITIC_LAMINAL)
                       .convert_to_container<
                           std::set<Diacritic> >()))  // combining square below
      (0x0303,
       Ipa(MANNER_UNDEF, PLACE_UNDEF, APERTURE_UNDEF, SHAPE_UNDEF, false, false,
           false, false, boost::assign::list_of<Diacritic>(DIACRITIC_NASALIZED)
                             .convert_to_container<
                                 std::set<Diacritic> >()))  // combining tilde
      (0x207F,
       Ipa(MANNER_UNDEF, PLACE_UNDEF, APERTURE_UNDEF, SHAPE_UNDEF, false, false,
           false, false,
           boost::assign::list_of<Diacritic>(DIACRITIC_NASAL_RELEASE)
               .convert_to_container<std::set<Diacritic> >()))  // superscript
                                                                // latin small n
      (0x02E1, Ipa(MANNER_UNDEF, PLACE_UNDEF, APERTURE_UNDEF, SHAPE_UNDEF,
                   false, false, false, false,
                   boost::assign::list_of<Diacritic>(DIACRITIC_LATERAL_RELEASE)
                       .convert_to_container<
                           std::set<Diacritic> >()))  // modifier letter small l
      (0x031A,
       Ipa(MANNER_UNDEF, PLACE_UNDEF, APERTURE_UNDEF, SHAPE_UNDEF, false, false,
           false, false,
           boost::assign::list_of<Diacritic>(DIACRITIC_NO_AUDIBLE_RELEASE)
               .convert_to_container<std::set<Diacritic> >()))  // combining
                                                                // left angle
                                                                // above
      (0x0304,
       Ipa(MANNER_UNDEF, PLACE_UNDEF, APERTURE_UNDEF, SHAPE_UNDEF, false, false,
           false, false,
           boost::assign::list_of<Diacritic>(DIACRITIC_MID_OR_FIRST_TONE)
               .convert_to_container<std::set<Diacritic> >()))  // combining
                                                                // macron
      (0x0301,
       Ipa(MANNER_UNDEF, PLACE_UNDEF, APERTURE_UNDEF, SHAPE_UNDEF, false, false,
           false, false,
           boost::assign::list_of<Diacritic>(DIACRITIC_HIGH_OR_SECOND_TONE)
               .convert_to_container<std::set<Diacritic> >()))  // combining
                                                                // acute accent
      (0x030C,
       Ipa(MANNER_UNDEF, PLACE_UNDEF, APERTURE_UNDEF, SHAPE_UNDEF, false, false,
           false, false,
           boost::assign::list_of<Diacritic>(DIACRITIC_RISING_OR_THIRD_TONE)
               .convert_to_container<std::set<Diacritic> >()))  // combining
                                                                // caron
      (0x0300,
       Ipa(MANNER_UNDEF, PLACE_UNDEF, APERTURE_UNDEF, SHAPE_UNDEF, false, false,
           false, false,
           boost::assign::list_of<Diacritic>(DIACRITIC_LOW_OR_FOURTH_TONE)
               .convert_to_container<std::set<Diacritic> >()))  // combining
                                                                // grave accent
      (0x0306,
       Ipa(MANNER_UNDEF, PLACE_UNDEF, APERTURE_UNDEF, SHAPE_UNDEF, false, false,
           false, false, boost::assign::list_of<Diacritic>(DIACRITIC_SHORT)
                             .convert_to_container<
                                 std::set<Diacritic> >()))  // combining breve

      // Added (syllabic diacritics)
      (0x002E, Ipa(MANNER_UNDEF, PLACE_UNDEF, APERTURE_UNDEF, SHAPE_UNDEF,
                   false, false, false, false,
                   boost::assign::list_of<Diacritic>(DIACRITIC_SYLLABLE_BREAK)
                       .convert_to_container<std::set<Diacritic> >()))  // .
      (0x203F,
       Ipa(MANNER_UNDEF, PLACE_UNDEF, APERTURE_UNDEF, SHAPE_UNDEF, false, false,
           false, false,
           boost::assign::list_of<Diacritic>(DIACRITIC_SYLLABLE_LINKING)
               .convert_to_container<std::set<Diacritic> >()))  // undertie
      (0x02D0,
       Ipa(MANNER_UNDEF, PLACE_UNDEF, APERTURE_UNDEF, SHAPE_UNDEF, false, false,
           false, false,
           boost::assign::list_of<Diacritic>(DIACRITIC_LONG)
               .convert_to_container<std::set<Diacritic> >()))  // combining
                                                                // left angle
                                                                // above
      (0x02C8, Ipa(MANNER_UNDEF, PLACE_UNDEF, APERTURE_UNDEF, SHAPE_UNDEF,
                   false, false, false, false,
                   boost::assign::list_of<Diacritic>(DIACRITIC_PRIMARY_STRESS)
                       .convert_to_container<
                           std::set<Diacritic> >()))  // combining with ˈ
      (0x02CC, Ipa(MANNER_UNDEF, PLACE_UNDEF, APERTURE_UNDEF, SHAPE_UNDEF,
                   false, false, false, false,
                   boost::assign::list_of<Diacritic>(DIACRITIC_SECONDARY_STRESS)
                       .convert_to_container<
                           std::set<Diacritic> >()))  // combining with ˌ
      // Special characters (symbol + embedded diacritics)
      (0x025A,
       Ipa(MANNER_UNDEF, PLACE_UNDEF, APERTURE_MID, SHAPE_CENTRAL, false, true,
           false, false,
           boost::assign::list_of<Diacritic>(DIACRITIC_RHOTICITY)
               .convert_to_container<std::set<Diacritic> >()))  // latin small
                                                                // letter schwa
                                                                // with hook
      (0x00F5,
       Ipa(MANNER_UNDEF, PLACE_UNDEF, APERTURE_CLOSE_MID, SHAPE_BACK, false,
           true, true, false,
           boost::assign::list_of<Diacritic>(DIACRITIC_NASALIZED)
               .convert_to_container<std::set<Diacritic> >()))  // latin small
                                                                // letter o with
                                                                // tilde
      (0x025D,
       Ipa(MANNER_UNDEF, PLACE_UNDEF, APERTURE_OPEN_MID, SHAPE_CENTRAL, false,
           true, false, false,
           boost::assign::list_of<Diacritic>(DIACRITIC_RHOTICITY)
               .convert_to_container<std::set<Diacritic> >()))  // latin
                                                                // reversed
                                                                // epsilon with
                                                                // hook
      (0x026B,
       Ipa(MANNER_LATERAL_APPROXIMANT, PLACE_ALVEOLAR, APERTURE_UNDEF,
           SHAPE_UNDEF, false, true, false, false,
           boost::assign::list_of<Diacritic>(
               DIACRITIC_VELARIZED_OR_PHARYNGEALIZED)
               .convert_to_container<std::set<Diacritic> >()))  // latin small
                                                                // letter l with
                                                                // middle tilde

      // Fr
      (0x00EF,
       Ipa(MANNER_UNDEF, PLACE_UNDEF, APERTURE_CLOSE, SHAPE_FRONT, false, true,
           false, false,
           boost::assign::list_of<Diacritic>(DIACRITIC_CENTRALIZED)
               .convert_to_container<std::set<Diacritic> >()))  // latin small
                                                                // letter i with
                                                                // diaeresis
      (0x00F1, Ipa(MANNER_NASAL, PLACE_ALVEOLAR, APERTURE_UNDEF, SHAPE_UNDEF,
                   false, true, false, false,
                   boost::assign::list_of<Diacritic>(DIACRITIC_NASALIZED)
                       .convert_to_container<std::set<Diacritic> >()))  // latin
                                                                        // n
                                                                        // with
                                                                        // tilde
      // En
      (0x00E4,
       Ipa(MANNER_UNDEF, PLACE_UNDEF, APERTURE_OPEN, SHAPE_FRONT, false, true,
           false, false,
           boost::assign::list_of<Diacritic>(DIACRITIC_CENTRALIZED)
               .convert_to_container<std::set<Diacritic> >()))  // latin small
                                                                // letter a with
                                                                // diaeresis
      (0x012D,
       Ipa(MANNER_UNDEF, PLACE_UNDEF, APERTURE_CLOSE, SHAPE_FRONT, false, true,
           false, false,
           boost::assign::list_of<Diacritic>(DIACRITIC_SHORT)
               .convert_to_container<std::set<Diacritic> >()))  // latin small
                                                                // letter i with
                                                                // breve

      // Br
      (0x00E3,
       Ipa(MANNER_UNDEF, PLACE_UNDEF, APERTURE_OPEN, SHAPE_FRONT, false, true,
           false, false,
           boost::assign::list_of<Diacritic>(DIACRITIC_NASALIZED)
               .convert_to_container<std::set<Diacritic> >()))  // latin small
                                                                // letter a with
                                                                // tilde
      (0x1EBD,
       Ipa(MANNER_UNDEF, PLACE_UNDEF, APERTURE_CLOSE_MID, SHAPE_FRONT, false,
           true, false, false,
           boost::assign::list_of<Diacritic>(DIACRITIC_NASALIZED)
               .convert_to_container<std::set<Diacritic> >()))  // latin small
                                                                // letter e with
                                                                // tilde
      (0x0129,
       Ipa(MANNER_UNDEF, PLACE_UNDEF, APERTURE_CLOSE, SHAPE_FRONT, false, true,
           false, false,
           boost::assign::list_of<Diacritic>(DIACRITIC_NASALIZED)
               .convert_to_container<std::set<Diacritic> >()))  // latin small
                                                                // letter i with
                                                                // tilde
      (0x1EF9,
       Ipa(MANNER_UNDEF, PLACE_UNDEF, APERTURE_CLOSE, SHAPE_FRONT, false, true,
           true, false,
           boost::assign::list_of<Diacritic>(DIACRITIC_NASALIZED)
               .convert_to_container<std::set<Diacritic> >()))  // latin small
                                                                // letter y with
                                                                // tilde

      //~ //Zh
      //~ (0x0101, Ipa(MANNER_UNDEF, PLACE_UNDEF,APERTURE_OPEN,
      //SHAPE_FRONT,		false,true,false,false,
      //boost::assign::list_of <Diacritic>
      //(DIACRITIC_MID_OR_FIRST_TONE).convert_to_container<std::set<Diacritic>
      //>()) ) // latin small letter a with macron
      //~ (0x00E1, Ipa(MANNER_UNDEF, PLACE_UNDEF,APERTURE_OPEN,
      //SHAPE_FRONT,		false,true,false,false,
      //boost::assign::list_of <Diacritic>
      //(DIACRITIC_HIGH_OR_SECOND_TONE).convert_to_container<std::set<Diacritic>
      //>()) ) // latin small letter a with acute
      //~ (0x01CE, Ipa(MANNER_UNDEF, PLACE_UNDEF,APERTURE_OPEN,
      //SHAPE_FRONT,		false,true,false,false,
      //boost::assign::list_of <Diacritic>
      //(DIACRITIC_RISING_OR_THIRD_TONE).convert_to_container<std::set<Diacritic>
      //>()) ) // latin small letter a with caron
      //~ (0x00E0, Ipa(MANNER_UNDEF, PLACE_UNDEF,APERTURE_OPEN,
      //SHAPE_FRONT,		false,true,false,false,
      //boost::assign::list_of <Diacritic>
      //(DIACRITIC_LOW_OR_FOURTH_TONE).convert_to_container<std::set<Diacritic>
      //>()) ) // latin small letter a with grave
      //~
      //~ (0x012B, Ipa(MANNER_UNDEF, PLACE_UNDEF,APERTURE_CLOSE,
      //SHAPE_FRONT,		false,true,false,false,
      //boost::assign::list_of <Diacritic>
      //(DIACRITIC_MID_OR_FIRST_TONE).convert_to_container<std::set<Diacritic>
      //>()) ) // latin small letter i with macron
      //~ (0x00ED, Ipa(MANNER_UNDEF, PLACE_UNDEF,APERTURE_CLOSE,
      //SHAPE_FRONT,		false,true,false,false,
      //boost::assign::list_of <Diacritic>
      //(DIACRITIC_HIGH_OR_SECOND_TONE).convert_to_container<std::set<Diacritic>
      //>()) ) // latin small letter i with acute
      //~ (0x01D0, Ipa(MANNER_UNDEF, PLACE_UNDEF,APERTURE_CLOSE,
      //SHAPE_FRONT,		false,true,false,false,
      //boost::assign::list_of <Diacritic>
      //(DIACRITIC_RISING_OR_THIRD_TONE).convert_to_container<std::set<Diacritic>
      //>()) ) // latin small letter i with caron
      //~ (0x00EC, Ipa(MANNER_UNDEF, PLACE_UNDEF,APERTURE_CLOSE,
      //SHAPE_FRONT,		false,true,false,false,
      //boost::assign::list_of <Diacritic>
      //(DIACRITIC_LOW_OR_FOURTH_TONE).convert_to_container<std::set<Diacritic>
      //>())) // latin small letter i with grave
      //~
      //~ (0x016B, Ipa(MANNER_UNDEF, PLACE_UNDEF,APERTURE_CLOSE,
      //SHAPE_BACK,			false,true, true,false,
      //boost::assign::list_of <Diacritic>
      //(DIACRITIC_MID_OR_FIRST_TONE).convert_to_container<std::set<Diacritic>
      //>()) ) // latin small letter u with macron
      //~ (0x00FA, Ipa(MANNER_UNDEF, PLACE_UNDEF,APERTURE_CLOSE,
      //SHAPE_BACK,			false,true, true,false,
      //boost::assign::list_of <Diacritic>
      //(DIACRITIC_HIGH_OR_SECOND_TONE).convert_to_container<std::set<Diacritic>
      //>()) ) // latin small letter u with acute
      //~ (0x01D4, Ipa(MANNER_UNDEF, PLACE_UNDEF,APERTURE_CLOSE,
      //SHAPE_BACK,			false,true, true,false,
      //boost::assign::list_of <Diacritic>
      //(DIACRITIC_RISING_OR_THIRD_TONE).convert_to_container<std::set<Diacritic>
      //>()) ) // latin small letter u with caron
      //~ (0x00F9, Ipa(MANNER_UNDEF, PLACE_UNDEF,APERTURE_CLOSE,
      //SHAPE_BACK,			false,true, true,false,
      //boost::assign::list_of <Diacritic>
      //(DIACRITIC_LOW_OR_FOURTH_TONE).convert_to_container<std::set<Diacritic>
      //>()) ) // latin small letter u with grave
      //~
      //~ (0x0113, Ipa(MANNER_UNDEF, PLACE_UNDEF,APERTURE_CLOSE_MID,
      //SHAPE_FRONT,		false,true,false,false,
      //boost::assign::list_of <Diacritic>
      //(DIACRITIC_MID_OR_FIRST_TONE).convert_to_container<std::set<Diacritic>
      //>()) ) // latin small letter e with macron
      (0x00E9,
       Ipa(MANNER_UNDEF, PLACE_UNDEF, APERTURE_CLOSE_MID, SHAPE_FRONT, false,
           true, false, false,
           boost::assign::list_of<Diacritic>(DIACRITIC_HIGH_OR_SECOND_TONE)
               .convert_to_container<std::set<Diacritic> >()))  // latin small
                                                                // letter e with
                                                                // acute
      //~ (0x011B, Ipa(MANNER_UNDEF, PLACE_UNDEF,APERTURE_CLOSE_MID,
      //SHAPE_FRONT,		false,true,false,false,
      //boost::assign::list_of <Diacritic>
      //(DIACRITIC_RISING_OR_THIRD_TONE).convert_to_container<std::set<Diacritic>
      //>()) ) // latin small letter e with caron
      (0x00E8,
       Ipa(MANNER_UNDEF, PLACE_UNDEF, APERTURE_CLOSE_MID, SHAPE_FRONT, false,
           true, false, false,
           boost::assign::list_of<Diacritic>(DIACRITIC_LOW_OR_FOURTH_TONE)
               .convert_to_container<std::set<Diacritic> >()))
          .to_container(unicodeToIpaMap);  // latin small letter e with grave
  //~
  //~ (0x014D, Ipa(MANNER_UNDEF, PLACE_UNDEF,APERTURE_CLOSE_MID,	SHAPE_BACK,
  //false,true, true,false,  boost::assign::list_of <Diacritic>
  //(DIACRITIC_MID_OR_FIRST_TONE).convert_to_container<std::set<Diacritic> >())
  //) // latin small letter o with macron
  //~ (0x00F3, Ipa(MANNER_UNDEF, PLACE_UNDEF,APERTURE_CLOSE_MID,	SHAPE_BACK,
  //false,true, true,false,  boost::assign::list_of <Diacritic>
  //(DIACRITIC_HIGH_OR_SECOND_TONE).convert_to_container<std::set<Diacritic>
  //>()) ) // latin small letter o with acute
  //~ (0x01D2, Ipa(MANNER_UNDEF, PLACE_UNDEF,APERTURE_CLOSE_MID,	SHAPE_BACK,
  //false,true, true,false, boost::assign::list_of <Diacritic>
  //(DIACRITIC_RISING_OR_THIRD_TONE).convert_to_container<std::set<Diacritic>
  //>()) ) // latin small letter o with caron
  //~ (0x00F2, Ipa(MANNER_UNDEF, PLACE_UNDEF,APERTURE_CLOSE_MID,	SHAPE_BACK,
  //false,true, true,false, boost::assign::list_of <Diacritic>
  //(DIACRITIC_LOW_OR_FOURTH_TONE).convert_to_container<std::set<Diacritic> >())
  //); // latin small letter o with grave
}

void Ipa::initialize_ipa_to_unicode_map() {
  std::map<UChar, Ipa>::iterator iter;

  // Fill the map
  iter = unicodeToIpaMap.begin();
  while (iter != unicodeToIpaMap.end()) {
    Ipa& ipa = (*iter).second;
    std::string hash = (ipa).hash_feature();
    ipaToUnicodeMap[hash] = (*iter).first;
    ipa.ipa_changed = true;
    iter++;
  }
  // 	// Pre-compute unicode characters
  // 	iter = unicodeToIpaMap.begin();
  // 	while(iter != unicodeToIpaMap.end())
  // 	  {
  // 	    Ipa &ipa = (*iter).second;
  // 	    ipa.to_unicode();
  // 	    iter++;
  // 	  }
}

std::string Ipa::to_string(int level) {
  UnicodeString us;      // Unicode character only
  std::stringstream ds;  // Description of the IPA
  std::stringstream ss;  // Global description (depends on the level)
  Classification vowelClassification, consonantClassification;

  const std::set<UChar>& uni_diacritic = get_unicode_diacritics_set();
  switch (level) {
    // level 0 : LEVEL_LABEL, precise IPA label (no code)
    case 0:
      for (std::set<UChar>::const_iterator it = uni_diacritic.begin();
           it != uni_diacritic.end(); ++it) {
        if (is_preceding_diacritic(get_diacritic_from_unicode(*it))) {
          us.append(*it);
        }
      }
      us.append(unicode);
      for (std::set<UChar>::const_iterator it = uni_diacritic.begin();
           it != uni_diacritic.end(); ++it) {
        if (is_following_diacritic(get_diacritic_from_unicode(*it))) {
          us.append(*it);
        }
      }
      ss << us;
      break;

    // level 1: LEVEL_CLASS_COARSE, IPA label + code + other info
    case 1:
      for (std::set<UChar>::const_iterator it = uni_diacritic.begin();
           it != uni_diacritic.end(); ++it) {
        if (is_preceding_diacritic(get_diacritic_from_unicode(*it))) {
          us.append(*it);
          ds << "+0x" << std::hex << *it;
        }
      }
      us.append(unicode);
      ds << " (0x" << std::hex << unicode;
      for (std::set<UChar>::const_iterator it = uni_diacritic.begin();
           it != uni_diacritic.end(); ++it) {
        if (is_following_diacritic(get_diacritic_from_unicode(*it))) {
          us.append(*it);
          ds << "+0x" << std::hex << *it;
        }
      }
      if (this->is_a(CATEGORY_VOWEL)) {
        ds << ", vowel";
      } else {
        ds << ", consonant";
      }
      ds << ", aperture=" << APERTURES[get_aperture()];
      ds << ", shape=" << SHAPES[get_shape()];
      ds << ", place=" << PLACES[get_place()];
      ds << ", manner=" << MANNERS[get_manner()];
      ds << ", is_affricate=" << get_is_affricate();
      ds << ", is_rounded=" << get_is_rounded();
      ds << ", is_doubled=" << get_is_double();
      ds << ", is_voiced=" << get_is_voiced();
      ds << ") ";
      ss << us << ds.str();
      break;

    // level 2: LEVEL_CLASS_FINE?
    case 2:
      if (this->is_a(CATEGORY_VOWEL)) {
        ss << "vowel";
        vowelClassification = this->get_vowel_classification();
        switch (vowelClassification) {
          case CLASSIFICATION_VOWEL_BACKNESS:
            if (this->is_a(CATEGORY_FRONT) || this->is_a(CATEGORY_NEAR_FRONT)) {
              ss << "_front";
            }

            if (this->is_a(CATEGORY_CENTRAL)) {
              ss << "_central";
            }

            if (this->is_a(CATEGORY_NEAR_BACK) || this->is_a(CATEGORY_BACK)) {
              ss << "_back";
            }
            break;
          case CLASSIFICATION_VOWEL_HEIGHT:
            if (this->is_a(CATEGORY_CLOSE) || this->is_a(CATEGORY_NEAR_CLOSE)) {
              ss << "_close";
            }

            if (this->is_a(CATEGORY_CLOSE_MID) || this->is_a(CATEGORY_MID) ||
                this->is_a(CATEGORY_OPEN_MID)) {
              ss << "_mid";
            }

            if (this->is_a(CATEGORY_OPEN) || this->is_a(CATEGORY_NEAR_OPEN)) {
              ss << "_open";
            }
            break;
          default:
            throw RootsException(
                __FILE__, __LINE__,
                "unknown value for 'vowelClassification' argument!\n");
        }

        if (this->is_a(CATEGORY_NASAL)) {
          ss << "_nasal";
        }

      } else {
        ss << "consonant";
        consonantClassification = this->get_consonant_classification();
        switch (consonantClassification) {
          case CLASSIFICATION_CONSONANT_MANNER:
            if (this->is_a(CATEGORY_FRICATIVE)) {
              ss << "_fricative";
            }
            if (this->is_a(CATEGORY_PLOSIVE)) {
              ss << "_plosive";
            }
            if (this->is_a(CATEGORY_APPROXIMANT)) {
              ss << "_approximant";
            }
            if (this->is_a(CATEGORY_LIQUID)) {
              ss << "_liquid";
            }
            break;
          case CLASSIFICATION_CONSONANT_PLACE:
            if (this->is_a(CATEGORY_LABIAL)) {
              ss << "_labial";
            }
            if (this->is_a(CATEGORY_CORONAL)) {
              ss << "_coronal";
            }
            if (this->is_a(CATEGORY_DORSAL)) {
              ss << "_dorsal";
            }
            if (this->is_a(CATEGORY_RADICAL) ||
                this->is_a(CATEGORY_LARYNGEAL)) {
              ss << "_laryngeal";
            }
            break;
          default:
            throw RootsException(
                __FILE__, __LINE__,
                "unknown value for 'consonantClassification' argument!\n");
        }

        if (this->is_a(CATEGORY_NASAL)) {
          ss << "_nasal";
        }
      }

      break;
    default:
      throw RootsException(__FILE__, __LINE__,
                           "unknown value for 'level' argument!\n");
  }
  return std::string(ss.str().c_str());
}

void Ipa::deflate(RootsStream* stream, bool is_terminal, int list_index) {
  BaseItem::deflate(stream, false, list_index);

  if (this->get_classname() == Ipa::classname()) {
    // ipa_changed = true;
    to_unicode();

    stream->append_uchar_content("unicode", this->get_unicode());
    if (diacritic.size() > 0) {
      stream->append_set_uchar_content("unicode_diacritic", unicode_diacritic);
    }
  }

  if (is_terminal) stream->close_object();
}

void Ipa::inflate(RootsStream* stream, bool is_terminal, int list_index) {
  std::string ipaClassname = stream->get_object_classname_no_read(
      roots::phonology::ipa::Ipa::xml_tag_name(), list_index);
  BaseItem::inflate(stream, false, list_index);

  if (ipaClassname.compare(roots::phonology::ipa::Ipa::classname()) == 0) {
    this->set_unicode(stream->get_uchar_content("unicode"));
    if (stream->has_child("unicode_diacritic")) {
      std::set<UChar> vdia = stream->get_set_uchar_content("unicode_diacritic");
      this->set_unicode_diacritics(vdia);
    }
    this->from_unicode();
  }
  if (is_terminal) stream->close_children();
}

Ipa* Ipa::inflate_object(RootsStream* stream, int list_index) {
  Ipa* t = new Ipa(true);
  t->inflate(stream, true, list_index);
  return t;
}

/****************************************************************************
 * Alphabet part
 ***************************************************************************/

bool Ipa::is_label(const std::string& label) const {
  UnicodeString unistr = icu::UnicodeString(label.c_str(), "utf-8");
  UChar32 label_uchar32[3];
  UErrorCode status = U_ZERO_ERROR;
  int n_char = unistr.toUTF32(label_uchar32, 3, status);
  if (n_char != 1) {
    return false;
  } else {
    UChar uchar = (UChar)label_uchar32[0];  // TODO test this
    Ipa ipa = Ipa::unicode_to_ipa(uchar);
    return Ipa::is_unicode(uchar) && !ipa.is_diacritic();
  }
}

std::vector<UChar> Ipa::get_symbols(const std::string& label) {
  UnicodeString unistr = icu::UnicodeString(label.c_str(), "utf-8");
  UChar32 label_uchar32[3];
  UErrorCode status = U_ZERO_ERROR;
  int n_char = unistr.toUTF32(label_uchar32, 3, status);
  if (n_char != 1) {
    AlphabetException e(__FILE__, __LINE__,
                        "Unknown label \"" + label + "\"!\n");
    throw(e);
  } else {
    std::vector<UChar> vec;
    UChar uchar = (UChar)label_uchar32[0];  // TODO test this
    Ipa ipa = Ipa::unicode_to_ipa(uchar);
    vec.push_back(uchar);
    return vec;
  }
}

bool Ipa::is_diacritic(
    const std::string& diacritic_str,
    roots::phonology::ipa::DiacriticPosition diac_pos) const {
  UnicodeString unistr;
  if (diacritic_str == "\xE2\x80") {
    unistr = "\xE2\x80\xBF";
  }
  else {
    unistr = icu::UnicodeString(diacritic_str.c_str(), "utf-8");
  }
  bool result = is_diacritic_static(unistr[0]) && (unistr.length() == 1);
  if (result) {
    Diacritic diac = get_diacritic_from_unicode(unistr[0]);
    bool position = (diac_pos == DIACRITIC_POSITION_ANY ||
                     (diac_pos == DIACRITIC_POSITION_PRECEDING &&
                      is_preceding_diacritic(diac)) ||
                     (diac_pos == DIACRITIC_POSITION_FOLLOWING &&
                      is_following_diacritic(diac)));

    return position;
  } else {
    return false;
  }
}

bool Ipa::is_preceding_diacritic(roots::phonology::ipa::Diacritic diac) const {
  return Ipa::is_preceding_diacritic_static(diac);
}

bool Ipa::is_following_diacritic(Diacritic diac) const {
  return Ipa::is_following_diacritic_static(diac);
}

UChar Ipa::get_diacritic(const std::string& diacritic_str) {
  UnicodeString unistr;
  if (diacritic_str == "\xE2\x80") {
    unistr = "\xE2\x80\xBF";
  }
  else {
    unistr = icu::UnicodeString(diacritic_str.c_str(), "utf-8");
  }
  Ipa ipa = from_unicode(unistr);
  ipa.to_unicode();
  if (ipa.get_diacritics().size() > 0 && ipa.is_diacritic() &&
      Ipa::is_unicode(*ipa.unicode_diacritic.begin())) {
    return *ipa.unicode_diacritic.begin();
  } else {
    AlphabetException e(__FILE__, __LINE__,
                        "Unknown diacritic \"" + diacritic_str + "\"!\n");
    throw(e);
  }
}

std::set<UChar> Ipa::get_diacritic_symbols(const std::string& label) {
  UnicodeString unistr = icu::UnicodeString(label.c_str(), "utf-8");
  Ipa ipa = from_unicode(unistr);
  return ipa.get_unicode_diacritics_set();
}

std::vector<std::string>* Ipa::get_alphabet_set() {
  return Ipa::get_alphabet_set_static();
}

std::vector<std::string>* Ipa::get_alphabet_set_static() {
  std::vector<std::string>* vec = new std::vector<std::string>();
  std::map<UChar, roots::phonology::ipa::Ipa>::iterator iter =
      unicodeToIpaMap.begin();

  while (iter != unicodeToIpaMap.end()) {
    (*vec).push_back((*iter).second.to_string());
    iter++;
  }
  return vec;
}

std::vector<std::string>* Ipa::get_all_diacritics_set() {
  return Ipa::get_all_diacritics_set_static();
}

std::vector<std::string>* Ipa::get_all_diacritics_set_static() {
  std::vector<std::string>* vec = new std::vector<std::string>();
  std::map<UChar, roots::phonology::ipa::Ipa>::iterator iter =
      unicodeToIpaMap.begin();

  while (iter != unicodeToIpaMap.end()) {
    if ((*iter).second.is_diacritic()) {
      (*vec).push_back((*iter).second.to_string());
    }
    iter++;
  }

  return vec;
}

bool Ipa::test_ipa_extraction(std::string& str) {
  try {
    std::vector<Ipa*> result = extract_ipas(str);
    for (std::vector<Ipa*>::iterator it = result.begin(); it != result.end();
         ++it) {
      (*it)->destroy();
    }
  } catch (IpaException e) {
    return false;
  }
  return true;
}

std::vector<Ipa*> Ipa::extract_ipas(std::string& str) {
  std::vector<Ipa*> result = std::vector<Ipa*>();
  UChar uchar;
  Ipa ipa;
  std::set<UChar> diacs;
  std::string prev_str = "";

  while (str != prev_str && str != "") {
    prev_str = str;
//     std::clog << str << std::endl;
    extract_first_diacritic(
        str,
        DIACRITIC_POSITION_FOLLOWING);  // warning : needed for Blizzard 2015

    uchar = extract_first_diacritic(str, DIACRITIC_POSITION_PRECEDING);

    if (uchar != 0) {
      diacs.insert(uchar);
    }
  }
  
  while (str != "") {
    // Parse preceding diacritics

    // std::set<UChar> uni_diacritics_set;
    std::string prev_str = "";
    while (str != prev_str && str != "") {
      prev_str = str;
      uchar = extract_first_diacritic(str, DIACRITIC_POSITION_PRECEDING);

      if (uchar != 0) {
        diacs.insert(uchar);
      }
    }

	// Parse core symbol + its potential embedded diacritics
	uchar = extract_first_label(str);

    if (uchar == 0) {
      std::stringstream ss;
      ss << "Ipa::extract_ipas - unknown character sequence in string <";
      ss << str << "> for alphabet <" << get_name() << "> " << "max_symbol_string_length=" << max_symbol_string_length << " !" << std::endl;
      IpaException e(__FILE__, __LINE__, ss.str());
      throw(e);
    } else {
      // Get a IPA unicode
      ipa = Ipa::unicode_to_ipa(uchar);
      // Separate symbol and diacritics
      diacs.insert(ipa.unicode_diacritic.begin(), ipa.unicode_diacritic.end());
      ipa.diacritic.clear();
      result.push_back(new Ipa(ipa));

      // Parse extra diacritics
      bool found = true;
      // std::set<UChar> uni_diacritics_set;
      std::string prev_str = str;
      while (found && str != "") {
        uchar = extract_first_diacritic(str, DIACRITIC_POSITION_FOLLOWING);
        if (uchar != 0) {
          diacs.insert(uchar);
          prev_str = str;
        } else if (str == prev_str) {
          found = false;
        }
      }

      // Set all parsed diacritics (embedded + extra) to the current IPA
      result.back()->set_unicode_diacritics(diacs);
      result.back()->from_unicode();
      diacs.clear();
    }
  }

  for (std::vector<Ipa*>::iterator it = result.begin(); it != result.end();
       ++it) {
    (*it)->from_unicode();
  }

  return result;
}

std::vector<Ipa*> Ipa::extract_ipas(const char* c_str) {
  std::string str(c_str);
  return extract_ipas(str);
}

UChar Ipa::extract_first_label(std::string& str) {
  // Strategy: Find the longest one first
  int curlen = 0;
  int maxLabelLen = 0;
  bool found = false;
  std::string candidate;
  // Max length of label
  maxLabelLen = Ipa::get_max_symbol_string_length();

  // Test the substring with length from maxLabelLen to 1
  // curlen = (((size_t) maxLabelLen) < str.length() ? maxLabelLen :
  // str.length());
  curlen = std::min(maxLabelLen, (int)str.size());
  while (curlen >= 1 && !found) {
    candidate = str.substr(0, curlen);
    // std::clog << "candidate: " << candidate;
    found = is_label(candidate);
    // std::clog << " " << (found?"true":"false") << std::endl;
    --curlen;
  }

  if (!found) {
    return 0;
  } else {
    str.erase(0, candidate.size());
    return get_symbols(candidate).at(0);
  }
}

UChar Ipa::extract_first_diacritic(
    std::string& str, roots::phonology::ipa::DiacriticPosition diac_pos) {
  // Strategy: Find the longest one first
  int curlen = 0;
  int maxLabelLen = 0;
  bool found = false;
  std::string candidate = "";;

  // Max length of diacritic
  maxLabelLen = roots::phonology::ipa::Ipa::get_max_diacritic_string_length();
  
  // Test the substring with length from maxLabelLen to 1
  curlen = std::min(maxLabelLen, (int)str.size());

  while (curlen >= 1 && !found) {
    candidate = str.substr(0, curlen);
    // WARNING: This is a part to handle the syllable_linking diacritic which should be encoded on 3 bytes but cannot be since UChar are 2 bytes long.
    // This is really dirty but the other solution would be to change the UChar type to a 24 or 32 bit representation
    std::string comp_str = str.substr(0, 3);
    if (diac_pos == DiacriticPosition::DIACRITIC_POSITION_FOLLOWING
    && curlen == 2
    && candidate == "\xE2\x80"
    && comp_str == "\xE2\x80\xBF") {
      str = "\xE2\x80" + str.substr(3, (int) str.size()-3);
    }
    // std::clog << "candidate diacritic: " << candidate;
    found = is_diacritic(candidate, diac_pos);
    // std::clog << " " << (found?"true":"false") << std::endl;
    --curlen;
  }

  if (!found) {
    return 0;
  } else {
    str.erase(0, candidate.size());
    return get_diacritic(candidate);
  }
}

std::string Ipa::approximate_ipas(
    const std::vector<roots::phonology::ipa::Ipa*>& ipa,
    const std::string& separator) {
  return convert_ipas(ipa, separator);
}

std::string Ipa::approximate_phoneme(const roots::phonology::Phoneme& phoneme) {
  std::vector<Ipa*> ipa;
  for (int ipaIdx = 0; ipaIdx < phoneme.get_n_ipa(); ++ipaIdx) {
    ipa.push_back(const_cast<Ipa*>(&phoneme.get_ipa(ipaIdx)));
  }
  return approximate_ipas(ipa);
}

std::string Ipa::convert_ipas(const std::vector<Ipa*>& ipa,
                              const std::string& separator) {
  std::string corresp_label = "";

  for (std::vector<Ipa*>::const_iterator iter = ipa.begin(); iter != ipa.end();
       ++iter) {
    if (corresp_label != "") {
      corresp_label += separator;
    }
    corresp_label += (*iter)->to_string();
  }
  return corresp_label;
}

std::string Ipa::convert_phoneme(const roots::phonology::Phoneme& phoneme) {
  std::vector<Ipa*> ipa;
  for (int ipaIdx = 0; ipaIdx < phoneme.get_n_ipa(); ++ipaIdx) {
    ipa.push_back(const_cast<Ipa*>(&phoneme.get_ipa(ipaIdx)));
  }
  return convert_ipas(ipa);
}

int Ipa::get_max_symbol_string_length() {
  if (max_symbol_string_length == -1) {
    int maxLabelLen = 0;
    std::vector<std::string>* vec = get_alphabet_set_static();
    std::vector<std::string>::const_iterator iter = vec->begin();
    while (iter != vec->end()) {
      int curlen = (*iter).size();
      if (curlen > maxLabelLen) maxLabelLen = curlen;
      iter++;
    }
    max_symbol_string_length = maxLabelLen;
    delete vec;
  }

  return max_symbol_string_length;
}

int Ipa::get_max_diacritic_string_length() {
  if (max_diacritic_string_length == -1) {
    int maxLabelLen = 0;
    std::vector<std::string>* vec = get_all_diacritics_set_static();
    std::vector<std::string>::const_iterator iter = vec->begin();
    while (iter != vec->end()) {
      int curlen = (*iter).size();
      if (curlen > maxLabelLen) maxLabelLen = curlen;
      iter++;
    }
    max_diacritic_string_length = maxLabelLen;
    delete vec;
  }

  return max_diacritic_string_length;
}
}
}
}  // END OF NAMESPACES
