/*
    This file is part of Roots.

    Roots is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Roots is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Roots.  If not, see <http://www.gnu.org/licenses/>.

    Copyright (c) 2013, 2014 IRISA
*/


#ifndef IPA_CONSTANTS_H
#define IPA_CONSTANTS_H

#include <boost/assign/list_of.hpp> // for 'map_list_of()'


namespace roots { namespace phonology { namespace ipa {

  static const int CLASS_VOWEL_BACKNESS = 0;
  static const int CLASS_VOWEL_HEIGHT = 1;
  static const int CLASS_CONSONANT_MANNER = 2;
  static const int CLASS_CONSONANT_PLACE = 3;

  //static const int PLACES_LENGTH;
  //static const std::string PLACES[];
  enum Place
  {
	  PLACE_UNDEF	 		= 0,
	  PLACE_BILABIAL 		= 1,
	  PLACE_LABIODENTAL 		= 2,
	  PLACE_DENTAL 			= 3,
	  PLACE_ALVEOLAR 		= 4,
	  PLACE_POSTALVEOLAR 		= 5,
	  PLACE_RETROFLEX 		= 6,
	  PLACE_ALVEOLOPALATAL 		= 7,
	  PLACE_PALATOALVEOLAR 		= 8,
	  PLACE_PALATAL 		= 9,
	  PLACE_PALATOVELAR 		= 10,
	  PLACE_VELAR 			= 11,
	  PLACE_UVULAR 			= 12,
	  PLACE_PHARYNGEAL		= 13,
	  PLACE_EPIGLOTTAL 		= 14,
	  PLACE_GLOTTAL 		= 15,
	  PLACE_LABIOVELAR 		= 16,
	  PLACE_LABIOPALATAL 		= 17
  };

  static const int PLACES_LENGTH = 18;
  static const std::string PLACES[] = {
		  "undef","bilabial","labiodental","dental","alveolar","postalveolar",
		  "palatoalveolar", "retroflex", "palatal", "velar", "uvular",
		  "pharyngeal", "epiglottal", "glottal", "labiovelar",
		  "labiopalatal", "alveolopalatal", "palatovelar"
  };

  //static const int MANNERS_LENGTH;
  //static const std::string MANNERS[];
  enum Manner
  {
	  MANNER_UNDEF	 			= 0,
	  MANNER_NASAL 				= 10,
	  MANNER_PLOSIVE 			= 20,
	  MANNER_FRICATIVE 			= 30,
	  MANNER_LATERAL_FRICATIVE 		= 31,
	  MANNER_LATERAL_APPROXIMANT 		= 40,
	  MANNER_APPROXIMANT 			= 46,
	  MANNER_TRILL 				= 50,
	  MANNER_FLAP 				= 60,
	  MANNER_LATERAL_FLAP			= 61,
	  MANNER_TAP 				= 70,
	  MANNER_CLICK 				= 80,
	  MANNER_VOICED_IMPLOSIVE 		= 90,
	  MANNER_EJECTIVE 			= 100
  };

  inline std::map<Manner, std::string> init_manners() {
    std::map<Manner, std::string> m;
    m[MANNER_UNDEF] = "undef";
    m[MANNER_NASAL] = "nasal";
    m[MANNER_PLOSIVE] = "plosive";
    m[MANNER_FRICATIVE] = "fricative";
    m[MANNER_LATERAL_APPROXIMANT] = "approximant";
    m[MANNER_TRILL] = "trill";
    m[MANNER_FLAP] = "flap";
    m[MANNER_TAP] = "tap";
    m[MANNER_LATERAL_FRICATIVE] = "lateral_fricative";
    m[MANNER_LATERAL_APPROXIMANT] = "lateral_approximant";
    m[MANNER_LATERAL_FLAP] = "lateral_flap";
    m[MANNER_CLICK] = "click";
    m[MANNER_VOICED_IMPLOSIVE] = "voiced_implosive";
    m[MANNER_EJECTIVE] = "ejective";
    return m;
  }
  static std::map<Manner, std::string> MANNERS = init_manners();
  

  //static const int APERTURES_LENGTH;
  //static const std::string APERTURES[];
  enum Aperture
  {
	  APERTURE_UNDEF	 	= 0,
	  APERTURE_CLOSE 		= 1,
	  APERTURE_NEAR_CLOSE  	 	= 2,
	  APERTURE_CLOSE_MID 	 	= 3,
	  APERTURE_MID 		 	= 4,
	  APERTURE_OPEN_MID 	 	= 5,
	  APERTURE_NEAR_OPEN 	 	= 6,
	  APERTURE_OPEN 		= 7
  };

  static const int APERTURES_LENGTH = 8;
  static const std::string APERTURES[] = {
		  "undef","close", "near_close", "close_mid", "mid", "open_mid",
		  "near_open", "open"
  };

  //static const int SHAPES_LENGTH;
  //static const std::string SHAPES[];
  enum Shape
  {
	  SHAPE_UNDEF			= 0,
	  SHAPE_FRONT			= 1,
	  SHAPE_NEAR_FRONT	 	= 2,
	  SHAPE_CENTRAL		 	= 3,
	  SHAPE_NEAR_BACK		= 4,
	  SHAPE_BACK			= 5
  };

  static const int SHAPES_LENGTH = 6;
  static const std::string SHAPES[] = {
		  "undef","front", "near_front", "central", "near_back", "back"
  };

  //static const int DIACRITICS_LENGTH;
  //static const std::string DIACRITICS[];
  enum Diacritic
  {
	  DIACRITIC_UNDEF	 					= 0,
	  DIACRITIC_VOICELESS 					= 1,
	  DIACRITIC_VOICED 						= 2,
	  DIACRITIC_ASPIRATED 					= 3,
	  DIACRITIC_MORE_ROUNDED 				= 4,
	  DIACRITIC_LESS_ROUNDED 				= 5,
	  DIACRITIC_ADVANCED 					= 6,
	  DIACRITIC_RETRACTED 					= 7,
	  DIACRITIC_CENTRALIZED 				= 8,
	  DIACRITIC_MID_CENTRALIZED 			= 9,
	  DIACRITIC_SYLLABIC 					= 10,
	  DIACRITIC_NON_SYLLABIC 				= 11,
	  DIACRITIC_RHOTICITY 					= 12,
	  DIACRITIC_BREATHY_VOICE 				= 13,
	  DIACRITIC_CREAKY_VOICE  				= 14,
	  DIACRITIC_LINGUOLABIAL 				= 15,
	  DIACRITIC_LABIALIZED 					= 16,
	  DIACRITIC_PALATALIZED 				= 17,
	  DIACRITIC_VELARIZED 					= 18,
	  DIACRITIC_PHARYNGEALIZED 				= 19,
	  DIACRITIC_VELARIZED_OR_PHARYNGEALIZED 		= 20,
	  DIACRITIC_RAISED 						= 21,
	  DIACRITIC_LOWERED 					= 22,
	  DIACRITIC_ADVANCED_TONGUE_ROOT 		= 23,
	  DIACRITIC_RETRACTED_TONGUE_ROOT 		= 24,
	  DIACRITIC_DENTAL 						= 25,
	  DIACRITIC_APICAL 						= 26,
	  DIACRITIC_LAMINAL 					= 27,
	  DIACRITIC_NASALIZED 					= 28,
	  DIACRITIC_NASAL_RELEASE 				= 29,
	  DIACRITIC_LATERAL_RELEASE 			= 30,
	  DIACRITIC_NO_AUDIBLE_RELEASE 			= 31,
	  DIACRITIC_LONG                        = 32,
	  DIACRITIC_MID_OR_FIRST_TONE			= 33,
	  DIACRITIC_HIGH_OR_SECOND_TONE			= 34,
	  DIACRITIC_RISING_OR_THIRD_TONE		= 35,
	  DIACRITIC_LOW_OR_FOURTH_TONE			= 36,
	  DIACRITIC_PRIMARY_STRESS              = 37,
	  DIACRITIC_SHORT						= 38,
	  DIACRITIC_SECONDARY_STRESS            = 39,
		DIACRITIC_SYLLABLE_BREAK = 40,
		DIACRITIC_SYLLABLE_LINKING = 41
  };

  static const int DIACRITICS_LENGTH = 42;
  static const std::string DIACRITICS[] = {
		  "undef","voiceless", "voiced", "aspirated", "more_rounded", "less_rounded",
		  "advanced", "retracted", "centralized", "mid_centralized", "syllabic",
		  "non_syllabic", "rhoticity", "breathy_voiced", "creaky_voiced", "linguolabial",
		  "labialized", "palatalized", "velarized", "pharyngealized", "velarized_or_pharyngealized",
		  "raised", "lowered", "advanced_tongue_root", "retracted_tongue_root",
		  "dental", "apical", "laminal", "nasalized",
		  "nasal_release", "lateral_release", "no_audible_release",
		  "long", "mid_or_first_tone",
		  "high_or_second_tone","rising_or_third_tone","low_or_fourth_tone",
		  "primary_stress", "short","secondary_stress", "syllable_break",
			"syllable_linking"
  };

  enum DiacriticPosition {
    DIACRITIC_POSITION_ANY = 0,
    DIACRITIC_POSITION_PRECEDING = 1,
    DIACRITIC_POSITION_FOLLOWING = 2
  };

  static const int DIACRITIC_POSITIONS_LENGTH = 3;
  static const std::string DIACRITIC_POSITIONS[] = {
		  "any", "preceding", "following"
  };

  enum StressLevel {
    STRESS_LEVEL_NONE = 0,
    STRESS_LEVEL_LOW = 1,
    STRESS_LEVEL_HIGH = 2
  };

  //static const int CATEGORIES_LENGTH;
  //static const std::string CATEGORIES[];

  enum Category {
	  CATEGORY_VOWEL 			= 0,
	  CATEGORY_CONSONANT 			= 1,
          CATEGORY_SYLLABIC_CONSONANT           = 2,
          CATEGORY_NASAL 			= 3,
	  CATEGORY_FRICATIVE 			= 4,
	  CATEGORY_PULMONIC 			= 5,
	  CATEGORY_NON_PULMONIC 		= 6,
	  CATEGORY_CO_ARTICULATED 		= 7,
	  CATEGORY_PLOSIVE 			= 8,
	  CATEGORY_APPROXIMANT 			= 9,
	  CATEGORY_TRILL 			= 10,
	  CATEGORY_TAP 				= 11,
	  CATEGORY_FLAP 			= 12,
	  CATEGORY_LATERAL 			= 13,
	  CATEGORY_LIQUID 			= 14,
	  CATEGORY_SEMI_VOWEL 			= 15,
	  CATEGORY_LABIAL 			= 16,
	  CATEGORY_CORONAL 			= 17,
	  CATEGORY_DORSAL 			= 18,
	  CATEGORY_RADICAL 			= 19,
	  CATEGORY_LARYNGEAL 			= 20,
	  CATEGORY_BILABIAL 			= 21,
	  CATEGORY_LABIO_DENTAL 		= 22,
	  CATEGORY_DENTAL 			= 23,
	  CATEGORY_ALVEOLAR 			= 24,
	  CATEGORY_PALATO_ALVEOLAR 		= 25,
	  CATEGORY_RETROFLEX 			= 26,
	  CATEGORY_PALATAL 			= 27,
	  CATEGORY_VELAR 			= 28,
	  CATEGORY_UVULAR 			= 29,
	  CATEGORY_PHARYNGEAL 			= 30,
	  CATEGORY_EPIGLOTTAL 			= 31,
	  CATEGORY_GLOTTAL 			= 32,
	  CATEGORY_CLICK 			= 33,
	  CATEGORY_VOICED_IMPLOSIVE 		= 34,
	  
	  CATEGORY_FRONT 			= 35,
	  CATEGORY_NEAR_FRONT 			= 36,
	  CATEGORY_CENTRAL 			= 37,
	  CATEGORY_NEAR_BACK 			= 38,
	  CATEGORY_BACK 			= 39,
	  
	  CATEGORY_CLOSE 			= 40,
	  CATEGORY_NEAR_CLOSE 			= 41,
	  CATEGORY_CLOSE_MID 			= 42,
	  CATEGORY_MID 				= 43,
	  CATEGORY_OPEN_MID 			= 44,
	  CATEGORY_NEAR_OPEN 			= 45,
	  CATEGORY_OPEN 			= 46,
	  
	  CATEGORY_ROUNDED 			= 47,
	  CATEGORY_VOICED 			= 48,
	  CATEGORY_UNVOICED 			= 49,
	  CATEGORY_AFFRICATE			= 50,
	  CATEGORY_DOUBLE 			= 51,
	  CATEGORY_GLIDE 			= 52
  };

  enum Classification {
	  CLASSIFICATION_VOWEL_BACKNESS 	= 0,
	  CLASSIFICATION_VOWEL_HEIGHT 		= 1,
	  CLASSIFICATION_CONSONANT_MANNER 	= 2,
	  CLASSIFICATION_CONSONANT_PLACE 	= 3
  };

  static const int CATEGORIES_LENGTH = 53;
  
  inline std::vector<std::string> init_categories() {
    std::vector<std::string> m = boost::assign::list_of
		  ("vowel")("consonant")("syllabic-consonant")("nasal")("fricative")
		  ("pulmonic")("non-pulmonic")("co-articulated")("plosive")
		  ("approximant")("trill")("tap")("flap")
		  ("lateral")("liquid")("semi-vowel")("labial")
		  ("coronal")("dorsal")("radical")("laryngeal")
		  ("bilabial")("labio-dental")("dental")("alveolar")
		  ("palato-alveolar")("retroflex")("palatal")("velar")
		  ("uvular")("pharyngeal")("epiglottal")("glottal")
		  ("click")("voiced-implosive")("front")("near-front")
		  ("central")("back")("near-back")("close")
		  ("near-close")("mid")("close-mid")("open-mid")
		  ("open")("near-open")("rounded")("voiced")
		  ("unvoiced")("affricate")("double")("glide");		
    return m;
  }  
  static const std::vector<std::string> CATEGORIES = init_categories();

} } }

#endif
