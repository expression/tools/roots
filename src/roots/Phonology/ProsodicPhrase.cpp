/*
    This file is part of Roots.

    Roots is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Roots is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Roots.  If not, see <http://www.gnu.org/licenses/>.

    Copyright (c) 2013, 2014 IRISA
*/


#include "ProsodicPhrase.h"

namespace roots
{

namespace phonology
{
namespace prosodicphrase
{

ProsodicPhrase::ProsodicPhrase(const bool noInit):roots::tree::Tree<ProsodicPhraseNode>(noInit)
{
	_phonemeSequence = NULL;
}

ProsodicPhrase::ProsodicPhrase(const ProsodicPhrase &ProsodicPhrase) : roots::tree::Tree<ProsodicPhraseNode>(ProsodicPhrase)
{
	_phonemeSequence = ProsodicPhrase.get_phoneme_sequence();
}


ProsodicPhrase::~ProsodicPhrase()
{
	// Assume wordSequence is destroyed elsewhere after.
}

ProsodicPhrase *ProsodicPhrase::clone() const
{
	return new ProsodicPhrase(*this);
}


ProsodicPhrase& ProsodicPhrase::operator=(const ProsodicPhrase &node)
{
	roots::tree::Tree<ProsodicPhraseNode>::operator=(node);

	_phonemeSequence = node.get_phoneme_sequence();

	return *this;
}

const std::string ProsodicPhrase::get_phoneme_sequence_id() const 
{
  if(_phonemeSequence != NULL)
    {
      return _phonemeSequence->get_id();
    }
  else
    {
      return "";
    }
};

std::vector<int> ProsodicPhrase::get_phoneme_index_array()
{
	// Traverse the tree to get indexes of phonemes
	std::vector<int> phonemeIndexArray = std::vector<int>();
	
	std::vector<ProsodicPhraseNode*> leaves;
	leaves = this->get_leaf_nodes();
	
	int leafIndex = 0;
	int nLeaves = leaves.size();
	while(leafIndex < nLeaves)
	{
		const std::vector<int>& pIndexArray = leaves[leafIndex]->get_phoneme_index_array();
		phonemeIndexArray.insert(phonemeIndexArray.end(), pIndexArray.begin(), pIndexArray.end());
		++leafIndex;
	}

	return phonemeIndexArray;
}

int ProsodicPhrase::get_first_target_index() const
{
	ProsodicPhraseNode *leftLeaf = static_cast<ProsodicPhraseNode*>(rootNode->get_first_leaf());
	if(leftLeaf == NULL)
	{
		return -1;
	}
	return leftLeaf->get_start();
}

int ProsodicPhrase::get_last_target_index() const
{
	ProsodicPhraseNode *rightLeaf = static_cast<ProsodicPhraseNode*>(rootNode->get_last_leaf());
	if(rightLeaf == NULL)
	{
		return -1;
	}
	return rightLeaf->get_end();
}

ProsodicPhraseNode* ProsodicPhrase::get_phoneme_prosodic_phrase_node(int phonemeIndex) const
{
	std::vector<ProsodicPhraseNode*> leaves;

	if(phonemeIndex <0)
	{
		std::stringstream ss;
		ss << "phoneme index out of bounds!";
		throw RootsException(__FILE__, __LINE__, ss.str().c_str());
	}

	leaves = this->get_leaf_nodes();
	
	int leafIndex = 0;
	int nLeaves = leaves.size();
	
	bool found = false;
	while( !found && (leafIndex < nLeaves) )
	{
		//cout << "start="<<(*iter).get_start()<<", phonemeIndex="<<phonemeIndex<<", end="<<(*iter).get_end()<<endl;
		if( (leaves[leafIndex]->get_start()<=phonemeIndex) && (phonemeIndex<=leaves[leafIndex]->get_end()) )
		{
			found = true;
		}
		else
		{
			++leafIndex;
		}		
	}
	
	if(!found)
	{
		std::stringstream ss;
		ss << "phoneme index out of bounds!";
		throw RootsException(__FILE__, __LINE__, ss.str().c_str());
	}
	
	return leaves[leafIndex];
}


std::vector<ProsodicPhraseNode*> ProsodicPhrase::get_prosodic_phrase_node_path_to_phoneme(int phonemeIndex) const
{
	std::vector<ProsodicPhraseNode*> vec, vecout;

	ProsodicPhraseNode *node = get_phoneme_prosodic_phrase_node(phonemeIndex);
	if(node != NULL)
	{
		ProsodicPhraseNode *parent = node;
		
		while(parent != NULL)
		{
			vec.push_back(static_cast<ProsodicPhraseNode*>(parent));
			parent = static_cast<ProsodicPhraseNode*>(node->get_parent());
		}
		
	}
	// reverse the vector
	vecout.insert(vecout.begin(), vec.rbegin(), vec.rend());
	
	return vecout;
}



std::string ProsodicPhrase::to_string( int level) const
{
	std::stringstream ss;

	switch (level)
	{
		case 0:
		default:
			ss << "digraph ProsodicPhrase {\n";
			ss << "ordering=out\n";
			ss << "node [shape=none]\n";
			ss << rootNode->output_dot();
			ss << "}\n";
			break;
	}
	return std::string(ss.str().c_str());
}

void ProsodicPhrase::deflate(RootsStream * stream, bool is_terminal, int list_index)
{
	roots::tree::Tree<ProsodicPhraseNode>::deflate(stream,false,list_index);

	stream->append_unicodestring_content("phoneme_sequence_id", this->get_phoneme_sequence_id());

	if(is_terminal)
		stream->close_object();
}


	void ProsodicPhrase::inflate(RootsStream * stream, bool is_terminal, int list_index)
	{
		roots::tree::Tree<ProsodicPhraseNode>::inflate(stream,false,list_index);
			
		//stream->open_children();
		std::string id = stream->get_unicodestring_content("phoneme_sequence_id");
		roots::sequence::PhonemeSequence *phonemeSeq = (roots::sequence::PhonemeSequence *)stream->get_object_ptr(id);

		if(phonemeSeq == NULL)
		{
			std::stringstream ss;
			ss << "cannot find sequence with id="<< id << " while inflating ProsodicPhrase object!";
			std::cerr << ss.str().c_str() << std::endl;
			throw RootsException(__FILE__, __LINE__, ss.str().c_str());
		}
		this->set_phoneme_sequence(phonemeSeq);

		if(is_terminal) stream->close_children();
	}

	ProsodicPhrase * ProsodicPhrase::inflate_object(RootsStream * stream, int list_index)
	{
		ProsodicPhrase *t = new ProsodicPhrase(true);
		t->inflate(stream,true,list_index);
		return t;	
	}


std::ostream& operator<<(std::ostream& out, ProsodicPhrase& ProsodicPhrase)
{
	out << ProsodicPhrase.to_string() << std::endl;
	return out;
}


} } } // END OF NAMESPACES
