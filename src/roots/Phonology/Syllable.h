/*
    This file is part of Roots.

    Roots is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Roots is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Roots.  If not, see <http://www.gnu.org/licenses/>.

    Copyright (c) 2013, 2014 IRISA
*/

#ifndef SYLLABLE_H
#define SYLLABLE_H

#include "../BaseItem.h"  // Base class: roots::BaseItem

#include "../Acoustic/Allophone.h"
#include "../Sequence.h"
#include "../Sequence/PhonemeSequence.h"
#include "./Ipa.h"
#include "./Phoneme.h"

#include "./SyllableException.h"

#include <algorithm>
#include <boost/assign/list_inserter.hpp>
#include <boost/assign/list_of.hpp>  // for 'map_list_of()'

namespace roots {

namespace phonology {

namespace syllable {

typedef ipa::StressLevel ProminenceTag;

class Syllable : public roots::BaseItem {
 public:
  Syllable(const bool noInit = false);
  Syllable(const Syllable&);
  virtual ~Syllable();
  /**
   * @brief Duplicates the syllable
   * @return A clone of the syllable
   */
  virtual Syllable* clone() const;

  Syllable& operator=(const Syllable& syl);

  void set_coda_indices(const std::vector<int>& _coda) { this->_coda = _coda; }
  void set_nucleus_indices(const std::vector<int>& _nucleus) {
    this->_nucleus = _nucleus;
  }
  void set_onset_indices(const std::vector<int>& _onset) {
    this->_onset = _onset;
  }

  void set_coda(const std::vector<Phoneme*>& _coda);
  void set_nucleus(const std::vector<Phoneme*>& _nucleus);
  void set_onset(const std::vector<Phoneme*>& _onset);

  void set_phoneme_sequence(sequence::PhonemeSequence* _phonemeSequence);

  void set_related_sequence(sequence::Sequence* _relatedSequence);

  void set_prosodic_class(int _prosodicClass) {
    this->_prosodicClass = _prosodicClass;
  }

  /**
   * @brief Return the indices of the coda phonemes in the phoneme sequence
   * @return A read-only reference to the vector of indices.
   */
  const std::vector<int>& get_coda_indices() const { return _coda; }

  /**
   * @brief Return the indices of the nucleus phonemes in the phoneme sequence
   * @return A read-only reference to the vector of indices.
   */
  const std::vector<int>& get_nucleus_indices() const { return _nucleus; }

  /**
  * @brief Return the indices of the onset phonemes in the phoneme sequence
  * @return A read-only reference to the vector of indices.
  */
  const std::vector<int>& get_onset_indices() const { return _onset; }

  /**
  * @brief Return the coda phonemes from the phoneme sequence
  * @return A vector of phoneme pointers
  */
  std::vector<Phoneme*> get_coda() const;

  /**
  * @brief Return the nucleus phonemes from the phoneme sequence
  * @return A vector of phoneme pointers
  */
  std::vector<Phoneme*> get_nucleus() const;

  /**
  * @brief Return the onset phonemes from the phoneme sequence
  * @return A vector of phoneme pointers
  */
  std::vector<Phoneme*> get_onset() const;

  bool has_related_sequence() const { return (get_related_sequence() != NULL); }

  /**
   * @brief Return the number of phonemes in the coda of the current syllable.
   * @return A positive or null integer.
   */
  size_t get_coda_length() const { return _coda.size(); }

  /**
  * @brief Return the number of phonemes in the nucleus of the current syllable.
  * @return A positive or null integer.
  */
  size_t get_nucleus_length() const { return _nucleus.size(); }

  /**
  * @brief Return the number of phonemes in the onset of the current syllable.
  * @return A positive or null integer.
  */
  size_t get_onset_length() const { return _onset.size(); }

  sequence::PhonemeSequence* get_phoneme_sequence() const {
    return _phonemeSequence;
  }

  virtual sequence::Sequence* get_related_sequence() const {
    return get_phoneme_sequence();
  }

  /**
   * @brief Modifies the current syllable to take into account the insertion of
   *an element into the related sequence
   * @param index index of the element added in the related sequence
   **/
  void insert_related_element(int index);

  /**
   * @brief Modifies the current syllable to take into account the deletion of
   *an element into the related sequence
   * @param index index of the element removed from the related sequence
   **/
  void remove_related_element(int index);

  const std::string get_phoneme_sequence_id() const;

  int get_prosodic_class() const { return _prosodicClass; }

  bool in_onset(int phoneme_index);

  bool in_nucleus(int phoneme_index);

  bool in_coda(int phoneme_index);

  bool in_onset(const Phoneme* ph);

  bool in_nucleus(const Phoneme* ph);

  bool in_coda(const Phoneme* ph);

  /**
   * @brief Test if the current syllable is stressed.
   * @return True if the syllable is prominent or if any of the constituent
   * phonemes is stressed, false otherwise.
   */
  bool is_stressed() const;

  /**
   * @brief Return the stress level of the current syllable.
   * @return A positive or null integer: 0 is not stressed, the greater the more
   * stressed otherwise.
   */
  ipa::StressLevel get_stress_level() const;

  /**
   * @brief Removes the stress information carried by phonemes and directly
   * store an information of the current syllable.
   * @warning This operation modifies the underlying phonemes.
   */
  void fetch_phoneme_stress();

  /**
   * @brief Check if the current syllable is open (no coda)
   * @return true if the syllable is open, false otherwise
   **/
  bool is_open() const { return (_coda.size() == 0); }

  /**
   * @brief Check if the current syllable is closed (the coda exists)
   * @return true if the syllable is closed, false otherwise
   **/
  bool is_closed() const { return (_coda.size() > 0); }

  void translate_index(int offset);

 public:
  virtual void from_phoneme_indices(const std::vector<int>&);
  static Syllable from_phoneme_indices(
      sequence::PhonemeSequence* _phonemeSequence, const std::vector<int>&);

  /**
   * @brief Set the nucleus, onset and coda based on the caracteristics of the
   * given input phonemes.
   * @param phonemes Vector of phonemes which are part of the current syllable.
   * @warning Make sure that of phonemes are all part of one unique phoneme
   * sequence.
   */
  virtual void from_phonemes(const std::vector<Phoneme*>& phonemes);

  /**
   * @brief Return the list of indices in the phoneme sequence for each phoneme
   * in the current syllable.
   * @return A vector of phoneme indices.
   */
  std::vector<int> to_phoneme_indices() const;

  /**
   * @brief Return the list of phonemes in the current syllable.
   * @return A vector of phoneme pointers.
   */
  std::vector<Phoneme*> to_phonemes() const;

  virtual std::string to_string(int level = 0) const;

  /**
   * @brief Write the entity into a RootsStream
   * @param stream the stream from which we inflate the element
   * @param is_terminal indicates that the node is terminal (true by default)
   * @param parentNode
   */
  virtual void deflate(RootsStream* stream, bool is_terminal = true,
                       int list_index = -1);
  /**
   * @brief Extracts the entity from the stream
   * @param stream the stream from which we inflate the element
   */
  virtual void inflate(RootsStream* stream, bool is_terminal = true,
                       int list_index = -1);
  /**
   * @brief Extracts the entity from the stream
   * @param stream the stream from which we inflate the element
   */
  static Syllable* inflate_object(RootsStream* stream, int list_index = -1);
  /**
   * @brief returns the XML tag name value for current object
   * @return string constant representing the XML tag name
   */
  virtual std::string get_xml_tag_name() const { return xml_tag_name(); };
  /**
   * @brief returns the XML tag name value for current class
   * @return string constant representing the XML tag name
   */
  static std::string xml_tag_name() { return "syllable"; };
  /**
   * @brief returns classname for current object
   * @return string constant representing the classname
   */
  virtual std::string get_classname() const { return classname(); };
  /**
   * @brief returns classname for current class
   * @return string constant representing the classname
   */
  static std::string classname() { return "Phonology::Syllable"; };
  /**
   * @brief returns display color for current object
   * @return string constant representing the pgf display color
   */
  virtual std::string get_pgf_display_color() const {
    return pgf_display_color();
  };
  /**
   * @brief returns display color for current class
   * @return string constant representing the pgf display color
   */
  static std::string pgf_display_color() { return "orange!25"; };

  ProminenceTag get_prominent() const { return _prominent; }

  bool is_prominent() const {
    return (get_prominent() != ipa::STRESS_LEVEL_NONE);
  }

  void set_prominent(ProminenceTag _prominent) {
    this->_prominent = _prominent;
  }

 private:
  std::vector<int>
      _onset; /**< index array of phoneme ranks defining the onset of the
                 syllable */
  std::vector<int> _nucleus; /**< index array of phoneme ranks defining the
                                nucleus of the syllable */
  std::
      vector<int>
          _coda; /**< index array of phoneme ranks defining the coda of the
                    syllable */
  sequence::PhonemeSequence*
      _phonemeSequence;     /**< reference on the phoneme/allophone sequence */
  ProminenceTag _prominent; /**< Prominence value for the current syllable */
  int _prosodicClass;       /**< ID of the prosodic class */

  static std::map<syllable::ProminenceTag, std::string>
      prominencetagToString;  ///< Static map to transform a prominence tag to a
                              ///string
  static std::map<syllable::ProminenceTag, ipa::Diacritic>
      prominencetagToDiacritic;  ///< Static map to transform stress information
                                 ///to an IPA representation
  static std::map<std::string, syllable::ProminenceTag>
      stringToProminencetag;  ///< Static map to transform a string to a
                              ///prominence tag
};

/*template<> void Syllable::_from_phoneme<Acoustic::Allophone> (const
  std::vector<int> &);
  template<> void Syllable::_from_phoneme<Phoneme> (const std::vector<int> &);*/
}
}
}

#endif  // SYLLABLE_H
