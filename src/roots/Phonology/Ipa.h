/*
    This file is part of Roots.

    Roots is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Roots is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Roots.  If not, see <http://www.gnu.org/licenses/>.

    Copyright (c) 2013, 2014 IRISA
*/

#ifndef IPA_H_
#define IPA_H_

#include <map>
#include "../BaseItem.h"
#include "../common.h"
#include "Ipa/Alphabet.h"
#include "IpaConstants.h"
#include "IpaException.h"

#include <boost/assign/list_inserter.hpp>
#include <boost/assign/list_of.hpp>  // for 'map_list_of()'
#include <boost/serialization/vector.hpp>

namespace roots {
namespace phonology {
namespace ipa {

class Ipa : public BaseItem, public Alphabet {
  /****************************************************************************
   * BaseItem part
   ***************************************************************************/

 public:
  Ipa(const bool noInit = false);
  Ipa(Manner manner, Place place, Aperture aperture, Shape shape,
      bool is_affricate, bool is_voiced, bool is_rounded, bool is_double,
      const std::vector<Diacritic>& diacritic);
  Ipa(Manner manner, Place place, Aperture aperture, Shape shape,
      bool is_affricate, bool is_voiced, bool is_rounded, bool is_double,
      const std::set<Diacritic>& diacritic);
  Ipa(const Ipa& ipa);
  virtual ~Ipa();
  /**
   * @brief Duplicates the ipa object
   * @return A clone of the ipa object
   */
  virtual Ipa* clone() const;
  Ipa& operator=(const Ipa& ipa);

  bool operator==(const Ipa& ipa) const;
  bool operator!=(const Ipa& ipa) const;

  /**
   * @brief Compute the distance between the current IPA and another
   * @param ipa IPA to compute the distance with.
   * @return A distance, ie, a positive number which is 0 is the IPAs are
   * equivalent.
   */
  virtual double compute_dissimilarity(const roots::Base* b) const;

  Aperture get_aperture() const;
  std::vector<Diacritic> get_diacritics() const;
  const std::set<Diacritic>& get_diacritics_set() const;

  static Diacritic get_diacritic_from_unicode(UChar diac);
  static UChar get_unicode_from_diacritic(Diacritic diac);
  static Ipa* get_ipa_from_diacritic(Diacritic diac);

  bool get_is_affricate() const;
  bool get_is_double() const;
  bool get_is_rounded() const;
  bool get_is_voiced() const;
  Manner get_manner() const;
  Place get_place() const;
  Shape get_shape() const;
  UChar get_unicode();
  std::vector<UChar> get_unicode_diacritics();
  const std::set<UChar>& get_unicode_diacritics_set();
  Classification get_vowel_classification() const;
  Classification get_consonant_classification() const;

  void set_aperture(Aperture aperture);
  void set_diacritic(Diacritic dia);
  void set_diacritics(const std::vector<Diacritic>& dia);
  void set_diacritics(const std::set<Diacritic>& dia);
  void set_is_affricate(bool is_affricate);
  void set_is_double(bool is_double);
  void set_is_rounded(bool is_rounded);
  void set_is_voiced(bool is_voiced);
  void set_manner(Manner manner);
  void set_place(Place place);
  void set_shape(Shape shape);
  void set_unicode(UChar unicode);
  void set_unicode_diacritics(const std::vector<UChar>& unicode_diacritic);
  void set_unicode_diacritics(const std::set<UChar>& unicode_diacritic);
  void set_vowel_classification(Classification aClass);
  void set_consonant_classification(Classification aClass);

  void add_diacritic(Diacritic dia);
  void add_diacritics(const std::vector<Diacritic>& dia);
  void add_diacritics(const std::set<Diacritic>& dia);

  /**
   * @brief Try to remove a given diacritic from the current IPA instance
   * @param dia The diacritic to be removed
   * @return True if the diacritic has effectively been removed, false otherwise
   * (the diacritic has not been found)
   */
  bool remove_diacritic(Diacritic dia);

  /**
   * @brief Clear all diacritics in the current IPA instance
   */
  void remove_all_diacritics();

  /**
   * @brief Test if the current instance of IPA is a pure diacritic (no phoneme
   * symbol)
   * @return True if the current IPA is a pure diacritic, false otherwise
   */
  bool is_diacritic() const;

  /**
   * @brief Test if the current instance of IPA is a pure prefix diacritic (no
   * phoneme symbol and prefix)
   * @return True if the current IPA is a pure prefix diacritic, false otherwise
   */
  bool is_preceding_diacritic() const;

  /**
   * @brief Test if the current instance of IPA is a pure suffix diacritic (no
   * phoneme symbol and suffix)
   * @return True if the current IPA is a pure suffix diacritic, false otherwise
   */
  bool is_following_diacritic() const;

  /**
   * @brief Test if the given diacritic is part of the current IPA instance
   * @param dia The diacritic to be tested
   * @return True if the diacritic is found, false otherwise
   */
  bool is_diacritic(Diacritic dia) const;

  /**
   * @brief Test if a given unicode character is a known diacritic
   * @param u The unicode character to be tested
   * @return True if the unicode character is part of the IPA table, false
   * otherwise
   */
  static bool is_diacritic_static(UChar u);

  /**
   * @brief Test if a given diacritic should precede its combined symbol
   * @param u The unicode character to be tested
   * @return True if preceding or indifferent, false otherwise
   */
  static bool is_preceding_diacritic_static(UChar u);

  /**
   * @brief Test if a given diacritic should follow its combined symbol
   * @param u The unicode character to be tested
   * @return True if following or indifferent, false otherwise
   */
  static bool is_following_diacritic_static(UChar u);

  /**
   * @brief Test if a given diacritic should precede its combined symbol
   * @param dia The diacritic to be tested
   * @return True if preceding or indifferent, false otherwise
   */
  static bool is_preceding_diacritic_static(Diacritic dia);

  /**
   * @brief Test if a given diacritic should follow its combined symbol
   * @param dia The diacritic to be tested
   * @return True if following or indifferent, false otherwise
   */
  static bool is_following_diacritic_static(Diacritic dia);

  static Ipa unicode_to_ipa(const UChar& symbol);

  std::vector<bool> in_category(const std::vector<Category>& categories) const;
  static std::vector<bool> symbol_in_category(
      const UChar& symbol,
      const std::vector<Category>&
          categories);  // utilisation d'un masque - méthode de classe
  bool is_a(const Category category) const;
  bool is_one_of(const std::vector<Category>& categories) const;
  bool is_all_of(const std::vector<Category>& categories) const;

  bool is_vowel() const;
  bool is_consonant() const;
  bool is_semi_vowel() const;
  bool is_liquid() const;
  bool is_pulmonic() const;
  bool is_non_pulmonic() const;
  bool is_nasal() const;
  bool is_plosive() const;
  bool is_fricative() const;
  bool is_approximant() const;
  bool is_trill() const;
  bool is_flap() const;
  bool is_lateral() const;
  bool is_non_lateral() const;
  bool is_click() const;
  bool is_voiced_implosive() const;
  bool is_ejective() const;
  bool is_bilabial() const;
  bool is_labiodental() const;
  bool is_dental() const;
  bool is_alveolar() const;
  bool is_palatoalveolar() const;
  bool is_retroflex() const;
  bool is_palatal() const;
  bool is_velar() const;
  bool is_uvular() const;
  bool is_pharyngeal() const;
  bool is_epiglottal() const;
  bool is_glottal() const;
  bool is_front() const;
  bool is_near_front() const;
  bool is_central() const;
  bool is_back() const;
  bool is_near_back() const;
  bool is_close() const;
  bool is_near_close() const;
  bool is_open() const;
  bool is_near_open() const;
  bool is_mid() const;
  bool is_close_mid() const;
  bool is_open_mid() const;

  bool is_syllabic() const;
  bool is_long() const;

  /**
   * @brief Test if the current Ipa object is stressed, whatever the type of
   * stress is.
   * @return True is stressed,
   */
  bool is_stressed() const;

  /**
   * @brief Return the level of stress of the current Ipa object.
   * @return 0 if no stress, a positive integer otherwise. The greater, the more
   * stressed.
   */
  StressLevel get_stress_level() const;

  /**
   * @brief Remove the stress diacritics in any.
   */
  void unset_stress();

  std::string hash_feature() const;
  static bool is_unicode(const UChar& code);
  static bool is_unicode(const icu::UnicodeString& code);

  /**
   * Completes the information of the current Ipa object using unicode and
   * diacritic information.
   * @return returns a new instance of Ipa
   */
  static Ipa from_unicode(
      const icu::UnicodeString&
          code);  // throw (roots::phonology::ipa::IpaException);

  /**
   * Completes the information of the current Ipa object using unicode and
   * diacritic information.
   * @return returns a new instance of Ipa
   */
  static Ipa from_unicode(
      const UChar& symbol,
      const std::vector<UChar>&
          uni_diacritics);  // throw (roots::phonology::ipa::IpaException);

  /**
   * Completes the information of the current Ipa object using unicode and
   * diacritic information.
   * @return returns a new instance of Ipa
   */
  static Ipa from_unicode(
      const UChar& symbol,
      const std::set<UChar>&
          uni_diacritics);  // throw (roots::phonology::ipa::IpaException);

  void from_unicode();

  /**
   * Completes the unicode and diacritics values from the features
   * @notice If no change happened since the last call, no new computation will
   * be run.
   */
  void to_unicode();

  static void init();

  virtual std::string to_string(int level = 0);

  /**
   * @brief Write the entity into a RootsStream
   * @param stream the stream from which we inflate the element
   * @param is_terminal indicates that the node is terminal (true by default)
   * @param parentNode
   */
  virtual void deflate(RootsStream* stream, bool is_terminal = true,
                       int list_index = -1);
  /**
   * @brief Extracts the entity from the stream
   * @param stream the stream from which we inflate the element
   */
  virtual void inflate(RootsStream* stream, bool is_terminal = true,
                       int list_index = -1);
  /**
   * @brief Extracts the entity from the stream
   * @param stream the stream from which we inflate the element
   */
  static Ipa* inflate_object(RootsStream* stream, int list_index = -1);
  /**
   * @brief returns the XML tag name value for current object
   * @return string constant representing the XML tag name
   */
  virtual std::string get_xml_tag_name() const { return xml_tag_name(); };
  /**
   * @brief returns the XML tag name value for current class
   * @return string constant representing the XML tag name
   */
  static std::string xml_tag_name() { return "ipa"; };
  /**
   * @brief returns classname for current object
   * @return string constant representing the classname
   */
  virtual std::string get_classname() const { return classname(); };
  /**
   * @brief returns classname for current class
   * @return string constant representing the classname
   */
  static std::string classname() { return "Phonology::Ipa"; };

  /****************************************************************************
   * Alphabet part
   ***************************************************************************/

  virtual bool is_label(const std::string& label) const;

  virtual std::vector<UChar> get_symbols(const std::string& label);

  /**
   * @brief Test is a given string symbol can be mapped to any diacritic in the
   * current alphabet
   * @param diacritic_str String to tested for parsing
   * @param diac_pos Optional, limit the test of preceding or following
   * diacritics in the alphabet
   * @return bool
   */
  virtual bool is_diacritic(const std::string& diacritic_str,
                            roots::phonology::ipa::DiacriticPosition diac_pos =
                                DIACRITIC_POSITION_ANY) const;

  /**
   * @brief Test if a given diacritic should precede its combined symbol in the
   * current alphabet
   * @param diac The diacritic to be tested
   * @return True if preceding or indifferent, false otherwise
   * @notice By default, no diacritic is supposed to precede its symbol.
   */
  virtual bool is_preceding_diacritic(Diacritic diac) const;

  /**
   * @brief Test if a given diacritic should follow its combined symbol in the
   * current alphabet
   * @param diac The diacritic to be tested
   * @return True if following or indifferent, false otherwise
   * * @notice By default, all diacritics are supposed to follow their symbol.
   */
  virtual bool is_following_diacritic(roots::phonology::ipa::Diacritic) const;

  virtual UChar get_diacritic(const std::string& diacritic_str);

  virtual std::set<UChar> get_diacritic_symbols(const std::string& label);

  virtual std::vector<std::string>* get_alphabet_set();
  virtual std::vector<std::string>* get_all_diacritics_set();

  virtual bool test_ipa_extraction(std::string& str);

  /**
   * @brief Parse a string containing a sequence of alphabet symbols and return
   *the sequence of corresponding IPAs
   * @param str String containing alphabet symbols
   * @warning The input string is modified by the method (matched symbols are
   *consumed).
   * @return A vector of IPAs extract from the string
   **/
  virtual std::vector<Ipa*> extract_ipas(std::string& str);

  /**
   * @brief Parse a string containing a sequence of alphabet symbols and return
   *the sequence of corresponding IPAs
   * @param c_str A C string containing alphabet symbols
   * @return A vector of IPAs extract from the string
   **/
  virtual std::vector<Ipa*> extract_ipas(const char* c_str);

  /**
   * @brief Try to convert an IPA (ot 2 IPAs) into the corresponding symbol in
   *the current alphabet.
   * @param ipa sequence of IPA to be converted
   * @param separator Optional, string separator between alphabet symbols.
   *Default is no separator.
   * @return An alphabet symbol if the correspondence is possible, an empty
   *string otherwise.
   **/
  virtual std::string convert_ipas(const std::vector<Ipa*>& ipa,
                                   const std::string& separator = "");

  /**
   * @brief Approximate an IPA (ot 2 IPAs) into the corresponding symbol in the
   *current alphabet.
   * @param ipa sequence of IPA to be converted
   * @param separator Optional, string separator between alphabet symbols.
   *Default is no separator.
   * @return An alphabet symbol if the correspondence is possible, an empty
   *string otherwise.
   **/
  virtual std::string approximate_ipas(
      const std::vector<roots::phonology::ipa::Ipa*>& ipa,
      const std::string& separator = "");

  /**
   * @brief Try to convert a phoneme into the corresponding symbol in the
   *current alphabet.
   * @param phoneme Phoneme to be converted
   * @return An alphabet symbol if the correspondence is possible, an empty
   *string otherwise.
   **/
  virtual std::string convert_phoneme(const Phoneme& phoneme);

  /**
   * @brief Approximate a phoneme into the most accurate symbol in the current
   *alphabet.
   * @param phoneme Phoneme to be approximated
   * @return An alphabet symbol.
   **/
  virtual std::string approximate_phoneme(const Phoneme& phoneme);
  static void initialize_unicode_to_ipa_map();
  static void initialize_ipa_to_unicode_map();

 protected:
  void clear_features();
  void clear_unicode();
  static std::vector<std::string>* get_alphabet_set_static();
  static std::vector<std::string>* get_all_diacritics_set_static();
  /*static*/ int get_max_symbol_string_length();
  /*static*/ int get_max_diacritic_string_length();

  /**
   * @brief Try to consume the first IPA symbol starting from the left the input
   *string and return it
   * @param str String from which an IPA should be shifted
   * @return The unicode value of an IPA symbol from the current alphabet, 0 if
   *nothing was extracted
   **/
  virtual UChar extract_first_label(std::string& str);

  /**
   * @brief Try to consume the first diacritic symbol starting from the left of
   *the input string and return it
   * @param str String from which a diacritric should be shifted
   * @param direction Optional, 0 for any diacritic, 1 for preceding diacritics
   *only, 2 for following diacritics only. Default is 0.
   * @return The unicode value of an diacritic symbol from the current alphabet
   *if any encountered, 0 string otherwise
   **/
  virtual UChar extract_first_diacritic(
      std::string& str, roots::phonology::ipa::DiacriticPosition diac_pos);

 protected:
  Manner manner;
  Place place;
  Aperture aperture;
  Shape shape;
  Classification vowelClassification;
  Classification consonantClassification;
  bool is_affricate;
  bool is_voiced;
  bool is_rounded;
  bool is_double;
  bool ipa_changed;
  std::set<Diacritic> diacritic;
  UChar unicode;
  std::set<UChar> unicode_diacritic;

  static std::map<UChar, Ipa> unicodeToIpaMap;
  static std::map<std::string, UChar> ipaToUnicodeMap;
};
}
}
}  // END OF NAMESPACES

#endif /* IPA_H_ */
