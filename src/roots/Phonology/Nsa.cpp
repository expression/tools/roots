/*
    This file is part of Roots.

    Roots is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Roots is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Roots.  If not, see <http://www.gnu.org/licenses/>.

    Copyright (c) 2013, 2014 IRISA
*/

#include "Nsa.h"
#include "md5.h"

namespace roots {
namespace phonology {
namespace nsa {

std::map<std::string, Nsa> Nsa::tagToNsaMap = std::map<std::string, Nsa>();
std::map<std::string, std::string> Nsa::nsaToTagMap =
    std::map<std::string, std::string>();

Nsa::Nsa(const bool noInit)
    : BaseItem(noInit),
      tag("silence"),
      category(CATEGORY_SILENCE),
      origin(ORIGIN_UNDEF),
      duration(DURATION_UNDEF) {}

Nsa::Nsa(const std::string& atag, const Category& acategory,
         const Origin& aorigin, const Duration& aduration)
    : tag(atag), category(acategory), origin(aorigin), duration(aduration) {}

Nsa::~Nsa() {}

Nsa* Nsa::clone() const { return new Nsa(*this); }

Nsa::Nsa(const Nsa& nsa) : BaseItem(nsa) {
  clear_feature();
  set_tag(nsa.get_tag());
  set_category(nsa.get_category());
  set_duration(nsa.get_duration());
  set_origin(nsa.get_origin());
}

Nsa& Nsa::operator=(const Nsa& nsa) {
  BaseItem::operator=(nsa);

  clear_feature();
  set_tag(nsa.get_tag());
  set_category(nsa.get_category());
  set_duration(nsa.get_duration());
  set_origin(nsa.get_origin());
  return *this;
}

/**
 * @brief Check if Nsa is a silence
 * @return true if category of the Nsa is silence, false else
 */
bool Nsa::is_silence() const {
  return this->get_category() == CATEGORY_SILENCE;
}

/**
 * @brief Check if Nsa is an inspiration
 * @return true if category of the Nsa is inspiration, false else
 */
bool Nsa::is_inspiration() const {
  return this->get_category() == CATEGORY_INSPIRATION;
}

/**
 * @brief Check if Nsa is a noise
 * @return true if category of the Nsa is noise, false else
 */
bool Nsa::is_noise() const { return this->get_category() == CATEGORY_NOISE; }

/**
 * @brief Check if Nsa is a phonation
 * @return true if category of the Nsa is phonation, false else
 */
bool Nsa::is_phonation() const {
  return this->get_category() == CATEGORY_PHONATION;
}

/**
 * @brief Check if Nsa is a paraverbal cprom
 * @return true if category of the Nsa is cprom_paraverbal, false else
 */
bool Nsa::is_cprom_paraverbal() const {
  return this->get_category() == CATEGORY_CPROM_PARAVERBAL;
}

void Nsa::from_tag() {
  try {
    Nsa nsa = Nsa::from_tag(get_tag());

    set_tag((nsa).get_tag());
    set_category((nsa).get_category());
    set_duration((nsa).get_duration());
    set_origin((nsa).get_origin());

    // delete nsa;
  } catch (NsaException e) {
    // cout << e.what();
    throw NsaException(__FILE__, __LINE__, "report exception\n");
  }
}

Nsa Nsa::from_tag(const std::string& atag) {
  Nsa nsa;
  try {
    nsa = Nsa::tag_to_nsa(atag);
  } catch (NsaException e) {
    //		cout << e.what();
    throw NsaException(__FILE__, __LINE__, "report exception\n");
  }
  return nsa;
}

void Nsa::clear_feature() {
  set_tag("");
  set_category(CATEGORY_UNDEF);
  set_duration(DURATION_UNDEF);
  set_origin(ORIGIN_UNDEF);
}

std::string Nsa::hash_feature() const {
/* 
  std::string str;
  unsigned char digest[MD5_DIGEST_LENGTH];
  char charDigest[2 * MD5_DIGEST_LENGTH + 1];

  str += "tag=";
  str += get_tag();
  str += ",category=";
  str += get_category();
  str += ",origin=";
  str += get_origin();
  str += ",duration=";
  str += get_duration();
  MD5((unsigned char*)str.c_str(), (size_t)str.length() * sizeof(char), digest);
  for (int i = 0; i < MD5_DIGEST_LENGTH; i++)
    sprintf(charDigest + 2 * i, "%02x", digest[i]);
  return std::string(charDigest);
 */
  std::string str;

  str += "tag=";
  str += get_tag();
  str += ",category=";
  str += get_category();
  str += ",origin=";
  str += get_origin();
  str += ",duration=";
  str += get_duration();
  MD5 md5;
  return std::string(md5.digestString(str.c_str(), (size_t)str.length() * sizeof(char)));
}

Nsa Nsa::tag_to_nsa(const std::string& atag) {
  Nsa nsa;
  if (Nsa::tagToNsaMap.find(atag) != Nsa::tagToNsaMap.end()) {
    nsa = Nsa::tagToNsaMap[atag];
  } else {
    NsaException e(__FILE__, __LINE__, "Unknown tag!\n");
    throw e;
  }
  return nsa;
}

void Nsa::init() {
  initialize_tag_to_nsa_map();
  initialize_nsa_to_tag_map();
}

void Nsa::initialize_tag_to_nsa_map() {
  Nsa::tagToNsaMap = std::map<std::string, Nsa>();
  tagToNsaMap =
      boost::assign::list_of<std::pair<std::string, Nsa> >(
          "silence",
          Nsa("silence", CATEGORY_SILENCE, ORIGIN_UNDEF, DURATION_UNDEF))(
          "pause_s",
          Nsa("pause_s", CATEGORY_SILENCE, ORIGIN_UNDEF, DURATION_SHORT))(
          "pause_l",
          Nsa("pause_l", CATEGORY_SILENCE, ORIGIN_UNDEF, DURATION_LONG))(
          "inspiration", Nsa("inspiration", CATEGORY_INSPIRATION, ORIGIN_HUMAN,
                             DURATION_UNDEF))(
          "glottal_voiced", Nsa("glottal_voiced", CATEGORY_PHONATION,
                                ORIGIN_HUMAN, DURATION_UNDEF))(
          "glottal_unvoiced", Nsa("glottal_unvoiced", CATEGORY_NOISE,
                                  ORIGIN_HUMAN, DURATION_UNDEF))(
          "cprom_paraverbal", Nsa("cprom_paraverbal", CATEGORY_CPROM_PARAVERBAL,
                                  ORIGIN_HUMAN, DURATION_UNDEF))(
          "vocal_noise",
          Nsa("noise", CATEGORY_NOISE, ORIGIN_HUMAN, DURATION_UNDEF))(
          "external_noise",
          Nsa("external_noise", CATEGORY_NOISE, ORIGIN_OTHER, DURATION_UNDEF))
          .to_container(tagToNsaMap);
}

void Nsa::initialize_nsa_to_tag_map() {
  Nsa::nsaToTagMap = std::map<std::string, std::string>();
  std::map<std::string, Nsa>::iterator iter;

  iter = tagToNsaMap.begin();
  while (iter != tagToNsaMap.end()) {
    Nsa& nsa = (*iter).second;
    nsaToTagMap[(nsa).hash_feature()] = (*iter).first;
    iter++;
  }
}

std::string Nsa::to_string(int level) const {
  std::stringstream ss;

  switch (level) {
    // level 0 : precise IPA code
    case LEVEL_LABEL:
      ss << "category = " << CATEGORIES[get_category()]
         << ", origin = " << ORIGINS[get_origin()]
         << ", duration = " << DURATIONS[get_duration()];
      break;
    case LEVEL_CLASS_FINE:
      ss << CATEGORIES[get_category()];
      break;
    case LEVEL_CLASS_COARSE:
      ss << CATEGORIES[get_category()];
      break;
    default:
      throw RootsException(__FILE__, __LINE__,
                           "unknown value for 'level' argument!\n");
  }
  return std::string(ss.str().c_str());
}

void Nsa::deflate(RootsStream* stream, bool is_terminal, int list_index) {
  BaseItem::deflate(stream, false, list_index);

  if (this->get_classname() == Nsa::classname()) {
    stream->append_unicodestring_content("tag", this->get_tag());
  }

  if (is_terminal) stream->close_object();
}

void Nsa::inflate(RootsStream* stream, bool is_terminal, int list_index) {
  std::string nsaClassname = stream->get_object_classname_no_read(
      roots::phonology::nsa::Nsa::xml_tag_name());

  BaseItem::inflate(stream, false, list_index);

  // stream->open_children();

  if (nsaClassname.compare(roots::phonology::nsa::Nsa::classname()) == 0) {
    this->set_tag(stream->get_unicodestring_content("tag"));
    this->from_tag();
  }
  if (is_terminal) stream->close_children();
}

Nsa* Nsa::inflate_object(RootsStream* stream, int list_index) {
  Nsa* t = new Nsa(true);
  t->inflate(stream, true, list_index);
  return t;
}
}
}
}  // END OF NAMESPACES
