/*
    This file is part of Roots.

    Roots is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Roots is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Roots.  If not, see <http://www.gnu.org/licenses/>.

    Copyright (c) 2013, 2014 IRISA
*/


#ifndef NSALPHABET_H_
#define NSALPHABET_H_

#include "../../common.h"
#include "../../Base.h"
#include "NsAlphabetException.h"

#include <boost/assign/list_of.hpp> // for 'map_list_of()'
#include <boost/assign/list_inserter.hpp>


namespace roots { namespace phonology { namespace nsa {

      typedef std::string t_nsAlphabet_elt;
      typedef std::map<std::string, t_nsAlphabet_elt > t_nsAlphabetMap;

      class NsAlphabet: public roots::Base
	{
	public:
	  NsAlphabet(const std::string& name);
	  virtual ~NsAlphabet();
	  NsAlphabet *clone() const;

	  const std::string& get_name() const {return name;};
	  void set_name(const std::string& n) {name = n;};

	  const std::string& get_alphabet_name() const {return name; };
	  const t_nsAlphabetMap& get_alphabet_map() const {return alphabetMap; };

	  bool is_label(const std::string& label);
	  const std::string& get_symbol(const std::string& label);
	  void add_symbol(const std::string& label, const std::string& symbol);
	  const std::string get_label_from_symbol(const std::string& symbol);

	  std::vector<std::string>* get_alphabet_set();

	  /**
	   * @brief returns the XML tag name value for current object
	   * @return string constant representing the XML tag name
	   */
	  virtual std::string get_xml_tag_name() const { return xml_tag_name(); };
	  
	  /**
	   * @brief returns the XML tag name value for current class
	   * @return string constant representing the XML tag name
	   */
	  static std::string xml_tag_name() { return "NoAlphabet"; };

		static std::vector<std::string> get_alphabet_names();

		static std::vector<std::string> get_alphabet_xml_tags();

		static roots::phonology::nsa::NsAlphabet * get_alphabet(const std::string & alphabetName);

		static roots::phonology::nsa::NsAlphabet * get_alphabet_from_xml_tag(const std::string & alphabetName);

		static void add_alphabet(roots::phonology::nsa::NsAlphabet * alphabet);


        friend std::ostream& operator<< (std::ostream &, NsAlphabet&);

	private:
		static std::vector<roots::phonology::nsa::NsAlphabet *> static_init();

	protected:
	  t_nsAlphabetMap alphabetMap;
	  std::string name;
	  static std::vector<roots::phonology::nsa::NsAlphabet *> alphabets;

	};

    } } }

#endif /* NSALPHABET_H_ */
