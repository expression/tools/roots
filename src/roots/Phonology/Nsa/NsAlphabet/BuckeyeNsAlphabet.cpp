/*
    This file is part of Roots.

    Roots is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Roots is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Roots.  If not, see <http://www.gnu.org/licenses/>.

    Copyright (c) 2013, 2014 IRISA
*/


#include "BuckeyeNsAlphabet.h"

namespace roots { namespace phonology { namespace nsa {


BuckeyeNsAlphabet& BuckeyeNsAlphabet::get_instance()
{
	static BuckeyeNsAlphabet instance;
	return instance;
}

BuckeyeNsAlphabet* BuckeyeNsAlphabet::get_instance_ptr()
{
	return &static_cast<BuckeyeNsAlphabet&>(get_instance());
}


BuckeyeNsAlphabet::BuckeyeNsAlphabet() : NsAlphabet("buckeye")
{
	alphabetMap = boost::assign::list_of <std::pair<std::string,t_nsAlphabet_elt> >
			("IVER", 	"silence")
			("SIL", 	"silence")
			("VOCNOISE", 	"vocal_noise")
			("NOISE", 	"external_noise")
			("LAUGH", 	"glottal_voiced").to_container(alphabetMap);
}

BuckeyeNsAlphabet::~BuckeyeNsAlphabet()
{
}

} } }
