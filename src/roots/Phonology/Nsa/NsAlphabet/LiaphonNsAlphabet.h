/*
    This file is part of Roots.

    Roots is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Roots is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Roots.  If not, see <http://www.gnu.org/licenses/>.

    Copyright (c) 2013, 2014 IRISA
*/


#ifndef LIAPHONNSALPHABET_H_
#define LIAPHONNSALPHABET_H_

#include "../../../common.h"
#include "../../../Base.h"
#include "../NsAlphabet.h"

namespace roots { namespace phonology { namespace nsa {

class LiaphonNsAlphabet: public NsAlphabet
{
public:
	static LiaphonNsAlphabet& get_instance();
  static LiaphonNsAlphabet* get_instance_ptr();
	static const std::string name() {return "liaphon";};

	/**
	 * @brief returns the XML tag name value for current class
	 * @return string constant representing the XML tag name
	 */
	static std::string xml_tag_name() { return "Liaphon"; };

protected:
	LiaphonNsAlphabet();
	virtual ~LiaphonNsAlphabet();

};

} } }

#endif /* LIAPHONNSALPHABET_H_ */
