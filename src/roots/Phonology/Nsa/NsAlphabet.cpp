/*
    This file is part of Roots.

    Roots is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Roots is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Roots.  If not, see <http://www.gnu.org/licenses/>.

    Copyright (c) 2013, 2014 IRISA
*/

#include "NsAlphabet.h"

#include "NsAlphabet/IrisaNsAlphabet.h"
#include "NsAlphabet/LiaphonNsAlphabet.h"
//#include "NsAlphabet/VoxygenNsAlphabet.h"
#include "NsAlphabet/ArpabetNsAlphabet.h"
#include "NsAlphabet/BuckeyeNsAlphabet.h"
#include "NsAlphabet/CmudictNsAlphabet.h"
#include "NsAlphabet/CpromNsAlphabet.h"
#include "NsAlphabet/SampaNsAlphabet.h"
#include "NsAlphabet/TranscriberNsAlphabet.h"

namespace roots {
namespace phonology {
namespace nsa {

std::vector<roots::phonology::nsa::NsAlphabet*> NsAlphabet::alphabets =
    NsAlphabet::static_init();

std::vector<roots::phonology::nsa::NsAlphabet*> NsAlphabet::static_init() {
  std::vector<roots::phonology::nsa::NsAlphabet*> alphabets;

  alphabets.push_back(
      roots::phonology::nsa::BuckeyeNsAlphabet::get_instance_ptr());
  alphabets.push_back(
      roots::phonology::nsa::CpromNsAlphabet::get_instance_ptr());
  // alphabets.push_back(roots::phonology::nsa::VoxygenNsAlphabet::get_instance_ptr());
  alphabets.push_back(
      roots::phonology::nsa::IrisaNsAlphabet::get_instance_ptr());
  alphabets.push_back(
      roots::phonology::nsa::LiaphonNsAlphabet::get_instance_ptr());
  alphabets.push_back(
      roots::phonology::nsa::SampaNsAlphabet::get_instance_ptr());
  alphabets.push_back(
      roots::phonology::nsa::CmudictNsAlphabet::get_instance_ptr());
  alphabets.push_back(
      roots::phonology::nsa::ArpabetNsAlphabet::get_instance_ptr());
  alphabets.push_back(
      roots::phonology::nsa::TranscriberNsAlphabet::get_instance_ptr());
  return alphabets;
}

NsAlphabet::NsAlphabet(const std::string& name) {
  alphabetMap = std::map<std::string, t_nsAlphabet_elt>();
  this->name = name;
  this->set_label(name);
}

NsAlphabet::~NsAlphabet() {}

NsAlphabet* NsAlphabet::clone() const { return new NsAlphabet(*this); }

bool NsAlphabet::is_label(const std::string& label) {
  t_nsAlphabetMap::iterator iter = alphabetMap.begin();
  bool found = false;

  while (iter != alphabetMap.end() && !found) {
    found = ((*iter).first).compare(label) == 0;
    iter++;
  }

  return found;
}

const std::string& NsAlphabet::get_symbol(const std::string& label) {
  if (alphabetMap.find(label) != alphabetMap.end()) {
    return alphabetMap[label];
  }

  NsAlphabetException e(__FILE__, __LINE__, "Unknown label!\n");
  throw(e);
}

void NsAlphabet::add_symbol(const std::string& label,
                            const std::string& symbol) {
  if (!this->is_label(label)) {
    alphabetMap[label] = symbol;
  } else {
    NsAlphabetException e(__FILE__, __LINE__, "Label already exists!\n");
    throw(e);
  }
}

const std::string NsAlphabet::get_label_from_symbol(const std::string& symbol) {
  bool found = false;
  t_nsAlphabetMap::const_iterator iter = get_alphabet_map().begin();
  std::string label;

  while (iter != get_alphabet_map().end() && !found) {
    if ((*iter).second.compare(symbol) == 0) {
      found = true;
      label = (*iter).first;
    }
    iter++;
  }

  if (!found) {
    throw NsAlphabetException(__FILE__, __LINE__,
                              "Unknown symbol in nsAlphabet!\n");
  }

  return label;
}

std::vector<std::string>* NsAlphabet::get_alphabet_set() {
  std::vector<std::string>* vec = new std::vector<std::string>();
  t_nsAlphabetMap::const_iterator iter = get_alphabet_map().begin();

  while (iter != get_alphabet_map().end()) {
    (*vec).push_back((*iter).first);
    iter++;
  }

  return vec;
}

std::vector<std::string> NsAlphabet::get_alphabet_names() {
  std::vector<std::string> names;

  for (std::vector<roots::phonology::nsa::NsAlphabet*>::const_iterator iter =
           alphabets.begin();
       iter != alphabets.end(); ++iter) {
    names.push_back((*iter)->get_name());
  }

  return names;
}

std::vector<std::string> NsAlphabet::get_alphabet_xml_tags() {
  std::vector<std::string> names;

  for (std::vector<roots::phonology::nsa::NsAlphabet*>::const_iterator iter =
           alphabets.begin();
       iter != alphabets.end(); ++iter) {
    names.push_back((*iter)->get_xml_tag_name());
  }

  return names;
}

roots::phonology::nsa::NsAlphabet* NsAlphabet::get_alphabet(
    const std::string& alphabetName) {
  roots::phonology::nsa::NsAlphabet* alph = NULL;

  for (std::vector<roots::phonology::nsa::NsAlphabet*>::const_iterator it =
           alphabets.begin();
       it != alphabets.end() && alph == NULL; ++it) {
    if ((*it)->get_name() == alphabetName) alph = *it;
  }

  /*if(alph==NULL)
  {
          std::clog << "warning: alphabet " << alphabetName << " is unknown!" <<
  std::endl;
  }*/

  return alph;
}

roots::phonology::nsa::NsAlphabet* NsAlphabet::get_alphabet_from_xml_tag(
    const std::string& alphabetName) {
  roots::phonology::nsa::NsAlphabet* alph = NULL;

  for (std::vector<roots::phonology::nsa::NsAlphabet*>::const_iterator it =
           alphabets.begin();
       it != alphabets.end() && alph == NULL; ++it) {
    if ((*it)->get_xml_tag_name() == alphabetName) alph = *it;
  }

  /*if(alph==NULL)
  {
          std::clog << "warning: alphabet " << alphabetName << " is unknown!" <<
  std::endl;
  }*/

  return alph;
}

void NsAlphabet::add_alphabet(roots::phonology::nsa::NsAlphabet* alphabet) {
  if (get_alphabet(alphabet->get_name()) == NULL) {
    alphabets.push_back(alphabet);
  }
}

std::ostream& operator<<(std::ostream& out, NsAlphabet& alphabet) {
  t_nsAlphabetMap::const_iterator iter =
      ((alphabet.get_alphabet_map())).begin();

  while (iter != alphabet.get_alphabet_map().end()) {
    out << (*iter).first << ": " << (*iter).second << std::endl;
    iter++;
  }
  return out;
}
}
}
}  // END OF NAMESPACES
