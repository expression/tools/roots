/*
    This file is part of Roots.

    Roots is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Roots is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Roots.  If not, see <http://www.gnu.org/licenses/>.

    Copyright (c) 2013, 2014 IRISA
*/


#ifndef NSACOMMON_H_
#define NSACOMMON_H_

#include "../../common.h"
#include "../../Base.h"
#include "../Nsa.h"
#include "NsAlphabet.h"

namespace roots { namespace phonology { namespace nsa {

class NsaCommon: public Nsa
{
public:
	NsaCommon(NsAlphabet &alph);
	NsaCommon(NsAlphabet &alph, const std::string& alabel);
	NsaCommon(const std::string& alabel);
	NsaCommon(const NsaCommon& nsa);
	virtual ~NsaCommon();
	virtual NsaCommon *clone() const;

	NsaCommon& operator=(const NsaCommon& nsa);

	//NsAlphabet& get_alphabet() const { return *alphabet;};
	NsAlphabet* get_alphabet() const { return alphabet;};
	void set_alphabet(NsAlphabet * al);

	//static void add_alphabet(NsAlphabet& alph);

	//static NsAlphabet& get_alphabet(const std::string& name);

	void from_label(const std::string& label);
	
	virtual std::string to_string(int level=LEVEL_LABEL) const;
		
	/**
	 * @brief Write the entity into a RootsStream
	 * @param stream the stream from which we inflate the element
	 * @param is_terminal indicates that the node is terminal (true by default)
	 */
	virtual void deflate(RootsStream * stream, bool is_terminal=true, int list_index=-1);
	/**
	 * @brief Extracts the entity from the stream
	 * @param stream the stream from which we inflate the element
	 */
	virtual void inflate(RootsStream * stream, bool is_terminal=true, int list_index=-1);
	/**
	 * @brief Extracts the entity from the stream
	 * @param stream the stream from which we inflate the element
	 */
	static NsaCommon * inflate_object(RootsStream * stream, int list_index=-1);
	/**
	 * @brief returns classname for current object
	 * @return string constant representing the classname
	 */
	virtual std::string get_classname() const { return classname(); };
	/**
	 * @brief returns classname for current class
	 * @return string constant representing the classname
	 */
	static std::string classname() { return "Phonology::Nsa::Common"; };
	
protected:
	NsaCommon(const bool noInit = false);
	
private:
	NsAlphabet* alphabet;
	//static std::vector<NsAlphabet*> registeredAlphabets;

};

} } } // END OF NAMESPACES

#endif /* NSACOMMON_H_ */
