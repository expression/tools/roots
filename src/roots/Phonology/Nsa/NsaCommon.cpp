/*
    This file is part of Roots.

    Roots is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Roots is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Roots.  If not, see <http://www.gnu.org/licenses/>.

    Copyright (c) 2013, 2014 IRISA
*/


#include "NsaCommon.h"
#include "NsAlphabet/LiaphonNsAlphabet.h"

namespace roots {
  namespace phonology {	
    namespace nsa {

      NsaCommon::NsaCommon(const bool noInit) : Nsa(noInit)
      {
	alphabet = NULL;
      }


      NsaCommon::NsaCommon(NsAlphabet &alph)
      {
	set_alphabet(&alph);
	set_label("");
      }

      NsaCommon::NsaCommon(const std::string& alabel)
      {
	set_alphabet(phonology::nsa::LiaphonNsAlphabet::get_instance_ptr());
	from_label(alabel);
      }

      NsaCommon::NsaCommon(NsAlphabet &alph, const std::string& alabel)
      {
	set_alphabet(&alph);
	from_label(alabel);
      }

      NsaCommon::NsaCommon(const NsaCommon& nsa): Nsa(nsa)
      {
	set_alphabet(nsa.get_alphabet());
      }


      NsaCommon::~NsaCommon()
      {
      }

      NsaCommon *NsaCommon::clone() const
      {
	return new NsaCommon(*this);
      }

      NsaCommon& NsaCommon::operator=(const NsaCommon& nsa)
      {
	((Nsa) *this).operator=(nsa);
	set_alphabet(nsa.get_alphabet());
	return *this;
      }

      void NsaCommon::set_alphabet(NsAlphabet* al)
      {
	alphabet = al;
      }


      void NsaCommon::from_label(const std::string& label)
	  {
		  set_label(label);
		  NsAlphabet *alphabet = get_alphabet();
		  /*if(alphabet == NULL)
		  {
			  std::clog << "warning: " << __FILE__ << ":" << __LINE__ << ": alphabet is NULL" << std::endl;
		  }else{*/
		  if(alphabet != NULL)
		  {
			  if(alphabet->is_label(label))
			  {
				  set_tag(alphabet->get_symbol(label));
				  from_tag();
			  }
			  else
			  {
				  std::string msg=  ": Unknown label : " + label + " !\n";
				  NsaException e(__FILE__, __LINE__, msg);
				  throw(e);
			  }		
		  }
      }


      std::string NsaCommon::to_string(int level) const
      {
    std::stringstream ss;

	switch (level)
	  {
	    // level 0 : precise label (ipa in alphabet)
	  case LEVEL_LABEL:
	    //ss	<< "\"" << get_label() << "\"";
	    ss	<<  get_label();
	    break;
	  case LEVEL_CLASS_FINE:
	  case LEVEL_CLASS_COARSE:
	    ss	<< Nsa::to_string(level);
	    break;
	  default:
        std::stringstream msg;
	    msg << "unknown value "<< level << " for 'level' argument!\n";
	    throw RootsException(__FILE__, __LINE__, msg.str());

	  }
	return std::string(ss.str().c_str());
      }

      void NsaCommon::deflate(RootsStream * stream, bool is_terminal, int list_index)
      {
	Nsa::deflate(stream,false,list_index);

	std::string alphabetName = "";
	if(get_alphabet() != NULL)
		alphabetName = get_alphabet()->get_alphabet_name();
		
	stream->append_unicodestring_content("alphabet", alphabetName);
	stream->append_unicodestring_content("label", this->get_label());

	if(is_terminal)
	  stream->close_object();
      }

      void NsaCommon::inflate(RootsStream * stream, bool is_terminal, int list_index)
      {
	Nsa::inflate(stream,false,list_index);

	//stream->open_children();
	std::string tmpName = stream->get_unicodestring_content("alphabet");
	NsAlphabet* nsAlphabet = NsAlphabet::get_alphabet(tmpName);
	/*if(nsAlphabet == NULL)
	{
		throw RootsException(__FILE__, __LINE__, "Unknown NS alphabet! <"+tmpName+">");
	}*/
	set_alphabet(nsAlphabet);
	from_label(stream->get_unicodestring_content("label"));
	if(is_terminal) stream->close_children();
      }

      NsaCommon * NsaCommon::inflate_object(RootsStream * stream, int list_index)
      {
	NsaCommon *t = new NsaCommon(true);
	t->inflate(stream,true,list_index);
	return t;
      }

    }
  }
} // END OF NAMESPACES
