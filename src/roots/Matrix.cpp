/*
    This file is part of Roots.

    Roots is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Roots is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Roots.  If not, see <http://www.gnu.org/licenses/>.

    Copyright (c) 2013, 2014 IRISA
*/


#include "Matrix.h"
#include "Sequence.h"

namespace roots
{

Matrix::Matrix()
{
}

Matrix::~Matrix()
{
}

std::vector<int>* Matrix::sum_rows()
{
	std::vector<int> *vec = new std::vector<int>();
	int sum = 0;

	for(int i=0; i<this->get_n_row(); i++)
	{
		sum = 0;
		for(int j=0; j<this->get_n_col(); j++)
		{
			sum += this->get_element(i,j);
		}
		vec->push_back(sum);
	}

	return vec;
}

std::vector<int>* Matrix::sum_columns()
{
	std::vector<int> *vec = new std::vector<int>();
	int sum = 0;

	for(int i=0; i<this->get_n_col(); i++)
	{
		sum = 0;
		for(int j=0; j<this->get_n_row(); j++)
		{
			sum += this->get_element(i,j);
		}
		vec->push_back(sum);
	}

	return vec;
}

  std::string Matrix::to_string(int level) const
  {
     std::stringstream ss;
    switch(level) {
      case 0:
	ss << (*this) <<  std::endl;
      default:
	ss << (*this)<< std::endl;
	for (int r = 0; r < get_n_row(); r++) {
	  if (r > 0) {
	    ss << "\n";
	  }
	  for (int c = 0; c < get_n_col(); c++) {
	    if (c > 0) {
	      ss << "\t";
	    }
	    ss << get_element(r, c);
	  }
	}
	break;
    }
    return std::string(ss.str().c_str());
  }

   std::ostream& operator<<( std::ostream& out, const Matrix& mat )//__attribute__((unused)))
{
  return out << "Matrix : ["<< mat.get_n_row() <<";"<< mat.get_n_col() <<"]";

  //	return out << "Matrix";
}

void Matrix::deflate(RootsStream * stream, bool is_terminal, int list_index)
{
     std::stringstream convert;
	convert << this->get_xml_tag_name();

	stream->append_object(convert.str().c_str(), list_index); // Creates a new node and set it to current node
	stream->set_object_classname(this->get_classname());

	if(is_terminal)
	    stream->close_object();
}

void Matrix::inflate(RootsStream * stream, bool is_terminal, int list_index)
{
     std::stringstream convert;
	convert << this->get_xml_tag_name();

	stream->open_object(convert.str().c_str(), list_index);   
	stream->open_children();
	
	std::string classname = stream->get_object_classname();
	//cout << classname <<  std::endl;
	
	if(is_terminal) stream->close_children();
}


}
