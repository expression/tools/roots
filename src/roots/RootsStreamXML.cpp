/*
    This file is part of Roots.

    Roots is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Roots is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Roots.  If not, see <http://www.gnu.org/licenses/>.

    Copyright (c) 2013, 2014 IRISA
*/


#include "RootsStreamXML.h"

#include <boost/filesystem.hpp>

namespace bfs = boost::filesystem;

namespace roots {

  void RootsStreamXML::load (const std::string & _filename)
  {
    std::stringstream ss;
    char thefilename[1024];
    std::string baseDirName;
    
    ss << _filename;
    
    strcpy (thefilename, ss.str ().c_str ());
    
    baseDirName = bfs::path(_filename).parent_path().string();
    
    // Setting the instance attribute containing the path to the files' directory
    //cout << "===================== " << baseDirName << endl;
    set_base_dir_name (baseDirName);
    
    if (std::ifstream (thefilename).fail ())
    {
      std::cerr << "file not found!" << std::endl;
      fflush (stderr);
      std::stringstream ss2;
      ss2 << "file \"" << thefilename << "\" not found! \n";
      throw RootsException (__FILE__, __LINE__, ss2.str ().c_str ());
    }
    
    xercesc::XercesDOMParser * parser = new xercesc::XercesDOMParser;
    parser->setIncludeIgnorableWhitespace (false);
    
    if (doc != NULL)
    {
      doc->release ();
      doc = NULL;
    }
    
    try
    {
      parser->parse (thefilename);
      doc = parser->getDocument ();	// Check *doc
      parser->adoptDocument ();
    }
    catch (xercesc::TranscodingException e)
    {
      //cerr << *e.getMessage();
      throw RootsException (__FILE__, __LINE__,
			    "Impossible to parse file! check its format.\n");
    }
    rootNode = doc->getDocumentElement ();
    currentNode = rootNode;
    set_current_node(currentNode);
    open_children();
    
    #if ROOTS_STREAM_DEBUG == 1
    cout << "load:node_name = " << get_node_name() << endl;
    #endif
    
    parentNodes = std::stack < xercesc::DOMNode * >();
    parentNodes.push(rootNode);
    
    delete parser;
  }
  
} // End namespace roots
