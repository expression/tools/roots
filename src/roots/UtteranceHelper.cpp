/*
  This file is part of Roots.

  Roots is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Roots is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with Roots.  If not, see <http://www.gnu.org/licenses/>.

  Copyright (c) 2013, 2014 IRISA
*/

#include "UtteranceHelper.h"

namespace roots {

template <>
void get_all_nss<void>(
    const std::string &label, bool &is_nss, unsigned int &n_nss,
    sequence::NssSequence *nssSequence,
    sequence::SegmentSequence *segmentSequence,
    std::vector<std::pair<unsigned int, unsigned int> > &segNssRelationElts) {
  try {
    roots::acoustic::NonSpeechSound nss(
        roots::phonology::nsa::Nsa::from_tag(label));
    nssSequence->add(&nss);
    is_nss = true;
    segNssRelationElts.push_back(std::pair<int, int>(
        segmentSequence->count() - 1, nssSequence->count() - 1));
    n_nss++;
  } catch (roots::phonology::nsa::NsaException e) {
    // Do nothing
  }
};

template <>
void get_all_allophone<void>(
    std::string &label, bool &is_allophone, unsigned int &n_allophone,
    sequence::AllophoneSequence *allophoneSequence,
    sequence::SegmentSequence *segmentSequence,
    std::vector<std::pair<unsigned int, unsigned int> > &segAllRelationElts) {
  try {
    std::vector<roots::phonology::ipa::Ipa *> vec_ipas =
        roots::phonology::ipa::Ipa().extract_ipas(label);

    for (std::vector<roots::phonology::ipa::Ipa *>::iterator it_ipas =
             vec_ipas.begin();
         it_ipas != vec_ipas.end(); ++it_ipas) {
      roots::acoustic::Allophone alo(**it_ipas);
      allophoneSequence->add(&alo);
      is_allophone = true;
      segAllRelationElts.push_back(std::pair<int, int>(
          segmentSequence->count() - 1, allophoneSequence->count() - 1));
      n_allophone++;
    }
  } catch (roots::phonology::ipa::IpaException e) {
    // std::cerr<< "exception occured 1" << endl;
  }
};
}
