/*
    This file is part of Roots.

    Roots is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Roots is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Roots.  If not, see <http://www.gnu.org/licenses/>.

    Copyright (c) 2013, 2014 IRISA
*/


#include "MemoryChunk.h"

namespace roots
{

  MemoryChunk::MemoryChunk() : Base()
  {
    uttLayers = std::map<std::string, std::vector<Utterance> > ();
  }


  MemoryChunk::MemoryChunk(const std::vector<std::string> & layerNames) : Base()
  {
    uttLayers = std::map<std::string, std::vector<Utterance> > ();
    for(std::vector<std::string>::const_iterator it = layerNames.begin();
	it != layerNames.end();
	++it)
      {	uttLayers[*it] = std::vector<Utterance>(); }
  }


  MemoryChunk::~MemoryChunk()
  {
  }


  MemoryChunk::MemoryChunk(const MemoryChunk& mchunk) : Base(*this)
  {
    uttLayers = mchunk.uttLayers;
  }


  MemoryChunk& MemoryChunk::operator= (const MemoryChunk& mchunk)
  {
    if(this != &mchunk)
      {
	Base::operator=(mchunk);
	uttLayers = mchunk.uttLayers;
      }

    return *this;
  }


  MemoryChunk *MemoryChunk::clone() const
  {
    return new MemoryChunk(*this);
  }

  BaseChunk *MemoryChunk::clone_to_basechunk() const
  {
    return new MemoryChunk(*this);
  }


  void MemoryChunk::unload_all()
  {
    // Nothing to do
  }


  void MemoryChunk::load_all()
  {
    // Nothing to do
  }

  bool MemoryChunk::is_loaded_for_utterance(const int)
  {
    return true;
  }

  int MemoryChunk::count_utterances() const
  {
    if (!uttLayers.empty()) { return uttLayers.begin()->second.size(); }
    else { return 0; }
  }

  void MemoryChunk::update_counts()
  {
    // Nothing to do in a in-memory ckunk
  }

  int MemoryChunk::count_layers() const
  {
    return uttLayers.size();
  }

  std::vector<std::string> MemoryChunk::get_all_layer_names() const
  {
    std::vector<std::string> layer_names;
    for (std::map<std::string, std::vector<Utterance> >::const_iterator it =
	   uttLayers.begin();
	 it != uttLayers.end();
	 ++it)
      { layer_names.push_back(it->first); }
    return layer_names;
  }


  void MemoryChunk::add_utterance(const Utterance& utt, const std::string layerName)
  //  throw(RootsException)
  {
    size_t nb_layers = uttLayers.size();
    // Not the first utterance nor the correct number of layers
    if (nb_layers > 1 || (nb_layers == 1 && uttLayers.begin()->first != layerName))
      {
	std::stringstream ss;
	ss << "MemoryChunk::add_utterance: Uncompatible layer <" << layerName << ">!";
	throw RootsException(__FILE__, __LINE__, ss.str().c_str());
      }

    if(nb_layers == 0)
      {uttLayers[layerName] = std::vector<Utterance>();}

    uttLayers[layerName].push_back(utt);
  }


  void MemoryChunk::add_utterance(const std::map<std::string, Utterance> &utt)
  //  throw(RootsException)
  {
    size_t nb_layers = uttLayers.size();
    // Not the first utterance nor the correct number of layers
    if (nb_layers > 0 && utt.size() != nb_layers) {
      throw RootsException(__FILE__,__LINE__, "MemoryChunk::add_utterance: Wrong number of layers");
    }

    for (std::map<std::string, Utterance>::const_iterator it = utt.begin();
	 it != utt.end();
	 ++it)
      {
	if(nb_layers == 0)
	  {uttLayers[it->first] = std::vector<Utterance>();}
	else if (uttLayers.find(it->first) == uttLayers.end())
	  {
	    std::stringstream ss;
	    ss << "MemoryChunk::add_utterance: Unknown layer <" << it->first << ">!";
	    throw RootsException(__FILE__, __LINE__, ss.str().c_str());
	  }
      }

    // Add into each layer
    for (std::map<std::string, Utterance>::const_iterator it = utt.begin();
	 it != utt.end();
	 ++it) {
      uttLayers[it->first].push_back(it->second);
    }

  }

  void MemoryChunk::add_utterances(const std::vector<Utterance>& utt, const std::string layerName)
  //  throw(RootsException)
  {
    size_t nb_layers = uttLayers.size();
    // Not the first utterance nor the correct number of layers
    if (nb_layers > 1 || (nb_layers == 1 && uttLayers.begin()->first != layerName))
      {
	std::stringstream ss;
	ss << "MemoryChunk::add_utterance: Uncompatible layer <" << layerName << ">!";
	throw RootsException(__FILE__, __LINE__, ss.str().c_str());
      }

    if(nb_layers == 0)
      {uttLayers[layerName] = std::vector<Utterance>();}

    uttLayers[layerName].insert(uttLayers[layerName].end(), utt.begin(), utt.end());
  }


  void MemoryChunk::add_utterances(const std::map<std::string, std::vector<Utterance> >& utt)
  //  throw(RootsException)
  {
    // Check size of each layer
    int n_utt = -1;
    for (std::map<std::string, std::vector<Utterance> >::const_iterator it = utt.begin();
      it != utt.end();
      ++it) {
      if (n_utt == -1) {
	n_utt = it->second.size();
      }
      else {
	if (n_utt != (int )it->second.size()) {
	  std::stringstream ss;
	  ss << "MemoryChunk::add_utterances: Layers are not the same size. <" << it->first << "> is " << it->second.size() << " whereas preceding layer is " << n_utt << ".";
	  throw RootsException(__FILE__, __LINE__, ss.str().c_str());
	}
      }
    }

    // Check if vectors should be created before adding (first utterances in the current chunk)
    bool create_vector = (uttLayers.empty());

    // Now (safely) add new utterances layer by layer
    for (std::map<std::string, std::vector<Utterance> >::const_iterator it = utt.begin();
	 it != utt.end();
         ++it) {
      if (create_vector) {
	uttLayers[it->first] = std::vector<Utterance>();
      }
      uttLayers[it->first].insert(uttLayers[it->first].end(), it->second.begin(), it->second.end());
    }
  }


  void MemoryChunk::delete_utterance(int id)
  {
    load_all();
    for (std::map<std::string, std::vector<Utterance> >::iterator it = uttLayers.begin();
	 it != uttLayers.end();
	 ++it)
      { it->second.erase(it->second.begin() + id); }
    if (count_utterances() == 0) {
      clear();
    }
  }


  void MemoryChunk::clear()
  {
    for(std::map<std::string, std::vector<Utterance> >::iterator it = uttLayers.begin();
	it != uttLayers.end();
    ++it)
	{ it->second.clear(); }
    uttLayers.clear();
  }


  roots::Utterance* MemoryChunk::get_utterance(int id)
  {
    roots::Utterance * utterance = NULL;
    for(std::map<std::string, std::vector<Utterance> >::const_iterator it = uttLayers.begin();
	it != uttLayers.end();
	++it)
      {	load_in_utterance(it->first, id, utterance); }

    return utterance;
  }


  roots::Utterance* MemoryChunk::get_utterance(const std::vector<std::string> & layerToLoad, int id)
  {
    roots::Utterance * utterance = NULL;

    for(std::vector<std::string >::const_iterator it = layerToLoad.begin();
	it != layerToLoad.end();
	++it)
      { load_in_utterance(*it, id, utterance); }
    return utterance;
  }


  void MemoryChunk::update_utterance(int id, const Utterance& utt)
  {
    size_t nb_layers = uttLayers.size();
    // Check if considered layer is implicit
    if (nb_layers > 1)
    {
      std::stringstream ss;
      ss << "MemoryChunk::update_utterance: current chunk is made of multiple layers! Please use a map to specify considered layer.";
      throw RootsException(__FILE__, __LINE__, ss.str().c_str());
    }
    std::string layerName = (*uttLayers.begin()).first;
    uttLayers[layerName][id] = utt;
  }


  void MemoryChunk::update_utterance(int id, const std::map<std::string, Utterance> &utt)
  {
    std::map<std::string, Utterance>::const_iterator it;

    for (it = utt.begin(); it != utt.end(); ++it)
    {
      if (uttLayers.find(it->first) == uttLayers.end())
      {
	std::stringstream ss;
	ss << "MemoryChunk::update_utterance: unknown layer <" << it->first << ">!";
	throw RootsException(__FILE__, __LINE__, ss.str().c_str());
      }
    }

    for (it = utt.begin(); it != utt.end(); ++it) {
      uttLayers[it->first][id] = it->second;
    }
  }


  const std::map<std::string, std::vector<Utterance> > & MemoryChunk::get_utterances() const
  {
    return uttLayers;
  }

  void MemoryChunk::prepare_save(const std::string & target_base_dir)
  {
   for (std::map<std::string, std::vector<Utterance> >::iterator it = uttLayers.begin();
	 it != uttLayers.end();
	 ++it)
      {
	// Deflate each utterance in the layer
	for (size_t i_utt = 0; i_utt < it->second.size(); ++i_utt)
	  { it->second[i_utt].prepare_save(target_base_dir); }
      }
  }

  void MemoryChunk::load_in_utterance(const std::string & layerToAdd,
				      int index,
				      roots::Utterance *& utterance)
  {
    std::map<std::string, std::vector<Utterance> >::const_iterator it = uttLayers.find(layerToAdd);
    if(it != uttLayers.end())
      {
	if(utterance == NULL)
	  { utterance = new roots::Utterance(it->second.at(index)); }
	else
	  { utterance->merge_utterance(&(it->second.at(index))); }
      }
  }


  void MemoryChunk::deflate(RootsStream * stream, bool is_terminal, int list_index)
  {
    Base::deflate(stream,false,list_index);

    // Get layer names (and their number)
    std::vector<std::string> layerNames = get_all_layer_names();
    size_t index = 0;

    stream->append_unicodestring_content("chunk_name", this->get_label());
    stream->append_int_content("n_utterances", count_utterances());
    stream->append_vector_unicodestring_content("layer_names", layerNames);
    // Deflate each layer
    for (std::map<std::string, std::vector<Utterance> >::iterator it = uttLayers.begin();
	 it != uttLayers.end();
	 ++it)
      {
	// Deflate each utterance in the layer
	for (size_t i_utt = 0; i_utt < it->second.size(); i_utt++) {
	  it->second[i_utt].deflate(stream, true, index);
	  index++;
	}
      }
    if(is_terminal)
      stream->close_object();
  }


  void MemoryChunk::inflate(RootsStream * stream, bool is_terminal, int list_index)
  {
    clear();

    Base::inflate(stream,false,list_index);
    size_t n_utterances = 0;
    size_t n_layers = 0;
    std::string layer_name = BaseChunk::ANONYMOUS_LAYER;
    std::vector<std::string> layerNames;
    size_t index = 0;

    //stream->open_children();
    set_label(stream->get_unicodestring_content("chunk_name"));
    n_utterances = stream->get_int_content("n_utterances");
    layerNames = stream->get_vector_unicodestring_content("layer_names");
    n_layers = layerNames.size();
    for (size_t i_layer = 0; i_layer < n_layers; i_layer++) {
      layer_name = layerNames[i_layer];
//       stream->open_object("layer");
//       stream->open_children();
//       layer_name = stream->get_unicodestring_content("layer_name");
      uttLayers[layer_name] = std::vector<Utterance>();
      for (size_t i_utt = 0; i_utt < n_utterances; i_utt++) {
	Utterance utt;
	uttLayers[layer_name].push_back(utt);
	uttLayers[layer_name].back().inflate(stream, true, index);
	stream->next_child();
	index++;
      }
//       stream->close_children();
//       stream->close_object();
    }

    if(is_terminal) stream->close_children();
  }

} // END OF NAMESPACE
