/*
    This file is part of Roots.

    Roots is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Roots is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Roots.  If not, see <http://www.gnu.org/licenses/>.

    Copyright (c) 2013, 2014 IRISA
*/


#ifndef ROOTS_H_
#define ROOTS_H_

#include "common.h"
#include "Base.h"
#include "BaseLink.h"
#include "Sequence.h"
#include "SequenceLinkedObject.h"
#include "BaseLink.h"
#include "BaseItem.h"
#include "Void.h"
#include "Sequence.h"
#include "SequenceLinkedObject.h"
#include "Relation.h"
#include "RelationLinkedObject.h"
#include "RootsException.h"
#include "Matrix.h"
#include "Matrix/SparseMatrix.h"
#include "Tree/TreeNode.h"
#include "Tree.h"
#include "Align.h"
#include "Align/Alignment.h"
#include "Align/Levenshtein.h"

#include "Number.h"
#include "Embedding.h"
#include "Symbol.h"

#include "Acoustic/Allophone.h"
#include "Acoustic/NonSpeechSound.h"
#include "Acoustic/Segment.h"
#include "Acoustic/Segment/SignalSegment.h"
#include "Acoustic/Segment/TimeSegment.h"
#include "Acoustic/Segment/SpectralSegment.h"
#include "Acoustic/Segment/F0Segment.h"
#include "Acoustic/Segment/PitchmarkSegment.h"
#include "Acoustic/Atom.h"

#include "Phonology/Ipa/Alphabet.h"
#include "Phonology/Ipa.h"
#include "Phonology/Ipa/MappedAlphabet.h"
#include "Phonology/Ipa/Alphabet/IrisaAlphabet.h"
#include "Phonology/Ipa/Alphabet/LiaphonAlphabet.h"
#include "Phonology/Ipa/Alphabet/SampaAlphabet.h"
#include "Phonology/Ipa/Alphabet/BdlexSampaAlphabet.h"
#include "Phonology/Ipa/Alphabet/ArpabetAlphabet.h"
#include "Phonology/Ipa/Alphabet/CmudictAlphabet.h"
#include "Phonology/Ipa/Alphabet/PinyinAlphabet.h"
#include "Phonology/Ipa/Alphabet/LoadableAlphabet.h"
#include "Phonology/Ipa/AlphabetConverter.h"

#include "Phonology/Nsa.h"
#include "Phonology/Nsa/NsaCommon.h"
#include "Phonology/Nsa/NsAlphabet.h"
#include "Phonology/Nsa/NsAlphabet/IrisaNsAlphabet.h"
#include "Phonology/Nsa/NsAlphabet/SampaNsAlphabet.h"
#include "Phonology/Nsa/NsAlphabet/CmudictNsAlphabet.h"
#include "Phonology/Nsa/NsAlphabet/ArpabetNsAlphabet.h"
#include "Phonology/Nsa/NsAlphabet/LiaphonNsAlphabet.h"
#include "Phonology/Nsa/NsAlphabet/CpromNsAlphabet.h"
#include "Phonology/Nsa/NsAlphabet/TranscriberNsAlphabet.h"
#include "Phonology/Nsa/NsAlphabet/BuckeyeNsAlphabet.h"
#include "Phonology/Nsa/NsAlphabetConverter.h"

#include "Phonology/Phoneme.h"
#include "Phonology/PhonemeBlock.h"
#include "Phonology/Syllable.h"
#include "Phonology/ProsodicPhrase.h"
#include "Phonology/ProsodicPhrase/ProsodicPhraseNode.h"


#include "Linguistic/Filler.h"
#include "Linguistic/Filler/Nsa.h"
#include "Linguistic/Filler/Word.h"
#include "Linguistic/NamedEntity.h"
#include "Linguistic/NamedEntity/Location.h"
#include "Linguistic/NamedEntity/Location/Address.h"
#include "Linguistic/NamedEntity/Number.h"
#include "Linguistic/NamedEntity/Organization.h"
#include "Linguistic/NamedEntity/Organization/EthnicGroup.h"
#include "Linguistic/NamedEntity/Organization/Other.h"
#include "Linguistic/NamedEntity/Period.h"
#include "Linguistic/NamedEntity/Period/PeriodTime.h"
#include "Linguistic/NamedEntity/Person.h"
#include "Linguistic/NamedEntity/Time.h"
#include "Linguistic/POS.h"
#include "Linguistic/POS/Adjective.h"
#include "Linguistic/POS/Adverb.h"
#include "Linguistic/POS/Conjunction.h"
#include "Linguistic/POS/Determiner.h"
#include "Linguistic/POS/Interjection.h"
#include "Linguistic/POS/Noun.h"
#include "Linguistic/POS/Preposition.h"
#include "Linguistic/POS/Pronoun.h"
#include "Linguistic/POS/Punctuation.h"
#include "Linguistic/POS/Verb.h"
#include "Linguistic/POS/ForeignWord.h"
#include "Linguistic/POS/Prefix.h"
#include "Linguistic/POS/Particle.h"
#include "Linguistic/POS/Symbol.h"
#include "Linguistic/POS/Determiner/Article.h"
#include "Linguistic/POS/Determiner/Demonstrative.h"
#include "Linguistic/POS/Determiner/Indefinite.h"
#include "Linguistic/POS/Determiner/Interrogative.h"
#include "Linguistic/POS/Determiner/Numeral.h"
#include "Linguistic/POS/Determiner/Possessive.h"
#include "Linguistic/Syntax.h"
#include "Linguistic/Syntax/SyntaxNode.h"
#include "Linguistic/Word.h"
#include "Linguistic/Grapheme.h"

#include "File/Audio.h"
#include "File/F0.h"
#include "File/Pitchmark.h"
#include "File/HTK.h"
#include "File/HTKLab.h"
#include "File/PraatPitch.h"
#include "File/TextGrid.h"

#include "Sequence/VoidSequence.h"
#include "Sequence/SymbolSequence.h"
#include "Sequence/EmbeddingSequence.h"
#include "Sequence/NumberSequence.h"

#include "Sequence/AllophoneSequence.h"
#include "Sequence/SegmentSequence.h"
#include "Sequence/NssSequence.h"

#include "Sequence/WordSequence.h"
#include "Sequence/GraphemeSequence.h"
#include "Sequence/NamedEntitySequence.h"
#include "Sequence/SyllableSequence.h"
#include "Sequence/FillerSequence.h"
#include "Sequence/PhonemeSequence.h"
#include "Sequence/SyntaxSequence.h"
#include "Sequence/PosSequence.h"
#include "Sequence/SegmentSequence.h"
#include "Sequence/ProsodicPhraseSequence.h"
#include "Sequence/PhonemeBlockSequence.h"
#include "Sequence/AtomSequence.h"

#include "Relation.h"
#include "RelationGraph.h"

#include "Utterance.h"
#include "UtteranceLinkedObject.h"
#include "UtteranceHelper.h"

#include "BaseChunk.h"
#include "Corpus.h"
#include "FileChunk.h"
#include "MemoryChunk.h"

#include "RootsStream.h"
#include "RootsStreamJson.h"

#ifdef USE_STREAM_XML
#include "RootsStreamXML.h"
#endif


#include "RootsDisplay.h"


inline void ROOTS_INIT()
{

#ifdef USE_STREAM_XML
	try {
		xercesc::XMLPlatformUtils::Initialize();
	}
    catch (const xercesc::XMLException& toCatch) {
		ROOTS_LOGGER_WARN("cannot initialize xercesc!");
		throw;
	}
#endif

    roots::phonology::ipa::Ipa::init();
	roots::phonology::nsa::Nsa::init();
}

inline void ROOTS_TERMINATE()
{
#ifdef USE_STREAM_XML
    xercesc::XMLPlatformUtils::Terminate();
#endif
}

inline void ROOTS_SET_LOGGER_LEVEL(ROOTS_LOGGER_LEVEL_TYPE loglevel)
{   
    ROOTS_LOGGER_LEVEL = loglevel;
}

#endif /* ROOTS_H_ */
