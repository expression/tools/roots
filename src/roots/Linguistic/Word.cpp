/*
    This file is part of Roots.

    Roots is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Roots is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Roots.  If not, see <http://www.gnu.org/licenses/>.

    Copyright (c) 2013, 2014 IRISA
*/


#include "Word.h"

roots::linguistic::Word::Word(const bool noInit) : BaseItem(noInit)
{
}

roots::linguistic::Word::Word(const std::string &aLabel) : BaseItem(aLabel)
{
}

roots::linguistic::Word::Word(const Word &aWord) : BaseItem(aWord)
{
}

roots::linguistic::Word::~Word()
{
}

roots::linguistic::Word *roots::linguistic::Word::clone() const
{
	return new roots::linguistic::Word(*this);
}

roots::linguistic::Word &roots::linguistic::Word::operator=(const roots::linguistic::Word &word)
{
	BaseItem::operator=(word);
	return *this;
}

std::string roots::linguistic::Word::to_string(int level) const
{
	std::stringstream ss;

	switch (level)
	{
		// level 0 : precise POS tag
		case 0:
		default:
			ss 	<<  get_label();
			break;
	}
	return std::string(ss.str().c_str());
}

void roots::linguistic::Word::deflate(RootsStream * stream, bool is_terminal, int list_index)
{
  BaseItem::deflate(stream,false,list_index);

  stream->append_unicodestring_content("text", this->get_label());

  if(is_terminal)
	stream->close_object();
}

void roots::linguistic::Word::inflate(RootsStream * stream, bool is_terminal, int list_index)
{
	BaseItem::inflate(stream,false,list_index);

	this->set_label(stream->get_unicodestring_content("text"));

	if(is_terminal) stream->close_children();
}

roots::linguistic::Word * roots::linguistic::Word::inflate_object(RootsStream * stream, int list_index)
{
	Word *t = new Word(true);
	t->inflate(stream,true,list_index);
	return t;	
}
