/*
    This file is part of Roots.

    Roots is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Roots is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Roots.  If not, see <http://www.gnu.org/licenses/>.

    Copyright (c) 2013, 2014 IRISA
*/


#ifndef NAMEDENTITY_H_
#define NAMEDENTITY_H_


#include "../common.h"
#include "../BaseItem.h"

#include <boost/assign.hpp>

namespace roots
{
  namespace linguistic
  {
    namespace namedentity
    {
      /**
       * @brief The possible categories for named entities
       */
      enum Category
      {
	CATEGORY_UNKNOWN								= 0, //!< CATEGORY_UNKNOWN
	CATEGORY_TIME									= 1, //!< CATEGORY_TIME
	CATEGORY_TIME_RELATIVE							= 2, //!< CATEGORY_TIME_RELATIVE
	CATEGORY_PERSON									= 3, //!< CATEGORY_PERSON
	CATEGORY_PERIOD									= 4, //!< CATEGORY_PERIOD
	CATEGORY_ORGANIZATION							= 5, //!< CATEGORY_ORGANIZATION
	CATEGORY_NUMBER									= 6, //!< CATEGORY_NUMBER
	CATEGORY_NUMBER_MONEY							= 7, //!< CATEGORY_NUMBER_MONEY
	CATEGORY_LOCATION								= 8, //!< CATEGORY_LOCATION
	CATEGORY_PERIOD_TIME							= 9, //!< CATEGORY_PERIOD_TIME
	CATEGORY_PERIOD_OTHER							= 10,//!< CATEGORY_PERIOD_OTHER
	CATEGORY_ORGANIZATION_OTHER						= 11,//!< CATEGORY_ORGANIZATION_OTHER
	CATEGORY_ORGANIZATION_ETHNIC_GROUP				= 12,//!< CATEGORY_ORGANIZATION_ETHNIC_GROUP
	CATEGORY_ORGANIZATION_ETHNIC_GROUP_OTHER		= 13,//!< CATEGORY_ORGANIZATION_ETHNIC_GROUP_OTHER
	CATEGORY_ORGANIZATION_ETHNIC_GROUP_NATIONALITY	= 14,//!< CATEGORY_ORGANIZATION_ETHNIC_GROUP_NATIONALITY
	CATEGORY_LOCATION_ADDRESS						= 15,//!< CATEGORY_LOCATION_ADDRESS
	CATEGORY_LOCATION_ADDRESS_OTHER					= 16 //!< CATEGORY_LOCATION_ADDRESS_OTHER
      };


      /**
       *  @class roots::linguistic::NamedEntity
       *  @brief This class represents named entities in ROOTS
       *  @details
       * description : http://nlp.cs.nyu.edu/ene/version7_1_0Beng.html <br/>
       * 
       * - ORGANISATION
       * - PERSONNE
       * - LIEU
       * - ORGANISATION
       * - NATIONALITE
       * - HEURE
       * - AGE
       * - DATE_ABSOLUE
       * - DATE_RELATIVE
       * - FONCTION_ADMINISTRATIVE
       * - FONCTION_MILITAIRE
       * - FONCTION_POLITIQUE
       * - FONCTION_RELIGIEUSE
       * - FONCTION_ARISTOCRATIQUE
       * - DUREE,
       * - ADRESSE,
       * - TEMPERATURE,
       * - LONGUEUR,
       * - SURFACE,
       * - VOLUME,
       * - POIDS,
       * - VITESSE,
       * - VALEUR_MONETAIRE,
       * - AUTRE_MESURE,
       * - TELEPHONE,
       * - PRODUIT_DOCUMENT,
       * - PRODUIT_TRANSPORT,
       * - PRODUIT_ARTISTIQUE,
       * - PRODUIT_PRIX,
       * - ADRESSE_INTERNET,
       * - PRODUIT_COMMERCIAL,
       * - EVENEMENT,
       * - AUTRE
       * @author Cordial Group
       * @version 0.1
       * @date 2011
       */
      class NamedEntity: public BaseItem
      {
      protected:
	/**
	 * @brief Default constructor
	 */
	NamedEntity(const bool noInit = false);
      public:
	/**
	 * @brief Constructor
	 * @param cat the named entity category
	 */
	NamedEntity(namedentity::Category cat); //	throw(RootsException);
	/**
	 * @brief Constructor
	 * @param cat the named entity category
	 */
	NamedEntity(std::string cat); //	throw(RootsException);
	/**
	 * @brief Copy Constructor
	 * @param ne a named entity
	 */
	NamedEntity(const NamedEntity &ne);
	/**
	 * @brief Destructor
	 */
	virtual ~NamedEntity();
	/**
	 * @brief Duplicates the named entity
	 * @return A clone of the named entity
	 */
	virtual NamedEntity *clone() const;
	/**
	 * @brief Copy NamedEntity features into the current named entity
	 * @param ne
	 * @return reference to the current named entity
	 */
	NamedEntity& operator= (const NamedEntity &ne);
	/**
	 * @brief Returns the category of the named entity
	 * @return the category
	 */
	virtual namedentity::Category get_category() const { return category; }
	/**
	 * @brief Returns the category of the named entity
	 * @return the std::string representation of the category
	 */
	virtual std::string get_unicodestring_category() { return categoryToString[category]; }
	/**
	 * @brief Sets the category of a named entity
	 * @details This method is only applicable to some named entities. This
	 * method has to be overridden in subclasses.
	 * @throw RootsException
	 * @param cat the category
	 */
	virtual void set_category(namedentity::Category cat); //  throw(RootsException);
	/**
	 * @brief Sets the category of a named entity
	 * @details This method is only applicable to some named entities. This
	 * method has to be overridden in subclasses.
	 * @throw RootsException
	 * @param cat the category
	 */
	virtual void set_category(std::string cat); //  throw(RootsException);

	/**
	 * @brief Returns the value of an attribute
	 * @param key name of the attribute
	 * @throw RootsException exception thrown if the attribute is forbidden
	 * @return the value of the attribute
	 */
	virtual std::string get_attribute(std::string key); // throw(RootsException);
	/**
	 * @brief Sets an attribute of the named entity
	 * @details This method is applicable only to some named entities. If the attribute
	 * is not authorized, this methods throws an exception. This method is only applicable to some named entities. This
	 * method has to be overridden in subclasses.
	 * @param key name of the attribute
	 * @param value value of the attribute
	 * @throw RootsException exception thrown if the attribute is forbidden
	 */
	virtual void set_attribute(std::string key, std::string value); // throw(RootsException);

	/**
	 * @brief returns a string representation of any Roots object
	 * @param level the precision level of the output
	 * @return string representation of the object
	 */
	virtual std::string to_string(int level=0) const;
	/**
	 * @brief Write the entity into a RootsStream
	 * @param stream the stream from which we inflate the element
	 * @param is_terminal indicates that the node is terminal (true by default)
	 * @param parentNode
	 */
	virtual void deflate(RootsStream * stream, bool is_terminal=true, int list_index=-1);
	/**
	 * @brief Extracts the entity from the stream
	 * @param stream the stream from which we inflate the element
	 */
	virtual void inflate(RootsStream * stream, bool is_terminal=true, int list_index=-1);
	/**
	 * @brief Extracts the entity from the stream
	 * @param stream the stream from which we inflate the element
	 */
	static NamedEntity * inflate_object(RootsStream * stream, int list_index=-1);
	/**
	 * @brief returns the XML tag name value for current object
	 * @return string constant representing the XML tag name
	 */
	virtual std::string get_xml_tag_name() const { return xml_tag_name(); };
	/**
	 * @brief returns the XML tag name value for current class
	 * @return string constant representing the XML tag name
	 */
	static std::string xml_tag_name() { return "named_entity"; };
	/**
	 * @brief returns classname for current object
	 * @return string constant representing the classname
	 */
	virtual std::string get_classname() const { return classname(); };
	/**
	 * @brief returns classname for current class
	 * @return string constant representing the classname
	 */
	static std::string classname() { return "Linguistic::NamedEntity"; };
	/**
	 * @brief returns display color for current object
	 * @return string constant representing the pgf display color
	 */
	virtual std::string get_pgf_display_color() const { return pgf_display_color();};
	/**
	 * @brief returns display color for current class
	 * @return string constant representing the pgf display color
	 */
	static std::string pgf_display_color() { return "blue!25"; };
      protected:
	namedentity::Category category;	///< The category of the named entity
    std::map<std::string,std::string>	attributes; ///< Map containing the attributes under the form (key,value)
    static std::map<namedentity::Category, std::string>	categoryToString; ///< Static map to transform a category to a string
    static std::map<std::string, namedentity::Category>	stringToCategory; ///< Static map to transform a string to a category
      };

    } } }

#endif /* NAMEDENTITY_H_ */
