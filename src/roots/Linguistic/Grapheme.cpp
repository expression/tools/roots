/*
    This file is part of Roots.

    Roots is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Roots is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Roots.  If not, see <http://www.gnu.org/licenses/>.

    Copyright (c) 2013, 2014 IRISA
*/


#include "Grapheme.h"

roots::linguistic::Grapheme::Grapheme(const bool noInit) : BaseItem(noInit)
{
}

roots::linguistic::Grapheme::Grapheme(const UChar &aLabel) : BaseItem(aLabel)
{
}

roots::linguistic::Grapheme::Grapheme(const std::string &aLabel) : BaseItem(aLabel)
{
// Problem : a grapheme can be coded on 2 bytes (eg. é)
//  if(aLabel.length() != 1)
//    {throw RootsException ( __FILE__,__LINE__, "Grapheme must be one char long!\n" );}
}

roots::linguistic::Grapheme::Grapheme(const Grapheme &aChar) : BaseItem(aChar)
{
}

roots::linguistic::Grapheme::~Grapheme()
{
}

roots::linguistic::Grapheme *roots::linguistic::Grapheme::clone() const
{
  return new roots::linguistic::Grapheme(*this);
}

roots::linguistic::Grapheme &roots::linguistic::Grapheme::operator=(const roots::linguistic::Grapheme &achar)
{
  BaseItem::operator=(achar);
  return *this;
}

double roots::linguistic::Grapheme::compute_dissimilarity ( const roots::Base* b ) const
{
    if (const Grapheme* g = dynamic_cast<const Grapheme*>(b)) {
	  if (g->get_label() == this->get_label()) {
	    return roots::Base::MIN_DISSIMILARITY;
	  }
	  std::string g_lc = g->get_label(); 
	  std::transform(g_lc.begin(), g_lc.end(), g_lc.begin(), ::tolower);
	  std::string this_lc = this->get_label(); 
	  std::transform(this_lc.begin(), this_lc.end(), this_lc.begin(), ::tolower);
	  if (g_lc == this_lc) {
	    return (roots::Base::MIN_DISSIMILARITY + roots::Base::MAX_DISSIMILARITY)/2.0;
	  }
	  else {
	    return roots::Base::MAX_DISSIMILARITY;
	  }
	}
	else {
	  return ((Base *)this)->compute_dissimilarity(b);
	}
}


std::string roots::linguistic::Grapheme::to_string(int level) const
{
  std::stringstream ss;

  switch (level)
    {
      // level 0 : precise POS tag
    case 0:
    default:
      ss	<<  get_label();
      break;
    }
  return std::string(ss.str().c_str());
}

void roots::linguistic::Grapheme::deflate(RootsStream * stream, bool is_terminal, int list_index)
{
  BaseItem::deflate(stream,false,list_index);

  stream->append_unicodestring_content("text", this->get_label());

  if(is_terminal)
    stream->close_object();
}

void roots::linguistic::Grapheme::inflate(RootsStream * stream, bool is_terminal, int list_index)
{
  BaseItem::inflate(stream,false,list_index);

  this->set_label(stream->get_unicodestring_content("text"));
  //  this->set_label(stream->get_uchar_content("text"));

  if(is_terminal) stream->close_children();
}

roots::linguistic::Grapheme * roots::linguistic::Grapheme::inflate_object(RootsStream * stream, int list_index)
{
  Grapheme *t = new Grapheme(true);
  t->inflate(stream,true,list_index);
  return t;
}
