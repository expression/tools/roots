/*
    This file is part of Roots.

    Roots is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Roots is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Roots.  If not, see <http://www.gnu.org/licenses/>.

    Copyright (c) 2013, 2014 IRISA
*/


#ifndef SYNTAXNODE_H_
#define SYNTAXNODE_H_

#include "../../common.h"
#include "../../Tree/TreeNode.h"
#include "../../SequenceLinkedObject.h"
#include "../../RelationLinkedObject.h"


namespace roots { 
  namespace linguistic {
    namespace syntax {

      static const int SYNTACTIC_FUNCTION_LENGTH = 44;
      static const std::string SYNTACTIC_FUNCTION[] = {
	"syntactic_function_unknown", "group_unknown", "sentence", "relative", "independent", "principal",
	"subordinate", "coordinate", "interpolated", "infinitive", "participiale",
	"group_unknown", "is_subject_attribute", "in_subject_attribute", "is_COD",
	"in_COD", "is_COI", "in_COI", "is_agent", "is_infinitive_complement",
	"is_adverbial", "is_time_adverbial", "is_place_adverbial", "is_apposition",
	"in_apposition", "is_apostrophe", "in_apostrophe",
	"is_negative_complement", "is_subject", "in_subject", "personnal_pronoun",
	"verb", "indirect_object_subordinate", "is_real_subject",
	"in_real_subject", "adjective_adding", "subject_attribute_resumption",
	"COD_resumption", "COI_resumption", "adverbial_resumption", "noun_adding",
	"pronoun_adding", "subject_resumption", "verb_adding"
      };
      static const std::string SYNTACTIC_FUNCTION_SHORT[] = {
	"unkn.", "unkn.", "sent.", "rela.", "inde.", "prin.",
	"subo.", "coor.", "inte.", "infi.", "part.",
	"unkn.", "is.SA", "in.SA", "is.COD", "in.COD", "is.COI",
	"in.COI", "is.age.", "is.IC", "is.Adv.",
	"is.TAdv.", "is.PAdv.", "is.appo.",
	"in.appo.", "is.apos.", "in.apos.",
	"is.NComp.", "is.subj.", "in.subj.", "PPron.",
	"verb.", "IOS", "is.RSubj.",
	"in.RSubj.", "AA", "SAR",
	"COD.resu.", "COI.resu.", "AResu.", "NAddi.",
	"PAddi.", "SResu.", "VAddi."
      };
      /**
       * @brief The possible syntactic functions
       */
      enum SyntacticFunction
      {
	SYNTACTIC_FUNCTION_UNDEF				= 0,	       //!< SYNTACTIC_FUNCTION_UNDEF
	PROPOSITION_UNDEF					= 1,		    //!< PROPOSITION_UNDEF
	PROPOSITION_SENTENCE					= 2,		   //!< PROPOSITION_SENTENCE
	PROPOSITION_RELATIVE					= 3,		  //!< PROPOSITION_RELATIVE
	PROPOSITION_INDEPENDENT					= 4,		//!< PROPOSITION_INDEPENDENT
	PROPOSITION_PRINCIPAL					= 5,		 //!< PROPOSITION_PRINCIPAL
	PROPOSITION_SUBORDINATE					= 6,		//!< PROPOSITION_SUBORDINATE
	PROPOSITION_COORDINATE					= 7,		//!< PROPOSITION_COORDINATE
	PROPOSITION_INTERPOLATED				= 8,	       //!< PROPOSITION_INTERPOLATED
	PROPOSITION_INFINITIVE					= 9,		 //!< PROPOSITION_INFINITIVE
	PROPOSITION_PARTICIPIALE				= 10,		//!< PROPOSITION_PARTICIPIALE
	FUNCTION_GROUP_UNKNOWN					= 11,		//!< FUNCTION_GROUP_UNKNOWN
	FUNCTION_IS_SUBJECT_ATTRIBUTE			= 12,	   //!< FUNCTION_IS_SUBJECT_ATTRIBUTE
	FUNCTION_IN_SUBJECT_ATTRIBUTE			= 13,	   //!< FUNCTION_IN_SUBJECT_ATTRIBUTE
	FUNCTION_IS_COD					= 14,		      //!< FUNCTION_IS_COD
	FUNCTION_IN_COD					= 15,		      //!< FUNCTION_IN_COD
	FUNCTION_IS_COI					= 16,		      //!< FUNCTION_IS_COI
	FUNCTION_IN_COI					= 17,		      //!< FUNCTION_IN_COI
	FUNCTION_IS_AGENT				= 18,		     //!< FUNCTION_IS_AGENT
	FUNCTION_IS_INFINITIVE_COMPLEMENT		= 19,	//!< FUNCTION_IS_INFINITIVE_COMPLEMENT
	FUNCTION_IS_ADVERBIAL				= 20,	//!< FUNCTION_IS_ADVERBIAL
	FUNCTION_IS_TIME_ADVERBIAL			= 21,	//!< FUNCTION_IS_TIME_ADVERBIAL
	FUNCTION_IS_PLACE_ADVERBIAL			= 22,	//!< FUNCTION_IS_PLACE_ADVERBIAL
	FUNCTION_IS_APPOSITION				= 23,	//!< FUNCTION_IS_APPOSITION
	FUNCTION_IN_APPOSITION				= 24,	//!< FUNCTION_IN_APPOSITION
	FUNCTION_IS_APOSTROPHE				= 25,	//!< FUNCTION_IS_APOSTROPHE
	FUNCTION_IN_APOSTROPHE				= 26,	//!< FUNCTION_IN_APOSTROPHE
	FUNCTION_IS_NEGATIVE_COMPLEMENT			= 27,	  //!< FUNCTION_IS_NEGATIVE_COMPLEMENT
	FUNCTION_IS_SUBJECT				= 28,	   //!< FUNCTION_IS_SUBJECT
	FUNCTION_IN_SUBJECT				= 29,	   //!< FUNCTION_IN_SUBJECT
	FUNCTION_PERSONAL_PRONOUN			= 30,	      //!< FUNCTION_PERSONAL_PRONOUN
	FUNCTION_VERB					= 31,	       //!< FUNCTION_VERB
	FUNCTION_INDIRECT_OBJECT_SUBORDINATE	= 32,  //!< FUNCTION_INDIRECT_OBJECT_SUBORDINATE
	FUNCTION_IS_REAL_SUBJECT				= 33,	       //!< FUNCTION_IS_REAL_SUBJECT
	FUNCTION_IN_REAL_SUBJECT				= 34,	       //!< FUNCTION_IN_REAL_SUBJECT
	FUNCTION_ADJECTIVE_ADDING				= 35,	      //!< FUNCTION_ADJECTIVE_ADDING
	FUNCTION_SUBJECT_ATTRIBUTE_RESUMPTION	= 36,//!< FUNCTION_SUBJECT_ATTRIBUTE_RESUMPTION
	FUNCTION_COD_RESUMPTION					= 37,		//!< FUNCTION_COD_RESUMPTION
	FUNCTION_COI_RESUMPTION					= 38,		//!< FUNCTION_COI_RESUMPTION
	FUNCTION_ADVERBIAL_RESUMPTION			= 39,	   //!< FUNCTION_ADVERBIAL_RESUMPTION
	FUNCTION_NOUN_ADDING					= 40,		  //!< FUNCTION_NOUN_ADDING
	FUNCTION_PRONOUN_ADDING					= 41,		//!< FUNCTION_PRONOUN_ADDING
	FUNCTION_SUBJECT_RESUMPTION			= 42,	     //!< FUNCTION_SUBJECT_RESUMPTION
	FUNCTION_VERB_ADDING					= 43		  //!< FUNCTION_VERB_ADDING
      };

      /**
       * @brief The node value in the syntax tree
       * @details
       * the node value describes a group in the syntax tree. The description is
       * composed by the indexes of the different words involved in the group
       * and the kind of this group. Furthermore, the leaf of this tree
       * contains an array of indexes of words.
       * @author Cordial Group
       * @version 0.1
       * @date 2011
       * @todo to_pgf
       */
      class SyntaxNode: public roots::tree::TreeNode
	{
	public:
	  /**
	   * @brief Default constructor with no base init
	   */
	  SyntaxNode(const bool noInit = false);
	  /**
	   * @brief Constructor
	   */
	  SyntaxNode(const SyntaxNode&);
	  /**
	   * @brief Destructor
	   */
	  virtual ~SyntaxNode();
	  /**
	   * @brief clone the current object
	   * @return a copy of the object
	   */
	  virtual SyntaxNode *clone() const;
	  /**
	   * @brief Copy a node on the current one
	   * @param node
	   * @return reference on the current node
	   */
	  SyntaxNode& operator= (const SyntaxNode& node);
	  /**
	   * @brief Compares the content of two syntax nodes
	   * @param node
	   * @return true if node are different
	   */
	  bool operator!=(const SyntaxNode& node) const;
	  /**
	   * @brief Compares the content of two syntax nodes
	   * @param node
	   * @return true if node are equal
	   */
	  bool operator==(const SyntaxNode& node) const;

	  void translate_index(int offset);


	  bool is_proposition_sentence() const {return syntacticFunction == PROPOSITION_SENTENCE;}
	  bool is_proposition_relative() const {return syntacticFunction == PROPOSITION_RELATIVE;}
	  bool is_proposition_independent() const {return syntacticFunction == PROPOSITION_INDEPENDENT;}
	  bool is_proposition_principal() const {return syntacticFunction == PROPOSITION_PRINCIPAL;}
	  bool is_proposition_subordinate() const {return syntacticFunction == PROPOSITION_SUBORDINATE;}
	  bool is_proposition_coordinate() const {return syntacticFunction == PROPOSITION_COORDINATE;}
	  bool is_proposition_interpolated() const {return syntacticFunction == PROPOSITION_INTERPOLATED;}
	  bool is_proposition_infinitive() const {return syntacticFunction == PROPOSITION_INFINITIVE;}
	  bool is_proposition_participiale() const {return syntacticFunction == PROPOSITION_PARTICIPIALE;}

	  bool is_group_unknown() const {return syntacticFunction == FUNCTION_GROUP_UNKNOWN;}
	  bool is_group_is_subject_attribute() const {return syntacticFunction == FUNCTION_IS_SUBJECT_ATTRIBUTE;}
	  bool is_group_in_subject_attribute() const {return syntacticFunction == FUNCTION_IN_SUBJECT_ATTRIBUTE;}
	  bool is_group_is_COD() const {return syntacticFunction == FUNCTION_IS_COD;}
	  bool is_group_in_COD() const {return syntacticFunction == FUNCTION_IN_COD;}
	  bool is_group_is_COI() const {return syntacticFunction == FUNCTION_IS_COI;}
	  bool is_group_in_COI() const {return syntacticFunction == FUNCTION_IN_COI;}
	  bool is_group_is_agent() const {return syntacticFunction == FUNCTION_IS_AGENT;}
	  bool is_group_is_infinitive_complement() const {return syntacticFunction == FUNCTION_IS_INFINITIVE_COMPLEMENT;}
	  bool is_group_is_adverbial() const {return syntacticFunction == FUNCTION_IS_ADVERBIAL;}
	  bool is_group_is_time_adverbial() const {return syntacticFunction == FUNCTION_IS_TIME_ADVERBIAL;}
	  bool is_group_is_place_adverbial() const {return syntacticFunction == FUNCTION_IS_PLACE_ADVERBIAL;}
	  bool is_group_is_apposition() const {return syntacticFunction == FUNCTION_IS_APPOSITION;}
	  bool is_group_in_apposition() const {return syntacticFunction == FUNCTION_IN_APPOSITION;}
	  bool is_group_is_apostrophe() const {return syntacticFunction == FUNCTION_IS_APOSTROPHE;}
	  bool is_group_in_apostrophe() const {return syntacticFunction == FUNCTION_IN_APOSTROPHE;}
	  bool is_group_is_negative_complement() const {return syntacticFunction == FUNCTION_IS_NEGATIVE_COMPLEMENT;}
	  bool is_group_is_subject() const {return syntacticFunction == FUNCTION_IS_SUBJECT;}
	  bool is_group_in_subject() const {return syntacticFunction == FUNCTION_IN_SUBJECT;}
	  bool is_group_personnal_pronoun() const {return syntacticFunction == FUNCTION_PERSONAL_PRONOUN;}
	  bool is_group_verb() const {return syntacticFunction == FUNCTION_VERB;}
	  bool is_group_indirect_object_subordinate() const {return syntacticFunction == FUNCTION_INDIRECT_OBJECT_SUBORDINATE;}
	  bool is_group_is_real_subject() const {return syntacticFunction == FUNCTION_IS_REAL_SUBJECT;}
	  bool is_group_in_real_subject() const {return syntacticFunction == FUNCTION_IN_REAL_SUBJECT;}
	  bool is_group_adjective_adding() const {return syntacticFunction == FUNCTION_ADJECTIVE_ADDING;}
	  bool is_group_subject_attribute_resumption() const {return syntacticFunction == FUNCTION_SUBJECT_ATTRIBUTE_RESUMPTION;}
	  bool is_group_COD_resumption() const {return syntacticFunction == FUNCTION_COD_RESUMPTION;}
	  bool is_group_COI_resumption() const {return syntacticFunction == FUNCTION_COI_RESUMPTION;}
	  bool is_group_adverbial_resumption() const {return syntacticFunction == FUNCTION_ADVERBIAL_RESUMPTION;}
	  bool is_group_noun_adding() const {return syntacticFunction == FUNCTION_NOUN_ADDING;}
	  bool is_group_pronoun_adding() const {return syntacticFunction == FUNCTION_PRONOUN_ADDING;}
	  bool is_group_subject_resumption() const {return syntacticFunction == FUNCTION_SUBJECT_RESUMPTION;}
	  bool is_group_verb_adding() const {return syntacticFunction == FUNCTION_VERB_ADDING;}

	  void set_start(int astart) { start = astart;}
	  void set_end(int aend) { end = aend;}
	  void set_syntactic_function(SyntacticFunction atype) { syntacticFunction = atype;}
	  void set_main_word_index(int amainWordIndex) { mainWordIndex = amainWordIndex;}
	  void set_word_index_array(const std::vector<int>& awordIndexArray) { wordIndexArray = awordIndexArray;}

	  int get_start() const {return start;};
	  int get_end() const {return end;};
	  SyntacticFunction get_syntactic_function() const {return syntacticFunction;};

	  int get_main_word_index() const {return mainWordIndex;};
	  const std::vector<int>& get_word_index_array() const {	return wordIndexArray;	};
	  std::vector<int>& get_word_index_array() {	return wordIndexArray;	};

	  virtual int get_first_target_index() const;
	  virtual int get_last_target_index() const;

		/**
		 * @brief Modifies the current syllable to take into account the insertion of an element into the related sequence
		 * @param index index of the element added in the related sequence
		 **/
		void insert_related_element(int index);

		/**
		 * @brief Modifies the current syllable to take into account the deletion of an element into the related sequence
		 * @param index index of the element removed from the related sequence
		 **/
		void remove_related_element(int index);

	  virtual std::string to_string(int level=0) const;

	  virtual roots::tree::TreeNode* new_child(const bool noInit = false) const;

	  /**
	   * @brief Build a dot representation of the tree
	   * @param level
	   * @param rank prefix counter used for each node
	   * @return
	   */
	  virtual std::string output_dot(unsigned int level=0, unsigned int rank=0) const;

	  /**
	   * @brief Extracts the entity from the stream
	   * @param stream the stream from which we inflate the element
	   */
	  static SyntaxNode * inflate_object(RootsStream * stream, int list_index=-1);
	  /**
	   * @brief returns the XML tag name value for current object
	   * @return string constant representing the XML tag name
	   */
	  virtual std::string get_xml_tag_name() const
	  {
	    return xml_tag_name();
	  };
	  /**
	   * @brief returns the XML tag name value for current class
	   * @return string constant representing the XML tag name
	   */
	  static std::string xml_tag_name()
	  {
	    return "syntax_node";
	  };
	  /**
	   * @brief returns classname for current object
	   * @return string constant representing the classname
	   */
	  virtual std::string get_classname() const
	  {
	    return classname();
	  };
	  /**
	   * @brief returns classname for current class
	   * @return string constant representing the classname
	   */
	  static std::string classname()
	  {
	    return "Linguistic::Syntax::Node";
	  };
	  /**
	   * @brief returns display color for current object
	   * @return string constant representing the pgf display color
	   */
	  virtual std::string get_pgf_display_color() const { return pgf_display_color();};
	  /**
	   * @brief returns display color for current class
	   * @return string constant representing the pgf display color
	   */
	  static std::string pgf_display_color() { return "red!25"; };

	  /**
	   * Serialize the object into a stream as a string
	   *
	   * @param out
	   * @param syntaxNode
	   * @return the modified stream
	   */
	  friend std::ostream& operator<<(std::ostream& out, SyntaxNode& syntaxNode);

	protected:
	  /**
	   * @brief Deflates the specific content of the current node
	   * @param stream
	   */
	  virtual void deflate_content(RootsStream * stream, int list_index=-1);
	  /**
	   * @brief Extracts the entity from the stream
	   * @param stream the stream from which we inflate the element
	   */
	  virtual void inflate_content(RootsStream * stream, bool is_terminal=true, int list_index=-1);

	private:
	  int start; /**< the start index in the word sequence */
	  int end; /**< the end index in the word sequence */
	  SyntacticFunction syntacticFunction; /**< the kind of group */
	  int mainWordIndex; /**< main word index in the word sequence */
	  std::vector<int> wordIndexArray; /**< array which contains the word' indices */
	};

    } } }

#endif /* SYNTAXNODE_H_ */
