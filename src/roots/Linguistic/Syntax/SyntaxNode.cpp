/*
    This file is part of Roots.

    Roots is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Roots is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Roots.  If not, see <http://www.gnu.org/licenses/>.

    Copyright (c) 2013, 2014 IRISA
*/


#include "SyntaxNode.h"

namespace roots
{

  namespace linguistic
  {

    namespace syntax
    {
      /*const std::string SyntaxNode::SYNTACTIC_FUNCTION[] = {
	"-","Principale", "Relative", "Independent", "Subordinate",
	"Coordinate", "Incise", "Infinitive", "Participial", "A", "B", "C",
	"D", "E", "F", "G", "I", "H", "K", "L", "M", "N", "O", "P", "Q",
	"S", "T", "U", "V", "W", "Y", "Z", "a", "c", "d", "e", "h", "n",
	"p", "s", "t"
	};*/


      SyntaxNode::SyntaxNode(const bool noInit) : TreeNode(noInit)
      {
	start = -1;
	end = -1;
	syntacticFunction = SYNTACTIC_FUNCTION_UNDEF;
	mainWordIndex = -1;
	wordIndexArray = std::vector<int>();
      }

      SyntaxNode::SyntaxNode(const SyntaxNode& node) : TreeNode(node)
      {
	set_start(node.get_start());
	set_end(node.get_end());
	set_syntactic_function(node.get_syntactic_function());
	set_main_word_index(node.get_main_word_index());
	set_word_index_array(node.get_word_index_array());
      }


      SyntaxNode::~SyntaxNode()
      {
      }

      SyntaxNode *SyntaxNode::clone() const
      {
	return new SyntaxNode(*this);
      }

      SyntaxNode& SyntaxNode::operator=(const SyntaxNode &node)
      {
	TreeNode::operator=(node);
	set_start(node.get_start());
	set_end(node.get_end());
	set_syntactic_function(node.get_syntactic_function());
	set_main_word_index(node.get_main_word_index());
	set_word_index_array(node.get_word_index_array());
	return *this;
      }

      void SyntaxNode::translate_index(int offset)
      {
	std::vector<int>::iterator it;

	start += offset;
	end += offset;
	mainWordIndex += offset;

	for(it = wordIndexArray.begin(); it != wordIndexArray.end(); ++it)
	  {
	    *it += offset;
	  }
      }


      bool SyntaxNode::operator!=(const SyntaxNode& node) const
      {
	return not (*this == node);
      }

      bool SyntaxNode::operator==(const SyntaxNode& node) const
      {
	return (this->start == node.get_start())
	  && (this->end == node.get_end())
	  && (this->syntacticFunction == node.get_syntactic_function())
	  && (this->mainWordIndex == node.get_main_word_index())
	  && (this->wordIndexArray == node.get_word_index_array());
      }

      int SyntaxNode::get_first_target_index() const
      {
	/*SyntaxNode *leaf = static_cast<SyntaxNode*>(const_cast<SyntaxNode *>(this)->get_first_leaf());
	  if(leaf == NULL)
	  {
	  return -1;
	  }
	  return leaf->get_word_index_array()[0];*/
	return this->get_start();
      }

      int SyntaxNode::get_last_target_index() const
      {
	/*SyntaxNode *leaf = static_cast<SyntaxNode*>(const_cast<SyntaxNode *>(this)->get_last_leaf());
	  if(leaf == NULL)
	  {
	  return -1;
	  }
	  std::vector<int> wIndexArray = leaf->get_word_index_array();
	  return wIndexArray[wIndexArray.size() - 1];*/
	return this->get_end();
      }

      void SyntaxNode::insert_related_element(int index)
      {
	for(std::vector<int>::iterator itw=wordIndexArray.begin();
	    itw!=wordIndexArray.end(); ++itw)
	  {
	    (*itw) = ((*itw)>=index) ? (*itw)+1 : (*itw);
	  }

	set_start(wordIndexArray[0]);
	set_end(wordIndexArray.back());
	if(mainWordIndex>=index) ++mainWordIndex;
      }

      void SyntaxNode::remove_related_element(int index)
      {
	//std::cerr << "plouf "<< index << " on " << this << std::endl;

	for(std::vector<int>::iterator itw=wordIndexArray.begin();
	    itw!=wordIndexArray.end(); ++itw)
	  {
	    (*itw) = ((*itw)>index) ? (*itw)-1 : (*itw);
	  }

	//std::cerr << "wa of "<< this << " size "<< wordIndexArray.size() << std::endl;
	if(wordIndexArray.size()>0)
	  {
	    set_start(wordIndexArray[0]);
	    set_end(wordIndexArray.back());

	    if(mainWordIndex==index)
	      {
		if(mainWordIndex>get_start() && mainWordIndex<get_end()) mainWordIndex--;
		else
		  {
		    if(mainWordIndex<get_start()) mainWordIndex=get_start();
		    if(mainWordIndex>get_end()) mainWordIndex=get_end();
		  }
	      }
	  }
	else
	  {
	    int st = get_start();
	    if( st > index)
	      {set_start(st -1);}
	    int en = get_end();
	    if( en > index) 
	      {set_end(en -1);}
	    if(mainWordIndex > index)
	      {set_main_word_index(mainWordIndex -1);}
	  }
      }

      std::string SyntaxNode::to_string(int level) const
      {
	std::stringstream ss;

	switch (level)
	  {
	    // level 0 : precise SyntaxNode tag
	  case 0:
	    ss	<<  SYNTACTIC_FUNCTION_SHORT[get_syntactic_function()] ;
	    break;
	  case 1:
	    //cout << "!!  " << get_syntactic_function() << " -- " << get_main_word_index() << " -- " << get_first_target_index() << " - " << get_last_target_index() << std::endl;fflush(stdout);
	    //cout << "!!  " << SYNTACTIC_FUNCTION_SHORT[get_syntactic_function()] << std::endl;
	    ss	<<  SYNTACTIC_FUNCTION[get_syntactic_function()] ;
	    break;
	  default:
	    //cout << "!!  " << get_syntactic_function() << " -- " << get_main_word_index() << " -- " << get_first_target_index() << " - " << get_last_target_index() << std::endl;fflush(stdout);
	    //cout << "!!  " << SYNTACTIC_FUNCTION_SHORT[get_syntactic_function()] << std::endl;
	    ss	<<  SYNTACTIC_FUNCTION_SHORT[get_syntactic_function()] ;
	    break;
	  }
	return std::string(ss.str().c_str());
      }

      std::string SyntaxNode::output_dot(unsigned int level, unsigned int rank) const
      {
	std::stringstream ss, ss2;
	std::string str;

	ss << "\"" << rank << " ";

	const std::vector<int>& refWordIndexArray = this->wordIndexArray;
	int startIndex = this->get_start();
	int endIndex = this->get_end();

	if(refWordIndexArray.size() > 0)
	  {
	    ss << "" << syntax::SYNTACTIC_FUNCTION[this->get_syntactic_function()] << " (";
	    for(unsigned int idx=0;idx<refWordIndexArray.size();idx++)
	      ss << refWordIndexArray[idx] << " ";
	    ss << ")" << "\"";
	  }else{
	  ss << "" << level << ":" << syntax::SYNTACTIC_FUNCTION[this->get_syntactic_function()] << " [";
	  ss << startIndex << " " << endIndex;
	  ss << "]" << "\"";
	}

	if(get_number_of_children() == 0)
	  {
	    str = std::string(ss.str().c_str());
	    for(unsigned int idx=0;idx<refWordIndexArray.size();idx++)
	      ss2 << str << "-> \" " << refWordIndexArray[idx] << "\" ;" << std::endl;
	    str = std::string(ss2.str().c_str());
	  }else{
	  str = std::string(ss.str().c_str());

	  int childIndex = 0;
	  while(childIndex < get_number_of_children())
	    {
	      ss2 << str << " -> " << get_children()[childIndex]->output_dot(level+1, ++rank);
	      ++childIndex;
	    }
	  str = std::string(ss2.str().c_str());
	}

	return str;
      }


      void SyntaxNode::deflate_content (RootsStream * stream, int list_index)
      {
	Base::deflate(stream,false,list_index);
	stream->append_int_content("start_word_index", this->get_start());
	stream->append_int_content("end_word_index", this->get_end());
	stream->append_int_content("syntactic_function", this->get_syntactic_function());
	stream->append_int_content("main_word_index", this->get_main_word_index());
	stream->append_vector_int_content("word_index_array", this->get_word_index_array());
	stream->close_object();
      }

      roots::tree::TreeNode* SyntaxNode::new_child(const bool noInit) const
      {
	//cout << "NEW CHILD SYNTAXNODE" << std::endl;
	return new SyntaxNode(noInit);
      }


      void SyntaxNode::inflate_content(RootsStream * stream, bool is_terminal, int list_index)
      {
	Base::inflate(stream, false, list_index);

	//stream->open_children();

	this->set_start(stream->get_int_content("start_word_index"));
	this->set_end(stream->get_int_content("end_word_index"));

	int synfun = stream->get_int_content("syntactic_function");
	if(synfun < SYNTACTIC_FUNCTION_LENGTH) this->set_syntactic_function((SyntacticFunction)synfun);

	this->set_main_word_index(stream->get_int_content("main_word_index"));
	this->set_word_index_array(stream->get_vector_int_content("word_index_array"));

	if(is_terminal) stream->close_children();
      }

      SyntaxNode * SyntaxNode::inflate_object(RootsStream * stream, int list_index)
      {
	SyntaxNode *t = new SyntaxNode(true);
	t->inflate(stream,true,list_index);
	return t;
      }

    std::ostream& operator<<(std::ostream& out, SyntaxNode& syntaxNode)
      {
	out << syntaxNode.to_string() << std::endl;
	return out;
      }

    } } } // END OF NAMESPACES
