/*
    This file is part of Roots.

    Roots is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Roots is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Roots.  If not, see <http://www.gnu.org/licenses/>.

    Copyright (c) 2013, 2014 IRISA
*/


#ifndef SYNTAX_H_
#define SYNTAX_H_

#include "../common.h"
#include "../Sequence.h"
#include "../Sequence/WordSequence.h"
#include "../Phonology/Syllable.h"
#include "Syntax/SyntaxNode.h"
#include "../Tree.h"
#include "../Tree/TreeNode.h"


namespace roots { namespace linguistic { namespace syntax {

/**
 * @brief The Syntax tree
 * @details
 * Models the syntax tree of an utterance. Each node
 * corresponds to a syntax level; each leaf is linked to a Word.
 * @author Cordial Group
 * @version 0.1
 * @date 2011
 */
class Syntax: public roots::tree::Tree<roots::linguistic::syntax::SyntaxNode>
{
public:

	Syntax(const bool noInit = false);
	Syntax(const Syntax &syntax);
	
	virtual ~Syntax();
	/**
	 * @brief clone the current object
	 * @return a copy of the object
	 */
	virtual Syntax *clone() const;

	Syntax& operator= (const Syntax&);

	static const int SYNAPSE = 0x00;


	sequence::WordSequence* get_word_sequence() const {return _wordSequence;} ;
	virtual sequence::Sequence* get_related_sequence() const {return get_word_sequence();} ;
	void set_word_sequence(sequence::WordSequence* wseq) { _wordSequence = wseq; }
	void set_related_sequence(sequence::Sequence* _relatedSequence){ set_word_sequence(_relatedSequence->as<sequence::WordSequence>());}
	bool has_related_sequence() const {return true;}
	void translate_index(int offset);


	const std::string get_word_sequence_id() const;
	std::vector<int> get_word_index_array();

	int get_first_target_index() const;
	int get_last_target_index() const;

	int get_word_depth(int wordIndex) const;

	SyntaxNode* get_word_syntax_node(int wordIndex) const;
	std::vector<SyntaxNode*> get_syntax_node_path_to_word(int wordIndex) const;

private:
	/**
	 * @brief Build a dot representation of the tree
	 * @param rootNode
	 * @param level
	 * @return
	 */
	//virtual std::string output_dot(kptree::tree<SyntaxNode>::iterator rootNode=NULL, unsigned int level=1) const;
	/**
	 * @brief Builds a PGF/Tikz representation of the tree
	 * @param xCoord
	 * @param yCoord
	 * @param xLength
	 * @param yLength
	 * @param level
	 * @param mapping
	 * @param sequenceId
	 * @param nodePart
	 * @param nodePartIndex
	 * @param rootNode
	 * @return
	 */
	/*virtual std::pair<std::string, std::vector<roots::matrix::GridElement> >  output_pgf(int xCoord=0, int yCoord=0, int xLength=2,
				int yLength=2, int xScaleMillimeter=2, int yScaleMillimeter=2, int level=0,
				roots::matrix::GridMapping mapping = roots::matrix::GridMapping(),
				std::string sequenceId="",
				int nodePart=1, int nodePartIndex=0,
				kptree::tree<SyntaxNode>::iterator rootNode=NULL) const;*/
	/**
	 * @brief Builds a matrix representation of the tree
	 * @param matrix output matrix (must be pre-allocated)
	 * @param height height of the syntax tree
	 * @param length length of the matrix
	 * @param xCoord used internally (default value must be used for first call)
	 * @param yCoord used internally (default value must be used for first call)
	 * @param mapping mapping between the tree and the target word sequence
	 * @param rootNode
	 * @return mapping to the elements (for internal use)
	 */
	/*virtual std::vector<roots::matrix::GridElement> output_aligned_matrix(
				Base* matrix[], int height, int length,
				int xCoord=0, int yCoord=0,				
				roots::matrix::GridMapping mapping = roots::matrix::GridMapping(),				
				kptree::tree<SyntaxNode>::iterator rootNode=NULL);*/



public:
	/**
	 * @brief returns a string representation of any Roots object
	 * @param level the precision level of the output
	 * @return string representation of the object
	 */
	virtual std::string to_string(int level=0) const;
	
	/**
	 * @brief Write the entity into a RootsStream
	 * @param stream the stream from which we inflate the element
	 * @param is_terminal indicates that the node is terminal (true by default)
	 */
	virtual void deflate(RootsStream * stream, bool is_terminal=true, int list_index=-1);
	/**
	 * @brief Extracts the entity from the stream
	 * @param stream the stream from which we inflate the element
	 */
	virtual void inflate(RootsStream * stream, bool is_terminal=true, int list_index=-1);
	/**
	 * @brief Extracts the entity from the stream
	 * @param stream the stream from which we inflate the element
	 */
	static Syntax * inflate_object(RootsStream * stream, int list_index=-1);

	/**
	 * @brief returns the XML tag name value for current object
	 * @return string constant representing the XML tag name
	 */
	virtual std::string get_xml_tag_name() const
	{
		return xml_tag_name();
	};
	/**
	 * @brief returns the XML tag name value for current class
	 * @return string constant representing the XML tag name
	 */
	static std::string xml_tag_name()
	{
		return "syntax";
	};
	/**
	 * @brief returns classname for current object
	 * @return string constant representing the classname
	 */
	virtual std::string get_classname() const
	{
		return classname();
	};
	/**
	 * @brief returns classname for current class
	 * @return string constant representing the classname
	 */
	static std::string classname()
	{
		return "Linguistic::Syntax";
	};
	/**
	 * @brief returns display color for current object
	 * @return string constant representing the pgf display color
	 */
	virtual std::string get_pgf_display_color() const { return pgf_display_color();};
	/**
	 * @brief returns display color for current class
	 * @return string constant representing the pgf display color
	 */
	static std::string pgf_display_color() { return "red!25"; };
	/**
	 * Serialize the object into a stream as a string
	 *
	 * @param out
	 * @param syntax
	 * @return the modified stream
	 */
	friend std::ostream& operator<<(std::ostream& out, Syntax& syntax);

private:
	sequence::WordSequence *_wordSequence; /**< reference on the word sequence */
};

} } }

#endif /* SYNTAX_H_ */
