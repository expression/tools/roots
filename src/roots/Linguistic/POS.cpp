/*
    This file is part of Roots.

    Roots is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Roots is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Roots.  If not, see <http://www.gnu.org/licenses/>.

    Copyright (c) 2013, 2014 IRISA
*/


#include "POS.h"


namespace roots {
  namespace linguistic {
    namespace pos
    {


      std::map<Degree, std::string> POS::degreeToString = boost::assign::map_list_of
	(POS_DEGREE_UNKNOWN, "unknown")
	(POS_DEGREE_POSITIVE, "positive") //DEGREE_POSITIVE = 'p'
	(POS_DEGREE_NEGATIVE, "negative") //DEGREE_POSITIVE = 'p'
	(POS_DEGREE_COMPARATIVE, "comparative") //DEGREE_COMPARATIVE = 'c'
	(POS_DEGREE_SUPERLATIVE, "surperlative"); //DEGREE_SUPERLATIVE = 's'
      std::map<Degree, char> POS::degreeToChar = boost::assign::map_list_of
	(POS_DEGREE_UNKNOWN, 'u')
	(POS_DEGREE_NEGATIVE, 'n')
	(POS_DEGREE_POSITIVE, 'p')
	(POS_DEGREE_COMPARATIVE, 'c')
	(POS_DEGREE_SUPERLATIVE, 's');
      std::map<std::string, Degree> POS::stringToDegree = boost::assign::map_list_of
	("unknown", POS_DEGREE_UNKNOWN)
	("positive", POS_DEGREE_POSITIVE)
	("negative", POS_DEGREE_NEGATIVE)
	("comparative", POS_DEGREE_COMPARATIVE)
	("surperlative", POS_DEGREE_SUPERLATIVE);


      std::map<Gender, std::string> POS::genderToString = boost::assign::map_list_of
	(POS_GENDER_UNKNOWN, "unknown")
	(POS_GENDER_MALE, "male")
	(POS_GENDER_FEMALE, "female")
	(POS_GENDER_COMMON, "common")
	(POS_GENDER_NEUTRAL, "neutral");
      std::map<Gender, char> POS::genderToChar = boost::assign::map_list_of
	(POS_GENDER_UNKNOWN, 'u')
	(POS_GENDER_MALE, 'm')
	(POS_GENDER_FEMALE, 'f')
	(POS_GENDER_COMMON, 'c')
	(POS_GENDER_NEUTRAL, 'n');
      std::map<std::string, Gender> POS::stringToGender = boost::assign::map_list_of
	("unknown", POS_GENDER_UNKNOWN)
	("male", POS_GENDER_MALE)
	("female", POS_GENDER_FEMALE)
	("common", POS_GENDER_COMMON)
	("neutral", POS_GENDER_NEUTRAL);



      std::map<Number, std::string> POS::numberToString = boost::assign::map_list_of
	(POS_NUMBER_UNKNOWN, "unknown")
	(POS_NUMBER_SINGULAR, "singular")
	(POS_NUMBER_PLURAL, "plural");
      std::map<Number, char> POS::numberToChar = boost::assign::map_list_of
	(POS_NUMBER_UNKNOWN, 'u')
	(POS_NUMBER_SINGULAR, 's')
	(POS_NUMBER_PLURAL, 'p');
      std::map<std::string, Number> POS::stringToNumber = boost::assign::map_list_of
	("unknown", POS_NUMBER_UNKNOWN)("singular", POS_NUMBER_SINGULAR) ("plural", POS_NUMBER_PLURAL);


      std::map<ConjunctionCategory, std::string> POS::conjunctionCategoryToString = boost::assign::map_list_of
	(POS_CONJUNCTION_CATEGORY_UNKNOWN, "unknown")
	(POS_CONJUNCTION_CATEGORY_COORDINATING, "coordinating")
	(POS_CONJUNCTION_CATEGORY_SUBORDINATING, "subordinating");
      std::map<ConjunctionCategory, char> POS::conjunctionCategoryToChar = boost::assign::map_list_of
	(POS_CONJUNCTION_CATEGORY_UNKNOWN, 'u')
	(POS_CONJUNCTION_CATEGORY_COORDINATING, 'c')
	(POS_CONJUNCTION_CATEGORY_SUBORDINATING, 's');
      std::map<std::string, ConjunctionCategory> POS::stringToConjunctionCategory = boost::assign::map_list_of
	("unknown", POS_CONJUNCTION_CATEGORY_UNKNOWN)
	("coordinating", POS_CONJUNCTION_CATEGORY_COORDINATING)
	("subordinating", POS_CONJUNCTION_CATEGORY_SUBORDINATING);


      std::map<Category, std::string> POS::categoryToString = boost::assign::map_list_of
	(POS_CATEGORY_UNKNOWN, "unknown")
	(POS_CATEGORY_PERSONAL, "personal")
	(POS_CATEGORY_DEMONSTRATIVE, "demonstrative")
	(POS_CATEGORY_POSSESSIVE, "possessive")
	(POS_CATEGORY_NUMERAL, "numeral")
	(POS_CATEGORY_INTERROGATIVE, "interrogative")
	(POS_CATEGORY_EXCLAMATIVE, "exclamative")
	(POS_CATEGORY_RELATIVE, "relative")
	(POS_CATEGORY_REFLEXIVE, "reflexive")
	(POS_CATEGORY_INDEFINITE, "indefinite");
      std::map<Category, char> POS::categoryToChar = boost::assign::map_list_of
	(POS_CATEGORY_UNKNOWN, 'u')
	(POS_CATEGORY_PERSONAL, 'p')
	(POS_CATEGORY_DEMONSTRATIVE, 'd')
	(POS_CATEGORY_POSSESSIVE, 's')
	(POS_CATEGORY_NUMERAL, 'n')
	(POS_CATEGORY_INTERROGATIVE, 'i')
	(POS_CATEGORY_EXCLAMATIVE, 'e')
	(POS_CATEGORY_RELATIVE, 'r')
	(POS_CATEGORY_REFLEXIVE, 'x')
	(POS_CATEGORY_INDEFINITE, 'f');
      std::map<std::string, Category> POS::stringToCategory = boost::assign::map_list_of
	("unknown", POS_CATEGORY_UNKNOWN)
	("personal", POS_CATEGORY_PERSONAL)
	("demonstrative", POS_CATEGORY_DEMONSTRATIVE)
	("possessive", POS_CATEGORY_POSSESSIVE)
	("numeral", POS_CATEGORY_NUMERAL)
	("interrogative", POS_CATEGORY_INTERROGATIVE)
	("exclamative", POS_CATEGORY_EXCLAMATIVE)
	("relative", POS_CATEGORY_RELATIVE)
	("reflexive", POS_CATEGORY_REFLEXIVE)
	("indefinite", POS_CATEGORY_INDEFINITE);


      std::map<PunctuationCategory, std::string> POS::punctuationCategoryToString = boost::assign::map_list_of
	(POS_PUNCTUATION_CATEGORY_UNKNOWN, "unknown")
	(POS_PUNCTUATION_CATEGORY_FINAL, "final")
	(POS_PUNCTUATION_CATEGORY_PAUSE, "pause")
	(POS_PUNCTUATION_CATEGORY_INSERT, "insert")
	(POS_PUNCTUATION_CATEGORY_INSERT_CLOSE, "close")
	(POS_PUNCTUATION_CATEGORY_INSERT_OPEN, "open");
      std::map<PunctuationCategory, char> POS::punctuationCategoryToChar = boost::assign::map_list_of
	(POS_PUNCTUATION_CATEGORY_UNKNOWN, 'u')
	(POS_PUNCTUATION_CATEGORY_FINAL, 'f')
	(POS_PUNCTUATION_CATEGORY_PAUSE, 'p')
	(POS_PUNCTUATION_CATEGORY_INSERT, 'i')
	(POS_PUNCTUATION_CATEGORY_INSERT_CLOSE, 'c')
	(POS_PUNCTUATION_CATEGORY_INSERT_OPEN, 'o');
      std::map<std::string, PunctuationCategory> POS::stringToPunctuationCategory = boost::assign::map_list_of
	("unknown", POS_PUNCTUATION_CATEGORY_UNKNOWN)
	("final", POS_PUNCTUATION_CATEGORY_FINAL)
	("pause", POS_PUNCTUATION_CATEGORY_PAUSE)
	("insert", POS_PUNCTUATION_CATEGORY_INSERT)
	("close", POS_PUNCTUATION_CATEGORY_INSERT_CLOSE)
	("open", POS_PUNCTUATION_CATEGORY_INSERT_OPEN);


      std::map<Mood, std::string> POS::moodToString = boost::assign::map_list_of
	(POS_MOOD_UNKNOWN, "unknown")
	(POS_MOOD_INFINITIVE, "infinitive")
	(POS_MOOD_INDICATIVE, "indicative")
	(POS_MOOD_CONDITIONAL, "conditional")
	(POS_MOOD_SUBJONCTIVE, "subjonctive")
	(POS_MOOD_IMPERATIVE, "imperative")
	(POS_MOOD_PARTICIPLE, "participle")
	(POS_MOOD_GERUND, "gerund");
      std::map<Mood, char> POS::moodToChar = boost::assign::map_list_of
	(POS_MOOD_UNKNOWN, 'u')
	(POS_MOOD_INFINITIVE, 'n')
	(POS_MOOD_INDICATIVE, 'i')
	(POS_MOOD_CONDITIONAL, 'c')
	(POS_MOOD_SUBJONCTIVE, 's')
	(POS_MOOD_IMPERATIVE, 'f')
	(POS_MOOD_PARTICIPLE, 'p')
	(POS_MOOD_GERUND, 'g');
      std::map<std::string, Mood> POS::stringToMood = boost::assign::map_list_of
	("unknown", POS_MOOD_UNKNOWN)
	("infinitive", POS_MOOD_INFINITIVE)
	("indicative", POS_MOOD_INDICATIVE)
	("conditional", POS_MOOD_CONDITIONAL)
	("subjonctive", POS_MOOD_SUBJONCTIVE)
	("imperative", POS_MOOD_IMPERATIVE)
	("participle", POS_MOOD_PARTICIPLE)
	("gerund", POS_MOOD_GERUND);


      std::map<Tense, std::string> POS::tenseToString = boost::assign::map_list_of
	(POS_TENSE_UNKNOWN, "unknown")
	(POS_TENSE_PRESENT, "present")
	(POS_TENSE_IMPERFECT, "imperfect")
	(POS_TENSE_PAST, "past")
	(POS_TENSE_FUTURE, "future");
      std::map<Tense, char> POS::tenseToChar = boost::assign::map_list_of
	(POS_TENSE_UNKNOWN, 'u')
	(POS_TENSE_PRESENT, 'p')
	(POS_TENSE_IMPERFECT, 'i')
	(POS_TENSE_PAST, 's')
	(POS_TENSE_FUTURE, 'f');
      std::map<std::string, Tense> POS::stringToTense = boost::assign::map_list_of
	("unknown", POS_TENSE_UNKNOWN)
	("present", POS_TENSE_PRESENT)
	("imperfect", POS_TENSE_IMPERFECT)
	("past", POS_TENSE_PAST)
	("future", POS_TENSE_FUTURE);

      std::map<Aspect, std::string> POS::aspectToString = boost::assign::map_list_of
	(POS_ASPECT_UNKNOWN, "unknown")
	(POS_ASPECT_FINITE, "finite")
	(POS_ASPECT_NON_FINITE, "nonfinite");
      std::map<Aspect, char> POS::aspectToChar = boost::assign::map_list_of
	(POS_ASPECT_UNKNOWN, 'u')
	(POS_ASPECT_FINITE, 'f')
	(POS_ASPECT_NON_FINITE, 'n');
      std::map<std::string, Aspect> POS::stringToAspect = boost::assign::map_list_of
	("unknown", POS_ASPECT_UNKNOWN)
	("finite", POS_ASPECT_FINITE)
	("nonfinite", POS_ASPECT_NON_FINITE);

      std::map<Person, std::string> POS::personToString = boost::assign::map_list_of
	(POS_PERSON_UNKNOWN, "unknown")
	(POS_PERSON_FIRST, "1")
	(POS_PERSON_SECOND, "2")
	(POS_PERSON_THIRD, "3");
      std::map<Person, char> POS::personToChar = boost::assign::map_list_of
	(POS_PERSON_UNKNOWN, 'u')
	(POS_PERSON_FIRST, '1')
	(POS_PERSON_SECOND, '2')
	(POS_PERSON_THIRD, '3');
      std::map<std::string, Person> POS::stringToPerson = boost::assign::map_list_of
	("unknown", POS_PERSON_UNKNOWN)
	("1", POS_PERSON_FIRST)
	("2", POS_PERSON_SECOND)
	("3", POS_PERSON_THIRD);

      char POS::whToChar = 'w';
      std::string POS::whToString = "wh";

      POS::POS(const bool noInit) : BaseItem(noInit)
      {
	type = pos::POS_UNKNOWN;
      }


      POS::POS(const POS &pos) : BaseItem(pos)
      {
	set_type(pos.get_type());
      }

      POS::~POS()
      {
      }

      POS *POS::clone() const
      {
	return new POS(*this);
      }

      POS& POS::operator=(const POS &pos)
      {
	BaseItem::operator=(pos);
	set_type(pos.get_type());
	set_label(pos.get_label());

	return *this;
      }

      void POS::deflate(RootsStream * stream, bool is_terminal, int list_index)
      {
	BaseItem::deflate(stream,false,list_index);

	if(is_terminal)
	  stream->close_object();
      }

      std::string POS::to_string(int level) const
      {
	std::stringstream ss;

	switch (level)
	  {
	  case LEVEL_LABEL:
	  case LEVEL_CLASS_FINE:
	  case LEVEL_CLASS_COARSE:
	  case LEVEL_CLASS_TREEBANK_FR:
	  default:
	    ss	<<  "";
	    break;
	  }
	return std::string(ss.str().c_str());
      }

      void POS::inflate(RootsStream * stream, bool is_terminal, int list_index)
      {
	BaseItem::inflate(stream,false,list_index);
	if(is_terminal) stream->close_children();	
      }

      POS * POS::inflate_object(RootsStream * stream, int list_index)
      {
	POS *t = new POS(true);
	t->inflate(stream,true,list_index);
	return t;
      }


    } } }
