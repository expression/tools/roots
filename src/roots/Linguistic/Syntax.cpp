/*
    This file is part of Roots.

    Roots is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Roots is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Roots.  If not, see <http://www.gnu.org/licenses/>.

    Copyright (c) 2013, 2014 IRISA
*/

#include "Syntax.h"

namespace roots {

namespace linguistic {
namespace syntax {

Syntax::Syntax(const bool noInit) : roots::tree::Tree<SyntaxNode>(noInit) {
  _wordSequence = NULL;
}

Syntax::Syntax(const Syntax &syntax) : roots::tree::Tree<SyntaxNode>(syntax) {
  _wordSequence = syntax.get_word_sequence();
}

Syntax::~Syntax() {
  // Assume wordSequence is destroyed elsewhere after.
}

Syntax *Syntax::clone() const { return new Syntax(*this); }

Syntax &Syntax::operator=(const Syntax &node) {
  roots::tree::Tree<SyntaxNode>::operator=(node);
  
  _wordSequence = node.get_word_sequence();

  return *this;
}

const std::string Syntax::get_word_sequence_id() const {
  if (_wordSequence != NULL) {
    return _wordSequence->get_id();
  } else {
    return "";
  }
};

std::vector<int> Syntax::get_word_index_array() {
  // Traverse the tree to get indexes of phonemes
  std::vector<int> wordIndexArray = std::vector<int>();

  std::vector<SyntaxNode *> leaves = this->get_leaf_nodes();

  for (std::vector<SyntaxNode *>::const_iterator it = leaves.begin();
       it != leaves.end(); ++it) {
    const std::vector<int> &wIndexArray = (*it)->get_word_index_array();
    wordIndexArray.insert(wordIndexArray.end(), wIndexArray.begin(),
                          wIndexArray.end());
  }

  /*
int leafIndex = 0;
int nLeaves = leaves.size();
while(leafIndex < nLeaves)
{
const std::vector<int>& wIndexArray = leaves[leafIndex]->get_word_index_array();
wordIndexArray.insert(wordIndexArray.end(), wIndexArray.begin(),
wIndexArray.end());
++leafIndex;
}
  */
  return wordIndexArray;
}

int Syntax::get_first_target_index() const {
  SyntaxNode *leftLeaf = static_cast<SyntaxNode *>(rootNode->get_first_leaf());
  if (leftLeaf == NULL) {
    return -1;
  }
  return leftLeaf->get_start();
}

int Syntax::get_last_target_index() const {
  SyntaxNode *rightLeaf = static_cast<SyntaxNode *>(rootNode->get_last_leaf());
  if (rightLeaf == NULL) {
    return -1;
  }
  return rightLeaf->get_end();
}

void Syntax::translate_index(int offset) {
  std::vector<SyntaxNode *> leaves = this->get_nodes();
  for (std::vector<SyntaxNode *>::iterator it = leaves.begin();
       it != leaves.end(); ++it) {
    (*it)->translate_index(offset);
  }
}

int Syntax::get_word_depth(int wordIndex) const {
  std::vector<SyntaxNode *> vec;
  vec = this->get_syntax_node_path_to_word(wordIndex);

  return vec.size() - 1;
}

SyntaxNode *Syntax::get_word_syntax_node(int wordIndex) const {
  std::vector<SyntaxNode *> leaves;

  if (wordIndex < 0) {
    std::stringstream ss;
    ss << "word index out of bounds!";
    throw RootsException(__FILE__, __LINE__, ss.str().c_str());
  }

  leaves = this->get_leaf_nodes();

  int leafIndex = 0;
  int nLeaves = leaves.size();

  bool found = false;
  while (!found && (leafIndex < nLeaves)) {
    // cout << "start="<<(*iter).get_start()<<", wordIndex="<<wordIndex<<",
    // end="<<(*iter).get_end()<<std::endl;
    if ((leaves[leafIndex]->get_start() <= wordIndex) &&
        (wordIndex <= leaves[leafIndex]->get_end())) {
      found = true;
    } else {
      ++leafIndex;
    }
  }

  if (!found) {
    // std::stringstream ss;
    // ss << "word index not found!";
    // throw RootsException(__FILE__, __LINE__, ss.str().c_str());
    return NULL;
  }

  return leaves[leafIndex];
}

std::vector<SyntaxNode *> Syntax::get_syntax_node_path_to_word(
    int wordIndex) const {
  std::vector<SyntaxNode *> vec, vecout;

  SyntaxNode *node = get_word_syntax_node(wordIndex);
  if (node != NULL) {
    SyntaxNode *parent = node;

    while (parent != NULL) {
      vec.push_back(static_cast<SyntaxNode *>(parent));
      parent = static_cast<SyntaxNode *>(node->get_parent());
    }
  }
  // reverse the vector
  vecout.insert(vecout.begin(), vec.rbegin(), vec.rend());

  return vecout;
}

std::string Syntax::to_string(int level) const {
  std::stringstream ss;

  switch (level) {
    case 0:
    default:
      ss << "digraph Syntax {\n";
      ss << "ordering=out\n";
      ss << "node [shape=none]\n";
      ss << rootNode->output_dot();
      ss << "}\n";
      break;
  }
  return std::string(ss.str().c_str());
}

void Syntax::deflate(RootsStream *stream, bool is_terminal, int list_index) {
  roots::tree::Tree<SyntaxNode>::deflate(stream, false, list_index);

  stream->append_unicodestring_content("word_sequence_id",
                                       this->get_word_sequence_id());

  if (is_terminal) stream->close_object();
}

std::ostream &operator<<(std::ostream &out, Syntax &syntax) {
  out << syntax.to_string() << std::endl;
  return out;
}

void Syntax::inflate(RootsStream *stream, bool is_terminal, int list_index) {
  roots::tree::Tree<SyntaxNode>::inflate(stream, false, list_index);

  // stream->open_children();
  std::string id = stream->get_unicodestring_content("word_sequence_id");
  roots::sequence::WordSequence *wordSeq =
      (roots::sequence::WordSequence *)stream->get_object_ptr(id);
  if (wordSeq == NULL) {
    std::stringstream ss;
    ss << "cannot find sequence with id=" << id
       << " while inflating Syntax object!";
    std::cerr << ss.str().c_str() << std::endl;
    throw RootsException(__FILE__, __LINE__, ss.str().c_str());
  }
  this->set_word_sequence(wordSeq);
  if (is_terminal) stream->close_children();
}

Syntax *Syntax::inflate_object(RootsStream *stream, int list_index) {
  Syntax *t = new Syntax(true);
  t->inflate(stream, true, list_index);
  return t;
}
}
}
}  // END OF NAMESPACES
