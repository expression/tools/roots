/*
    This file is part of Roots.

    Roots is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Roots is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Roots.  If not, see <http://www.gnu.org/licenses/>.

    Copyright (c) 2013, 2014 IRISA
*/


#include "NamedEntity.h"

namespace roots {
  namespace linguistic {
    namespace namedentity {


        std::map<namedentity::Category, std::string>		NamedEntity::categoryToString = boost::assign::map_list_of
	(namedentity::CATEGORY_UNKNOWN, "unknown")
	(namedentity::CATEGORY_TIME, "time")
	(namedentity::CATEGORY_TIME_RELATIVE, "time_relative")
	(namedentity::CATEGORY_PERSON, "person")
	(namedentity::CATEGORY_PERIOD, "period")
	(namedentity::CATEGORY_ORGANIZATION, "organization")
	(namedentity::CATEGORY_NUMBER, "number")
	(namedentity::CATEGORY_NUMBER_MONEY, "number_money")
	(namedentity::CATEGORY_LOCATION, "location")
	(namedentity::CATEGORY_PERIOD_TIME, "period_time")
	(namedentity::CATEGORY_PERIOD_OTHER, "period_other")
	(namedentity::CATEGORY_ORGANIZATION_OTHER, "organization_other")
	(namedentity::CATEGORY_ORGANIZATION_ETHNIC_GROUP, "ethnic_group")
	(namedentity::CATEGORY_ORGANIZATION_ETHNIC_GROUP_OTHER, "ethnic_group_other")
	(namedentity::CATEGORY_ORGANIZATION_ETHNIC_GROUP_NATIONALITY, "ethnic_group_nationality")
	(namedentity::CATEGORY_LOCATION_ADDRESS, "address")
	(namedentity::CATEGORY_LOCATION_ADDRESS_OTHER, "address_other");

        std::map<std::string, namedentity::Category>		NamedEntity::stringToCategory = boost::assign::map_list_of
	("unknown", namedentity::CATEGORY_UNKNOWN)
	("time", namedentity::CATEGORY_TIME)
	("time_relative", namedentity::CATEGORY_TIME_RELATIVE)
	("person", namedentity::CATEGORY_PERSON)
	("period", namedentity::CATEGORY_PERIOD)
	("organization", namedentity::CATEGORY_ORGANIZATION)
	("number", namedentity::CATEGORY_NUMBER)
	("number_money", namedentity::CATEGORY_NUMBER_MONEY)
	("location", namedentity::CATEGORY_LOCATION)
	("period_time", namedentity::CATEGORY_PERIOD_TIME)
	("period_other", namedentity::CATEGORY_PERIOD_OTHER)
	("organization_other", namedentity::CATEGORY_ORGANIZATION_OTHER)
	("ethnic_group", namedentity::CATEGORY_ORGANIZATION_ETHNIC_GROUP)
	("ethnic_group_other", namedentity::CATEGORY_ORGANIZATION_ETHNIC_GROUP_OTHER)
	("ethnic_group_nationality", namedentity::CATEGORY_ORGANIZATION_ETHNIC_GROUP_NATIONALITY)
	("address", namedentity::CATEGORY_LOCATION_ADDRESS)
	("address_other", namedentity::CATEGORY_LOCATION_ADDRESS_OTHER);


      NamedEntity::NamedEntity(const bool noInit) : BaseItem(noInit)
      {
	category = namedentity::CATEGORY_UNKNOWN;//set_category(namedentity::CATEGORY_UNKNOWN);
      }

      NamedEntity::NamedEntity(namedentity::Category cat) // throw(RootsException)
      {
	set_category(cat);
      }

	  NamedEntity::NamedEntity(std::string cat) // throw(RootsException)
      {
	set_category(cat);
      }

      NamedEntity::NamedEntity(const NamedEntity& ne) : BaseItem(ne)
      {
	category = ne.get_category();
      }

      NamedEntity::~NamedEntity()
      {

      }

      NamedEntity *NamedEntity::clone() const
      {
	return new NamedEntity(*this);
      }

      NamedEntity& NamedEntity::operator= (const NamedEntity& ne)
      {
	BaseItem::operator=(ne);

	set_category(ne.get_category());

	return *this;

      }

		void NamedEntity::set_category(namedentity::Category cat) // throw(RootsException)
      {
	if(cat != namedentity::CATEGORY_UNKNOWN)
	  {
	    std::stringstream ss;
	    ss << "category " << cat << " incorrect for " << this->get_classname() <<" object!";
	    throw RootsException(__FILE__, __LINE__, ss.str().c_str());
	  }
	category = namedentity::CATEGORY_UNKNOWN;
      }

		void NamedEntity::set_category(std::string cat) // throw(RootsException)
      {
	if(stringToCategory[cat] != namedentity::CATEGORY_UNKNOWN)
	  {
	    std::stringstream ss;
	    ss << "category " << cat << " incorrect for " << this->get_classname() <<" object!";
	    throw RootsException(__FILE__, __LINE__, ss.str().c_str());
	  }
	category = namedentity::CATEGORY_UNKNOWN;
      }

		std::string NamedEntity::get_attribute(std::string key) // throw(RootsException)
      {
	if(attributes.find(key) == attributes.end())
	  {
	    std::stringstream ss;
	    ss << "attribute " << key << " incorrect for " << this->get_classname() <<" object!";
	    throw RootsException(__FILE__, __LINE__, ss.str().c_str());
	  }

	return attributes[key];
      }

	  void NamedEntity::set_attribute(std::string key, std::string value __attribute__((unused))) // throw(RootsException)
      {
	std::stringstream ss;
	ss << "attribute " << key << " incorrect for " << this->get_classname() <<" object!";
	throw RootsException(__FILE__, __LINE__, ss.str().c_str());
      }

      std::string NamedEntity::to_string(int level) const
      {
	std::stringstream ss;

	switch (level)
	  {
	  case 0:
	  default:
	    ss	<<  categoryToString[get_category()];
	    break;
	  }
	return std::string(ss.str().c_str());
      }

      void NamedEntity::deflate(RootsStream * stream, bool is_terminal, int list_index)
      {
	BaseItem::deflate(stream,false,list_index);

	stream->append_uint_content("category", this->get_category());//categoryToString[this->get_category()]);
	stream->append_map_unistr_unistr_content("attribute", attributes);

	if(is_terminal)
	  stream->close_object();
      }

      void NamedEntity::inflate(RootsStream * stream, bool is_terminal, int list_index)
      {
	BaseItem::inflate(stream,false,list_index);

	//stream->open_children();
	this->set_category((Category)stream->get_uint_content("category"));
	this->attributes = stream->get_map_unistr_unistr_content("attribute");
	if(is_terminal) stream->close_children();
      }

      NamedEntity * NamedEntity::inflate_object(RootsStream * stream, int list_index)
      {
	NamedEntity *t = new NamedEntity(true);
	t->inflate(stream,true,list_index);
	return t;
      }
  
    }
  }
 }
