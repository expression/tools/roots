/*
    This file is part of Roots.

    Roots is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Roots is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Roots.  If not, see <http://www.gnu.org/licenses/>.

    Copyright (c) 2013, 2014 IRISA
*/


#include "Organization.h"

namespace roots
{

namespace linguistic
{

namespace namedentity
{


Organization::Organization()
{
	set_category(CATEGORY_ORGANIZATION);
}

Organization::Organization(const Organization& ne) : NamedEntity(ne)
{
	set_category(ne.get_category());
}

Organization::~Organization()
{

}

Organization *Organization::clone() const
{
	return new Organization(*this);
}

Organization& Organization::operator= (const Organization& ne)
{
	NamedEntity::operator=(ne);
	set_category(ne.get_category());

	return *this;

}

void Organization::set_category(Category cat) // throw(RootsException)
{
	if(cat != CATEGORY_ORGANIZATION)
	{
		std::stringstream ss;
		ss << "category " << cat << " incorrect for " << this->get_classname() <<" object!";
		throw RootsException(__FILE__, __LINE__, ss.str().c_str());
	}
	category = cat;
}

void Organization::set_category(std::string cat) // throw(RootsException)
{
	if(stringToCategory[cat] != CATEGORY_ORGANIZATION)
	{
		std::stringstream ss;
		ss << "category " << cat << " incorrect for " << this->get_classname() <<" object!";
		throw RootsException(__FILE__, __LINE__, ss.str().c_str());
	}
	category = stringToCategory[cat];
}

Organization * Organization::inflate_object(RootsStream * stream, int list_index)
{
	Organization *t = new Organization();
	t->inflate(stream,true,list_index);
	return t;	
}

} } }
