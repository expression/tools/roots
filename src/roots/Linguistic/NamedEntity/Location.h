/*
    This file is part of Roots.

    Roots is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Roots is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Roots.  If not, see <http://www.gnu.org/licenses/>.

    Copyright (c) 2013, 2014 IRISA
*/


#ifndef LOCATION_H_
#define LOCATION_H_

#include "../NamedEntity.h"

namespace roots
{

namespace linguistic
{

namespace namedentity
{
/**
 *  @brief This class represents the named entity Location
 *  @details This named entity has no attribute.  The applicable category is
 *  CATEGORY_LOCATION.
 *  @author Cordial Group
 *  @version 0.1
 *  @date 2011
 */
class Location: public roots::linguistic::namedentity::NamedEntity
{
public:
	/**
	 * @brief Default constructor
	 */
	Location();
public:
	/**
	 * @brief Copy Constructor
	 * @param ne a named entity
	 */
	Location(const Location &ne);
	/**
	 * @brief Destructor
	 */
	virtual ~Location();
	/**
	 * @brief clone the current object
	 * @return a copy of the object
	 */
	virtual Location *clone() const;
	/**
	 * @brief Copy NamedEntity features into the current named entity
	 * @param ne
	 * @return reference to the current named entity
	 */
	Location& operator= (const Location &ne);
	/**
	 * @brief Sets the category of a named entity
	 * @details This method is only applicable to some named entities
	 * @throw RootsException
	 * @param cat the category
	 */
	virtual void set_category(Category cat); // throw(RootsException);
	/**
	 * @brief Sets the category of a named entity
	 * @details This method is only applicable to some named entities
	 * @throw RootsException
	 * @param cat the category
	 */
	virtual void set_category(std::string cat); // throw(RootsException);
	/**
	 * @brief Extracts the entity from the stream
	 * @param stream the stream from which we inflate the element
	 */
	static Location * inflate_object(RootsStream * stream, int list_index=-1);

	/**
	 * @brief returns classname for current object
	 * @return string constant representing the classname
	 */
	virtual std::string get_classname() const { return classname(); };
	/**
	 * @brief returns classname for current class
	 * @return string constant representing the classname
	 */
	static std::string classname() { return "Linguistic::NamedEntity::Location"; };

protected:
};

}

}

}

#endif /* LOCATION_H_ */
