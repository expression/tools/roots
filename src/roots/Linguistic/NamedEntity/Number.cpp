/*
    This file is part of Roots.

    Roots is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Roots is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Roots.  If not, see <http://www.gnu.org/licenses/>.

    Copyright (c) 2013, 2014 IRISA
*/


#include "Number.h"

namespace roots
{

namespace linguistic
{

namespace namedentity
{

Number::Number()
{
	set_category(CATEGORY_NUMBER);
}

Number::Number(const Number& ne) : NamedEntity(ne)
{
	set_category(ne.get_category());
}

Number::~Number()
{

}

Number *Number::clone() const
{
	return new Number(*this);
}

Number& Number::operator= (const Number& ne)
{
	NamedEntity::operator=(ne);
	set_category(ne.get_category());

	return *this;

}

void Number::set_category(Category cat) // throw(RootsException)
{
	if(cat != CATEGORY_NUMBER && cat != CATEGORY_NUMBER_MONEY)
	{
		std::stringstream ss;
		ss << "category " << cat << " incorrect for " << this->get_classname() <<" object!";
		throw RootsException(__FILE__, __LINE__, ss.str().c_str());
	}
	category = cat;
}

void Number::set_category(std::string cat) // throw(RootsException)
{
	if(stringToCategory[cat] != CATEGORY_NUMBER && stringToCategory[cat] != CATEGORY_NUMBER_MONEY)
	{
		std::stringstream ss;
		ss << "category " << cat << " incorrect for " << this->get_classname() <<" object!";
		throw RootsException(__FILE__, __LINE__, ss.str().c_str());
	}
	category = stringToCategory[cat];
}


Number * Number::inflate_object(RootsStream * stream, int list_index)
{
	Number *t = new Number();
	t->inflate(stream,true,list_index);
	return t;	
}

}

}

}
