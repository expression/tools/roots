/*
    This file is part of Roots.

    Roots is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Roots is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Roots.  If not, see <http://www.gnu.org/licenses/>.

    Copyright (c) 2013, 2014 IRISA
*/


#include "Other.h"

namespace roots
{

namespace linguistic
{

namespace namedentity
{

namespace organization
{

Other::Other()
{
  set_category(CATEGORY_ORGANIZATION_OTHER);
}

Other::Other(const Other& ne) : Organization(ne)
{
	set_category(ne.get_category());
}

Other::~Other()
{

}


Other& Other::operator= (const Other& ne)
{
	NamedEntity::operator=(ne);
	set_category(ne.get_category());

	return *this;

}

Other *Other::clone() const
{
	return new Other(*this);
}

void Other::set_category(Category cat) // throw(RootsException)
{
	if(cat != CATEGORY_ORGANIZATION_OTHER)
	{
		std::stringstream ss;
		ss << "category " << cat << " incorrect for " << this->get_classname() <<" object!";
		throw RootsException(__FILE__, __LINE__, ss.str().c_str());
	}
	category = cat;
}

void Other::set_category(std::string cat) // throw(RootsException)
{
	if(stringToCategory[cat] != CATEGORY_ORGANIZATION_OTHER)
	{
		std::stringstream ss;
		ss << "category " << cat << " incorrect for " << this->get_classname() <<" object!";
		throw RootsException(__FILE__, __LINE__, ss.str().c_str());
	}
	category = stringToCategory[cat];
}

Other * Other::inflate_object(RootsStream * stream, int list_index)
{
	Other *t = new Other();
	t->inflate(stream,true,list_index);
	return t;	
}

} } } }
