/*
    This file is part of Roots.

    Roots is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Roots is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Roots.  If not, see <http://www.gnu.org/licenses/>.

    Copyright (c) 2013, 2014 IRISA
*/


#include "EthnicGroup.h"

namespace roots
{

namespace linguistic
{

namespace namedentity
{

namespace organization
{

EthnicGroup::EthnicGroup()
{
	set_category(CATEGORY_ORGANIZATION_ETHNIC_GROUP);
}

EthnicGroup::EthnicGroup(const EthnicGroup& ne) : Organization(ne)
{
	set_category(ne.get_category());
}

EthnicGroup::~EthnicGroup()
{

}


EthnicGroup& EthnicGroup::operator= (const EthnicGroup& ne)
{
	NamedEntity::operator=(ne);
	set_category(ne.get_category());

	return *this;

}

EthnicGroup *EthnicGroup::clone() const
{
	return new EthnicGroup(*this);
}

void EthnicGroup::set_category(Category cat) // throw(RootsException)
{
	if(cat != CATEGORY_ORGANIZATION_ETHNIC_GROUP &&
			cat != CATEGORY_ORGANIZATION_ETHNIC_GROUP_OTHER &&
			cat != CATEGORY_ORGANIZATION_ETHNIC_GROUP_NATIONALITY)
	{
		std::stringstream ss;
		ss << "category " << cat << " incorrect for " << this->get_classname() <<" object!";
		throw RootsException(__FILE__, __LINE__, ss.str().c_str());
	}
	category = cat;
}

void EthnicGroup::set_category(std::string cat) // throw(RootsException)
{
	if(stringToCategory[cat] != CATEGORY_ORGANIZATION_ETHNIC_GROUP &&
			stringToCategory[cat] != CATEGORY_ORGANIZATION_ETHNIC_GROUP_OTHER &&
			stringToCategory[cat] != CATEGORY_ORGANIZATION_ETHNIC_GROUP_NATIONALITY)
	{
		std::stringstream ss;
		ss << "category " << cat << " incorrect for " << this->get_classname() <<" object!";
		throw RootsException(__FILE__, __LINE__, ss.str().c_str());
	}
	category = stringToCategory[cat];
}

void EthnicGroup::set_attribute(std::string key, std::string value) // throw(RootsException)
{
	if(get_category() == CATEGORY_ORGANIZATION_ETHNIC_GROUP_NATIONALITY
			&& key == "nationality")
	{
		attributes["nationality"] = value;
	}else{
		std::stringstream ss;
		ss << "attribute " << key << " incorrect for " << this->get_classname() <<" object!";
		throw RootsException(__FILE__, __LINE__, ss.str().c_str());
	}
}

EthnicGroup * EthnicGroup::inflate_object(RootsStream * stream, int list_index)
{
	EthnicGroup *t = new EthnicGroup();
	t->inflate(stream,true,list_index);
	return t;	
}

} } } }
