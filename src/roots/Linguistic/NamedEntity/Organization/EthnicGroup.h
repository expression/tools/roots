/*
    This file is part of Roots.

    Roots is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Roots is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Roots.  If not, see <http://www.gnu.org/licenses/>.

    Copyright (c) 2013, 2014 IRISA
*/


#ifndef ETHNICGROUP_H_
#define ETHNICGROUP_H_

#include "../Organization.h"

namespace roots
{

namespace linguistic
{

namespace namedentity
{

namespace organization
{

/**
 *  @brief This class represents the named entity EthnicGroup
 *  @details This named entity has one attribute for the sub category
 *  CATEGORY_ORGANIZATION_ETHNIC_GROUP_NATIONALITY which is "nationality". The
 *  applicable categories are:
 *  - CATEGORY_ORGANISATION_ETHNIC_GROUP
 *  - CATEGORY_ORGANISATION_ETHNIC_GROUP_OTHER
 *  - CATEGORY_ORGANISATION_ETHNIC_GROUP_NATIONALITY.
 *  @author Cordial Group
 *  @version 0.1
 *  @date 2011
 */
class EthnicGroup: public roots::linguistic::namedentity::Organization
{
public:
	/**
	 * @brief Default constructor
	 */
	EthnicGroup();
public:
	/**
	 * @brief Copy Constructor
	 * @param ne a named entity
	 */
	EthnicGroup(const EthnicGroup &ne);
	/**
	 * @brief Destructor
	 */
	virtual ~EthnicGroup();
	/**
	 * @brief Copy NamedEntity features into the current named entity
	 * @param ne
	 * @return reference to the current named entity
	 */
	EthnicGroup& operator= (const EthnicGroup &ne);
	/**
	 * @brief Duplicates the named entity
	 * @return A clone of the named entity
	 */
	virtual EthnicGroup *clone() const;
	/**
	 * @brief Sets the category of a named entity
	 * @details This method is only applicable to some named entities
	 * @throw RootsException
	 * @param cat the category
	 */
	virtual void set_category(Category cat); // throw(RootsException);
	/**
	 * @brief Sets the category of a named entity
	 * @details This method is only applicable to some named entities
	 * @throw RootsException
	 * @param cat the category
	 */
	virtual void set_category(std::string cat); // throw(RootsException);
	/**
	 * @brief Sets an attribute of the named entity
	 * @details This method is applicable only to some named entities. If the attribute
	 * is not authorized, this methods throws an exception. This method is only applicable to some named entities. This
	 * method has to be overridden in subclasses. This named entity has one attribute for the sub category
	 * CATEGORY_ORGANIZATION_ETHNIC_GROUP_NATIONALITY which is "nationality".
	 * @param key name of the attribute
	 * @param value value of the attribute
	 * @throw RootsException exception thrown if the attribute is forbidden
	 */
	virtual void set_attribute(std::string key, std::string value); // throw(RootsException);
	/**
	 * @brief Extracts the entity from the stream
	 * @param stream the stream from which we inflate the element
	 */
	static EthnicGroup * inflate_object(RootsStream * stream, int list_index=-1);

	/**
	 * @brief returns classname for current object
	 * @return string constant representing the classname
	 */
	virtual std::string get_classname() const { return classname(); };
	/**
	 * @brief returns classname for current class
	 * @return string constant representing the classname
	 */
	static std::string classname() { return "Linguistic::NamedEntity::Organization::EthnicGroup"; };

protected:
};

} } } }

#endif /* ETHNICGROUP_H_ */
