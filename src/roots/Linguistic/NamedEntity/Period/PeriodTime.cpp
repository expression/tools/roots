/*
    This file is part of Roots.

    Roots is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Roots is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Roots.  If not, see <http://www.gnu.org/licenses/>.

    Copyright (c) 2013, 2014 IRISA
*/


#include "PeriodTime.h"

namespace roots
{

namespace linguistic
{

namespace namedentity
{

namespace period
{



PeriodTime::PeriodTime()
{
	set_category(CATEGORY_PERIOD_TIME);
}

PeriodTime::PeriodTime(const PeriodTime& ne) : Period(ne)
{
	set_category(ne.get_category());
}

PeriodTime::~PeriodTime()
{

}


PeriodTime& PeriodTime::operator= (const PeriodTime& ne)
{
	NamedEntity::operator=(ne);
	set_category(ne.get_category());

	return *this;

}

PeriodTime *PeriodTime::clone() const
{
	return new PeriodTime(*this);
}

void PeriodTime::set_category(Category cat) // throw(RootsException)
{
	if(cat != CATEGORY_PERIOD_TIME && cat != CATEGORY_PERIOD_OTHER)
	{
		std::stringstream ss;
		ss << "category " << cat << " incorrect for " << this->get_classname() <<" object!";
		throw RootsException(__FILE__, __LINE__, ss.str().c_str());
	}
	category = cat;
}

void PeriodTime::set_category(std::string cat) // throw(RootsException)
{
	if(stringToCategory[cat] != CATEGORY_PERIOD_TIME && stringToCategory[cat] != CATEGORY_PERIOD_OTHER)
	{
		std::stringstream ss;
		ss << "category " << cat << " incorrect for " << this->get_classname() <<" object!";
		throw RootsException(__FILE__, __LINE__, ss.str().c_str());
	}
	category = stringToCategory[cat];
}

PeriodTime * PeriodTime::inflate_object(RootsStream * stream, int list_index)
{
	PeriodTime *t = new PeriodTime();
	t->inflate(stream,true,list_index);
	return t;	
}

} } } }
