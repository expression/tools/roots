/*
    This file is part of Roots.

    Roots is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Roots is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Roots.  If not, see <http://www.gnu.org/licenses/>.

    Copyright (c) 2013, 2014 IRISA
*/


#ifndef FILLERNSA_H_
#define FILLERNSA_H_

#include "../Filler.h"
#include "../../Phonology/Nsa.h"
#include "../../Phonology/Nsa/NsaCommon.h"


namespace roots
{

namespace linguistic
{

namespace filler
{

/**
 * @brief This class represents a filler non speech event
 * @details
 * @author Cordial Group
 * @version 0.1
 * @date 2011
 */
class Nsa : public Filler
{
protected:
	/**
	 * @brief Default constructor
	 */
	Nsa(const bool noInit = false);
public:
	//Nsa(const std::string &tag);
	/**
	 * @brief Constructor
	 * @details builds a filler::Nsa from a phonology::Nsa
	 * @param aNsa
	 */
	Nsa(const roots::phonology::nsa::Nsa &aNsa);
	/**
	 * @brief Copy constructor
	 * @param aNsa
	 */
	Nsa(const Nsa &aNsa);
	/**
	 * @brief Destructor
	 */
	virtual ~Nsa();
	/**
	 * @brief clone the current object
	 * @return a copy of the object
	 */
	virtual Nsa *clone() const;
	/**
	 * @brief Copy filler features into the current filler
	 * @param nsa
	 * @return reference to the current filler
	 */
	Nsa& operator= (const Nsa &nsa);
	/**
	 * @brief Sets the current phonology::Nsa associated to the filler
	 * @param aNsa pointer to the phonology::Nsa
	 */
	void set_nsa(const roots::phonology::nsa::Nsa &aNsa);
	/**
	 * @brief Returns the phonology::Nsa associated to the filler
	 * @return the nsa
	 */
	roots::phonology::nsa::Nsa &get_nsa() const {return *nsa;};
	/**
	 * @brief Return false
	 * @return false
	 */
	virtual bool is_speech() {return false;};
	/**
	 * @brief Return true
	 * @return true
	 */
	virtual bool is_non_speech() {return true;};

	/**
	 * Todo: get_label_from_alphabet
	 * Todo: get_alphabet
	 * Todo: from_label
	 */

	/**
	 * @brief Returns the label of the included nsa object
	 * @return the label
	 */
	virtual const std::string& get_label() const { return nsa->get_label();}
	/**
	 * @brief returns a string representation of any Roots object
	 * @param level the precision level of the output
	 * @return string representation of the object
	 */
	virtual std::string to_string(int level=0) const;

	/**
	 * @brief Write the entity into a RootsStream
	 * @param stream the stream from which we inflate the element
	 * @param is_terminal indicates that the node is terminal (true by default)
	 */
	virtual void deflate(RootsStream * stream, bool is_terminal=true, int list_index=-1);
	/**
	 * @brief Extracts the entity from the stream
	 * @param stream the stream from which we inflate the element
	 */
	virtual void inflate(RootsStream * stream, bool is_terminal=true, int list_index=-1);
	/**
	 * @brief Extracts the entity from the stream
	 * @param stream the stream from which we inflate the element
	 */
	static Nsa * inflate_object(RootsStream * stream, int list_index=-1);
	/**
	* @brief returns the XML tag name value for current object
	* @return string constant representing the XML tag name
	*/
	virtual std::string get_xml_tag_name() const { return xml_tag_name(); };
	/**
	* @brief returns the XML tag name value for current class
	* @return string constant representing the XML tag name
	*/
	static std::string xml_tag_name() { return "filler"; };
	/**
	* @brief returns classname for current object
	* @return string constant representing the classname
	*/
	virtual std::string get_classname() const { return classname(); };
	/**
	* @brief returns classname for current class
	* @return string constant representing the classname
	*/
	static std::string classname() { return "Linguistic::Filler::Nsa"; };
	/**
	* @brief returns display color for current object
	* @return string constant representing the pgf display color
	*/
	virtual std::string get_pgf_display_color() const { return pgf_display_color();};
	/**
	* @brief returns display color for current class
	* @return string constant representing the pgf display color
	*/
	static std::string pgf_display_color() { return "orange!25"; };

private:
	roots::phonology::nsa::Nsa *nsa; ///< the phonology::Nsa associated to the filler
};

} } }

#endif /* FILLERNSA_H_ */
