/*
    This file is part of Roots.

    Roots is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Roots is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Roots.  If not, see <http://www.gnu.org/licenses/>.

    Copyright (c) 2013, 2014 IRISA
*/


#include "Nsa.h"

namespace roots
{

namespace linguistic
{

namespace filler
{

/*Nsa::Nsa(const std::string &tag) :
{
	//TODO nsa(tag)
}*/

Nsa::Nsa(const bool noInit) : Filler(noInit)
{
	nsa = NULL;
}

Nsa::Nsa(const roots::phonology::nsa::Nsa &aNsa) : Filler()
{
	// TODO nsa(aNsa)
	nsa = aNsa.clone();
}

Nsa::Nsa(const Nsa& aNsa) : Filler(aNsa)
{
	nsa = NULL;
	this->set_nsa(aNsa.get_nsa());
}

Nsa::~Nsa()
{
	if(nsa != NULL) delete nsa;
}

Nsa *Nsa::clone() const
{
	return new Nsa(*this);
}

Nsa& Nsa::operator= (const Nsa& nsa)
{
	Filler::operator=(nsa);
	this->set_nsa(nsa.get_nsa());
	return *this;
}

void Nsa::set_nsa(const roots::phonology::nsa::Nsa &aNsa)
{
	if(nsa != NULL) delete nsa;
	nsa = aNsa.clone();
}

std::string Nsa::to_string(int level) const
{
	return nsa->to_string(level);
}

void Nsa::deflate(RootsStream * stream, bool is_terminal, int list_index)
{
  Filler::deflate(stream,false,list_index);
  
  nsa->deflate(stream);

  if(is_terminal)
	stream->close_object();
}

void Nsa::inflate(RootsStream * stream, bool is_terminal, int list_index)
{

	Filler::inflate(stream,false,list_index);

	//stream->open_children();
	std::string classname = stream->get_object_classname_no_read(phonology::nsa::Nsa::xml_tag_name());

	if(classname.compare(phonology::nsa::Nsa::classname()) == 0)
	{
		phonology::nsa::Nsa *nsa = phonology::nsa::Nsa::inflate_object(stream);
		set_nsa(*nsa);
		delete nsa;
	} else if(classname.compare(phonology::nsa::NsaCommon::classname()) == 0)
	{
		phonology::nsa::Nsa *nsa = phonology::nsa::NsaCommon::inflate_object(stream);
		set_nsa(*nsa);
		delete nsa;
	} else {
        std::stringstream ss;
		ss << classname << " element type is unknown!";
		throw RootsException(__FILE__, __LINE__, ss.str().c_str());
	}

	if(is_terminal) stream->close_children();
}

Nsa * Nsa::inflate_object(RootsStream * stream, int list_index)
{
	Nsa *t = new Nsa(true);
	t->inflate(stream,true,list_index);
	return t;	
}


} } }  // END OF NAMESPACE

