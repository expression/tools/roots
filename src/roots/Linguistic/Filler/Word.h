/*
    This file is part of Roots.

    Roots is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Roots is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Roots.  If not, see <http://www.gnu.org/licenses/>.

    Copyright (c) 2013, 2014 IRISA
*/


#ifndef FILLERWORD_H_
#define FILLERWORD_H_

#include "../Filler.h"
#include "../Word.h"

namespace roots
{
  namespace linguistic
  {
    namespace filler
    {

      /**
       * @brief This class represents a filler word
       * @details
       * @author Cordial Group
       * @version 0.1
       * @date 2011
       */
      class Word: public Filler
      {
      protected:
	/**
	 * @brief Default constructor
	 */
	Word(const bool noInit= false);
      public:
	/**
	 * @brief Constructor
	 * @details builds a filler::Word from a linguistic::Word
	 * @param aWord
	 */
	Word(roots::linguistic::Word &aWord);
	/**
	 * @brief Copy constructor
	 * @param aWord
	 */
	Word(const Word &aWord);
	/**
	 * @brief Destructor
	 */
	virtual ~Word();
	/**
	 * @brief clone the current object
	 * @return a copy of the object
	 */
	virtual Word *clone() const;
	/**
	 * @brief Copy filler features into the current filler
	 * @param word
	 * @return reference to the current filler
	 */
	Word& operator= (const Word &word);
	/**
	 * @brief Sets the current linguistic::Word associated to the filler
	 * @param aWord pointer to the linguistic::Word
	 */
	void set_word(const roots::linguistic::Word &aWord);
	/**
	 * @brief Returns the linguistic::Word associated to the filler
	 * @return the word
	 */
	roots::linguistic::Word &get_word() const {return *word;};

	/**
	 * @brief Return true
	 * @return true
	 */
	virtual bool is_speech() {return true;};
	/**
	 * @brief Return false
	 * @return false
	 */
	virtual bool is_non_speech() {return false;};

	/**
	 * Todo: get_label_from_alphabet
	 * Todo: get_alphabet
	 * Todo: from_label
	 */
	/**
	 * @brief Returns the label of the included nsa object
	 * @return the label
	 */
	virtual const std::string& get_label() const { return word->get_label();}

	/**
	 * @brief returns a string representation of any Roots object
	 * @param level the precision level of the output
	 * @return string representation of the object
	 */
	virtual std::string to_string(int level=0) const;

	/**
	 * @brief Write the entity into a RootsStream
	 * @param stream the stream from which we inflate the element
	 * @param is_terminal indicates that the node is terminal (true by default)
	 */
	virtual void deflate(RootsStream * stream, bool is_terminal=true, int list_index=-1);
	/**
	 * @brief Extracts the entity from the stream
	 * @param stream the stream from which we inflate the element
	 */
	virtual void inflate(RootsStream * stream, bool is_terminal=true, int list_index=-1);
	/**
	 * @brief Extracts the entity from the stream
	 * @param stream the stream from which we inflate the element
	 */
	static Word * inflate_object(RootsStream * stream, int list_index=-1);
	/**
	 * @brief returns the XML tag name value for current object
	 * @return string constant representing the XML tag name
	 */
	virtual std::string get_xml_tag_name() const { return xml_tag_name(); };
	/**
	 * @brief returns the XML tag name value for current class
	 * @return string constant representing the XML tag name
	 */
	static std::string xml_tag_name() { return "filler"; };
	/**
	 * @brief returns classname for current object
	 * @return string constant representing the classname
	 */
	virtual std::string get_classname() const { return classname(); };
	/**
	 * @brief returns classname for current class
	 * @return string constant representing the classname
	 */
	static std::string classname() { return "Linguistic::Filler::Word"; };
	/**
	 * @brief returns display color for current object
	 * @return string constant representing the pgf display color
	 */
	virtual std::string get_pgf_display_color() const { return pgf_display_color();};
	/**
	 * @brief returns display color for current class
	 * @return string constant representing the pgf display color
	 */
	static std::string pgf_display_color() { return "orange!25"; };

      private:
	roots::linguistic::Word *word; ///< the linguistic::Word associated to the filler
      };

    }

  }

}

#endif /* FILLERWORD_H_ */
