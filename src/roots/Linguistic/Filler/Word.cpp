/*
    This file is part of Roots.

    Roots is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Roots is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Roots.  If not, see <http://www.gnu.org/licenses/>.

    Copyright (c) 2013, 2014 IRISA
*/


#include "Word.h"

namespace roots
{

  namespace linguistic
  {

    namespace filler
    {

      Word::Word(const bool noInit) : Filler(noInit)
      {
	word = NULL;
      }

      Word::Word(roots::linguistic::Word &aWord) : Filler()
      {
	word = aWord.clone();
      }

      Word::Word(const Word& aWord) : Filler(aWord)
      {
	word = NULL;
	this->set_word(aWord.get_word());
      }

      Word::~Word()
      {
	if(word != NULL) delete word;
      }

      Word *Word::clone() const
      {
	return new Word(*this);
      }

      Word& Word::operator= (const Word& aWord)
      {
	Filler::operator=(aWord);
	this->set_word(aWord.get_word());
	return *this;
      }

      void Word::set_word(const roots::linguistic::Word &aWord)
      {
	if(word != NULL) delete word;
	word = aWord.clone();
      }

      std::string Word::to_string(int level) const
      {
	return word->to_string(level);
      }

      void Word::deflate(RootsStream * stream, bool is_terminal, int list_index)
      {
	Filler::deflate(stream,false,list_index);

	word->deflate(stream);

	if(is_terminal)
	  stream->close_object();
      }

      void Word::inflate(RootsStream * stream, bool is_terminal, int list_index)
      {
	Filler::inflate(stream,false,list_index);

	std::string classname = stream->get_object_classname_no_read(linguistic::Word::xml_tag_name());

	//stream->open_children();

	if(classname.compare(linguistic::Word::classname()) == 0)
	  {
	    linguistic::Word *w = linguistic::Word::inflate_object(stream);
	    set_word(*w);
	    delete w;
	  } else {
      std::stringstream ss;
	  ss << classname << " element type is unknown!";
	  throw RootsException(__FILE__, __LINE__, ss.str().c_str());
	}

	if(is_terminal) stream->close_children();
      }

      Word * Word::inflate_object(RootsStream * stream, int list_index)
      {
	Word *t = new Word(true);
	t->inflate(stream,true,list_index);
	return t;
      }

    }
  }
}
