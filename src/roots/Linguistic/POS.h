/*
    This file is part of Roots.

    Roots is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Roots is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Roots.  If not, see <http://www.gnu.org/licenses/>.

    Copyright (c) 2013, 2014 IRISA
*/


#ifndef POS_H_
#define POS_H_

#include "../common.h"
#include "../BaseItem.h"

#include <boost/assign.hpp>

namespace roots
{
  namespace linguistic
  {
    namespace pos
    {



      static const int LEVEL_LABEL = 0;
      static const int LEVEL_CLASS_FINE = 1;
      static const int LEVEL_CLASS_COARSE = 2;
      static const int LEVEL_CLASS_TREEBANK_FR = 3;

      const int POS_ADJECTIVE = 0x00;
      const int POS_ADVERB = 0x01;
      const int POS_CONJUNCTION = 0x02;
      const int POS_INTERJECTION = 0x03;
      const int POS_NOUN = 0x04;
      const int POS_PREPOSITION = 0x05;
      const int POS_PRONOUN = 0x06;
      const int POS_PUNCTUATION = 0x07;
      const int POS_VERB = 0x08;


      const int POS_DETERMINER = 0x0F;
      const int POS_ARTICLE = 0x09;
      const int POS_DEMONSTRATIVE = 0x0A;
      const int POS_INDEFINITE = 0x0B;
      const int POS_INTERROGATIVE = 0x0C;
      const int POS_NUMERAL = 0x0D;
      const int POS_POSSESSIVE = 0x0E;

      const int POS_FOREIGN_WORD = 0x10;
      const int POS_PREFIX = 0x11;
      const int POS_PARTICLE = 0x12;
      const int POS_SYMBOL = 0x13;


      const char POS_IS_DEFINITE = '1';
      const char POS_NOT_IS_DEFINITE = '0';
      const char POS_IS_PROPER = '1';
      const char POS_NOT_IS_PROPER = '0';
      const char POS_IS_INTERROGATIVE = '1';
      const char POS_NOT_IS_INTERROGATIVE = '0';
      const char POS_IS_NEGATION = '1';
      const char POS_NOT_IS_NEGATION = '0';
      const char POS_IS_AUXILIARY = '1';
      const char POS_NOT_IS_AUXILIARY = '0';
      const char POS_IS_COLLECTIVE = '1';
      const char POS_NOT_IS_COLLECTIVE = '0';
      const char POS_IS_COUNTABLE = '1';
      const char POS_NOT_IS_COUNTABLE = '0';
      const char POS_IS_COMPOUND = '1';
      const char POS_NOT_IS_COMPOUND = '0';
      const char POS_IS_GERUND = '1';
      const char POS_NOT_IS_GERUND = '0';
      const char POS_IS_INVARIABLE = '1';
      const char POS_NOT_IS_INVARIABLE = '0';
      const char POS_IS_CLOSING = '1';
      const char POS_NOT_IS_CLOSING = '0';
      const char POS_IS_PASSIVE = '1';
      const char POS_NOT_IS_PASSIVE = '0';
      const char POS_IS_PARTITIVE = '1';
      const char POS_NOT_IS_PARTITIVE = '0';
      const char POS_IS_CARDINAL = '1';
      const char POS_NOT_IS_CARDINAL = '0';
      const char POS_IS_ORDINAL = '1';
      const char POS_NOT_IS_ORDINAL = '0';

      const char POS_UNKNOWN = 'u';

      enum Degree
      {
	POS_DEGREE_UNKNOWN = 0,
	POS_DEGREE_POSITIVE = 1,
	POS_DEGREE_NEGATIVE = 2,
	POS_DEGREE_COMPARATIVE = 3,
	POS_DEGREE_SUPERLATIVE = 4
      };


      enum Gender
      {
	POS_GENDER_UNKNOWN = 0,
	POS_GENDER_MALE = 1,
	POS_GENDER_FEMALE = 2,
	POS_GENDER_COMMON = 3,
	POS_GENDER_NEUTRAL = 4
      };


      enum Number
      {
	POS_NUMBER_UNKNOWN = 0,
	POS_NUMBER_SINGULAR = 1,
	POS_NUMBER_PLURAL = 2
      };


      enum ConjunctionCategory
      {
	POS_CONJUNCTION_CATEGORY_UNKNOWN = 0,
	POS_CONJUNCTION_CATEGORY_COORDINATING = 1,
	POS_CONJUNCTION_CATEGORY_SUBORDINATING = 2
      };


      enum Category
      {
	POS_CATEGORY_UNKNOWN = 0,
	POS_CATEGORY_PERSONAL = 1,
	POS_CATEGORY_DEMONSTRATIVE = 2,
	POS_CATEGORY_POSSESSIVE = 3,
	POS_CATEGORY_NUMERAL = 4,
	POS_CATEGORY_INTERROGATIVE = 5,
	POS_CATEGORY_EXCLAMATIVE = 6,
	POS_CATEGORY_RELATIVE = 7,
	POS_CATEGORY_REFLEXIVE = 8,
	POS_CATEGORY_INDEFINITE = 9
      };


      enum PunctuationCategory
      {
	POS_PUNCTUATION_CATEGORY_UNKNOWN = 0,
	POS_PUNCTUATION_CATEGORY_FINAL = 1,
	POS_PUNCTUATION_CATEGORY_PAUSE = 2,
	POS_PUNCTUATION_CATEGORY_INSERT = 3,
	POS_PUNCTUATION_CATEGORY_INSERT_CLOSE = 4,
	POS_PUNCTUATION_CATEGORY_INSERT_OPEN = 5
      };


      enum Mood
      {
	POS_MOOD_UNKNOWN = 0,
	POS_MOOD_INFINITIVE = 1,
	POS_MOOD_INDICATIVE = 2,
	POS_MOOD_CONDITIONAL = 3,
	POS_MOOD_SUBJONCTIVE = 4,
	POS_MOOD_IMPERATIVE = 5,
	POS_MOOD_PARTICIPLE = 6,
	POS_MOOD_GERUND = 7,
      };


      enum Tense
      {
	POS_TENSE_UNKNOWN = 0,
	POS_TENSE_PRESENT = 1,
	POS_TENSE_IMPERFECT = 2,
	POS_TENSE_PAST = 3,
	POS_TENSE_FUTURE = 4
      };


      enum Aspect
      {
	POS_ASPECT_UNKNOWN = 0,
	POS_ASPECT_FINITE = 1,
	POS_ASPECT_NON_FINITE = 2
      };


      enum Person
      {
	POS_PERSON_UNKNOWN = 0,
	POS_PERSON_FIRST = 1,
	POS_PERSON_SECOND = 2,
	POS_PERSON_THIRD = 3
      };


      /**
       * @brief Part Of Speech class
       * @details This class represents a generic Part Of Speech object. More precise
       * objects are defined as subclasses.
       * @author Cordial Group
       * @version 0.1
       * @date 2011
       */
      class POS : public BaseItem
      {
      protected:
      public:
	/**
	 * @brief Default constructor
	 */
	POS(const bool noInit = false);
	/**
	 * @brief Constructor
	 * @param pos
	 */
	POS(const POS &pos);
	/**
	 * @brief Destructor
	 */
	virtual ~POS();
	/**
	 * @brief clone the current object
	 * @return a copy of the object
	 */
	virtual POS *clone() const;
	/**
	 * @brief Copy POS features into the current POS object
	 * @param pos
	 * @return reference to the current POS object
	 */
	POS& operator=(const POS &pos);

	/**
	 * @brief Modifies the type of the POS Object
	 * @param atype
	 */
	void set_type(int atype)
	{
	  type = atype;
	}

	/**
	 * @brief Returns the POS type
	 * @return
	 */
	int get_type() const
	{
	  return type;
	};

	virtual bool is_adjective() const
	{
	  return false;
	}

	virtual bool is_adverb() const
	{
	  return false;
	}

	virtual bool is_conjunction() const
	{
	  return false;
	}

	virtual bool is_interjection() const
	{
	  return false;
	}

	virtual bool is_noun() const
	{
	  return false;
	}

	virtual bool is_preposition() const
	{
	  return false;
	}

	virtual bool is_pronoun() const
	{
	  return false;
	}

	virtual bool is_punctuation() const
	{
	  return false;
	}

	virtual bool is_verb() const
	{
	  return false;
	}

	virtual bool is_determiner() const
	{
	  return false;
	}

	virtual bool is_demonstrative() const
	{
	  return false;
	}

	virtual bool is_numeral() const
	{
	  return false;
	}

	virtual bool is_interrogative() const
	{
	  return false;
	}

	virtual bool is_indefinite() const
	{
	  return false;
	}

	virtual bool is_possessive() const
	{
	  return false;
	}

	virtual bool is_article() const
	{
	  return false;
	}

	virtual bool is_foreign_word() const
	{
	  return false;
	}

	virtual bool is_prefix() const
	{
	  return false;
	}

	virtual bool is_particle() const
	{
	  return false;
	}

	virtual bool is_symbol() const
	{
	  return false;
	}

	virtual bool is_unknown() const
	{
	  return true;
	}

	virtual bool is_invariable() const
	{
	  // Exception triggered iff :	(type != pos::POS_PRONOUN)
	  throw RootsException ( __FILE__,__LINE__, "There is no sense in asking this question in this class. \n" );
	  return false;
	}

	virtual bool is_personal() const
	{
	  // Exception triggered iff :	(type != pos::POS_PRONOUN)
	  throw RootsException ( __FILE__,__LINE__, "There is no sense in asking this question in this class. \n" );
	  return false;
	}

	virtual bool is_exclamative() const
	{
	  // Exception triggered iff :	(type != pos::POS_PRONOUN)
	  throw RootsException ( __FILE__,__LINE__, "There is no sense in asking this question in this class. \n" );
	  return false;
	}

	virtual bool is_relative() const
	{
	  // Exception triggered iff :	(type != pos::POS_PRONOUN)
	  throw RootsException ( __FILE__,__LINE__, "There is no sense in asking this question in this class. \n" );
	  return false;
	}

	virtual bool is_reflexive() const
	{
	  // Exception triggered iff :	(type != pos::POS_PRONOUN)
	  throw RootsException ( __FILE__,__LINE__, "There is no sense in asking this question in this class. \n" );
	  return false;
	}

	virtual bool is_male() const
	{
	  // Exception triggered iff :	(type != pos::POS_PRONOUN && type != pos::POS_DETERMINER && type != pos::POS_ADJECTIVE && type != pos::POS_NOUN && type != pos::POS_VERB)
	  throw RootsException ( __FILE__,__LINE__, "There is no sense in asking this question in this class. \n" );
	  return false;
	}

	virtual bool is_female() const
	{
	  // Exception triggered iff :	(type != pos::POS_PRONOUN && type != pos::POS_DETERMINER && type != pos::POS_ADJECTIVE && type != pos::POS_NOUN && type != pos::POS_VERB)
	  throw RootsException ( __FILE__,__LINE__, "There is no sense in asking this question in this class. \n" );
	  return false;
	}

	virtual bool is_common() const
	{
	  // Exception triggered iff :	(type != pos::POS_PRONOUN && type != pos::POS_DETERMINER && type != pos::POS_ADJECTIVE && type != pos::POS_NOUN && type != pos::POS_VERB)
	  throw RootsException ( __FILE__,__LINE__, "There is no sense in asking this question in this class. \n" );
	  return false;
	}

	virtual bool is_neutral() const
	{
	  // Exception triggered iff :	(type != pos::POS_PRONOUN && type != pos::POS_DETERMINER && type != pos::POS_ADJECTIVE && type != pos::POS_NOUN && type != pos::POS_VERB)
	  throw RootsException ( __FILE__,__LINE__, "There is no sense in asking this question in this class. \n" );
	  return false;
	}

	virtual bool is_singular() const
	{
	  // Exception triggered iff :	(type != pos::POS_PRONOUN && type != pos::POS_DETERMINER && type != pos::POS_ADJECTIVE && type != pos::POS_NOUN && type != pos::POS_VERB && type != pos::POS_POSSESSIVE)
	  throw RootsException ( __FILE__,__LINE__, "There is no sense in asking this question in this class. \n" );
	  return false;
	}

	virtual bool is_plural() const
	{
	  // Exception triggered iff :	(type != pos::POS_PRONOUN && type != pos::POS_DETERMINER && type != pos::POS_ADJECTIVE && type != pos::POS_NOUN && type != pos::POS_VERB && type != pos::POS_POSSESSIVE)
	  throw RootsException ( __FILE__,__LINE__, "There is no sense in asking this question in this class. \n" );
	  return false;
	}

	virtual bool is_first_person() const
	{
	  // Exception triggered iff :	(type != pos::POS_PRONOUN && type != pos::POS_VERB && type != pos::POS_POSSESSIVE)
	  throw RootsException ( __FILE__,__LINE__, "There is no sense in asking this question in this class. \n" );
	  return false;
	}

	virtual bool is_second_person() const
	{
	  // Exception triggered iff :	(type != pos::POS_PRONOUN && type != pos::POS_VERB && type != pos::POS_POSSESSIVE)
	  throw RootsException ( __FILE__,__LINE__, "There is no sense in asking this question in this class. \n" );
	  return false;
	}

	virtual bool is_third_person() const
	{
	  // Exception triggered iff :	(type != pos::POS_PRONOUN && type != pos::POS_VERB && type != pos::POS_POSSESSIVE)
	  throw RootsException ( __FILE__,__LINE__, "There is no sense in asking this question in this class. \n" );
	  return false;
	}

	virtual bool is_first_person_singular() const
	{
	  // Exception triggered iff :	(type != pos::POS_PRONOUN)
	  throw RootsException ( __FILE__,__LINE__, "There is no sense in asking this question in this class. \n" );
	  return false;
	}

	virtual bool is_first_person_plural() const
	{
	  // Exception triggered iff :	(type != pos::POS_PRONOUN)
	  throw RootsException ( __FILE__,__LINE__, "There is no sense in asking this question in this class. \n" );
	  return false;
	}

	virtual bool is_second_person_singular() const
	{
	  // Exception triggered iff :	(type != pos::POS_PRONOUN)
	  throw RootsException ( __FILE__,__LINE__, "There is no sense in asking this question in this class. \n" );
	  return false;
	}

	virtual bool is_second_person_plural() const
	{
	  // Exception triggered iff :	(type != pos::POS_PRONOUN)
	  throw RootsException ( __FILE__,__LINE__, "There is no sense in asking this question in this class. \n" );
	  return false;
	}

	virtual bool is_third_person_singular() const
	{
	  // Exception triggered iff :	(type != pos::POS_PRONOUN)
	  throw RootsException ( __FILE__,__LINE__, "There is no sense in asking this question in this class. \n" );
	  return false;
	}

	virtual bool is_third_person_plural() const
	{
	  // Exception triggered iff :	(type != pos::POS_PRONOUN)
	  throw RootsException ( __FILE__,__LINE__, "There is no sense in asking this question in this class. \n" );
	  return false;
	}

	virtual bool is_positive() const
	{
	  // Exception triggered iff :	(type != pos::POS_ADJECTIVE && type != pos::POS_ADVERB)
	  throw RootsException ( __FILE__,__LINE__, "There is no sense in asking this question in this class. \n" );
	  return false;
	}

	virtual bool is_comparative() const
	{
	  // Exception triggered iff :	(type != pos::POS_ADJECTIVE && type != pos::POS_ADVERB)
	  throw RootsException ( __FILE__,__LINE__, "There is no sense in asking this question in this class. \n" );
	  return false;
	}

	virtual bool is_superlative() const
	{
	  // Exception triggered iff :	(type != pos::POS_ADJECTIVE && type != pos::POS_ADVERB)
	  throw RootsException ( __FILE__,__LINE__, "There is no sense in asking this question in this class. \n" );
	  return false;
	}

	virtual bool is_negation() const
	{
	  // Exception triggered iff :	(type != pos::POS_ADVERB)
	  throw RootsException ( __FILE__,__LINE__, "There is no sense in asking this question in this class. \n" );
	  return false;
	}

	virtual bool is_ordinal() const
	{
	  // Exception triggered iff :	(type != pos::POS_NUMERAL)
	  throw RootsException ( __FILE__,__LINE__, "There is no sense in asking this question in this class. \n" );
	  return false;
	}

	virtual bool is_cardinal() const
	{
	  // Exception triggered iff :	(type != pos::POS_NUMERAL)
	  throw RootsException ( __FILE__,__LINE__, "There is no sense in asking this question in this class. \n" );
	  return false;
	}

	virtual bool is_coordinating() const
	{
	  // Exception triggered iff :	(type != pos::POS_CONJUNCTION)
	  throw RootsException ( __FILE__,__LINE__, "There is no sense in asking this question in this class. \n" );
	  return false;
	}

	virtual bool is_subordinating() const
	{
	  // Exception triggered iff :	(type != pos::POS_CONJUNCTION)
	  throw RootsException ( __FILE__,__LINE__, "There is no sense in asking this question in this class. \n" );
	  return false;
	}

	virtual bool is_closing() const
	{
	  // Exception triggered iff :	(type != pos::POS_PUNCTUATION)
	  throw RootsException ( __FILE__,__LINE__, "There is no sense in asking this question in this class. \n" );
	  return false;
	}

	virtual bool is_final() const
	{
	  // Exception triggered iff :	(type != pos::POS_PUNCTUATION)
	  throw RootsException ( __FILE__,__LINE__, "There is no sense in asking this question in this class. \n" );
	  return false;
	}

	virtual bool is_pause() const
	{
	  // Exception triggered iff :	(type != pos::POS_PUNCTUATION)
	  throw RootsException ( __FILE__,__LINE__, "There is no sense in asking this question in this class. \n" );
	  return false;
	}

	virtual bool is_insert() const
	{
	  // Exception triggered iff :	(type != pos::POS_PUNCTUATION)
	  throw RootsException ( __FILE__,__LINE__, "There is no sense in asking this question in this class. \n" );
	  return false;
	}

	virtual bool is_proper() const
	{
	  // Exception triggered iff :	(type != pos::POS_NOUN)
	  throw RootsException ( __FILE__,__LINE__, "There is no sense in asking this question in this class. \n" );
	  return false;
	}

	virtual bool is_collective() const
	{
	  // Exception triggered iff :	(type != pos::POS_NOUN)
	  throw RootsException ( __FILE__,__LINE__, "There is no sense in asking this question in this class. \n" );
	  return false;
	}

	virtual bool is_countable() const
	{
	  // Exception triggered iff :	(type != pos::POS_NOUN)
	  throw RootsException ( __FILE__,__LINE__, "There is no sense in asking this question in this class. \n" );
	  return false;
	}

	virtual bool is_compound() const
	{
	  // Exception triggered iff :	(type != pos::POS_NOUN)
	  throw RootsException ( __FILE__,__LINE__, "There is no sense in asking this question in this class. \n" );
	  return false;
	}

	virtual bool is_gerund() const
	{
	  // Exception triggered iff :	(type != pos::POS_NOUN)
	  throw RootsException ( __FILE__,__LINE__, "There is no sense in asking this question in this class. \n" );
	  return false;
	}

	virtual bool is_passive() const
	{
	  // Exception triggered iff :	(type != pos::POS_VERB)
	  throw RootsException ( __FILE__,__LINE__, "There is no sense in asking this question in this class. \n" );
	  return false;
	}

	virtual bool is_auxiliary() const
	{
	  // Exception triggered iff :	(type != pos::POS_VERB)
	  throw RootsException ( __FILE__,__LINE__, "There is no sense in asking this question in this class. \n" );
	  return false;
	}

	virtual bool is_infinitive() const
	{
	  // Exception triggered iff :	(type != pos::POS_VERB)
	  throw RootsException ( __FILE__,__LINE__, "There is no sense in asking this question in this class. \n" );
	  return false;
	}

	virtual bool is_indicative() const
	{
	  // Exception triggered iff :	(type != pos::POS_VERB)
	  throw RootsException ( __FILE__,__LINE__, "There is no sense in asking this question in this class. \n" );
	  return false;
	}

	virtual bool is_conditional() const
	{
	  // Exception triggered iff :	(type != pos::POS_VERB)
	  throw RootsException ( __FILE__,__LINE__, "There is no sense in asking this question in this class. \n" );
	  return false;
	}

	virtual bool is_subjonctive() const
	{
	  // Exception triggered iff :	(type != pos::POS_VERB)
	  throw RootsException ( __FILE__,__LINE__, "There is no sense in asking this question in this class. \n" );
	  return false;
	}

	virtual bool is_imperative() const
	{
	  // Exception triggered iff :	(type != pos::POS_VERB)
	  throw RootsException ( __FILE__,__LINE__, "There is no sense in asking this question in this class. \n" );
	  return false;
	}

	virtual bool is_participle() const
	{
	  // Exception triggered iff :	(type != pos::POS_VERB)
	  throw RootsException ( __FILE__,__LINE__, "There is no sense in asking this question in this class. \n" );
	  return false;
	}

	virtual bool is_present() const
	{
	  // Exception triggered iff :	(type != pos::POS_VERB)
	  throw RootsException ( __FILE__,__LINE__, "There is no sense in asking this question in this class. \n" );
	  return false;
	}

	virtual bool is_imperfect() const
	{
	  // Exception triggered iff :	(type != pos::POS_VERB)
	  throw RootsException ( __FILE__,__LINE__, "There is no sense in asking this question in this class. \n" );
	  return false;
	}

	virtual bool is_past() const
	{
	  // Exception triggered iff :	(type != pos::POS_VERB)
	  throw RootsException ( __FILE__,__LINE__, "There is no sense in asking this question in this class. \n" );
	  return false;
	}

	virtual bool is_future() const
	{
	  // Exception triggered iff :	(type != pos::POS_VERB)
	  throw RootsException ( __FILE__,__LINE__, "There is no sense in asking this question in this class. \n" );
	  return false;
	}

	virtual bool is_finite() const
	{
	  // Exception triggered iff :	(type != pos::POS_VERB)
	  throw RootsException ( __FILE__,__LINE__, "There is no sense in asking this question in this class. \n" );
	  return false;
	}

	virtual bool is_non_finite() const
	{
	  // Exception triggered iff :	(type != pos::POS_VERB)
	  throw RootsException ( __FILE__,__LINE__, "There is no sense in asking this question in this class. \n" );
	  return false;
	}

	virtual bool is_wh() const
	{
	  // Exception triggered iff :	(type != pos::POS_VERB)
	  //throw RootsException ( __FILE__,__LINE__, "There is no sense in asking this question in this class. \n" );
	  return false;
	}

	/**
	 * @brief returns a string representation of any Roots object
	 * @param level the precision level of the output
	 * @return string representation of the object
	 */
	virtual std::string to_string(int level = 0) const;

	/**
	 * @brief Write the entity into a RootsStream
	 * @param stream the stream from which we inflate the element
	 * @param is_terminal indicates that the node is terminal (true by default)
	 */
	virtual void deflate(RootsStream * stream, bool is_terminal=true, int list_index=-1);
	/**
	 * @brief Extracts the entity from the stream
	 * @param stream the stream from which we inflate the element
	 */
	virtual void inflate(RootsStream * stream, bool is_terminal=true, int list_index=-1);
	/**
	 * @brief Extracts the entity from the stream
	 * @param stream the stream from which we inflate the element
	 */
	static POS * inflate_object(RootsStream * stream, int list_index=-1);
	/**
	 * @brief returns the XML tag name value for current object
	 * @return string constant representing the XML tag name
	 */
	virtual std::string get_xml_tag_name() const
	  {
	    return xml_tag_name();
	  };

	/**
	 * @brief returns the XML tag name value for current class
	 * @return string constant representing the XML tag name
	 */
	static std::string xml_tag_name()
	{
	  return "pos";
	};

	/**
	 * @brief returns classname for current object
	 * @return string constant representing the classname
	 */
	virtual std::string get_classname() const
	  {
	    return classname();
	  };

	/**
	 * @brief returns classname for current class
	 * @return string constant representing the classname
	 */
	static std::string classname()
	{
	  return "Linguistic::POS";
	};

	/**
	 * @brief returns display color for current object
	 * @return string constant representing the pgf display color
	 */
	virtual std::string get_pgf_display_color() const
	  {
	    return pgf_display_color();
	  };

	/**
	 * @brief returns display color for current class
	 * @return string constant representing the pgf display color
	 */
	static std::string pgf_display_color()
	{
	  return "blue!25";
	};

      protected:
	int type; /**<Type of POS Tag*/

	static std::map<Degree, std::string> degreeToString;
	static std::map<Degree, char> degreeToChar;
	static std::map<std::string, Degree> stringToDegree;
	static std::map<Gender, std::string> genderToString;
	static std::map<Gender, char> genderToChar;
	static std::map<std::string, Gender> stringToGender;
	static std::map<Number, std::string> numberToString;
	static std::map<Number, char> numberToChar;
	static std::map<std::string, Number> stringToNumber;
	static std::map<ConjunctionCategory, std::string> conjunctionCategoryToString;
	static std::map<ConjunctionCategory, char> conjunctionCategoryToChar;
	static std::map<std::string, ConjunctionCategory> stringToConjunctionCategory;
	static std::map<Category, std::string> categoryToString;
	static std::map<Category, char> categoryToChar;
	static std::map<std::string, Category> stringToCategory;
	static std::map<PunctuationCategory, std::string> punctuationCategoryToString;
	static std::map<PunctuationCategory, char> punctuationCategoryToChar;
	static std::map<std::string, PunctuationCategory> stringToPunctuationCategory;
	static std::map<Mood, std::string> moodToString;
	static std::map<Mood, char> moodToChar;
	static std::map<std::string, Mood> stringToMood;
	static std::map<Tense, std::string> tenseToString;
	static std::map<Tense, char> tenseToChar;
	static std::map<std::string, Tense> stringToTense;
	static std::map<Aspect, std::string> aspectToString;
	static std::map<Aspect, char> aspectToChar;
	static std::map<std::string, Aspect> stringToAspect;
	static std::map<Person, std::string> personToString;
	static std::map<Person, char> personToChar;
	static std::map<std::string, Person> stringToPerson;
	static char whToChar;
	static std::string whToString;


      };


    }
  }
}// END OF NAMESPACES


#endif /* POS_H_ */
