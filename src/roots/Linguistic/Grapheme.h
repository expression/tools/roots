/*
    This file is part of Roots.

    Roots is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Roots is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Roots.  If not, see <http://www.gnu.org/licenses/>.

    Copyright (c) 2013, 2014 IRISA
*/


#ifndef CHARACTER_H_
#define CHARACTER_H_

#include "../BaseItem.h"

namespace roots { namespace linguistic {

/**
 * @brief Grapheme class
 * @details This class represents a Grapheme.
 * @author Cordial Group
 * @version 0.1
 * @date 2011
 */
class Grapheme: public BaseItem
{
protected:
	/**
	 * @brief Default constructor
	 */
  Grapheme(const bool noInit = false);
public:
	/**
	 * @brief Build a char from a label
	 * @param aLabel the char label
	 */
	Grapheme(const UChar &aLabel);

	/**
	 * @brief Build a char from a label
	 * @param aLabel the char label
	 */
	Grapheme(const std::string &aLabel);

	/**
	 * @brief Build a char from another char
	 * @param aChar
	 */
	Grapheme(const Grapheme &aChar);
	/**
	 * @brief Destructor
	 */
	virtual ~Grapheme();
    /**
     * @brief clone the current char
     * @return a copy of the char
     */
	virtual Grapheme *clone() const;
	/**
	 * @brief Copy char features into the current char
	 * @param char
	 * @return reference to the current char
	 */
	Grapheme& operator= (const Grapheme &achar);
	
	virtual double compute_dissimilarity ( const Base* b ) const;
	
	/**
	 * @brief returns a string representation of any Roots object
	 * @param level the precision level of the output
	 * @return string representation of the object
	 */
	virtual std::string to_string(int level=0) const;
	/**
	 * @brief Write the entity into a RootsStream
	 * @param stream the stream from which we inflate the element
	 * @param is_terminal indicates that the node is terminal (true by default)
	 */
	virtual void deflate(RootsStream * stream, bool is_terminal=true, int list_index=-1);
	/**
	 * @brief Extracts the entity from the stream
	 * @param stream the stream from which we inflate the element
	 */
	virtual void inflate(RootsStream * stream, bool is_terminal=true, int list_index=-1);
	/**
	 * @brief Extracts the entity from the stream
	 * @param stream the stream from which we inflate the element
	 */
	static Grapheme * inflate_object(RootsStream * stream, int list_index=-1);									
	/**
	 * @brief returns the XML tag name value for current object
	 * @return string constant representing the XML tag name 
	 */
	virtual std::string get_xml_tag_name() const { return xml_tag_name(); };
	/**
	 * @brief returns the XML tag name value for current class
	 * @return string constant representing the XML tag name 
	 */
	static std::string xml_tag_name() { return "grapheme"; };
	/**
	 * @brief returns classname for current object
	 * @return string constant representing the classname
	 */
	virtual std::string get_classname() const { return classname(); };
	/**
	 * @brief returns classname for current class
	 * @return string constant representing the classname
	 */
	static std::string classname() { return "Linguistic::Grapheme"; };
	/**
	 * @brief returns display color for current object
	 * @return string constant representing the pgf display color
	 */
	virtual std::string get_pgf_display_color() const { return pgf_display_color();};
	/**
	 * @brief returns display color for current class
	 * @return string constant representing the pgf display color
	 */
	static std::string pgf_display_color() { return "green!25"; };

};

} } // END OF NAMESPACES


#endif /* CHAR_H_ */
