/*
    This file is part of Roots.

    Roots is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Roots is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Roots.  If not, see <http://www.gnu.org/licenses/>.

    Copyright (c) 2013, 2014 IRISA
*/



#include "Interjection.h"

namespace roots { namespace linguistic {  namespace pos {


Interjection::Interjection(const bool noInit) : POS(noInit)
{
	set_type(POS_INTERJECTION);
}

Interjection::Interjection(const Interjection &inter) : POS(inter)
{
	set_type(POS_INTERJECTION);
}

Interjection::~Interjection()
{
}

Interjection *Interjection::clone() const
{
	return new Interjection(*this);
}

Interjection& Interjection::operator=(const Interjection &inte)
{
	POS::operator=(inte);
	set_type(inte.get_type());

	return *this;
}

std::string Interjection::to_string(int level) const
{
	std::stringstream ss;

	switch (level)
	{
	  case LEVEL_CLASS_TREEBANK_FR:
		ss << "I";
		break;
    case LEVEL_CLASS_COARSE:
        ss << "Interjection";
        break;
        
    case LEVEL_CLASS_FINE:
        ss << "Interjection";
        break;
        
    case LEVEL_LABEL:
    default:
        ss 	<<  "In";
	}
	return std::string(ss.str().c_str());
}

void Interjection::deflate(RootsStream * stream, bool is_terminal, int list_index)
{
  POS::deflate(stream,false,list_index);

  if(is_terminal)
	stream->close_object();
}


void Interjection::inflate(RootsStream * stream, bool is_terminal, int list_index)
{
	POS::inflate(stream,false,list_index);
	if(is_terminal) stream->close_children();
}

Interjection * Interjection::inflate_object(RootsStream * stream, int list_index)
{
	Interjection *t = new Interjection(true);
	t->inflate(stream,true,list_index);
	return t;	
}


} } } // END OF NAMESPACES
