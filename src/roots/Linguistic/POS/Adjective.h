/*
    This file is part of Roots.

    Roots is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Roots is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Roots.  If not, see <http://www.gnu.org/licenses/>.

    Copyright (c) 2013, 2014 IRISA
*/


#ifndef ADJECTIVE_H_
#define ADJECTIVE_H_

#include "../../common.h"
#include "../POS.h"

namespace roots { namespace linguistic { namespace pos {

/**
 * @brief Adjective class
 * @details This class represents the Part Of Speech Adjective.
 * @author Cordial Group
 * @version 0.1
 * @date 2011
 */
class Adjective: public POS
{
public:
	Adjective(const bool noInit = false);
	Adjective(Degree d, Number n, Gender g);
	Adjective(const Adjective &adjective);
	virtual ~Adjective();
	/**
	 * @brief clone the current object
	 * @return a copy of the object
	 */
	virtual Adjective *clone() const;

	Adjective& operator=(const Adjective &adj);

	void set_degree(Degree aDegree);
	void set_number(Number aNumber);
	void set_gender(Gender aGender);

	Degree get_degree() const {return degree;}
	Number get_number() const {return number;}
	Gender get_gender() const {return gender;}

	bool is_adjective() const { return true; }
	bool is_unknown() const { return false; }

	bool is_positive() const
	{
		return get_degree() == POS_DEGREE_POSITIVE;
	}

	bool is_comparative() const
	{
		return get_degree() == POS_DEGREE_COMPARATIVE;
	}

	bool is_superlative() const
	{
		return get_degree() == POS_DEGREE_SUPERLATIVE;
	}

	bool is_negation() const
	{
		return get_degree() == POS_DEGREE_NEGATIVE;
	}

	bool is_male() const
	{
		return get_gender() == POS_GENDER_MALE;
	}

	bool is_female() const
	{
		return get_gender() == POS_GENDER_FEMALE;
	}

	bool is_common() const
	{
		return get_gender() == POS_GENDER_COMMON;
	}

	bool is_neutral() const
	{
		return get_gender() == POS_GENDER_NEUTRAL;
	}

	bool is_singular() const
	{
		return get_number() == POS_NUMBER_SINGULAR;
	}

	bool is_plural() const
	{
		return get_number() == POS_NUMBER_PLURAL;
	}

	/**
	 * @brief returns a string representation of any Roots object
	 * @param level the precision level of the output
	 * @return string representation of the object
	 */
	virtual std::string to_string(int level=0) const;

	/**
	 * @brief Write the entity into a RootsStream
	 * @param stream the stream from which we inflate the element
	 * @param is_terminal indicates that the node is terminal (true by default)
	 */
	virtual void deflate(RootsStream * stream, bool is_terminal=true, int list_index=-1);
	/**
	 * @brief Extracts the entity from the stream
	 * @param stream the stream from which we inflate the element
	 */
	virtual void inflate(RootsStream * stream, bool is_terminal=true, int list_index=-1);
	/**
	 * @brief Extracts the entity from the stream
	 * @param stream the stream from which we inflate the element
	 */
	static Adjective * inflate_object(RootsStream * stream, int list_index=-1);
	/**
	 * @brief returns classname for current object
	 * @return string constant representing the classname
	 */
	virtual std::string get_classname() const { return classname(); };
	/**
	 * @brief returns classname for current class
	 * @return string constant representing the classname
	 */
	static std::string classname() { return "Linguistic::POS::Adjective"; };

private:
	Degree degree;
	Number number;
	Gender gender;

};


} } }// END OF NAMESPACES


#endif /* ADJECTIVE_H_ */
