/*
    This file is part of Roots.

    Roots is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Roots is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Roots.  If not, see <http://www.gnu.org/licenses/>.

    Copyright (c) 2013, 2014 IRISA
*/


#ifndef NOUN_H_
#define NOUN_H_

#include "../../common.h"
#include "../POS.h"

namespace roots { namespace linguistic { namespace pos {

/**
 * @brief Noun class
 * @details This class represents the Part Of Speech Noun.
 * @author Cordial Group
 * @version 0.1
 * @date 2011
 */
class Noun: public POS
{
public:
	Noun(const bool noInit = false);
	Noun(Gender g, Number n, char gerund, char proper, char collective, char countable, char compound);
    Noun(Gender g, Number n, char proper);
	Noun(const Noun &noun);
	virtual ~Noun();
	/**
	 * @brief clone the current object
	 * @return a copy of the object
	 */
	virtual Noun *clone() const;

	Noun& operator=(const Noun &noun);


	void set_number(Number aNumber);
	void set_gender(Gender aGender);
	void set_is_gerund(char b) {isGerund = b;}
	void set_is_proper(char b) {isProper = b;}
	void set_is_collective(char b) {isCollective = b;}
	void set_is_countable(char b) {isCountable = b;}
	void set_is_compound(char b) {isCompound = b;}

	Number get_number() const {return number;}
	Gender get_gender() const {return gender;}
	char get_is_gerund() const {return isGerund;}
	char get_is_proper() const {return isProper;}
	char get_is_collective() const {return isCollective;}
	char get_is_countable() const {return isCountable;}
	char get_is_compound() const {return isCompound;}

	bool is_noun() const { return true; }
	bool is_unknown() const { return false; }

	bool is_gerund() const
	{
		return get_is_gerund() == POS_IS_GERUND;
	}

	bool is_proper() const
	{
		return get_is_proper() == POS_IS_PROPER;
	}

	bool is_collective() const
	{
		return get_is_collective() == POS_IS_COLLECTIVE;
	}

	bool is_countable() const
	{
		return get_is_countable() == POS_IS_COUNTABLE;
	}

	bool is_compound() const
	{
		return get_is_compound() == POS_IS_COMPOUND;
	}

	/**
	 * @brief Returns true for male gender
	 */
	bool is_male() const
	{
		return get_gender() == POS_GENDER_MALE;
	}

	/**
	 * @brief Returns true for female gender
	 */
	bool is_female() const
	{
		return get_gender() == POS_GENDER_FEMALE;
	}

	/**
	 * @brief Returns true for common gender
	 */
	bool is_common() const
	{
		return get_gender() == POS_GENDER_COMMON;
	}

	/**
	 * @brief Returns true for neutral gender
	 */
	bool is_neutral() const
	{
		return get_gender() == POS_GENDER_NEUTRAL;
	}

	/**
	 * @brief Returns true for singular number
	 */
	bool is_singular() const
	{
		return get_number() == POS_NUMBER_SINGULAR;
	}

	/**
	 * @brief Returns true for plural number
	 */
	bool is_plural() const
	{
		return get_number() == POS_NUMBER_PLURAL;
	}

	/**
	 * @brief returns a string representation of any Roots object
	 * @param level the precision level of the output
	 * @return string representation of the object
	 */
	virtual std::string to_string(int level=0) const;
	/**
	 * @brief Write the entity into a RootsStream
	 * @param stream the stream from which we inflate the element
	 * @param is_terminal indicates that the node is terminal (true by default)
	 */
	virtual void deflate(RootsStream * stream, bool is_terminal=true, int list_index=-1);
	/**
	 * @brief Extracts the entity from the stream
	 * @param stream the stream from which we inflate the element
	 */
	virtual void inflate(RootsStream * stream, bool is_terminal=true, int list_index=-1);
	/**
	 * @brief Extracts the entity from the stream
	 * @param stream the stream from which we inflate the element
	 */
	static Noun * inflate_object(RootsStream * stream, int list_index=-1);
	/**
	 * @brief returns classname for current object
	 * @return string constant representing the classname
	 */
	virtual std::string get_classname() const { return classname(); };
	/**
	 * @brief returns classname for current class
	 * @return string constant representing the classname
	 */
	static std::string classname() { return "Linguistic::POS::Noun"; };

private:
	Gender gender;
	Number number;
	char isGerund;
	char isProper;
	char isCollective;
	char isCountable;
	char isCompound;

};

} } }// END OF NAMESPACES


#endif /* NOUN_H_ */
