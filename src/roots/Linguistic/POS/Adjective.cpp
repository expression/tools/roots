/*
    This file is part of Roots.

    Roots is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Roots is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Roots.  If not, see <http://www.gnu.org/licenses/>.

    Copyright (c) 2013, 2014 IRISA
*/


#include "Adjective.h"

namespace roots { namespace linguistic { namespace pos {

      Adjective::Adjective(const bool noInit) : POS(noInit)
{
	set_type(POS_ADJECTIVE);
	set_degree(POS_DEGREE_UNKNOWN);
	set_number(POS_NUMBER_UNKNOWN);
	set_gender(POS_GENDER_UNKNOWN);
}

Adjective::Adjective(Degree d, Number n, Gender g) : degree(d), number(n), gender(g)
{
	set_type(POS_ADJECTIVE);
}

Adjective::Adjective(const Adjective &adjective) : POS(adjective)
{
	set_degree(adjective.get_degree());
	set_number(adjective.get_number());
	set_gender(adjective.get_gender());
}

Adjective::~Adjective()
{
}

Adjective *Adjective::clone() const
{
	return new Adjective(*this);
}

Adjective& Adjective::operator=(const Adjective &adj)
{
	POS::operator=(adj);
	set_type(adj.get_type());
	set_degree(adj.get_degree());
	set_number(adj.get_number());
	set_gender(adj.get_gender());

	return *this;
}

void Adjective::set_degree(Degree aDegree)
{
	if(degreeToString.find(aDegree) == degreeToString.end())
	{
		std::stringstream ss2;
		ss2 << "Unknown degree: "<<aDegree<<"!\n";
		throw RootsException(__FILE__, __LINE__, ss2.str().c_str());
	}
	degree = aDegree;
}
void Adjective::set_number(Number aNumber)
{
	if(numberToString.find(aNumber) == numberToString.end())
	{
		std::stringstream ss2;
		ss2 << "Unknown number: "<<aNumber<<"!\n";
		throw RootsException(__FILE__, __LINE__, ss2.str().c_str());
	}
	number = aNumber;
}
void Adjective::set_gender(Gender aGender)
{
	if(genderToString.find(aGender) == genderToString.end())
	{
		std::stringstream ss2;
		ss2 << "Unknown gender: "<<aGender<<"!\n";
		throw RootsException(__FILE__, __LINE__, ss2.str().c_str());
	}
	gender = aGender;
}

std::string Adjective::to_string(int level) const
{
	std::stringstream ss;

	switch (level)
	{   
	  case LEVEL_CLASS_TREEBANK_FR:
		ss << "A";
		break;
    case LEVEL_CLASS_COARSE:
        ss << "Adjective";
        break;
        
    case LEVEL_CLASS_FINE:
        ss 	<<  "Adjective";
        if(this->degree != POS_DEGREE_UNKNOWN) ss << "_" << degreeToString[this->degree];
        if(this->gender != POS_GENDER_UNKNOWN) ss << "_" << genderToString[this->gender];
        if(this->number != POS_NUMBER_UNKNOWN) ss << "_" << numberToString[this->number];
        break;
        
    case LEVEL_LABEL:
    default:
        ss 	<<  "Aj";
        if(this->degree != POS_DEGREE_UNKNOWN) ss << degreeToChar[this->degree];
        if(this->gender != POS_GENDER_UNKNOWN) ss << genderToChar[this->gender];
        if(this->number != POS_NUMBER_UNKNOWN) ss << numberToChar[this->number];
	}
	return std::string(ss.str().c_str());
}

void Adjective::deflate(RootsStream * stream, bool is_terminal, int list_index)
{
  POS::deflate(stream,false,list_index);

  stream->append_int_content("degree", this->get_degree());
  stream->append_int_content("number", this->get_number());
  stream->append_int_content("gender", this->get_gender());

  if(is_terminal)
	stream->close_object();
}


void Adjective::inflate(RootsStream * stream, bool is_terminal, int list_index)
{
	int tmp=0;
	POS::inflate(stream,false,list_index);

	//stream->open_children();

	tmp =  stream->get_int_content("degree");
	if(degreeToString.find((Degree)tmp) == degreeToString.end())
	  {
	    std::stringstream ss2;
	    ss2 << "Unknown degree: "<<tmp<<"!\n";
	    throw RootsException(__FILE__, __LINE__, ss2.str().c_str());
	  }
	this->set_degree((Degree)tmp);

	tmp =  stream->get_int_content("number");
	if(numberToString.find((Number)tmp) == numberToString.end())
	  {
	    std::stringstream ss2;
	    ss2 << "Unknown number: "<<tmp<<"!\n";
	    throw RootsException(__FILE__, __LINE__, ss2.str().c_str());
	  }
	this->set_number((Number)tmp);

	tmp =  stream->get_int_content("gender");
	if(genderToString.find((Gender)tmp) == genderToString.end())
	  {
	    std::stringstream ss2;
	    ss2 << "Unknown gender: "<<tmp<<"!\n";
	    throw RootsException(__FILE__, __LINE__, ss2.str().c_str());
	  }
	this->set_gender((Gender)tmp);

	if(is_terminal) stream->close_children();	
}

Adjective * Adjective::inflate_object(RootsStream * stream, int list_index)
{
	Adjective *t = new Adjective(true);
	t->inflate(stream,true,list_index);
	return t;	
}
} } } // END OF NAMESPACES

