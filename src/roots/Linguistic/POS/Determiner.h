/*
    This file is part of Roots.

    Roots is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Roots is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Roots.  If not, see <http://www.gnu.org/licenses/>.

    Copyright (c) 2013, 2014 IRISA
*/


#ifndef DETERMINER_H_
#define DETERMINER_H_

#include "../../common.h"
#include "../POS.h"

namespace roots { namespace linguistic { namespace pos {

/**
 * @brief Determiner class
 * @details This class represents the Part Of Speech Determiner.
 * @author Cordial Group
 * @version 0.1
 * @date 2011
 */
class Determiner: public POS
{
public:
	Determiner(const bool noInit = false);
	Determiner(Gender aGender, Number aNumber);
	Determiner(const Determiner &det);
	virtual ~Determiner();
	/**
	 * @brief clone the current object
	 * @return a copy of the object
	 */
	virtual Determiner *clone() const;

	Determiner& operator=(const Determiner &det);

	void set_number(Number aNumber);
	void set_gender(Gender aGender);
	void set_is_wh(bool b) {isWh = b;}

	Number get_number() const {return number;}
	Gender get_gender() const {return gender;}

	bool is_determiner() const { return true; }
	bool is_unknown() const { return false; }


	bool is_article() const
	{
		return get_type() == POS_ARTICLE;
	}

	bool is_demonstrative() const
	{
		return get_type() == POS_DEMONSTRATIVE;
	}

	bool is_indefinite() const
	{
		return get_type() == POS_INDEFINITE;
	}

	bool is_possessive() const
	{
		return get_type() == POS_POSSESSIVE;
	}

	bool is_interrogative() const
	{
		return get_type() == POS_INTERROGATIVE;
	}

	bool is_numeral() const
	{
		return get_type() == POS_NUMERAL;
	}

	bool is_male() const
	{
		return get_gender() == POS_GENDER_MALE;
	}

	bool is_female() const
	{
		return get_gender() == POS_GENDER_FEMALE;
	}

	bool is_common() const
	{
		return get_gender() == POS_GENDER_COMMON;
	}

	bool is_neutral() const
	{
		return get_gender() == POS_GENDER_NEUTRAL;
	}

	bool is_singular() const
	{
		return get_number() == POS_NUMBER_SINGULAR;
	}

	bool is_plural() const
	{
		return get_number() == POS_NUMBER_PLURAL;
	}
	
	bool is_wh() const
	{
	  return isWh;
	}

	/**
	 * @brief returns a string representation of any Roots object
	 * @param level the precision level of the output
	 * @return string representation of the object
	 */
	virtual std::string to_string(int level=0) const;
	/**
	 * @brief Write the entity into a RootsStream
	 * @param stream the stream from which we inflate the element
	 * @param is_terminal indicates that the node is terminal (true by default)
	 */
	virtual void deflate(RootsStream * stream, bool is_terminal=true, int list_index=-1);
	/**
	 * @brief Extracts the entity from the stream
	 * @param stream the stream from which we inflate the element
	 */
	virtual void inflate(RootsStream * stream, bool is_terminal=true, int list_index=-1);
	/**
	 * @brief Extracts the entity from the stream
	 * @param stream the stream from which we inflate the element
	 */
	static Determiner * inflate_object(RootsStream * stream, int list_index=-1);
	/**
	 * @brief returns classname for current object
	 * @return string constant representing the classname
	 */
	virtual std::string get_classname() const { return classname(); };
	/**
	 * @brief returns classname for current class
	 * @return string constant representing the classname
	 */
	static std::string classname() { return "Linguistic::POS::Determiner"; };

protected:
	Gender gender;
	Number number;
	bool isWh;
};

} } }// END OF NAMESPACES

#endif /* DETERMINER_H_ */
