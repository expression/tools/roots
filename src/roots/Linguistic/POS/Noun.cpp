/*
    This file is part of Roots.

    Roots is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Roots is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Roots.  If not, see <http://www.gnu.org/licenses/>.

    Copyright (c) 2013, 2014 IRISA
*/


#include "Noun.h"

namespace roots { namespace linguistic { namespace pos {

Noun::Noun(const bool noInit) : POS(noInit)
{
	set_type(POS_NOUN);
	set_gender(POS_GENDER_UNKNOWN);
	set_number(POS_NUMBER_UNKNOWN);
	set_is_gerund(POS_UNKNOWN);
	set_is_proper(POS_UNKNOWN);
	set_is_collective(POS_UNKNOWN);
	set_is_countable(POS_UNKNOWN);
	set_is_compound(POS_UNKNOWN);
}

Noun::Noun(Gender g, Number n, char gerund, char proper, char collective, char countable, char compound):
	gender(g), number(n), isGerund(gerund), isProper(proper), isCollective(collective),
	isCountable(countable), isCompound(compound)
{
	set_type(POS_NOUN);
}

Noun::Noun(Gender g, Number n, char proper):
    gender(g), number(n), isGerund(POS_NOT_IS_GERUND), isProper(proper), isCollective(POS_NOT_IS_COLLECTIVE),
        isCountable(POS_NOT_IS_COUNTABLE), isCompound(POS_NOT_IS_COMPOUND)
{
	set_type(POS_NOUN);
}

Noun::Noun(const Noun &noun) : POS(noun)
{
	set_type(POS_NOUN);
	set_gender(noun.get_gender());
	set_number(noun.get_number());
	set_is_gerund(noun.get_is_gerund());
	set_is_proper(noun.get_is_proper());
	set_is_collective(noun.get_is_collective());
	set_is_countable(noun.get_is_countable());
	set_is_compound(noun.get_is_compound());
}

Noun::~Noun()
{
}

Noun *Noun::clone() const
{
	return new Noun(*this);
}

Noun& Noun::operator=(const Noun &noun)
{
	POS::operator=(noun);
	set_type(noun.get_type());
	set_gender(noun.get_gender());
	set_number(noun.get_number());
	set_is_gerund(noun.get_is_gerund());
	set_is_proper(noun.get_is_proper());
	set_is_collective(noun.get_is_collective());
	set_is_countable(noun.get_is_countable());
	set_is_compound(noun.get_is_compound());

	return *this;
}

void Noun::set_number(Number aNumber)
{
	if(numberToString.find(aNumber) == numberToString.end())
	{
		std::stringstream ss2;
		ss2 << "Unknown number: "<<aNumber<<"!\n";
		throw RootsException(__FILE__, __LINE__, ss2.str().c_str());
	}
	number = aNumber;
}

void Noun::set_gender(Gender aGender)
{
	if(genderToString.find(aGender) == genderToString.end())
	{
		std::stringstream ss2;
		ss2 << "Unknown gender: "<<aGender<<"!\n";
		throw RootsException(__FILE__, __LINE__, ss2.str().c_str());
	}
	gender = aGender;
}


std::string Noun::to_string(int level) const
{
	std::stringstream ss;
    
	switch (level)
	{
	  case LEVEL_CLASS_TREEBANK_FR:
		if(this->get_is_proper())		
			ss << "NP";
		else
			ss << "NC";
		break;
    case LEVEL_CLASS_COARSE:
        ss << "Noun";
        break;
        
    case LEVEL_CLASS_FINE:
        ss << "Noun";
        if(this->gender != POS_GENDER_UNKNOWN)
            ss << "_" << genderToString[this->gender];
        if(this->number != POS_NUMBER_UNKNOWN)
            ss << "_" << numberToString[this->number];
        break;
        
    case LEVEL_LABEL:
    default:
        ss << "Nn";
        if(this->gender != POS_GENDER_UNKNOWN) ss << genderToChar[this->gender];
        if(this->number != POS_NUMBER_UNKNOWN) ss << numberToChar[this->number];
	}
	return std::string(ss.str().c_str());
}

void Noun::deflate(RootsStream * stream, bool is_terminal, int list_index)
{
  POS::deflate(stream,false,list_index);

  stream->append_int_content("gender", this->get_gender());
  stream->append_int_content("number", this->get_number());
  stream->append_char_content("is_gerund", this->get_is_gerund());
  stream->append_char_content("is_proper", this->get_is_proper());
  stream->append_char_content("is_collective", this->get_is_collective());
  stream->append_char_content("is_countable", this->get_is_countable());
  stream->append_char_content("is_compound", this->get_is_compound());

  if(is_terminal)
	stream->close_object();
}


void Noun::inflate(RootsStream * stream, bool is_terminal, int list_index)
{
	int tmp=0;
	POS::inflate(stream,false,list_index);

	//stream->open_children();

	tmp = stream->get_int_content("gender");
	if(genderToString.find((Gender)tmp) == genderToString.end())
	  {
	    std::stringstream ss2;
	    ss2 << "Unknown gender: "<<tmp<<"!\n";
	    throw RootsException(__FILE__, __LINE__, ss2.str().c_str());
	  }
	this->set_gender((Gender)tmp);

	tmp = stream->get_int_content("number");
	if(numberToString.find((Number)tmp) == numberToString.end())
	  {
	    std::stringstream ss2;
	    ss2 << "Unknown number: "<<tmp<<"!\n";
	    throw RootsException(__FILE__, __LINE__, ss2.str().c_str());
	  }
	this->set_number((Number)tmp);


	this->set_is_gerund(stream->get_char_content("is_gerund"));
	this->set_is_proper(stream->get_char_content("is_proper"));
	this->set_is_collective(stream->get_char_content("is_collective"));
	this->set_is_countable(stream->get_char_content("is_countable"));
	this->set_is_compound(stream->get_char_content("is_compound"));

	if(is_terminal) stream->close_children();	
}

Noun * Noun::inflate_object(RootsStream * stream, int list_index)
{
	Noun *t = new Noun(true);
	t->inflate(stream,true,list_index);
	return t;	
}


} } } // END OF NAMESPACES
