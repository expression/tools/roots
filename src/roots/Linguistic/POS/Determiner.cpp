/*
    This file is part of Roots.

    Roots is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Roots is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Roots.  If not, see <http://www.gnu.org/licenses/>.

    Copyright (c) 2013, 2014 IRISA
*/


#include "Determiner.h"

namespace roots { namespace linguistic { namespace pos {

      Determiner::Determiner(const bool noInit) : POS(noInit), gender(POS_GENDER_UNKNOWN), number(POS_NUMBER_UNKNOWN)
{
	set_type(POS_DETERMINER);
	set_is_wh(false);
}

Determiner::Determiner(Gender aGender, Number aNumber) : gender(aGender), number(aNumber), isWh(false)
{
	set_type(POS_DETERMINER);
}

Determiner::Determiner(const Determiner &det) : POS(det)
{
	set_type(POS_DETERMINER);
	set_gender(det.get_gender());
	set_number(det.get_number());
	set_is_wh(det.is_wh());
}

Determiner::~Determiner()
{
}

Determiner *Determiner::clone() const
{
	return new Determiner(*this);
}

Determiner& Determiner::operator=(const Determiner &det)
{
	POS::operator=(det);
	set_type(det.get_type());
	set_gender(det.get_gender());
	set_number(det.get_number());
	set_is_wh(det.is_wh());

	return *this;
}

void Determiner::set_number(Number aNumber)
{
	if(numberToString.find(aNumber) == numberToString.end())
	{
		std::stringstream ss2;
		ss2 << "Unknown number: "<<aNumber<<"!\n";
		throw RootsException(__FILE__, __LINE__, ss2.str().c_str());
	}
	number = aNumber;
}

void Determiner::set_gender(Gender aGender)
{
	if(genderToString.find(aGender) == genderToString.end())
	{
		std::stringstream ss2;
		ss2 << "Unknown gender: "<<aGender<<"!\n";
		throw RootsException(__FILE__, __LINE__, ss2.str().c_str());
	}
	gender = aGender;
}

std::string Determiner::to_string(int level) const
{
	std::stringstream ss;

	switch (level)
	{
	  case LEVEL_CLASS_TREEBANK_FR:
		ss << "D";
		break;
    case LEVEL_CLASS_COARSE:
        ss 	<<  "Determiner";
        break;
        
    case LEVEL_CLASS_FINE:
        ss 	<<  "Determiner";
        if(this->gender != POS_GENDER_UNKNOWN)
            ss << "_" << genderToString[this->gender];
        if(this->number != POS_NUMBER_UNKNOWN)
            ss << "_" << numberToString[this->number];
	if(this->is_wh())
            ss << "_" << whToString;
        break;
        
    case LEVEL_LABEL:
    default:
        ss 	<<  "D";
        if(this->gender != POS_GENDER_UNKNOWN) ss << genderToChar[this->gender];
        if(this->number != POS_NUMBER_UNKNOWN) ss << numberToChar[this->number];
	if(this->is_wh()) ss << whToChar;
	}
	return std::string(ss.str().c_str());
}

void Determiner::deflate(RootsStream * stream, bool is_terminal, int list_index)
{
  POS::deflate(stream,false,list_index);

  stream->append_int_content("gender", this->get_gender());
  stream->append_int_content("number", this->get_number());
  if (is_wh()) { stream->append_bool_content("is_wh", true); }

  if(is_terminal)
	stream->close_object();
}


void Determiner::inflate(RootsStream * stream, bool is_terminal, int list_index)
{
	int tmp=0;
	POS::inflate(stream,false,list_index);

	//stream->open_children();

	tmp =  stream->get_int_content("gender");
	if(genderToString.find((Gender)tmp) == genderToString.end())
	  {
	    std::stringstream ss2;
	    ss2 << "Unknown gender: "<<tmp<<"!\n";
	    throw RootsException(__FILE__, __LINE__, ss2.str().c_str());
	  }
	this->set_gender((Gender)tmp);

	tmp =  stream->get_int_content("number");
	if(numberToString.find((Number)tmp) == numberToString.end())
	  {
	    std::stringstream ss2;
	    ss2 << "Unknown number: "<<tmp<<"!\n";
	    throw RootsException(__FILE__, __LINE__, ss2.str().c_str());
	  }
	this->set_number((Number)tmp);
	
	if (stream->has_child("is_wh")) { this->set_is_wh(stream->get_bool_content("is_wh")); }

	if(is_terminal) stream->close_children();	
}

Determiner * Determiner::inflate_object(RootsStream * stream, int list_index)
{
	Determiner *t = new Determiner(true);
	t->inflate(stream,true,list_index);
	return t;	
}

} } } // END OF NAMESPACES
