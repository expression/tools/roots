/*
    This file is part of Roots.

    Roots is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Roots is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Roots.  If not, see <http://www.gnu.org/licenses/>.

    Copyright (c) 2013, 2014 IRISA
*/



#include "Particle.h"

namespace roots { namespace linguistic {  namespace pos {


Particle::Particle(const bool noInit) : POS(noInit)
{
	set_type(POS_PARTICLE);
}

Particle::Particle(const Particle &part) : POS(part)
{
	set_type(POS_PARTICLE);
}

Particle::~Particle()
{
}

Particle *Particle::clone() const
{
	return new Particle(*this);
}

Particle& Particle::operator=(const Particle &inte)
{
  POS::operator=(inte);
  set_type(inte.get_type());
  return *this;
}

std::string Particle::to_string(int level) const
{
  std::stringstream ss;
  switch (level)
  {
    default:
      ss <<  "Particle";
  }
  return std::string(ss.str().c_str());
}

void Particle::deflate(RootsStream * stream, bool is_terminal, int list_index)
{
  POS::deflate(stream,false,list_index);

  if(is_terminal)
	stream->close_object();
}


void Particle::inflate(RootsStream * stream, bool is_terminal, int list_index)
{
	POS::inflate(stream,false,list_index);
	if(is_terminal) stream->close_children();
}

Particle * Particle::inflate_object(RootsStream * stream, int list_index)
{
	Particle *t = new Particle(true);
	t->inflate(stream,true,list_index);
	return t;	
}


} } } // END OF NAMESPACES
