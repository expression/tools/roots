/*
    This file is part of Roots.

    Roots is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Roots is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Roots.  If not, see <http://www.gnu.org/licenses/>.

    Copyright (c) 2013, 2014 IRISA
*/


#ifndef PRONOUN_H_
#define PRONOUN_H_

#include "../../common.h"

#include "../POS.h"

namespace roots { namespace linguistic { namespace pos {

/**
 * @brief Pronoun class
 * @details This class represents the Part Of Speech Pronoun.
 * @author Cordial Group
 * @version 0.1
 * @date 2011
 */
class Pronoun: public POS
{
public:
	Pronoun(const bool noInit = false);
	Pronoun(Category cat, Number num, Gender gen, Person per, Number nump, char inv); // , char isInterrogative);
	Pronoun(const Pronoun &pro);
	virtual ~Pronoun();
	/**
	 * @brief clone the current object
	 * @return a copy of the object
	 */
	virtual Pronoun *clone() const;

	Pronoun& operator=(const Pronoun &pro);


	void set_category(Category cat);
	void set_number(Number aNumber);
	void set_gender(Gender aGender);
	void set_person(Person aPerson);
	void set_person_number(Number aPerson);
	void set_is_invariable(char b) {isInvariable = b;}
	void set_is_wh(bool b) {isWh = b;}
	// void set_is_interrogative(char b) {isInterrogative = b;}

	Category get_category() const {return category;}
	Number get_number() const {return number;}
	Gender get_gender() const {return gender;}
	Person get_person() const {return person;}
	Number get_person_number() const {return personNumber;}
	char get_is_invariable() const {return isInvariable;}
	// char get_is_interrogative() const {return isInterrogative;}

	bool is_pronoun() const { return true; }
	bool is_unknown() const { return false; }

	bool is_invariable() const
	{
		return isInvariable == POS_IS_INVARIABLE;
	}

	bool is_personal() const
	{
		return category == POS_CATEGORY_PERSONAL;
	}

	bool is_demonstrative() const
	{
		return category == POS_CATEGORY_DEMONSTRATIVE;
	}

	bool is_numeral() const
	{
		return category == POS_CATEGORY_NUMERAL;
	}

	bool is_interrogative() const
	{
		return category == POS_CATEGORY_INTERROGATIVE;
	}

	bool is_exclamative() const
	{
		return category == POS_CATEGORY_EXCLAMATIVE;
	}

	bool is_relative() const
	{
		return category == POS_CATEGORY_RELATIVE;
	}

	bool is_reflexive() const
	{
		return category == POS_CATEGORY_REFLEXIVE;
	}

	bool is_indefinite() const
	{
		return category == POS_CATEGORY_INDEFINITE;
	}

	bool is_possessive() const
	{
		return category == POS_CATEGORY_POSSESSIVE;
	}

	bool is_male() const
	{
		return gender == POS_GENDER_MALE;
	}

	bool is_female() const
	{
		return gender == POS_GENDER_FEMALE;
	}

	bool is_common() const
	{
		return gender == POS_GENDER_COMMON;
	}

	bool is_neutral() const
	{
		return gender == POS_GENDER_NEUTRAL;
	}

	bool is_singular() const
	{
		return number == POS_NUMBER_SINGULAR;
	}

	bool is_plural() const
	{
		return number == POS_NUMBER_PLURAL;
	}

	bool is_first_person() const
	{
		return person == POS_PERSON_FIRST;
	}

	bool is_second_person() const
	{
		return person == POS_PERSON_SECOND;
	}

	bool is_third_person() const
	{
		return person == POS_PERSON_THIRD;
	}

	bool is_first_person_singular() const
	{
		return personNumber == POS_NUMBER_SINGULAR;
	}

	bool is_first_person_plural() const
	{
		return personNumber == POS_NUMBER_PLURAL;
	}

	bool is_second_person_singular() const
	{
		return personNumber == POS_NUMBER_SINGULAR;
	}

	bool is_second_person_plural() const
	{
		return personNumber == POS_NUMBER_PLURAL;
	}

	bool is_third_person_singular() const
	{
		return personNumber == POS_NUMBER_SINGULAR;
	}

	bool is_third_person_plural() const
	{
		return personNumber == POS_NUMBER_PLURAL;
	}
	
	bool is_wh() const
	{
	  return isWh;
	}

	/**
	 * @brief returns a string representation of any Roots object
	 * @param level the precision level of the output
	 * @return string representation of the object
	 */
	virtual std::string to_string(int level=0) const;
	/**
	 * @brief Write the entity into a RootsStream
	 * @param stream the stream from which we inflate the element
	 * @param is_terminal indicates that the node is terminal (true by default)
	 */
	virtual void deflate(RootsStream * stream, bool is_terminal=true, int list_index=-1);
	/**
	 * @brief Extracts the entity from the stream
	 * @param stream the stream from which we inflate the element
	 */
	virtual void inflate(RootsStream * stream, bool is_terminal=true, int list_index=-1);
	/**
	 * @brief Extracts the entity from the stream
	 * @param stream the stream from which we inflate the element
	 */
	static Pronoun * inflate_object(RootsStream * stream, int list_index=-1);
	/**
	 * @brief returns classname for current object
	 * @return string constant representing the classname
	 */
	virtual std::string get_classname() const { return classname(); };
	/**
	 * @brief returns classname for current class
	 * @return string constant representing the classname
	 */
	static std::string classname() { return "Linguistic::POS::Pronoun"; };

private:
	Category category;
	Gender gender;
	Number number;
	Person person;
	Number personNumber;
	char isInvariable;
	bool isWh;
	// char isInterrogative;

};


} } }// END OF NAMESPACES

#endif /* PRONOUN_H_ */
