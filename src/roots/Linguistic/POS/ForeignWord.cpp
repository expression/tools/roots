/*
    This file is part of Roots.

    Roots is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Roots is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Roots.  If not, see <http://www.gnu.org/licenses/>.

    Copyright (c) 2013, 2014 IRISA
*/



#include "ForeignWord.h"

namespace roots { namespace linguistic {  namespace pos {


ForeignWord::ForeignWord(const bool noInit) : POS(noInit)
{
	set_type(POS_FOREIGN_WORD);
}

ForeignWord::ForeignWord(const ForeignWord &inter) : POS(inter)
{
	set_type(POS_FOREIGN_WORD);
}

ForeignWord::~ForeignWord()
{
}

ForeignWord *ForeignWord::clone() const
{
	return new ForeignWord(*this);
}

ForeignWord& ForeignWord::operator=(const ForeignWord &inte)
{
	POS::operator=(inte);
	set_type(inte.get_type());

	return *this;
}

std::string ForeignWord::to_string(int level) const
{
	std::stringstream ss;

	switch (level)
	{
	  case LEVEL_CLASS_TREEBANK_FR:
		ss << "ET";
		break;
    case LEVEL_CLASS_COARSE:
        ss << "ForeignWord";
        break;
        
    case LEVEL_CLASS_FINE:
        ss << "ForeignWord";
        break;
        
    case LEVEL_LABEL:
    default:
        ss 	<<  "Fw";
	}
	return std::string(ss.str().c_str());
}

void ForeignWord::deflate(RootsStream * stream, bool is_terminal, int list_index)
{
  POS::deflate(stream,false,list_index);

  if(is_terminal)
	stream->close_object();
}


void ForeignWord::inflate(RootsStream * stream, bool is_terminal, int list_index)
{
	POS::inflate(stream,false,list_index);
	if(is_terminal) stream->close_children();
}

ForeignWord * ForeignWord::inflate_object(RootsStream * stream, int list_index)
{
	ForeignWord *t = new ForeignWord(true);
	t->inflate(stream,true,list_index);
	return t;	
}


} } } // END OF NAMESPACES
