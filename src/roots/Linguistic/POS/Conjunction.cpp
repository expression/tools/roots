/*
    This file is part of Roots.

    Roots is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Roots is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Roots.  If not, see <http://www.gnu.org/licenses/>.

    Copyright (c) 2013, 2014 IRISA
*/


#include "Conjunction.h"

namespace roots { namespace linguistic { namespace pos {

Conjunction::Conjunction(const bool noInit) : POS(noInit)
{
	set_type(POS_CONJUNCTION);
	set_category(POS_CONJUNCTION_CATEGORY_UNKNOWN);
}

Conjunction::Conjunction(ConjunctionCategory cat) : category(cat)
{
  set_type(POS_CONJUNCTION);
}

Conjunction::Conjunction(const Conjunction &conj) : POS(conj)
{
	set_type(POS_CONJUNCTION);
	set_category(conj.get_category());
}

Conjunction::~Conjunction()
{
}

Conjunction *Conjunction::clone() const
{
	return new Conjunction(*this);
}

Conjunction& Conjunction::operator=(const Conjunction &conj)
{
	POS::operator=(conj);
	set_type(conj.get_type());
	set_category(conj.get_category());

	return *this;
}

void Conjunction::set_category(ConjunctionCategory cat)
{
	if(conjunctionCategoryToString.find(cat) == conjunctionCategoryToString.end())
	{
		std::stringstream ss2;
		ss2 << "Unknown category: "<<cat<<"!\n";
		throw RootsException(__FILE__, __LINE__, ss2.str().c_str());
	}

	category = cat;
}

std::string Conjunction::to_string(int level) const
{
	std::stringstream ss;
    
	switch (level)
	{
	  case LEVEL_CLASS_TREEBANK_FR:
		if(this->category == POS_CONJUNCTION_CATEGORY_COORDINATING)
			ss << "CC";
		else
			ss << "CS";
		break;
    case LEVEL_CLASS_COARSE:
        ss << "Conjunction";
        break;
        
    case LEVEL_CLASS_FINE:
        ss << "Conjunction";
        if(this->category != POS_CONJUNCTION_CATEGORY_UNKNOWN)
            ss << "_" << conjunctionCategoryToString[this->category];
        break;
        
    case LEVEL_LABEL:
    default:
        ss << "Cj";
        if(this->category != POS_CONJUNCTION_CATEGORY_UNKNOWN) ss << conjunctionCategoryToChar[this->category];
	}
	return std::string(ss.str().c_str());
}

void Conjunction::deflate(RootsStream * stream, bool is_terminal, int list_index)
{
  POS::deflate(stream,false,list_index);

  stream->append_int_content("category", this->get_category());

  if(is_terminal)
	stream->close_object();
}

void Conjunction::inflate(RootsStream * stream, bool is_terminal, int list_index)
{
	int tmp=0;
	POS::inflate(stream,false,list_index);

	//stream->open_children();

	tmp =  stream->get_int_content("category");
	if(conjunctionCategoryToString.find((ConjunctionCategory)tmp) == conjunctionCategoryToString.end())
	  {
	    std::stringstream ss2;
	    ss2 << "Unknown conjunction category: "<<tmp<<"!\n";
	    throw RootsException(__FILE__, __LINE__, ss2.str().c_str());
	  }
	this->set_category((ConjunctionCategory)tmp);

	if(is_terminal) stream->close_children();	
}

Conjunction * Conjunction::inflate_object(RootsStream * stream, int list_index)
{
	Conjunction *t = new Conjunction(true);
	t->inflate(stream,true,list_index);
	return t;	
}

} } } // END OF NAMESPACES
