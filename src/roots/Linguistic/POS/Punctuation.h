/*
    This file is part of Roots.

    Roots is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Roots is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Roots.  If not, see <http://www.gnu.org/licenses/>.

    Copyright (c) 2013, 2014 IRISA
*/


#ifndef PUNCTUATION_H_
#define PUNCTUATION_H_

#include "../../common.h"
#include "../POS.h"

namespace roots { namespace linguistic { namespace pos {

/**
 * @brief Punctuation class
 * @details This class represents the Part Of Speech Punctuation.
 * @author Cordial Group
 * @version 0.1
 * @date 2011
 */
class Punctuation: public POS
{
public:
	Punctuation(const bool noInit = false);
	Punctuation(PunctuationCategory cat, char closing);
	Punctuation(const Punctuation &punc);
	virtual ~Punctuation();
	/**
	 * @brief clone the current object
	 * @return a copy of the object
	 */
	virtual Punctuation *clone() const;

	Punctuation& operator=(const Punctuation &pun);


	void set_category(PunctuationCategory cat);
	void set_is_closing(char b) {isClosing = b;}

	PunctuationCategory get_category() const {return category;}
	char get_is_closing() const {return isClosing;}

	bool is_punctuation() const { return true; }
	bool is_unknown() const { return false; }

	bool is_closing() const
	{
		return get_is_closing() == POS_IS_CLOSING;
	}

	bool is_final() const
	{
		return get_category() == POS_PUNCTUATION_CATEGORY_FINAL;
	}

	bool is_pause() const
	{
		return get_category() == POS_PUNCTUATION_CATEGORY_PAUSE;
	}

	bool is_insert() const
	{
		return get_category() == POS_PUNCTUATION_CATEGORY_INSERT;
	}

	bool is_unknown_category() const
	{
		return get_category() == POS_PUNCTUATION_CATEGORY_UNKNOWN;
	}

	/**
	 * @brief returns a string representation of any Roots object
	 * @param level the precision level of the output
	 * @return string representation of the object
	 */
	virtual std::string to_string(int level=0) const;
	/**
	 * @brief Write the entity into a RootsStream
	 * @param stream the stream from which we inflate the element
	 * @param is_terminal indicates that the node is terminal (true by default)
	 */
	virtual void deflate(RootsStream * stream, bool is_terminal=true, int list_index=-1);
	/**
	 * @brief Extracts the entity from the stream
	 * @param stream the stream from which we inflate the element
	 */
	virtual void inflate(RootsStream * stream, bool is_terminal=true, int list_index=-1);
	/**
	 * @brief Extracts the entity from the stream
	 * @param stream the stream from which we inflate the element
	 */
	static Punctuation * inflate_object(RootsStream * stream, int list_index=-1);
	/**
	 * @brief returns classname for current object
	 * @return string constant representing the classname
	 */
	virtual std::string get_classname() const { return classname(); };
	/**
	 * @brief returns classname for current class
	 * @return string constant representing the classname
	 */
	static std::string classname() { return "Linguistic::POS::Punctuation"; };

private:
	PunctuationCategory category;
	char isClosing;

};

} } }// END OF NAMESPACES


#endif /* PUNCTUATION_H_ */
