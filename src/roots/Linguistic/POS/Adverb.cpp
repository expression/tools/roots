/*
    This file is part of Roots.

    Roots is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Roots is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Roots.  If not, see <http://www.gnu.org/licenses/>.

    Copyright (c) 2013, 2014 IRISA
*/


#include "Adverb.h"

namespace roots { namespace linguistic { namespace pos {

      Adverb::Adverb(const bool noInit) : POS(noInit)
{
	set_type(POS_ADVERB);
	set_degree(POS_DEGREE_UNKNOWN);
	set_is_wh(false);
}

Adverb::Adverb(Degree d) : degree(d), isWh(false)
{
	set_type(POS_ADVERB);
}

Adverb::Adverb(const Adverb &adverb) : POS(adverb)
{
	set_type(POS_ADVERB);
	set_degree(adverb.get_degree());
	set_is_wh(adverb.is_wh());
}

Adverb::~Adverb()
{
}

Adverb *Adverb::clone() const
{
	return new Adverb(*this);
}

Adverb& Adverb::operator=(const Adverb &adv)
{
	POS::operator=(adv);
	set_type(adv.get_type());
	set_degree(adv.get_degree());
	set_is_wh(adv.is_wh());

	return *this;
}

void Adverb::set_degree(Degree aDegree)
{
	if(degreeToString.find(aDegree) == degreeToString.end())
	{
		std::stringstream ss2;
		ss2 << "Unknown degree: "<<aDegree<<"!\n";
		throw RootsException(__FILE__, __LINE__, ss2.str().c_str());
	}
	degree = aDegree;
}

std::string Adverb::to_string(int level) const
{
	std::stringstream ss;
    
	switch (level)
	{
	  case LEVEL_CLASS_TREEBANK_FR:
		ss << "Adv";
		break;
    case LEVEL_CLASS_COARSE:
        ss << "Adverb";
        break;
        
    case LEVEL_CLASS_FINE:
        ss << "Adverb";
        if(this->degree != POS_DEGREE_UNKNOWN)
            ss << "_" << degreeToString[this->get_degree()];
	if(this->is_wh())
            ss << "_" << whToString;
        break;
        
    case LEVEL_LABEL:
    default:
        ss << "Av";
        if(this->degree != POS_DEGREE_UNKNOWN)
            ss << degreeToChar[this->degree];
	if(this->is_wh())
	    ss << whToChar;
	}
	return std::string(ss.str().c_str());
}

void Adverb::deflate(RootsStream * stream, bool is_terminal, int list_index)
{
  POS::deflate(stream,false,list_index);

  stream->append_int_content("degree", this->get_degree());
  if (is_wh()) { stream->append_bool_content("is_wh", true); }


  if(is_terminal)
	stream->close_object();
}

void Adverb::inflate(RootsStream * stream, bool is_terminal, int list_index)
{
	int tmp=0;
	POS::inflate(stream,false,list_index);

	//stream->open_children();

	tmp =  stream->get_int_content("degree");
	if(degreeToString.find((Degree)tmp) == degreeToString.end())
	  {
	    std::stringstream ss2;
	    ss2 << "Unknown degree: "<<tmp<<"!\n";
	    throw RootsException(__FILE__, __LINE__, ss2.str().c_str());
	  }
	this->set_degree((Degree)tmp);
	
	if (stream->has_child("is_wh")) { this->set_is_wh(stream->get_bool_content("is_wh")); }

	if(is_terminal) stream->close_children();	
}

Adverb * Adverb::inflate_object(RootsStream * stream, int list_index)
{
	Adverb *t = new Adverb(true);
	t->inflate(stream,true,list_index);
	return t;	
}

} } } // END OF NAMESPACES
