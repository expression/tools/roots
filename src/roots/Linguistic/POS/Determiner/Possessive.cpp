/*
    This file is part of Roots.

    Roots is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Roots is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Roots.  If not, see <http://www.gnu.org/licenses/>.

    Copyright (c) 2013, 2014 IRISA
*/


#include "Possessive.h"

namespace roots { namespace linguistic { namespace pos {

Possessive::Possessive(const bool noInit) : Determiner(noInit)
{
	set_type(POS_POSSESSIVE);
	set_person(POS_PERSON_UNKNOWN);
	set_person_number(POS_NUMBER_UNKNOWN);
}
 
Possessive::Possessive(Gender aGender, Number aNumber, Person aPerson, Number aPersonNumber) :
	Determiner(aGender, aNumber), person(aPerson), personNumber(aPersonNumber)
{
	set_type(POS_POSSESSIVE);
}

Possessive::Possessive(const Possessive &pos) :
		Determiner(pos),
		person(pos.person), personNumber(pos.personNumber)
{
	set_type(POS_POSSESSIVE);
}

Possessive::~Possessive()
{
}

Possessive *Possessive::clone() const
{
	return new Possessive(*this);
}

Possessive& Possessive::operator=(const Possessive &pos)
{
	Determiner::operator=(pos);
	set_type(pos.get_type());
	set_person(pos.get_person());
	set_person_number(pos.get_person_number());

	return *this;
}

void Possessive::set_person(Person aPerson)
{
	if(personToString.find(aPerson) == personToString.end())
	{
		std::stringstream ss2;
		ss2 << "Unknown person: "<<aPerson <<"!\n";
		throw RootsException(__FILE__, __LINE__, ss2.str().c_str());
	}
	person = aPerson;
}

void Possessive::set_person_number(Number aPerson)
{
	if(numberToString.find(aPerson) == numberToString.end())
	{
		std::stringstream ss2;
		ss2 << "Unknown number: "<<aPerson <<"!\n";
		throw RootsException(__FILE__, __LINE__, ss2.str().c_str());
	}
	personNumber = aPerson;
}
std::string Possessive::to_string(int level) const
{
	std::stringstream ss;
    ss << Determiner::to_string(level);

	switch (level)
	{
	  case LEVEL_CLASS_TREEBANK_FR:
		break;
    case LEVEL_CLASS_COARSE:
        break;
        
    case LEVEL_CLASS_FINE:
        ss << "_Possessive";
        break;
        
    case LEVEL_LABEL:
    default:
        ss 	<< "p";
	}
	return std::string(ss.str().c_str());
}


void Possessive::deflate(RootsStream * stream, bool is_terminal, int list_index)
{
  Determiner::deflate(stream,false,list_index);

  stream->append_int_content("person", this->get_person());
  stream->append_int_content("person_number", this->get_person_number());

  if(is_terminal)
	stream->close_object();
}


void Possessive::inflate(RootsStream * stream, bool is_terminal, int list_index)
{
	int tmp = 0;
	Determiner::inflate(stream,false,list_index);

	//stream->open_children();

	tmp = stream->get_int_content("person");
	if(personToString.find((Person)tmp) == personToString.end())
	  {
	    std::stringstream ss2;
	    ss2 << "Unknown person: "<<tmp<<"!\n";
	    throw RootsException(__FILE__, __LINE__, ss2.str().c_str());
	  }
	this->set_person((Person)tmp);

	tmp = stream->get_int_content("person_number");
	if(numberToString.find((Number)tmp) == numberToString.end())
	  {
	    std::stringstream ss2;
	    ss2 << "Unknown person_number: "<<tmp<<"!\n";
	    throw RootsException(__FILE__, __LINE__, ss2.str().c_str());
	  }
	this->set_person_number((Number)tmp);

	if(is_terminal) stream->close_children();
}

Possessive * Possessive::inflate_object(RootsStream * stream, int list_index)
{
	Possessive *t = new Possessive(true);
	t->inflate(stream,true,list_index);
	return t;	
}


} } } // END OF NAMESPACES
