/*
    This file is part of Roots.

    Roots is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Roots is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Roots.  If not, see <http://www.gnu.org/licenses/>.

    Copyright (c) 2013, 2014 IRISA
*/


#include "Numeral.h"

namespace roots { namespace linguistic { namespace pos {

Numeral::Numeral(const bool noInit) : Determiner(noInit)
{
	set_type(POS_NUMERAL);
}

Numeral::Numeral(Gender aGender, Number aNumber, char ord, char card) :
	Determiner(aGender, aNumber), isOrdinal(ord), isCardinal(card)
{
	set_type(POS_NUMERAL);
	set_is_ordinal(POS_UNKNOWN);
	set_is_cardinal(POS_UNKNOWN);
}

Numeral::Numeral(const Numeral &num) :
		Determiner(num),
		isOrdinal(num.isOrdinal), isCardinal(num.isCardinal)
{
	set_type(POS_NUMERAL);
}

Numeral::~Numeral()
{
}

Numeral *Numeral::clone() const
{
	return new Numeral(*this);
}

Numeral& Numeral::operator=(const Numeral &num)
{
	Determiner::operator=(num);
	set_type(num.get_type());
	set_is_ordinal(num.get_is_ordinal());
	set_is_cardinal(num.get_is_cardinal());

	return *this;
}

std::string Numeral::to_string(int level) const
{
	std::stringstream ss;
    
    ss << Determiner::to_string(level);

	switch (level)
	{
	  case LEVEL_CLASS_TREEBANK_FR:
		break;
    case LEVEL_CLASS_COARSE:
        break;
        
    case LEVEL_CLASS_FINE:
        ss << "Numeral";
        break;
        
    case LEVEL_LABEL:
    default:
        ss 	<< "n";
	}
	return std::string(ss.str().c_str());
}


void Numeral::deflate(RootsStream * stream, bool is_terminal, int list_index)
{
  Determiner::deflate(stream,false,list_index);

  stream->append_char_content("is_ordinal", this->get_is_ordinal());
  stream->append_char_content("is_cardinal", this->get_is_cardinal());

  if(is_terminal)
	stream->close_object();
}


void Numeral::inflate(RootsStream * stream, bool is_terminal, int list_index)
{
	Determiner::inflate(stream,false,list_index);

	//stream->open_children();
	this->set_is_ordinal(stream->get_char_content("is_ordinal"));
	this->set_is_cardinal(stream->get_char_content("is_cardinal"));
	if(is_terminal) stream->close_children();
}

Numeral * Numeral::inflate_object(RootsStream * stream, int list_index)
{
	Numeral *t = new Numeral(true);
	t->inflate(stream,true,list_index);
	return t;	
}

} } } // END OF NAMESPACES
