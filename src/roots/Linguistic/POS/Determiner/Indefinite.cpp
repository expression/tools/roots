/*
    This file is part of Roots.

    Roots is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Roots is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Roots.  If not, see <http://www.gnu.org/licenses/>.

    Copyright (c) 2013, 2014 IRISA
*/


#include "Indefinite.h"

namespace roots { namespace linguistic { namespace pos {

Indefinite::Indefinite(const bool noInit) : Determiner(noInit)
{
	set_type(POS_INDEFINITE);
}

Indefinite::Indefinite(Gender aGender, Number aNumber): Determiner(aGender, aNumber)
{
	set_type(POS_INDEFINITE);
}

Indefinite::Indefinite(const Indefinite &ind) : Determiner(ind)
{
	set_type(POS_INDEFINITE);
}

Indefinite::~Indefinite()
{
}

Indefinite *Indefinite::clone() const
{
	return new Indefinite(*this);
}

Indefinite& Indefinite::operator=(const Indefinite &ind)
{
	Determiner::operator=(ind);
	set_type(ind.get_type());

	return *this;
}

std::string Indefinite::to_string(int level) const
{
	std::stringstream ss;

    ss << Determiner::to_string(level);
    
	switch (level)
	{
	  case LEVEL_CLASS_TREEBANK_FR:
		break;
    case LEVEL_CLASS_COARSE:
        break;
        
    case LEVEL_CLASS_FINE:
        ss << "_Indefinite";
        break;
        
    case LEVEL_LABEL:
    default:
        ss 	<< "i";
	}
	return std::string(ss.str().c_str());
}


void Indefinite::deflate(RootsStream * stream, bool is_terminal, int list_index)
{
  Determiner::deflate(stream,false,list_index);

  if(is_terminal)
	stream->close_object();
}



void Indefinite::inflate(RootsStream * stream, bool is_terminal, int list_index)
{
	Determiner::inflate(stream,false,list_index);	
	if(is_terminal) stream->close_children();
}

Indefinite * Indefinite::inflate_object(RootsStream * stream, int list_index)
{
	Indefinite *t = new Indefinite(true);
	t->inflate(stream,true,list_index);
	return t;	
}

} } } // END OF NAMESPACES
