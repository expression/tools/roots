/*
    This file is part of Roots.

    Roots is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Roots is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Roots.  If not, see <http://www.gnu.org/licenses/>.

    Copyright (c) 2013, 2014 IRISA
*/


#ifndef NUMERAL_H_
#define NUMERAL_H_

#include "../../../common.h"
#include "../Determiner.h"

namespace roots { namespace linguistic { namespace pos {

/**
 * @brief Numeral class
 * @details This class represents the Part Of Speech Numeral.
 * @author Cordial Group
 * @version 0.1
 * @date 2011
 */
class Numeral: public Determiner
{
public:
	Numeral(const bool noInit = false);
	Numeral(Gender aGender, Number aNumber, char ord, char card);
	Numeral(const Numeral &num);
	virtual ~Numeral();
	/**
	 * @brief clone the current object
	 * @return a copy of the object
	 */
	virtual Numeral *clone() const;

	Numeral& operator=(const Numeral &num);

	void set_is_ordinal(char b) {isOrdinal = b;}
	void set_is_cardinal(char b) {isCardinal = b;}

	char get_is_ordinal() const {return isOrdinal;}
	char get_is_cardinal() const {return isCardinal;}

	bool is_numeral() const { return true; }

	bool is_ordinal() const
	{
		return get_is_ordinal() == POS_IS_ORDINAL;
	}

	bool is_cardinal() const
	{
		return get_is_cardinal() == POS_IS_CARDINAL;
	}

	/**
	 * @brief returns a string representation of any Roots object
	 * @param level the precision level of the output
	 * @return string representation of the object
	 */
	virtual std::string to_string(int level=0) const;
	/**
	 * @brief Write the entity into a RootsStream
	 * @param stream the stream from which we inflate the element
	 * @param is_terminal indicates that the node is terminal (true by default)
	 */
	virtual void deflate(RootsStream * stream, bool is_terminal=true, int list_index=-1);
	/**
	 * @brief Extracts the entity from the stream
	 * @param stream the stream from which we inflate the element
	 */
	virtual void inflate(RootsStream * stream, bool is_terminal=true, int list_index=-1);
	/**
	 * @brief Extracts the entity from the stream
	 * @param stream the stream from which we inflate the element
	 */
	static Numeral * inflate_object(RootsStream * stream, int list_index=-1);
	/**
	 * @brief returns classname for current object
	 * @return string constant representing the classname
	 */
	virtual std::string get_classname() const { return classname(); };
	/**
	 * @brief returns classname for current class
	 * @return string constant representing the classname
	 */
	static std::string classname() { return "Linguistic::POS::Determiner::Numeral"; };

private:
	char isOrdinal;
	char isCardinal;

};

} } }// END OF NAMESPACES

#endif /* NUMERAL_H_ */
