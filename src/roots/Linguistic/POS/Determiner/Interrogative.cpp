/*
    This file is part of Roots.

    Roots is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Roots is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Roots.  If not, see <http://www.gnu.org/licenses/>.

    Copyright (c) 2013, 2014 IRISA
*/


#include "Interrogative.h"

namespace roots { namespace linguistic { namespace pos {

      Interrogative::Interrogative(const bool noInit) : Determiner(noInit)
      {
	set_type(POS_INTERROGATIVE);
      }

Interrogative::Interrogative(Gender aGender, Number aNumber): Determiner(aGender, aNumber)
{
	set_type(POS_INTERROGATIVE);
}

Interrogative::Interrogative(const Interrogative &inte) : Determiner(inte)
{
	set_type(POS_INTERROGATIVE);
}

Interrogative::~Interrogative()
{
}

Interrogative *Interrogative::clone() const
{
	return new Interrogative(*this);
}

Interrogative& Interrogative::operator=(const Interrogative &inte)
{
	Determiner::operator=(inte);
	set_type(inte.get_type());

	return *this;
}

std::string Interrogative::to_string(int level) const
{
	std::stringstream ss;
    
    ss << Determiner::to_string(level);

	switch (level)
	{
	  case LEVEL_CLASS_TREEBANK_FR:
		break;
    case LEVEL_CLASS_COARSE:
        break;
        
    case LEVEL_CLASS_FINE:
        ss << "_Interrogative";
        break;
        
    case LEVEL_LABEL:
    default:
        ss 	<< "g";
	}
	return std::string(ss.str().c_str());
}


void Interrogative::deflate(RootsStream * stream, bool is_terminal, int list_index)
{
  Determiner::deflate(stream,false,list_index);

  if(is_terminal)
	stream->close_object();
}


void Interrogative::inflate(RootsStream * stream, bool is_terminal, int list_index)
{
	Determiner::inflate(stream,false,list_index);
	if(is_terminal) stream->close_children();
}

Interrogative * Interrogative::inflate_object(RootsStream * stream, int list_index)
{
	Interrogative *t = new Interrogative(true);
	t->inflate(stream,true,list_index);
	return t;	
}


} } } // END OF NAMESPACES
