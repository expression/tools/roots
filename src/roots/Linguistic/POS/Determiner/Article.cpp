/*
    This file is part of Roots.

    Roots is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Roots is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Roots.  If not, see <http://www.gnu.org/licenses/>.

    Copyright (c) 2013, 2014 IRISA
*/


#include "Article.h"

namespace roots { namespace linguistic {  namespace pos {

Article::Article(const bool noInit) : Determiner(noInit)
{
	set_type(POS_ARTICLE);
	set_is_definite(POS_UNKNOWN);
	set_is_partitive(POS_UNKNOWN);
}

Article::Article(Gender aGender, Number aNumber, char definite, char partitive):
	Determiner(aGender, aNumber), isDefinite(definite), isPartitive(partitive)
{
	set_type(POS_ARTICLE);
}

Article::Article(const Article &art) : Determiner(art)
{
	set_type(POS_ARTICLE);
	set_is_definite(art.get_is_definite());
	set_is_partitive(art.get_is_partitive());
}

Article::~Article()
{
}

Article *Article::clone() const
{
	return new Article(*this);
}

Article& Article::operator=(const Article &art)
{
	Determiner::operator=(art);
	set_type(art.get_type());
	set_is_definite(art.get_is_definite());
	set_is_partitive(art.get_is_partitive());

	return *this;
}

std::string Article::to_string(int level) const
{
	std::stringstream ss;

    ss << Determiner::to_string(level);
    
	switch (level)
	{
	  case LEVEL_CLASS_TREEBANK_FR:
		break;
    case LEVEL_CLASS_COARSE:
        break;
        
    case LEVEL_CLASS_FINE:
        ss << "_Article";
        break;
        
    case LEVEL_LABEL:
    default:
        ss 	<< "a";
	}
	return std::string(ss.str().c_str());
}


void Article::deflate(RootsStream * stream, bool is_terminal, int list_index)
{
  Determiner::deflate(stream,false,list_index);

  stream->append_char_content("is_definite", this->get_is_definite());
  stream->append_char_content("is_partitive", this->get_is_partitive());

  if(is_terminal)
	stream->close_object();
}


void Article::inflate(RootsStream * stream, bool is_terminal, int list_index)
{
	Determiner::inflate(stream,false,list_index);

	//stream->open_children();

	this->set_is_definite(stream->get_char_content("is_definite"));
	this->set_is_partitive(stream->get_char_content("is_partitive"));

	if(is_terminal) stream->close_children();	
}

Article * Article::inflate_object(RootsStream * stream, int list_index)
{
	Article *t = new Article(true);
	t->inflate(stream,true,list_index);
	return t;	
}


} } } // END OF NAMESPACES
