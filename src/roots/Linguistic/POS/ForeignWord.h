/*
    This file is part of Roots.

    Roots is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Roots is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Roots.  If not, see <http://www.gnu.org/licenses/>.

    Copyright (c) 2013, 2014 IRISA
*/


#ifndef POS_FOREIGN_WORD_H_
#define POS_FOREIGN_WORD_H_

#include "../../common.h"
#include "../POS.h"

namespace roots
{
namespace linguistic
{
namespace pos
{

/**
 * @brief ForeignWord class
 * @details This class represents the Part Of Speech ForeignWord.
 * @author Cordial Group
 * @version 0.1
 * @date 2011
 */
class ForeignWord : public POS
{
public:
	ForeignWord(const bool noInit = false);
	ForeignWord(const ForeignWord &inter);
	virtual ~ForeignWord();
	/**
	 * @brief clone the current object
	 * @return a copy of the object
	 */
	virtual ForeignWord *clone() const;

	ForeignWord& operator=(const ForeignWord &inte);

	bool is_foreign_word() const { return true; }
	bool is_unknown() const { return false; }

	/**
	 * @brief returns a string representation of any Roots object
	 * @param level the precision level of the output
	 * @return string representation of the object
	 */
	virtual std::string to_string(int level = 0) const;
	/**
	 * @brief Write the entity into a RootsStream
	 * @param stream the stream from which we inflate the element
	 * @param is_terminal indicates that the node is terminal (true by default)
	 */
	virtual void deflate(RootsStream * stream, bool is_terminal=true, int list_index=-1);
   	/**
	 * @brief Extracts the entity from the stream
	 * @param stream the stream from which we inflate the element
	 */
	virtual void inflate(RootsStream * stream, bool is_terminal=true, int list_index=-1);
	/**
	 * @brief Extracts the entity from the stream
	 * @param stream the stream from which we inflate the element
	 */
	static ForeignWord * inflate_object(RootsStream * stream, int list_index=-1);

	/**
	 * @brief returns classname for current object
	 * @return string constant representing the classname
	 */
	virtual std::string get_classname() const
	{
		return classname();
	};

	/**
	 * @brief returns classname for current class
	 * @return string constant representing the classname
	 */
	static std::string classname()
	{
		return "Linguistic::POS::ForeignWord";
	};

};

}
}
}// END OF NAMESPACES

#endif /* INTERJECTION_H_ */
