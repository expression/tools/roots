/*
    This file is part of Roots.

    Roots is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Roots is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Roots.  If not, see <http://www.gnu.org/licenses/>.

    Copyright (c) 2013, 2014 IRISA
*/


#include "Pronoun.h"

namespace roots { namespace linguistic { namespace pos {

Pronoun::Pronoun(const bool noInit) : POS(noInit)
{
	set_type(POS_PRONOUN);
	set_category(POS_CATEGORY_UNKNOWN);
	set_gender(POS_GENDER_UNKNOWN);
	set_number(POS_NUMBER_UNKNOWN);
	set_person(POS_PERSON_UNKNOWN);
	set_person_number(POS_NUMBER_UNKNOWN);
	set_is_invariable(POS_UNKNOWN);
	set_is_wh(false);
}

Pronoun::Pronoun(Category cat, Number num, Gender gen, Person per, Number nump, char inv): // , char inte):
	category(cat), gender(gen), number(num), person(per), personNumber(nump),
	isInvariable(inv), isWh(false) // , isInterrogative(inte)
{
	set_type(POS_PRONOUN);
}

Pronoun::Pronoun(const Pronoun &pro) : POS(pro)
{
	set_type(POS_PRONOUN);
	set_category(pro.get_category());
	set_gender(pro.get_gender());
	set_number(pro.get_number());
	set_person(pro.get_person());
	set_person_number(pro.get_person_number());
	set_is_invariable(pro.get_is_invariable());
	set_is_wh(pro.is_wh());
	// set_is_interrogative(pro.get_is_interrogative());
}

Pronoun::~Pronoun()
{
}

Pronoun *Pronoun::clone() const
{
	return new Pronoun(*this);
}

Pronoun& Pronoun::operator=(const Pronoun &pro)
{
	POS::operator=(pro);
	set_type(pro.get_type());
	set_category(pro.get_category());
	set_gender(pro.get_gender());
	set_number(pro.get_number());
	set_person(pro.get_person());
	set_person_number(pro.get_person_number());
	set_is_invariable(pro.get_is_invariable());
	set_is_wh(pro.is_wh());
	// set_is_interrogative(pro.get_is_interrogative());

	return *this;
}

void Pronoun::set_category(Category cat)
{
	if(categoryToString.find(cat) == categoryToString.end())
	{
		std::stringstream ss2;
		ss2 << "Unknown category: "<<cat <<"!\n";
		throw RootsException(__FILE__, __LINE__, ss2.str().c_str());
	}
	category = cat;
}

void Pronoun::set_number(Number aNumber)
{
	if(numberToString.find(aNumber) == numberToString.end())
	{
		std::stringstream ss2;
		ss2 << "Unknown number: "<<aNumber<<"!\n";
		throw RootsException(__FILE__, __LINE__, ss2.str().c_str());
	}
	number = aNumber;
}

void Pronoun::set_gender(Gender aGender)
{
	if(genderToString.find(aGender) == genderToString.end())
	{
		std::stringstream ss2;
		ss2 << "Unknown gender: "<<aGender<<"!\n";
		throw RootsException(__FILE__, __LINE__, ss2.str().c_str());
	}
	gender = aGender;
}

void Pronoun::set_person(Person aPerson)
{
	if(personToString.find(aPerson) == personToString.end())
	{
		std::stringstream ss2;
		ss2 << "Unknown person: "<<aPerson <<"!\n";
		throw RootsException(__FILE__, __LINE__, ss2.str().c_str());
	}
	person = aPerson;
}

void Pronoun::set_person_number(Number aPerson)
{
	if(numberToString.find(aPerson) == numberToString.end())
	{
		std::stringstream ss2;
		ss2 << "Unknown number: "<<aPerson <<"!\n";
		throw RootsException(__FILE__, __LINE__, ss2.str().c_str());
	}
	personNumber = aPerson;
}

std::string Pronoun::to_string(int level) const
{
    std::stringstream ss;
    
	switch (level)
	{
	  case LEVEL_CLASS_TREEBANK_FR:
		ss << "PRO";
		break;
    case LEVEL_CLASS_COARSE:
        ss << "Pronoun";
        break;
        
    case LEVEL_CLASS_FINE:
        ss << "Pronoun";
        if(this->category != POS_CATEGORY_UNKNOWN)
            ss << "_" << categoryToString[this->category];
        if(this->personNumber != POS_NUMBER_UNKNOWN)
            ss << "_" << numberToString[this->personNumber];
        if(this->gender != POS_GENDER_UNKNOWN)
            ss << "_" << genderToString[this->gender];
        if(this->number != POS_NUMBER_UNKNOWN)
            ss << "_" << numberToString[this->number];
        if(this->person != POS_PERSON_UNKNOWN)
            ss << "_" << personToString[this->person];
	if(this->is_wh())
            ss << "_" << whToString;
        break;
        
    case LEVEL_LABEL:
    default:
        ss << "Pn";
        if(this->category != POS_CATEGORY_UNKNOWN) ss << categoryToChar[this->category];
        if(this->personNumber != POS_NUMBER_UNKNOWN) ss << numberToChar[this->personNumber];
        if(this->gender != POS_GENDER_UNKNOWN) ss << genderToChar[this->gender];
        if(this->number != POS_NUMBER_UNKNOWN) ss << numberToChar[this->number];
        if(this->person != POS_PERSON_UNKNOWN) ss << personToChar[this->person];
	if(this->is_wh()) ss << whToChar;
	}
	return std::string(ss.str().c_str());
}


void Pronoun::deflate(RootsStream * stream, bool is_terminal, int list_index)
{
  POS::deflate(stream,false,list_index);

  stream->append_int_content("category", this->get_category());
  stream->append_int_content("gender", this->get_gender());
  stream->append_int_content("number", this->get_number());
  stream->append_int_content("person", this->get_person());
  stream->append_int_content("person_number", this->get_person_number());
  stream->append_char_content("is_invariable", this->get_is_invariable());
  if (is_wh()) { stream->append_bool_content("is_wh", true); }

  if(is_terminal)
	stream->close_object();
}

void Pronoun::inflate(RootsStream * stream, bool is_terminal, int list_index)
{
	int tmp=0;
	POS::inflate(stream,false,list_index);

	//stream->open_children();

	tmp = stream->get_int_content("category");
	if(categoryToString.find((Category)tmp) == categoryToString.end())
	  {
	    std::stringstream ss2;
	    ss2 << "Unknown category: "<<tmp<<"!\n";
	    throw RootsException(__FILE__, __LINE__, ss2.str().c_str());
	  }
	this->set_category((Category)tmp);

	tmp = stream->get_int_content("gender");
	if(genderToString.find((Gender)tmp) == genderToString.end())
	  {
	    std::stringstream ss2;
	    ss2 << "Unknown gender: "<<tmp<<"!\n";
	    throw RootsException(__FILE__, __LINE__, ss2.str().c_str());
	  }
	this->set_gender((Gender)tmp);

	tmp = stream->get_int_content("number");
	if(numberToString.find((Number)tmp) == numberToString.end())
	  {
	    std::stringstream ss2;
	    ss2 << "Unknown number: "<<tmp<<"!\n";
	    throw RootsException(__FILE__, __LINE__, ss2.str().c_str());
	  }
	this->set_number((Number)tmp);

	tmp = stream->get_int_content("person");
	if(personToString.find((Person)tmp) == personToString.end())
	  {
	    std::stringstream ss2;
	    ss2 << "Unknown person: "<<tmp<<"!\n";
	    throw RootsException(__FILE__, __LINE__, ss2.str().c_str());
	  }
	this->set_person((Person)tmp);

	tmp = stream->get_int_content("person_number");
	if(numberToString.find((Number)tmp) == numberToString.end())
	  {
	    std::stringstream ss2;
	    ss2 << "Unknown person_number: "<<tmp<<"!\n";
	    throw RootsException(__FILE__, __LINE__, ss2.str().c_str());
	  }
	this->set_person_number((Number)tmp);

	this->set_is_invariable(stream->get_char_content("is_invariable"));
	
	if (stream->has_child("is_wh")) { this->set_is_wh(stream->get_bool_content("is_wh")); }

	if(is_terminal) stream->close_children();	
}

Pronoun * Pronoun::inflate_object(RootsStream * stream, int list_index)
{
	Pronoun *t = new Pronoun(true);
	t->inflate(stream,true,list_index);
	return t;	
}

} } } // END OF NAMESPACES
