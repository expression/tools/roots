/*
    This file is part of Roots.

    Roots is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Roots is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Roots.  If not, see <http://www.gnu.org/licenses/>.

    Copyright (c) 2013, 2014 IRISA
*/


#include "Punctuation.h"

namespace roots { namespace linguistic { namespace pos {

Punctuation::Punctuation(const bool noInit) : POS(noInit)
{
	set_type(POS_PUNCTUATION);
	set_category(POS_PUNCTUATION_CATEGORY_UNKNOWN);
	set_is_closing(POS_UNKNOWN);
}
 
Punctuation::Punctuation(PunctuationCategory cat, char closing) : category(cat), isClosing(closing)
{
	set_type(POS_PUNCTUATION);
}

Punctuation::Punctuation(const Punctuation &punc) : POS(punc)
{
	set_type(POS_PUNCTUATION);
	set_category(punc.get_category());
	set_is_closing(punc.get_is_closing());
}

Punctuation::~Punctuation()
{
}

Punctuation *Punctuation::clone() const
{
	return new Punctuation(*this);
}

Punctuation& Punctuation::operator=(const Punctuation &pun)
{
	POS::operator=(pun);
	set_type(pun.get_type());
	set_category(pun.get_category());
	set_is_closing(pun.get_is_closing());

	return *this;
}

void Punctuation::set_category(PunctuationCategory cat)
{
	if(punctuationCategoryToString.find(cat) == punctuationCategoryToString.end())
	{
		std::stringstream ss2;
		ss2 << "Unknown punctuation category: "<<cat <<"!\n";
		throw RootsException(__FILE__, __LINE__, ss2.str().c_str());
	}
	category = cat;
}

std::string Punctuation::to_string(int level) const
{
    std::stringstream ss;
    
	switch (level)
	{
	  case LEVEL_CLASS_TREEBANK_FR:
		ss << "PONCT";
		break;
    case LEVEL_CLASS_COARSE:
        ss << "Punctuation";
        break;
        
    case LEVEL_CLASS_FINE:
        ss << "Punctuation";
        if(this->category != POS_PUNCTUATION_CATEGORY_UNKNOWN)
            ss << "_" << punctuationCategoryToString[this->category];
        break;
            
    case LEVEL_LABEL:
    default:
        ss << "Pc";
        if(this->category != POS_PUNCTUATION_CATEGORY_UNKNOWN) ss << punctuationCategoryToChar[this->category];
	}
	return std::string(ss.str().c_str());
}

void Punctuation::deflate(RootsStream * stream, bool is_terminal, int list_index)
{
  POS::deflate(stream,false,list_index);

  stream->append_int_content("category", this->get_category());
  stream->append_char_content("is_closing", this->get_is_closing());

  if(is_terminal)
	stream->close_object();
}

void Punctuation::inflate(RootsStream * stream, bool is_terminal, int list_index)
{
	int tmp=0;
	POS::inflate(stream,false,list_index);

	//stream->open_children();

	tmp = stream->get_int_content("category");
	if(punctuationCategoryToString.find((PunctuationCategory)tmp) == punctuationCategoryToString.end())
	  {
	    std::stringstream ss2;
	    ss2 << "Unknown punctuation category: "<<tmp<<"!\n";
	    throw RootsException(__FILE__, __LINE__, ss2.str().c_str());
	  }
	this->set_category((PunctuationCategory)tmp);

	this->set_is_closing(stream->get_char_content("is_closing"));

	if(is_terminal) stream->close_children();	
}

Punctuation * Punctuation::inflate_object(RootsStream * stream, int list_index)
{
	Punctuation *t = new Punctuation(true);
	t->inflate(stream,true,list_index);
	return t;	
}

} } } // END OF NAMESPACES
