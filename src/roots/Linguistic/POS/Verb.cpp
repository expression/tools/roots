/*
    This file is part of Roots.

    Roots is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Roots is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Roots.  If not, see <http://www.gnu.org/licenses/>.

    Copyright (c) 2013, 2014 IRISA
*/


#include "Verb.h"

namespace roots { namespace linguistic { namespace pos {


Verb::Verb(const bool noInit) : POS(noInit)
{
	set_type(POS_VERB);
	set_mood(POS_MOOD_UNKNOWN);
	set_tense(POS_TENSE_UNKNOWN);
	set_aspect(POS_ASPECT_UNKNOWN);
	set_number(POS_NUMBER_UNKNOWN);
	set_person(POS_PERSON_UNKNOWN);
	set_gender(POS_GENDER_UNKNOWN);
	set_is_auxiliary(POS_UNKNOWN);
	set_is_passive(POS_UNKNOWN);
}


Verb::Verb(Mood moo, Tense ten, Aspect asp, Number num, Gender gen,
		Person per, char aux, char pas):
		mood(moo), tense(ten), aspect(asp), number(num),
		person(per), gender(gen), isAuxiliary(aux), isPassive(pas)
{
	set_type(POS_VERB);
}

Verb::Verb(const Verb &verb) : POS(verb)
{
	set_type(POS_VERB);
	set_mood(verb.get_mood());
	set_tense(verb.get_tense());
	set_aspect(verb.get_aspect());
	set_number(verb.get_number());
	set_person(verb.get_person());
	set_gender(verb.get_gender());
	set_is_auxiliary(verb.get_is_auxiliary());
	set_is_passive(verb.get_is_passive());
}

Verb::~Verb()
{
}

Verb *Verb::clone() const
{
	return new Verb(*this);
}

Verb& Verb::operator=(const Verb &verb)
{
	POS::operator=(verb);
	set_type(verb.get_type());
	set_mood(verb.get_mood());
	set_tense(verb.get_tense());
	set_aspect(verb.get_aspect());
	set_number(verb.get_number());
	set_person(verb.get_person());
	set_gender(verb.get_gender());
	set_is_auxiliary(verb.get_is_auxiliary());
	set_is_passive(verb.get_is_passive());

	return *this;
}

void Verb::set_mood(Mood aMood)
{
	if(moodToString.find(aMood) == moodToString.end())
	{
		std::stringstream ss2;
		ss2 << "Unknown mood: "<<aMood<<"!\n";
		throw RootsException(__FILE__, __LINE__, ss2.str().c_str());
	}
	mood = aMood;
}

void Verb::set_tense(Tense aTense)
{
	if(tenseToString.find(aTense) == tenseToString.end())
	{
		std::stringstream ss2;
		ss2 << "Unknown tense: "<<aTense<<"!\n";
		throw RootsException(__FILE__, __LINE__, ss2.str().c_str());
	}
	tense = aTense;
}

void Verb::set_aspect(Aspect aAspect)
{
	if(aspectToString.find(aAspect) == aspectToString.end())
	{
		std::stringstream ss2;
		ss2 << "Unknown aspect: "<<aAspect<<"!\n";
		throw RootsException(__FILE__, __LINE__, ss2.str().c_str());
	}
	aspect = aAspect;
}

void Verb::set_number(Number aNumber)
{
	if(numberToString.find(aNumber) == numberToString.end())
	{
		std::stringstream ss2;
		ss2 << "Unknown number: "<<aNumber<<"!\n";
		throw RootsException(__FILE__, __LINE__, ss2.str().c_str());
	}
	number = aNumber;
}

void Verb::set_gender(Gender aGender)
{
	if(genderToString.find(aGender) == genderToString.end())
	{
		std::stringstream ss2;
		ss2 << "Unknown gender: "<<aGender<<"!\n";
		throw RootsException(__FILE__, __LINE__, ss2.str().c_str());
	}
	gender = aGender;
}

void Verb::set_person(Person aPerson)
{
	if(personToString.find(aPerson) == personToString.end())
	{
		std::stringstream ss2;
		ss2 << "Unknown person: "<<aPerson<<"!\n";
		throw RootsException(__FILE__, __LINE__, ss2.str().c_str());
	}
	person = aPerson;
}

std::string Verb::to_string(int level) const
{
    std::stringstream ss;
    
	switch (level)
	{
	  case LEVEL_CLASS_TREEBANK_FR:
		ss << "V";
		break;
    case LEVEL_CLASS_COARSE:
        ss << "Verb";
        break;
        
    case LEVEL_CLASS_FINE:
        ss << "Verb";
        if(this->mood != POS_MOOD_UNKNOWN) ss << "_" << moodToString[this->mood];
        if(this->tense != POS_TENSE_UNKNOWN) ss << "_" << tenseToString[this->tense];
        if(this->aspect != POS_ASPECT_UNKNOWN) ss << "_" << aspectToString[this->aspect];
        if(this->gender != POS_GENDER_UNKNOWN) ss << "_" << genderToString[this->gender];
        if(this->number != POS_NUMBER_UNKNOWN) ss << "_" << numberToString[this->number];
        if(this->person != POS_PERSON_UNKNOWN) ss << "_" << personToString[this->person];
        break;
        
    case LEVEL_LABEL:
    default:
        ss 	<<  "Vb";
        if(this->mood != POS_MOOD_UNKNOWN) ss << moodToChar[this->mood];
        if(this->tense != POS_TENSE_UNKNOWN) ss << tenseToChar[this->tense];
        if(this->aspect != POS_ASPECT_UNKNOWN) ss << aspectToChar[this->aspect];
        if(this->gender != POS_GENDER_UNKNOWN) ss << genderToChar[this->gender];
        if(this->number != POS_NUMBER_UNKNOWN) ss << numberToChar[this->number];
        if(this->person != POS_PERSON_UNKNOWN) ss << personToChar[this->person];
	}
	return std::string(ss.str().c_str());
}

void Verb::deflate(RootsStream * stream, bool is_terminal, int list_index)
{
  POS::deflate(stream,false,list_index);

  stream->append_int_content("mood", this->get_mood());
  stream->append_int_content("tense", this->get_tense());
  stream->append_int_content("aspect", this->get_aspect());
  stream->append_int_content("number", this->get_number());
  stream->append_int_content("person", this->get_person());
  stream->append_int_content("gender", this->get_gender());
  stream->append_char_content("is_auxiliary", this->get_is_auxiliary());
  stream->append_char_content("is_passive", this->get_is_passive());

  if(is_terminal)
	stream->close_object();
}

void Verb::inflate(RootsStream * stream, bool is_terminal, int list_index)
{
	int tmp=0;
	POS::inflate(stream,false,list_index);

	//stream->open_children();

	tmp = stream->get_int_content("mood");
	if(moodToString.find((Mood)tmp) == moodToString.end())
	  {
	    std::stringstream ss2;
	    ss2 << "Unknown mood: "<<tmp<<"!\n";
	    throw RootsException(__FILE__, __LINE__, ss2.str().c_str());
	  }
	this->set_mood((Mood)tmp);

	tmp = stream->get_int_content("tense");
	if(tenseToString.find((Tense)tmp) == tenseToString.end())
	  {
	    std::stringstream ss2;
	    ss2 << "Unknown tense: "<<tmp<<"!\n";
	    throw RootsException(__FILE__, __LINE__, ss2.str().c_str());
	  }
	this->set_tense((Tense)tmp);

	tmp = stream->get_int_content("aspect");
	if(aspectToString.find((Aspect)tmp) == aspectToString.end())
	  {
	    std::stringstream ss2;
	    ss2 << "Unknown aspect: "<<tmp<<"!\n";
	    throw RootsException(__FILE__, __LINE__, ss2.str().c_str());
	  }
	this->set_aspect((Aspect)tmp);

	tmp = stream->get_int_content("number");
	if(numberToString.find((Number)tmp) == numberToString.end())
	  {
	    std::stringstream ss2;
	    ss2 << "Unknown number: "<<tmp<<"!\n";
	    throw RootsException(__FILE__, __LINE__, ss2.str().c_str());
	  }
	this->set_number((Number)tmp);

	tmp = stream->get_int_content("person");
	if(personToString.find((Person)tmp) == personToString.end())
	  {
	    std::stringstream ss2;
	    ss2 << "Unknown person: "<<tmp<<"!\n";
	    throw RootsException(__FILE__, __LINE__, ss2.str().c_str());
	  }
	this->set_person((Person)tmp);

	tmp = stream->get_int_content("gender");
	if(genderToString.find((Gender)tmp) == genderToString.end())
	  {
	    std::stringstream ss2;
	    ss2 << "Unknown gender: "<<tmp<<"!\n";
	    throw RootsException(__FILE__, __LINE__, ss2.str().c_str());
	  }
	this->set_gender((Gender)tmp);

	this->set_is_auxiliary(stream->get_char_content("is_auxiliary"));
	this->set_is_passive(stream->get_char_content("is_passive"));

	if(is_terminal) stream->close_children();	
}

Verb * Verb::inflate_object(RootsStream * stream, int list_index)
{
	Verb *t = new Verb(true);
	t->inflate(stream,true,list_index);
	return t;	
}

} } } // END OF NAMESPACES
