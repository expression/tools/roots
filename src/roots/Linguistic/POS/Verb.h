/*
    This file is part of Roots.

    Roots is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Roots is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Roots.  If not, see <http://www.gnu.org/licenses/>.

    Copyright (c) 2013, 2014 IRISA
*/


#ifndef VERB_H_
#define VERB_H_

#include "../../common.h"
#include "../POS.h"

namespace roots { namespace linguistic { namespace pos {


/**
 * @brief Verb class
 * @details This class represents the Part Of Speech Verb.
 * @author Cordial Group
 * @version 0.1
 * @date 2011
 */
class Verb: public POS
{
public:
	Verb(const bool noInit = false);
	Verb(Mood moo, Tense ten, Aspect asp, Number num, Gender gen,
			Person per, char aux, char pas);
	Verb(const Verb &verb);
	virtual ~Verb();
	/**
	 * @brief clone the current object
	 * @return a copy of the object
	 */
	virtual Verb *clone() const;

	Verb& operator=(const Verb &verb);


	void set_mood(Mood aMood);
	void set_tense(Tense aTense);
	void set_aspect(Aspect aAspect);
	void set_number(Number aNumber);
	void set_gender(Gender aGender);
	void set_person(Person aPerson);
	void set_is_auxiliary(char b) {isAuxiliary = b;}
	void set_is_passive(char b) {isPassive = b;}


	Mood get_mood() const {return mood;}
	Tense get_tense() const {return tense;}
	Aspect get_aspect() const {return aspect;}
	Number get_number() const {return number;}
	Gender get_gender() const {return gender;}
	Person get_person() const {return person;}
	char get_is_auxiliary() const {return isAuxiliary;}
	char get_is_passive() const {return isPassive;}

	bool is_verb() const { return true; }
	bool is_unknown() const { return false; }

	bool is_passive() const
	{
		return get_is_passive() == POS_IS_PASSIVE;
	}

	bool is_auxiliary() const
	{
		return get_is_auxiliary() == POS_IS_AUXILIARY;
	}

	bool is_infinitive() const
	{
		return get_mood() == POS_MOOD_INFINITIVE;
	}

	bool is_indicative() const
	{
		return get_mood() == POS_MOOD_INDICATIVE;
	}

	bool is_conditional() const
	{
		return get_mood() == POS_MOOD_CONDITIONAL;
	}

	bool is_subjonctive() const
	{
		return get_mood() == POS_MOOD_SUBJONCTIVE;
	}

	bool is_imperative() const
	{
		return get_mood() == POS_MOOD_IMPERATIVE;
	}

	bool is_participle() const
	{
		return get_mood() == POS_MOOD_PARTICIPLE;
	}

	bool is_present() const
	{
		return get_tense() == POS_TENSE_PRESENT;
	}

	bool is_imperfect() const
	{
		return get_tense() == POS_TENSE_IMPERFECT;
	}

	bool is_past() const
	{
		return get_tense() == POS_TENSE_PAST;
	}

	bool is_future() const
	{
		return get_tense() == POS_TENSE_FUTURE;
	}

	bool is_finite() const
	{
		return get_aspect() == POS_ASPECT_FINITE;
	}

	bool is_non_finite() const
	{
		return get_aspect() == POS_ASPECT_NON_FINITE;
	}

	bool is_male() const
	{
		return get_gender() == POS_GENDER_MALE;
	}

	bool is_female() const
	{
		return get_gender() == POS_GENDER_FEMALE;
	}

	bool is_common() const
	{
		return get_gender() == POS_GENDER_COMMON;
	}

	bool is_neutral() const
	{
		return get_gender() == POS_GENDER_NEUTRAL;
	}

	bool is_singular() const
	{
		return get_number() == POS_NUMBER_SINGULAR;
	}

	bool is_plural() const
	{
		return get_number() == POS_NUMBER_PLURAL;
	}

	bool is_first_person() const
	{
		return get_person() == POS_PERSON_FIRST;
	}

	bool is_second_person() const
	{
		return get_person() == POS_PERSON_SECOND;
	}

	bool is_third_person() const
	{
		return get_person() == POS_PERSON_THIRD;
	}

	/**
	 * @brief returns a string representation of any Roots object
	 * @param level the precision level of the output
	 * @return string representation of the object
	 */
	virtual std::string to_string(int level=0) const;
	/**
	 * @brief Write the entity into a RootsStream
	 * @param stream the stream from which we inflate the element
	 * @param is_terminal indicates that the node is terminal (true by default)
	 */
	virtual void deflate(RootsStream * stream, bool is_terminal=true, int list_index=-1);
	/**
	 * @brief Extracts the entity from the stream
	 * @param stream the stream from which we inflate the element
	 */
	virtual void inflate(RootsStream * stream, bool is_terminal=true, int list_index=-1);
	/**
	 * @brief Extracts the entity from the stream
	 * @param stream the stream from which we inflate the element
	 */
	static Verb * inflate_object(RootsStream * stream, int list_index=-1);
	/**
	 * @brief returns classname for current object
	 * @return string constant representing the classname
	 */
	virtual std::string get_classname() const { return classname(); };
	/**
	 * @brief returns classname for current class
	 * @return string constant representing the classname
	 */
	static std::string classname() { return "Linguistic::POS::Verb"; };

private:
	Mood mood;
	Tense tense;
	Aspect aspect;
	Number number;
	Person person;
	Gender gender;
	char isAuxiliary;
	char isPassive;

};

} } }// END OF NAMESPACES


#endif /* VERB_H_ */
