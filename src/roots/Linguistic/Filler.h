/*
    This file is part of Roots.

    Roots is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Roots is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Roots.  If not, see <http://www.gnu.org/licenses/>.

    Copyright (c) 2013, 2014 IRISA
*/


#ifndef FILLER_H_
#define FILLER_H_

#include "../BaseItem.h"

namespace roots { namespace linguistic { namespace filler {

/**
 * @brief This class is the main class of fillers
 * @details This class is abstract
 * @author Cordial Group
 * @version 0.1
 * @date 2011
 */
class Filler: public BaseItem
{
protected:
	/**
	 * @brief Default constructor
	 */
	Filler(const bool noInit = false);
public:
	/**
	 * @brief Constructor
	 * @param filler
	 */
	Filler(const Filler &filler);
	/**
	 * @brief Destructor
	 */
	virtual ~Filler();
	/**
	 * @brief Copy filler features into the current filler
	 * @param filler
	 * @return reference to the current filler
	 */
	Filler& operator= (const Filler &filler);
	/**
	 * @brief clone the current default object
	 * @return a copy of the object
	 */
	virtual Filler *clone() const =0;	
	/**
	 * @brief Return true if the filler corresponds to speech
	 * @return true or false
	 */
	virtual bool is_speech() =0;
	/**
	 * @brief Return false if the filler corresponds to speech
	 * @return true or false
	 */
	virtual bool is_non_speech() =0;

	/**
	 * @brief Write the entity into a RootsStream
	 * @param stream the stream from which we inflate the element
	 * @param is_terminal indicates that the node is terminal (true by default)
	 * @param parentNode
	 */
	virtual void deflate(RootsStream * stream, bool is_terminal=true, int list_index=-1);
	/**
	 * @brief Extracts the entity from the stream
	 * @param stream the stream from which we inflate the element
	 */
	virtual void inflate(RootsStream * stream, bool is_terminal=true, int list_index=-1);
	/**
	 * @brief Extracts the entity from the stream
	 * @param stream the stream from which we inflate the element
	 */
	static Filler * inflate_object(RootsStream * stream, int list_index=-1);
	/**
	* @brief returns the XML tag name value for current object
	* @return string constant representing the XML tag name
	*/
	virtual std::string get_xml_tag_name() const { return xml_tag_name(); };
	/**
	* @brief returns the XML tag name value for current class
	* @return string constant representing the XML tag name
	*/
	static std::string xml_tag_name() { return "filler"; };
	/**
	* @brief returns classname for current object
	* @return string constant representing the classname
	*/
	virtual std::string get_classname() const { return classname(); };
	/**
	* @brief returns classname for current class
	* @return string constant representing the classname
	*/
	static std::string classname() { return "Linguistic::Filler"; };
	/**
	* @brief returns display color for current object
	* @return string constant representing the pgf display color
	*/
	virtual std::string get_pgf_display_color() const { return pgf_display_color();};
	/**
	* @brief returns display color for current class
	* @return string constant representing the pgf display color
	*/
	static std::string pgf_display_color() { return "orange!25"; };
	
	template<class U> U* as()
	{
	  //return reinterpret_cast<U*>(this);
	  //return dynamic_cast<U*>(this);
	  return static_cast<U*>(this);
	}
private:

};

} } }

#endif /* FILLER_H_ */
