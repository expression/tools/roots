/*
    This file is part of Roots.

    Roots is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Roots is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Roots.  If not, see <http://www.gnu.org/licenses/>.

    Copyright (c) 2013, 2014 IRISA
*/


#include "Filler.h"

namespace roots
{

  namespace linguistic
  {

    namespace filler
    {

      Filler::Filler(const bool noInit) : BaseItem(noInit)
      {}

      Filler::Filler(const Filler& filler) : BaseItem(filler)
      {}

      Filler::~Filler()
      {}

      Filler& Filler::operator= (const Filler& filler)
      {
	BaseItem::operator=(filler);
	return *this;
      }

      void Filler::deflate(RootsStream * stream, bool is_terminal, int list_index)
      {
	BaseItem::deflate(stream,false,list_index);

	if(is_terminal)
	  stream->close_object();
      }



		void Filler::inflate(RootsStream * stream, bool /* is_terminal*/, int list_index)
      {
	BaseItem::inflate(stream,false,list_index);

	//this->set_label(stream->get_unicodestring_content("label"));
      }

		Filler * Filler::inflate_object(RootsStream * /* stream */, int /* list_index */)
      {
	//	Filler *t = new Filler();
	//	t->inflate(stream,true,list_index);
	//	return t;
	return NULL;
      }


    }  
  }
}  // END OF NAMESPACE
