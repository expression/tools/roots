/*
    This file is part of Roots.

    Roots is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Roots is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Roots.  If not, see <http://www.gnu.org/licenses/>.

    Copyright (c) 2013, 2014 IRISA
*/


#ifndef BASE_H_
#define BASE_H_

#include "common.h"
#include "RootsException.h"
#include "RootsStream.h"
#include "Matrix.h"
#include <boost/lexical_cast.hpp>

namespace roots
{
  /**
   * @brief This is the base class of all entities in ROOTS
   * @details
   * This class provides the basic informations and an basic XML interface. All entities of ROOTS have to derived it and adapt the information and the XML interface to their specificities.
   *
   * @author Cordial Group
   * @version 0.1
   * @date 2011
   */
  class Base
  {


  public:
    /**
     * @brief Default constructor
     * @param noInit Optional, true if no timestamp should be initialized when creating the instance
     * Initialize label to the empty string and also compute the timestamp and generate an ID.
     */
    Base(const bool noInit = false);

    /**
     * @brief Default constructor
     * Initialize label to the empty string and also compute the timestamp and generate an ID.
     * @param aLabel the label of the object
     * @param noInit Optional, true if no timestamp should be initialized when creating the instance
     */
    Base(const std::string& aLabel, const bool noInit = false);

    /**
     * @brief Copy constructor
     * Initialize label and ID from the given object but compute a new timestamp.
     */
    Base(const Base &base);

    /**
     * @brief Destructor
     */
    virtual ~Base();

    /**
     * @brief Destructor for wrappers
     */
    virtual void destroy();

    /**
     * @brief clone the current default object
     * @return a copy of the object
     */
    virtual Base *clone() const;

    /**
     * @brief Copy the given object into the current one except for the timestamp that is recomputed.
     * @param base source base object
     * @return reference to the current object
     */
    Base& operator=(const Base &base);

    /**
     * @brief Assigns the label with a new value
     * @param alabel new value of the label
     */
    virtual void set_label(const std::string &alabel) { label = alabel; }

   /**
     * @brief Assigns the description with a new value
     * @param the description
     */
    virtual void set_description(const std::string & descr);
 
    /**
     * @brief Assigns the description with a new value
     * @param pointer to the description (may be null if no description)
     */
    virtual void set_description(const std::string * descr);

   /**
     * @brief remove the description
     */
    virtual void remove_description();

    /**
     * Compute a new timestamp
     */
    void set_timestamp();

    /**
     * @brief Assigns the timestamp with a new value
     * @param value the new value
     */
    void set_timestamp(const std::string &value);

    /**
     * @brief Returns the label
     * @return the label
     */
    virtual const std::string& get_label() const;

    /**
     * @brief Returns the ID
     * @return the ID
     */
    virtual const std::string get_id() const;

    /**
     * @brief Returns the timestamp
     * @return the timestamp
     */
    virtual const std::string get_timestamp() const;

    /**
     * @brief Returns the timestamp
     * @return the timestamp with a pretty printing format
     */
    std::string get_pretty_timestamp() const;

    /**
     * @brief Returns the description
     * @return the description
     */
    virtual const std::string * get_description() const;

    /**
     * @brief Compute the dissimilarity between elements.
     * @param bi A reference to the element to be compared to.
     * @return A float number ranging between MIN_SIMILARITY and MAX_SIMILARITY, the highest the most dissimilar.
     * @notice The returned value between elements of different (unrelated) types is MAX_SIMILARITY.
     */
    virtual double compute_dissimilarity( const roots::Base * b ) const;
    
    /**
     * @brief Compute the similarity between elements.
     * @param bi A reference to the element to be compared to.
     * @return A float number ranging between MIN_SIMILARITY and MAX_SIMILARITY, the highest the most similar.
     * @notice The returned value between elements of different (unrelated) types is MIN_SIMILARITY.
     */
    virtual double compute_similarity( const roots::Base * b ) const;

    /**
     * @brief Split the current element into sub-elements, i.e. finer grain elements.
     * @note By default, an element is decomposed into the graphemes of its string representation.
     * @return A pointer to a vector of sub-element pointers.
     * @warning It is the responsability of the user to free the returned pointed sub-elements.
     */
    virtual std::vector< Base * > * split();

    /**
     * Serialize the object into a stream as a string
     *
     * @param out
     * @param seg
     * @return the modified stream
     */
    friend std::ostream& operator<< (std::ostream& out, Base& seg);

    /**
     * @brief Generate the XML part of the entity
     * @param stream
     * @param is_terminal
     */
    virtual void deflate(RootsStream * stream, bool is_terminal=true, int list_index=-1);
    /**
     * @brief Extracts the entity from the stream
     * @param stream the stream from which we inflate the element
     */
    virtual void inflate(RootsStream * stream, bool is_terminal=true, int list_index=-1);

    /**
     * @brief returns the XML tag name value for current object
     * @return string constant representing the XML tag name
     */
    virtual std::string get_xml_tag_name() const { return xml_tag_name(); };
    /**
     * @brief returns the XML tag name value for current class
     * @return string constant representing the XML tag name
     */
    static std::string xml_tag_name() { return "base"; };
    /**
     * @brief returns classname for current class
     * @return string constant representing the classname
     */
    virtual std::string get_classname() const { return classname(); };
    /**
     * @brief returns classname for current class
     * @return string constant representing the classname
     */
    static std::string classname() { return "Base"; };
    /**
     * @brief returns display color for current object
     * @return string constant representing the pgf display color
     */
    virtual std::string get_pgf_display_color() const { return pgf_display_color();};
    /**
     * @brief returns display color for current class
     * @return string constant representing the pgf display color
     */
    static std::string pgf_display_color() { return "yellow!25"; };
    /**
     * @brief returns the display height for the current object
     * @return display height for the current object
     */
    virtual int get_pgf_height() const { return 1; };
    /**
     * @brief returns a string representation of any Roots object
     * @param level the precision level of the output
     * @return string representation of the object
     */
    virtual std::string to_string(int level=0) const;
    /**
     * @brief returns a UnicodeString representation of any Roots object
     * @param level the precision level of the output
     * @return string representation of the object as an UnicodeString
     */
    virtual icu::UnicodeString to_unistr(int level=0) const;
    
    	
    /**
     * @brief Minimum similarity between 2 elements.
     **/
    static const double MIN_SIMILARITY;
    
    /**
     * @brief Maximum similarity between 2 elements.
     **/
    static const double MAX_SIMILARITY;
    
    /**
     * @brief Minimum dissimilarity between 2 elements.
     **/
    static const double MIN_DISSIMILARITY;
    
    /**
     * @brief Maximum dissimilarity between 2 elements.
     **/
    static const double MAX_DISSIMILARITY;
    
  protected:
    const uint64_t & get_raw_timestamp() const;
    void set_raw_timestamp(const uint64_t & timestamp);

   virtual std::string generate_id_key() const;

  private:
    std::string label;		/**< the label of the object */
    uint64_t timestamp;		/**< the timestamp */
    std::string* description;

  protected:
    static const icu::Locale currentLocale;
  };

  /**
   * Returns whether the two objects are identical (attributes) or not
   *
   * @param a, first Base instance
   * @param b, second Base instance
   * @return true if the objects are identical, false otherwise
   */
  bool operator==(Base const& a, Base const& b);

} // END OF NAMESPACE

#endif /* BASE_H_ */
