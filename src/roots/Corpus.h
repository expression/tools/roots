/*
    This file is part of Roots.

    Roots is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Roots is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Roots.  If not, see <http://www.gnu.org/licenses/>.

    Copyright (c) 2013, 2014 IRISA
*/


#ifndef CORPUS_H
#define CORPUS_H

#include "BaseChunk.h"

namespace roots {

  class Corpus : public BaseChunk
  {

    /* Constructor / Destructor / Copy */
  public:
        
    /**
     * @brief Default constructor
     * @param sizeLoadBuffer Optional, size of the cache over chunks.
     **/
    Corpus(int sizeLoadBuffer=DEFAULT_CACHE_SIZE);
        
    /**
     * @brief Constructor building a new corpus and feeding it with a Roots file
     * @param fileName Path to the Roots file to be loaded.
     * @param sizeLoadBuffer Optional, size of cache over chunks.
     **/
    Corpus(const std::string & fileName, int sizeLoadBuffer=DEFAULT_CACHE_SIZE);

    /**
     * @brief Destructor
     **/
    virtual ~Corpus();

    /**
     * @brief Copy constructor
     * @param uset Corpus to be copied
     **/
    Corpus(const Corpus& uset);
    
    /**
     * @brief Affectation operator
     * @param uset Corpus to be copied onto the current corpus
     * @return A reference to the current corpus
     **/
    Corpus& operator= (const Corpus& uset);
    
    /**
     * @brief Clone a corpus
     * @return A pointer to the clone
     **/
    Corpus *clone() const;
    
    /**
     * @brief Clone a corpus and cast it as a \ref <BaseChunk> BaseChunk object
     * @return A BaseChunk pointer to the clone
     **/
    virtual BaseChunk * clone_to_basechunk() const;


    /* Utterance management */
  public:

    /** Counting Utterance functions **/
    /**
     * @brief Returns the number of utterances in the corpus
     * @warning Since this methods may access files where utterances are stored, this method may be very time-consuming.
     */
    int count_utterances() const;

    /**
     * @brief Returns the number of utterances in a given corpus chunk
     * @warning This method may be very time-consuming if the target chunk is stored in a file.
     */
    int count_utterances_in_chunk(const int chunkId) const;

    /**
     * @brief Update the number of utterances
     * @note may loads files
     */
    virtual void update_counts() ;

    /** Get Utterance functions **/

    /**
     * @brief Retrieve an utterance from the current corpus
     * @note All layers are merged in the returned utterance.
     * @param index Index of the utterance in the corpus
     * @return Pointer to a copy of the utterance
     * @warning This methods returns a pointer to a dynamically allocated utterance. It is the user's duty to free it. Likewise, modifications on the referred utterance will not modify the corpus. Use methods \ref update_utterance to do so.
     **/
    Utterance* get_utterance(int index);

    /**
     * @brief Retrieve n utterances from the current corpus
     * @note All layers are merged in the returned utterances. This method will use parallelism.
     * @param index Index of the utterance in the corpus.
     * @param number Number of utterance to retrieved.
     * @return vector of utterance pointers
     * @warning This methods returns a pointers to a dynamically allocated utterances. It is the user's duty to free them. Likewise, modifications on the referred utterance will not modify the corpus. Use methods \ref update_utterance to do so.
     **/
    std::vector<Utterance*> get_utterances(int index, unsigned int number);


    /**
     * @brief Retrieve an utterance from the current corpus
     * @note Only input layers are merged in the returned utterance.
     * @param layersToMerge List of layers to be merged
     * @param index Index of the utterance in the corpus
     * @return Pointer to a copy of the utterance
     * @warning This methods returns a pointer to a dynamically allocated utterance. It is the user's duty to free it. Likewise, modifications on the referred utterance will not modify the corpus. Use methods \ref update_utterance to do so.
     **/
    Utterance* get_utterance(const std::vector<std::string> & layersToMerge,
			     int index = 0) ;

    /** Add Utterance functions **/

    /**
     * @brief Adds a new utterance in the current file group, assuming there is no layer
     * @param utt pointer to the utterance
     */
    virtual void add_utterance(const Utterance& utt,
			       const std::string layerName=ANONYMOUS_LAYER);
    //  throw(RootsException) ;

    /**
     * @brief Adds a new (sliced) utterance in the current file group
     * @param utt map giving layer names to utterance (one utterance per layer)
     */
    virtual void add_utterance(const std::map<std::string, Utterance>& utt);
    //  throw(RootsException);

    /**
     * @brief Add an utterance to the current corpus
     * @exception RootsException Thrown if the index is out of bound
     */
    void add_utterance(const Utterance& utt, const int chunkId); // throw (RootsException);

    /**
     * @brief Adds new utterances in the current file group, assuming there is no layer
     * @param utt pointer to the utterance
     */
    virtual void add_utterances(const std::vector<Utterance>& utt,
								const std::string layerName=ANONYMOUS_LAYER);
		//  throw(RootsException) ;

    /**
     * @brief Adds a new (sliced) utterance in the current file group
     * @param utt map giving layer names to utterances
     */
    virtual void add_utterances(const std::map<std::string, std::vector<Utterance> >& utt);
    //  throw(RootsException);

    virtual void add_utterances(const std::vector<Utterance>& utt,
								const int chunkId);
    //  throw(RootsException) ;

    virtual void add_utterance(const Utterance& utt,
							   const std::string layerLabel,
							   const int chunkId);
		//  throw (RootsException);

    virtual void add_utterance(const std::map<std::string, Utterance>& utt,
							   const int chunkId);
		//  throw (RootsException);

    virtual void add_utterances(const std::vector<Utterance>& utt,
								const std::string layerName,
								const int chunkId);
    //  throw(RootsException) ;

    virtual void add_utterances(const std::map<std::string, std::vector<Utterance> >& utt,
								const int chunkId);
		//  throw(RootsException);


    /** Update Utterance functions **/
     /**
     * @brief Replace a given utterance by another
     * @param id ID of the utterance to be replaced
     * @param utt the new utterance split into layers
     **/
    void update_utterance(int id, const Utterance& utt);
    
    /**
     * @brief returns the XML tag name value for current class
     * @return string constant representing the XML tag name
     */
    void update_utterance(int id, const std::map<std::string, Utterance> &utt);

    /** Delete Utterance functions **/
    
    /**
     * @brief Deletes an utterance (all layers)
     * @param id ID of the utterance to be removed. Default ID is 0 (first utterance).
     */
    void delete_utterance(int id = 0);
    
    /**
     * @brief Removes all utterances and cleans the chunk
     */
    virtual void clear();

    /* Chunks management */
  public:
    /**
     * @brief Add an empty memory chunk
     * @return The ID of the memory chunk in the current corpus
     */
    int new_memory_chunk();
    
    /**
     * @brief Add a new file chunk without layers
     * @param file Name of the file to be referenced in the current corpus
     * @param corpus_abs_dir Optional, absolute path to the directory containing the current corpus
     * @param nbUtterances Optional, number of utterances
     * @return The ID of the new chunk in the current corpus
     **/
    int new_file_chunk(const std::string & file,
		       const std::string corpus_abs_dir = "",
		       const int nbUtterances = 0);
    
    /**
     * @brief Add a new file chunk with multiple layers
     * @param layersToFiles Correspondance between each layer and its file name
     * @param corpus_abs_dir Optional, absolute path to the directory containing the current corpus
     * @param nbUtterances Optional, number of utterances
     * @return The ID of the new chunk in the current corpus
     **/
    int new_file_chunk(const std::map<std::string, std::string>& layersToFiles,
		       const std::string corpus_abs_dir = "",
		       const int nbUtterances = 0);
    int add_chunk(BaseChunk& bc);
    int add_chunk(BaseChunk& bc, int nbUtterances);

    void set_chunks(const std::vector<BaseChunk *>& chks);

    BaseChunk * get_chunk(int index); // throw (RootsException);
    const std::vector<BaseChunk *>& get_all_chunks() const; // Use with cautious
    std::vector<BaseChunk *> get_all_chunks_copy() const;
    
    /**
     * @brief Get the ID of the chunk containing a given utterance
     * @param utt_id ID of the utterance in the corpus
     * @return ID of the chunk
     **/
    int get_chunk_id(int utt_id);

    /**
     * @brief Return the total number of chunks in the current corpus
     * @return A positive or null integer
     */
    int count_chunks() const;
    
    /**
     * @brief Delete a chunk and all its utterances
     * @param id ID of the chunk to be removed.
     * @warning Removing a FileChunk does not remove the file stored in filesystem. Do it explicitly o your own.
     **/
    void delete_chunk(int id);

    /* Layers management */
  public:
    
    /**
     * @brief Returns the number of layers
     **/
    virtual int count_layers() const;

    /**
     * @brief Returns the name of all the layers described in the group of UtteranceSets
     * @note return the layers for one chunk, there is no garenty that all chunks have the same layers.
     */
    virtual std::vector<std::string> get_all_layer_names() const;

    /* Loading/Save functions */

  public:
    /**
     * @brief Loads a corpus stored in a file.
     * @note Push back loaded data if current corpus is not empty.
     * @param filename File path
     */
    void load(const std::string & filename);
    
     /**
     * @brief Saves a corpus into a file.
     * @note Data contained in the file are erased.
     * @note Already existing basedirs are not changed.
     * @param filename File path
     */
    void save(const std::string & filename);
    
     /**
     * @brief Slice memory chunks of the corpus into file chunks of a given size, save all the (formerly existing and new) file chunks and save the corpus. The name of generated FileChunks is built using the name of the corpus file concatenated with a chunk index.
     * @note Data contained in the files are erased.
     * @warning The order of utterances accross chunks is NOT necessarily preserved by this method.
     * @param filename File path
     * @param chunk_size_limit Size of each chunk
     * @param filechunk_relative_dir Optional, relative directory according the corpus directory where chunks are saved. If not set, the directory is the same as for the corpus file.
     */
    void save(const std::string & filename, int chunk_size_limit, const std::string & filechunk_relative_dir="");
    
    /**
     * @brief Change the alternate temporary directory for file chunks used in case the default temporary directory cannot be used.
     * @param new_temp_dir New directory
     **/
    void change_alternate_temp_dir(const std::string & new_alt_dir);
    
    /**
     * @brief Checks if unsaved changes have been made since last loading or last save_chunk
     * @return True if unsaved changes have been made, false otherwise.
     **/
    bool unsaved_changes() const;

  public:
    /**
     * @brief Saves a given chunk in external files
     * @note Does not save the current corpus.
     */
    // void save_chunk(const std::string chunkName); // TODO

    /**
     * @brief Generate the XML part of the entity
     * @param stream
     * @param is_terminal
     */
    virtual void deflate(RootsStream * stream, bool is_terminal=true, int list_index=-1);
    /**
     * @brief Extracts the entity from the stream
     * @param stream the stream from which we inflate the element
     */
    virtual void inflate(RootsStream * stream, bool is_terminal=true, int list_index=-1);

    /**
     * @brief returns the XML tag name value for current object
     * @return string constant representing the XML tag name
     */
    virtual std::string get_xml_tag_name() const {
      return xml_tag_name();
    };
    /**
     * @brief returns the XML tag name value for current class
     * @return string constant representing the XML tag name
     */
    static std::string xml_tag_name() {
      return "corpus";
    };
    /**
     * @brief returns classname for current object
     * @return string constant representing the classname
     */
    virtual std::string get_classname() const {
      return classname();
    };
    /**
     * @brief returns classname for current class
     * @return string constant representing the classname
     */
    static std::string classname() {
      return "Corpus";
    };

    /* chunks caching functions */
   /**
     * @brief Loads all the layers in memory
     * @note This method is used to handle caching
     */
    virtual void load_all() ;

    /**
     * @brief Test if everything is loaded to get an utterance
     */
    virtual bool is_loaded_for_utterance(const int id);

    /**
     * @brief Clears temporary data from the group of layers
     * @note This method is used to handle caching
     */
    virtual void unload_all();
    
    /**
     * @brief Loads one chunk in memory
     * @param chunkId ID of the chunk
     * @note This method is used to handle caching
     */
    virtual void load_chunk(const int chunkId);

    /**
     * @brief Clears temporary data from one chunk
     * @param chunkId ID of the chunk
     * @note This method is used to handle caching
     */
    virtual void unload_chunk(const int chunkId);

    virtual bool is_loaded_chunk(const int chunkId);

    virtual void may_be_loaded_chunk(const int chunkId);

    /* Internal functions */
  protected:
//     /**
//      * @brief Prepare a modified file chunk to be backup in a temporary file in order to save or reload changes later. Temporary file will be removed when destroying or saving the file chunk. Backup preparation fails if the chunk is not a memory chunk or if the target file chunk is already backup.
//      * @param index Index of the file chunk to be backup
//      * @return True if preparation has been achieved, false otherwise
//      **/
//     bool prepare_file_chunk_for_backup(int index);
    
    /**
     * @brief Test if the current corpus includes a filechunk with a given file name
     * @param name File name to be searched
     * @return True if such a filechunk is found, false otherwise
     */
    bool filechunk_name_exists(const std::string & name);
    
    /*
      void export_to_filechunk (const std::map<std::string, std::string> &fileNameMapping,
      const std::string chunkName,
      const std::string baseDir=""); // throw (RootsException);
    */ // TODO

    /* Members */
  protected:
    std::vector<BaseChunk *>	 chunks;	  /** List of chunks, each containing utterances */
    std::vector<int>		 chunkOffset;	  /** Indices of the first utterance in each chunk */

    size_t			 sizeLoadBuffer;  /** Number of chunks that can be loaded at the same time */
    std::deque<int>		 loadedBuffer;	  /** List of loaded chunk sort by age */

    std::vector<int>		 utteranceToChunk;/** Correspondance between an Utterance index and the index of its BaseChunk */
    //	  std::map<std::string, int> chunkNameToID;	  /** Correspondance between a chunk name and its ID */

    /**
     * @brief Flag enabled if a modification has occurred recently on utterances. This flag triggers a new computation of index tables when needed before being reset to false.
     */
    bool modificationFlagUtt;
    
    /**
     * @brief Flag enabled if a modification has occurred recently on the corpus structure (chunks).
     **/
    bool modificationFlagChunks;
        
    /**
     * Default size of the cache of chunks
     */
    static const int DEFAULT_CACHE_SIZE;

  };

}

#endif // CORPUS_H
