/*
    This file is part of Roots.

    Roots is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Roots is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Roots.  If not, see <http://www.gnu.org/licenses/>.

    Copyright (c) 2013, 2014 IRISA
*/


#ifndef BASELINK_H_
#define BASELINK_H_
#include "common.h"
namespace roots {
  /**
   * @brief This class models a link towards an object
   * @details
   * This template class contains a link that enables to reference an object
   * from another in a generic way. This class is used RelationLinkedObject
   * and SequenceLinkedObject to model backward links. The template value is
   * the type of the referenced object.
   *
   * @author Cordial Group
   * @version 0.1
   * @date 2011
   */
  template <class T>
    class BaseLink {
  public:
    /**
     * @brief Default constructor
     */
    BaseLink() {
      link = NULL;
    };

    /**
     * @brief Constructor that takes a pointer to the targeted object as a parameter
     * @param _link pointer to the object to link
     */
    BaseLink(T* _link) {
      link = _link;
    };

    /**
     * @brief Copy constructor
     * @param blink the BaseCopy object to copy
     */
    BaseLink(const BaseLink<T>& blink) {
      this->link = blink.link;
    };

    /**
     * @brief Destructor
     */
    virtual ~BaseLink() {};

    /**
     * @brief Returns the link value (pointer to the linked object)
     * @return pointer to the linked object
     */
    T *get_link() const {
      return link;
    }

    /**
     * @brief Assigns the link with a new value
     * @param link pointer to the new linked object
     */
    void set_link(T *link) {
      this->link = link;
    }


    BaseLink<T>& operator= ( const BaseLink<T>& blink)
      {
	this->link = blink.link;
	return *this;
      }

    bool operator==(const BaseLink<T> & bl) const
    {
      return link == bl.link;
    }

    size_t HashCode() const
    { 
      return boost::hash<void *>()(link);
    }
    
  private:
    T *link;	/**< pointer to the linked object */
  };
}

namespace boost
{
  template <class T>
    struct hash<roots::BaseLink<T> >
    {
      std::size_t operator()(const roots::BaseLink<T>& bl) const
	{
	  return bl.HashCode();
	}
    };
}


#endif /* BASELINK_H_ */
