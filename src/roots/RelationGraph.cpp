/*
    This file is part of Roots.

    Roots is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Roots is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Roots.  If not, see <http://www.gnu.org/licenses/>.

    Copyright (c) 2013, 2014 IRISA
*/

#include "RelationGraph.h"

namespace roots {

RelationGraph::RelationGraph() { relationGraph = Graph(); }

RelationGraph::~RelationGraph() {
  // relationGraph.clear();
}

RelationGraph *RelationGraph::clone() const { return new RelationGraph(*this); }

void RelationGraph::free_managed_relations() {
  /*
    std::pair<boost::graph_traits<Graph>::edge_iterator,
    boost::graph_traits<Graph>::edge_iterator> p;
    boost::graph_traits<Graph>::edge_iterator current;

    p = edges(relationGraph);
    current = p.first;
    while(current != p.second)
    {
    RelationGraphEdge & rge =	 relationGraph[(*current)];

    if ((rge.relation_reference != NULL) && (rge.relation_is_inverse ))
    {
    rge.relation_reference->destroy();
    rge.relation_reference = NULL;
    }
    ++current;
    }
  */
}

void RelationGraph::clear() {
  free_managed_relations();
  relationGraph.clear();
}

RelationGraph::VertexID RelationGraph::get_vertex_id(
    const std::string &seqLabel) {
  RelationGraph::VertexIterator vertexIter, vertexIterEnd;

  // cout << "searching for sequence: " << seqLabel;
  // cout << std::endl;
  for (boost::tie(vertexIter, vertexIterEnd) = vertices(relationGraph);
       vertexIter != vertexIterEnd; ++vertexIter) {
    if (seqLabel ==
        (relationGraph[(*vertexIter)].sequence_reference->get_label()))
      return (*vertexIter);
  }

  std::stringstream ss;
  ss << "get_vertex_id: vertex \"" << seqLabel << "\" not found!"
     << std::endl;
  throw RootsException(__FILE__, __LINE__, ss.str());
  return 0;
}

RelationGraph::EdgeID RelationGraph::get_edge_id(
    const std::string &srcSeqLabel,
    const std::string &trgSeqLabel) {
  RelationGraph::OutEdgeIterator edgeIter, edgeIterEnd;
  RelationGraph::VertexID sourceVertexId;
  bool found = false;

  try {
    sourceVertexId = this->get_vertex_id(srcSeqLabel);
  } catch (RootsException e) {
    // cerr << e.what();
    throw RootsException(__FILE__, __LINE__, "Missing vertex in graph!\n");
  }

  // cout << "sourceSequence = " << srcSeqLabel << std::endl;
  // cout << "targetSequence = " << trgSeqLabel << std::endl;
  boost::tie(edgeIter, edgeIterEnd) = out_edges(sourceVertexId, relationGraph);
  while (edgeIter != edgeIterEnd && !found) {
    if (!relationGraph[(*edgeIter)].relation_is_inverse) {
      //			cout << "Examining edge from <" <<
      // relationGraph[(*edgeIter)].relation_reference->get_source_sequence()->get_label()
      //<< "> ";
      //			cout << "to <" <<
      // relationGraph[(*edgeIter)].relation_reference->get_target_sequence()->get_label()
      //<< "> with relation <" <<
      // relationGraph[(*edgeIter)].relation_reference->get_label() << ">" <<
      // std::endl;
      //			cout << "EdgeId = " << *edgeIter << std::endl;

      found = (trgSeqLabel ==
               relationGraph[(*edgeIter)]
                   .relation_reference->get_target_sequence()
                   ->get_label());
    } else {
      //			cout << "Examining edge from <" <<
      // relationGraph[(*edgeIter)].relation_reference->get_target_sequence()->get_label()
      //<< "> ";
      //			cout << "to <" <<
      // relationGraph[(*edgeIter)].relation_reference->get_source_sequence()->get_label()
      //<< "> with relation <" <<
      // relationGraph[(*edgeIter)].relation_reference->get_label() << ">" <<
      // std::endl;
      //			cout << "EdgeId = " << *edgeIter << std::endl;

      found = (trgSeqLabel ==
               relationGraph[(*edgeIter)]
                   .relation_reference->get_source_sequence()
                   ->get_label());
    }
    if (!found) ++edgeIter;
  }
  if (!found) {
    throw RootsException(__FILE__, __LINE__, "edge not found!\n");
  }

  return (*edgeIter);
}

bool RelationGraph::exists_sequence(sequence::Sequence *seq) {
  return exists_sequence(seq->get_label());
}

bool RelationGraph::exists_sequence(const std::string &seqLabel) {
  try {
    get_vertex_id(seqLabel);
  } catch (RootsException e) {
    return false;
  }
  return true;
}

bool RelationGraph::exists_relation(Relation *relation) {
  RelationGraph::OutEdgeIterator edgeIter, edgeIterEnd;
  RelationGraph::VertexID sourceVertexId;
  std::string relationLabel = relation->get_label();
  std::string srcSeqLabel = relation->get_source_label();
  std::string trgSeqLabel = relation->get_target_label();
  bool found = false;

  try {
    sourceVertexId = get_vertex_id(srcSeqLabel);
  } catch (RootsException e) {
    return false;
  }

  boost::tie(edgeIter, edgeIterEnd) = out_edges(sourceVertexId, relationGraph);
  while (edgeIter != edgeIterEnd && !found) {
    found = (relationLabel ==
             (relationGraph[(*edgeIter)].relation_reference->get_label()));
    if (!found) ++edgeIter;
  }
  return found;
  return exists_relation(srcSeqLabel, trgSeqLabel);
}

bool RelationGraph::exists_relation(const std::string &srcSeqLabel,
                                    const std::string &trgSeqLabel) {
  RelationGraph::OutEdgeIterator edgeIter, edgeIterEnd;
  RelationGraph::VertexID sourceVertexId;
  bool found = false;

  try {
    sourceVertexId = get_vertex_id(srcSeqLabel);
  } catch (RootsException e) {
    return false;
  }

  boost::tie(edgeIter, edgeIterEnd) = out_edges(sourceVertexId, relationGraph);
  while (edgeIter != edgeIterEnd && !found) {
    found =
        (trgSeqLabel ==
         (relationGraph[(*edgeIter)].relation_reference->get_target_label()));
    if (!found) ++edgeIter;
  }
  return found;
}

RelationGraph::EdgeID RelationGraph::get_edge_id(Relation *relation,
                                                 bool direct) {
  RelationGraph::OutEdgeIterator edgeIter, edgeIterEnd;
  RelationGraph::VertexID sourceVertexId;
  std::string srcSeqLabel = relation->get_source_label();
  std::string trgSeqLabel = relation->get_target_label();
  std::string relationLabel = relation->get_label();
  bool found = false;

  try {
    sourceVertexId = get_vertex_id(srcSeqLabel);
  } catch (RootsException e) {
    // cerr << e.what() << std::endl;
    throw RootsException(__FILE__, __LINE__, "Missing vertex in graph!\n");
  }

  boost::tie(edgeIter, edgeIterEnd) = out_edges(sourceVertexId, relationGraph);
  while (edgeIter != edgeIterEnd && !found) {
    found = (relationLabel ==
             (relationGraph[(*edgeIter)].relation_reference->get_label())) &&
            (direct != (relationGraph[(*edgeIter)].relation_is_inverse));
    if (!found) ++edgeIter;
  }
  if (!found) {
    throw RootsException(__FILE__, __LINE__, "edge not found!\n");
  }

  return (*edgeIter);
}

RelationGraph::RelationGraphEdge RelationGraph::get_edge(
    const std::string &srcSeqLabel,
    const std::string &trgSeqLabel) {
  return relationGraph[get_edge_id(srcSeqLabel, trgSeqLabel)];
}

std::vector<sequence::Sequence *> RelationGraph::get_related_sequence(
    const std::string &seqLabel) {
  RelationGraph::OutEdgeIterator edgeIter, edgeIterEnd;
  RelationGraph::VertexID sourceVertexId;
  std::vector<sequence::Sequence *> result;

  try {
    sourceVertexId = get_vertex_id(seqLabel);
  } catch (RootsException e) {
    // cerr << e.what();
    /*
      std::std::stringstream ss;
      ss << "Sequence \"" << seqLabel << "\" is not in the graph!" <<
      std::std::endl;
      throw RootsException(__FILE__, __LINE__, ss.str());
    */
  }

  boost::tie(edgeIter, edgeIterEnd) = out_edges(sourceVertexId, relationGraph);
  while (edgeIter != edgeIterEnd) {
    if (!relationGraph[(*edgeIter)].relation_is_inverse) {
      Relation *rel = relationGraph[(*edgeIter)].relation_reference;
      result.push_back(rel->get_target_sequence());
    } else {
      Relation *rel = relationGraph[(*edgeIter)].relation_reference;
      result.push_back(rel->get_source_sequence());
    }
    edgeIter++;
  }

  return (result);
}

std::vector<roots::Relation *> RelationGraph::get_out_related_relation(
    const std::string &seqLabel) {
  RelationGraph::OutEdgeIterator edgeIter, edgeIterEnd;
  std::vector<roots::Relation *> result;

  try {
    RelationGraph::VertexID sourceVertexId = this->get_vertex_id(seqLabel);

    boost::tie(edgeIter, edgeIterEnd) =
        out_edges(sourceVertexId, relationGraph);
    while (edgeIter != edgeIterEnd) {
      Relation *rel = relationGraph[(*edgeIter)].relation_reference;
      result.push_back(rel);
      edgeIter++;
    }
  } catch (RootsException e) {
    // cerr << e.what();
    /*
      std::std::stringstream ss;
      ss << "Sequence \"" << seqLabel << "\" is not in the graph!" <<
      std::std::endl;
      throw RootsException(__FILE__, __LINE__, ss.str());
    */
  }

  return (result);
}

void RelationGraph::add_relation(Relation *relation) {
  sequence::Sequence *sourceSequence = NULL, *targetSequence = NULL;
  std::string srcSeqLabel, trgSeqLabel;
  std::string relationLabel;
  VertexID sourceVertexId, targetVertexId;
  EdgeID edgeId;
  int weight = 0;

  if (relation == NULL) {
    std::stringstream ss;
    ss << "Relation parameter is NULL!\n";
    throw RootsException(__FILE__, __LINE__, ss.str().c_str());
  }

  relationLabel = relation->get_label();

  // if(this->get_relation(relationLabel) == NULL)
  //{
  //	std::stringstream ss;
  //	ss << "Relation \"" + this->get_label() + "\" is not contained in this
  // utterance!\n";
  //	throw RootsException(__FILE__, __LINE__, ss.str().c_str());
  //}

  sourceSequence = relation->get_source_sequence();
  srcSeqLabel = relation->get_source_label();
  targetSequence = relation->get_target_sequence();
  trgSeqLabel = relation->get_target_label();

  // line 878 in Utterance.pm
  try {
    sourceVertexId = get_vertex_id(srcSeqLabel);
  } catch (RootsException e) {
    // cout << "Adding vertex : " << sourceSequence->get_label() << std::endl;
    sourceVertexId = add_vertex(relationGraph);
    relationGraph[sourceVertexId].sequence_reference = sourceSequence;
  }

  try {
    targetVertexId = get_vertex_id(trgSeqLabel);
  } catch (RootsException e) {
    // cout << "Adding vertex : " << targetSequence->get_label() << std::endl;
    targetVertexId = add_vertex(relationGraph);
    relationGraph[targetVertexId].sequence_reference = targetSequence;
  }

  // line 895 in Utterance.pm
  try {
    edgeId = get_edge_id(srcSeqLabel, trgSeqLabel);
  } catch (RootsException e) {
    add_edge(sourceVertexId, targetVertexId, relationGraph);
    edgeId = boost::edge(sourceVertexId, targetVertexId, relationGraph).first;

    // cout << "Adding edge from <" <<
    // relationGraph[sourceVertexId].sequence_reference->get_label() << "> ";
    // cout << "to <" <<
    // relationGraph[targetVertexId].sequence_reference->get_label() << "> with
    // relation <" << relation->get_label() << ">" << std::endl;
    // cout << "EdgeId = " << edgeId << std::endl;
    // cout << "relation_is_composite = " << relation->get_is_composite() <<
    // std::endl;

    relationGraph[edgeId].relation_reference = relation;
    relationGraph[edgeId].relation_is_composite = relation->get_is_composite();
    relationGraph[edgeId].relation_is_inverse = false;

    weight = 1;
    if (relation->get_is_composite()) {
      weight++;
    }

    relationGraph[edgeId].weight = weight;

    if (!relation->get_is_composite()) {
      add_edge(targetVertexId, sourceVertexId, relationGraph);
      edgeId = boost::edge(targetVertexId, sourceVertexId, relationGraph).first;

      // cout << "Adding edge from <" <<
      // relationGraph[targetVertexId].sequence_reference->get_label() << "> ";
      // cout << "to <" <<
      // relationGraph[sourceVertexId].sequence_reference->get_label() << ">
      // with relation <" << relation->get_label() << ">" << std::endl;
      // cout << "EdgeId = " << edgeId << std::endl;

      relationGraph[edgeId].relation_reference = relation;
      relationGraph[edgeId].relation_is_composite =
          relation->get_is_composite();
      relationGraph[edgeId].relation_is_inverse = true;
      relationGraph[edgeId].weight = 1;
    }
  }
}

void RelationGraph::remove_sequence(sequence::Sequence *seq) {
  try {
    RelationGraph::VertexID vertexId = this->get_vertex_id(seq->get_label());
    boost::clear_vertex(vertexId, relationGraph);
    boost::remove_vertex(vertexId, relationGraph);
  } catch (RootsException e) {
    // cerr << e.what() << std::endl;
    // throw RootsException(__FILE__, __LINE__, "Missing vertex in graph!\n");
  }
}

void RelationGraph::remove_relation(Relation *relation) {
  // TODO : free manged relation ?!!
  //	sequence::Sequence *sourceSequence=NULL, *targetSequence=NULL;
  std::string srcSeqLabel, trgSeqLabel;
  std::string relationLabel;
  RelationGraph::OutEdgeIterator edgeIter, edgeIterEnd;
  RelationGraph::VertexID sourceVertexId;
  RelationGraph::VertexID targetVertexId;
  EdgeID edgeId, reverseEdgeId;
  bool found = false;
  bool foundReverse = false;

  if (relation == NULL) {
    std::stringstream ss;
    ss << "Relation parameter is NULL!\n";
    throw RootsException(__FILE__, __LINE__, ss.str().c_str());
  }

  relationLabel = relation->get_label();

  //	if(this->get_relation(relationLabel) == NULL)
  //	{
  //		std::stringstream ss;
  //		ss << "Relation \"" + this->get_label() + "\" is not contained
  // in
  // this utterance!\n";
  //		throw RootsException(__FILE__, __LINE__, ss.str().c_str());
  //	}

  srcSeqLabel = relation->get_source_label();
  trgSeqLabel = relation->get_target_label();

  try {
    sourceVertexId = this->get_vertex_id(srcSeqLabel);
    targetVertexId = this->get_vertex_id(trgSeqLabel);
  } catch (RootsException e) {
    // cerr << e.what() << std::endl;
    std::stringstream ss;
    ss << "Missing vertex in graph!" << std::endl << e.what() << std::endl;
    throw RootsException(__FILE__, __LINE__, ss.str());
  }

  boost::tie(edgeIter, edgeIterEnd) = out_edges(sourceVertexId, relationGraph);
  found = false;

  roots::RelationGraph::OutEdgeIterator relationPosition;
  while (edgeIter != edgeIterEnd && !found) {
    found = (relationLabel ==
             (relationGraph[(*edgeIter)].relation_reference->get_label()));
    relationPosition = edgeIter;
    ++edgeIter;
  }

  if (!found) {
    std::stringstream ss;
    ss << "Relation \"" + relation->get_label() +
              "\" is not contained in the graph!\n";
    throw RootsException(__FILE__, __LINE__, ss.str().c_str());
  }

  boost::tie(edgeIter, edgeIterEnd) = out_edges(targetVertexId, relationGraph);
  foundReverse = false;
  roots::RelationGraph::OutEdgeIterator reverseRelationPosition;

  while (edgeIter != edgeIterEnd && !foundReverse) {
    foundReverse =
        (relationLabel ==
         (relationGraph[(*edgeIter)].relation_reference->get_label()));
    reverseRelationPosition = edgeIter;
    ++edgeIter;
  }

  if (relationGraph[*relationPosition].relation_reference == relation) {
    boost::remove_edge(*relationPosition, relationGraph);

    if (foundReverse) {
      boost::remove_edge(*reverseRelationPosition, relationGraph);
    }
  }
}

std::vector<Relation *> RelationGraph::get_relation_path(
    const std::string &source_sequence, const std::string &target_sequence,
    std::vector<bool> &inverseRelation) {
  std::vector<RelationGraph::VertexID> p(
      boost::num_vertices(relationGraph));                 // Predecessors
  std::vector<int> d(boost::num_vertices(relationGraph));  // Distances
  RelationGraph::VertexID sourceVertexId, targetVertexId;
  std::vector<Relation *> result;

  try {
    sourceVertexId = this->get_vertex_id(source_sequence);
    targetVertexId = this->get_vertex_id(target_sequence);
  } catch (RootsException e) {
    // cerr << e.what();
    throw RootsException(__FILE__, __LINE__, "Missing vertex in graph!\n");
  }

  // cout << "found source vertex : " <<
  // relationGraph[sourceVertexId].sequence_reference->get_label() << std::endl;
  // cout << "found target vertex : " <<
  // relationGraph[targetVertexId].sequence_reference->get_label() << std::endl;
  // fflush(stdout);

  try {  // Encore une fois, la découverte d'un chemin est signalée par une
         // exception, donc il faut un try/catch.
    boost::astar_search(
        relationGraph, sourceVertexId,
        boost::astar_heuristic<Graph,
                               float>(),  // No heuristic, complete search
        boost::predecessor_map(&p[0])
            .distance_map(&d[0])
            .visitor(astar_goal_visitor(targetVertexId))
            .weight_map(boost::get(&RelationGraphEdge::weight, relationGraph)));

  } catch (found_goal fg) {  // Exception thrown when a path has been found
    // cout << "found path!" << std::endl;
    // fflush(stdout);

    std::list<RelationGraph::VertexID> shortest_path;
    RelationGraph::VertexID v = targetVertexId;

    while (p[v] != v) {
      shortest_path.push_front(v);
      v = p[v];
    }

    std::list<RelationGraph::VertexID>::iterator spi = shortest_path.begin();
    sequence::Sequence *seq = relationGraph[sourceVertexId].sequence_reference;
    // cout << seq->get_label();

    while (spi != shortest_path.end()) {
      Relation *relationInPath;
      bool isInverse;

      try {
        EdgeID edgeId =
            get_edge_id(seq->get_label(),
                        relationGraph[(*spi)].sequence_reference->get_label());

        relationInPath = relationGraph[edgeId].relation_reference;
        isInverse = relationGraph[edgeId].relation_is_inverse;

        //			cout << "- edge from <" <<
        // relationGraph[edgeId].relation_reference->get_source_sequence()->get_label()
        //<< "> ";
        //				cout << "to <" <<
        // relationGraph[edgeId].relation_reference->get_target_sequence()->get_label()
        //<< "> with relation <" <<
        // relationGraph[edgeId].relation_reference->get_label() << ">" <<
        // std::endl;
        //				cout << "EdgeId = " << edgeId <<
        // std::endl;
        //				cout << "is_inverse = " <<
        // node.is_inverse
        //<<
        // std::endl;
        //				cout << "relationLabel = " <<
        // node.relation->get_label() << std::endl;

      } catch (RootsException e) {
        std::stringstream ss;
        // cerr << e.what();
        ss << "Relation is not contained in the graph!\n";
        throw RootsException(__FILE__, __LINE__, ss.str().c_str());
      }

      result.push_back(relationInPath);
      inverseRelation.push_back(isInverse);

      // cout << " -> " << node.targetSequence->get_label();

      seq = relationGraph[(*spi)].sequence_reference;

      spi++;
    }

    // cout << std::endl;
    // cout << "Total travel time: " << d[targetVertexId] << std::endl;
  }

  return result;
}

std::string RelationGraph::to_dot(void) {
  RelationGraph::VertexIterator vertexIter, vertexIterEnd;
  RelationGraph::OutEdgeIterator edgeIter, edgeIterEnd;
  RelationGraph::VertexID sourceVertexId;

  std::stringstream streamDot, streamNodeName;
  std::vector<RelationGraph::VertexID> nodeId;
  std::vector<std::string> nodeNameArray;
  std::vector<std::string> nodeLabelArray;
  std::string nodeName, sourceNodeName, targetNodeName;
  std::map<std::string, std::string> hashNode;
  int nNode, k;

  streamDot << "digraph relation_graph {" << std::endl;

  k = 0;
  for (boost::tie(vertexIter, vertexIterEnd) = vertices(relationGraph);
       vertexIter != vertexIterEnd; vertexIter++) {
    nodeId.push_back(*vertexIter);
    nodeName = "node_" + boost::lexical_cast<std::string>(k);
    nodeNameArray.push_back(nodeName);
    nodeLabelArray.push_back(
        relationGraph[(*vertexIter)].sequence_reference->get_label());
    hashNode[relationGraph[(*vertexIter)].sequence_reference->get_label()] =
        nodeName;
    k = k + 1;
  }
  nNode = k;

  for (k = 0; k < nNode; k++) {
    sourceVertexId = nodeId[k];

    boost::tie(edgeIter, edgeIterEnd) =
        out_edges(sourceVertexId, relationGraph);
    while (edgeIter != edgeIterEnd) {
      if (relationGraph[(*edgeIter)].relation_is_inverse) {
        sourceNodeName = hashNode[relationGraph[(*edgeIter)]
                                      .relation_reference->get_source_sequence()
                                      ->get_label()];
        targetNodeName = hashNode[relationGraph[(*edgeIter)]
                                      .relation_reference->get_target_sequence()
                                      ->get_label()];
      } else {
        sourceNodeName = hashNode[relationGraph[(*edgeIter)]
                                      .relation_reference->get_target_sequence()
                                      ->get_label()];
        targetNodeName = hashNode[relationGraph[(*edgeIter)]
                                      .relation_reference->get_source_sequence()
                                      ->get_label()];
      }

      streamDot << sourceNodeName << " -> " << targetNodeName << ";"
                << std::endl;

      edgeIter++;
    }
  }

  for (k = 0; k < nNode; k++) {
    streamDot << nodeNameArray[k] << " [label=\"" << nodeLabelArray[k] << "\"];"
              << std::endl;
  }

  streamDot << "}" << std::endl;

  return std::string(streamDot.str().c_str());
}
}
