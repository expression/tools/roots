/*
  This file is part of Roots.

  Roots is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Roots is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with Roots.  If not, see <http://www.gnu.org/licenses/>.

  Copyright (c) 2013, 2014 IRISA
*/
#include "Base.h"
#include "Align/Levenshtein.h"
#include "Linguistic/Grapheme.h"
#include "md5.h"

const icu::Locale roots::Base::currentLocale = icu::Locale::getFrance();

const double roots::Base::MIN_SIMILARITY = 0.0;
const double roots::Base::MAX_SIMILARITY = 1.0;
const double roots::Base::MIN_DISSIMILARITY = roots::Base::MIN_SIMILARITY;
const double roots::Base::MAX_DISSIMILARITY = roots::Base::MAX_SIMILARITY;

roots::Base::Base(const bool noInit) {
  set_label("");
  if (noInit) {
    set_raw_timestamp(0);
  } else {
    set_timestamp();
  }
  description = NULL;
}

roots::Base::Base(const std::string& aLabel, const bool noInit)
    : label(aLabel) {
  if (noInit) {
    set_raw_timestamp(0);
  } else {
    set_timestamp();
  }
  description = NULL;
}

roots::Base::Base(const Base& base) {
  set_label(base.get_label());
  set_timestamp();  // Need for a new timestamp to get a new ID
  //   set_raw_timestamp(base.get_raw_timestamp());
  description = NULL;
  set_description(base.get_description());
}

roots::Base::~Base() { remove_description(); }

void roots::Base::destroy() { delete this; }

roots::Base* roots::Base::clone() const { return new roots::Base(*this); }

roots::Base& roots::Base::operator=(const roots::Base& base) {
  set_raw_timestamp(base.get_raw_timestamp());
  set_label(base.get_label());
  set_description(base.get_description());

  return *this;
}

void roots::Base::set_description(const std::string& descr) {
  // Remove previews description
  remove_description();

  description = new std::string(descr);
}

void roots::Base::set_description(const std::string* descr) {
  // Remove previews description
  remove_description();

  if (descr != NULL) {
    description = new std::string(*descr);
  } else {
    description = NULL;
  }
}

void roots::Base::remove_description() {
  if (description != NULL) {
    delete description;
  }
  description = NULL;
}

void roots::Base::set_timestamp() {
  uint64_t ts;
#ifdef __i386
  __asm__ volatile("rdtsc" : "=A"(ts));
#elif defined __amd64
  uint64_t a, d;
  __asm__ volatile("rdtsc" : "=a"(a), "=d"(d));
  ts = (d << 32) | a;
#else
  struct timeval tval;
  if (gettimeofday(&tval, NULL) == -1) {
    throw RootsException(__FILE__, __LINE__, "cannot compute timestamp!\n");
  }
  ts = (tval.tv_sec << 32) | tval.tv_usec;
#endif

  timestamp = ts;
}

void roots::Base::set_raw_timestamp(const uint64_t& ts) { timestamp = ts; }

const std::string roots::Base::get_timestamp() const {
  std::stringstream str_usec;
  str_usec << timestamp;
  return std::string(str_usec.str().c_str());
}

const uint64_t& roots::Base::get_raw_timestamp() const { return timestamp; }

const std::string* roots::Base::get_description() const { return description; }

std::string roots::Base::generate_id_key() const {
  std::stringstream buff;
  buff << get_classname() << " - " << get_timestamp();
  return buff.str();
}

const std::string roots::Base::get_id() const {
/*
  unsigned char digest[MD5_DIGEST_LENGTH];

  std::string str = generate_id_key();
  MD5((unsigned char*)str.c_str(), str.length(), digest);

  std::string result;
  result.reserve(32);
  for (std::size_t i = 0; i < MD5_DIGEST_LENGTH; ++i) {
    result += "0123456789abcdef"[digest[i] / 16];
    result += "0123456789abcdef"[digest[i] % 16];
  }

  return std::string(result.c_str());
 */
  std::string str = generate_id_key();
  MD5 md5;
  md5.digestString(str.c_str(), (size_t)str.length() * sizeof(char));

  std::string result;
  result.reserve(32);
  for (std::size_t i = 0; i < sizeof(md5.digestChars); ++i) {
    result += "0123456789abcdef"[md5.digestChars[i] / 16];
    result += "0123456789abcdef"[md5.digestChars[i] % 16];
  }

  return std::string(result.c_str());
}

const std::string& roots::Base::get_label() const { return label; }

std::string roots::Base::get_pretty_timestamp() const {
  double sec = (double)((timestamp & 0xFFFFFFFF00000000) >> 32);
  double usec = (double)(timestamp & 0xFFFFFFFF);
  time_t isec = sec;

  std::string tstamp(ctime(&isec));

  std::stringstream str_usec;
  str_usec << usec;
  std::string ts(str_usec.str());
  ts.append(" - ").append(tstamp);

  return ts;
}

void roots::Base::set_timestamp(const std::string& value) {
  std::stringstream buff;
  buff << value;
  timestamp = boost::lexical_cast<uint64_t>(buff.str());
}

namespace roots {

double Base::compute_dissimilarity(const Base* b) const {
  // TODO: allow inheritance
  if (b->get_classname() != this->get_classname()) {
    return MAX_DISSIMILARITY;
  } else {
    std::string this_str = this->to_string();
    std::string b_str = b->to_string();
    std::vector<char> this_data(this_str.begin(), this_str.end());
    std::vector<char> bi_data(b_str.begin(), b_str.end());
    align::Levenshtein alg(this_data, bi_data);
    alg.align();
    return alg.get_dissimilarity();
  }
}

double Base::compute_similarity(const Base* b) const {
  return MAX_SIMILARITY - compute_dissimilarity(b);
}

std::vector<Base*>* Base::split() {
  std::vector<Base*>* subelements = new std::vector<Base*>();
  std::string str = this->to_string(0);
  size_t n_str = str.size();
  for (size_t i = 0; i < n_str; i++) {
    subelements->push_back(new linguistic::Grapheme(str.substr(i, 1)));
  }
  return subelements;
}

void Base::deflate(RootsStream* stream, bool is_terminal, int list_index) {
  std::stringstream convert;
  convert << this->get_xml_tag_name();

  stream->append_object(
      convert.str().c_str(),
      list_index);  // Creates a new node and set it to current node
  stream->set_object_classname(this->get_classname());
  stream->append_unicodestring_content("id", this->get_id());
  stream->append_unicodestring_content("timestamp", this->get_timestamp());

  const std::string* ptDescription = this->get_description();
  if (ptDescription != NULL && *ptDescription != "") {
    stream->append_unicodestring_content("description", *ptDescription);
  }

  if (is_terminal) stream->close_object();
}

void Base::inflate(RootsStream* stream, bool is_terminal, int list_index) {
  std::stringstream convert;
  convert << this->get_xml_tag_name();

  stream->open_object(convert.str().c_str(), list_index);
  stream->open_children();

  //     Not used
  //     std::string classname = stream->get_object_classname();
  std::string idString = stream->get_unicodestring_content("id");
  // this->set_id(idString);
  this->set_timestamp(stream->get_unicodestring_content("timestamp"));
  if (stream->has_child("description")) {
    std::string tmpDescr = stream->get_unicodestring_content("description");
    if (tmpDescr != "") {
      this->set_description(tmpDescr);
    } else {
      this->set_description(NULL);
    }
  } else {
    this->set_description(NULL);
  }

  if (is_terminal) stream->close_children();

  //    std::cerr << "Compare : "<< idString << " To " << get_id() << std::endl;

  stream->add_object_to_list(idString, this);
  // stream->add_object_to_list(get_id(),this);
}

std::string Base::to_string(int level) const {
  std::stringstream ss;

  switch (level) {
    // level 0 : precise POS tag
    case 0:
      ss << get_label();
      break;
    case 1:
      ss << "label = " << get_label()
         << ", timestamp = " << get_pretty_timestamp() << ", id = " << get_id();
      break;
    default:
      throw RootsException(__FILE__, __LINE__,
                           "unknown value for 'level' argument!\n");
  }
  return std::string(ss.str().c_str());
}

icu::UnicodeString Base::to_unistr(int level) const {
  std::stringstream ss;
  ss << this->to_string(level);
  return icu::UnicodeString(ss.str().c_str());
}

std::ostream& operator<<(std::ostream& out, roots::Base& seg) {
  return out << seg.to_string() << std::endl;
}

bool operator==(Base const& a, Base const& b)  // DG: In non-member function to
                                               // avoid issues with casted
                                               // objects
{
  return (a.get_label() == b.get_label());
}
}
