/*
  This file is part of Roots.

  Roots is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Roots is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with Roots.  If not, see <http://www.gnu.org/licenses/>.

  Copyright (c) 2013, 2014 IRISA
*/

#include "Utterance.h"

#include "RootsDisplay.h"

#include "Sequence/AllophoneSequence.h"
#include "Sequence/AtomSequence.h"
#include "Sequence/FillerSequence.h"
#include "Sequence/GraphemeSequence.h"
#include "Sequence/NamedEntitySequence.h"
#include "Sequence/NssSequence.h"
#include "Sequence/NumberSequence.h"
#include "Sequence/EmbeddingSequence.h"
#include "Sequence/PhonemeBlockSequence.h"
#include "Sequence/PhonemeSequence.h"
#include "Sequence/PosSequence.h"
#include "Sequence/ProsodicPhraseSequence.h"
#include "Sequence/SegmentSequence.h"
#include "Sequence/SyllableSequence.h"
#include "Sequence/SymbolSequence.h"
#include "Sequence/SyntaxSequence.h"
#include "Sequence/VoidSequence.h"
#include "Sequence/WordSequence.h"

namespace roots {

Utterance::Utterance() {
  sequenceHash = t_sequence_hash();
  relationHash = t_relation_hash();
  relationGraph = RelationGraph();
  cached_relation = t_relation_hash();
  use_cached_relation = false;
}

Utterance::Utterance(const Utterance& utt) : Base(utt) {
  sequenceHash = t_sequence_hash();
  relationHash = t_relation_hash();
  relationGraph = RelationGraph();
  cached_relation = t_relation_hash();
  use_cached_relation = utt.use_cached_relation;
  this->concatenate_utterance(&utt);
}

Utterance::~Utterance() { clear(); }

void Utterance::clear() {
  t_relation_hash::iterator itr;
  t_sequence_hash::iterator its;
  t_sequence_hash_label::iterator itls;

  flush_cached_relation();

  // Clear the relation graph
  relationGraph.clear();

  for (itr = relationHash.begin(); itr != relationHash.end(); ++itr) {
    // std::cerr << itr->second->to_string() << std::endl;
    Relation* rel = itr->second;
    rel->remove_from_utterance(this, false);
    rel->destroy();
  }
  relationHash.clear();

  for (its = sequenceHash.begin(); its != sequenceHash.end(); ++its) {
    for (itls = (its->second).begin(); itls != (its->second).end(); ++itls) {
      itls->second->remove_from_utterance(this, false);
      itls->second->destroy();
    }
    (its->second).clear();
  }
  sequenceHash.clear();
}

Utterance* Utterance::clone() const { return new Utterance(*this); }

Utterance& Utterance::operator=(const Utterance& utt) {
  Base::operator=(utt);

  // Clear myself
  clear();

  // Copy everything
  use_cached_relation = utt.use_cached_relation;

  this->concatenate_utterance(&utt);
  return *this;
}

unsigned int Utterance::get_n_sequence(const std::string& contentType) const {
  if (contentType == "") {
    return sequenceHash.size();
  } else {
    t_sequence_hash::const_iterator it = sequenceHash.begin();
    it = sequenceHash.find(contentType);
    if (it != sequenceHash.end()) {
      return (*it).second.size();
    } else {
      return 0;
    }
  }
}

bool Utterance::is_valid_sequence(const std::string& label,
                                  const std::string& contentType) const {
  t_sequence_hash::const_iterator it;
  t_sequence_hash_label::const_iterator itseq;

  if (contentType == "") {
    for (it = sequenceHash.begin(); it != sequenceHash.end(); ++it) {
      itseq = (*it).second.find(label);
      if (itseq != (*it).second.end()) {
        return true;
      }
    }
  } else {
    it = sequenceHash.find(contentType);
    if (it != sequenceHash.end()) {
      itseq = (*it).second.find(label);
      if (itseq != (*it).second.end()) {
        return true;
      }
    }
  }
  return false;
}

std::vector<std::string>* Utterance::get_all_sequence_labels(
    const std::string& contentType) const {
  std::vector<std::string>* labelVector = new std::vector<std::string>();
  t_sequence_hash::const_iterator it = sequenceHash.begin();
  t_sequence_hash_label::const_iterator itseq;
  t_sequence_hash_label::const_iterator itseq_end;

  if (contentType == "") {
    while (it != sequenceHash.end()) {
      itseq = (*it).second.begin();
      itseq_end = (*it).second.end();
      while (itseq != itseq_end) {
        labelVector->push_back(((*itseq).first));
        itseq++;
      }

      it++;
    }
  } else {
    it = sequenceHash.find(contentType);
    if (it != sequenceHash.end()) {
      itseq = (*it).second.begin();
      itseq_end = (*it).second.end();
      while (itseq != itseq_end) {
        labelVector->push_back(((*itseq).first));
        itseq++;
      }
    }
  }

  return labelVector;
}

std::vector<sequence::Sequence*>* Utterance::get_all_sequences(
    const std::string& contentType) const {
  std::vector<sequence::Sequence*>* sequenceVector =
      new std::vector<sequence::Sequence*>();
  t_sequence_hash::const_iterator it = sequenceHash.begin();
  t_sequence_hash_label::const_iterator itseq;
  t_sequence_hash_label::const_iterator itseq_end;

  if (contentType == "") {
    while (it != sequenceHash.end()) {
      itseq = (*it).second.begin();
      itseq_end = (*it).second.end();
      while (itseq != itseq_end) {
        sequenceVector->push_back(((*itseq).second));
        itseq++;
      }

      it++;
    }
  } else {
    it = sequenceHash.find(contentType);
    if (it != sequenceHash.end()) {
      itseq = (*it).second.begin();
      itseq_end = (*it).second.end();
      while (itseq != itseq_end) {
        sequenceVector->push_back(((*itseq).second));
        itseq++;
      }
    }
  }

  return sequenceVector;
}

sequence::Sequence* Utterance::get_sequence(
    const std::string& label, const std::string& contentType) const {
  sequence::Sequence* seq = NULL;
  t_sequence_hash::const_iterator it;
  t_sequence_hash_label::const_iterator itseq;

  // cout << "Utterance::get_sequence : " << label << std::endl;

  if (contentType == "") {
    it = sequenceHash.begin();
    while (it != sequenceHash.end() && seq == NULL) {
      itseq = (*it).second.find(label);
      if (itseq != (*it).second.end()) seq = (*itseq).second;
      it++;
    }
  } else {
    it = sequenceHash.find(contentType);
    if (it != sequenceHash.end()) {
      itseq = (*it).second.find(label);
      if (itseq != (*it).second.end()) seq = (*itseq).second;
    }
  }

  if (seq == NULL) {
    ROOTS_LOGGER_DEBUG(std::string("Utt: Asking for nonexistent sequence: \"") +
                       label + std::string("\" (type = \"") + contentType +
                       std::string("\")"));
  }

  return seq;
}

sequence::Sequence* Utterance::add_sequence_copy(
    const sequence::Sequence& sequence) {
  sequence::Sequence* seqCopy = sequence.clone();
  add_sequence(seqCopy);
  return seqCopy;
}

void Utterance::add_sequence(sequence::Sequence* seq,
                             const std::string& label) {
  add_sequence_no_propagate(seq, label);

  if (seq->has_related_sequence()) {
    ROOTS_LOGGER_DEBUG(std::string("Utt add embedded rel from: ") +
                       seq->get_label());
    // add_relation will add the related sequence to
    sequence::Sequence* seqRel = seq->get_related_sequence();
    std::string label = seqRel->get_label();
    if (!is_valid_sequence(label)) {
      add_sequence(seqRel, label);
    }

    relationGraph.add_relation(seq->get_embedded_relation());
  }
}

void Utterance::add_sequence(sequence::Sequence* sequence) {
  // Try to get a label if ever it is not precised as an argument
  add_sequence(sequence, sequence->get_label());
}

void Utterance::add_sequence_no_propagate(sequence::Sequence* sequence,
                                          const std::string& label) {
  std::string contentType;

  if (is_valid_sequence(label)) {
    std::stringstream ss;
    ss << "Sequence \"" + label + "\" already exists!\n";
    throw RootsException(__FILE__, __LINE__, ss.str().c_str());
  } else {
    contentType = sequence->get_content_type();
    if (sequenceHash.find(contentType) == sequenceHash.end()) {
      // First time we add this content type
      sequenceHash[contentType] = t_sequence_hash_label();
    }

    if (label != "") {
      sequence->set_label(label);
    }

    ROOTS_LOGGER_DEBUG(std::string("Utt add seq: ") + sequence->get_label());
    sequenceHash[contentType][sequence->get_label()] = sequence;

    sequence->add_in_utterance(this);
  }
}

void Utterance::update_sequence(sequence::Sequence* sequence,
                                const std::string& label) {
  std::string label_used = label;
  if (label == "") {
    label_used = sequence->get_label();
  }
  if (is_valid_sequence(label_used)) {
    if (get_sequence(label_used) != sequence) {
      sequence::Sequence* removed = remove_sequence(label_used);
      removed->destroy();
      add_sequence(sequence, label_used);
    }
  } else {
    add_sequence(sequence, label_used);
  }
}

std::vector<sequence::Sequence*> Utterance::rename_sequence(
    const std::string& old_name, const std::string& new_name,
    const std::string& contentType) {
  t_sequence_hash_label::iterator itseq;
  t_sequence_hash_label::iterator itprevseq;
  std::vector<sequence::Sequence*> removed_seqs;
  sequence::Sequence* seq = NULL;

  if (old_name != new_name) {
    // Delete the previous sequence if the new was used already
    while (is_valid_sequence(new_name, contentType)) {
      removed_seqs.push_back(remove_sequence(new_name, contentType));
    }

    t_sequence_hash::iterator it;
    t_sequence_hash::iterator end = sequenceHash.end();
    if (contentType == "") {
      it = sequenceHash.begin();
    } else {
      it = sequenceHash.find(contentType);
      if (it != sequenceHash.end()) {
        end = it;
        ++end;
      }
    }

    while (it != end) {
      itseq = (*it).second.find(old_name);
      if (itseq != (*it).second.end() && itseq->second != NULL) {
        // Change the association in the sequence hash
        seq = itseq->second;
        it->second.erase(itseq);
        it->second[new_name] = seq;

        if (seq->get_label() != new_name) {
          seq->set_label(new_name);
        }
      }
      it++;
    }
  }
  return removed_seqs;
}

std::vector<sequence::Sequence*> Utterance::rename_sequence(
    sequence::Sequence* sequence, const std::string& new_name) {
  std::vector<sequence::Sequence*> removed_seqs;

  t_sequence_hash::iterator it;
  t_sequence_hash::iterator end;
  t_sequence_hash_label::iterator itseq;
  t_sequence_hash_label::iterator itprevseq;
  sequence::Sequence* seq = NULL;

  // cout << "Utterance::rename_sequence : " << old_name << " -> " << new_name
  // << std::endl;

  // Delete the previous sequence if the new was used already
  if (is_valid_sequence(new_name, sequence->get_content_type())) {
    seq = get_sequence(new_name, sequence->get_content_type());
    if (seq != sequence) {
      remove_sequence(seq);
      removed_seqs.push_back(seq);
    }
  }

  it = sequenceHash.find(sequence->get_content_type());
  end = it;
  ++end;
  while (it != end) {
    for (itseq = (*it).second.begin(); itseq != (*it).second.end(); ++itseq) {
      if (itseq->second == sequence) {
        // Change the association in the sequence hash
        seq = itseq->second;
        it->second.erase(itseq);
        it->second[new_name] = seq;

        if (seq->get_label() != new_name) {
          seq->set_label(new_name);
        }
        return removed_seqs;
      }
    }
    it++;
  }
  return removed_seqs;
}

void Utterance::no_more_linked(UtteranceLinkedObject* ulo) {
  sequence::Sequence* seq = dynamic_cast<sequence::Sequence*>(ulo);
  if (seq != NULL) {
    remove_sequence(seq);
  }
  Relation* rel = dynamic_cast<Relation*>(ulo);
  if (rel != NULL) {
    remove_relation(rel);
  }
}

sequence::Sequence* Utterance::remove_sequence(const std::string& label,
                                               const std::string& contentType) {
  t_sequence_hash::iterator it;
  t_sequence_hash_label::iterator itseq;
  bool found = false;

  sequence::Sequence* seq = NULL;

  if (contentType == "") {
    it = sequenceHash.begin();
    found = false;
    while (it != sequenceHash.end() && !found) {
      itseq = (it->second).find(label);
      if (itseq != it->second.end()) {
        found = true;
        seq = &(*itseq->second);
        seq->remove_from_utterance(this, false);
        it->second.erase(itseq);
        ROOTS_LOGGER_DEBUG(std::string("Utt del seq: ") + label);
      }

      ++it;
    }
  } else {
    it = sequenceHash.find(contentType);
    if (it != sequenceHash.end()) {
      itseq = it->second.find(label);
      if (itseq != it->second.end()) {
        seq = &(*itseq->second);
        seq->remove_from_utterance(this, false);
        it->second.erase(itseq);
        ROOTS_LOGGER_DEBUG(std::string("Utt del seq: ") + label);
      }
    }
  }

  if (seq != NULL) {
    std::vector<Relation*> relationToRemove = seq->get_all_relations();
    for (std::vector<Relation*>::iterator itRel = relationToRemove.begin();
         itRel != relationToRemove.end(); ++itRel) {
      remove_relation(*itRel);
    }
    relationGraph.remove_sequence(seq);
  }

  return seq;
}

bool Utterance::remove_sequence(const sequence::Sequence* seq) {
  return (remove_sequence(seq->get_label(), seq->get_content_type()) == NULL);
}

unsigned int Utterance::get_n_relation() const { return relationHash.size(); }

bool Utterance::is_valid_relation(const std::string& label) {
  return (get_relation(label) != NULL);
}

bool Utterance::is_valid_relation(const std::string& srcSeqLabel,
                                  const std::string& trgSeqLabel) {
  Relation* rel = get_relation(srcSeqLabel, trgSeqLabel);
  if (rel != NULL) {
    rel->destroy();
    return true;
  } else {
    return false;
  }
}

bool Utterance::is_valid_relation(const roots::sequence::Sequence* srcSeq,
                                  const roots::sequence::Sequence* trgSeq) {
  Relation* rel = get_relation(srcSeq, trgSeq);
  if (rel != NULL) {
    rel->destroy();
    return true;
  } else {
    return false;
  }
}

Relation* Utterance::add_relation_copy(const Relation& rel) {
  Relation* reCopy = rel.clone();
  add_relation(reCopy);
  return reCopy;
}

void Utterance::add_relation(Relation* rel) {
  Matrix* matrix;
  std::string label = rel->get_label();

  if (relationHash.find(label) != relationHash.end()) {
    std::stringstream ss;
    ss << "Relation \"" << label << "\" already exists!\n";
    ROOTS_LOGGER_DEBUG(ss.str());
    throw RootsException(__FILE__, __LINE__, ss.str().c_str());
  } else {
    matrix = rel->get_mapping();
    if (matrix != NULL) {
      if (rel->get_source_sequence()->has_related_sequence() &&
          (rel->get_source_sequence()->get_related_sequence() ==
           rel->get_target_sequence())) {
        // Embeded relation from source to target
        // do not add in relation hash
        ROOTS_LOGGER_DEBUG(std::string("Utt add rel (embeded S->T): ") + label);
      } else if (rel->get_target_sequence()->has_related_sequence() &&
                 (rel->get_target_sequence()->get_related_sequence() ==
                  rel->get_source_sequence())) {
        // Embeded relation from target to source
        // do not add in relation hash
        ROOTS_LOGGER_DEBUG(std::string("Utt add rel (embeded T->S): ") + label);
      } else {
        // Std relation
        ROOTS_LOGGER_DEBUG(std::string("Utt add rel: ") + label);

        rel->add_in_utterance(this);  // do not add if not in hash or cache
        relationHash[label] = rel;
      }

      // add the sequences too
      if (!is_valid_sequence(rel->get_source_label(),
                             rel->get_source_sequence()->get_content_type())) {
        add_sequence(rel->get_source_sequence(), rel->get_source_label());
      }
      if (!is_valid_sequence(rel->get_target_label(),
                             rel->get_target_sequence()->get_content_type())) {
        add_sequence(rel->get_target_sequence(), rel->get_target_label());
      }
//      rel->set_is_composite(false); // Warning, if the relation is composite, then only one edge is created !!
      relationGraph.add_relation(rel);
    }
  }
}

void Utterance::update_relation(Relation* relation) {
  const std::string relLabel = relation->get_label();
  if (get_relation(relLabel) != relation) {
    if (is_valid_relation(relLabel)) {
      Relation* removed = remove_relation(relLabel);
      removed->destroy();
     } else {
      // Look directly in the hash table to check if the hash table key is not up to date
      // I.E. the relation was renamed when its sequences were renamed.
      t_relation_hash::iterator it = relationHash.begin();
      while((it !=relationHash.end())&& (it->second != relation)){
        ++it;
      }
      if(it !=relationHash.end()){
        Relation* removed =  it->second;
        std::string label = removed->get_label();
        // remove the relation then add it "just" to rename it
//        remove_relation(it->second); // this cannot be used as this relation is not in the relationHash...
        removed->remove_from_utterance(this, false);
        relationGraph.remove_relation(removed);
        relationHash.erase(it);  // Sequences are unlinked by ~Relation()

        /* Remove from cache, FIXME : we will not be able to find it with the label... */
        /*
        t_relation_hash::iterator it = cached_relation.find(label);
        if (it != cached_relation.end()) {
          if (it->second->get_is_composite()) {
            it->second->remove_from_utterance(this, false);
            it->second->destroy();
          }
          cached_relation.erase(it);
        }
        */

        ROOTS_LOGGER_DEBUG(std::string("Utt update relation: ") + label);
        // Do not destroy since it is the same relation
      }
    }
    add_relation(relation);
  }
}

roots::Relation* Utterance::remove_relation(const std::string& label) {
  if (relationHash.find(label) == relationHash.end()) {
    /*
      std::stringstream ss;
      ss << "Relation \"" << label << "\" does not exists!\n";
      throw RootsException(__FILE__, __LINE__, ss.str().c_str());
    */
    return NULL;
  } else {
    roots::Relation* rel = get_relation(label);
    rel->remove_from_utterance(this, false);
    relationGraph.remove_relation(rel);
    relationHash.erase(label);  // Sequences are unlinked by ~Relation()

    /* Remove from cache */
    t_relation_hash::iterator it = cached_relation.find(label);
    if (it != cached_relation.end()) {
      if (it->second->get_is_composite()) {
        it->second->remove_from_utterance(this, false);
        it->second->destroy();
      }
      cached_relation.erase(it);
    }

    ROOTS_LOGGER_DEBUG(std::string("Utt del relation: ") + label);
    return rel;
  }
}

bool Utterance::remove_relation(const roots::Relation* rel) {
  return (remove_relation(rel->get_label()) != NULL);
}

Relation* Utterance::get_relation(const std::string& label) const {
  Relation* rel = NULL;
  t_relation_hash::const_iterator it;

  it = relationHash.find(label);
  if (it != relationHash.end()) {
    rel = (*it).second;
  }

  return rel;
}

bool Utterance::linked(const std::string& srcSeqLabel,
                       const std::string& trgSeqLabel) {
  //     return (get_relation_between_sequence(srcSeq,
  // trgSeq) != NULL);
  try {
    return !relationGraph.get_edge(srcSeqLabel, trgSeqLabel)
                .relation_is_composite;
  } catch (RootsException e) {
    return false;
  }
}

bool Utterance::linked(const sequence::Sequence* srcSeq,
                       const sequence::Sequence* trgSeq) {
  return linked(srcSeq->get_label(), trgSeq->get_label());
}

Relation* Utterance::get_relation_between_sequence(const std::string& srcSeq,
                                                   const std::string& trgSeq) {
  std::string relationLabel = Relation::generic_label(srcSeq, trgSeq);
  return get_relation(relationLabel);
}

Relation* Utterance::get_relation(const std::string& srcSeq,
                                  const std::string& trgSeq) {
  Relation* globalRelation = NULL;

  bool computeRelation = true;

  if (use_cached_relation) {
    t_relation_hash::const_iterator it =
        cached_relation.find(Relation::generic_label(srcSeq, trgSeq));
    if (it != cached_relation.end()) {
      globalRelation = it->second;
      computeRelation = false;
    }
  }

  if (computeRelation) {
    if (srcSeq == trgSeq) {
      /* Identity case */
      // TODO move the identity matrix building in the martix source file !
      sequence::Sequence* currentSeq = this->get_sequence(srcSeq);
      int matrixSize = currentSeq->count();

      roots::SparseMatrix* mapping =
          new roots::SparseMatrix(matrixSize, matrixSize);
      for (int i = 0; i < matrixSize; ++i) {
        mapping->set_element(i, i, 1);
      }

      globalRelation = new Relation(currentSeq, currentSeq, mapping);
      globalRelation->set_is_composite(true);
    } else {
      std::vector<bool> inverseRelationFlag;
      std::vector<Relation*> path;
      int pathLength = 0;

      try {
        path = relationGraph.get_relation_path(srcSeq, trgSeq,
                                               inverseRelationFlag);
        pathLength = path.size();
      } catch (RootsException e) {
        ROOTS_LOGGER_DEBUG(
            std::string("get_relation - Exception caught: ") + e.what() +
            std::string("; get_relation will return a null pointer."));
        pathLength = 0;
      }

      int i = 0;
      while (i < pathLength) {
        bool flagInverseRelation = inverseRelationFlag[i];
        Relation* currentRelation = path[i];

        if (i == 0) {
          globalRelation = currentRelation->clone();
          if (flagInverseRelation) {
            globalRelation->inverse();
          }
        } else {
          Relation* relation = currentRelation->clone();
          if (flagInverseRelation) {
            relation->inverse();
          }
          globalRelation->compound_inplace(relation, true);
          relation->destroy();
        }

        i++;
      }
    }
  }

  /* Store relation if use cached relation */
  if (use_cached_relation && computeRelation && globalRelation != NULL) {
    globalRelation->add_in_utterance(this);  // add bc in cache
    cached_relation[globalRelation->get_label()] = globalRelation;
  }

  return globalRelation;
}

Relation* Utterance::get_relation(const sequence::Sequence* srcSeq,
                                  const sequence::Sequence* trgSeq) {
  return get_relation(srcSeq->get_label(), trgSeq->get_label());
}

std::vector<BaseItem*> Utterance::get_related_items(
    unsigned int index, const std::string& sourceLabel,
    const std::string& targetLabel) {
  std::vector<BaseItem*> res;
  Relation* rel = get_relation(sourceLabel, targetLabel);
  if (rel != NULL) {
    res = rel->get_related_items(index);
    rel->destroy();
  }
  return res;
}

std::vector<int> Utterance::get_related_indices(
    unsigned int index, const std::string& sourceLabel,
    const std::string& targetLabel) {
  std::vector<int> res;
  Relation* rel = get_relation(sourceLabel, targetLabel);
  if (rel != NULL) {
    res = rel->get_related_indices(index);
    rel->destroy();
  }
  return res;
}

std::vector<BaseItem*> Utterance::get_related_items(
    unsigned int index, const sequence::Sequence* sourceSequence,
    const sequence::Sequence* targetSequence) {
  if (sourceSequence != NULL && targetSequence != NULL) {
    return get_related_items(index, sourceSequence->get_label(),
                             targetSequence->get_label());
  } else {
    return std::vector<BaseItem*>();
  }
}

void Utterance::set_use_cached_relation(bool use_it) {
  use_cached_relation = use_it;
  flush_cached_relation();
}

bool Utterance::get_use_cached_relation() { return use_cached_relation; }

void Utterance::flush_cached_relation() {
  /* Remove all computed relations stored */
  for (t_relation_hash::iterator it = cached_relation.begin();
       it != cached_relation.end(); ++it) {
    if (it->second->get_is_composite()) { // All utterances added in the cache are copies and must be cleared ? POSSIBLE FIXME
      it->second->remove_from_utterance(this, false);
      it->second->destroy();
    }
  }

  /* Flush hash */
  cached_relation.clear();
}

std::vector<std::string>* Utterance::get_all_relation_labels() const {
  std::vector<std::string>* labelVector = new std::vector<std::string>();

  for (t_relation_hash::const_iterator it = relationHash.begin();
       it != relationHash.end(); ++it) {
    labelVector->push_back((*it).first);
  }

  return labelVector;
}

std::vector<Relation*>* Utterance::get_all_relations() const {
  std::vector<Relation*>* relationVector = new std::vector<Relation*>();

  for (t_relation_hash::const_iterator it = relationHash.begin();
       it != relationHash.end(); ++it) {
    relationVector->push_back((*it).second);
  }

  return relationVector;
}

std::vector<Relation*>* Utterance::get_all_relations(
    const std::string& lab) const {
  std::vector<Relation*>* relationVector = new std::vector<Relation*>();

  for (t_relation_hash::const_iterator it = relationHash.begin();
       it != relationHash.end(); ++it) {
    if ((*it).second->get_source_label() == lab ||
        (*it).second->get_target_label() == lab) {
      relationVector->push_back((*it).second);
    }
  }

  return relationVector;
}

void Utterance::prepare_save(const std::string& target_base_dir) {
  std::vector<sequence::Sequence*>* seqVect = get_all_sequences();

  for (std::vector<sequence::Sequence*>::iterator it = seqVect->begin();
       it != seqVect->end(); ++it) {
    (*it)->prepare_save(target_base_dir);
  }

  delete seqVect;
}

std::string Utterance::to_string(int level) const {
  std::stringstream ss;
  std::vector<sequence::Sequence*>* seqs = get_all_sequences();
  std::vector<Relation*>* rels = get_all_relations();
  unsigned int i;
  int i_level = 0;

  ss << "================================================" << std::endl;
  ss << "Sequences (" << seqs->size() << ")" << std::endl;
  ss << "================================================" << std::endl;
  for (i = 0; i < seqs->size(); i++) {
    ss << "[ " << seqs->at(i)->get_label() << " ]" << std::endl;
    for (i_level = 0; i_level <= level; i_level++) {
      ss << seqs->at(i)->to_string(i_level) << std::endl;
    }
    if (i < seqs->size() - 1) {
      ss << "------------------------------------------------" << std::endl;
    }
  }
  delete (seqs);
  ss << "================================================" << std::endl;
  ss << "Relations (" << rels->size() << ")" << std::endl;
  ss << "================================================" << std::endl;
  for (i = 0; i < rels->size(); i++) {
    ss << rels->at(i)->to_string(level) << std::endl;
    if (i < rels->size() - 1) {
      ss << "------------------------------------------------" << std::endl;
    }
  }
  delete (rels);
  ss << "================================================" << std::endl;
  return std::string(ss.str().c_str());
}

std::string Utterance::to_pgf(int level, bool draw_relations) {
  std::string result;
  std::vector<sequence::Sequence*>* sequences = get_all_sequences();
  std::vector<std::string> sequence_labels;
  std::vector<std::string> related_labels;
  unsigned int max_size = 0;
  unsigned int max_i = 0;
  std::string max_label = "";
  unsigned int n = sequences->size();

  for (unsigned int i = 0; i < n; i++) {
    if ((*sequences)[i]->count() > max_size) {
      max_size = (*sequences)[i]->count();
      max_label = (*sequences)[i]->get_label();
      max_i = i;
    }
  }

  related_labels.push_back("");
  sequence_labels.push_back(max_label);
  for (unsigned int i = 0; i < n; i++) {
    if (i != max_i) {
      related_labels.push_back(max_label);
      sequence_labels.push_back((*sequences)[i]->get_label());
    }
  }

  delete (sequences);

  return to_pgf(sequence_labels, level, draw_relations);
}

std::string Utterance::to_pgf(const std::vector<std::string>& sequencesOrder,
                              const int level, const bool draw_relations) {
  RootsDisplay roots_display;

  return roots_display.to_pgf(*this, sequencesOrder, level, draw_relations);
}

void Utterance::deflate(RootsStream* stream, bool is_terminal, int list_index) {
  Base::deflate(stream, false, list_index);

  // Count the number of sequences to deflate
  unsigned int nb_seq = 0;
  unsigned int sequenceIndex = 0;
  unsigned int relationIndex = 0;
  t_sequence_hash::const_iterator it = sequenceHash.begin();
  while (it != sequenceHash.end()) {
    t_sequence_hash_label::const_iterator it2 = ((*it).second).begin();
    while (it2 != ((*it).second).end()) {
      nb_seq++;
      it2++;
    }
    it++;
  }
  stream->append_uint_content("n_sequence", nb_seq);

  // Count the number of relations to deflate
  t_relation_hash::const_iterator it3 = relationHash.begin();
  unsigned int nb_rel = 0;
  while (it3 != relationHash.end()) {
    /*if (((*it3).second->get_source_sequence()->count() != 0) &&
        ((*it3).second->get_target_sequence()->count() != 0)) {
      nb_rel++;
      }*/
    nb_rel++;
    it3++;
  }
  stream->append_uint_content("n_relation", nb_rel);

  // Deflates of sequence_hash
  //  -> prendre iterator sur le hash
  //  -> faire deflate sur chaque élément
  // stream->append_list("sequence");
  sequenceIndex = 0;
  it = sequenceHash.begin();
  while (it != sequenceHash.end()) {
    t_sequence_hash_label::const_iterator it2 = ((*it).second).begin();
    while (it2 != ((*it).second).end()) {
      if (((*it2).second)->get_is_simple_item()) {
        ((*it2).second)->deflate(stream, true, sequenceIndex);
        sequenceIndex++;
      }
      it2++;
    }
    it++;
  }

  it = sequenceHash.begin();
  while (it != sequenceHash.end()) {
    t_sequence_hash_label::const_iterator it2 = ((*it).second).begin();
    while (it2 != ((*it).second).end()) {
      if (!((*it2).second)->get_is_simple_item()) {
        ((*it2).second)->deflate(stream, true, sequenceIndex);
        sequenceIndex++;
      }
      it2++;
    }
    it++;
  }
  // stream->close_list();

  // Deflates of relation_hash
  //  -> prendre iterator sur le hash
  //  -> faire deflate sur chaque élément
  // stream->append_list("relation");
  relationIndex = 0;
  it3 = relationHash.begin();
  while (it3 != relationHash.end()) {
    ((*it3).second)->deflate(stream, true, relationIndex);
    relationIndex++;    
    /*if (((*it3).second->get_source_sequence()->count() != 0) &&
        ((*it3).second->get_target_sequence()->count() != 0)) {
      ((*it3).second)->deflate(stream, true, relationIndex);
      relationIndex++;
      }*/
    it3++;
  }
  // stream->close_list();
  if (is_terminal) stream->close_object();
}

void Utterance::inflate(RootsStream* stream, bool is_terminal, int list_index) {
  unsigned int n_sequence = 0, n_relation = 0;
  sequence::Sequence* seq = NULL;

  Base::inflate(stream, false, list_index);

  n_sequence = stream->get_uint_content("n_sequence");
  n_relation = stream->get_uint_content("n_relation");

  // read the sequence
  for (unsigned int index_sequence = 0; index_sequence < n_sequence;
       ++index_sequence) {
    std::string sequenceClassname = stream->get_object_classname_no_read(
        sequence::Sequence::xml_tag_name(), index_sequence);

    if (sequenceClassname.compare(sequence::AllophoneSequence::classname()) ==
        0) {
      seq = sequence::AllophoneSequence::inflate_sequence(
          stream, acoustic::Allophone::xml_tag_name(), index_sequence);
    } else if (sequenceClassname.compare(sequence::NssSequence::classname()) ==
               0) {
      seq = sequence::NssSequence::inflate_sequence(
          stream, acoustic::NonSpeechSound::xml_tag_name(), index_sequence);
    } else if (sequenceClassname.compare(
                   sequence::PhonemeSequence::classname()) == 0) {
      seq = sequence::PhonemeSequence::inflate_sequence(
          stream, phonology::Phoneme::xml_tag_name(), index_sequence);
    } else if (sequenceClassname.compare(sequence::PosSequence::classname()) ==
               0) {
      seq = sequence::PosSequence::inflate_sequence(
          stream, linguistic::pos::POS::xml_tag_name(), index_sequence);
    } else if (sequenceClassname.compare(
                   sequence::SegmentSequence::classname()) == 0) {
      seq = sequence::SegmentSequence::inflate_sequence(
          stream, acoustic::Segment::xml_tag_name(), index_sequence);
    } else if (sequenceClassname.compare(
                   sequence::SyllableSequence::classname()) == 0) {
      seq = sequence::SyllableSequence::inflate_sequence(
          stream, phonology::syllable::Syllable::xml_tag_name(),
          index_sequence);
    } else if (sequenceClassname.compare(
                   sequence::SyntaxSequence::classname()) == 0) {
      seq = sequence::SyntaxSequence::inflate_sequence(
          stream, linguistic::syntax::Syntax::xml_tag_name(), index_sequence);
    } else if (sequenceClassname.compare(sequence::WordSequence::classname()) ==
               0) {
      seq = sequence::WordSequence::inflate_sequence(
          stream, linguistic::Word::xml_tag_name(), index_sequence);
    } else if (sequenceClassname.compare(
                   sequence::SymbolSequence::classname()) == 0) {
      seq = sequence::SymbolSequence::inflate_sequence(
          stream, Symbol::xml_tag_name(), index_sequence);
    } else if (sequenceClassname.compare(
                   sequence::NumberSequence::classname()) == 0) {
      seq = sequence::NumberSequence::inflate_sequence(
          stream, Number::xml_tag_name(), index_sequence);
    } else if (sequenceClassname.compare(
                   sequence::EmbeddingSequence::classname()) == 0) {
      seq = sequence::EmbeddingSequence::inflate_sequence(
          stream, Embedding::xml_tag_name(), index_sequence);
    } else if (sequenceClassname.compare(
                   sequence::GraphemeSequence::classname()) == 0) {
      seq = sequence::GraphemeSequence::inflate_sequence(
          stream, linguistic::Grapheme::xml_tag_name(), index_sequence);
    } else if (sequenceClassname.compare(sequence::VoidSequence::classname()) ==
               0) {
      seq = sequence::VoidSequence::inflate_sequence(
          stream, Void::xml_tag_name(), index_sequence);
    } else if (sequenceClassname.compare(
                   sequence::FillerSequence::classname()) == 0) {
      seq = sequence::FillerSequence::inflate_sequence(
          stream, linguistic::filler::Filler::xml_tag_name(), index_sequence);
    } else if (sequenceClassname.compare(
                   sequence::NamedEntitySequence::classname()) == 0) {
      seq = sequence::NamedEntitySequence::inflate_sequence(
          stream, linguistic::namedentity::NamedEntity::xml_tag_name(),
          index_sequence);
    } else if (sequenceClassname.compare(
                   sequence::PhonemeBlockSequence::classname()) == 0) {
      seq = sequence::PhonemeBlockSequence::inflate_sequence(
          stream, phonology::PhonemeBlock::xml_tag_name(), index_sequence);
    } else if (sequenceClassname.compare(
                   sequence::ProsodicPhraseSequence::classname()) == 0) {
      seq = sequence::ProsodicPhraseSequence::inflate_sequence(
          stream, phonology::prosodicphrase::ProsodicPhrase::xml_tag_name(),
          index_sequence);
    } else if (sequenceClassname.compare(sequence::AtomSequence::classname()) ==
               0) {
      seq = sequence::AtomSequence::inflate_sequence(
          stream, acoustic::Atom::xml_tag_name(), index_sequence);
    } else {
      std::stringstream ss;
      ss << sequenceClassname << " sequence type is unknown!";
      throw RootsException(__FILE__, __LINE__, ss.str().c_str());
    }

    add_sequence(seq);
    stream->next_child();
  }

  // read the relations
  Relation* rel = NULL;
  for (unsigned int index_relation = 0; index_relation < n_relation;
       ++index_relation) {
    rel = Relation::inflate_relation(stream, index_relation);
    add_relation(rel);
    /*if ((rel->get_source_sequence()->count() != 0) &&
        (rel->get_target_sequence()->count() != 0)) {
      add_relation(rel);
    } else {
      rel->destroy();
      rel = NULL;
      }*/
    stream->next_child();
  }

  if (is_terminal) stream->close_children();
}

std::string Utterance::relation_graph_to_dot() {
  return relationGraph.to_dot();
}

/**
 * @brief Concatenate an utterance after this
 * @param Utterance to concatenate
 * @warning Utterance to concatenate must be compatible with the current
 *Utterance :
 *    - Two sequences with same label and an embedded relation must be
 *related with the same label sequence.
 */
void Utterance::concatenate_utterance(const Utterance* concatUtt) {
  /* Get labels and relations */
  std::vector<std::string>* seqContLabels =
      concatUtt->get_all_sequence_labels();
  std::vector<std::string>* relContLabels =
      concatUtt->get_all_relation_labels();

  std::map<sequence::Sequence*, int> itemsToUpdate;
  std::map<std::string, int> seqOffset;
  std::vector<sequence::Sequence*>* thisSeq = get_all_sequences();
  for (std::vector<sequence::Sequence*>::const_iterator it = thisSeq->begin();
       it != thisSeq->end(); ++it) {
    seqOffset[(*it)->get_label()] = (*it)->count();
  }
  delete thisSeq;

  // Add sequences in the current utt (not the relations)
  for (std::vector<std::string>::const_iterator it = seqContLabels->begin();
       it != seqContLabels->end(); ++it) {
    sequence::Sequence* cseq = concatUtt->get_sequence(*it);

    if (is_valid_sequence(*it)) {
      // The sequence was in the current relation : concatenate
      sequence::Sequence* cseqFinal = get_sequence(*it);
      if (cseq->has_related_sequence()) {
        // Store offset to find items to update later (when i am sure the
        // target
        itemsToUpdate[cseqFinal] = cseqFinal->count();
      }
      cseqFinal->concatenate_no_propagate(cseq);

    } else {
      // The sequence was not in the current relation : add copy
      sequence::Sequence* cseqFinal = cseq->clone();
      seqOffset[cseqFinal->get_label()] = 0;
      add_sequence_no_propagate(cseqFinal, *it);
      if (cseq->has_related_sequence()) {
        // Store offset to find items to update later (when i am sure the
        // target
        itemsToUpdate[cseqFinal] = 0;
      }
    }
  }

  // Update related items
  for (std::map<sequence::Sequence*, int>::const_iterator it =
           itemsToUpdate.begin();
       it != itemsToUpdate.end(); ++it) {
    // Shift items
    sequence::Sequence* cseq = it->first;
    int startNewIdx = it->second;
    sequence::Sequence* targetSeq =
        get_sequence(cseq->get_related_sequence()->get_label());
    int offset = seqOffset[targetSeq->get_label()];
    int nbitem = cseq->count();
    for (int i = startNewIdx; i < nbitem; ++i) {
      BaseItem* citem = cseq->get_item(i);
      citem->set_related_sequence(targetSeq);
      citem->translate_index(offset);
    }

    // Add relation in relation graph
    relationGraph.add_relation(cseq->get_embedded_relation());
  }

  // Update relations added from new utt
  for (std::vector<std::string>::const_iterator it = relContLabels->begin();
       it != relContLabels->end(); ++it) {
    Relation* crel = concatUtt->get_relation(*it);
    Relation* crelFinal = get_relation(*it);

    int offsetSrc = seqOffset[crel->get_source_label()];
    int offsetTrg = seqOffset[crel->get_target_label()];

    if ((offsetSrc != 0) || (offsetTrg != 0)) {
      // copy and shift
      Relation* rclone = crel->clone();
      rclone->set_source_sequence(get_sequence(crel->get_source_label()));
      rclone->set_target_sequence(get_sequence(crel->get_target_label()));
      rclone->refresh_relation_size();
      rclone->get_mapping()->shift(offsetSrc, offsetTrg);

      // At least one sequence was in the original utterance
      if (crelFinal == NULL) {
        // But the relation was not in the original utterance
        add_relation(rclone);
      } else {
        // Union of relations
        crelFinal->refresh_relation_size();
        crelFinal->merge(rclone);
      }

      // destroy the tmp copy
      rclone->destroy();

    } else {
      // Add a copy of the relation
      roots::Relation* rclone = crel->clone();
      rclone->set_source_sequence(get_sequence(crel->get_source_label()));
      rclone->set_target_sequence(get_sequence(crel->get_target_label()));
      add_relation(rclone);
    }
  }

  // Refresh relations that are not in the utterance (but no merging)
  for (t_relation_hash::const_iterator it = relationHash.begin();
       it != relationHash.end(); ++it) {
    it->second->refresh_relation_size();
  }

  delete relContLabels;
  delete seqContLabels;
}

/**
 * @brief merge an utterance with this
 * @param Utterance to merge
 * @warning Utterance to merge must be compatible with the current Utterance :
 *    - sequence and relation labels must be identical
 */
void Utterance::merge_utterance(const Utterance* mergingUtterance) {
  // DEBUG
  /*
    std::clog << "Fusion init : "; // DEBUG
    this->Debug();
    std::clog << "Fusion to add : "; // DEBUG
    utterance->Debug();
  */

  /* Get labels and relations */
  std::vector<std::string>* sequenceSetLabels =
      mergingUtterance->get_all_sequence_labels();
  std::vector<std::string>* relationSetLabels =
      mergingUtterance->get_all_relation_labels();

  std::vector<int> embededRelationSeqIndex;

  int sequenceSetLabelsSize = sequenceSetLabels->size();
  int relationSetLabelsSize = relationSetLabels->size();

  /* fusion sequence */
  for (int i = 0; i < sequenceSetLabelsSize; ++i) {
    std::string currentLabel = sequenceSetLabels->at(i);
    if (!is_valid_sequence(currentLabel)) {
      sequence::Sequence* currentSequenceToAdd =
          mergingUtterance->get_sequence(currentLabel);

      /* Embedded relation are managed later */
      if (currentSequenceToAdd->has_related_sequence()) {
        embededRelationSeqIndex.push_back(i);
      }

      add_sequence_no_propagate(currentSequenceToAdd->clone(), currentLabel);
    }
  }

  /* Update embedded relations */
  for (std::vector<int>::iterator it = embededRelationSeqIndex.begin();
       it != embededRelationSeqIndex.end(); ++it) {
    /* update each element */
    roots::sequence::Sequence* seqToUpdate =
        get_sequence(sequenceSetLabels->at(*it));

    for (unsigned int j = 0; j < seqToUpdate->count(); ++j) {
      /* Note : return type of get_items should be changed */
      BaseItem* item = (BaseItem*)seqToUpdate->get_item(j);
      sequence::Sequence* relatedSeq = item->get_related_sequence();
      if (relatedSeq != NULL) {
        item->set_related_sequence(get_sequence(relatedSeq->get_label()));
      }
    }
    /* Add embedded relation */
    if (seqToUpdate->count() != 0) {
      add_relation(seqToUpdate->get_embedded_relation());
    }
  }

  /* merge relations (missing relations)*/
  for (int i = 0; i < relationSetLabelsSize; i++) {
    std::string currentLabel = relationSetLabels->at(i);
    // std::cout << "Adding relation " << currentLabel << std::endl; // DEBUG

    roots::Relation* currentRelationFinal = this->get_relation(currentLabel);

    if (currentRelationFinal == NULL) {
      roots::Relation* relationClone =
          mergingUtterance->get_relation(currentLabel)->clone();

      relationClone->set_source_sequence(
          this->get_sequence(relationClone->get_source_label()));
      relationClone->set_target_sequence(
          this->get_sequence(relationClone->get_target_label()));
      this->add_relation(relationClone);
    }
  }

  delete relationSetLabels;
  delete sequenceSetLabels;
}

/*
 * Note: This extraction function do not guarantee the restoration of the
 * utterance's semantic at the end of the computation. Things such as the
 * concatenation of two TimeSegment sequences will not have any effect on the
 * data (time start and time end values in that example) within the contained
 * items.
 * More over, this function may fail for some pathological tree topology. Be
 * careful ! i.e : not safe if you cut in the middle of an item with embedded
 * items.
 */
Utterance* Utterance::extract_from_sequence(const std::string& sourceSeqLabel,
                                            int sourceBeginIndex,
                                            int sourceEndIndex) {
  Utterance* extractedUtterance = new Utterance();

  /* Get labels and relations */
  std::vector<std::string>* sequenceSetLabels = this->get_all_sequence_labels();
  std::vector<std::string>* relationSetLabels = this->get_all_relation_labels();

  /* Add sub relation */
  std::vector<int> embededRelationSeqIndex;

  int sequenceSetLabelsSize = sequenceSetLabels->size();
  int relationSetLabelsSize = relationSetLabels->size();

  std::map<std::string, int> sequenceSetOffsets;
  std::map<std::string, std::pair<int, int> > setEmbeddedRangeTarget;

  // this->Debug();

  /* add sequence */
  for (int i = 0; i < sequenceSetLabelsSize; i++) {
    //  std::cout << "Adding sequence " << sequenceSetLabels->at(i) <<
    // std::endl; // DEBUG

    roots::sequence::Sequence* currentSequenceToAdd = NULL;
    std::string currentLabel = sequenceSetLabels->at(i);
    sequence::Sequence* currentSeq = this->get_sequence(currentLabel);

    if (currentSeq->count() > 0) {
      roots::Relation* currentRelation =
          this->get_relation(sourceSeqLabel, currentLabel);
      if (currentRelation != NULL) {
        // std::cout << "\tGet related element from "<< sourceSeqLabel<< "["<<
        // sourceBeginIndex<< ";" << sourceEndIndex <<"] to " <<
        // currentLabel <<std::endl << (*currentRelation) <<  std::endl;
        // // DEBUG

        std::vector<int> minMaxIndex = currentRelation->get_related_elements(
            sourceBeginIndex, sourceEndIndex, true);
        currentRelation->destroy();

        if (minMaxIndex.size() == 2) {
          currentSequenceToAdd =
              currentSeq->get_subsequence(minMaxIndex[0], minMaxIndex[1], 0);

          sequenceSetOffsets[currentLabel] = minMaxIndex[0];

          /* Embedded relation are managed later */
          if (currentSequenceToAdd->has_related_sequence()) {
            if (currentSeq->count() != 0) {
              embededRelationSeqIndex.push_back(i);
              roots::Relation* embeddedRelation =
                  currentSeq->get_embedded_relation();
              std::vector<int> minMaxIndexEmbeded =
                  embeddedRelation->get_related_elements(minMaxIndex[0],
                                                         minMaxIndex[1], true);
              if (minMaxIndexEmbeded.size() == 2) {
                setEmbeddedRangeTarget[currentLabel] = std::make_pair(
                    minMaxIndexEmbeded[0], minMaxIndexEmbeded[1]);
              }
            }
          }
        }
      }
    }

    if (currentSequenceToAdd == NULL) {
      // No relation but add an empty sequence (with a correct type)
      currentSequenceToAdd = currentSeq->clone_empty();
      currentSequenceToAdd->set_label(currentLabel);
      sequenceSetOffsets[currentLabel] = 0;
    }

    // std::cout << "\tAdd sub seq " << sequenceSetLabels->at(i) << std::endl;
    // // DEBUG
    extractedUtterance->add_sequence_no_propagate(currentSequenceToAdd,
                                                  currentLabel);
  }

  /* Update embeded relations */

  for (std::vector<int>::iterator it = embededRelationSeqIndex.begin();
       it != embededRelationSeqIndex.end(); ++it) {
    // std::cout << "Working on embed sequence " << sequenceSetLabels->at(*it)
    // << std::endl; // DEBUG
    std::string currentLabel = sequenceSetLabels->at(*it);
    sequence::Sequence* seqToUpdate =
        extractedUtterance->get_sequence(currentLabel);
    // std::cout << "Sequence look like " <<*seqToUpdate << std::endl; // DEBUG

    /* If the sub sequence cut in a middle of an embded, remove the sequence */
    bool complexe_cut = false;
    std::pair<int, int> targetRange = setEmbeddedRangeTarget[currentLabel];

    /* Update only newly add items */
    for (unsigned int j = 0; j < seqToUpdate->count(); ++j) {
      /* Note : return type of get_items should be changed */
      BaseItem* item = (BaseItem*)seqToUpdate->get_item(j);
      sequence::Sequence* relatedSeq = item->get_related_sequence();

      if (relatedSeq != NULL) {
        std::string targetLabel = relatedSeq->get_label();
        sequence::Sequence* newSeq =
            extractedUtterance->get_sequence(targetLabel);
        item->set_related_sequence(newSeq);

        /* Translate inside indices : */
        int offset = sequenceSetOffsets[targetLabel];
        item->translate_index(-offset);

        // std::cerr << "I will translate " << targetLabel << " of " << -offset
        // << std::endl;
        /* Test range */
        if ((targetRange.first - offset < 0) ||
            (targetRange.second - offset >= (int)newSeq->count())) {
          complexe_cut = true;
        }
      }
    }

    if (complexe_cut) {
      ROOTS_LOGGER_WARN(std::string("Cutting in a middle of a complexe item. "
                                    "The folowing sequence will be erase: ") +
                        currentLabel);
      roots::sequence::Sequence* currentSequenceToAdd =
          extractedUtterance->get_sequence(currentLabel)->clone_empty();
      currentSequenceToAdd->set_label(currentLabel);
      sequenceSetOffsets[currentLabel] = 0;

      sequence::Sequence* removedSeq =
          extractedUtterance->remove_sequence(currentLabel);
      removedSeq->destroy();

      extractedUtterance->add_sequence_no_propagate(currentSequenceToAdd,
                                                    currentLabel);
    } else {
      /* Add embeded relation */
      add_relation(seqToUpdate->get_embedded_relation());
    }
  }

  /* Add sub relation */
  for (int i = 0; i < relationSetLabelsSize; i++) {
    std::string currentLabel = relationSetLabels->at(i);
    // std::cout << "Adding relation " << currentLabel << std::endl; // DEBUG

    roots::Relation* currentRelationToAdd = this->get_relation(currentLabel);
    std::string sourceLabel = currentRelationToAdd->get_source_label();
    std::string targetLabel = currentRelationToAdd->get_target_label();

    roots::Relation* currentSubRelation =
        currentRelationToAdd->get_reduced_subrelation(
            extractedUtterance->get_sequence(sourceLabel),
            sequenceSetOffsets[sourceLabel],
            extractedUtterance->get_sequence(targetLabel),
            sequenceSetOffsets[targetLabel]);
    extractedUtterance->add_relation(currentSubRelation);
  }

  delete relationSetLabels;
  delete sequenceSetLabels;

  return extractedUtterance;
}

void Utterance::Debug() {
  // DEBUG
  std::clog << "Utterance::Debug : " << std::endl;  // DEBUG
  std::vector<std::string>* sl = this->get_all_sequence_labels();
  std::vector<std::string>* rl = this->get_all_relation_labels();
  int w = 0;
  for (std::vector<std::string>::iterator it = sl->begin(); it != sl->end();
       it++) {
    std::clog << "Sequence " << w << " : " << *it << std::endl;  // DEBUG
    w++;
  }
  w = 0;
  for (std::vector<std::string>::iterator it = rl->begin(); it != rl->end();
       it++) {
    std::clog << "Relation " << w << " : " << *it << std::endl;  // DEBUG
    w++;
  }
  delete rl;
  delete sl;
}

} /* End namespace Roots */
