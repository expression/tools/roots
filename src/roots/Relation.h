/*
    This file is part of Roots.

    Roots is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Roots is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Roots.  If not, see <http://www.gnu.org/licenses/>.

    Copyright (c) 2013, 2014 IRISA
*/

#ifndef RELATION_H_
#define RELATION_H_

#include "Base.h"
#include "Matrix.h"
#include "Matrix/SparseMatrix.h"
#include "RelationLinkedObject.h"
#include "UtteranceLinkedObject.h"
#include "common.h"

namespace roots {

namespace sequence {
class Sequence;  // partial definition
}

class Matrix;  // partial definition
namespace acoustic {
class Allophone;  // partial definition
class Segment;    // partial definition
}

/**
 * @brief Represent a relation between two sequences of items
 * @details
 * This class contains two sequences of items, namely a source sequence
 * and a target sequence. The relation between the items of the sequences
 * is modelled with a matrix.
 * @author Cordial Group
 * @version 0.1
 * @date 2011
 */
class Relation : public Base, public UtteranceLinkedObject {
 public:
  /**
   * @brief Constructor.
   * @param source pointer to the source sequence
   * @param target pointer to the target sequence
   */
  Relation(sequence::Sequence *source, sequence::Sequence *target);
  /**
   * @brief Constructor
   * @param source pointer to the source sequence
   * @param target pointer to the target sequence
   * @param mapping the matrix containing the relation between items of the two
   * sequences
   */
  Relation(sequence::Sequence *source, sequence::Sequence *target,
           Matrix *mapping);

  /**
   * @brief Destructor if no more inside an utterance
   */
  virtual void destroy();

 protected:
  /**
   * @brief Destructor
   * @warning Protected: must use destroy
   */
  ~Relation();

 public:
  /**
  * @brief The union of two relations
  * @note technicaly, it add matrix so values inside after could be more than 1.
  */
  virtual void merge(const Relation *rel);

  /**
   * @brief Returns the source sequence label
   * @return source sequence label
   */
  virtual const std::string &get_source_label() const;

  /**
   * @brief Returns the target sequence label
   * @return target sequence label
   */
  virtual const std::string &get_target_label() const;

  /**
   * @brief Updates the source sequence label with the given label
   * @param new_label the new label for the target sequence
   * @return target sequence label
   */
  virtual void set_source_label(const std::string &new_label);

  /**
   * @brief Updates the target sequence label with the given label
   * @param new_label the new label for the target sequence
   * @return target sequence label
   */
  virtual void set_target_label(const std::string &new_label);

  /**
   * @brief Returns the source sequence type
   * @return source sequence type
   */
  virtual const std::string get_source_type() const;

  /**
   * @brief Returns the target sequence type
   * @return target sequence type
   */
  virtual const std::string get_target_type() const;

  /**
   * @brief Assigns the source sequence to a new one
   * @param seq pointer to the new source sequence
   * @todo erase link of the previous sequence if exists
   */
  virtual void set_source_sequence(sequence::Sequence *seq);

  /**
   * @brief Assigns the target sequence to a new one
   * @param seq pointer to the new target sequence
   * @todo erase link of the previous sequence if exists
   */
  virtual void set_target_sequence(sequence::Sequence *seq);

  /**
   * @brief Returns a pointer to the source sequence
   * @return pointer to the source sequence
   */
  virtual sequence::Sequence *get_source_sequence() const;

  /**
   * @brief Returns a pointer to the target sequence
   * @return pointer to the target sequence
   */
  virtual sequence::Sequence *get_target_sequence() const;

  /**
   * @brief Inform the relation to no more link a sequence
   * (relationLinkedObject)
   * @Warning the source or the target sequence may be invalidate and the
   * relation could be useless
   */
  void no_more_linked(roots::RelationLinkedObject *rlo);

  /**
   * @brief Returns the is_composite attribute value
   * @return is_composite
   */
  virtual bool get_is_composite() const { return is_composite; };

  /**
   * @brief Sets the is_composite attribute value
   */
  virtual void set_is_composite(bool is_comp) { is_composite = is_comp; };

  /**
   * @brief Returns a pointer to the mapping matrix
   * @return pointer to the mapping matrix
   */
  virtual Matrix *get_mapping() const;

  /**
   * @brief Returns a generic label composed by source and target labels
   * @return a string containing the label
   */
  virtual std::string generic_label() const;

  /**
   * @brief Returns a generic label composed by source and target labels
   * @param sourceLabel
   * @param targetLabel
   * @return a string containing the label
   */
  static std::string generic_label(const std::string &sourceLabel,
                                   const std::string &targetLabel);

  /**
   * @brief Verify if specified elements are in relation
   * @remarks This method is a synonymous of in_relation
   * @param source_index the index of the source element
   * @param target_index the index of the target element
   * @return true if the source and target elements are in relation
   */
  virtual bool linked(int source_index, int target_index) const;

  /**
   * @brief Adds a relation link between two items from the source and
   *destination sequences
   *
   * @param source_index Index of the item in the source sequence
   * @param target_index Index of the item in the destination sequence
   **/
  virtual void link(int source_index, int target_index);

  /**
   * @brief Removes a relation link between two items from the source and
   *destination sequences
   *
   * @param source_index Index of the item in the source sequence
   * @param target_index Index of the item in the destination sequence
   **/
  virtual void unlink(int source_index, int target_index);

  /**
   * @brief Link all elements from the source sequence to all elements from the
   *target sequence
   **/
  virtual void fill();

  /**
   * @brief Unlink all pairs of elements
   **/
  virtual void clear();

  /**
   * @brief Verify if specified elements are in relation
   *
   * @deprecated
   * @param source_index the index of the source element
   * @param target_index the index of the target element
   * @return true if the source and target elements are in relation
   */
  virtual bool in_relation(int source_index, int target_index) const;

  /**
   * @brief Synonymous of method get_related_indices
   * @deprecated
   */
  virtual std::vector<int> get_related_elements(int index,
                                                bool is_row_index = true) const;

  /**
   * @brief Synonymous of method get_related_indices
   * @deprecated
   */
  virtual std::vector<int> get_related_elements(int beginIndex, int endIndex,
                                                bool is_row_index) const;

  /**
   * @brief Gets indices of elements that are in relation with the specified
   * element
   * @details
   * This method returns indices of source/target elements if they are in
   * relation with
   * a target/source element. The boolean parameter is_row_index is used to
   * choose the
   * sense of the relation, either source -> target (true) or the opposite
   * (false).
   * @param index the index of the row or column for which we want the
   * corresponding column or row indices
   * @param is_row_index indicates if the index is a row index (direct) or a
   * column one (inverse) (default: true)
   * @return the indices of the source or target elements as a vector, or an
   * empty vector otherwise
   */
  virtual std::vector<int> get_related_indices(int index,
                                               bool is_row_index = true) const;

  /**
   * @brief return minimum and maximum index of items related with a range of
   * elements
   * @details
   * This method returns minimum and maximum indices of source/target elements
   * if they are in relation with
   * a range of target/source elements. The boolean parameter is_row_index is
   * used to choose the
   * sense of the relation, either source -> target (true) or the opposite
   * (false).
   * @param beginIndex start index of the row or column for which we want the
   * corresponding column or row indices
   * @param endIndex end index of the row or column for which we want the
   * corresponding column or row indices
   * @param is_row_index indicates if the index is a row index (direct) or a
   * column one (inverse) (default: true)
   * @return a vector with two elements : the first is the smallest index of the
   * related elements and the second the biggest. return empty vector if no
   * related element.
   */
  virtual std::vector<int> get_related_indices(int beginIndex, int endIndex,
                                               bool is_row_index) const;

  /**
   * @brief Get elements that are in relation with the specified element
   * @details
   * This method returns source/target elements if they are in relation with
   * a target/source element. The boolean parameter is_row_index is used to
   * choose the
   * sense of the relation, either source -> target (true) or the opposite
   * (false).
   * @param index the index of the row or column for which we want the
   * corresponding column or row elements
   * @param is_row_index indicates if the index is a row index (direct) or a
   * column one (inverse) (default: true)
   * @return Source or target elements as a vector, or an empty vector otherwise
   */
  virtual std::vector<BaseItem *> get_related_items(
      int index, bool is_row_index = true) const;

  /**
   * @brief Computes the list of items related with a range of elements
   * @details
   * This method returns source/target elements if they are in relation with
   * a range of target/source elements. The boolean parameter is_row_index is
   * used to choose the
   * sense of the relation, either source -> target (true) or the opposite
   * (false).
   * @param beginIndex start index of the row or column for which we want the
   * corresponding column or row elements
   * @param endIndex end index of the row or column for which we want the
   * corresponding column or row elements
   * @param is_row_index indicates if the index is a row index (direct) or a
   * column one (inverse) (default: true)
   * @return A vector of elements
   */
  virtual std::vector<BaseItem *> get_related_items(int beginIndex,
                                                    int endIndex,
                                                    bool is_row_index) const;

  /**
   * @brief build a sub relation from two sub sequences.
   * @details
   * @param source_subseq
   * @param source_seq_offset offset of the source sub sequence related with the
   * original source sequence
   * @param target_subseq
   * @param target_seq_offset offset of the target sub sequence related with the
   * original target sequence
   * @return a sub relation for the two subsequence
   * @warning the two sub sequence must be build from the original sequence of
   * the current relation. No test proceded.
   */
  virtual Relation *get_reduced_subrelation(sequence::Sequence *source_subseq,
                                            int source_seq_offset,
                                            sequence::Sequence *target_subseq,
                                            int target_seq_offset) const;

  /**
   * @brief Assigns a new mapping matrix to the relation. Use this method with
   * caution!
   * This method gives a direct access to the matrix mapping in the relation. It
   * has to be
   * used with caution since it might throw an exception if size of the matrix
   * is
   * incorrect. The previous matrix mapping is deleted.
   * @throw RootsException if matrix size is incoherent with length of the
   * sequences
   * @param mapping pointer to the new mapping
   */
  virtual void set_mapping(Matrix *mapping);

  /**
   * @brief Assigns a new mapping matrix to the relation. Use this method with
   * caution!
   * This method gives a direct access to the matrix mapping in the relation. It
   * has to be
   * used with caution since it might throw an exception if size of the matrix
   * is
   * incorrect. The previous matrix mapping is deleted.
   * @throw RootsException if mapping size is incoherent with length of the
   * sequences
   * @param mapping vector the target indices corresponding to the source
   * elements. If a source element have no target, you can use a negative index
   * value.
   */
  virtual void set_mapping_from_source(const std::vector<int> &mapping);

  /**
   * @brief Assigns a new mapping matrix to the relation. Use this method with
   * caution!
   * This method gives a direct access to the matrix mapping in the relation. It
   * has to be
   * used with caution since it might throw an exception if size of the matrix
   * is
   * incorrect. The previous matrix mapping is deleted.
   * @throw RootsException if mapping size is incoherent with length of the
   * sequences
   * @param mapping vector the source indices corresponding to the target
   * elements If a target element have no source, you can use a negative index
   * value.
   */
  virtual void set_mapping_from_target(const std::vector<int> &mapping);

  /**
   * @brief Returns the clone of the relation
   * @return Pointer to the new Relation
   */
  virtual Relation *clone() const;

  /**
   * @brief Inverse the relation (source <-> target) - Inplace operation
   */
  virtual void inverse();

  /**
   * @brief Compound the relation with another relation.
   * @param relation the relation to compound with
   * @param rightComposition true if relation is applied to the right, false for
   * a left composition
   * @warning Elements of the compound matrix can be greater than 1 (elements
   * are integers not booleans)
   */
  virtual void compound_inplace(Relation *relation,
                                bool rightComposition = true);

  /**
   * @brief Compound the relation with another relation.
   * @param relation the relation to compound with
   * @param rightComposition true if relation is applied to the right, false for
   * a left composition
   * @return the new relation
   * @warning Elements of the compound matrix can be greater than 1 (elements
   * are integers not booleans)
   */
  virtual Relation *compound(Relation *relation, bool rightComposition = true);

  /**
   * @brief Inserts a row into the relation
   * @param index of the row to add
   */
  virtual void insert_source(int index);

  /**
   * @brief Inserts a column into the relation
   * @param index of the column to add
   */
  virtual void insert_target(int index);

  /**
   * @brief recompute the size of the ralation from the size of the related
   * sequence
   * @details not links are added but some can be removed if the related
   * sequences are shorter.
   * @warning only use as internal function
   */
  virtual void refresh_relation_size();

  /**
   * @brief Rmoves a row into the relation
   * @param index of the row to remove
   */
  virtual void remove_source(int index, bool propagateEmbeded = true);

  /**
   * @brief Removes a column into the relation
   * @param index of the column to remove
   */
  virtual void remove_target(int index, bool propagateEmbeded = true);

  /**
   * @brief Informs the sequences that they are linked to the current relation
   */
  virtual void propagate_link();

  /**
   * @brief Informs the sequences that they are no more linked to the current
   * relation
   */
  virtual void propagate_remove_link();

  /**
   * Serialize the object into a stream as a string
   *
   * @param out
   * @param rel
   * @return the modified stream
   */
  friend std::ostream &operator<<(std::ostream &out, roots::Relation &rel);

  /**
   * @brief Returns a raw text string representation of the current relation
   * @param level precision level of the output (default: 0)
   * @return A unicode string
   */
  std::string to_string(int level = 0) const;

  /**
   * @brief Generate the XML part of the entity
   * @param stream
   * @param is_terminal
   */
  virtual void deflate(RootsStream *stream, bool is_terminal = true,
                       int list_index = -1);
  /**
   * @brief Extracts the entity from the stream
   * @param stream the stream from which we inflate the element
   */
  virtual void inflate(RootsStream *stream, bool is_terminal = true,
                       int list_index = -1);
  /**
   * @brief Extracts the entity from the stream
   * @param stream the stream from which we inflate the element
   */
  static Relation *inflate_relation(RootsStream *stream, int list_index = -1);
  /**
   * @brief returns the XML tag name value for current object
   * @return string constant representing the XML tag name
   */
  virtual std::string get_xml_tag_name() const { return xml_tag_name(); };
  /**
   * @brief returns the XML tag name value for current class
   * @return string constant representing the XML tag name
   */
  static std::string xml_tag_name() { return "relation"; };
  /**
   * @brief returns classname for current object
   * @return string constant representing the classname
   */
  virtual std::string get_classname() const { return classname(); };
  /**
   * @brief returns classname for current class
   * @return string constant representing the classname
   */
  static std::string classname() { return "Relation"; };

 private:
  sequence::Sequence *sourceSequence; /**<  pointer to the source sequence */
  sequence::Sequence *targetSequence; /**<  pointer to the target sequence */
  std::string sourceLabel;            /**<  label of the source sequence */
  std::string targetLabel;            /**<  label of the target sequence */
  std::string sourceType;             /**<  type of the source sequence */
  std::string targetType;             /**<  type of the target sequence */
  bool is_composite; /**<  indicate whether the relation is composite or not */
  Matrix *mapping;   /**<  pointer to the matrix that makes the mapping between
                        source and target sequences*/

  /**
   * @brief Default constructor
   */
  Relation(bool noInit = false);
};
}

#endif /* RELATION_H_ */
