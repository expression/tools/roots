/*
    This file is part of Roots.

    Roots is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Roots is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Roots.  If not, see <http://www.gnu.org/licenses/>.

    Copyright (c) 2013, 2014 IRISA
*/


#ifndef UTTERANCELINKEDOBJECT_H_
#define UTTERANCELINKEDOBJECT_H_

#include "common.h"
#include "BaseLink.h"

namespace roots
{
  
  class BaseItem;
  class Utterance;

  /**
   * @brief Represent a link between an object and an utterance
   * @author Cordial Group
   * @date 2013
   */
  class UtteranceLinkedObject
  {
  public:
    /**
     * @brief Default constructor
     * @note There is a dirty hack for RootsDisplay : it is possible to add a link to NULL utterance. this utterance link will never be returned with get_all_utterance(_links) and inform will always be ignored but it will be count when call count_utterances or in_utterance methods.
     */
    UtteranceLinkedObject();

    /**
     * @brief Default constructor
     */
    UtteranceLinkedObject(const UtteranceLinkedObject& rel_link);

    /**
     * @brief Default constructor
     * @todo check
     */
    virtual ~UtteranceLinkedObject();

    /**
     * @brief Tells the current sequence that it is added inside an Utterance.
     */
    void add_in_utterance(Utterance * utt);
    
    /**
     * @brief Tells the current sequence that it is removed from an Utterance.
     * @param utt Provide the pointer to the utterance where the sequence is included when you know it. Otherwise, pointer is considered as NULL.
     * @return True if removal has been correctly operated, false otherwise.
     */
    bool remove_from_utterance(Utterance * utt, bool informUtterance = true);
    
    /**
     * @brief Return the number of utterances where the current sequence is included
     * @return The number of utterances
     **/
    virtual int count_utterances() const;
    
    /**
     * @brief Check if the current object is included in at least one utterance
     * @return True if included in one or more utterances, false otherwise
     **/
    virtual bool in_utterance() const;
 
    /**
     * @brief Return all the links to utterances
     * @return A vector of links
     **/
    virtual std::vector< BaseLink<Utterance> > get_all_utterance_links() const;
 
    /**
     * @brief Return all the linked utterances
     * @return A vector of pointers to utterances
     **/
    virtual std::vector< Utterance* > get_all_utterances() const;
      
    UtteranceLinkedObject& operator=(const UtteranceLinkedObject& utt_link);

  protected:
    /**
     * @brief Returns the hash containing the links
     * Be carefull, this hash is part of the class internals.
     * @return a reference to the hash
     */
    boost::unordered_set< BaseLink<Utterance> >& get_hash();

    /**
     * @brief Assigns the hash containing the links
     * Be carefull, this hash is part of the class internals.
     * @param hash a reference to the hash
     */
    void set_hash(const boost::unordered_set< BaseLink<Utterance> >& hash);

  private:
    
    /**
     * Vector of the utterance links.
     */
       boost::unordered_set< BaseLink<Utterance> > linkHash;

  };

}

#endif /* UTTERANCELINKEDOBJECT_H_ */
