/*
    This file is part of Roots.

    Roots is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Roots is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Roots.  If not, see <http://www.gnu.org/licenses/>.

    Copyright (c) 2013, 2014 IRISA
*/


#ifndef ITEMSEQUENCELINK_H_
#define ITEMSEQUENCELINK_H_

#include "common.h"
#include "BaseLink.h"


namespace roots
{
  
  namespace sequence { class Sequence; }
  
  /**
   * @brief Represent a link between an object and a relation
   * @details
   * @author Cordial Group
   * @version 0.1
   * @date 2011
   */
  class SequenceLinkedObject
  {
  public:
    
    /**
     * @brief Default constructor
     */
    SequenceLinkedObject();
    
    SequenceLinkedObject(const SequenceLinkedObject& seq_link);
    
    /**
     * @brief Destructor
     * @todo check
     */
    virtual ~SequenceLinkedObject();
    
    SequenceLinkedObject& operator=(const SequenceLinkedObject& seq_link);
    
    /**
     * @brief Returns the index of the object in the sequence
     * @return index of the item
     */
    int get_in_sequence_index() const;
    
    /**
     * @brief Assigns the sequence index
     * @param rank the new rank of the item
     */
    void set_in_sequence_index(int rank);
    
    /**
     * @brief Removes the sequence index (set it to -1)
     */
    void remove_in_sequence_index();
    
    /**
     * @brief Test is the current item is part of a sequence.
     * @return True if part of a sequence, false otherwise.
     */
    bool is_in_sequence() const;
    
    /**
     * @brief Return the linked sequence
     * @return pointer to the sequence
     */
    sequence::Sequence* get_in_sequence() const;

    /**
     * @brief Links the object to a sequence
     * @param _sequenceLink pointer to the sequence
     */
    void set_in_sequence(sequence::Sequence* _sequenceLink, unsigned int rank);
    
    /**
     * @brief Unlinks the object
     */ 
    void remove_from_sequence();
    
    /**
     * @brief Says if the item is the first item of the sequence
     */ 
    bool is_first_in_sequence();
    
    /**
     * @brief Says if the item is the first item of the sequence
     */ 
    bool is_last_in_sequence();
    
  private:
    
    /**
     * Link to the sequence
     */
    BaseLink<sequence::Sequence > sequenceLink;
    
    /**
     * Rank in the sequence
     */
    unsigned int sequenceRank;
    
  };
  
}

#endif /* ITEMSEQUENCELINK_H_ */
