/*
    This file is part of Roots.

    Roots is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Roots is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Roots.  If not, see <http://www.gnu.org/licenses/>.

    Copyright (c) 2013, 2014 IRISA
*/


#ifndef ROOTSEXCEPTION_H_
#define ROOTSEXCEPTION_H_

#include "common.h"
#include <string>
#include <exception>


namespace roots
{

/**
 * @brief The top-level exception class of Roots
 * @details
 * @author Cordial Group
 * @version 0.1
 * @date 2011
 */
class RootsException: public std::exception
{
	public:
	/**
	 * @brief Construtor
	 * @param file filename where the exception happens (use __FILE__ directive)
	 * @param line line at which the exception happens (use __LINE__ directive)
	 * @param msg the message to display
	 */
    RootsException(const char* file, const int line, const std::string& msg)
	{
		this->file = file;
		this->line = line;
		message = msg;
        std::stringstream str;
		str << file << ":" << line <<" : "<< message;
		whatMessage = str.str();
	}
	/**
	 * @brief Destructor
	 */
	virtual ~RootsException() throw()
	{

	}

	/**
	 * @brief Returns the message
	 */
	virtual const char* what() const throw()
	{
	  /*
		stringstream str;
		str << file << ":" << line <<" : "<< message;
		return str.str().c_str();
	  */
	  return whatMessage.c_str();
	}

private:
    std::string file;
	int line;
	std::string message;
	std::string whatMessage;
};


class OutOfBoundIndexException : public roots::RootsException
{
public:
	OutOfBoundIndexException(const char* file, const int line, const std::string& msg) :
	roots::RootsException(file, line, msg) {};
	virtual ~OutOfBoundIndexException() throw() {};
};


}

#endif /* ROOTSEXCEPTION_H_ */
