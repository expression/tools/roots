/*
    This file is part of Roots.

    Roots is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Roots is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Roots.  If not, see <http://www.gnu.org/licenses/>.

    Copyright (c) 2013, 2014 IRISA
*/

#include "Segment.h"

namespace roots {
namespace acoustic {

const double Segment::TIME_UNIT_FACTOR_METRIC =
    1.0; /**< factor to convert to second */
const double Segment::TIME_UNIT_FACTOR_HTK =
    1.0e-7; /**< factor to convert to second from HTK */

Segment::Segment(const bool noInit) : BaseItem(noInit) {
  set_segment_start(0.0);
  set_segment_end(0.0);
  set_time_scaling(Segment::TIME_UNIT_FACTOR_METRIC);
}

Segment::Segment(double aStartSegment, double aEndSegment, double aTimeUnit) {
  set_segment_start(aStartSegment);
  set_segment_end(aEndSegment);
  set_time_scaling(aTimeUnit);
}

Segment::Segment(const Segment &seg) : BaseItem(seg) {
  set_segment_start(seg.get_segment_start());
  set_segment_end(seg.get_segment_end());
  set_time_scaling(seg.get_time_scaling());
}

Segment *Segment::clone() const { return new Segment(*this); }

Segment::~Segment() {}

Segment &Segment::operator=(const Segment &seg) {
  BaseItem::operator=(seg);

  set_segment_start(seg.get_segment_start());
  set_segment_end(seg.get_segment_end());
  set_time_scaling(seg.get_time_scaling());

  return *this;
}

double Segment::get_segment_start() const { return this->startSegment; }

double Segment::get_segment_end() const { return this->endSegment; }

double Segment::get_segment_duration() const {
  return get_segment_end() - get_segment_start();
}

void Segment::set_segment_start(double aStartSegment) {
  this->startSegment = aStartSegment;
}

void Segment::set_segment_end(double aEndSegment) {
  this->endSegment = aEndSegment;
}

double Segment::get_time_scaling() const { return timeScalingFactor; }

void Segment::set_time_scaling(double aTimeScalingFactor) {
  timeScalingFactor = aTimeScalingFactor;
}

void Segment::deflate(RootsStream *stream, bool is_terminal, int list_index) {
  BaseItem::deflate(stream, false, list_index);

  stream->append_long_double_content("segment_start",
                                     this->get_segment_start());
  stream->append_long_double_content("segment_end", this->get_segment_end());
  stream->append_double_content("time_scaling", this->get_time_scaling());

  if (is_terminal) stream->close_object();
}

void Segment::inflate(RootsStream *stream, bool is_terminal, int list_index) {
  BaseItem::inflate(stream, false, list_index);

  // stream->open_children();
  this->set_segment_start(stream->get_long_double_content("segment_start"));
  this->set_segment_end(stream->get_long_double_content("segment_end"));
  this->set_time_scaling(stream->get_double_content("time_scaling"));
  if (is_terminal) stream->close_children();
}

Segment *Segment::inflate_object(RootsStream *stream, int list_index) {
  Segment *t = new Segment(true);
  t->inflate(stream, true, list_index);
  return t;
  /*
 std::stringstream ss;
  ss << "Segment::inflate_object : cannot inflate Segment::";
  throw RootsException(__FILE__, __LINE__, ss.str().c_str());

  return NULL;
  */
}

std::string Segment::to_string(int level) const {
  std::stringstream ss;

  ss.setf(std::ios::fixed, std::ios::floatfield);
  ss.precision(3 + level);
  ss << get_segment_start() << " - " << get_segment_end();
  if (get_time_scaling() == TIME_UNIT_FACTOR_HTK) {
    ss << " (ns)";
  } else {
    ss << " (s) "<< get_time_scaling();
  }

  return std::string(ss.str());
}
}
}
