/*
    This file is part of Roots.

    Roots is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Roots is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Roots.  If not, see <http://www.gnu.org/licenses/>.

    Copyright (c) 2013, 2014 IRISA
*/


#ifndef SEGMENT_H_
#define SEGMENT_H_

//#include "common.h"
#include "../BaseItem.h"


namespace roots {
  namespace acoustic {

    /**
     * @brief Representation of a segment
     * @details
     * An item \p segment is an anchor on signal for upper levels. It is
     * defined by its \p start and \p end times expressed in continuous or discrete time.
     * @author Cordial Group
     * @version 0.1
     * @date 2011
     */
    class Segment: public BaseItem
    {

    public:
      /**
       * @brief constructor
       * @param aStartSegment
       * @param aEndSegment
       * @param aSamlingFrequency
       * @param aTimeFactor is either TIME_UNIT_FACTOR_METRIC or TIME_UNIT_FACTOR_HTK
       */

      Segment( double aStartSegment,
	       double aEndSegment,
	       double aTimeUnit = Segment::TIME_UNIT_FACTOR_METRIC);

      /**
       * @brief Copy constructor
       * @param seg
       */
      Segment(const Segment &seg);

      /**
       * @brief Destructor
       * @todo check
       */
      virtual ~Segment();

      /**
       * @brief clone the current object
       * @return a copy of the object
       */
      virtual Segment *clone() const;

      /**
       * @brief Copy segment features into the current object
       * @details Links to relations or sequence are copied.
       * @param seg
       * @return reference to the current segment
       */
      virtual Segment &operator=(const Segment &seg);

      /**
       * @brief Write the entity into a RootsStream
       * @param stream the stream from which we inflate the element
       * @param is_terminal indicates that the node is terminal (true by default)
       * @param parentNode
       */
      virtual void deflate (RootsStream * stream, bool is_terminal=true, int list_index=-1);

      /**
       * @brief Extracts the entity from the stream
       * @param stream the stream from which we inflate the element
       */
      virtual void inflate(RootsStream * stream, bool is_terminal=true, int list_index=-1);

      /**
       * @brief Extracts the entity from the stream
       * @param stream the stream from which we inflate the element
       */
      static Segment * inflate_object(RootsStream * stream, int list_index=-1);

      /**
       * @brief returns the XML tag name value for current object
       * @return string constant representing the XML tag name
       */
      virtual std::string get_xml_tag_name() const { return xml_tag_name(); };

      /**
       * @brief returns the XML tag name value for current class
       * @return string constant representing the XML tag name
       */
      static std::string xml_tag_name() { return "segment"; };

      /**
       * @brief returns classname for current object
       * @return string constant representing the classname
       */
      virtual std::string get_classname() const { return classname(); };

      /**
       * @brief returns classname for current class
       * @return string constant representing the classname
       */
      static std::string classname() { return "Acoustic::Segment"; };

      /**
       * @brief returns display color for current object
       * @return string constant representing the pgf display color
       */
      virtual std::string get_pgf_display_color() const { return pgf_display_color();};

      /**
       * @brief returns display color for current class
       * @return string constant representing the pgf display color
       */
      static std::string pgf_display_color() { return "green!25"; };

      /**
       * @brief returns a string representation of any Roots object
       * @param level the precision level of the output
       * @return string representation of the object
       */
      virtual std::string to_string(int level=0) const;


      template<class U> U* as()
	{
	  //return reinterpret_cast<U*>(this);
	  //return dynamic_cast<U*>(this);
	  return static_cast<U*>(this);
	}

      /**
       * @brief Provides access to start segment
       * @return start time
       */
      double get_segment_start() const;

      /**
       * @brief Provides access to end segment
       * @return end time
       */
      double get_segment_end() const;

     /**
      * @brief return segment duration
      * @return end time - start time
      */
      double get_segment_duration() const;

      /**
       * @brief Modifies start segment
       * @param end aStartSegment
       */
      void set_segment_start(double aStartSegment);

      /**
       * @brief Modifies end segment
       * @param end aEndSegment
       */
      void set_segment_end(double aEndSegment);

      /**
       * @brief Provides access to time scaling factor
       * @return time scaling factor
       */
      virtual double get_time_scaling() const;

      /**
       * @brief Modifies time scaling factor
       * @param aTimeScalingFactor new time scaling factor
       */
      virtual void set_time_scaling(double aTimeScalingFactor);

      static const double TIME_UNIT_FACTOR_METRIC ;	/**< factor to convert to second */
      static const double TIME_UNIT_FACTOR_HTK ;  /**< factor to convert to second from HTK */

    protected:
      Segment(const bool noInit = false);

    private:
      double  startSegment;				/**< start segment */
      double  endSegment;				/**< end segment */
      double  timeScalingFactor;		/**< time scaling factor */
    };

 
  }
 } // END OF NAMESPACES


#endif /* SEGMENT_H_ */
