/*
    This file is part of Roots.

    Roots is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Roots is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Roots.  If not, see <http://www.gnu.org/licenses/>.

    Copyright (c) 2013, 2014 IRISA
*/


#ifndef ALLOPHONE_H_
#define ALLOPHONE_H_

#include "../common.h"
#include "../Phonology/Phoneme.h"


namespace roots { namespace acoustic {

/**
 * @brief Representation of an Allophone
 * @details
 * An item \p allophone links linguistic level (syllable) to signal
 * level (segment). It is defined by a phonetic label, optionally
 * formant position...
 * @author Cordial Group
 * @version 0.1
 * @date 2011
 * @todo
 * Complete instance methods on questions (is_voiced, is_plosive...) (reimpletation of ipa methods)
 */
class Allophone: public roots::phonology::Phoneme
{
protected:
	/**
	 * @brief Default constructor
	 */
	Allophone(const bool noInit = false);
public:
	/**
	 * @brief Constructor
	 * @note ipa are copied
	 * @param ipa The IPA phonological description of the allophone
	 * @param alphabet used for the allophone
	 */
	Allophone(const roots::phonology::ipa::Ipa& ipa, roots::phonology::ipa::Alphabet* alphabet=NULL);

	/**
	 * @brief Constructor
	 * @note ipa are copied
	 * @param ipa The IPA phonological description of the allophone
	 * @param ipa2 second IPA in case of diphthong
	 * @param alphabet used for the allophone
	 */
	Allophone(const roots::phonology::ipa::Ipa & ipa, const roots::phonology::ipa::Ipa & ipa2, roots::phonology::ipa::Alphabet* alphabet=NULL);
	
	/**
	 * @brief Constructor
	 * @note ipa are copied
	 * @param ipas The IPA phonological description of the allophone as a vector of IPA objects
	 * @param alphabet used for the allophone
	 */
	Allophone( const std::vector< roots::phonology::ipa::Ipa* >& ipas, roots::phonology::ipa::Alphabet* alphabet = 0 );

	/**
	 * @brief Copy constructor
	 * @details This constructor does not copy links to relations or sequence.
	 * @param all
	 */
	Allophone(const Allophone &all);
	/**
	 * @brief Destructor
	 * @todo check
	 */
	virtual ~Allophone();
	/**
	 * @brief clone the current object
	 * @return a copy of the object
	 */
	virtual Allophone *clone() const;

	/**
	 * @brief Copy allophone features into the current allphone
	 * @details Links to relations or sequence are copied.
	 * @param all
	 * @return reference to the current allophone
	 */
	Allophone& operator=(const Allophone &all);
	/**
	 * @brief Write the entity into a RootsStream
	 * @param stream the stream from which we inflate the element
	 * @param is_terminal indicates that the node is terminal (true by default)
	 * @param parentNode
	 */
	virtual void deflate(RootsStream * stream, bool is_terminal=true, int list_index=-1);

	/**
	 * @brief Extracts the entity from the stream
	 * @param stream the stream from which we inflate the element
	 */
	virtual void inflate(RootsStream * stream, bool is_terminal=true, int list_index=-1);
	/**
	 * @brief Extracts the entity from the stream
	 * @param stream the stream from which we inflate the element
	 */
	static Allophone * inflate_object(RootsStream * stream, int list_index=-1);
	/**
	 * @brief returns the XML tag name value for current object
	 * @return string constant representing the XML tag name
	 */
	virtual std::string get_xml_tag_name() const { return xml_tag_name(); };
	/**
	 * @brief returns the XML tag name value for current class
	 * @return string constant representing the XML tag name
	 */
	static std::string xml_tag_name() { return "allophone"; };
	/**
	 * @brief returns classname for current object
	 * @return string constant representing the classname
	 */
	virtual std::string get_classname() const { return classname(); };
	/**
	 * @brief returns classname for current class
	 * @return string constant representing the classname
	 */
	static std::string classname() { return "Acoustic::Allophone"; };
	/**
	 * @brief returns display color for current object
	 * @return string constant representing the pgf display color
	 */
	virtual std::string get_pgf_display_color() const { return pgf_display_color();};
	/**
	 * @brief returns display color for current class
	 * @return string constant representing the pgf display color
	 */
	static std::string pgf_display_color() { return "red!25"; };

private:

};

} } // END OF NAMESPACES

#endif /* ALLOPHONE_H_ */
