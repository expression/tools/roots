/*
  This file is part of Roots.

  Roots is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Roots is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	See the
  GNU Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with Roots.  If not, see <http://www.gnu.org/licenses/>.

  Copyright (c) 2013, 2014 IRISA
*/


#ifndef SPECTRALSEGMENT_H_
#define SPECTRALSEGMENT_H_

#include "../../common.h"
#include "../../Base.h"
#include "../../Acoustic/Segment.h"
#include "../../RootsException.h"
#include "../../File/HTK.h"
#include "../../Align/DTW.h"

#include <numeric>
#include <sstream>

namespace roots{
  namespace acoustic{

    /**
     * @brief Representation of a spectral segment
     * @details
     * This class is derived from Segment and provides additionnal access
     * to the vectors for the segment. The spectal vectors
     * are loaded into memory.
     * @author Cordial Group
     * @version 0.1
     * @date 2011
     * @todo to_string, to_pgf
     */
    class SpectralSegment: public roots::acoustic::Segment, public roots::file::htk::HTK
      {
      public:

	SpectralSegment(const std::string& aFileName, const std::string& spectralKind, double aStartTime, double aEndTime, float frameLength=0.0, float frameShift=0.0);

	/**
	 * @brief Copy constructor
	 * @param seg
	 */
	SpectralSegment(const SpectralSegment& seg);
	/**
	 * @brief Destructor
	 * @todo check
	 */
	virtual ~SpectralSegment();
	/**
	 * @brief clone the current object
	 * @return a copy of the object
	 */
	virtual SpectralSegment *clone() const;
	/**
	 * @brief Copy segment features into the current object
	 * @details Links to relations or sequence are copied.
	 * @param seg
	 * @return reference to the current segment
	 */
	SpectralSegment& operator=(const SpectralSegment& seg);

	const std::string& get_base_dir_name() const;
	void set_base_dir_name(const std::string& name);

	void set_file_name(const std::string& filename);
	const std::string& get_file_name() const;

	roots::file::htk::htk_parameter_kind_t get_spectral_kind() const;
	void set_spectral_kind(roots::file::htk::htk_parameter_kind_t aSpectralKind);

	float get_frame_length() const;
	void set_frame_length(float aFrameLength);

	float get_frame_shift() const;
	void set_frame_shift(float aFrameShift);

	int get_energy_dimension_index() const;
	void set_energy_dimension_index(int aEnergyDimensionIndex);

	int get_delta_dimension_start_index() const;
	void set_delta_dimension_start_index(int aDeltaDimensionStartIndex);

	int get_delta_dimension_end_index() const;
	void set_delta_dimension_end_index(int aDeltaDimensionEndIndex);

	int get_acceleration_dimension_start_index() const;
	void set_acceleration_dimension_start_index(int aAccelerationDimensionStartIndex);

	int get_acceleration_dimension_end_index() const;
	void set_acceleration_dimension_end_index(int aAccelerationDimensionEndIndex);

	bool is_frame_time_reference_centered() const;
	void set_is_frame_time_reference_centered(int isframeTimeReferencCentered);



	/**
	 * @brief Return value for a spectrum time.
	 **/
	std::vector<float> get_value_from_time(double time, roots::Approx approx = APPROX_ROUND) ;

	/**
	 * @brief Return values for spectrum interval
	 **/
	std::vector<std::vector<float> > get_values_from_time(double startTime=0,
							      double endTime=-1,
							      roots::Approx approxStart = APPROX_FLOOR,
							      roots::Approx approxEnd = APPROX_CEIL) ;

	long convert_time_to_index( double time, roots::Approx approxPolicy=APPROX_ROUND);
	double convert_index_to_time( long idx) const;

	/**
	 * @brief Compute DTW with a same dimension vector of vector
	 **/
	double DTW(std::vector<std::vector<float> >& s2);

	/**
	 * @brief Compute DTW with a second SpectralSegment
	 **/
	double DTW(SpectralSegment& s2);

	/**
	 * @brief Compute normalized by length DTW with a same dimension vector of vector
	 **/
	double DTW_normalized(std::vector<std::vector<float> >& s2);

	/**
	 * @brief Compute normalized by length DTW with a second SpectralSegment
	 **/
	double DTW_normalized(SpectralSegment& s2);

	/**
	 * @brief returns a string representation of any Roots object
	 * @param level the precision level of the output
	 * @return string representation of the object
	 */
	virtual std::string to_string(int level=0) const;

	/**
	 * @brief Prepare an item to be saved in a directory. Basicaly, a signal segment items will copy the associated wav file if target_base_dir change.
	 * @Warning items may be changed (for instance the base directory of signal segment items)!
	 */
	virtual void prepare_save(const std::string & target_base_dir);

	/**
	 * @brief Write the entity into a RootsStream
	 * @param stream the stream from which we inflate the element
	 * @param is_terminal indicates that the node is terminal (true by default)
	 * @param parentNode
	 */
	virtual void deflate(RootsStream * stream, bool is_terminal=true, int list_index=-1);
	/**
	 * @brief Extracts the entity from the stream
	 * @param stream the stream from which we inflate the element
	 */
	virtual void inflate(RootsStream * stream, bool is_terminal=true, int list_index=-1);
	/**
	 * @brief Extracts the entity from the stream
	 * @param stream the stream from which we inflate the element
	 */
	static SpectralSegment * inflate_object(RootsStream * stream, int list_index=-1);
	/**
	 * @brief returns classname for current object
	 * @return string constant representing the classname
	 */
	virtual std::string get_classname() const { return classname(); };
	/**
	 * @brief returns classname for current class
	 * @return string constant representing the classname
	 */
	static std::string classname() { return "Acoustic::Segment::SpectralSegment"; };

      protected:
	void update_sub_filename();


      private:

	SpectralSegment(const bool noInit = false);

	std::string	    filename;
	std::string					baseDirName;
	float								frameLength;
	float								frameShift;
	int									energyDimension;
	int									deltaEndDimension;
	int									deltaStartDimension;
	int									accelerationStartDimension;
	int									accelerationEndDimension;
	bool								frameTimeReferencIsCentered;
      };

  } // End namespace acoustic
} // End namespace roots

#endif /* SAMPLESEGMENT_H_ */
