/*
    This file is part of Roots.

    Roots is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Roots is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Roots.  If not, see <http://www.gnu.org/licenses/>.

    Copyright (c) 2013, 2014 IRISA
*/


#include "F0Segment.h"
#include "../../File/F0.h"
#include <boost/graph/graph_concepts.hpp>
#include <boost/filesystem.hpp>

namespace bfs = boost::filesystem;


namespace roots
{
  namespace acoustic
  {

    F0Segment::F0Segment(const bool noInit) :
    Segment(noInit),
      F0()
    {
      set_file_name("");
      set_base_dir_name("");
    }

    F0Segment::F0Segment(const std::string& aFileName,
			 double aStartSample,
			 double aEndSample,
			 int aSamlingFrequency,
			 roots::file::f0::file_format_t aFormat,
			 double aTimeScalingFactor) :
    Segment(aStartSample,
	    aEndSample,
	    aTimeScalingFactor),
      F0()
    {
      set_file_name(aFileName);
      set_base_dir_name(std::string(""));

      set_file_format(aFormat);

      set_sampling_frequency(aSamlingFrequency);
    }

    F0Segment::F0Segment(const F0Segment& seg) :
    Segment(seg),
      F0(seg)
    {
      set_file_name(seg.get_file_name());
      set_base_dir_name(seg.get_base_dir_name());
    }

    F0Segment::~F0Segment()
    {
    }

    F0Segment *F0Segment::clone() const
    {
      return new F0Segment(*this);
    }

    F0Segment& F0Segment::operator=(const F0Segment& seg)
    {
      Segment::operator=(seg);
      F0::operator=(seg);

      set_file_name(seg.get_file_name());
      set_base_dir_name(seg.get_base_dir_name());

      return *this;
    }

    void F0Segment::update_sub_filename()
    {
      unload();
      /* Update file::f0 */
      bfs::path source_path(get_base_dir_name());
      source_path /= get_file_name();
      F0::set_file_name(source_path.string());
    }

    const std::string& F0Segment::get_base_dir_name() const
    {
      return baseDirName;
    }

    void F0Segment::set_base_dir_name(const std::string& name)
    {
      baseDirName = name;
      update_sub_filename();
    }

    const std::string& F0Segment::get_file_name() const
    {
      return filename;
    }

    void F0Segment::set_file_name(const std::string& filename)
    {
      this->filename = filename;
      update_sub_filename();
    }

    void F0Segment::prepare_save(const std::string & target_base_dir)
    {
      bfs::path source_path(get_base_dir_name());
      bfs::path target_path(target_base_dir);
#if BOOST_VERSION < 104800
      source_path = bfs::canonical(source_path);
      target_path = bfs::canonical(target_path);
#else
      // WARNING: to be checked
      source_path = source_path.normalize();
      target_path = target_path.normalize();
#endif
      // If difference between target basedir and current basedir
      if (source_path != target_path)
	{
	  // Copy target file
	  source_path /= this->get_file_name();
	  target_path /= this->get_file_name();

	  if (!boost::filesystem::exists(target_path.parent_path()))
	    { bfs::create_directories(target_path.parent_path()); }
	  bfs::copy_file(source_path, target_path, bfs::copy_option::overwrite_if_exists);

	  // Then, change basedir
	  set_base_dir_name(target_base_dir);
	}
      // Otherwise, nothing to do !
    }

    void F0Segment::deflate(RootsStream * stream, bool is_terminal, int list_index)
    {
      Segment::deflate(stream,false,list_index);

      stream->append_int_content("f0_sampling_frequency", this->get_sampling_frequency());
      stream->append_unicodestring_content("file_name", this->get_file_name());
      stream->append_int_content("file_format", this->get_file_format());
      stream->append_double_content("time_offset", this->get_time_offset());
      stream->append_int_content("f0_unit", this->get_f0_unit());

      if(is_terminal)
	stream->close_object();
    }


    void F0Segment::inflate(RootsStream * stream, bool is_terminal, int list_index)
    {
      Segment::inflate(stream,false,list_index);

      //stream->open_children();
      this->set_sampling_frequency(stream->get_double_content("f0_sampling_frequency"));
      this->set_file_name(stream->get_unicodestring_content("file_name"));

      int file_format = stream->get_uint_content("file_format");
      if(file::f0::F0::fileFormatToString.find((file::f0::file_format_t)file_format) != 
	 file::f0::F0::fileFormatToString.end())
	{this->set_file_format((file::f0::file_format_t)file_format);}
      else
	{this->set_file_format(file::f0::FILE_FORMAT_F0_UNDEF);}

      this->set_time_offset(stream->get_double_content("time_offset"));

      int f0_unit = stream->get_uint_content("f0_unit");
      if(file::f0::F0::f0unitToString.find((file::f0::f0_unit_t)f0_unit) != 
	 file::f0::F0::f0unitToString.end())
	{this->set_f0_unit((file::f0::f0_unit_t)f0_unit);}
      else
	{this->set_f0_unit(file::f0::F0_UNIT_HZ);}

      this->set_base_dir_name(stream->get_base_dir_name());

      if(is_terminal) stream->close_children();
    }

    F0Segment * F0Segment::inflate_object(RootsStream * stream, int list_index)
    {
      F0Segment *t = new F0Segment(true);
      t->inflate(stream,true,list_index);
      return t;
    }

  }
} // END OF NAMESPACES
