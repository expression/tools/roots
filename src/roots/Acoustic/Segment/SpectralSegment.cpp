/*
  This file is part of Roots.

  Roots is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Roots is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	See the
  GNU Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with Roots.  If not, see <http://www.gnu.org/licenses/>.

  Copyright (c) 2013, 2014 IRISA
*/


#include "SpectralSegment.h"
#include <boost/filesystem.hpp>

namespace bfs = boost::filesystem;

namespace roots{
  namespace acoustic{

    SpectralSegment::SpectralSegment(const bool noInit) : Segment(noInit), HTK()
    {
      set_file_name(std::string(""));
      set_base_dir_name(std::string(""));
      set_spectral_kind(roots::file::htk::HTK_PARAMETER_KIND_UNDEF);
      set_frame_length(0.0);
      set_frame_shift(0.0);
      set_energy_dimension_index(-1);
      set_delta_dimension_start_index(-1);
      set_delta_dimension_end_index(-1);
      set_acceleration_dimension_start_index(-1);
      set_acceleration_dimension_end_index(-1);
      set_is_frame_time_reference_centered(false);
    }


    SpectralSegment::SpectralSegment(const std::string& aFileName, const std::string& spectralKind, double aStartTime, double aEndTime, float frameLength, float frameShift) :
      Segment((long double)aStartTime,(long double)aEndTime,Segment::TIME_UNIT_FACTOR_METRIC), HTK(aFileName)
    {
      set_file_name(aFileName);
      set_base_dir_name(std::string(""));

      int i=0;
      while((i<roots::file::htk::HTK_PARAMETER_KIND_LENGTH)
	    &&(roots::file::htk::HTK_PARAMETER_KIND[i]!=spectralKind))
	{
	  i = i + 1;
	}
      if(i>=roots::file::htk::HTK_PARAMETER_KIND_LENGTH)
	{
	  set_spectral_kind(roots::file::htk::HTK_PARAMETER_KIND_UNDEF);
	}
      else
	{
	  set_spectral_kind((enum roots::file::htk::htk_parameter_kind_t)i);
	}

      set_frame_length(frameLength);
      set_frame_shift(frameShift);
      set_energy_dimension_index(-1);
      set_delta_dimension_start_index(-1);
      set_delta_dimension_end_index(-1);
      set_acceleration_dimension_start_index(-1);
      set_acceleration_dimension_end_index(-1);
      set_is_frame_time_reference_centered(false);
    }


    SpectralSegment::SpectralSegment(const SpectralSegment& seg) : Segment(seg), HTK(seg)
    {
      set_file_name(seg.get_file_name());
      set_base_dir_name(seg.get_base_dir_name());
      set_spectral_kind(seg.get_spectral_kind());
      set_frame_length(seg.get_frame_length());
      set_frame_shift(seg.get_frame_shift());
      set_energy_dimension_index(seg.get_energy_dimension_index());
      set_delta_dimension_start_index(seg.get_delta_dimension_start_index());
      set_delta_dimension_end_index(seg.get_delta_dimension_end_index());
      set_acceleration_dimension_start_index(seg.get_acceleration_dimension_start_index());
      set_acceleration_dimension_end_index(seg.get_acceleration_dimension_end_index());
      set_is_frame_time_reference_centered(seg.is_frame_time_reference_centered());
    }

    SpectralSegment::~SpectralSegment()
    {	 }

    SpectralSegment *SpectralSegment::clone() const
    {
      return new SpectralSegment(*this);
    }

    SpectralSegment& SpectralSegment::operator=(const SpectralSegment& seg)
    {
      Segment::operator=(seg);
      roots::file::htk::HTK::operator=(seg);

      set_file_name(seg.get_file_name());
      set_base_dir_name(seg.get_base_dir_name());
      set_spectral_kind(seg.get_spectral_kind());
      set_frame_length(seg.get_frame_length());
      set_frame_shift(seg.get_frame_shift());
      set_energy_dimension_index(seg.get_energy_dimension_index());
      set_delta_dimension_start_index(seg.get_delta_dimension_start_index());
      set_delta_dimension_end_index(seg.get_delta_dimension_end_index());
      set_acceleration_dimension_start_index(seg.get_acceleration_dimension_start_index());
      set_acceleration_dimension_end_index(seg.get_acceleration_dimension_end_index());
      set_is_frame_time_reference_centered(seg.is_frame_time_reference_centered());

      return *this;
    }

    void SpectralSegment::update_sub_filename()
    {
      unload();
      /* Update file::f0 */
      bfs::path source_path(get_base_dir_name());
      source_path /= get_file_name();
      HTK::set_file_name(source_path.string());
    }

    const std::string& SpectralSegment::get_base_dir_name() const
    {
      return baseDirName;
    }

    void SpectralSegment::set_base_dir_name(const std::string& name)
    {
      baseDirName = name;
      update_sub_filename();
    }

    const std::string& SpectralSegment::get_file_name() const
    {
      return filename;
    }

    void SpectralSegment::set_file_name(const std::string& filename)
    {
      this->filename = filename;
      update_sub_filename();
    }

    roots::file::htk::htk_parameter_kind_t SpectralSegment::get_spectral_kind() const
    {
      return get_parameter_kind_htk();
    }

    void SpectralSegment::set_spectral_kind(roots::file::htk::htk_parameter_kind_t aSpectralKind)
    {
      set_parameter_kind_htk(aSpectralKind);
    }

    float SpectralSegment::get_frame_length() const
    {
      return this->frameLength;
    }

    void SpectralSegment::set_frame_length(float aFrameLength)
    {
      this->frameLength = aFrameLength;
    }

    float SpectralSegment::get_frame_shift() const
    {
      return this->frameShift;
    }

    void SpectralSegment::set_frame_shift(float aFrameShift)
    {
      this->frameShift = aFrameShift;
    }

    int SpectralSegment::get_energy_dimension_index() const
    {
      return this->energyDimension;
    }

    void SpectralSegment::set_energy_dimension_index(int aEnergyDimensionIndex)
    {
      this->energyDimension = aEnergyDimensionIndex;
    }

    int SpectralSegment::get_delta_dimension_start_index() const
    {
      return this->deltaStartDimension;
    }

    void SpectralSegment::set_delta_dimension_start_index(int aDeltaDimensionStartIndex)
    {
      this->deltaStartDimension = aDeltaDimensionStartIndex;
    }

    int SpectralSegment::get_delta_dimension_end_index() const
    {
      return this->deltaEndDimension;
    }

    void SpectralSegment::set_delta_dimension_end_index(int aDeltaDimensionEndIndex)
    {
      this->deltaEndDimension = aDeltaDimensionEndIndex;
    }

    int SpectralSegment::get_acceleration_dimension_start_index() const
    {
      return this->accelerationStartDimension;
    }

    void SpectralSegment::set_acceleration_dimension_start_index(int aAccelerationDimensionStartIndex)
    {
      this->accelerationStartDimension = aAccelerationDimensionStartIndex;
    }

    int SpectralSegment::get_acceleration_dimension_end_index() const
    {
      return this->accelerationEndDimension;
    }

    void SpectralSegment::set_acceleration_dimension_end_index(int aAccelerationDimensionEndIndex)
    {
      this->accelerationEndDimension = aAccelerationDimensionEndIndex;
    }

    bool SpectralSegment::is_frame_time_reference_centered() const
    {
      return this->frameTimeReferencIsCentered;
    }

    void SpectralSegment::set_is_frame_time_reference_centered(int isframeTimeReferenceCentered)
    {
      this->frameTimeReferencIsCentered = isframeTimeReferenceCentered;
    }

    long SpectralSegment::convert_time_to_index( double time, roots::Approx approxPolicy)
    {
      if(this->get_frame_shift()==0)
	{
	  std::stringstream ss;
	  ss << "SpectralSegment::convert_time_to_index(), frame shift should be not null";
	  throw RootsException(__FILE__,__LINE__, ss.str());
	}

      double virtualIndex = time;
      if(this->is_frame_time_reference_centered())
	{
	  if(this->get_frame_length()==0)
	    {
	      std::stringstream ss;
	      ss << "SpectralSegment::convert_time_to_index(), frame length should be not null";
	      throw RootsException(__FILE__,__LINE__, ss.str());
	    }

	  virtualIndex -= this->get_frame_length()/2;
	}
      virtualIndex = virtualIndex/this->get_frame_shift();

      virtualIndex =  std::max(virtualIndex, 0.0);

      if(roots::TimeUtils::TimeUtils::equal_to(convert_index_to_time(std::floor(virtualIndex)), time))
	{virtualIndex = std::floor(virtualIndex);}
      else if(roots::TimeUtils::TimeUtils::equal_to(convert_index_to_time(std::ceil(virtualIndex)), time))
	{virtualIndex = std::ceil(virtualIndex);}

      std::stringstream ss;
      switch(approxPolicy)
	{
	case APPROX_FLOOR:
	  return std::floor(virtualIndex);
	  break;
	case APPROX_CEIL :
	  return std::ceil(virtualIndex);
	  break;
	case APPROX_ROUND :
	  return std::floor(virtualIndex + 0.5);
	  break;
	case APPROX_STRICT_FLOOR :
	  return std::max(std::ceil(virtualIndex - 1 ), 0.0);
	  break;
	case APPROX_STRICT_CEIL :
	  return std::min(std::floor(virtualIndex + 1 ),
			  (double)(this->get_vector_loaded_start() + this->get_n_vector_loaded()));
	  break;
	case APPROX_UNDEF :
	default:
	  ss << "SpectralSegment::convert_time_to_index: Unknow or undef approximation policy!";
	  throw RootsException(__FILE__, __LINE__, ss.str().c_str());
	}
    }

    double SpectralSegment::convert_index_to_time( long idx) const
    {
      if(this->get_frame_shift()==0)
	{
	  std::stringstream ss;
	  ss << "SpectralSegment::convert_index_to_time(), frame shift should be not null";
	  throw RootsException(__FILE__,__LINE__, ss.str());
	}

      double curTime = (double)idx*this->get_frame_shift();
      if(this->is_frame_time_reference_centered())
	{
	  if(this->get_frame_length()==0)
	    {
	      std::stringstream ss;
	      ss << "SpectralSegment::convert_index_to_time(), frame length should be not null";
	      throw RootsException(__FILE__,__LINE__, ss.str());
	    }

	  curTime += this->get_frame_length()/2;
	}

      return curTime;
    }




    /*******************************************/

    std::vector<float> SpectralSegment::get_value_from_time(double time, roots::Approx approx)
    {
      if(!this->is_loaded())
	{ load(); }

      return get_value(convert_time_to_index(time, approx));
    }


    std::vector<std::vector<float> > SpectralSegment::get_values_from_time(double startTime,	double endTime,	roots::Approx approxStart, roots::Approx approxEnd)
    {
      if(!this->is_loaded())
	{ load(); }

      return get_values(convert_time_to_index(startTime, approxStart),
			convert_time_to_index(endTime, approxEnd));
    }

    /*******************************************/

    double SpectralSegment::DTW(std::vector<std::vector<float> >& s2){

      std::vector<std::vector<float> > s1 = get_values_from_time();
      roots::align::DTW<float> a(s1, s2);
      a.align();
      return a.get_cost();

    }

    double SpectralSegment::DTW(SpectralSegment& s2){
      std::vector<std::vector<float> > v = s2.get_values_from_time();
      return DTW(v);
    }

   double SpectralSegment::DTW_normalized(std::vector<std::vector<float> >& s2){

      std::vector<std::vector<float> > s1 = get_values_from_time();
      roots::align::DTW<float> a(s1, s2);
      a.align();
      return a.get_dissimilarity();

    }

    double SpectralSegment::DTW_normalized(SpectralSegment& s2){
      std::vector<std::vector<float> > v = s2.get_values_from_time();
      return DTW_normalized(v);
    }
    /*******************************************/


    std::string SpectralSegment::to_string(int level __attribute__((unused))) const
    {
      std::stringstream ss;

      ss.setf(std::ios::fixed,std::ios::floatfield);
      ss.precision(3);

      if(get_time_scaling()==TIME_UNIT_FACTOR_METRIC)
	{
	  ss	<< get_segment_start() << " - " << get_segment_end() << " (s)";
	}
      else
	{
	  ss	<< get_segment_start() << " - " << get_segment_end() << " (ns)";
	}

      return std::string(ss.str().c_str());
    }

    void SpectralSegment::prepare_save(const std::string & target_base_dir)
    {
      std::stringstream source, target;
      source << get_base_dir_name();
      target << target_base_dir;

      bfs::path source_path(source.str().c_str());
      bfs::path target_path(target.str().c_str());
#if BOOST_VERSION < 104800
      source_path = bfs::canonical(source_path);
      target_path = bfs::canonical(target_path);
#else
      // WARNING: to be checked
      source_path = source_path.normalize();
      target_path = target_path.normalize();
#endif
      // If difference between target basedir and current basedir
      if (source_path != target_path)
	{
	  // Copy target file
	  source_path /= this->get_file_name();
	  target_path /= this->get_file_name();

	  if (!boost::filesystem::exists(target_path.parent_path()))
	    { bfs::create_directories(target_path.parent_path()); }
	  bfs::copy_file(source_path, target_path, bfs::copy_option::overwrite_if_exists);

	  // Then, change basedir
	  set_base_dir_name(target_base_dir);
	}
      // Otherwise, nothing to do !
    }

    void SpectralSegment::deflate(RootsStream * stream, bool is_terminal, int list_index)
    {
      Segment::deflate(stream,false,list_index);

      stream->append_int_content("spectral_kind", this->get_spectral_kind());
      stream->append_unicodestring_content("file_name", this->get_file_name());
      stream->append_float_content("frame_length", this->get_frame_length());
      stream->append_float_content("frame_shift", this->get_frame_shift());
      stream->append_int_content("energy_index", this->get_energy_dimension_index());
      stream->append_int_content("delta_start_index", this->get_delta_dimension_start_index());
      stream->append_int_content("delta_end_index", this->get_delta_dimension_end_index());
      stream->append_int_content("acceleration_start_index", this->get_acceleration_dimension_start_index());
      stream->append_int_content("acceleration_end_index", this->get_acceleration_dimension_end_index());
      stream->append_bool_content("frame_time_reference_centered", this->is_frame_time_reference_centered());

      if(is_terminal)
	stream->close_object();
    }


    void SpectralSegment::inflate(RootsStream * stream, bool is_terminal, int list_index)
    {
      Segment::inflate(stream,false,list_index);

      //stream->open_children();
      this->set_spectral_kind((roots::file::htk::htk_parameter_kind_t)stream->get_uint_content("spectral_kind"));
      this->set_file_name(stream->get_unicodestring_content("file_name"));
      this->set_frame_length(stream->get_float_content("frame_length"));
      this->set_frame_shift(stream->get_float_content("frame_shift"));
      this->set_energy_dimension_index(stream->get_int_content("energy_index"));
      this->set_delta_dimension_start_index(stream->get_int_content("delta_start_index"));
      this->set_delta_dimension_end_index(stream->get_int_content("delta_end_index"));
      this->set_acceleration_dimension_start_index(stream->get_int_content("acceleration_start_index"));
      this->set_acceleration_dimension_end_index(stream->get_int_content("acceleration_end_index"));
      this->set_is_frame_time_reference_centered(stream->get_int_content("frame_time_reference_centered"));
      if(is_terminal) stream->close_children();

      this->set_base_dir_name(stream->get_base_dir_name());
    }

    SpectralSegment * SpectralSegment::inflate_object(RootsStream * stream, int list_index)
    {
      SpectralSegment *t = new SpectralSegment(true);
      t->inflate(stream,true,list_index);
      return t;
    }

  } // End namespace acoustic
} // End namespace roots
