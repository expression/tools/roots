/*
    This file is part of Roots.

    Roots is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Roots is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Roots.  If not, see <http://www.gnu.org/licenses/>.

    Copyright (c) 2013, 2014 IRISA
*/


#ifndef TIMESEGMENT_H_
#define TIMESEGMENT_H_

#include "../../common.h"
#include "../../Base.h"
#include "../../Acoustic/Segment.h"
#include "../../RootsException.h"
#include "../../File/Audio.h"

#include <numeric>
#include <sstream>

namespace roots
{

namespace acoustic
{

/**
 * @brief Representation of a signal segment with signal samples
 * @details 
 * This class is derived from Segment and provides additionnal access
 * to the signal samples for the segment. This class uses Sox library
 * to load and save files and detect the format of the signal. The signal
 * is loaded into memory in a unique format. See Sox library documentation
 * for more info.
 * @author Cordial Group
 * @version 0.1
 * @date 2011
 * @todo to_string, to_pgf
 */
		class TimeSegment: public roots::acoustic::Segment
		{
		public:
			/**
			 * @brief Constructor
			 * @param aFilename
			 * @param aStartSample
			 * @param aEndSample
			 * @param aTimeScalingFactor
			 * @param aSamlingFrequency
			 */

			TimeSegment(double aStartTime, double aEndTime);

			TimeSegment(double aStartTime, double aEndTime, double aTimeScalingFactor);
			/**
			 * @brief Copy constructor
			 * @param seg 
			 */
			TimeSegment(const TimeSegment& seg);
			/**
			 * @brief Destructor
			 * @todo check
			 */
			virtual ~TimeSegment();
			/**
			 * @brief clone the current object
			 * @return a copy of the object
			 */
			virtual TimeSegment *clone() const;
			/**
			 * @brief Copy segment features into the current object
			 * @details Links to relations or sequence are copied.
			 * @param seg
			 * @return reference to the current segment
			 */
			TimeSegment& operator=(const TimeSegment& seg);

			virtual double get_time_start() const;

			virtual double get_time_end() const;

			double duration();

			void set_time_start(double aTimeStart);

			void set_time_end(double aTimeEnd);

			/**
			 * @brief returns a string representation of any Roots object
			 * @param level the precision level of the output
			 * @return string representation of the object
			 */
			virtual std::string to_string(int level=0) const;

			/**
			 * @brief Write the entity into a RootsStream
			 * @param stream the stream from which we inflate the element
			 * @param is_terminal indicates that the node is terminal (true by default)
			 * @param parentNode
			 */
			virtual void deflate(RootsStream * stream, bool is_terminal=true, int list_index=-1);

			/**
			 * @brief Extracts the entity from the stream
			 * @param stream the stream from which we inflate the element
			 */
			virtual void inflate(RootsStream * stream, bool is_terminal=true, int list_index=-1);
			/**
			 * @brief Extracts the entity from the stream
			 * @param stream the stream from which we inflate the element
			 */
			static TimeSegment * inflate_object(RootsStream * stream, int list_index=-1);
			/**
			 * @brief returns classname for current object
			 * @return string constant representing the classname
			 */
			virtual std::string get_classname() const { return classname(); };
			/**
			 * @brief returns classname for current class
			 * @return string constant representing the classname
			 */
			static std::string classname() { return "Acoustic::Segment::TimeSegment"; };

		private:

			TimeSegment(const bool noInit = false);
		};

	} } // END OF NAMESPACES

#endif /* SAMPLESEGMENT_H_ */
