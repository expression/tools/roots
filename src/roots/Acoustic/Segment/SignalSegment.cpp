/*
    This file is part of Roots.

    Roots is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Roots is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Roots.  If not, see <http://www.gnu.org/licenses/>.

    Copyright (c) 2013, 2014 IRISA
*/


#include "SignalSegment.h"
#include <boost/filesystem.hpp>

namespace bfs = boost::filesystem;

namespace roots
{

  namespace acoustic
  {

    SignalSegment::SignalSegment(const bool noInit) :
      Segment(noInit),
      Audio()
    {
      set_base_dir_name("",false);
      set_file_name("");
    }

    SignalSegment::SignalSegment(const std::string& aFileName,
				 double aStartValue,
				 double aEndValue,
				 int aSamplingFrequency,
				 roots::file::audio::file_format_t aFormat,
				 double aTimeScalingFactor ):
      Segment(aStartValue, aEndValue, aTimeScalingFactor),
      Audio()
    {
      set_base_dir_name("",false);
      set_file_name(aFileName);
      set_file_format(aFormat);
      set_rate(aSamplingFrequency);
    }

    SignalSegment::SignalSegment(const SignalSegment& seg) :
      Segment(seg),
      Audio(seg)
    {
      set_base_dir_name(seg.get_base_dir_name(),false);
      set_file_name(seg.get_file_name());
    }

    SignalSegment::~SignalSegment()
    {
    }

    SignalSegment *SignalSegment::clone() const
    {
      return new SignalSegment(*this);
    }

    SignalSegment& SignalSegment::operator=(const SignalSegment& seg)
    {
      Segment::operator=(seg);
      Audio::operator=(seg);

      set_base_dir_name(seg.get_base_dir_name(),false);
      set_file_name(seg.get_file_name());

      return *this;
    }

    void SignalSegment::update_sub_filename()
    {
      unload();
      /* Update file::f0 */
      bfs::path source_path(get_base_dir_name());
      source_path /= get_file_name();
      Audio::set_file_name(source_path.string());
    }

    const std::string& SignalSegment::get_base_dir_name() const
    {
      return baseDirName;
    }

    void SignalSegment::set_base_dir_name(const std::string& name, bool updateSub)
    {
      baseDirName = name;
      if(updateSub) {update_sub_filename();}
    }

    const std::string& SignalSegment::get_file_name() const
    {
      return filename;
    }

    void SignalSegment::set_file_name(const std::string& filename, bool updateSub)
    {
      this->filename = filename;
      if(updateSub) {update_sub_filename();}
    }

    void SignalSegment::prepare_save(const std::string & target_base_dir)
    {
      bfs::path source_path(get_base_dir_name());
      bfs::path target_path(target_base_dir);
#if BOOST_VERSION < 104800
      source_path = bfs::canonical(source_path);
      target_path = bfs::canonical(target_path);
#else
      // WARNING: to be checked
      source_path = source_path.normalize();
      target_path = target_path.normalize();
#endif
      // If difference between target basedir and current basedir
      if (source_path != target_path)
	{
	  // Copy target file
	  source_path /= this->get_file_name();
	  target_path /= this->get_file_name();

	  if (!boost::filesystem::exists(target_path.parent_path()))
	    { bfs::create_directories(target_path.parent_path()); }
	  bfs::copy_file(source_path, target_path, bfs::copy_option::overwrite_if_exists);

	  // Then, change basedir
	  set_base_dir_name(target_base_dir);
	}
      // Otherwise, nothing to do !
    }

    void SignalSegment::deflate(RootsStream * stream, bool is_terminal, int list_index)
    {
      Segment::deflate(stream,false,list_index);

      if(this->get_rate()==0)
	{
	  ROOTS_LOGGER_WARN("SignalSegment::deflate, sampling frequency is null");
	}

      stream->append_int_content("sampling_frequency", this->get_rate());
      stream->append_unicodestring_content("file_name", this->get_file_name());
      stream->append_int_content("n_channel", this->get_channels());
      stream->append_int_content("precision", this->get_precision());

      if(is_terminal)
	stream->close_object();
    }

    void SignalSegment::inflate(RootsStream * stream, bool is_terminal, int list_index)
    {
      Segment::inflate(stream,false,list_index);

      //stream->open_children();
      this->set_rate(stream->get_double_content("sampling_frequency"));
      this->set_file_name(stream->get_unicodestring_content("file_name"),false);
      this->set_channels(stream->get_int_content("n_channel"));

      // Optional for backward compatibility - please make it mandatory one day ! (2014)
      if (stream->has_child("precision"))
	{ this->set_precision(stream->get_int_content("precision")); }

      if(is_terminal) stream->close_children();
      this->set_base_dir_name(stream->get_base_dir_name());

    }

    SignalSegment * SignalSegment::inflate_object(RootsStream * stream, int list_index)
    {
      SignalSegment *t = new SignalSegment(true);
      t->inflate(stream,true,list_index);
      return t;
    }

    std::string SignalSegment::to_string(int level) const
    {
        std::stringstream ss;

        if (level == 0)
          {
            ss<<Segment::to_string(level);
          } else if (level >= 1)
          {
            ss<<this->get_file_name();
          } else {
            ss<<Segment::to_string(level)<<" "<<this->get_file_name();
          }

      return std::string(ss.str());
    }
  }
} // END OF NAMESPACES
