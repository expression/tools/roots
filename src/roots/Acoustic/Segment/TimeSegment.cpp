/*
    This file is part of Roots.

    Roots is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Roots is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Roots.  If not, see <http://www.gnu.org/licenses/>.

    Copyright (c) 2013, 2014 IRISA
*/

#include "TimeSegment.h"

namespace roots {

namespace acoustic {

TimeSegment::TimeSegment(const bool noInit) : Segment(noInit) {}

TimeSegment::TimeSegment(double aStartTime, double aEndTime)
    : Segment((long double)aStartTime, (long double)aEndTime) {}

TimeSegment::TimeSegment(double aStartTime, double aEndTime,
                         double aTimeScalingFactor)
    : Segment((long double)aStartTime, (long double)aEndTime,
              aTimeScalingFactor) {}

TimeSegment::TimeSegment(const TimeSegment& seg) : Segment(seg) {}

TimeSegment::~TimeSegment() {}

TimeSegment* TimeSegment::clone() const { return new TimeSegment(*this); }

TimeSegment& TimeSegment::operator=(const TimeSegment& seg) {
  Segment::operator=(seg);

  return *this;
}

double TimeSegment::get_time_start() const {
  return (double)this->get_segment_start() * this->get_time_scaling();
}

double TimeSegment::get_time_end() const {
  return (double)this->get_segment_end() * this->get_time_scaling();
}

double TimeSegment::duration() {
  return this->get_time_end() - this->get_time_start();
}

void TimeSegment::set_time_start(double aTimeStart) {
  this->set_segment_start((long double)aTimeStart);
}

void TimeSegment::set_time_end(double aTimeEnd) {
  this->set_segment_end((long double)aTimeEnd);
}

std::string TimeSegment::to_string(int level __attribute__((unused))) const {
  std::stringstream ss;

  ss << get_time_start() << " - " << get_time_end() << " (s)";
  return std::string(ss.str().c_str());
}

void TimeSegment::deflate(RootsStream* stream, bool is_terminal,
                          int list_index) {
  Segment::deflate(stream, false, list_index);

  if (is_terminal) stream->close_object();
}

void TimeSegment::inflate(RootsStream* stream, bool is_terminal,
                          int list_index) {
  Segment::inflate(stream, false, list_index);
  if (is_terminal) stream->close_children();
}

TimeSegment* TimeSegment::inflate_object(RootsStream* stream, int list_index) {
  TimeSegment* t = new TimeSegment(true);
  t->inflate(stream, true, list_index);
  return t;
}
}
}  // END OF NAMESPACES
