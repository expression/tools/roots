/*
    This file is part of Roots.

    Roots is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Roots is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Roots.  If not, see <http://www.gnu.org/licenses/>.

    Copyright (c) 2013, 2014 IRISA
*/


#ifndef SIGNALSEGMENT_H_
#define SIGNALSEGMENT_H_

#include "../../common.h"
#include "../../Base.h"
#include "../../Acoustic/Segment.h"
#include "../../RootsException.h"
#include "../../File/Audio.h"

#include <numeric>
#include <sstream>

namespace roots
{
  namespace acoustic
  {

    /**
     * @brief Representation of a signal segment with signal samples
     * @details
     * This class is derived from Segment and provides additionnal access
     * to the signal samples for the segment. This class uses Sox library
     * to load and save files and detect the format of the signal. The signal
     * is loaded into memory in a unique format. See Sox library documentation
     * for more info.
     * @author Cordial Group
     * @version 0.1
     * @date 2011
     */
    class SignalSegment: public roots::acoustic::Segment, public roots::file::audio::Audio
      {
      public:
	
	/* Constructors does not use any default parameter bc swig is not efficient with */

	/**
	 * @brief Constructor
	 * @param aFilename
	 * @param aStartValue
	 * @param aEndValue
	 * @param aSamlingFrequency  [-1]
	 * @param aFormat            [roots::file::audio::FILE_FORMAT_AUDIO_UNDEF]
	 * @param aTimeScalingFactor [Segment::TIME_UNIT_FACTOR_METRIC]
	 */
	SignalSegment(const std::string& aFileName,
		      double aStartValue,
		      double aEndValue,
		      int aSamlingFrequency = -1,
		      roots::file::audio::file_format_t aFormat = roots::file::audio::FILE_FORMAT_AUDIO_UNDEF,
		      double aTimeScalingFactor = Segment::TIME_UNIT_FACTOR_METRIC);

	/**
	 * @brief Copy constructor
	 * @param seg
	 */
	SignalSegment(const SignalSegment& seg);

	/**
	 * @brief Destructor
	 * @todo check
	 */
	virtual ~SignalSegment();

	/**
	 * @brief clone the current object
	 * @return a copy of the object
	 */
	virtual SignalSegment *clone() const;

	/**
	 * @brief Copy segment features into the current object
	 * @details Links to relations or sequence are copied.
	 * @param seg
	 * @return reference to the current segment
	 */
	virtual SignalSegment& operator=(const SignalSegment& seg);

	const std::string& get_base_dir_name() const;

	void set_base_dir_name(const std::string& filename, bool updateSub=true);

	/**
	 * @brief Modifies the filename
	 * @param filename
	 */
	void set_file_name(const std::string& filename, bool updateSub=true);

	const std::string& get_file_name() const;

	/**
	 * @brief Prepare an item to be saved in a directory. Basicaly, a signal segment items will copy the associated wav file if target_base_dir change.
	 * @Warning items may be changed (for instance the base directory of signal segment items)!
	 */
	virtual void prepare_save(const std::string & target_base_dir);

	/**
	 * @brief Write the entity into a RootsStream
	 * @param stream the stream from which we inflate the element
	 * @param is_terminal indicates that the node is terminal (true by default)
	 */
	virtual void deflate(RootsStream * stream, bool is_terminal=true, int list_index=-1);

	/**
	 * @brief Extracts the entity from the stream
	 * @param stream the stream from which we inflate the element
	 */
	virtual void inflate(RootsStream * stream, bool is_terminal=true, int list_index=-1);

	/**
	 * @brief Extracts the entity from the stream
	 * @param stream the stream from which we inflate the element
	 */
	static SignalSegment * inflate_object(RootsStream * stream, int list_index=-1);

	/**
	 * @brief returns classname for current object
	 * @return string constant representing the classname
	 */
	virtual std::string get_classname() const { return classname(); };

	/**
	 * @brief returns classname for current class
	 * @return string constant representing the classname
	 */
	static std::string classname() { return "Acoustic::Segment::SignalSegment"; };

    /**
     * @brief returns a string representation of any Roots object
     * @param level the precision level of the output
     * @return string representation of the object
     */
	std::string to_string(int level) const;


      protected:
	void update_sub_filename();

      private:
	SignalSegment(const bool noInit = false);

	std::string	filename;		/**< filename loaded by the object */
	std::string	baseDirName;
      };

  }
} // END OF NAMESPACES

#endif /* SAMPLESEGMENT_H_ */
