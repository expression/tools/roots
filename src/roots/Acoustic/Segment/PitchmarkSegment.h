/*
    This file is part of Roots.

    Roots is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Roots is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Roots.  If not, see <http://www.gnu.org/licenses/>.

    Copyright (c) 2013, 2014 IRISA
*/


#ifndef PITCHMARKSEGMENT_H_
#define PITCHMARKSEGMENT_H_

#include "../../common.h"
#include "../../Base.h"
#include "../../Acoustic/Segment.h"
#include "../../RootsException.h"
#include "../../File/Pitchmark.h"

#include <numeric>
#include <sstream>

namespace roots
{
  namespace acoustic
  {
    /**
     * @brief Representation of a pitchmark segment
     * @details
     * This class is derived from Segment and provides additionnal access
     * to the pitchmark samples for the segment.
     * @author Cordial Group
     * @version 0.1
     * @date 2011
     */
    class PitchmarkSegment: public roots::acoustic::Segment, public roots::file::pitchmark::Pitchmark
      {
      public:

	/**
	 * @brief Constructor
	 * @param aFilename
	 * @param aStartValue
	 * @param aEndValue
	 * @param aSamlingFrequency
	 * @param aFormat
	 * @param aTimeScalingFactor
	 */
	PitchmarkSegment(const std::string& aFileName,
			 double aStartValue,
			 double aEndValue,
			 roots::file::pitchmark::file_format_t aFormat = roots::file::pitchmark::FILE_FORMAT_PITCHMARK_UNDEF,
			 double aTimeScalingFactor = Segment::TIME_UNIT_FACTOR_METRIC);

	/**
	 * @brief Copy constructor
	 * @param seg
	 */
	PitchmarkSegment(const PitchmarkSegment& seg);

	/**
	 * @brief Destructor
	 * @todo check
	 */
	virtual ~PitchmarkSegment();

	/**
	 * @brief clone the current object
	 * @return a copy of the object
	 */
	virtual PitchmarkSegment *clone() const;

	/**
	 * @brief Copy segment features into the current object
	 * @details Links to relations or sequence are copied.
	 * @param seg
	 * @return reference to the current segment
	 */
	virtual PitchmarkSegment& operator=(const PitchmarkSegment& seg);

	const std::string& get_base_dir_name() const;

	void set_base_dir_name(const std::string& filename);

	/**
	 * @brief Modifies the filename
	 * @param filename
	 */
	void set_file_name(const std::string& filename);

	const std::string& get_file_name() const;

	/**
	 * @brief Prepare an item to be saved in a directory. Basicaly, a signal segment items will copy the associated wav file if target_base_dir change.
	 * @Warning items may be changed (for instance the base directory of signal segment items)!
	 */
	virtual void prepare_save(const std::string & target_base_dir);

	/**
	 * @brief Write the entity into a RootsStream
	 * @param stream the stream from which we inflate the element
	 * @param is_terminal indicates that the node is terminal (true by default)
	 */
	virtual void deflate(RootsStream * stream, bool is_terminal=true, int list_index=-1);

	/**
	 * @brief Extracts the entity from the stream
	 * @param stream the stream from which we inflate the element
	 */
	virtual void inflate(RootsStream * stream, bool is_terminal=true, int list_index=-1);

	/**
	 * @brief Extracts the entity from the stream
	 * @param stream the stream from which we inflate the element
	 */
	static PitchmarkSegment * inflate_object(RootsStream * stream, int list_index=-1);

	/**
	 * @brief returns classname for current object
	 * @return string constant representing the classname
	 */
	virtual std::string get_classname() const { return classname(); };

	/**
	 * @brief returns classname for current class
	 * @return string constant representing the classname
	 */
	static std::string classname() { return "Acoustic::Segment::PitchmarkSegment"; };

      protected:
	void update_sub_filename();

      private:
	PitchmarkSegment(const bool noInit = false);

	std::string	filename;		/**< filename loaded by the object */
	std::string	baseDirName;
      };
  }
} // END OF NAMESPACES

#endif /* PITCHMARKSEGMENT_H_ */
