/*
    This file is part of Roots.

    Roots is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Roots is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Roots.  If not, see <http://www.gnu.org/licenses/>.

    Copyright (c) 2013, 2014 IRISA
*/


#include "PitchmarkSegment.h"
#include "../../File/Pitchmark.h"
#include <boost/filesystem.hpp>

namespace bfs = boost::filesystem;

namespace roots
{
  namespace acoustic
  {
    PitchmarkSegment::PitchmarkSegment(const bool noInit) :
      Segment(noInit),
      Pitchmark()
    {
      set_file_name("");
      set_base_dir_name("");
    }

    PitchmarkSegment::PitchmarkSegment(const std::string& aFileName,
				       double aStartSample,
				       double aEndSample,
				       roots::file::pitchmark::file_format_t aFormat,
				       double aTimeScalingFactor) :
      Segment(aStartSample,
	      aEndSample,
	      aTimeScalingFactor),
      Pitchmark()
    {
      set_file_name(aFileName);
      set_base_dir_name(std::string(""));
      
      set_file_format(aFormat);
    }

    PitchmarkSegment::PitchmarkSegment(const PitchmarkSegment& seg) :
      Segment(seg),
      Pitchmark(seg)
    {
      set_file_name(seg.get_file_name());
      set_base_dir_name(seg.get_base_dir_name());
    }

    PitchmarkSegment::~PitchmarkSegment()
    {
    }

    PitchmarkSegment *PitchmarkSegment::clone() const
    {
      return new PitchmarkSegment(*this);
    }

    PitchmarkSegment& PitchmarkSegment::operator=(const PitchmarkSegment& seg)
    {
      Segment::operator=(seg);
      Pitchmark::operator=(seg);

      set_file_name(seg.get_file_name());
      set_base_dir_name(seg.get_base_dir_name());

      return *this;
    }

    void PitchmarkSegment::update_sub_filename()
    {
      unload();
      /* Update file::pitchmark */
      bfs::path source_path(get_base_dir_name());
      source_path /= get_file_name();
      Pitchmark::set_file_name(source_path.string());
    }

    const std::string& PitchmarkSegment::get_base_dir_name() const
    {
      return baseDirName;
    }

    void PitchmarkSegment::set_base_dir_name(const std::string& name)
    {
      baseDirName = name;
      update_sub_filename();
    }

    const std::string& PitchmarkSegment::get_file_name() const
    {
      return filename;
    }

    void PitchmarkSegment::set_file_name(const std::string& filename)
    {
      this->filename = filename;
      update_sub_filename();
    }

    void PitchmarkSegment::prepare_save(const std::string & target_base_dir)
    {
      bfs::path source_path(get_base_dir_name());
      bfs::path target_path(target_base_dir);
#if BOOST_VERSION < 104800
      source_path = bfs::canonical(source_path);
      target_path = bfs::canonical(target_path);
#else
      // WARNING: to be checked
      source_path = source_path.normalize();
      target_path = target_path.normalize();
#endif
      // If difference between target basedir and current basedir
      if (source_path != target_path)
	{
	  // Copy target file
	  source_path /= this->get_file_name();
	  target_path /= this->get_file_name();

	  if (!boost::filesystem::exists(target_path.parent_path()))
	    { bfs::create_directories(target_path.parent_path()); }
	  bfs::copy_file(source_path, target_path, bfs::copy_option::overwrite_if_exists);

	  // Then, change basedir
	  set_base_dir_name(target_base_dir);
	}
      // Otherwise, nothing to do !
    }


    void PitchmarkSegment::deflate(RootsStream * stream, bool is_terminal, int list_index)
    {
      Segment::deflate(stream,false,list_index);

      stream->append_unicodestring_content("file_name", this->get_file_name());
      stream->append_int_content("file_format", this->get_file_format());
      
      if(is_terminal)
	stream->close_object();
    }

    void PitchmarkSegment::inflate(RootsStream * stream, bool is_terminal, int list_index)
    {
      Segment::inflate(stream,false,list_index);

      //stream->open_children();
      //this->set_sampling_frequency(stream->get_double_content("sampling_frequency"));
      this->set_file_name(stream->get_unicodestring_content("file_name"));

      int file_format = stream->get_uint_content("file_format");
      if(file::pitchmark::Pitchmark::fileFormatToString.find((file::pitchmark::file_format_t)file_format) != file::pitchmark::Pitchmark::fileFormatToString.end())
	{
	  this->set_file_format((file::pitchmark::file_format_t)file_format);
	}else{
	this->set_file_format(file::pitchmark::FILE_FORMAT_PITCHMARK_UNDEF);
      }

      // this->set_signal_file_name(stream->get_unicodestring_content("signal_file_name"));
      // this->set_n_values(stream->get_uint_content("n_values"));
      if(is_terminal) stream->close_children();

      this->set_base_dir_name(stream->get_base_dir_name());
    }

    PitchmarkSegment * PitchmarkSegment::inflate_object(RootsStream * stream, int list_index)
    {
      PitchmarkSegment *t = new PitchmarkSegment(true);
      t->inflate(stream,true,list_index);
      return t;
    }

  } } // END OF NAMESPACES
