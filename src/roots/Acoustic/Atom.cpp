/*
    This file is part of Roots.

    Roots is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Roots is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Roots.  If not, see <http://www.gnu.org/licenses/>.

    Copyright (c) 2013, 2014 IRISA
*/



#include "Atom.h"

namespace roots {
	namespace acoustic {

		const double Atom::ATOM_F0_FREQUENCY = 0.005;
		
		// Basic constructor
		Atom::Atom(const bool noInit):BaseItem(noInit)
		{
			set_atom_order(2); // Set default order as lowest possible: 2, as used by Fujusaki's model
			set_atom_theta(0.0);
			set_atom_amplitude(0.0);
			set_atom_temporal_position(0.0);
			set_atom_temporal_length(0.0);
			this->fs = 200; // Default
			this->type = "GammaAtom"; // Default type
		}

		// Partial constructor
		Atom::Atom(const double& pos,
				   const double& theta,
				   const double& amp,
				   const bool noInit) : roots::BaseItem(noInit)
		{
			set_atom_order(2); // Set default order as lowest possible: 2, as used by Fujusaki's model
			set_atom_theta(theta);
			set_atom_amplitude(amp);
			set_atom_temporal_position(pos);
			set_atom_temporal_length(0.0);
			this->fs = 200; // Default
			this->type = "GammaAtom"; // Default type
		}

		// Full object constructor
		Atom::Atom(const int& fs,
				   const int& k,
				   const double& pos,
				   const double& length,
				   const double& theta,
				   const double& amp,
				   const std::string& type,
				   const bool noInit) : roots::BaseItem(noInit)
		{
			set_atom_order(k);
			set_atom_theta(theta);
			set_atom_amplitude(amp);
			set_atom_temporal_position(pos);
			set_atom_temporal_length(length);
			this->fs = fs;
			this->type = type;
		}

		/**
		 * don't copy link to sequence or relation
		 *
		 */
		// Copy constructor
		Atom::Atom(const Atom &at) : roots::BaseItem(at)
		{
			set_atom_order(at.get_atom_order());
			set_atom_theta(at.get_atom_theta());
			set_atom_amplitude(at.get_atom_amplitude());
			set_atom_temporal_position(at.get_atom_temporal_position());
			set_atom_temporal_length(at.get_atom_temporal_length());
			this->fs = at.fs;
			this->type = at.type;
		}

		// Destructor
		Atom::~Atom()
		{}

		Atom *Atom::clone() const
		{
			return new Atom(*this);
		}

		Atom& Atom::operator=(const Atom &at)
		{
			BaseItem::operator=(at);
			set_atom_order(at.get_atom_order());
			set_atom_theta(at.get_atom_theta());
			set_atom_amplitude(at.get_atom_amplitude());
			set_atom_temporal_position(at.get_atom_temporal_position());
			set_atom_temporal_length(at.get_atom_temporal_length());
			this->fs = at.fs;
			this->type = at.type;
			return *this;
		}

		double Atom::get_atom_theta() const
		{
			return theta;
		}
		
		void Atom::set_atom_theta(double aTheta)
		{
			this->theta = aTheta;
		}

		double Atom::get_atom_amplitude() const
		{
			return amplitude;
		}

		void Atom::set_atom_amplitude(double anAmplitude)
		{
			this->amplitude = anAmplitude;
		}

		double Atom::get_atom_temporal_position() const
		{
			return position;
		}

		double Atom::get_atom_temporal_position_ms() const
		{
			return position*ATOM_F0_FREQUENCY*1000;
		}
		
		void Atom::set_atom_temporal_position(double aPosition)
		{
			this->position = aPosition;
		}

		double Atom::get_atom_temporal_length() const
		{
			return length;
		}

		double Atom::get_atom_temporal_length_ms() const
		{
			return length*ATOM_F0_FREQUENCY*1000;
		}
		
		void Atom::set_atom_temporal_length(double aLength)
		{
			this->length = aLength;
		}
		
		int Atom::get_atom_order() const
		{
			return k;
		}

		void Atom::set_atom_order(int anOrder)
		{
			this->k = anOrder;
		}

		std::string Atom::to_string(int level) const
		{
			std::stringstream ss;

            ss.setf(std::ios::fixed,std::ios::floatfield);
			ss.precision(3+level);
			
			ss << "{t=" << get_atom_theta() << ", p=" << get_atom_temporal_position_ms() << "(s), a=" << get_atom_amplitude() << "}";

			return std::string(ss.str());
		}
		
		void Atom::deflate (RootsStream * stream, bool is_terminal, int list_index)
		{
			BaseItem::deflate(stream,false,list_index);

			stream->append_int_content("fs", this->fs);
			stream->append_int_content("k", this->get_atom_order());
			stream->append_double_content("position", this->get_atom_temporal_position());
			stream->append_double_content("length", this->get_atom_temporal_length());
			stream->append_double_content("theta", this->get_atom_theta());
			stream->append_double_content("amplitude", this->get_atom_amplitude());
			stream->append_unicodestring_content("type", this->type);
			
			if(is_terminal)
			{
				stream->close_object();
			}
		}

		void Atom::inflate(RootsStream * stream, bool is_terminal, int list_index)
		{
			BaseItem::inflate(stream,false,list_index);

			this->fs = stream->get_int_content("fs");
			this->set_atom_order(stream->get_int_content("k"));
			this->set_atom_temporal_position(stream->get_double_content("position"));
			this->set_atom_temporal_length(stream->get_double_content("length"));
			this->set_atom_theta(stream->get_double_content("theta"));
			this->set_atom_amplitude(stream->get_double_content("amplitude"));
			this->type = stream->get_unicodestring_content("type");
			
			if(is_terminal) { stream->close_children(); }
		}

		Atom * Atom::inflate_object(RootsStream * stream, int list_index)
		{
			Atom *t = new Atom(true);
			t->inflate(stream,true,list_index);
			return t;
		}

}} // END OF NAMESPACES
