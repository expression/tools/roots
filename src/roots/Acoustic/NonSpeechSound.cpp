/*
    This file is part of Roots.

    Roots is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Roots is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Roots.  If not, see <http://www.gnu.org/licenses/>.

    Copyright (c) 2013, 2014 IRISA
*/


#include "NonSpeechSound.h"

namespace roots
{
  namespace acoustic
  {

    NonSpeechSound::NonSpeechSound(const bool noInit): BaseItem(noInit)
    {
      nsa = NULL;
    }

    NonSpeechSound::NonSpeechSound(const roots::phonology::nsa::Nsa &nsa)
    {
      this->nsa = nsa.clone();
    }

    NonSpeechSound::NonSpeechSound(const NonSpeechSound &nss): BaseItem(nss)
    {
      set_nsa(nss.get_nsa());
      set_label(nss.get_label());
    }

    NonSpeechSound::~NonSpeechSound()
    {
      if(nsa!=NULL) delete nsa;
    }

    NonSpeechSound *NonSpeechSound::clone() const
    {
      return new NonSpeechSound(*this);
    }

    NonSpeechSound& NonSpeechSound::operator=(const roots::acoustic::NonSpeechSound &nss)
    {
      BaseItem::operator=(nss);
      set_nsa(nss.get_nsa());
      set_label(nss.get_label());
      return *this;
    }

    const std::string& NonSpeechSound::get_label() const
    {
      if(nsa != NULL)
	return nsa->get_label();
      return BaseItem::get_label();
    }

    void NonSpeechSound::set_label(const std::string& alabel)
    {
      this->BaseItem::set_label(alabel);
      if(nsa != NULL)
	nsa->set_label(alabel);
    }

    std::string NonSpeechSound::to_string(int level) const
    {
        std::stringstream ss;

      switch (level)
	{
	case LEVEL_LABEL:
	  ss	<<  get_label();
	  break;
	case LEVEL_CLASS_FINE:
	  ss	<< nsa->to_string(LEVEL_CLASS_FINE);
	  break;
	case LEVEL_CLASS_COARSE:
	  ss	<< nsa->to_string(LEVEL_CLASS_COARSE);
	  break;
	default:
	  throw RootsException(__FILE__, __LINE__, "unknown value for 'level' argument!\n");

	}
      return std::string(ss.str().c_str());
    }

    void NonSpeechSound::deflate (RootsStream * stream, bool is_terminal, int list_index)
    {
      BaseItem::deflate(stream,false,list_index);

      nsa->deflate(stream);

      if(is_terminal)
	stream->close_object();
    }

    void NonSpeechSound::inflate(RootsStream * stream, bool is_terminal, int list_index)
    {
      BaseItem::inflate(stream,false,list_index);

      //stream->open_children();
      std::string nsaClassname = stream->get_object_classname_no_read(roots::phonology::nsa::Nsa::xml_tag_name());

      if(nsaClassname.compare(roots::phonology::nsa::Nsa::classname()) == 0)
	{
	  nsa = roots::phonology::nsa::Nsa::inflate_object(stream);
	} else if(nsaClassname.compare(roots::phonology::nsa::NsaCommon::classname()) == 0)
	{
	  nsa = roots::phonology::nsa::NsaCommon::inflate_object(stream);
	} else {
	std::stringstream ss;
	ss << nsaClassname << " nsa type is unknown!";
	throw RootsException(__FILE__, __LINE__, ss.str().c_str());
      }

      if(is_terminal) stream->close_children();
    }

    NonSpeechSound * NonSpeechSound::inflate_object(RootsStream * stream, int list_index)
    {
      NonSpeechSound *t = new NonSpeechSound(true);
      t->inflate(stream,true,list_index);
      return t;
    }

  } } // END OF NAMESPACES
