/*
    This file is part of Roots.

    Roots is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Roots is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Roots.  If not, see <http://www.gnu.org/licenses/>.

    Copyright (c) 2013, 2014 IRISA
*/


#include "Allophone.h"


roots::acoustic::Allophone::Allophone(const bool noInit) : Phoneme(noInit)
{}

roots::acoustic::Allophone::Allophone(const roots::phonology::ipa::Ipa& ipa, roots::phonology::ipa::Alphabet* alphabet) : Phoneme(ipa, alphabet)
{}

roots::acoustic::Allophone::Allophone(const roots::phonology::ipa::Ipa & ipa, const roots::phonology::ipa::Ipa & ipa2, roots::phonology::ipa::Alphabet* alphabet) : Phoneme(ipa, ipa2, alphabet)
{}

roots::acoustic::Allophone::Allophone ( const std::vector< roots::phonology::ipa::Ipa* >& ipas, roots::phonology::ipa::Alphabet* alphabet ) : Phoneme ( ipas, alphabet )
{}


/**
 * don't copy link to sequence or relation
 *
 */
roots::acoustic::Allophone::Allophone(const roots::acoustic::Allophone &all):
	Phoneme(static_cast<Phoneme>(all))
{
}

roots::acoustic::Allophone::~Allophone()
{
	//delete ipa; // not needed since ~Phoneme is called after
}

roots::acoustic::Allophone *roots::acoustic::Allophone::clone() const
{
	return new roots::acoustic::Allophone(*this);
}

roots::acoustic::Allophone& roots::acoustic::Allophone::operator=(const roots::acoustic::Allophone &all)
{
	this->Phoneme::operator=(all);
	return *this;
}

namespace roots { namespace acoustic {

		void Allophone::deflate (RootsStream * stream, bool is_terminal, int list_index)
		{
			Phoneme::deflate(stream,false,list_index);

			if(is_terminal)
				stream->close_object();
		}

		void Allophone::inflate(RootsStream * stream, bool is_terminal, int list_index)
		{
			Phoneme::inflate(stream,false,list_index);
			if(is_terminal) stream->close_children();
		}

		Allophone * Allophone::inflate_object(RootsStream * stream, int list_index)
		{
			Allophone *t = new Allophone(true);
			t->inflate(stream,true,list_index);
			return t;
		}

}} // END OF NAMESPACES
