/*
    This file is part of Roots.

    Roots is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Roots is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Roots.  If not, see <http://www.gnu.org/licenses/>.

    Copyright (c) 2013, 2014 IRISA
*/


#ifndef NONSPEECHSOUND_H_
#define NONSPEECHSOUND_H_

#include "../common.h"
#include "../BaseItem.h"
#include "../Phonology/Nsa.h"
#include "../Phonology/Nsa/NsaCommon.h"

namespace roots
{

namespace acoustic
{

static const int LEVEL_LABEL = 0;
static const int LEVEL_CLASS_FINE = 1;
static const int LEVEL_CLASS_COARSE = 2;

/**
 * @brief Representation of non-speech sound
 * @details 
 * Non-speech sounds are the part of signal which cannot be interpreted into a speech item (noise, ...)
 * @author Cordial Group
 * @version 0.1
 * @date 2011
 */
class NonSpeechSound:  public BaseItem
{
protected:
	/**
	 * @brief Default constructor
	 */
	NonSpeechSound(const bool noInit = false);
public:
	/**
	 * @brief Constructor
	 * @param nsa
	 */
	NonSpeechSound(const roots::phonology::nsa::Nsa &nsa);
	/**
	 * @brief Copy constructor
	 * @details This constructor does not copy links to relations or sequence.
	 * @param nss
	 */
	NonSpeechSound(const NonSpeechSound &nss);
	/**
	 * @brief Destructor
	 * @todo check
	 */
	virtual ~NonSpeechSound();
	/**
	 * @brief clone the current object
	 * @return a copy of the object
	 */
	virtual NonSpeechSound *clone() const;
	/**
	 * @brief Copy non-speech sound features into the current object
	 * @details Links to relations or sequence are copied.
	 * @param nss
	 * @return reference to the current non-speech sound
	 */
	NonSpeechSound& operator=(const NonSpeechSound &nss);
	/**
	 * @brief Returns the label of the non-speech sound
	 * @return the label
	 */
	virtual const std::string& get_label() const ;
	/**
	 * @brief Modifies the label of the non-speech sound
	 * @param alabel the new label
	 */
	virtual void set_label(const std::string& alabel);
	/**
	 * @brief Modifies the Nsa ref associated to this non-speech sound
	 * @param ns the new Nsa ref
	 */
	void set_nsa(const roots::phonology::nsa::Nsa &ns){ nsa = ns.clone();};
	/**
	 * @brief Returns reference to the Nsa object
	 * @return ref on Nsa
	 */
	const roots::phonology::nsa::Nsa &get_nsa() const {return *nsa;};

	virtual std::string to_string(int level=0) const;

	/**
	 * @brief Write the entity into a RootsStream
	 * @param stream the stream from which we inflate the element
	 * @param is_terminal indicates that the node is terminal (true by default)
	 * @param parentNode
	 */
	virtual void deflate (RootsStream * stream, bool is_terminal=true, int list_index=-1);
	/**
	 * @brief Extracts the entity from the stream
	 * @param stream the stream from which we inflate the element
	 */
	virtual void inflate(RootsStream * stream, bool is_terminal=true, int list_index=-1);
	/**
	 * @brief Extracts the entity from the stream
	 * @param stream the stream from which we inflate the element
	 */
	static NonSpeechSound * inflate_object(RootsStream * stream, int list_index=-1);
	/**
	 * @brief returns the XML tag name value for current object
	 * @return string constant representing the XML tag name 
	 */
	virtual std::string get_xml_tag_name() const { return xml_tag_name(); };
	/**
	 * @brief returns the XML tag name value for current class
	 * @return string constant representing the XML tag name 
	 */
	static std::string xml_tag_name() { return "nss"; };
	/**
	 * @brief returns classname for current object
	 * @return string constant representing the classname
	 */
	virtual std::string get_classname() const { return classname(); };
	/**
	 * @brief returns classname for current class
	 * @return string constant representing the classname
	 */
	static std::string classname() { return "Acoustic::NonSpeechSound"; };
	/**
	 * @brief returns display color for current object
	 * @return string constant representing the pgf display color
	 */
	virtual std::string get_pgf_display_color() const { return pgf_display_color();};
	/**
	 * @brief returns display color for current class
	 * @return string constant representing the pgf display color
	 */
	static std::string pgf_display_color() { return "yellow!25"; };
private:
	roots::phonology::nsa::Nsa *nsa; /**< The NSA object associated to the non speech sound */
};

} } // END OF NAMESPACES

#endif /* NONSPEECHSOUND_H_ */
