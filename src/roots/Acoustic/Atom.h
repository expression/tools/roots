/*
    This file is part of Roots.

    Roots is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Roots is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Roots.  If not, see <http://www.gnu.org/licenses/>.

    Copyright (c) 2013, 2014 IRISA
*/


#ifndef ATOM_H_
#define ATOM_H_

// #include "common.h"
#include "../BaseItem.h"


namespace roots {
	namespace acoustic {

		/**
		 * @brief Representation of an Atom
		 * @details
		 * An item \p atom is a component of the atom-based f0 contour decomposition. It is a spectral artefact generating f0 modification at some temporal point and on some period. It is represented by a gamma law which parameters are defined in instance variables. Law expression is the following: G[k,theta](t) = 1/(theta^k*gamma(k)) * t^(k-1) * e^(-t/theta) for t > 0
		 * @author Expression Group
		 * @version 0.1
		 * @date 2015
		 * @todo Nothing right now!
		 */
		class Atom: public roots::BaseItem
		{
		protected:
			/**
			 * @brief Default constructor
			 */
			Atom(const bool noInit = false);
		public:
			/**
			 * @brief Constructor
			 * @note ipa are copied
			 * @param ipa The IPA phonological description of the allophone
			 * @param alphabet used for the allophone
			 */
			Atom(const double& pos,
				 const double& theta,
				 const double& amp,
				 const bool noInit = false);
	
			/**
			 * @brief Constructor
			 * @note ipa are copied
			 * @param ipas The IPA phonological description of the allophone as a vector of IPA objects
			 * @param alphabet used for the allophone
			 */
			Atom(const int& fs,
				 const int& k,
				 const double& pos,
				 const double& length,
				 const double& theta,
				 const double& amp,
				 const std::string& type="GammaAtom",
				 const bool noInit = false);
	
			/**
			 * @brief Copy constructor
			 * @details This constructor does not copy links to relations or sequence.
			 * @param all
			 */
			Atom(const Atom &at);
			/**
			 * @brief Destructor
			 * @todo check
			 */
			virtual ~Atom();
			/**
			 * @brief clone the current object
			 * @return a copy of the object
			 */
			virtual Atom *clone() const;

			/**
			 * @brief Copy allophone features into the current allphone
			 * @details Links to relations or sequence are copied.
			 * @param all
			 * @return reference to the current allophone
			 */
			Atom& operator=(const Atom &at);
			/**
			 * @brief Write the entity into a RootsStream
			 * @param stream the stream from which we inflate the element
			 * @param is_terminal indicates that the node is terminal (true by default)
			 * @param parentNode
			 */
			virtual void deflate(RootsStream * stream, bool is_terminal=true, int list_index=-1);

			/**
			 * @brief Extracts the entity from the stream
			 * @param stream the stream from which we inflate the element
			 */
			virtual void inflate(RootsStream * stream, bool is_terminal=true, int list_index=-1);
			/**
			 * @brief Extracts the entity from the stream
			 * @param stream the stream from which we inflate the element
			 */
			static Atom * inflate_object(RootsStream * stream, int list_index=-1);
			/**
			 * @brief returns the XML tag name value for current object
			 * @return string constant representing the XML tag name
			 */
			virtual std::string get_xml_tag_name() const { return xml_tag_name(); };
			/**
			 * @brief returns the XML tag name value for current class
			 * @return string constant representing the XML tag name
			 */
			static std::string xml_tag_name() { return "atom"; };
			/**
			 * @brief returns classname for current object
			 * @return string constant representing the classname
			 */
			virtual std::string get_classname() const { return classname(); };
			/**
			 * @brief returns classname for current class
			 * @return string constant representing the classname
			 */
			static std::string classname() { return "Acoustic::Atom"; };
			/**
			 * @brief returns display color for current object
			 * @return string constant representing the pgf display color
			 */
			virtual std::string get_pgf_display_color() const { return pgf_display_color();};
			/**
			 * @brief returns display color for current class
			 * @return string constant representing the pgf display color
			 */
			static std::string pgf_display_color() { return "blue!25"; };

		public:

			/**
			 * @brief return atom theta from it's gamma law parameters
			 * @return Instance variable theta
			 */
			double get_atom_theta() const;

			/**
			 * @brief Modifies theta parameter for atom gamma law
			 * @param New theta parameter for atom gamma law
			 */
			void set_atom_theta(double aTheta);

			/**
			 * @brief return atom amplitude from it's gamma law parameters
			 * @return Instance variable amplitude
			 */
			double get_atom_amplitude() const;

			/**
			 * @brief Modifies atom amplitude parameter for atom gamma law
			 * @param New amplitude parameter for atom gamma law
			 */
			void set_atom_amplitude(double anAmplitude);	

			/**
			 * @brief return temporal position of the atom
			 * @return Instance variable pos
			 */
			double get_atom_temporal_position() const;

			/**
			 * @brief return temporal position in ms of the atom
			 * @return Instance variable pos
			 */
			double get_atom_temporal_position_ms() const;
			
			/**
			 * @brief Modifies temporal position in ms of the atom
			 * @param New temporal position for the atom
			 */
			void set_atom_temporal_position(double aPosition);

			/**
			 * @brief return temporal length of the atom
			 * @return Instance variable length
			 */
			double get_atom_temporal_length() const;

			/**
			 * @brief return temporal length in ms of the atom
			 * @return Instance variable length
			 */
			double get_atom_temporal_length_ms() const;
			
			/**
			 * @brief Modifies temporal length in ms of the atom
			 * @param New temporal length of the atom
			 */
			void set_atom_temporal_length(double aLength);

			/**
			 * @brief return k, the order of the gamma law associated with the atom (>= 2)
			 * @return Instance variable k
			 */
			int get_atom_order() const;

			/**
			 * @brief Modifies the order k of the gamma law associated with the atom (>= 2)
			 * @param New order k for atom gamma law
			 */
			void set_atom_order(int anOrder);

			/**
			 * @brief returns a string representation of an Atom
			 * @param level the precision level of the output
			 * @return string representation of the object
			 */
			virtual std::string to_string(int level=0) const;
			
		private:
			int fs; // No getter nor setter
			int k; // Order of the gamma law associated to the atom
			double position;
			double length;
			double theta;
			double amplitude;
			std::string type; // Atom type. Typically, an atom definded by a gamma function. // No getter nor setter
			static const double ATOM_F0_FREQUENCY;
		};

	} } // END OF NAMESPACES

#endif /* ATOM_H_ */
