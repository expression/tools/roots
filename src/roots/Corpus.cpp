/*
  This file is part of Roots.

  Roots is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Roots is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	See the
  GNU Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with Roots.  If not, see <http://www.gnu.org/licenses/>.

  Copyright (c) 2013, 2014 IRISA
*/


#include "FileChunk.h"
#include "Corpus.h"
#include <boost/filesystem.hpp>

namespace roots
{

  const int Corpus::DEFAULT_CACHE_SIZE = 1;

  /*************** Constructor / Destructor / Copy ********************/

  Corpus::Corpus(int sizeLoadBuffer) : Base()
  {
    chunks = std::vector<BaseChunk *>();
    utteranceToChunk = std::vector<int>();
    loadedBuffer = std::deque<int>();
    chunkOffset = std::vector<int>();
    this->sizeLoadBuffer = sizeLoadBuffer;
    modificationFlagUtt = false;
    modificationFlagChunks = false;
  }

  Corpus::Corpus(const std::string & fileName, int sizeLoadBuffer) : Base()
  {
    chunks = std::vector<BaseChunk *>();
    utteranceToChunk = std::vector<int>();
    loadedBuffer = std::deque<int>();
    chunkOffset = std::vector<int>();
    this->sizeLoadBuffer = sizeLoadBuffer;
    modificationFlagUtt = false;
    modificationFlagChunks = false;

    this->load(fileName);
  }

  Corpus::~Corpus()
  {
    /* Destroy all chunks */
    for(std::vector<BaseChunk *>::iterator it = chunks.begin();
	it != chunks.end();
	++it)
      { delete *it; }
  }

  Corpus::Corpus(const Corpus& corp) : Base(corp)
  {
    /* Destroy all chunks */
    for(std::vector<BaseChunk *>::iterator it = chunks.begin();
	it != chunks.end();
	++it)
      { delete *it; }

    chunks = corp.get_all_chunks_copy();
    utteranceToChunk = corp.utteranceToChunk;
    sizeLoadBuffer = corp.sizeLoadBuffer;
    chunkOffset = corp.chunkOffset;
    loadedBuffer = corp.loadedBuffer;
    modificationFlagUtt = corp.modificationFlagUtt;
    modificationFlagChunks = corp.modificationFlagChunks;
  }

  Corpus& Corpus::operator= (const Corpus& corp)
  {
    if(this != &corp)
      {
	for(std::vector<BaseChunk *>::iterator it = chunks.begin();
	    it != chunks.end();
	    ++it)
	  { delete *it; }


	Base::operator=(corp);
	chunks = corp.get_all_chunks_copy();
	utteranceToChunk = corp.utteranceToChunk;
	//	chunkNameToID = corp.chunkNameToID;
	sizeLoadBuffer = corp.sizeLoadBuffer;
	chunkOffset = corp.chunkOffset;
	loadedBuffer = corp.loadedBuffer;
	modificationFlagUtt = corp.modificationFlagUtt;
	modificationFlagChunks = corp.modificationFlagChunks;
      }

    return *this;
  }


  Corpus * Corpus::clone() const
  {
    return new Corpus(*this);
  }

  BaseChunk * Corpus::clone_to_basechunk() const
  {
    return new Corpus(*this);
  }


  /*************** Utterance managment ********************/
  /** Counting Utterance functions **/
  int Corpus::count_utterances() const
  {
    const_cast<Corpus *>(this)->update_counts(); // Dirty but usefull
    return utteranceToChunk.size();
  }

  int Corpus::count_utterances_in_chunk(const int chunkId) const
  {
    return chunks[chunkId]->count_utterances();
  }

  void Corpus::update_counts()
  {
    if(modificationFlagUtt)
      {
	utteranceToChunk = std::vector<int>();
	chunkOffset = std::vector<int>();
	for(unsigned int i = 0; i < chunks.size(); ++i)
	  {
	    int nbUtterance = chunks[i]->count_utterances();
	    chunkOffset.push_back(utteranceToChunk.size());
	    utteranceToChunk.insert(utteranceToChunk.end() ,nbUtterance, i);
	  }
	modificationFlagUtt = false;
      }
  }

  /** Get Utterance functions **/
  Utterance * Corpus::get_utterance(int index) // throw (RootsException)
  {
    ROOTS_LOGGER_DEBUG(std::string("Crp get utt: ") +
		       boost::lexical_cast<std::string>(index));

    update_counts();

    if(index < 0 or index>=count_utterances()) {
      std::stringstream ss;
      ss << "Corpus::get_utterance: index " << index << " out of bounds (0.." << count_utterances() << ")!";
      throw RootsException(__FILE__,
			   __LINE__,
			   ss.str().c_str());
    }

    int chunkIndex= utteranceToChunk[index];

    Utterance* utt = chunks[chunkIndex]->get_utterance(index - chunkOffset[chunkIndex]);
    if(!is_loaded_chunk(chunkIndex)) {
      may_be_loaded_chunk(chunkIndex);
    }
    return utt;
  }

  Utterance* Corpus::get_utterance(const std::vector<std::string> & layersToMerge,
				   int index)
  {
    update_counts();
    if(index < 0 or index>=count_utterances()){
	std::stringstream ss;
	ss << "Corpus::get_utterance: index " << index << " out of bounds (0.." << (count_utterances()-1) << ")!";
	throw RootsException(__FILE__,
			     __LINE__,
			     ss.str().c_str());
      }

    int chunkIndex= utteranceToChunk[index];

    Utterance* utt = chunks[chunkIndex]->get_utterance(layersToMerge,
						       index - chunkOffset[chunkIndex]);
    if(!is_loaded_chunk(chunkIndex)){
      may_be_loaded_chunk(chunkIndex);
    }

    return utt;
  }

  std::vector<Utterance *> Corpus::get_utterances(int index, unsigned int number)
  {
    update_counts();


    int maxId = std::min( (index + number), (unsigned int) count_utterances());
    unsigned int nbToProcess = maxId - index;
    if(index < 0 || maxId <= index)
      {
	std::stringstream ss;
	ss << "Corpus::get_utterance: index " << index << " -> "
	   << (maxId -1) << " (" << number << ") "
	   << " out of bounds (0.." << (count_utterances()-1) << ")!";
	throw RootsException(__FILE__,
			     __LINE__,
			     ss.str().c_str());
      }

    std::vector<Utterance *> res(nbToProcess);

#pragma omp parallel for
    for(int i = index; i < maxId; ++i)
      {
	Utterance * utt = NULL;

	if(!is_loaded_for_utterance(i))
	  {
	    //	    std::cerr << "I am waiting for utt "<< i<< std:: std::endl;
#pragma omp critical(Corpus_get_utterances_loading)
	    {
	      //	      std::cerr << "I have the lock for utt "<< i<< std::endl;
	      if(!is_loaded_for_utterance(i))
		{
		  // WARNING : I may have a bug bc of the cache : in this case, i need to mutex the unload!
		  utt = get_utterance(i);
		  //  std::cerr << "I will load for utt "<< i<< std:: std::endl;
		}
	    }
	  }
	if(utt == NULL)
	  {utt = get_utterance(i);}

	res[i-index] = utt;
	//#pragma omp critical(Corpus_get_utterances_setResult)
	//	{ res[i-index] = utt; }
      }
    return res;
  }

  /** Add Utterance functions **/

  void Corpus::add_utterance(const Utterance& utt, const std::string layerName)
  //  throw(RootsException)
  {
    add_utterance(utt, layerName, chunks.size() - 1);
  }

  void Corpus::add_utterance(const std::map<std::string, Utterance>& utt)
  //  throw(RootsException)
  {
    add_utterance(utt, chunks.size() - 1);
  }

  void Corpus::add_utterance(const Utterance& utt, const int chunkId)
  //  throw(RootsException)
  {
    add_utterance(utt, ANONYMOUS_LAYER, chunkId);
  }

  void Corpus::add_utterances(const std::vector<Utterance>& utt,
			      const std::string layerName)
  //  throw(RootsException)
  {
    add_utterances(utt, layerName, chunks.size() - 1);
  }

  void Corpus::add_utterances(const std::map<std::string, std::vector<Utterance> >& utt)
  //  throw(RootsException)
  {
    add_utterances(utt, chunks.size() - 1);
  }

  void Corpus::add_utterances(const std::vector<Utterance>& utt,
			      const int chunkId)
  //  throw(RootsException)
  {
    add_utterances(utt, ANONYMOUS_LAYER, chunkId);
  }


  void Corpus::add_utterance(const Utterance& utt,
			     const std::string layerLabel,
			     const int chunkId)
  //  throw (RootsException)
  {
    int currentId = chunkId;
    /* Without chunks, create an empty memory chunk */
    if(chunks.empty())
      {
	new_memory_chunk();
	currentId = 0;
      }

    may_be_loaded_chunk(currentId);

    chunks[currentId]->add_utterance(utt, layerLabel);

    modificationFlagUtt = true;
    modificationFlagChunks = true;
  }


  void Corpus::add_utterance(const std::map<std::string, Utterance>& utt,
			     const int chunkId)
  //  throw (RootsException)
  {
    int currentId = chunkId;
    /* Without chunks, create an empty memory chunk */
    if(chunks.empty())
      {
	new_memory_chunk();
	currentId = 0;
      }

    /* Add in the last chunk */
    may_be_loaded_chunk(currentId);
    chunks[currentId]->add_utterance(utt);

    modificationFlagUtt = true;
    modificationFlagChunks = true;
  }

  void Corpus::add_utterances(const std::vector<Utterance>& utt,
			      const std::string layerName,
			      const int chunkId)
  //  throw(RootsException)
  {
    int currentId = chunkId;
    /* Without chunks, create an empty memory chunk */
    if(chunks.empty())
      {
	new_memory_chunk();
	currentId = 0;
      }

    may_be_loaded_chunk(currentId);
    chunks[currentId]->add_utterances(utt, layerName);

    modificationFlagUtt = true;
    modificationFlagChunks = true;
  }

  void Corpus::add_utterances(const std::map<std::string, std::vector<Utterance> >& utt,
			      const int chunkId)
  //  throw(RootsException)
  {
    int currentId = chunkId;
    /* Without chunks, create an empty memory chunk */
    if(chunks.empty())
      {
	new_memory_chunk();
	currentId = 0;
      }

    /* Add in the last chunk */
    may_be_loaded_chunk(currentId);
    chunks[currentId]->add_utterances(utt);

    modificationFlagUtt = true;
    modificationFlagChunks = true;
  }

  /** Update Utterance functions **/
  void Corpus::update_utterance(int id, const Utterance& utt)
  {
    int chunkId = utteranceToChunk[id];
    may_be_loaded_chunk(chunkId);
    chunks[chunkId]->update_utterance(id - chunkOffset[chunkId], utt);
    modificationFlagUtt = true;
    modificationFlagChunks = true;
  }

  void Corpus::update_utterance(int id, const std::map<std::string, Utterance> &utt)
  {
    int chunkId = utteranceToChunk[id];
    may_be_loaded_chunk(chunkId);
    chunks[chunkId]->update_utterance(id - chunkOffset[chunkId], utt);
    modificationFlagUtt = true;
    modificationFlagChunks = true;
  }

  /** Delete Utterance functions **/
  void Corpus::delete_utterance(int id)
  {
    int chunkId = utteranceToChunk[id];
    may_be_loaded_chunk(chunkId);
    chunks[chunkId]->delete_utterance(id - chunkOffset[chunkId]);
    modificationFlagUtt = true;
    modificationFlagChunks = true;
    if (count_utterances() == 0) {
      clear();
    }
  }

  void Corpus::clear()
  {
    /* Destroy all chunks */
    for(std::vector<BaseChunk *>::iterator it = chunks.begin();
	it != chunks.end();
	++it)
      { delete *it; }

    chunks.clear();
    utteranceToChunk.clear();
    chunkOffset.clear();
    loadedBuffer.clear();
    modificationFlagUtt = true;
    modificationFlagChunks = true;
  }

  /*************** Chunks managment ********************/
  int Corpus::new_memory_chunk()
  {
    chunks.push_back(new MemoryChunk());
    chunkOffset.push_back(utteranceToChunk.size());
    modificationFlagChunks = true;
    return chunks.size() -1;
  }

  int Corpus::new_file_chunk(const std::string & file,
			     const std::string corpus_abs_dir,
			     const int nbUtterances)
  {
    chunks.push_back(new FileChunk(file, corpus_abs_dir, nbUtterances));
    int bcIndex = chunks.size() -1;

    chunkOffset.push_back(utteranceToChunk.size());

    for(int i = 0; i < nbUtterances; ++i)
      { utteranceToChunk.push_back(bcIndex); }
    if (nbUtterances > 0) {
      modificationFlagUtt = true;
    }
    modificationFlagChunks = true;
    return bcIndex;
  }

  int Corpus::new_file_chunk(const std::map<std::string, std::string>& layersToFiles,
			     const std::string corpus_abs_dir,
			     const int nbUtterances)
  {
    int nbU = nbUtterances;
    chunks.push_back(new FileChunk(layersToFiles, corpus_abs_dir, nbUtterances));
    int bcIndex = chunks.size() -1;

    chunkOffset.push_back(utteranceToChunk.size());

    if(nbU == 0)
      {
	chunks.back()->update_counts();
	nbU = chunks.back()->count_utterances();
      }
    for(int i = 0; i < nbU; ++i)
      { utteranceToChunk.push_back(bcIndex); }
    if (nbU > 0) {
      modificationFlagUtt = true;
    }
    modificationFlagChunks = true;
    return bcIndex;
  }

  int Corpus::add_chunk(BaseChunk& bc)
  {
    bc.update_counts();
    return add_chunk(bc, bc.count_utterances());
  }

  int Corpus::add_chunk(BaseChunk& bc, int nbUtterance)
  {
    chunks.push_back(bc.clone_to_basechunk());
    int bcIndex = chunks.size() -1;

    chunkOffset.push_back(utteranceToChunk.size());

    for(int i = 0; i < nbUtterance; ++i)
      { utteranceToChunk.push_back(bcIndex); }
    if (nbUtterance > 0) {
      modificationFlagUtt = true;
    }
    modificationFlagChunks = true;
    return bcIndex;
  }

  void Corpus::set_chunks(const std::vector<BaseChunk *>& chks)
  {
    chunks = chks;

    modificationFlagUtt = true;
    modificationFlagChunks = true;
    update_counts();
  }

  BaseChunk * Corpus::get_chunk(int chunkId) // throw (RootsException)
  {
    return chunks[chunkId];
  }

  const std::vector<BaseChunk *>& Corpus::get_all_chunks() const
  {
    return chunks;
  }

  std::vector<BaseChunk *> Corpus::get_all_chunks_copy() const
  {
    std::vector<BaseChunk *> res;
    for(std::vector<BaseChunk *>::const_iterator it = chunks.begin();
	it !=chunks.end();
	++it)
      { res.push_back((*it)->clone_to_basechunk()); }
    return res;
  }

  int Corpus::get_chunk_id(int utt_id)
  {
    return utteranceToChunk[utt_id];
  }

  int Corpus::count_chunks() const
  {
    return chunks.size();
  }

  //   bool Corpus::prepare_file_chunk_for_backup(int index)
  //   {
  //	 if (index >= 0 && count_chunks() > 0) {
  //	   FileChunk *fc = get_chunk(index);
  //	   if (fc->get_classname() == FileChunk::classname() && !fc->ready_for_backup()) {
  //	fc->prepare_backup();
  //	   }
  //	   else {
  //	return false;
  //	   }
  //	 }
  //	 else {
  //	   return false;
  //	 }
  //   }

  void Corpus::delete_chunk(int id)
  {
    // Update the correspondance between utterance ID and chunk ID
    int first_utt_id = chunkOffset[id];
    int n_utt = this->get_chunk(id)->count_utterances();
    utteranceToChunk.erase(utteranceToChunk.begin()+first_utt_id, utteranceToChunk.begin()+first_utt_id+n_utt);
    // Remove the offset of the first utterance for the chunk being removed
    chunkOffset.erase(chunkOffset.begin()+id);
    // Remove the ID of the chunk from the list of loaded chunks
    std::deque<int>::iterator it;
    bool found = false;
    for (it = loadedBuffer.begin(); !found && it != loadedBuffer.end(); ++it)
      {
	if (*it == id)
	  {
	    found = true;
	    loadedBuffer.erase(it);
	  }
      }
    // Remove all utterances in the chunk
    this->get_chunk(id)->clear();
    // Remove the chunk from the list of chunks
    chunks.erase(chunks.begin()+id);
    // Warn the corpus about those modifications
    modificationFlagUtt = true;
    modificationFlagChunks = true;
  }

  /*************** Layers management ********************/
  int Corpus::count_layers() const
  {
    if(chunks.empty())
      {return 0;}

    int chunkIndex = 0;
    const_cast<Corpus *>(this)->may_be_loaded_chunk(chunkIndex);// Dirty but usefull
    return chunks[chunkIndex]->count_layers();
  }

  std::vector<std::string> Corpus::get_all_layer_names() const
  {
    if(chunks.empty())
      {return std::vector<std::string>();}

    int chunkIndex = 0;
    const_cast<Corpus *>(this)->may_be_loaded_chunk(chunkIndex);// Dirty but usefull
    return chunks[chunkIndex]->get_all_layer_names();
  }

  /*************** Loading/Save functions ********************/

  bool Corpus::filechunk_name_exists ( const std::string& name )
  {
    for (std::vector<BaseChunk*>::iterator bc_it = chunks.begin(); bc_it != chunks.end(); ++bc_it) {
      if ((*bc_it)->get_classname() == FileChunk::classname()) {
	std::map<std::string, std::string> *fns = ((FileChunk *) *bc_it)->get_filenames();
	for (std::map<std::string, std::string>::iterator fn_it = fns->begin(); fn_it != fns->end(); ++fn_it) {
	  if (fn_it->second == name) {
	    delete fns;
	    return true;
	  }
	}
	delete fns;
      }
    }
    return false;
  }


  void Corpus::load(const std::string & filename)
  {
    roots::RootsStream * ptRootsStreamLoader = NULL;
    if(endsWith(filename,".json"))
      {
	ptRootsStreamLoader = new RootsStreamJson();
      }
#ifdef USE_STREAM_XML
    else if(endsWith(filename,".xml"))
      {
	ptRootsStreamLoader = new RootsStreamXML();
      }
#endif
    else
      {
	std::stringstream ss;
	ss << "Corpus::load : unknown file extension for file <" << filename << ">!";
	throw RootsException(__FILE__, __LINE__, ss.str().c_str());
      }
    ptRootsStreamLoader->load(filename);
    this->inflate(ptRootsStreamLoader);
    delete ptRootsStreamLoader;
  }


  void Corpus::save(const std::string & filename)
  {
    // Deep save?
    std::stringstream ss;
    ss << filename;
    boost::filesystem::path target_dirname(ss.str().c_str());
    target_dirname.remove_filename();
    if (target_dirname == "") { target_dirname = "."; }

    // Check if target dirname must be created
    if (!boost::filesystem::exists(target_dirname)) {
      boost::filesystem::create_directories(target_dirname);
    }

    // For each chunk
    int n_chunks = count_chunks();
    for (int i = 0; i < n_chunks; i++) {
      // Save the file chunk
      if (chunks[i]->get_classname() == FileChunk::classname()) {
	((FileChunk *) chunks[i])->save(std::string(target_dirname.c_str()));
      }
      // Save related files of memory chunks
      else if(chunks[i]->get_classname() == MemoryChunk::classname()) {
	((MemoryChunk *) chunks[i])->prepare_save(std::string(target_dirname.c_str()));
      }
    }

    // Shallow save
    roots::RootsStream * ptRootsStreamLoader = NULL;
    if(endsWith(filename,".json"))
      {
	ptRootsStreamLoader = new RootsStreamJson();
      }
#ifdef USE_STREAM_XML
    else if(endsWith(filename,".xml"))
      {
	ptRootsStreamLoader = new RootsStreamXML();
      }
#endif
    else
      {
	std::stringstream ss;
	ss << "Corpus::load : unknown file extension for file <" << filename << ">!";
	throw RootsException(__FILE__, __LINE__, ss.str().c_str());
      }
    this->deflate(ptRootsStreamLoader);
    ptRootsStreamLoader->save(filename);
    delete ptRootsStreamLoader;
    modificationFlagChunks = false;
  }


  void Corpus::save(const std::string & filename, int chunk_size_limit, const std::string & filechunk_relative_dir)
  {
    if (chunk_size_limit > 0) {
      std::stringstream ss;
      ss << filename;
      boost::filesystem::path corpus_abs_path(ss.str().c_str()); // Directory where file chunks will be saved
      std::string subchunk_name = "";
      std::string subchunk_ext = "";
      boost::filesystem::path corpus_dir_name;
      boost::filesystem::path subchunk_dir_name;
      int subchunk_id = 0;

      // Prepare the name of FileChunks to be generated
      subchunk_ext = corpus_abs_path.extension().string();
      subchunk_name = corpus_abs_path.filename().stem().string();
      corpus_dir_name = corpus_abs_path.parent_path();
      // Check if corpus dirname must be created
      if (!boost::filesystem::exists(corpus_dir_name)) {
	boost::filesystem::create_directories(corpus_dir_name);
      }
      subchunk_dir_name = corpus_dir_name;
      subchunk_dir_name /= boost::filesystem::path(filechunk_relative_dir);
      // Check if dirname of file chunks must be created
      if (!boost::filesystem::exists(subchunk_dir_name)) {
	boost::filesystem::create_directories(subchunk_dir_name);
      }

      int n_chk = this->count_chunks();
      BaseChunk *bc = NULL;
      for (int i_chk = 0; i_chk < n_chk; i_chk++) {
	bc = this->get_chunk(i_chk);
	if (bc->get_classname() == MemoryChunk::classname()) {

	  int n_utt = bc->count_utterances();
	  int i_utt = 0;
	  while (i_utt < n_utt) {
	    Corpus *subchunk = new Corpus();
	    int subchunk_size;
	    for (subchunk_size = 0; subchunk_size < chunk_size_limit && i_utt < n_utt; subchunk_size++) {
	      Utterance *utt_copy = bc->get_utterance(i_utt);
	      subchunk->add_utterance(*utt_copy);
	      i_utt++;
	      utt_copy->destroy();
	    }
	    // Save the subchunk
	    boost::filesystem::path subchunk_abs_path = subchunk_dir_name;
	    boost::filesystem::path subchunk_rel_path(filechunk_relative_dir);
	    std::stringstream subchunk_filename;
	    subchunk_filename << subchunk_name << "." << subchunk_id << subchunk_ext;
	    subchunk_abs_path /= boost::filesystem::path(subchunk_filename.str());
	    subchunk_rel_path /= boost::filesystem::path(subchunk_filename.str());
	    while (this->filechunk_name_exists(subchunk_rel_path.string())) {
	      subchunk_id++;
	      subchunk_filename.str("");
	      subchunk_filename << subchunk_name << "." << subchunk_id << subchunk_ext;
	      subchunk_abs_path = subchunk_dir_name / boost::filesystem::path(subchunk_filename.str());
	      subchunk_rel_path = filechunk_relative_dir / boost::filesystem::path(subchunk_filename.str());
	    }
	    subchunk->save(subchunk_abs_path.string());
	    subchunk_id++;
	    subchunk->destroy();
	    // Reference in the current corpys the file where the subchunk has just been stored
	    this->new_file_chunk(subchunk_rel_path.string(), corpus_dir_name.string(), subchunk_size);
	  }
	  // Remove the current memory chunk
	  this->delete_chunk(i_chk);
	  i_chk--;
	  n_chk--;

	}
      }
    }
    else {
      std::stringstream ss;
      ss << "Warning in Corpus::save(): Corpus cannot be sliced if argument chunk_size_limit < 0. Current value is " << chunk_size_limit << ". Saving without chunking" <<  std::endl;
      ROOTS_LOGGER_DEBUG(ss.str());
    }

    this->save(filename);

  }


  void Corpus::change_alternate_temp_dir(const std::string & new_alt_dir)
  {
    int n = count_chunks();
    for (int i = 0; i < n; i++) {
      if (chunks[i]->get_classname() == FileChunk::classname()) {
	((FileChunk*) chunks[i])->change_alternate_temp_dir(new_alt_dir);
      }
    }
  }


  bool Corpus::unsaved_changes() const
  {
    return modificationFlagChunks;
  }


  void Corpus::deflate(RootsStream * stream, bool is_terminal, int list_index)
  {
    update_counts();
    Base::deflate(stream,false,list_index);

    stream->append_uint_content("n_chunks", (unsigned int) chunks.size());

    int nbElem = chunks.size();
    for (int index = 0; index < nbElem; ++index)
      {	chunks[index]->deflate(stream, true, index); }

    stream->append_vector_int_content("chunk_offset", this->chunkOffset);

    stream->append_vector_int_content("utterance_to_chunk", this->utteranceToChunk);

    if(is_terminal)
      stream->close_object();
  }


  void Corpus::inflate(RootsStream * stream, bool is_terminal, int list_index)
  {
    clear();

    update_counts();
    unsigned int length = 0;
    Base::inflate(stream,false,list_index);

    //stream->open_children();
    length = stream->get_uint_content("n_chunks");

    for(unsigned int index=0; index <length; ++index)
      {
	BaseChunk * bc = NULL;
	std::string bcClassName = stream->get_object_classname_no_read(BaseChunk::xml_tag_name(), index);

	if(bcClassName.compare(MemoryChunk::classname()) == 0)
	  { bc=new MemoryChunk();}
	else if(bcClassName.compare(FileChunk::classname()) == 0)
	  { bc=new FileChunk();}

	bc->inflate(stream,true,index);
	this->chunks.push_back(bc);
	stream->next_child();
      }

    chunkOffset = stream->get_vector_int_content("chunk_offset");
    utteranceToChunk = stream->get_vector_int_content("utterance_to_chunk");

    if(is_terminal) stream->close_children();

  }

  /*************** Chunks caching functions ********************/
  void Corpus::load_all()
  {
    for(std::vector<BaseChunk *>::iterator it = chunks.begin();
	it !=chunks.end();
	++it)
      { (*it)->load_all(); }
  }

  bool Corpus::is_loaded_for_utterance(const int utt_id)
  {
    int chunkIndex= get_chunk_id(utt_id);
    return (chunks.at(chunkIndex)->is_loaded_for_utterance(utt_id -
							   chunkOffset[chunkIndex]));
  }

  void Corpus::unload_all()
  {
    for(std::vector<BaseChunk *>::iterator it = chunks.begin();
	it !=chunks.end();
	++it)
      { (*it)->unload_all(); }
  }

  void Corpus::load_chunk(const int chunkId)
  {
    chunks.at(chunkId)->load_all();
  }

  void Corpus::unload_chunk(const int chunkId)
  {
    chunks.at(chunkId)->unload_all();
  }

  bool Corpus::is_loaded_chunk(const int chunkId)
  {
    bool res = false;
    res = std::find(loadedBuffer.begin(), loadedBuffer.end(), chunkId) !=
      loadedBuffer.end();

    return res;
  }

  void Corpus::may_be_loaded_chunk(const int chunkId)
  {
    if(sizeLoadBuffer > 0)
      {
	// Loading Cache
	if(std::find(loadedBuffer.begin(), loadedBuffer.end(), chunkId) ==
	   loadedBuffer.end())
	  {
	    loadedBuffer.push_back(chunkId);
	    while(loadedBuffer.size() > sizeLoadBuffer)
	      {
		chunks[loadedBuffer.front()]->unload_all();
		loadedBuffer.pop_front();
	      }
	  }
      }
  }

  /*************** Internals functions ********************/





} // END OF NAMESPACE
