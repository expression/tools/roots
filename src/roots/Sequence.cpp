/*
    This file is part of Roots.

    Roots is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Roots is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Roots.  If not, see <http://www.gnu.org/licenses/>.

    Copyright (c) 2013, 2014 IRISA
*/

#include "Sequence.h"
#include "Align/Alignment.h"
#include "BaseItem.h"
#include "Relation.h"
#include "Utterance.h"

namespace roots {
namespace sequence {

Sequence::Sequence(bool noInit)
    : Base(noInit),
      roots::RelationLinkedObject(),
      roots::UtteranceLinkedObject() {
  set_is_simple_item(true);
}

Sequence::Sequence(const std::string& name, bool noInit)
    : Base(name, noInit),
      roots::RelationLinkedObject(),
      roots::UtteranceLinkedObject() {
  set_is_simple_item(true);
}

Sequence::Sequence(const Sequence& s)
    : Base(s),
      roots::RelationLinkedObject(),  // No link copy
      roots::UtteranceLinkedObject() {
  is_simple_item = s.is_simple_item;
}

Sequence::~Sequence() {}

void Sequence::destroy() {
  if (!in_utterance()) {
    delete this;
  }
}

void Sequence::set_label(const std::string& alabel) {
  if (get_label() != alabel) {
    std::string old_label(get_label());
    Base::set_label(alabel);
    // Update the utterance if part of an utterance
    if (in_utterance()) {
      std::vector<Utterance*> all_utts = get_all_utterances();
      for (std::vector<Utterance*>::iterator it = all_utts.begin();
           it != all_utts.end(); ++it) {
        (*it)->rename_sequence(this, alabel);
      }
    }
    // Update relations if involved in relations
    std::vector<Relation*> all_relations = get_all_relations();
    for (std::vector<Relation*>::iterator it = all_relations.begin();
         it != all_relations.end(); ++it) {
      // Source
      if ((*it)->get_source_sequence() == this) {
        (*it)->set_source_label(alabel);
      }
      // Or target
      else {
        (*it)->set_target_label(alabel);
      }
    }
  }
}

bool Sequence::get_is_simple_item() const { return is_simple_item; }

void Sequence::set_is_simple_item(bool b) { is_simple_item = b; }

bool Sequence::has_related_sequence() const { return false; }

Relation* Sequence::get_embedded_relation() { return NULL; }

Sequence* Sequence::get_related_sequence() const { return NULL; }

std::vector<BaseItem*> Sequence::get_related_items(
    unsigned int index, const std::string& targetLabel) const {
  // If including sequence is not in an utterance
  if (!in_utterance()) {
    // Then, just check for direct links (no transitivity)
    return get_linked_items(index, targetLabel);
  } else {
    // Otherwise, ask to each utterance
    return get_related_utterance_items(index, targetLabel);
  }
}

std::vector<BaseItem*> Sequence::get_linked_items(
    unsigned int index, const std::string& targetLabel) const {
  std::vector<Relation*> rels = get_all_relations();
  for (std::vector<Relation*>::const_iterator it = rels.begin();
       it != rels.end(); ++it) {
    if ((*it)->get_source_label() == targetLabel) {
      return (*it)->get_related_items(index, true);
    } else if ((*it)->get_target_label() == targetLabel) {
      return (*it)->get_related_items(index, false);
    }
  }
  // Return an empty vector otherwise
  return std::vector<BaseItem*>();
}

std::vector<BaseItem*> Sequence::get_related_utterance_items(
    unsigned int index, const std::string& targetLabel) const {
  std::vector<Utterance*> utts = get_all_utterances();
  for (std::vector<Utterance*>::const_iterator it = utts.begin();
       it != utts.end(); ++it) {
    // Return the corresponding items from the fist utterance where the target
    // sequence is found
    if ((*it)->is_valid_sequence(targetLabel)) {
      return (*it)->get_related_items(index, get_label(), targetLabel);
    }
  }
  // Return an empty vector if the target sequence has not been found
  return std::vector<BaseItem*>();
}

std::vector<int> Sequence::get_related_indices(
    unsigned int index, const std::string& targetLabel) const {
  // If including sequence is not in an utterance
  if (!in_utterance()) {
    // Then, just check for direct links (no transitivity)
    return get_linked_indices(index, targetLabel);
  } else {
    // Otherwise, ask to each utterance
    return get_related_utterance_indices(index, targetLabel);
  }
}

std::vector<int> Sequence::get_linked_indices(
    unsigned int index, const std::string& targetLabel) const {
  std::vector<Relation*> rels = get_all_relations();
  for (std::vector<Relation*>::const_iterator it = rels.begin();
       it != rels.end(); ++it) {
    if ((*it)->get_source_label() == targetLabel) {
      return (*it)->get_related_indices(index, true);
    } else if ((*it)->get_target_label() == targetLabel) {
      return (*it)->get_related_indices(index, false);
    }
  }
  // Return an empty vector otherwise
  return std::vector<int>();
}

std::vector<int> Sequence::get_related_utterance_indices(
    unsigned int index, const std::string& targetLabel) const {
  std::vector<Utterance*> utts = get_all_utterances();
  for (std::vector<Utterance*>::const_iterator it = utts.begin();
       it != utts.end(); ++it) {
    // Return the corresponding items from the fist utterance where the target
    // sequence is found
    if ((*it)->is_valid_sequence(targetLabel)) {
      return (*it)->get_related_indices(index, get_label(), targetLabel);
    }
  }
  // Return an empty vector if the target sequence has not been found
  return std::vector<int>();
}

int Sequence::get_pgf_height() const {
  int height = 1;

  for (unsigned int index = 0; index < this->count(); index++) {
    int itemHeight = this->get_item(index)->get_pgf_height();
    if (itemHeight > height) height = itemHeight;
  }
  return height;
}

std::string Sequence::to_string(int level) const {
  std::stringstream ss;
  int indexItem = 0;
  int nItem = this->count();

  for (indexItem = 0; indexItem < nItem; indexItem++) {
    ss << this->get_item(indexItem)->to_string(level);
    if (indexItem < nItem - 1) {
      ss << " ";
    }
  }

  return std::string(ss.str().c_str());
}

std::ostream& operator<<(std::ostream& out, sequence::Sequence& seq) {
  out << "Sequence:" << std::endl;
  out << "\tlabel: " << seq.get_label() << std::endl;
  out << "\tclassname: " << seq.get_classname() << std::endl;
  out << "\tcontent: " << std::endl;
  for (unsigned int i = 0; i < seq.count(); i++) {
    out << ((seq).get_item(i)->to_string()) << std::endl;
  }
  return out;
}

std::string Sequence::get_xml_tag_name() const { return xml_tag_name(); }

std::string Sequence::xml_tag_name() { return "sequence"; }

roots::Matrix* Sequence::make_mapping(
    roots::sequence::Sequence* target_sequence) {
  Matrix* mapping = NULL;

  std::vector<Base *> src_subitem_seq, tgt_subitem_seq;
  std::vector<Base*>* tmp_subitems;
  std::vector<int> src_subitem_origin, tgt_subitem_origin;
  std::vector<int> src_mapping, tgt_mapping;

  // Avoid useless computations and allocations
  mapping = new SparseMatrix(this->count(), target_sequence->count());
  if (this->count() == 0 || target_sequence->count() == 0) {
    return mapping;
  }

  // Build vector of subitem from this sequence for both sequences
  // and remember the original item of each subitem
  size_t src_n = this->count();
  for (size_t i = 0; i < src_n; i++) {
    tmp_subitems = this->get_item(i)->split();
    size_t m = tmp_subitems->size();
    for (size_t j = 0; j < m; j++) {
      src_subitem_seq.push_back(tmp_subitems->at(j));
      src_subitem_origin.push_back(i);
    }
    delete tmp_subitems;
  }

  size_t tgt_n = target_sequence->count();
  for (size_t i = 0; i < tgt_n; i++) {
    tmp_subitems = target_sequence->get_item(i)->split();
    size_t m = tmp_subitems->size();
    for (size_t j = 0; j < m; j++) {
      tgt_subitem_seq.push_back(tmp_subitems->at(j));
      tgt_subitem_origin.push_back(i);
    }
    delete tmp_subitems;
  }

  // Align subitems
  roots::align::Alignment aligner(src_subitem_seq, tgt_subitem_seq);
  aligner.align();

  src_mapping = aligner.get_source_mapping();
  tgt_mapping = aligner.get_target_mapping();

  if (src_mapping.size() != tgt_mapping.size()) {
    std::stringstream ss;
    ss << "Sequence::make_mapping: Source and target mappings are not the same "
          "size!\n";
    throw RootsException(__FILE__, __LINE__, ss.str().c_str());
  }

  // Rebuild the alignment of items based on the alignment of subitems
  for (size_t i = 0; i < src_mapping.size(); i++) {
    if (src_mapping[i] >= 0 && tgt_mapping[i] >= 0) {
      mapping->set_element(src_subitem_origin[src_mapping[i]],
                           tgt_subitem_origin[tgt_mapping[i]], 1);
    }
  }

  for (size_t i = 0; i < src_subitem_seq.size(); i++) {
    delete src_subitem_seq[i];
  }
  for (size_t i = 0; i < tgt_subitem_seq.size(); i++) {
    delete tgt_subitem_seq[i];
  }

  return mapping;
}

void Sequence::deflate(RootsStream* stream, bool is_terminal, int list_index) {
  std::stringstream ss;
  Base::deflate(stream, false, list_index);

  stream->append_unicodestring_content("label", this->get_label());
  stream->append_uint_content("n_elem", this->count());

  Base* item = NULL;

  for (unsigned int index = 0; index < count(); index++) {
    item = get_item(index);
    item->deflate(stream, true, index);
  }

  if (is_terminal) stream->close_object();
}

Relation* Sequence::align_1_to_1(Sequence* targetSequence) {
  std::vector<Base *> src, tgt;
  size_t src_n = count(), tgt_n = targetSequence->count();
  for (size_t i = 0; i < src_n; i++) {
    src.push_back(get_item(i));
  }
  for (size_t i = 0; i < tgt_n; i++) {
    tgt.push_back(targetSequence->get_item(i));
  }
  align::Alignment alg(src, tgt);
  alg.align();

  Relation* rel = new Relation(this, targetSequence);
  std::vector<int> src_mapping = alg.get_source_mapping(),
                   tgt_mapping = alg.get_target_mapping();
  size_t map_n = src_mapping.size();
  for (size_t i = 0; i < map_n; i++) {
    if (src_mapping[i] >= 0 && tgt_mapping[i] >= 0) {
      rel->link(src_mapping[i], tgt_mapping[i]);
    }
  }

  return rel;
}

Relation* Sequence::align_m_to_n(Sequence* targetSequence) {
  return new Relation(this, targetSequence, make_mapping(targetSequence));
}

std::vector<int>* Sequence::find_longest_common(
    sequence::Sequence* findSequence) {
  int nSequence, nFindSequence;
  int nSubSequence;
  std::vector<int>* returnValueVector;
  int i, j;

  returnValueVector = new std::vector<int>(2, -1);

  nSequence = this->count();
  nFindSequence = findSequence->count();

  std::vector<std::vector<int> > L(nSequence, std::vector<int>(nFindSequence));

  nSubSequence = 0;

  for (i = 0; i < nSequence; i++) {
    for (j = 0; j < nFindSequence; j++) {
      if (this->get_item(i)->to_string() ==
          findSequence->get_item(j)->to_string()) {
        if ((i == 0) || (j == 0)) {
          L[i][j] = 1;
        } else {
          L[i][j] = L[i - 1][j - 1] + 1;
        }

        if (L[i][j] > nSubSequence) {
          nSubSequence = L[i][j];
        }

        if (L[i][j] == nSubSequence) {
          (*returnValueVector)[0] = i - nSubSequence + 1;
          (*returnValueVector)[1] = i;
        }
      } else {
        L[i][j] = 0;
      }
    }
  }

  return returnValueVector;
}
}
}
