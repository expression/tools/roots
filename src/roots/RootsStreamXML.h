/*
    This file is part of Roots.

    Roots is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Roots is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Roots.  If not, see <http://www.gnu.org/licenses/>.

    Copyright (c) 2013, 2014 IRISA
*/


#ifndef ROOTS_STREAM_XML_H
#define ROOTS_STREAM_XML_H

#include "common.h"

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include <string.h>
#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <string>
#include <wchar.h>
#include <libgen.h>

#include <stack> 

#include <exception>

#include <unicode/utypes.h>	/* Basic ICU data types */
#include <unicode/ucnv.h>	/* C   Converter API	*/
#include <unicode/ustring.h>	/* some more string fcns */
#include <unicode/uchar.h>	/* char names		*/
#include <unicode/uloc.h>
#include <unicode/unistr.h>
#include <unicode/decimfmt.h>
#include <unicode/ustream.h>
#include <unicode/fmtable.h>
#include <unicode/numfmt.h>
#include <unicode/regex.h>

#include <xercesc/util/PlatformUtils.hpp>
#include <xercesc/framework/XMLFormatter.hpp>
#include <xercesc/framework/LocalFileFormatTarget.hpp>
#include <xercesc/util/XMLString.hpp>
#include <xercesc/dom/DOM.hpp>
#include <xercesc/util/OutOfMemoryException.hpp>
#include <xercesc/parsers/XercesDOMParser.hpp>
#include <xercesc/util/XMLUni.hpp>
#include <xercesc/util/XMLDouble.hpp>
#include <xercesc/util/TranscodingException.hpp>

#include "RootsException.h"
#include "RootsStream.h"

#ifndef ROOTS_STREAM_DEBUG
#define ROOTS_STREAM_DEBUG 0
#endif

namespace roots
{

  /**
   * @brief Class that provides a generic interfarce for roots input/output
   * @details
   * This class provides a set of convenient methods and functions to manipulate
   * streams in input and output.
   *
   * @author damien.lolive@irisa.fr
   * @version 0.1
   * @date 2013
   */
  class RootsStreamXML:public RootsStream
  {

  public:
    /**
     * @brief Default constructor
     */
    RootsStreamXML ()
      {
	init ();

      }
    /**
     * @brief Destructor
     */
    virtual ~ RootsStreamXML ()
      {
	// delete the document
	if (doc != NULL)
	  {
	    doc->release ();
	    doc = NULL;
	  }
      }

    /**
     * @brief Flush and clean the stream
     */
    virtual void clear()
    {
      if (doc != NULL)
	{
	  doc->release ();
	  doc = NULL;
	}
      init ();
    }

  private:
    /**
     * @brief Helper method used to initialized the document. Called by constructor
     */
    void init ()
    {
      XMLCh tempStr[100];

      RootsStream::init();

      xercesc::XMLString::transcode ("LS", tempStr, 99);
      xercesc::DOMImplementation * impl =
	xercesc::DOMImplementationRegistry::getDOMImplementation (tempStr);

      xercesc::XMLString::transcode ("roots", tempStr, 99);
      doc = impl->createDocument (0, tempStr, 0);

      rootNode = doc->getDocumentElement ();

      currentNode = rootNode;
      this->parentNodes = std::stack < xercesc::DOMNode * >();
    }

    /**
     * @brief modifies the current node
     * @param currentNode the new node
     */
    void set_current_node (xercesc::DOMNode * currentNode)
    {
      this->currentNode = currentNode;
    }
    /**
     * @brief Sets the document to new one
     * @param doc pointer to the new document
     */
    void set_doc (xercesc::DOMDocument * doc)
    {
      this->doc = doc;
    }
    /**
     * @brief Modifies the root node
     * @param rootNode the new root node
     */
    void set_root_node (xercesc::DOMNode * rootNode)
    {
      this->rootNode = rootNode;
    }
    /**
     * @brief gives the current node
     * @return current node
     */
    xercesc::DOMNode * get_current_node ()
      {
	return currentNode;
      }
    /**
     * @brief Returns the document
     * @return the current document
     */
    xercesc::DOMDocument * get_doc ()
      {

	return doc;
      }
    /**
     * @brief Returns the root node
     * @return the root node
     */
    xercesc::DOMNode * get_root_node ()
      {
	return rootNode;
      }
    /**
     * @brief clears the document
     */
    void clear_doc ()
    {
      if (doc != NULL)
	{
	  doc->release ();
	  doc = NULL;
	}
      init ();
    }


  public:
    /**
     * @brief loads the XML file content
     * @param _filename
     */
    void load (const std::string & _filename);
    
    /**
     * @brief loads the string content
     * @param _json
     */
    void from_string(const std::string& /*_json*/	,
		     const std::string  /*_baseDirName*/ = "")
    {
      throw RootsException (__FILE__, __LINE__,
			    "Not implemented.\n");

    }

    /**
     * @brief saves the XML file content
     * @param _filename
     */
    void save (const std::string & _filename)
    {
      std::stringstream ss;
      ss << _filename;

      XMLCh tempStr[100];
      xercesc::XMLString::transcode ("LS", tempStr, 99);
      xercesc::DOMImplementation * impl =
	xercesc::DOMImplementationRegistry::getDOMImplementation (tempStr);
      xercesc::DOMLSSerializer * theSerializer =
	((xercesc::DOMImplementationLS *) impl)->createLSSerializer ();

      if (theSerializer->getDomConfig ()->
	  canSetParameter (xercesc::XMLUni::fgDOMWRTFormatPrettyPrint, true))
	theSerializer->getDomConfig ()->
	  setParameter (xercesc::XMLUni::fgDOMWRTFormatPrettyPrint, true);
      theSerializer->getDomConfig ()->
	setParameter (xercesc::XMLUni::fgDOMWRTXercesPrettyPrint, false);

      xercesc::XMLFormatTarget * myFormTarget =
	new xercesc::LocalFileFormatTarget (ss.str ().c_str ());
      xercesc::DOMLSOutput * theOutput =
	((xercesc::DOMImplementationLS *) impl)->createLSOutput ();
      theOutput->setByteStream (myFormTarget);


      try
	{
	  // do the serialization through xercesc::DOMLSSerializer::write();
	  theSerializer->write (doc, theOutput);
	}
      catch (const xercesc::XMLException & toCatch)
	{
	  char *message = xercesc::XMLString::transcode (toCatch.getMessage ());
	  std::cout << "Exception message is: \n" << message << "\n";
	  xercesc::XMLString::release (&message);
	  return;
	}
      catch (const xercesc::DOMException & toCatch)
	{
	  char *message = xercesc::XMLString::transcode (toCatch.msg);
	  std::cout << "Exception message is: \n" << message << "\n";
	  xercesc::XMLString::release (&message);
	  return;
	}
      catch ( ...)
	{
	  std::cout << "Unexpected Exception \n";
	  return;
	}

      theOutput->release ();
      theSerializer->release ();
      delete myFormTarget;
    }


    /**
     * @brief returns a string representation of the XML document
     * @return string representation of the object
     */
    std::string to_string ()
    {
      // xercesc::DOMImplementationLS contains factory methods for creating objects
      // that implement the DOMBuilder and the DOMWriter interfaces
      //static const XMLCh gLS[] = { chLatin_L, chLatin_S, chNull };
      XMLCh tempStr[100];
      xercesc::XMLString::transcode ("LS", tempStr, 99);
      xercesc::DOMImplementation * impl =
	xercesc::DOMImplementationRegistry::getDOMImplementation (tempStr);

      // construct the DOMWriter
      xercesc::DOMLSSerializer * theSerializer =
	((xercesc::DOMImplementationLS *) impl)->createLSSerializer ();

      // plug in user's own error handler
      xercesc::DOMConfiguration * serializerConfig =
	theSerializer->getDomConfig ();
      if (serializerConfig->
	  canSetParameter (xercesc::XMLUni::fgDOMWRTFormatPrettyPrint, true))
	serializerConfig->
	  setParameter (xercesc::XMLUni::fgDOMWRTFormatPrettyPrint, true);
      serializerConfig->
	setParameter (xercesc::XMLUni::fgDOMWRTXercesPrettyPrint, false);
      // serialize the xercesc::DOMNode to a UTF-16 string
      XMLCh *theXMLString_Unicode =
	theSerializer->writeToString (doc->getDocumentElement ());

      std::stringstream ss;
      char *thestr = xercesc::XMLString::transcode (theXMLString_Unicode);
      ss << thestr;
      xercesc::XMLString::release (&thestr);

      // release the memory
      xercesc::XMLString::release (&theXMLString_Unicode);
      theSerializer->release ();
      return std::string (ss.str ().c_str ());
    }
    /**
     * @brief Appends a child element to a node
     * @param name the child name
     */
    void append_object (const char * name, int	list_index=-1 )
    {
      std::stringstream ss;
      XMLCh tempStr[100];

      (void) list_index; /* Makes compiler happy */

      xercesc::DOMNode * pNode = get_current_node ();
      if (pNode == NULL)
	{
	  pNode = get_root_node ();
	}
      this->parentNodes.push (pNode);

      ss.str ("");
      ss << name;
      xercesc::XMLString::transcode (ss.str ().c_str (), tempStr, 99);
      xercesc::DOMElement * element = get_doc ()->createElement (tempStr);
      pNode->appendChild (element);

      set_current_node (element);
    }

    void open_object(const char * name, int  list_index=-1 )
    {
      (void) list_index; /* Makes compiler happy */

#if ROOTS_STREAM_DEBUG == 1
      std::cout << "current_node_name = " << get_node_name() << std::endl;
#endif
      if(std::string(name) != get_node_name())
	{
	  std::stringstream ss;
	  ss << "found node <" << get_node_name() << "> while expecting <" << name << ">";
	  throw RootsException(__FILE__,__LINE__, ss.str());
	}
    }

    void close_object ()
    {
      if (this->parentNodes.size () > 0)
	{
	  xercesc::DOMNode * pNode = this->parentNodes.top ();
	  this->parentNodes.pop ();
	  set_current_node (pNode);
	}
      else
	{
	  set_current_node (NULL);
	}
    }

    void set_object_classname(const std::string& classname)
    {
      set_unicodestring_content("class", classname);
    }

    std::string get_object_classname()
    {
      std::string classname;
      classname = get_unicodestring_content("class");
      return classname;
    }

    // go to first child node
    void open_children()
    {
      xercesc::DOMNode * pNode = get_current_node();
      this->parentNodes.push (pNode);
#if ROOTS_STREAM_DEBUG == 1
      std::cout << "OPEN_CHILDREN before current_node_name: " << get_node_name() << std::endl;
#endif
      set_current_node(dynamic_cast<xercesc::DOMElement*>(pNode)->getFirstElementChild());
#if ROOTS_STREAM_DEBUG == 1
      std::cout << "OPEN_CHILDREN current_node_name: " << get_node_name() << std::endl;
#endif
    }

    // go back to parent node
    void close_children()
    {
#if ROOTS_STREAM_DEBUG == 1
      std::cout << "CLOSE_CHILDREN"<<endl;// current_node_name: " << get_node_name() << std::endl;
#endif
      close_object();
    }


    void next_child()
    {
      set_current_node(dynamic_cast<xercesc::DOMElement*>(get_current_node())->getNextElementSibling());
    }

    // the next object is the first child if exists
    std::string get_object_classname_no_read(const std::string& /* name */, int /* list_index=-1 */)
    {
      std::string attname = "class";
      std::string value = "";
      char *str = NULL;
      const XMLCh *ch = NULL;
      std::stringstream ss;

#if ROOTS_STREAM_DEBUG == 1
      std::cout << "get_object_classname_no_read for node " << get_node_name() << std::endl;
#endif
      xercesc::DOMElement *curElement = dynamic_cast<xercesc::DOMElement *>(get_current_node());
      curElement = curElement->getFirstElementChild();

      ch = curElement->getTagName ();
      str = xercesc::XMLString::transcode (ch);
      value = std::string(str);
      xercesc::XMLString::release (&str);

      if(std::string(attname) == value)
	{

	  ch = curElement->getTextContent ();
	  str = xercesc::XMLString::transcode (ch);

	  ss << str;
	  ss >> value;
	  xercesc::XMLString::release (&str);

	}

#if ROOTS_STREAM_DEBUG == 1
      std::cout << "CLASSNAME_NO_READ: " << value << std::endl;
#endif
      return value;
    }
    
    /**
     * @brief Test if the current node has a child (field) of a given name
     * @param name Name of the searched child
     * @return True if the child exists, false otherwise
     **/
    virtual bool has_child(const std::string& name)
    {
      return has_n_children(name, 1);
    }

    /**
     * @brief Test if the current node has a child (field) of a given name which is an array of n elements
     * @param name Name of the searched child
     * @param n Number of element to test    
     * @return True if the child exists and has n elements, false otherwise
     **/
    virtual bool has_n_children(const std::string& name, const unsigned int n)
    {
      XMLCh tempStr[100];
      xercesc::XMLString::transcode (name.c_str (), tempStr, 99);
      xercesc::DOMElement *curElement = dynamic_cast<xercesc::DOMElement *>(get_current_node()); 
      return (curElement->getElementsByTagName(tempStr)->getLength() == n);
    }

    unsigned int get_n_children(const std::string& name)
    {
      XMLCh tempStr[100];
      xercesc::XMLString::transcode (name.c_str (), tempStr, 99);
      xercesc::DOMElement *curElement = dynamic_cast<xercesc::DOMElement *>(get_current_node()); 
      return curElement->getElementsByTagName(tempStr)->getLength();
    }

    //////////////////////////////////////////

#define XML_DECLARE_APPEND(tna,tva)					\
    virtual void append_ ## tna ## _content(const char * name, const tva & content) \
    {									\
      append_object(name);						\
      set_ ## tna  ## _content(content);				\
      close_object();							\
    }

#define XML_DECLARE_SET_CONTENT_NUMERIC(tna, tva)	\
    void set_ ## tna ## _content (const tva & content)	\
    {							\
      std::stringstream ss;					\
      ss << content;					\
      set_cstr_content (ss.str ().c_str ());		\
    }


#define XML_DECLARE_SET_CONTENT_SCIENTIFIC_NUMERIC(tna, tva)	\
    void set_ ## tna ## _content (const tva & content)		\
    {								\
      std::stringstream ss;						\
      ss << std::scientific << content;				\
      set_cstr_content (ss.str ().c_str ());			\
    }

#define XML_DECLARE_GET_CONTENT(tna,tva)			\
    tva get_ ## tna ## _content ()				\
    {								\
      tva value;						\
      char *content = NULL;					\
      const XMLCh *ch = get_current_node ()->getTextContent ();	\
      content = xercesc::XMLString::transcode (ch);		\
      std::stringstream ss;					\
      ss << content;						\
      ss >> value;						\
      xercesc::XMLString::release (&content);			\
      return value;						\
    }

#define XML_DECLARE_SET_CONTENT_FOR_NODE(tna, tva)			\
    void set_ ## tna ## _content (const char * name, const tva & content) \
    {									\
      append_object(name);						\
      set_ ## tna ## _content(content);					\
      close_object();							\
    }

    /*
      #if ROOTS_STREAM_DEBUG == 1

      #endif
    */

#if ROOTS_STREAM_DEBUG == 1
#define XML_DISP_CONTENT_FOR_NODE  std::cout << "get content for node " << name << std::endl;
#else
#define XML_DISP_CONTENT_FOR_NODE
#endif

#define XML_DECLARE_GET_CONTENT_FOR_NODE(tna, tva)			\
    tva get_ ## tna ## _content (const char * name)			\
    {									\
      XML_DISP_CONTENT_FOR_NODE						\
	if(std::string(name) != get_node_name())			\
	  {								\
	    std::stringstream ss;						\
	    ss << "found node <" << get_node_name() << "> while expecting <" << name << ">"; \
	    throw RootsException(__FILE__,__LINE__, ss.str());		\
	  }								\
      tva value = get_ ## tna ## _content();				\
      next_child();							\
      return value;							\
    }

#define XML_DECLARE_SET_CONTENT_VECTOR(tna, tva, cva)			\
    virtual void set_vector_ ## tna ## _content(const tva & content)	\
    {									\
      set_uint_content("n_elem", (unsigned int) content.size());	\
      for(unsigned int index = 0; index < content.size(); ++index)	\
	{								\
	  append_object("element");					\
	  set_ ## tna ## _content(content[index]);			\
	  close_object();						\
	}								\
    }

#define	XML_DECLARE_GET_CONTENT_VECTOR(tna, tva, cva)		\
    virtual tva get_vector_ ## tna ## _content()		\
    {								\
      tva vec = tva ();						\
      open_children();						\
      unsigned int n_elem = get_uint_content("n_elem");		\
      for(unsigned int index = 0; index < n_elem; ++index)	\
	{							\
	  cva value = get_ ## tna ## _content("element");	\
	  vec.push_back(value);					\
	}							\
      close_children();						\
      return vec;						\
    }

#define XML_DECLARE_SET_CONTENT_SET(tna, tva, cva)     \
    virtual void set_set_ ## tna ## _content(const tva & content)  \
    {                 \
      set_uint_content("n_elem", (unsigned int) content.size());  \
      for( tva ::const_iterator iter = content.begin(); iter != content.end(); ++iter)  \
  {               \
    append_object("element");         \
    set_ ## tna ## _content(*iter);      \
    close_object();           \
  }               \
    }

#define XML_DECLARE_GET_CONTENT_SET(tna, tva, cva)   \
    virtual tva get_set_ ## tna ## _content()    \
    {               \
      tva vec = tva ();           \
      open_children();            \
      unsigned int n_elem = get_uint_content("n_elem");   \
      for(unsigned int index = 0; index < n_elem; ++index)  \
  {             \
    cva value = get_ ## tna ## _content("element"); \
    vec.insert(value);         \
  }             \
      close_children();           \
      return vec;           \
    }

    //////////////////////////////////////////

    // std::string
    XML_DECLARE_APPEND(unicodestring,std::string);

    void set_unicodestring_content (const std::string & content)
    {
      std::stringstream ss;
      XMLCh tempStr[100];

      ss.str ("");
      ss << content;
      xercesc::XMLString::transcode (ss.str ().c_str (), tempStr, 99);
      get_current_node ()->setTextContent (tempStr);
    }

    std::string get_unicodestring_content ()
    {
      char *content = NULL;
      const XMLCh *ch = get_current_node ()->getTextContent ();
      content = xercesc::XMLString::transcode (ch);
      std::string result (content);
      xercesc::XMLString::release (&content);

#if ROOTS_STREAM_DEBUG == 1
      std::cout << "content for node is: " << result << std::endl;
#endif

      return result;
    }

    XML_DECLARE_SET_CONTENT_FOR_NODE(unicodestring,std::string)
      XML_DECLARE_GET_CONTENT_FOR_NODE(unicodestring,std::string)

      // char *
      XML_DECLARE_APPEND(cstr,char*);

    void set_cstr_content (const char *content)
    {
      XMLCh *tempStr;
      unsigned int size = strlen(content);
      tempStr = new XMLCh[size+1];
      xercesc::XMLString::transcode (content, tempStr, size);
      get_current_node ()->setTextContent (tempStr);
      delete tempStr;
    }

    char *get_cstr_content ()
    {
      char *str = NULL;

      const XMLCh *ch = get_current_node ()->getTextContent ();
      str = xercesc::XMLString::transcode (ch);

      //next_child();

      return str;
    }

    XML_DECLARE_SET_CONTENT_FOR_NODE(cstr,char*);
    XML_DECLARE_GET_CONTENT_FOR_NODE(cstr,char*);

    // float
    XML_DECLARE_APPEND(float,float);
    XML_DECLARE_SET_CONTENT_SCIENTIFIC_NUMERIC(float, float);
    XML_DECLARE_GET_CONTENT(float, float);
    XML_DECLARE_SET_CONTENT_FOR_NODE(float, float);
    XML_DECLARE_GET_CONTENT_FOR_NODE(float, float);

    // double
    XML_DECLARE_APPEND(double,double);
    XML_DECLARE_SET_CONTENT_SCIENTIFIC_NUMERIC(double,double);
    XML_DECLARE_GET_CONTENT(double,double);
    XML_DECLARE_SET_CONTENT_FOR_NODE(double, double);
    XML_DECLARE_GET_CONTENT_FOR_NODE(double, double);

    // long double
    XML_DECLARE_APPEND(long_double,long double);
    XML_DECLARE_SET_CONTENT_SCIENTIFIC_NUMERIC(long_double, long double);
    XML_DECLARE_GET_CONTENT(long_double, long double);
    XML_DECLARE_SET_CONTENT_FOR_NODE(long_double, long double);
    XML_DECLARE_GET_CONTENT_FOR_NODE(long_double, long double);

    // int
    XML_DECLARE_APPEND(int,int);
    XML_DECLARE_SET_CONTENT_NUMERIC(int, int);
    XML_DECLARE_GET_CONTENT(int, int);
    XML_DECLARE_SET_CONTENT_FOR_NODE(int, int);
    XML_DECLARE_GET_CONTENT_FOR_NODE(int, int);

    // unsigned int
    XML_DECLARE_APPEND(uint,unsigned int);
    XML_DECLARE_SET_CONTENT_NUMERIC(uint, unsigned int);
    XML_DECLARE_GET_CONTENT(uint, unsigned int);
    XML_DECLARE_SET_CONTENT_FOR_NODE(uint, unsigned int);
    XML_DECLARE_GET_CONTENT_FOR_NODE(uint, unsigned int);

    // bool
    XML_DECLARE_APPEND(bool,bool);
    XML_DECLARE_SET_CONTENT_NUMERIC(bool, bool);
    XML_DECLARE_GET_CONTENT(bool, bool);
    XML_DECLARE_SET_CONTENT_FOR_NODE(bool, bool);
    XML_DECLARE_GET_CONTENT_FOR_NODE(bool, bool);

    // char
    XML_DECLARE_APPEND(char,char);

    void set_char_content (const char content)
    {
      set_unicodestring_content (std::string(&content, 1));
    }

    XML_DECLARE_GET_CONTENT(char, char);
    XML_DECLARE_SET_CONTENT_FOR_NODE(char,char);
    XML_DECLARE_GET_CONTENT_FOR_NODE(char,char);

    // UChar
    XML_DECLARE_APPEND(uchar,UChar);
    XML_DECLARE_SET_CONTENT_NUMERIC(uchar, UChar);
    //XML_DECLARE_GET_CONTENT(uchar, UChar);
    UChar get_uchar_content ()				
    {								
      UChar value;						
      char *content = NULL;					
      const XMLCh *ch = get_current_node ()->getTextContent ();	
      content = xercesc::XMLString::transcode (ch);		
      std::stringstream ss;					
      ss << content;
      unsigned short int v;
      ss >> v;
      value = v;						
      xercesc::XMLString::release (&content);			
      return value;						
    }

    XML_DECLARE_SET_CONTENT_FOR_NODE(uchar,UChar);
    XML_DECLARE_GET_CONTENT_FOR_NODE(uchar,UChar);

    // std::vector<float>
    XML_DECLARE_APPEND(vector_float, std::vector<float>);
    XML_DECLARE_SET_CONTENT_VECTOR(float, std::vector<float>, float);
    XML_DECLARE_GET_CONTENT_VECTOR(float, std::vector<float>, float);
    XML_DECLARE_SET_CONTENT_FOR_NODE(vector_float, std::vector<float>);
    XML_DECLARE_GET_CONTENT_FOR_NODE(vector_float, std::vector<float>);

    // std::vector<int>
    XML_DECLARE_APPEND(vector_int, std::vector<int>);
    XML_DECLARE_SET_CONTENT_VECTOR(int, std::vector<int>, int);
    XML_DECLARE_GET_CONTENT_VECTOR(int, std::vector<int>, int);
    XML_DECLARE_SET_CONTENT_FOR_NODE(vector_int, std::vector<int>);
    XML_DECLARE_GET_CONTENT_FOR_NODE(vector_int, std::vector<int>);

    // std::vector<bool>
    XML_DECLARE_APPEND(vector_bool, std::vector<bool>);
    XML_DECLARE_SET_CONTENT_VECTOR(bool, std::vector<bool>, bool);
    XML_DECLARE_GET_CONTENT_VECTOR(bool, std::vector<bool>, bool);
    XML_DECLARE_SET_CONTENT_FOR_NODE(vector_bool, std::vector<bool>);
    XML_DECLARE_GET_CONTENT_FOR_NODE(vector_bool, std::vector<bool>);

    // std::vector<UChar>
    XML_DECLARE_APPEND(vector_uchar, std::vector<UChar>);
    XML_DECLARE_SET_CONTENT_VECTOR(uchar, std::vector<UChar>, UChar);
    XML_DECLARE_GET_CONTENT_VECTOR(uchar, std::vector<UChar>, UChar);
    XML_DECLARE_SET_CONTENT_FOR_NODE(vector_uchar, std::vector<UChar>);
    XML_DECLARE_GET_CONTENT_FOR_NODE(vector_uchar, std::vector<UChar>);

    // std::vector<std::string>
    XML_DECLARE_APPEND(vector_unicodestring, std::vector<std::string>);
    XML_DECLARE_SET_CONTENT_VECTOR(unicodestring, std::vector<std::string>, std::string);
    XML_DECLARE_GET_CONTENT_VECTOR(unicodestring, std::vector<std::string>, std::string);
    XML_DECLARE_SET_CONTENT_FOR_NODE(vector_unicodestring, std::vector<std::string>);
    XML_DECLARE_GET_CONTENT_FOR_NODE(vector_unicodestring, std::vector<std::string>);

    // std::set<int>
    XML_DECLARE_APPEND(set_int, std::set<int>);
    XML_DECLARE_SET_CONTENT_SET(int, std::set<int>, int);
    XML_DECLARE_GET_CONTENT_SET(int, std::set<int>, int);
    XML_DECLARE_SET_CONTENT_FOR_NODE(set_int, std::set<int>);
    XML_DECLARE_GET_CONTENT_FOR_NODE(set_int, std::set<int>);

    // std::set<UChar>
    XML_DECLARE_APPEND(set_uchar, std::set<UChar>);
    XML_DECLARE_SET_CONTENT_SET(uchar, std::set<UChar>, UChar);
    XML_DECLARE_GET_CONTENT_SET(uchar, std::set<UChar>, UChar);
    XML_DECLARE_SET_CONTENT_FOR_NODE(set_uchar, std::set<UChar>);
    XML_DECLARE_GET_CONTENT_FOR_NODE(set_uchar, std::set<UChar>);



    // boost::numeric::ublas::mapped_matrix<int>
    XML_DECLARE_APPEND(matrix_int,boost::numeric::ublas::mapped_matrix<int>);

    void set_matrix_int_content(const boost::numeric::ublas::mapped_matrix<int>& content)
    {
      boost::numeric::ublas::mapped_matrix<int>::const_iterator1 iter1 = content.begin1();
      boost::numeric::ublas::mapped_matrix<int>::const_iterator2 iter2;

      set_uint_content("n_row", (unsigned int) content.size1());
      set_uint_content("n_col", (unsigned int) content.size2());

      unsigned int n_elem = 0;
      while(iter1 != content.end1())
	{
	  iter2 = iter1.begin();
	  while(iter2 != iter1.end())
	    {
	      if(*iter2 != 0) n_elem++;
	      iter2++;
	    }
	  iter1++;
	}
      set_uint_content("n_elem", n_elem);

      iter1 = content.begin1();
      while(iter1 != content.end1())
	{
	  iter2 = iter1.begin();
	  // Use iterators to access only non-null elements
	  while(iter2 != iter1.end())
	    {
	      append_object("matrix_element");
	      append_uint_content("row_index", (unsigned int) iter2.index1());
	      append_uint_content("column_index", (unsigned int) iter2.index2());
	      append_int_content("value", (*iter2));
	      close_object();
	      iter2++;
	    }

	  iter1++;
	}
    }


    boost::numeric::ublas::mapped_matrix<int> get_matrix_int_content()
      {
	boost::numeric::ublas::mapped_matrix<int> mat;
	unsigned int n_row=0;
	unsigned int n_col=0;
	unsigned int n_elem=0;
	unsigned int row_index, col_index;
	int value;

	open_children();
	n_row = get_uint_content("n_row");
	n_col = get_uint_content("n_col");
	n_elem = get_uint_content("n_elem");

	mat.resize(n_row, n_col,false);

	for(unsigned int index=0; index<n_elem; ++index)
	  {
	    open_children();
	    row_index = get_uint_content("row_index");
	    col_index = get_uint_content("column_index");
	    value = get_uint_content("value");

	    mat(row_index, col_index) = value;
	    close_children();
	    next_child();
	  }
	close_children();
	return mat;
      }

    XML_DECLARE_SET_CONTENT_FOR_NODE(matrix_int,boost::numeric::ublas::mapped_matrix<int>);
    XML_DECLARE_GET_CONTENT_FOR_NODE(matrix_int,boost::numeric::ublas::mapped_matrix<int>);


    // std::map<std::string, std::string>
    //	  XML_DECLARE_APPEND(map_unistr_unistr,std::map<std::string COMMA std::string>);
    virtual void append_map_unistr_unistr_content(const char * name, const std::map<std::string, std::string> & content)
    {
      append_object(name);
      set_map_unistr_unistr_content(content);
      close_object();
    }

    virtual void set_map_unistr_unistr_content(const std::map<std::string, std::string>& content)
    {
      std::map<std::string, std::string>::const_iterator iter;

      set_uint_content("n_elem", (unsigned int) content.size());

      iter = content.begin();
      while(iter != content.end())
	{
	  std::string value = (*iter).second;
	  std::string key = (*iter).first;

	  append_object("element");
	  set_unicodestring_content("key", key);
	  set_unicodestring_content("value",value);
	  close_object();

	  iter++;
	}
    }


    std::map<std::string, std::string> get_map_unistr_unistr_content()
      {
	unsigned int n_elem=0;
	std::map<std::string, std::string> content;

	open_children();
	n_elem = get_uint_content("n_elem");

	for(unsigned int index=0; index<n_elem; ++index)
	  {
	    open_children();
	    std::string key = get_unicodestring_content("key");
	    std::string value = get_unicodestring_content("value");
	    content[key] = value;
	    close_children();
	    next_child();
	  }

	close_children();

	return content;
      }

    //	 XML_DECLARE_SET_CONTENT_FOR_NODE(map_unistr_unistr,std::map<std::string COMMA std::string>);
    void set_map_unistr_unistr_content (const char * name, const std::map<std::string, std::string> & content)	
    {									
      append_object(name);						
      set_map_unistr_unistr_content(content);				
      close_object();							
    }

    //    XML_DECLARE_GET_CONTENT_FOR_NODE(map_unistr_unistr,std::map<std::string COMMA std::string>);
    std::map<std::string, std::string> get_map_unistr_unistr_content (const char * name)
      {									
	XML_DISP_CONTENT_FOR_NODE						
	  if(std::string(name) != get_node_name())			
	    {								
	      std::stringstream ss;
	      ss << "found node <" << get_node_name() << "> while expecting <" << name << ">"; 
	    throw RootsException(__FILE__,__LINE__, ss.str());		
	    }								
	std::map<std::string, std::string> value = get_map_unistr_unistr_content();				
	next_child();							
	return value;							
      }


    /**
     * @brief Returns the node name
     * @return Name of the node
     */
    std::string get_node_name ()
    {
      if(xercesc::DOMNode::ELEMENT_NODE == currentNode->getNodeType())
	{
	  char *str = NULL;
	  const XMLCh *ch = ((xercesc::DOMElement*)get_current_node ())->getTagName ();
	  str = xercesc::XMLString::transcode (ch);
	  std::string result (str);
	  xercesc::XMLString::release (&str);

	  return result;
	}
      return std::string("NOT AN ELEMENT");
    }

  private:
    xercesc::DOMDocument * doc;				/**< The XML document */
    xercesc::DOMNode * rootNode;			/**< The root node of the document */
    xercesc::DOMNode * currentNode;		/**< The current node, always a child of the root node */
    std::stack < xercesc::DOMNode * >parentNodes;		/**< The parent nodes, always a child of the root node */
  };

}

#endif // ROOTS_STREAM_XML_H
