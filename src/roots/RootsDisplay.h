/*
    This file is part of Roots.

    Roots is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Roots is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Roots.  If not, see <http://www.gnu.org/licenses/>.

    Copyright (c) 2013, 2014 IRISA
*/


/*
 * RootsDisplay.h
 *
 *  Created on: Sep 2, 2013
 *	Author: zene
 */

#ifndef ROOTSDISPLAY_H_
#define ROOTSDISPLAY_H_

#include "Utterance.h"
#include "Relation.h"

#include <list>
#include <algorithm>
#include <limits>
#include <boost/unordered_map.hpp>
#include <boost/unordered_set.hpp>
#include <boost/spirit/include/classic.hpp>

#define NIL	    -1
#define SHARPLIMIT  -1
#define IS_SHARP(i)  (i < SHARPLIMIT)
#define HASHINDEX_TO_INDEX(i)  (-i)

// Note : may need to change max tex memory : go to /usr/share/texlive/texmf/web2c then in texmf.cnf . Change main_memory = 3000000 to main_memory = 5000000. Then sudo fmtutil-sys --all .

namespace roots {



  class RootsDisplay {

  public:
    RootsDisplay();
    virtual ~RootsDisplay();

  private:
    class UtteranceDisplay;
    struct sharp {
      int firstElem;
      int lastElem;
      bool containsNILS;
    };

  private:
    int* aligned_matrix; // TODO : move into vector<vector<int>> ?
    int matrix_height;
    int matrix_length;
    std::vector<std::string> sequencesInMatrix;
    
    std::vector<sharp* > sharps_ht;
    std::vector<std::vector<int> > sharps_table;
    int sharpsNr;

  private:
    //to_aligned_matrix algorithm helper functions
    bool breaksItem(const std::vector<int>* c1, const std::vector<int>* c2, const std::vector<int>* c3);
    int orderable(const std::vector<int>* c1, const std::vector<int>* c2);
    void postProcessMatrix(std::list<std::vector<int>* > & matrix, int height);
    void genCompatibleCombinations(const std::list<  std::vector<int>* > & alignedMatrix,
				   const  std::vector<boost::unordered_set<int> > & relatedElements,
                                   std::vector< std::pair<int, bool> > & matched_col,
				    std::vector<  std::vector<int>* > & combinations,
				    std::vector<bool> & usedColumns);
    void initMatrix(std::list<std::vector<int>* > &matrix, sequence::Sequence &firstSequence);
    void insertColumn(std::list<std::vector<int>* > & matrix,  std::vector<int>* column);
    bool is_star(sharp* sharpRef, size_t row, UtteranceDisplay &utt, std::vector<std::string> sequencesInMatrix);
    void toAlignedMatrix( UtteranceDisplay & utt, // Note : utt is not constant since we use the relation cache !
			  const std::vector<std::string> & seqLabels);
  private:
    //to_aligned_matrix debugging functions
    void printMatrix(const std::list<std::vector<int>* > &matrix,
		     int height,
		     std::ostream &strout);
    void printColumn(std::vector<int>* c);
    void printSharps();

  private:
    //cleaning memory functions
    void deleteSharp(int key);
    void clean_mem();

  private:
    //to_pgf algorithm helper functions
    std::string make_color_darker(std::string color);
    void make_label_pgf_compatible(std::string& label);
    double get_item_width(const roots::sequence::Sequence & seq, int item_index, int level);
    std::string get_item_label(Base* item, int level);
    void draw_arrows(std::ostream &stream, UtteranceDisplay &utt,
		     const std::string &source_seq, const std::string &target_seg,
		     int item_index);


  public:
    //BaseItem* get_item(int row, int col);    TODO:
    //double get_item_width(int row, int col); TODO:
    //bool is_star(int row, int col);          TODO:   
    //bool is_sharp(int row, int col);         TODO:
    //std::vector<BaseItem*>* get_sharp_related_items(int row); TODO:
    std::string to_pgf(Utterance & utt, // Note : utt is not constant since we use the relation cache !
		       const std::vector<std::string> & seqLabels,
		       int level=0,
		       bool draw_relations=false);
    
    std::string to_pgf(Utterance & utt, // Note : utt is not constant since we use the relation cache !
			 const std::vector<std::string> & seqLabels,
			 const std::vector<std::string> & hiddensSeqLabels,
			 const std::vector<int> & displayLevels,
			 bool draw_relations=false);
    




  private:
  template<class T>
  inline bool relatedElementsEqual(const  std::vector<T> & r1, const  std::vector<T> &r2)
  {
    if(r1.size() != r2.size())
      {return false;}
    for( size_t i = 0; i != r1.size(); ++i)
      {
	if((r1[i].size() != r2[i].size()) ||
	   (!std::equal(r1[i].begin(), r1[i].end(), r2[i].begin())))
	  {return false;}
      }
    return true;
  }

    template <class ForwardIterator, class BinaryPredicate>
      ForwardIterator my_unique (ForwardIterator first, ForwardIterator last, BinaryPredicate pred)
    {
      if (first==last) return last;

      ForwardIterator result = first;
      int col;
      while (++first != last)
	{
	  col = pred(*result,*first);
	  if (!col)  {
	    *(++result)=*first;
	  }
	  else {
	    if(col > 0) {
	      delete *first;
	    }
	    else {
	      delete *result;
	      *result = *first;
	    }
	  }
	}
      return ++result;
    };


    template<class InputIterator, class T>
      InputIterator searchPlaceToInsert(InputIterator &first, InputIterator last, T& val, bool &isOrderable, int &contBreaker)
    {
      InputIterator next = first;
      InputIterator before_next = first;
      InputIterator after_next;
      bool smallerElementChance = true, forwardBreakChance = true;
      int res;
      do {
	if(next == last) {
	  next = before_next;
	  before_next--;
	  if(next != first && breaksItem(*(before_next), *next, val)) {
	    contBreaker = 2;
	    first = before_next;
	    return next;
	  }
	  return last;
	}

	res = orderable(val, *next);

	if (res == 0) {
	  isOrderable = false;
	  first = next;	  //the meaning here is that columns in interval [next, next] should be included in a sharp
	  return next;
	}

	if( res > 0 ) {
	  if(breaksItem(*(before_next), *next, val)) { //if inserting val after "first", "first" would break val from "before"
	    contBreaker = 2;
	    first = before_next;
	    return next;
	  }
	}
	else if(res < 0) {
	  //search if there is an element smaller than me after me
	  bool isSmallerElement = false, forwardBreak = false;
	  if(smallerElementChance || forwardBreakChance) {
	    after_next = next;
	    while(++after_next != last) {
	      if(orderable(val, *after_next) >= 0) { //patch: if the column is not smaller but unorderable we consider it smaller for the moment
		isSmallerElement = true;
		break;
	      }
	      if(breaksItem(val, *next, *after_next) > 0) {
		forwardBreak = true;
		break;
	      }
	    }
	  }
	  if(isSmallerElement || forwardBreak) { //try to advance carefully
	    while(next != after_next) {
	      if(orderable(*next, val) > 0) {
		if(isSmallerElement) {
		  isOrderable = false;
		  first = next;
		  return after_next;
		}
		else {
		  contBreaker = -1;
		  first = next;
		  return after_next;
		}
	      }
	      if(next != first && breaksItem(*(before_next), *next, val)) { //do i break myself by advancing?
		if(isSmallerElement) { //i have to advance so I have to solve this discontinuity
		  contBreaker = 2;
		  first = before_next;
		  return next;
		}
		else {	//it is not mandatory to advance so I prefer to preserve this continuity than try to obtain something I am not sure I could obtain
		  contBreaker = -1;
		  first = next;
		  return after_next;
		}
		before_next++;
	      }
	      next++;
	    }
	    next--;
	  }
	  else { //after_next == last
	    smallerElementChance = forwardBreakChance = false;
	    if(next != first) { //i have more at least two elements
	      if(!breaksItem(*before_next, val, *next)) { //do i break anything?
		return next;
	      }
	      else {
		if(orderable(*next, val) > 0 || breaksItem(*before_next, *next, val)) { //can I advance? If the next element is greater than me, or I break myself NO
		  contBreaker = 1;
		  first = before_next;
		  return next;
		}
		//else let the loop go on and find another place of insertion
	      }
	    }
	    else {
	      return next;
	    }
	  }

	}
	before_next = next;
	next++;
      }
      while(1);
    }

    template<class InputIterator>
      void solveContinuity(std::list< std::vector<int>* > & matrix, InputIterator first, InputIterator last, std::vector<int>* val, int contBreaker)
      {
	//find where the incontinuity appears

	std::vector<int> *c1 = NULL, *c2 = NULL, *c3 = NULL;
	switch(contBreaker) {
	case -1: c1 = val, c2 = *first, c3 = *last;
	  break;
	case 1: c1 = *first, c2 = val, c3 = *last;
	  break;
	case 2: c1 = *first, c2 = *last, c3 = val;
	  break;
	}

	for(size_t i = 0; i < c1->size(); i++) {
	  if( (*c1)[i] != NIL && (*c1)[i] == (*c3)[i] && (*c2)[i] == NIL) {
	    solveOrder(matrix, first, last, val, i);
	    break;
	  }
	}
      }

    /**
     * range [first, last] (inclusive last)
     */
		template<class InputIterator>
		void solveOrder(std::list< std::vector<int>* > & matrix, InputIterator first, InputIterator last,  std::vector<int>* c1, int row)
		{
			//find elements that should be included in sharp
			int item = (*c1)[row];
			
			//find elements from the column that is to be inserted
			int newLastEl, newFirstEl;
			int max_sharp_size = 0;
			int newSharpKey = (IS_SHARP(item)) ? item : 0 ;
			if(newSharpKey != 0) {
				sharp* s = sharps_ht[HASHINDEX_TO_INDEX(item)];
				max_sharp_size = s->lastElem - s->firstElem + 1;
				newFirstEl = s->firstElem;
				newLastEl = s->lastElem;
			} else {
				newFirstEl = (item != NIL)? item : INT_MAX ;
				newLastEl = (item != NIL)? item : 0;
			}
			
			//find elements from the range [first, last] (inclusive last)
			InputIterator tmp = first;
			do {
				item = (**tmp)[row];
				if(IS_SHARP(item)) {
					sharp* s = sharps_ht[HASHINDEX_TO_INDEX(item)];
					if(s->lastElem - s->firstElem + 1 > max_sharp_size) {
						max_sharp_size = s->lastElem - s->firstElem;
						newSharpKey = item;
					}
					newLastEl =  std::max(newLastEl, s->lastElem);
					newFirstEl = std::min(newFirstEl, s->firstElem);
				} else if(item != NIL) {
					newLastEl =  std::max(newLastEl, item);
					newFirstEl = std::min(newFirstEl, item);
				}
			} while(tmp++ != last); //this condition is needed because we have to iterate over the whole range (inclusive last)
			
			//create sharp
			sharp* newSharp;
			if ( newSharpKey != 0 ) {
				newSharp = sharps_ht[HASHINDEX_TO_INDEX(newSharpKey)];
			} else {
				newSharpKey = --sharpsNr;
				newSharp = new sharp;
				sharps_ht.push_back(newSharp);
			}
			newSharp->firstElem = newFirstEl;
			newSharp->lastElem = newLastEl;
			newSharp->containsNILS = false;
			//problem with * ???
			for(int i = newSharp->firstElem; i <= newSharp->lastElem; i++) {
				sharps_table[row][i] = newSharpKey;
			}
			
			//extend the range to every element that should be in the sharp
			bool canExtendLeft = true, canExtendRight = true;
			while(canExtendLeft || canExtendRight) {
				if(canExtendLeft) {
					tmp = first;
					if(tmp != matrix.begin() ) {
						tmp --;
					}
					while(tmp != matrix.begin() && (**tmp)[row] == NIL) {
						tmp--;
					}
					if(tmp == matrix.begin()) {
						canExtendLeft = false;
					} else {
						canExtendLeft = (((**tmp)[row] >= newSharp->firstElem) ||
						                 (IS_SHARP((**tmp)[row]) &&
						                  sharps_ht[HASHINDEX_TO_INDEX((**tmp)[row])]->firstElem >= newSharp->firstElem) );
					}
					if(canExtendLeft) {
						first = tmp;
						//	      first--;
					}
				}
				if(canExtendRight) {
					tmp = ++last;
					while(tmp != matrix.end() && (**tmp)[row] == NIL) {
						tmp++;
					}
					canExtendRight = (tmp != matrix.end() &&
					                  ( (!IS_SHARP((**tmp)[row]) && (**tmp)[row] <= newSharp->lastElem) ||
					                    ( IS_SHARP((**tmp)[row]) &&
					                      sharps_ht[HASHINDEX_TO_INDEX((**tmp)[row])]->lastElem <= newSharp->lastElem)) );
					if(canExtendRight) {
						last = tmp;
					}
				}
			}
			
			//transform all the elements in the range into a sharp
			while(first != last) {
				if((**first)[row] == NIL) {
					newSharp->containsNILS = true;
					(**first)[row] = newSharpKey;
				} else if((**first)[row] != newSharpKey) {
					(**first)[row] = newSharpKey;
				}
				first++;
			}
			
			//do not forget about the vector that is NOT YET inserted
			if((*c1)[row] != newSharpKey) {
				if((*c1)[row] == NIL) {
					newSharp->containsNILS = true;
				}
				(*c1)[row] = newSharpKey;
			}
		}
    class UtteranceDisplay {
    public:
      UtteranceDisplay(Utterance &utt, 
		       const std::vector<std::string> &seqLabels,
		       const std::vector<std::string> & hiddensSeqLabels,
		       const std::vector<int> & displayLevels);
      ~UtteranceDisplay();

    private:
      bool previousCacheUtt;
      Utterance *utt;
      boost::unordered_map<std::string, 
	std::pair<sequence::Sequence*, sequence::Sequence*> > virtual_sequences;
      std::map<std::string, Relation* > virtual_relations;
      std::vector<std::string> new_seq_labels;
      boost::unordered_map<std::string, bool > seqIsHidden;
      boost::unordered_map<std::string, int >  seqDisplayLevels;
    
    public:
      Relation* get_relation(const std::string& source_sequence, const std::string& target_sequence);
      sequence::Sequence* get_sequence(const std::string& label, const std::string& contentType = "") const;
      bool sequenceIsHidden(const std::string& label);
      int sequenceDisplayLevel(const std::string& label);

      const std::vector<std::string>& get_all_sequence_labels() const;

    private:
      void create_virtual_sequences(const std::string& label,
				    bool isHidden,
				    int level);
      Relation* create_virtual_relation(sequence::Sequence *source_seq, 
					sequence::Sequence *target_seq, 
					bool virt_is_source=true);
    };

  };

} /* namespace roots */
#endif /* ROOTSDISPLAY_H_ */
