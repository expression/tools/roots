/*! \page tutorial_sequence_creation How to to create Roots objects?
  \tableofcontents
  
  The structure of data is as shown in the figure below.
  \image html roots_architecture.png "Overview of data organization in Roots"
  \image latex roots_architecture.eps "Overview of data organization in Roots"
  Starting from scratch, creating a Roots corpus thus consists in building objects of all the hierarchical levels. First, items have to be created for each sequence of annotation under study and links between items of the various aligned sequences have to be set. Then, sequences of a same content have to be gathered into a same utterance. And finally, each built utterance is inserted into a corpus.
  
  Layers can be thought as a serie of corpora C_i where each corpus C_i shares a common sequence with the upper corpus C_(i-1) and the lower corpus C_(i+1). So, when designing a layered corpus, you should think about what is the shared/pivot sequence.
  
  On their side, chunks do not need to be anticipated in their simple most direct usage. Basically, the size of chunks (in number of utterances) can be given when saving a corpus. Ask your question on the [tracker](https://gforge.inria.fr/tracker/?atid=13965&group_id=5375&func=browse) for advanced usage.
  
  Better than writing tons of philosophical explanations, this tutorial will exhibit how a Roots corpus can be built based on outputs from various tools. This tutorial can be found in the folder \c sample/ of your Roots directory. First, input data are detailed and the subsequent multi-layered corpus design is exposed. Then, scripts to build each layer are given. Finally, the way to combine these scripts is given thanks to a shell (bash) script.
  
  \remark This tutorial does not aim any specific task. This is just for the purpose of explaining the corpus creation process.
  
  \section setup Setup
  
  \subsection data Overview of input data
  
  Let us assume 2 input raw texts, one in English and its translation in French. These are stored in the following files.
  \li \c data/sample1.en
  \verbinclude data/sample1.en
  \li \c data/sample1.fr
  \verbinclude data/sample1.fr
  We assume to have aligned these 2 files with Giza++, a toolkit for machine translation (see http://code.google.com/p/giza-pp). This leads to the following alignment file (one line per sentence).
  \li \c data/sample1.giza
  \verbinclude data/sample1.giza
  
  In addition, we considere that the English text has been POS-tagged using the Brill tagger (see http://www.ling.gu.se/~lager/mogul/brill-tagger), and the French text has been phonetized in the SAMPA format (word separations are marked).
  \li \c data/sample1.en.brill
  \verbinclude data/sample1.en.brill
  \li \c data/sample1.fr.sampa
  \verbinclude data/sample1.fr.sampa
  
  \subsection design Design of the corpus
  
  Based on the previous files, we want to build 2 layers: one for English-French translation, a second one for linguistics (here only POS). Each layer will include the following sequences:
  - \b Layer \b translation
    - English raw words: the sequence name is "Word EN"
    - French raw words: "Word FR"
  - \b Layer \b linguistics
    - English raw words: "Word EN"
    - English words as tokenized by the POS tagger: "Word POS EN"
    - English POS: "POS EN"
  - \b Layer \b phonetics
    - French raw words: "Word EN"
    - French phonemes with word separations: "Phoneme FR"
  
  \section layers Building of individual layers
  
  \subsection translation Translation layer
  Script \c giza2roots.pl
  \include giza2roots.pl
  
  \subsection pos-tagging Brill layer
  \remark The main part of the code is used to parse the part-of-speech alphabet. Here is the parsing is not completly exhaustive.
  Script \c brill2roots.pl
  \include brill2roots.pl
  
  \subsection phonetization Phonetic layer
  Script \c sampa2roots.pl
  \include sampa2roots.pl
  
  \section merge Merging of layers
  Script \c run-demo.sh
  \include run-demo.sh
  
  Below is a display of the first utterance of the generated corpus.
  \image html sample1_corpus.json_0.png "Complete view of the first sentence"
  \image latex sample1_corpus.json_0.ps "Complete view of the first sentence"
  
*/
