/*!\page tutorial_browsing How to browse Roots objects?
This tutorial presents how a Roots corpus can be browsed. Two task examples are given. Both seek to browse a corpus in order to extract statistics. This simple type of problem enables a clear exhibition of Roots mechanisms and functionnalities. Of course, more advanced tasks can also be addressed using these same concepts without any drastic increasing of the complexity.

The two sample tasks are presented using the Perl API first, and an example in C++ is then given for the second task.

\tableofcontents

\section word_freq Computing word frequencies (Perl)

In this first example, one wishes to count the number of occurrences of a target word in a whole corpus, in Roots JSON format. This is achieved by the sub-routine count_words which reads a target word and the Roots input file. First, the corpus is created and loaded. Then, it is browsed using a loop of all utterance indices. Each utterance i is read and its word sequence is extracted. Eventually, the sequence has to be cast into a specific sequence type (here a word sequence) using the method as_word_sequence(). This is optional in this example but this is useful in some situations to guarantee the access to specific attributes or methods of the sequence type. Then, the content of every word item j in this sequence is compared to the target word string, and a counter is incremented if the two words match.

\code{.pl}
use roots;

sub count_words {
	my $target_word = shift;
	my $roots_file = shift;
	my $frequency = 0;
	
	my $corpus = new roots::Corpus($roots_file);
	
	my $n_utt = $corpus->count_utterances();
	
	for (my $i = 0; $i < $n_utt; $i++) {
		my $utt = $corpus->get_utterance($i);
		my $word_seq = $utt->get_sequence("Word")->as_word_sequence();
		my $n_wrd = $word_seq->count();
		for (my $j = 0; $j < $n_wrd; $j++) {
			if ($word_seq->get_item($j)->get_label() eq $target_word) {
				$frequency++;
			}
		}
	}
	return $frequency;
}

my $frequency = count_word_frequency("foo", "/path/to/my_roots_files.json");
print "$frequency occurrences\n";
\endcode

Browsing the word sequence could be achieved in another way. Instead of iterating over each word item j, it is possible to directly retrieve (a reference to) an array of items. To do so, the inner part of the loop over utterances in the example above could be replaced by the following lines.
\code{.pl}
		my $utt = $corpus->get_utterance($i);
		my $word_seq = $utt->get_sequence("Word")->as_word_sequence();
		foreach my $word_item (@{$word_seq->get_all_items()}) {
			if ($word_item)->get_label() eq $target_word) {
				$frequency++;
			}
		}
\endcode

\section syllable_freq_perl Computing frequencies of last syllables (Perl)

In this second example, the goal is to count the frequency of syllables which end all the words in a corpus. This is done by creating a hash map %frequency whose keys are syllables and which is updated when reading each utterance of the corpus. Counting end-of-word syllables is performed by the sub-routine count_last_syllables which reads as an input a reference to the hash map of frequencies and an input.

First, the respective word and syllable sequences are read from the utterance and the relation between them is extracted. Then, for each word at position i, the positions of all corresponding syllables are computed. Based on the last index, the last syllable is read from the whole syllable sequence. Finally, the hash map is either initialized if the syllable is seen for the first time or simply incremented otherwise.

After applying the function on all utterances, global statistics can be dumped by browsing the hash map.

\code{.pl}
use roots;

sub count_last_syllables {   
	my $p_frequency = shift;
	my $utt = shift;
	
	my $word_seq = $utt->get_sequence("Word");
	my $syllable_seq = $utt->get_sequence("Syllable");
	my $relation = $utt->get_relation("Word", "Syllable");
	
	for (my $i = 0; $i < $word_seq->count(); $i++) {   
		my @index_list = @{$relation->get_related_elements($i)};
		my $last_index = pop(@index_list);
		my $last_syllable = $syllable_seq->get_item($last_index)->to_string();
		if(!exists($$p_frequency{$last_syllable})) {
			$$p_frequency{$last_syllable} = 1;
		}
		else {
			$$p_frequency{$last_syllable} += 1;
		}
	}
}

my %frequency;

my $corpus = new roots::Corpus("/path/to/my_roots_files.json");
my $n_utt = $corpus->count_utterances();
for (my $i = 0; $i < $n_utt; $i++) {
	my $utt = $corpus->get_utterance($i);
	count_last_syllables(\%frequency, $utt);
}

while (my ($syl,$freq) = each(%frequency)) {
	print "$syl\t$freq\n");
}
\endcode

Similarly as in the first example (word frequency computation), the iteration over items can be made implicit. By doing this way, relations can be hidden. The code of this alternative version is provided below.

\code{.pl}
sub count_last_syllables {   
	my $p_frequency = shift;
	my $utt = shift;
	
	my $word_seq = $utt->get_sequence("Word");
	
	foreach my $word (@{$word_seq->get_all_items()}) {   
		my @syllables = @{$word->get_related_items("Syllable")};
		my $last_syllable = pop(@syllables)->to_string();
		if(!exists($$p_frequency{$last_syllable})) {
			$$p_frequency{$last_syllable} = 1;
		}
		else  {
			$$p_frequency{$last_syllable} += 1;
		}
	}
}
\endcode

\section syllable_freq_cpp Computing syllable frequencies (C++)

Browsing data in C++ is very close to the code in Perl (which is logical is the Perl API is derived from the C++ API). This section provides the equivalent of both versions of last syllable frequency computation presented above in Perl. The main differences are: strong typing, memory deallocation, and STL containers handling (instead of Perl's arrays and hash maps).

Here is the first version, based on an explicit relation and iterations over indices. Notice that all the getter methods return a pointer to the original object, \b except the method get_utterance.
\code{.cpp}
#include "roots.h"

using namespace std;
using namespace roots;
using namespace log4cplus;

void count_last_syllables(std::map<std::string,int> &frequencies, roots::Utterance *utt) {
		roots::Sequence *word_seq = utt->get_sequence("Word");
		roots::Sequence *syllable_seq = utt->get_sequence("Syllable");
		roots::Relation *relation = utt->get_relation("Word", "Syllable");
		
		for (int i = 0; i < word_seq->count(); i++) {
			std::vector<int> index_list = relation->get_related_elements(i);
			int last_index = index_list.back();
			std::string last_syllable = syllable_seq->get_item(last_index)->to_string();
			if (frequencies.find(last_syllable) == frequencies.end()) {
				frequencies[last_syllable] = 1;
			}
			else {
				frequencies[last_syllable] += 1;
			}
		}
		
		// Nothing to delete here
}

int main(int argc, char *argv[]) {
	ROOTS_INIT();
	ROOTS_SET_LOGGER_LEVEL(ROOTS_LOGGER_ERROR_LEVEL);
	
	std::map<std::string,int> frequencies;
	
	roots::Corpus *corpus = new roots::Corpus("/path/to/my_roots_files.json");
	int n_utt = corpus->count_utterances();
	for (int i = 0; i < n; i++) {
		// Get a *copy* of utterance i
		roots::Utterance *utt = corpus->get_utterance(i);
		// Update the map
		count_last_syllables(frequencies, utt)
		// Delete the copy of utterance i
		delete utt;
	}
	delete corpus;
	
	for (std::map<std::string,int>::const_iterator it = frequencies.begin();
	     it != frequencies.end();
	     ++it) {
			cout << it->first << "\t" << it->second << endl;
	     }
	
	ROOTS_TERMINATE();
}
\endcode

And, here is the second version of the function count_last_syllables where the relation is hidden. Like in Perl, this version is shorter while remaining readable.
\code{.cpp}
void count_last_syllables(std::map<std::string,int> &frequencies, roots::Utterance *utt) {
	roots::Sequence *word_seq = utt->get_sequence("Word");
	std::vector<roots::Word*> all_words = word_seq->get_all_items();
	for (std::vector<roots::Word*>::const_iterator it = all_words.begin(); it != all_words.end(); ++it) {
		roots::phonology::Syllable *last_syllable = (*it)->get_related_items("Syllable")
		                                                 ->back()
		                                                 ->to_string();
		if (frequencies.find(last_syllable) == frequencies.end()) {
			frequencies[last_syllable] = 1;
		}
		else {
			frequencies[last_syllable] += 1;
		}
	}
}
\endcode

*/