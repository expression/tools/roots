/*
  This file is part of Roots.

  Roots is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Roots is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with Roots.  If not, see <http://www.gnu.org/licenses/>.

  Copyright (c) 2013, 2014 IRISA
*/

#include "../roots/roots.h"

#define NB_CONCAT 5

int main(int argc, char *argv[]) {
  if (argc == 3) {
    ROOTS_INIT();
    // ROOTS_SET_LOGGER_LEVEL(ROOTS_LOGGER_DEBUG_LEVEL);
    roots::Corpus corpus(argv[1]);
    roots::Corpus corpusTarget;
    int n = corpus.count_utterances();
    // n= 2; // DEBUG
    int j = 1;
    roots::Utterance *curUtt = NULL;
    for (int i = 0; i < n; ++i) {
      std::cout << "************************************" << std::endl
                << "Wokr on utt " << i << "/" << n - 1 << std::endl
                << "************************************" << std::endl;
      // Test concat
      if (j > 1) {
        std::cout << "** Get utt " << i << std::endl << std::endl;
        roots::Utterance *tmpUtt = corpus.get_utterance(i);
        std::cout << "** Concat " << i << std::endl << std::endl;
        curUtt->concatenate_utterance(tmpUtt);
        delete tmpUtt;
        --j;
      } else {
        if (curUtt != NULL) {
          std::cout << "** Add utt before " << i << std::endl << std::endl;
          corpusTarget.add_utterance(*curUtt);
          delete curUtt;
          curUtt = NULL;
        }
        std::cout << "** Get utt " << i << std::endl << std::endl;
        curUtt = corpus.get_utterance(i);
        j = NB_CONCAT;
      }
    }
    if (curUtt != NULL) {
      std::cout << "** Add utt before " << n - 1 << std::endl << std::endl;
      corpusTarget.add_utterance(*curUtt);
      delete curUtt;
    }
    corpusTarget.save(argv[2]);

    roots::Corpus corpus2(argv[2]);
    n = corpus2.count_utterances();
    n = std::min(1, n);  // DEBUG
    for (int i = 0; i < n; ++i) {
      curUtt = corpus2.get_utterance(i);
      std::cout << curUtt->to_string(0) << std::endl;
      delete curUtt;
    }

    ROOTS_TERMINATE();
  } else {
    std::cerr << "ERROR: Expecting 2 paths as arguments: 1 input corpus and 1 "
                 "output corpus."
              << std::endl;
  }
}
