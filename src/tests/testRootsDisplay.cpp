/*
    This file is part of Roots.

    Roots is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Roots is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Roots.  If not, see <http://www.gnu.org/licenses/>.

    Copyright (c) 2013, 2014 IRISA
*/


/*
 * main.cpp
 *
 *  Created on: Aug 14, 2013
 *	Author: zene
 */

#include "../roots/roots.h"
#include <time.h>

#define DEBUG1(x)

bool gen_permutation(int n, std::vector<int> &stack, int &k);
bool compare(const std::pair<int*, std::pair<int, int> > &referenceMatrix, const std::pair<int*, std::pair<int, int> > &crtMatrix, const std::vector<int> &stack);

std::ofstream fout;

int main(int argc, char **argv)
{
  if(argc != 4) {
    //error
    std::cout << "There should be only one argument - the path to one of the (aco, pho, lin, trs) file" << std::endl;
    exit(1);
  }

  std::string inputFile = argv[1];
  char outputFile[250] ="";
  strcat(outputFile, argv[2]); // Output directory
  strcat(outputFile, argv[1] + inputFile.find_last_of('/'));
  fout.open(outputFile);
  std::cout << "Input file: " << inputFile << std::endl;
  std::cout << "Output file: " << outputFile << std::endl;

  if(!endsWith(inputFile,".json")) {
    //error
    std::cout << "Unknown file format";
    exit(1);
  }


  //Utterance *utt = NULL;

  ROOTS_INIT();
  ROOTS_SET_LOGGER_LEVEL(ROOTS_LOGGER_FATAL_LEVEL);

  roots::Corpus corp(inputFile);

  /*
  if(corp.count_utterances() == 0)
   {exit(1);}
  std::vector<std::string> layer;
  layer.push_back("Linguistic Layer");
  layer.push_back("Phonologic Layer");
  layer.push_back("Textual Layer");
  */
  //  Utterance* utt_TMP = corp.get_utterance(layer, atoi(argv[3]));
  roots::Utterance* utt_TMP = corp.get_utterance(atoi(argv[3]));
  roots::Utterance* utt = utt_TMP;

  //  roots::RootsStream *rootsStreamOut = new roots::RootsStreamJson();
  //  utt->deflate(rootsStreamOut);
  //  std::string out = outputFile;
  //  rootsStreamOut->save(out + ".json");
  //std::cout << "*********************\nMy UTT : \n\n" << rootsStreamOut->to_string() << std::endl;



  std::vector<std::string>* sequencesLabels = utt->get_all_sequence_labels();
  std::vector<std::string> sequencesOrder;
  if(true)
    //    if(false)
    { 
      sequencesOrder = *sequencesLabels;

      roots::RootsDisplay display;    
      //  fout << display.to_pgf(*utt, sequencesOrder,0, true);
      fout << display.to_pgf(*utt, sequencesOrder,0, false);
   }
    else
      {
	std::vector<int> sequencesLevel;
	std::vector<std::string> hiddenSeq ;
  

	sequencesOrder.clear();
	sequencesOrder.push_back("Named Entity");
	sequencesLevel.push_back(0);
	sequencesOrder.push_back("Lemma");
	sequencesLevel.push_back(0);
	sequencesOrder.push_back("POS");
	sequencesLevel.push_back(1);
	sequencesOrder.push_back("Syntax");
	sequencesLevel.push_back(1);
	sequencesOrder.push_back("Word");
	sequencesLevel.push_back(0);
	sequencesOrder.push_back("NSS");
	sequencesLevel.push_back(0);
	sequencesOrder.push_back("Syllable");
	sequencesLevel.push_back(0);
	sequencesOrder.push_back("Phoneme");
	sequencesLevel.push_back(0);
	//	sequencesOrder.push_back("Grapheme Text");
	//	sequencesLevel.push_back(0);
   
	roots::RootsDisplay display;    
      //  fout << display.to_pgf(*utt, sequencesOrder,0, true);
	fout << display.to_pgf(*utt, sequencesOrder, hiddenSeq, sequencesLevel, false);
     }

  sequencesLabels->clear();
  delete sequencesLabels;

  delete utt;
  utt=NULL;


  ROOTS_TERMINATE();

  fout.close();

  return 0;
}

bool gen_permutation(int n, std::vector<int> &stack, int &k)
{
  if(k == 0) {
    stack[0] = -1;
  }
  while(k >= 0) {
    bool hasSucc, isValid;
    do{
      hasSucc = (++stack[k] < n);
      isValid = true;
      for(int i = 0; i < k; i++) {
	if(stack[k] ==	stack[i]) {
	  isValid = false;
	  break;
	}
      }
    }
    while(hasSucc && !isValid);
    if(hasSucc) {
      if(k == n - 1) {
	return true;
      }
      else {
	k++;
	stack[k] = -1;
      }
    }
    else {
      k--;
    }
  }
  return false;
}

bool compare(const std::pair<int*, std::pair<int, int> > &referenceMatrix, const std::pair<int*, std::pair<int, int> > &crtMatrix, const std::vector<int> &stack)
{
  DEBUG1(bool equal = true;)

    if(crtMatrix.second.first != referenceMatrix.second.first) {
      std::cout << " WARNING: THe matrixes have different length!" << std::endl;
    }

  int length = crtMatrix.second.first;
  int height = crtMatrix.second.second;
  DEBUG1(int posI; int posJ;)
    for(int i = 0; i < height; i++) {
      for(int j = 0; j < length; j++) {
	if( referenceMatrix.first[ stack[i] * referenceMatrix.second.first + j] != crtMatrix.first[ i * length + j ]) {
	  DEBUG1(equal = false;
		 posI = i;
		 posJ = j;
		 break;)
	    return false;

	}
      }
    }
  DEBUG1(
	 for(int i = 0; i < referenceMatrix.second.second; i++) {
	   for(int j = 0; j < referenceMatrix.second.first; j++) {
	     if(referenceMatrix.first[ stack[i] * referenceMatrix.second.first + j] != NIL) {
	       std::cout << std::setw(4) <<  referenceMatrix.first[ stack[i] * referenceMatrix.second.first + j];
	     }
	     else {
	       std::cout << std::setw(4) << "N";
	     }
	   }
	   std::cout << std::endl;
	 }
	 std::cout << std::endl;

	 for(int i = 0; i < height; i++) {
	   for(int j = 0; j < length; j++) {
	     if(crtMatrix.first[ i * length + j] != NIL) {
	       std::cout <<	std::setw(4) << crtMatrix.first[ i * length + j];
	     }
	     else {
	       std::cout << std::setw(4) << "N";
	     }
	   }
	   std::cout << std::endl;
	 }
	 std::cout << "Matrices are " << (!equal?"not ":"") << "equal";
	 if(!equal) {
	   std::cout << " at position (" << posI << ", " << posJ << ")";
	 }

	 DEBUGGETCH;

	 return equal;
	 )

    return true;
}

