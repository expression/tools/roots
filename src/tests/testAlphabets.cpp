/*
    This file is part of Roots.

    Roots is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Roots is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Roots.  If not, see <http://www.gnu.org/licenses/>.

    Copyright (c) 2013, 2014 IRISA
*/


#include "../roots/roots.h"


int main(int argc, char **argv)
{
//   roots::phonology::ipa::Ipa mon_ipa = new roots::phonology::ipa::Ipa();
//   return 0;
  
  ROOTS_INIT();
  
//   roots::phonology::ipa::LiaphonAlphabet &liaphon = roots::phonology::ipa::LiaphonAlphabet::get_instance();
//   roots::phonology::ipa::BdlexSampaAlphabet &bdlex = roots::phonology::ipa::BdlexSampaAlphabet::get_instance();
  // roots::phonology::ipa::ArpabetAlphabet &arpabet = roots::phonology::ipa::ArpabetAlphabet::get_instance();
  // roots::phonology::ipa::Ipa &ipaAlphabet = roots::phonology::ipa::Ipa();
  roots::phonology::ipa::Ipa ipa;
  
  std::string str;

  if (argc > 1) {
      str = std::string(argv[1]);
  }
  else {
    str = "t‿a";
  }
//   roots::phonology::ipa::Ipa an_ipa = roots::phonology::ipa::Ipa(MANNER_FRICATIVE,	PLACE_DENTAL,			APERTURE_UNDEF,SHAPE_UNDEF,false, true,false,false, std::set<roots::phonology::ipa::Diacritic> ()) ;
//   std::cout << an_ipa.to_string() << std::endl;
//   std::cout << an_ipa.to_string() << std::endl;
    std::cout << "=========" << str << "=========" << std::endl;
//   std::string str = "b";
//   std::vector<roots::phonology::ipa::Ipa *> ipas = bdlex.extract_ipas(str);
//   std::string target_str = arpabet.approximate_ipas(ipas, " ");
//   std::cout << target_str << std::endl;
//   std::vector<roots::phonology::ipa::Ipa *> ipas = ipa.extract_ipas(str);
  std::vector<roots::phonology::ipa::Ipa *> ipas = ipa.extract_ipas(str);
  for (std::vector<roots::phonology::ipa::Ipa *>::iterator it = ipas.begin(); it != ipas.end(); ++it) {
      std::cout << (*it)->to_string(1) << std::endl;
  }
  
//   std::vector<std::string> symbols = arpabet.extract_symbols(str);
//   for (std::vector<std::string>::iterator it = symbols.begin(); it != symbols.end(); ++it) {
//       std::cout << *it << std::endl;
//   }

  ROOTS_TERMINATE();
  
  return 0;
}
