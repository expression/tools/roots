/*
    This file is part of Roots.

    Roots is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Roots is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Roots.  If not, see <http://www.gnu.org/licenses/>.

    Copyright (c) 2013, 2014 IRISA
*/



#include "../roots/roots.h"


int main(int argc, char *argv[]) {
	if (argc == 3) {
	ROOTS_INIT();
	ROOTS_SET_LOGGER_LEVEL(ROOTS_LOGGER_ERROR_LEVEL);
	roots::Corpus *corpus = new roots::Corpus(argv[1]);
	int n = corpus->count_utterances();
	int total_seqs = 0; // Total number of sequences in the corpus
	for (int i = 0; i < n; i++) {
		roots::Utterance *utt = corpus->get_utterance(i);
		std::vector<roots::sequence::Sequence *> *seqs = utt->get_all_sequences();
		for (std::vector<roots::sequence::Sequence *>::const_iterator it = seqs->begin(); it != seqs->end(); ++it) {
			total_seqs++;
		}
		delete seqs;
		delete utt;
	}
    std::cout << total_seqs << " sequences" << std::endl;
	corpus->save(argv[2]);
	delete corpus;
	ROOTS_TERMINATE();
	}
	else {
        std::cerr << "ERROR: Expecting 2 paths as arguments: 1 input corpus and 1 output corpus." << std::endl;
	}
}
