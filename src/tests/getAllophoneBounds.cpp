/*
    This file is part of Roots.

    Roots is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Roots is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Roots.  If not, see <http://www.gnu.org/licenses/>.

    Copyright (c) 2013, 2014 IRISA
*/


#include <boost/program_options.hpp>
#include "../roots/roots.h"

#include <iostream>
#include <fstream>
#include <iomanip>
#include <string>
#include <unicode/regex.h>


#define VERSION_STRING "Version 0.1"

namespace po = boost::program_options;


int main(int argc, char **argv)
{
    std::string inputFile;
    roots::Utterance *utterance = NULL;
    roots::RootsStream *rootsStream = NULL;

    std::string allophoneSequenceLabel = "Allophone";
    std::string segmentSequenceLabel = "Segment";
	
	roots::sequence::AllophoneSequence *allophoneSequence = NULL;
	roots::sequence::SegmentSequence *segmentSequence = NULL;
	roots::Relation *relationAllophoneSegment = NULL;	

	unsigned int allophoneIndex = 0;
	std::vector<int> relatedIndices;
	roots::acoustic::Allophone *allophone=NULL;
	roots::acoustic::SignalSegment *segment=NULL;
    std::string allophoneLabel = "";
	long segmentSampleStart, segmentSampleEnd;
	
	ROOTS_INIT();

	// Declare the supported options.
	po::options_description visibleOptions("Options");
	visibleOptions.add_options()
	    ("version,v", "print version string")
	    ("help", "produce help message")    
	    ("allophoneSequence", po::value< std::string >(), "allophone sequence label")
	    ("segmentSequence", po::value< std::string >(), "segment sequence label")
	    ("binary,b", "load a binary roots file")
    ;

	po::options_description hiddenOptions("Hidden options");
	hiddenOptions.add_options()
    ("input-file", po::value< std::vector<std::string> >(), "input roots file (.xml|.json|.roots)")
    ;   	

	po::options_description cmdline_options;
	cmdline_options.add(visibleOptions).add(hiddenOptions);

	po::positional_options_description p;
	p.add("input-file", -1);

	po::variables_map vm;
	po::store(po::command_line_parser(argc, argv).
			  options(cmdline_options).positional(p).run(), vm);
	po::notify(vm);	

	if (vm.count("help"))
	{
        std::cout << visibleOptions << "\n";
		return 1;
	}
	
	if (vm.count("version"))
	{
        std::cout << VERSION_STRING << "\n";
		return 1;
	}
	
	if(vm.count("allophoneSequence"))
	{
		allophoneSequenceLabel = vm["allophoneSequence"].as<std::string>();
	}
	
	if(vm.count("segmentSequence"))
	{
		segmentSequenceLabel = vm["segmentSequence"].as<std::string>();
	}
	
	if(vm.count("input-file"))
	{
        std::vector<std::string> inputFiles = vm["input-file"].as< std::vector<std::string> >();
		
		if(inputFiles.size()>1)
		{
            std::cerr << "Only one file to process must be mentioned!" << std::endl;
            std::cout << visibleOptions << "\n";
			return 1;
		}
		
		inputFile = inputFiles[0].c_str();
		
		
	}else{
        std::cerr << "One file to process has to be mentioned!" << std::endl;
        std::cout << visibleOptions << "\n";
		return 1;
	}
	
	
	if (vm.count("binary"))
	{
	  //rootsStream = new RootsStreamBinary();
	}
	else
	{
#ifdef USE_STREAM_XML
		if(endsWith(inputFile, ".xml"))
		{
            rootsStream = new roots::RootsStreamXML();
		}
		else
		{
#endif
			if(endsWith(inputFile, ".json"))
			{
                rootsStream = new roots::RootsStreamJson();
			}
			else
			{
                std::cerr << "error: unsupported format of ouput file!" << std::endl;
				return EXIT_FAILURE;
			}
#ifdef USE_STREAM_XML
		}
#endif
	}


    utterance = new roots::Utterance();
	rootsStream->load(inputFile);

	utterance->inflate(rootsStream);

	delete rootsStream;
	rootsStream = NULL;
	
	
	allophoneSequence = utterance->get_sequence(allophoneSequenceLabel)->as<roots::sequence::AllophoneSequence>();
	if(allophoneSequence == NULL)
	{
        std::stringstream serr;
		serr << "Sequence not found (" << allophoneSequenceLabel << ") !";
		exit(EXIT_FAILURE);
	}
	
	segmentSequence = utterance->get_sequence(segmentSequenceLabel)->as<roots::sequence::SegmentSequence>();
	if(segmentSequence == NULL)
	{
        std::stringstream serr;
		serr << "Sequence not found (" << segmentSequenceLabel << ") !";
		exit(EXIT_FAILURE);
	}
	      
	relationAllophoneSegment = utterance->get_relation(allophoneSequenceLabel, segmentSequenceLabel);
	if(relationAllophoneSegment == NULL)
	{
        std::stringstream serr;
		serr << "Relation between <" << allophoneSequenceLabel << "> and <" << segmentSequenceLabel << "> does not exist!\n";
		ROOTS_LOGGER_ERROR(serr.str());
		exit(EXIT_FAILURE);
	}
	
	// parcourir les éléments 
	for(allophoneIndex=0; allophoneIndex<allophoneSequence->count(); allophoneIndex++)
	{
		allophone = static_cast<roots::acoustic::Allophone*> ( allophoneSequence->get_item(allophoneIndex) );
		allophoneLabel = allophone->get_label();
		
		relatedIndices.clear();
		relatedIndices = relationAllophoneSegment->get_related_elements(allophoneIndex, true);
		if(relatedIndices.size() == 0)
		{
            std::stringstream serr;
			serr << "Allophone not related to signal segments!";
			ROOTS_LOGGER_ERROR(serr.str());
			exit(EXIT_FAILURE);
		}
		
		segment = static_cast<roots::acoustic::SignalSegment*> (segmentSequence->get_item(relatedIndices[0]));
		segmentSampleStart = segment->get_segment_start();
		
		segment = static_cast<roots::acoustic::SignalSegment*> (segmentSequence->get_item(relatedIndices[relatedIndices.size() - 1]));
		segmentSampleEnd = segment->get_segment_end();
		
        std::cout << allophoneIndex << ": " << allophoneLabel << " from " << segmentSampleStart << " to " << segmentSampleEnd << std::endl;
	}
	

	delete utterance;
	utterance=NULL;

	ROOTS_TERMINATE();

    return 0;
}
