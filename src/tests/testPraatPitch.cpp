/*
    This file is part of Roots.

    Roots is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Roots is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Roots.  If not, see <http://www.gnu.org/licenses/>.

    Copyright (c) 2013, 2014 IRISA
*/


#include "../roots/roots.h"


int main(int argc __attribute__((unused)), char **argv __attribute__((unused)))
{
	ROOTS_INIT();
	
	if(argc == 1)
	{
			std::cout<< "you must give the name of the PraatPitch file to load!" << std::endl;
			exit(1);
	}
	
	// filename given as first parameter
	roots::file::praat::PraatPitch praatPitchFile(argv[1]);
	
	// load the file
	roots::file::praat::PraatPitchFile *structure = praatPitchFile.load();
	
	// display the structure content
	/*std::cout << "text_type = " << structure->text_type << std::endl;
	std::cout << "xmin = " << structure->xmin << std::endl;
	std::cout << "xmax = " << structure->xmax << std::endl;
	std::cout << "nx = " << structure->nx << std::endl;
	std::cout << "dx = " << structure->dx << std::endl;
	std::cout << "x1 = " << structure->x1 << std::endl;
	std::cout << "ceiling = " << structure->ceiling << std::endl;
	std::cout << "maxnCandidates = " << structure->maxnCandidates << std::endl;
	std::cout << "frames: " << std::endl;
	for(unsigned int itemIndex=0; itemIndex < structure->frames.size(); ++itemIndex)
	{
		std::cout << "  frame[" << itemIndex << "]:" << std::endl;
		std::cout << "    intensity = " << structure->frames[itemIndex]->intensity << std::endl;
		std::cout << "    nCandidates = " << structure->frames[itemIndex]->nCandidates << std::endl;
		
		for(unsigned int elementIndex=0; elementIndex < structure->frames[itemIndex]->candidate.size(); ++elementIndex)
		{
			std::cout << "    candidate[" << elementIndex << "]:" << std::endl;
			std::cout << "      frequency = " << structure->frames[itemIndex]->candidate[elementIndex]->frequency << std::endl;
			std::cout << "      strength = " << structure->frames[itemIndex]->candidate[elementIndex]->strength << std::endl;
		}
	}*/


	roots::file::praat::PraatPitchFile *newstructure = roots::file::praat::PraatPitch::extract_info_between_start_and_end(structure, 0.05, 0.1, true);

	// save it in another file
	praatPitchFile.save(newstructure, "./test.praatpitch", std::locale("en_GB.utf8"));
	
	// delete the structure
	roots::file::praat::PraatPitch::delete_structure(&structure);
	roots::file::praat::PraatPitch::delete_structure(&newstructure);
	
	ROOTS_TERMINATE();

    return 0;
}
