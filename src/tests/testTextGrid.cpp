/*
    This file is part of Roots.

    Roots is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Roots is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Roots.  If not, see <http://www.gnu.org/licenses/>.

    Copyright (c) 2013, 2014 IRISA
*/


#include "../roots/roots.h"


int main(int argc __attribute__((unused)), char **argv __attribute__((unused)))
{
	ROOTS_INIT();
	
	if(argc == 1)
	{
			std::cout<< "you must give the name of the TextGrid file to load!" << std::endl;
			exit(1);
	}
	
	// filename given as first parameter
	roots::file::praat::TextGrid textGridFile(argv[1]);
	
	// load the file
	roots::file::praat::TextGridFile *structure = textGridFile.load();
	
	// display the structure content
	std::cout << "text_type = " << structure->text_type << std::endl;
	std::cout << "xmin = " << structure->xmin << std::endl;
	std::cout << "xmax = " << structure->xmax << std::endl;
	std::cout << "items: " << std::endl;
	for(unsigned int itemIndex=0; itemIndex < structure->items.size(); ++itemIndex)
	{
		std::cout << "  item[" << itemIndex << "]:" << std::endl;
		std::cout << "    classname = " << *(structure->items[itemIndex]->classname) << std::endl;
		std::cout << "    name = " << *(structure->items[itemIndex]->name) << std::endl;
		std::cout << "    xmin = " << structure->items[itemIndex]->xmin << std::endl;
		std::cout << "    xmax = " << structure->items[itemIndex]->xmax << std::endl;	
	}
	
	roots::file::praat::TextGridFile *newstructure = roots::file::praat::TextGrid::extract_info_between_start_and_end(structure, 0.5, 10, true);

	
	// save it in another file
	textGridFile.save(newstructure, "./test.textgrid", std::locale("en_GB.utf8"));
	
	// delete the structure
	roots::file::praat::TextGrid::delete_structure(&structure);
	roots::file::praat::TextGrid::delete_structure(&newstructure);
	
	
	ROOTS_TERMINATE();

    return 0;
}
