/*
    This file is part of Roots.

    Roots is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Roots is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Roots.  If not, see <http://www.gnu.org/licenses/>.

    Copyright (c) 2013, 2014 IRISA
*/


#include "../roots/roots.h"


int main(int argc __attribute__((unused)), char **argv __attribute__((unused)))
{
    const std::string filename = "./test-xml-output2.xml";
	//const char* filename2 = "./test-xml-output3.xml";

	ROOTS_INIT();

	// create objects
	roots::Utterance *r = new roots::Utterance();

        roots::import_htklab<roots::phonology::ipa::LiaphonAlphabet,
                             roots::phonology::nsa::LiaphonNsAlphabet>(
            *r,
            "/commun/bdd/irisa/podalydes/ad/segmentation/automatic/lab/"
            "podalydes_ad_0101_t10_s0.lab",  // Jonathan_chronic_201_ref1.lab"
            roots::Labels());

        roots::sequence::Sequence *segmentSequence = r->get_sequence("NonSpeechSound");//NonSpeechSound Allophone
	std::cout << segmentSequence->to_string() << std::endl;
	
	// make an archive and save to file
    roots::RootsStream *xmlStream = new roots::RootsStreamXML();
	//segmentSequence->deflate(xmlStream);
	r->deflate(xmlStream);

	xmlStream->save(filename);	
	//cout << "SAVING:"<<endl<<xmlStream->toString()<<endl;
    
	// load xml file
    roots::RootsStream *xmlStream2 = new roots::RootsStreamXML();
	xmlStream2->load(filename);
	//cout << "LOADING:"<<endl<<xmlStream2->to_string() << endl;
		
	// load objects from archive
    roots::Utterance *r2 = new roots::Utterance();
	r2->inflate(xmlStream2);
	//roots::sequence::Sequence *seq = NULL;
	
	//seq = roots::sequence::NssSequence::inflate_sequence(*xmlStream2,"nss");
	
	//cout << "Nss Sequence loaded from xml: " << (*seq) << endl;

		
	delete xmlStream;
	delete xmlStream2;
	delete r2;

	ROOTS_TERMINATE();

    return 0;
}
