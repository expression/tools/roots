/*
    This file is part of Roots.

    Roots is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Roots is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Roots.  If not, see <http://www.gnu.org/licenses/>.

    Copyright (c) 2013, 2014 IRISA
*/


#include "../roots/roots.h"

int main(int argc __attribute__((unused)), char **argv __attribute__((unused)))
{
    const std::string f0Filename = "/vrac/bdd/irisa/podalydes/ad/f0/esps/0101/podalydes_ad_0101_t10.f0";
	const std::string xmlFilename = "./test-f0segment.xml";

	ROOTS_INIT();
	
    roots::acoustic::Segment *segment = NULL;
	roots::acoustic::F0Segment *f0segment = NULL;
	
	segment = new roots::acoustic::F0Segment(f0Filename, 0, -1, 100);
	//segment = new roots::acoustic::F0Segment(f0Filename, 0, -1, 44100, 100);
	
	f0segment = segment->as<roots::acoustic::F0Segment>();
	f0segment->set_file_format(roots::file::f0::FILE_FORMAT_F0_WAVESURFER);
	f0segment->open();
	f0segment->load();
	
	std::cout << "the segment is:" << std::endl;
	std::cout << segment->to_string() << std::endl;
	fflush(stdout);

	std::cout << "number of values: " << f0segment->get_n_values_loaded() <<std::endl;
	fflush(stdout);
	
	std::cout << "its values are:" << std::endl;
	
	std::vector<double> values = f0segment->get_values(0,-1);
	int i = 0;
	for (i = 0; i < f0segment->get_n_values_loaded(); i++)
	{
        std::cout << "i\tt="<< f0segment->convert_index_to_time(i) <<" v="<< values[i] << "\n";
	}

	ROOTS_TERMINATE();

    return 0;
}
