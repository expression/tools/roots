/*
    This file is part of Roots.

    Roots is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Roots is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Roots.  If not, see <http://www.gnu.org/licenses/>.

    Copyright (c) 2013, 2014 IRISA
*/


#include "../roots/roots.h"

int main(int argc __attribute__((unused)), char **argv __attribute__((unused)))
{
	ROOTS_INIT();
	
	roots::phonology::prosodicphrase::ProsodicPhrase prosodicPhrase;
	
	std::string inputFile = "/commun/bdd/irisa/podalydes/ad/roots/0101/podalydes_ad_0101_t2_pho.json";
    roots::Utterance *r = NULL;
	roots::sequence::AllophoneSequence *phonemeSeq = NULL;
	
    r = new roots::Utterance();
    roots::RootsStream *rootsStream = new roots::RootsStreamJson();
	rootsStream->load(inputFile);

	r->inflate(rootsStream);
	phonemeSeq = r->get_sequence("Allophone Automatic")->as<roots::sequence::AllophoneSequence>();	
	if(phonemeSeq == NULL)
	{
		ROOTS_LOGGER_ERROR("Allophone sequence not found!");
		exit(1);
	}
	
	prosodicPhrase.set_related_sequence(phonemeSeq);
	ROOTS_LOGGER_INFO("------------------------------------------------------------------------");
	ROOTS_LOGGER_INFO("tests with the empty prosodicphrase: ");
	std::cout << "first_target_index = " << prosodicPhrase.get_first_target_index() << " should be -1 " << std::endl;
	std::cout << "last_target_index = " << prosodicPhrase.get_last_target_index() << " should be -1 " << std::endl;
	std::cout << "phoneme_sequence_id = " << prosodicPhrase.get_phoneme_sequence_id() << " should be " << phonemeSeq->get_id() << std::endl;
	
	
	ROOTS_LOGGER_INFO("------------------------------------------------------------------------");
	ROOTS_LOGGER_INFO("build an hypothetic prosodic phrase: ");
	
	roots::phonology::prosodicphrase::ProsodicPhraseNode *root = prosodicPhrase.get_root_node();
	std::vector<int> indices;
	for(unsigned int i=0;i<phonemeSeq->count();++i)
	{
		indices.push_back(i);
	}
	root->set_phoneme_index_array(indices);
	root->set_label("root");
	root->set_prosodic_group(roots::phonology::prosodicphrase::PROSODIC_GROUP_PHRASE);
	
	indices.clear();
	roots::phonology::prosodicphrase::ProsodicPhraseNode *n1 = new roots::phonology::prosodicphrase::ProsodicPhraseNode();
	for(unsigned int i=0;i<phonemeSeq->count()/2;++i)
	{
		indices.push_back(i);
	}
	n1->set_phoneme_index_array(indices);
	n1->set_label("n1");
	n1->set_prosodic_group(roots::phonology::prosodicphrase::PROSODIC_GROUP_IG);
	
	indices.clear();
	
	
	roots::phonology::prosodicphrase::ProsodicPhraseNode *n2 = new roots::phonology::prosodicphrase::ProsodicPhraseNode();
	for(unsigned int i=phonemeSeq->count()/2;i<phonemeSeq->count();++i)
	{
		indices.push_back(i);
	}
	n2->set_phoneme_index_array(indices);
	n2->set_label("n2");
	n2->set_prosodic_group(roots::phonology::prosodicphrase::PROSODIC_GROUP_IG);
	
	indices.clear();
	
	roots::phonology::prosodicphrase::ProsodicPhraseNode *n3 = new roots::phonology::prosodicphrase::ProsodicPhraseNode();
	for(unsigned int i=0;i<phonemeSeq->count()/4;++i)
	{
		indices.push_back(i);
	}
	n3->set_phoneme_index_array(indices);
	n3->set_label("n3");
	n3->set_prosodic_group(roots::phonology::prosodicphrase::PROSODIC_GROUP_AG);
	
	indices.clear();
	
	roots::phonology::prosodicphrase::ProsodicPhraseNode *n4 = new roots::phonology::prosodicphrase::ProsodicPhraseNode();
	for(unsigned int i=phonemeSeq->count()/4;i<phonemeSeq->count()/2;++i)
	{
		indices.push_back(i);
	}
	n4->set_phoneme_index_array(indices);
	n4->set_label("n4");
	n4->set_prosodic_group(roots::phonology::prosodicphrase::PROSODIC_GROUP_AG);
	
	indices.clear();
	
	roots::phonology::prosodicphrase::ProsodicPhraseNode *n5 = new roots::phonology::prosodicphrase::ProsodicPhraseNode();
	for(unsigned int i=phonemeSeq->count()/2;i<(3*phonemeSeq->count())/4;++i)
	{
		indices.push_back(i);
	}
	n5->set_phoneme_index_array(indices);
	n5->set_label("n5");
	n5->set_prosodic_group(roots::phonology::prosodicphrase::PROSODIC_GROUP_AG);
	
	indices.clear();
	
	roots::phonology::prosodicphrase::ProsodicPhraseNode *n6 = new roots::phonology::prosodicphrase::ProsodicPhraseNode();
	for(unsigned int i=(3*phonemeSeq->count())/4;i<phonemeSeq->count();++i)
	{
		indices.push_back(i);
	}
	n6->set_phoneme_index_array(indices);
	n6->set_label("n6");
	n6->set_prosodic_group(roots::phonology::prosodicphrase::PROSODIC_GROUP_AG);
	
	indices.clear();
	
	
	root->add_child(n1);
	n1->add_child(n3);
	n1->add_child(n4);
	root->add_child(n2);
	n2->add_child(n5);
	n2->add_child(n6);
	
	std::cout << prosodicPhrase.to_string() << std::endl;
	ROOTS_LOGGER_INFO("------------------------------------------------------------------------");
	ROOTS_LOGGER_INFO("some more tests: ");
	/*std::RootsStream *stream = new std::RootsStreamJson();
		phonemeSeq->deflate(*stream);
	prosodicPhrase.deflate(*stream);
	std::cout << stream->toString() << std::endl;
	
	roots::phonology::prosodicphrase::ProsodicPhrase prosodicPhrase2;
	roots::sequence::PhonemeSequence phonemeSeq2;

	phonemeSeq2.inflate(*stream, "phoneme");
	prosodicPhrase2.inflate(*stream);
	std::cout << prosodicPhrase2.to_string() << std::endl;

	delete stream;
	stream = NULL;
	
	*/
	ROOTS_LOGGER_INFO("------------------------------------------------------------------------");
	ROOTS_LOGGER_INFO("get_phoneme_index_array:");
	std::vector<int> vec = root->get_phoneme_index_array();
	for(unsigned int idxVec=0; idxVec < vec.size(); ++idxVec)
	{
		std::cout << vec[idxVec] << ", ";
	}
	std::cout << std::endl;

	//ROOTS_LOGGER_INFO("------------------------------------------------------------------------");
	//ROOTS_LOGGER_INFO("a simple tree: output from the rootNode");
	//root->set_label("root");
	//c1->set_label("c1");
	//c2->set_label("c2");
	//c3->set_label("c3");
	//c4->set_label("c4");
	//root->add_child(c1,0);
	//root->add_child(c2,1);
	//c1->add_child(c3);
	//c1->add_child(c4);
	//std::cout << root->output_dot() << std::endl;
	//ROOTS_LOGGER_INFO("------------------------------------------------------------------------");
	
	//ROOTS_LOGGER_INFO("a simple tree: output from the tree itself");
	//std::cout << t.to_string() << std::endl;
	//ROOTS_LOGGER_INFO("------------------------------------------------------------------------");

	//ROOTS_LOGGER_INFO("get leaves:");
	//std::vector<roots::tree::TreeNode*> path = t.get_leaf_nodes();
	//for(std::vector<roots::tree::TreeNode*>::iterator iter=path.begin(); iter!=path.end(); ++iter)
	//{
		//std::cout << (*iter)->to_string() << std::endl;
	//}
	//ROOTS_LOGGER_INFO("------------------------------------------------------------------------");
	
	//ROOTS_LOGGER_INFO("remove node c1");
	//root->remove_child(0);
	//std::cout << t.to_string() << std::endl;
	//ROOTS_LOGGER_INFO("------------------------------------------------------------------------");
	
	//ROOTS_LOGGER_INFO("add node c1 as child of c2");
	//c1 = new roots::tree::TreeNode();
	//c1->set_label("new c1");
	//c2->add_child(c1);
	//std::cout << t.to_string() << std::endl;
	//ROOTS_LOGGER_INFO("------------------------------------------------------------------------");
	
	//ROOTS_LOGGER_INFO("remove node c2 and subtree");
	//root->remove_child_and_subtree(2);
	//std::cout << t.to_string() << std::endl;
	//ROOTS_LOGGER_INFO("------------------------------------------------------------------------");
	
	ROOTS_TERMINATE();

    return 0;
}
