/*
    This file is part of Roots.

    Roots is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Roots is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Roots.  If not, see <http://www.gnu.org/licenses/>.

    Copyright (c) 2013, 2014 IRISA
*/

#include "../roots/roots.h"


int main() {
  ROOTS_INIT();
  ROOTS_SET_LOGGER_LEVEL(ROOTS_LOGGER_ERROR_LEVEL);
  
  std::vector<char> str1 = boost::assign::list_of('a')('b')('c')('d');
  std::vector<char> str2 = boost::assign::list_of('d')('b')('f');
  roots::align::Levenshtein levenshtein(str1, str2);
  levenshtein.align();
  std::cout << "Edit distance = " << levenshtein.get_edit_distance() << std::endl;
  std::cout << "--" << std::endl;
  std::cout << levenshtein.to_string() << std::endl;
  
  roots::phonology::ipa::ArpabetAlphabet & src_alph = roots::phonology::ipa::ArpabetAlphabet::get_instance();
  roots::phonology::ipa::LiaphonAlphabet & tgt_alph = roots::phonology::ipa::LiaphonAlphabet::get_instance();
  
  std::vector<std::string> ph_str = boost::assign::list_of("ay")("ah")("ay")("ahw")("th")("ih")("n")("k")("ey");
  roots::sequence::PhonemeSequence *src_seq = new roots::sequence::PhonemeSequence((std::string) "source");
  roots::sequence::PhonemeSequence *tgt_seq = new roots::sequence::PhonemeSequence((std::string) "target");
  std::vector<roots::phonology::ipa::Ipa *> ipas;
  std::vector<roots::Base *>src_vec;
  std::vector<roots::Base *>tgt_vec;
  
  ipas = tgt_alph.extract_ipas("zz");
  size_t n = ipas.size();
  for (size_t i = 0; i < n; i++) {
      tgt_seq->add(new roots::phonology::Phoneme(*(ipas[i])));
    tgt_vec.push_back(tgt_seq->get_item(tgt_seq->count()-1));
  }
  for (std::vector<std::string>::iterator it = ph_str.begin(); it != ph_str.end(); ++it) {
    ipas = src_alph.extract_ipas(*it);
    src_seq->add(new roots::phonology::Phoneme(ipas));
    src_vec.push_back(src_seq->get_item(src_seq->count()-1));
    std::string tgt_str = tgt_alph.approximate_ipas(ipas);
    ipas = tgt_alph.extract_ipas(tgt_str);
    n = ipas.size();
    for (size_t i = 0; i < n; i++) {
        tgt_seq->add(new roots::phonology::Phoneme(*(ipas[i])));
      tgt_vec.push_back(tgt_seq->get_item(tgt_seq->count()-1));
    }
  }
  std::cout << src_seq->to_string() << std::endl;
  std::cout << tgt_seq->to_string() << std::endl;
  
  std::cout << "Dissimilarity = " << src_seq->get_item(0)->compute_dissimilarity(tgt_seq->get_item(0)) << std::endl;
  
  roots::align::Alignment alignment(src_vec, tgt_vec);
  alignment.align();
  std::cout << "Edit distance = " << alignment.get_edit_distance() << std::endl;
  std::cout << "--" << std::endl;
  std::cout << alignment.to_string() << std::endl;
  
  roots::Relation *rel = src_seq->align_1_to_1(tgt_seq);
  std::cout << rel->to_string(2) << std::endl;
  
  roots::Relation *rel2 = src_seq->align_m_to_n(tgt_seq);
  std::cout << rel2->to_string(2) << std::endl;
  
  src_seq->destroy();
  tgt_seq->destroy();

  ROOTS_TERMINATE();
}
