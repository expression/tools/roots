/*
    This file is part of Roots.

    Roots is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Roots is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Roots.  If not, see <http://www.gnu.org/licenses/>.

    Copyright (c) 2013, 2014 IRISA
*/


#include "../roots/roots.h"


int main(int argc __attribute__((unused)), char **argv __attribute__((unused)))
{
	ROOTS_INIT();
	
// 	const char *xmlFilename = "./testPhonemeBlock.xml";
	
	roots::phonology::PhonemeBlock phonemeBlock1;
	roots::phonology::PhonemeBlock phonemeBlock2;
	roots::phonology::PhonemeBlock phonemeBlock3;
	
	ROOTS_LOGGER_INFO("------------------------------------------------------------------------");
	
	
// 	cout << "\u0251" << endl;
// 	cout << "\u0251\u0303" << endl;
	
    roots::phonology::ipa::BdlexSampaAlphabet &alph = roots::phonology::ipa::BdlexSampaAlphabet::get_instance();
	
	std::string str("a~");
    roots::phonology::ipa::Ipa *ipa = alph.extract_ipas(str).front();
    roots::RootsStreamJson stream;
    std::cout << ipa->to_string() << std::endl;
	ipa->from_unicode();
	ipa->deflate(&stream,true);
    std::cout << stream.to_string() << std::endl;
	
// 	std::vector< roots::phonology::ipa::Ipa * > variant;
// 	std::vector< bool > optional;
// 	std::vector<int> weights;
// 	
// 	std::list<std::string> input;
// 	input.push_back("ll");
// 	input.push_back("ee");
// 	input.push_back("eu");
// 	
// 	// FIRST PHONEME BLOCK
// 	std::vector<roots::phonology::ipa::Ipa*> new_ipas;
// 	new_ipas = alpha.extract_ipas(input.front());
// 	input.pop_front();
// 	for (std::vector<roots::phonology::ipa::Ipa*>::iterator it = new_ipas.begin();
// 		 it != new_ipas.end();
// 		 ++it
// 	) {
// 		variant.push_back(*it);
// 	}
// 
// 	new_ipas = alpha.extract_ipas(input.front());
// 	input.pop_front();
// 	for (std::vector<roots::phonology::ipa::Ipa*>::iterator it = new_ipas.begin();
// 		 it != new_ipas.end();
// 		 ++it
// 	) {
// 		variant.push_back(*it);
// 	}
// 
// 	
// 	optional.push_back(false);
// 	optional.push_back(false);
// 		
// 	//phonemeBlockVariant11.set_phoneme_variant(variant, optional);
// 		
// 	delete variant[1];
// 	new_ipas = alpha.extract_ipas(input.front());
// 	input.pop_front();
// 	variant[1] = new_ipas.front();
// 	
// // 	delete variant[0];
// // 	delete variant[1];
// // 	variant.clear();
// // 	optional.clear();
// // 	// FIRST PHONEME BLOCK END		
// // 			
// // 	// SECOND PHONEME BLOCK
// // 	variant.push_back(new roots::phonology::ipa::IpaFR(roots::phonology::ipa::LiaphonAlphabet::get_instance(), "pp"));
// // 	variant.push_back(new roots::phonology::ipa::IpaFR(roots::phonology::ipa::LiaphonAlphabet::get_instance(), "ee"));
// // 	variant.push_back(new roots::phonology::ipa::IpaFR(roots::phonology::ipa::LiaphonAlphabet::get_instance(), "tt"));
// // 	variant.push_back(new roots::phonology::ipa::IpaFR(roots::phonology::ipa::LiaphonAlphabet::get_instance(), "ii"));
// // 	optional.push_back(false);
// // 	optional.push_back(false);
// // 	optional.push_back(false);
// // 	optional.push_back(false);
// // 	
// // 	//phonemeBlock2.add_phoneme_variant(phonemeBlockVariant2);		
// // 	
// // 	delete variant[0];
// // 	delete variant[1];
// // 	delete variant[2];
// // 	delete variant[3];
// // 	variant.clear();
// // 	optional.clear();
// // 	// SECOND PHONEME BLOCK END
// // 	
// // 	// THIRD PHONEME BLOCK
// // 	variant.push_back(new roots::phonology::ipa::IpaFR(roots::phonology::ipa::LiaphonAlphabet::get_instance(), "ch"));
// // 	variant.push_back(new roots::phonology::ipa::IpaFR(roots::phonology::ipa::LiaphonAlphabet::get_instance(), "aa"));
// // 	
// // 	optional.push_back(false);
// // 	optional.push_back(false);
// // 	
// // 	//phonemeBlockVariant3.set_phoneme_variant(variant, optional);	
// // 	//phonemeBlock3.add_phoneme_variant(phonemeBlockVariant3);		
// // 			
// // 	delete variant[0];
// // 	delete variant[1];
// // 	variant.clear();
// // 	optional.clear();
// // 	// THIRD PHONEME BLOCK END
// // 	
// // 	// FILL PHONEME BLOCK SEQUENCE
// // 	phonemeBlockSequence->add(&phonemeBlock1);
// // 	phonemeBlockSequence->add(&phonemeBlock2);
// // 	phonemeBlockSequence->add(&phonemeBlock3);
// // 
// // 	for(size_t i=0; i<phonemeBlockSequence->count(); i++)
// // 	  std::cout << phonemeBlockSequence->get_item(i)->to_string() << std::endl;
// // 
// // 	{
// // 		ROOTS_LOGGER_INFO("------------------------------------------------------------------------");
// // 		ROOTS_LOGGER_INFO("------- deflate to xmlstream : -----------------------------------------");
// // 		/*		
// // 		roots::RootsStream *xmlStream = new roots::RootsStreamXML();
// // 		phonemeBlockSequence.deflate(*xmlStream);
// // 		std::cout << xmlStream->toString() << std::endl;
// // 		
// // 		xmlStream->save(xmlFilename);		
// // 		*/
// // 		ROOTS_LOGGER_INFO("------------------------------------------------------------------------");
// // 		//delete xmlStream;
// // 		//xmlStream = NULL;
// // 	}
// 	phonemeBlockSequence->destroy();
// 
// // 	{
// // 		ROOTS_LOGGER_INFO("------------------------------------------------------------------------");
// // 		ROOTS_LOGGER_INFO("------- inflate from xmlstream : ---------------------------------------");
// // 		
// // 		roots::sequence::PhonemeBlockSequence * phonemeBlockSequence2 = 
// // 		  new roots::sequence::PhonemeBlockSequence();		
// // 		roots::RootsStream *xmlStream = new roots::RootsStreamXML();
// // 		xmlStream->load(xmlFilename);		
// // 		phonemeBlockSequence2->inflate(xmlStream);
// // 		
// // 		for(size_t i=0; i<phonemeBlockSequence2->count(); i++)
// // 			std::cout << phonemeBlockSequence2->get_item(i)->to_string() << std::endl;
// // 
// // 		
// // 		ROOTS_LOGGER_INFO("------------------------------------------------------------------------");
// // 		delete xmlStream;
// // 		xmlStream = NULL;
// // 		phonemeBlockSequence2->destroy();
// // 	}
	
	ROOTS_TERMINATE();

    return 0;
}
