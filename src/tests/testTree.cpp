/*
    This file is part of Roots.

    Roots is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Roots is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Roots.  If not, see <http://www.gnu.org/licenses/>.

    Copyright (c) 2013, 2014 IRISA
*/


#include "../roots/roots.h"


int main(int argc __attribute__((unused)), char **argv __attribute__((unused)))
{
	ROOTS_INIT();
	
	roots::tree::Tree<roots::tree::TreeNode> t;
	
	roots::tree::TreeNode *root = t.get_root_node();
	roots::tree::TreeNode *c1 = new roots::tree::TreeNode();
	roots::tree::TreeNode *c2 = new roots::tree::TreeNode();
	roots::tree::TreeNode *c3 = new roots::tree::TreeNode();
	roots::tree::TreeNode *c4 = new roots::tree::TreeNode();

	ROOTS_LOGGER_INFO("------------------------------------------------------------------------");
	ROOTS_LOGGER_INFO("a simple tree: output from the rootNode");
	root->set_label("root");
	c1->set_label("c1");
	c2->set_label("c2");
	c3->set_label("c3");
	c4->set_label("c4");
	root->add_child(c1,0);
	root->add_child(c2,1);
	c1->add_child(c3);
	c1->add_child(c4);
    std::cout << root->output_dot() << std::endl;
	ROOTS_LOGGER_INFO("------------------------------------------------------------------------");
	
	ROOTS_LOGGER_INFO("a simple tree: output from the tree itself");
    std::cout << t.to_string() << std::endl;
	ROOTS_LOGGER_INFO("------------------------------------------------------------------------");

	ROOTS_LOGGER_INFO("get leaves:");
	std::vector<roots::tree::TreeNode*> path = t.get_leaf_nodes();
	for(std::vector<roots::tree::TreeNode*>::iterator iter=path.begin(); iter!=path.end(); ++iter)
	{
		std::cout << (*iter)->to_string() << std::endl;
	}
	ROOTS_LOGGER_INFO("------------------------------------------------------------------------");
	
	ROOTS_LOGGER_INFO("remove node c1");
	root->remove_child(0);
	std::cout << t.to_string() << std::endl;
	ROOTS_LOGGER_INFO("------------------------------------------------------------------------");
	
	ROOTS_LOGGER_INFO("add node c1 as child of c2");
	c1 = new roots::tree::TreeNode();
	c1->set_label("new c1");
	c2->add_child(c1);
	std::cout << t.to_string() << std::endl;
	ROOTS_LOGGER_INFO("------------------------------------------------------------------------");
	
	ROOTS_LOGGER_INFO("remove node c2 and subtree");
	root->remove_child_and_subtree(2);
	std::cout << t.to_string() << std::endl;
	ROOTS_LOGGER_INFO("------------------------------------------------------------------------");
	
	
	ROOTS_TERMINATE();

    return 0;
}
