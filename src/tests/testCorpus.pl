#!/usr/bin/perl
#
# Test script for corpus and other chunks
#

BEGIN {
	use File::Basename;
	use Cwd 'abs_path';
	our $roots = abs_path(dirname(__FILE__));
	$roots =~ s/^(.*\/roots-cpp\/).*/$1/;
	unless ($ENV{LD_LIBRARY_PATH} =~ m#roots-cpp#) {
		$ENV{LD_LIBRARY_PATH} = $ENV{LD_LIBRARY_PATH}.":$roots/src/.libs:$roots/src/swig-wrapper/perl-wrapper";
		exec($^X, $0, @ARGV);
		exit;
	}
}


use lib "$roots/src/swig-wrapper/perl-wrapper";

# use lib dirname( __FILE__ )."/../../../perl";
# use lib dirname( __FILE__ )."/../../../corprep-pl";

use strict;
use utf8;
# 
# use Switch;
# use Error qw(:try);
use Data::Dumper;
# use IO::File;
# use File::Path;
# 
use roots;

sub print_banner {
	my $i = shift;
	print <<END;

###############################################################
Test $i
###############################################################

END
}

sub save_and_print {
	my $corpus = shift;
	my $istream = new roots::RootsStreamJson();
	my $ostream = new roots::RootsStreamJson();
	my $f = shift;
	$corpus->deflate($ostream, 0, 0);
	$ostream->save($f);
	$istream->load($f);
	$corpus->inflate($istream, 0, 0);
	for (my $i = 0; $i < $corpus->count_utterances(); $i++) {
		print "Utterance $i\n";
		print $corpus->get_utterance($i)->to_string()."\n";
	}
	$ostream->clear();
	$corpus->deflate($ostream);
	print $ostream->to_string()."\n";
}

my $f = shift(@ARGV);

# my $corpus = new roots::MemoryChunk();
my $corpus = new roots::Corpus();



# Utterance 1
my $utt = new roots::Utterance();
my $ws = new roots::WordSequence();
$ws->set_label("Word 1");

foreach my $str ("ROOTS", "marche") {
	my $w = new roots::linguistic_Word($str);
	$ws->add($w);
}

$utt->add_sequence($ws);

# Utterance 2
my $utt2 = new roots::Utterance();
my $ws2 = new roots::WordSequence();
$ws2->set_label("Word 2");

foreach my $str ("en", "Perl") {
	my $w = new roots::linguistic_Word($str);
	$ws2->add($w);
}

$utt2->add_sequence($ws2);

# Utterance 3
my $utt3 = new roots::Utterance();
my $ws3 = new roots::WordSequence();
$ws3->set_label("Word 3");

foreach my $str ("ça", "marche") {
	my $w = new roots::linguistic_Word($str);
	$ws3->add($w);
}

$utt3->add_sequence($ws3);

# Utterance 4
my $utt4 = new roots::Utterance();
my $ws4 = new roots::WordSequence();
$ws4->set_label("Word 4");

foreach my $str ("même", "bien") {
	my $w = new roots::linguistic_Word($str);
	$ws4->add($w);
}

$utt4->add_sequence($ws4);

###############################################################
# Add one utterance
###############################################################

print_banner(1);
$corpus->add_utterance($utt);
$corpus->add_utterance($utt2);
save_and_print($corpus, $f);
$corpus->clear();

###############################################################

print_banner(2);
$corpus->add_utterance($utt, "TOTO");
$corpus->add_utterance($utt2, "TOTO");
save_and_print($corpus, $f);
$corpus->clear();

###############################################################

print_banner(3);
my %map = ("foo" => $utt3, "bar" => $utt4);
$corpus->add_utterance(\%map);
save_and_print($corpus, $f);
$corpus->clear();

###############################################################
# Add several utterances at once
###############################################################

print_banner(4);
my @array = ($utt, $utt2);
my @array2 = ($utt3, $utt4);
$corpus->add_utterances(\@array);
$corpus->add_utterances(\@array2);
$corpus->update_utterance(0, $utt2);
save_and_print($corpus, $f);
$corpus->clear();

###############################################################

print_banner(5);
my @array = ($utt, $utt2);
my @array2 = ($utt3, $utt4);
my %map = ("foo" => \@array, "bar" => \@array2);
$corpus->add_utterances(\%map);
my %modif = ("bar" => $utt3, "foo" => $utt4);
$corpus->update_utterance(0, \%modif);
save_and_print($corpus, $f);
$corpus->clear();

###############################################################
