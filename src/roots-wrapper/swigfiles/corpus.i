//------------------------------------------------------------------------------
//--roots::BaseChunk---------------------------------------------------
//------------------------------------------------------------------------------

%include "roots/BaseChunk.h"

//------------------------------------------------------------------------------
//--roots::MemoryChunk---------------------------------------------------
//------------------------------------------------------------------------------

%include "roots/MemoryChunk.h"


//------------------------------------------------------------------------------
//--roots::Corpus---------------------------------------------------------
//------------------------------------------------------------------------------

%feature("notabstract") Corpus;
%include "roots/Corpus.h"



//------------------------------------------------------------------------------
//--roots::FileChunk---------------------------------------------------
//------------------------------------------------------------------------------

%include "roots/FileChunk.h"
