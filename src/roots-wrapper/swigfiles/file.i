//------------------------------------------------------------------------------
//--roots::file::F0----------------------------------------------------
//------------------------------------------------------------------------------
%rename(file_F0) roots::file::f0::F0;
%rename(file_f0_file_format_t) roots::file::f0::file_format_t;
%rename(file_f0_f0_unit_t) roots::file::f0::f0_unit_t;

%include "roots/File/F0.h"

%extend roots::file::f0::F0
{
    
}

//------------------------------------------------------------------------------
//--roots::file::pitchmark::Pitchmark----------------------------------------------------
//------------------------------------------------------------------------------
%rename(file_Pitchmark) roots::file::pitchmark::Pitchmark;
%rename(file_pitchmark_file_format_t) roots::file::pitchmark::file_format_t;


%include "roots/File/Pitchmark.h"

%extend roots::file::pitchmark::Pitchmark
{
    
}

//------------------------------------------------------------------------------
//--roots::file::praat::PraatPitch----------------------------------------------------
//------------------------------------------------------------------------------
%rename(file_PraatPitch) roots::file::praat::PraatPitch;
%rename(file_struct_PraatPitchFile) roots::file::praat::PraatPitchFile;
%rename(file_struct_PraatPitchFrame) roots::file::praat::PraatPitchFrame;
%rename(file_struct_PraatPitchCandidate) roots::file::praat::PraatPitchCandidate;

%include "roots/File/PraatPitch.h"

%extend roots::file::praat::PraatPitch
{
    
}

//------------------------------------------------------------------------------
//--roots::file::praat::TextGrid----------------------------------------------------
//------------------------------------------------------------------------------
%rename(file_TextGrid) roots::file::praat::TextGrid;

%include "roots/File/TextGrid.h"

%extend roots::file::praat::TextGrid
{
    
}

//------------------------------------------------------------------------------
//--roots::file::audio::Audio----------------------------------------------------
//------------------------------------------------------------------------------
%rename(file_Audio) roots::file::audio::Audio;
%rename(file_audio_file_format_t) roots::file::audio::file_format_t;

%include "roots/File/Audio.h"

%extend roots::file::audio::Audio
{
    
}

//------------------------------------------------------------------------------
//--roots::file::htk::HTK----------------------------------------------------
//------------------------------------------------------------------------------
%rename(file_HTK) roots::file::htk::HTK;
%rename(file_htk_parameter_kind_t) roots::file::htk::htk_parameter_kind_t;

%include "roots/File/HTK.h"

%extend roots::file::htk::HTK
{
    
}

//------------------------------------------------------------------------------
//--roots::file::htk::HTKLab----------------------------------------------------
//------------------------------------------------------------------------------
%rename(file_HTKLab) roots::file::htk::HTKLab;

%include "roots/File/HTKLab.h"

%extend roots::file::htk::HTKLab
{
    
}



