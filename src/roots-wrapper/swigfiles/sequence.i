//------------------------------------------------------------------------------
//--roots::Sequence-------------------------------------------------------------
//------------------------------------------------------------------------------


%include "roots/Sequence.h"

%include "roots/Sequence/TemplateSequence.h"

/*
 * Macro to patch the import of static methods from TemplateSequence
 * sequence_classname: type of the sequence
 * item_namespace: C++ namespace of the templated class
 * item_classname: type of the templated class
 */
%define DEFINE_SEQUENCE_CLASS(sequence_classname,item_namespace,item_classname)
//------------------------------------------------------------------------------
//--roots::Sequence:: sequence_classname -------------------------------------------------------------
//------------------------------------------------------------------------------

%template(T ## sequence_classname) roots::sequence::TemplateSequence<roots::item_namespace ## item_classname>;

%include "roots/Sequence/sequence_classname.h"

%rename("%(lowercase)s") roots::sequence::Sequence::as_ ## item_classname ## _sequence();

%extend roots::sequence::Sequence
{
	virtual roots::sequence::sequence_classname* as_ ## item_classname ## _sequence() {
		return $self->as<roots::sequence::sequence_classname>();
	}
}

%extend roots::item_namespace ## item_classname
{
	virtual roots::item_namespace ## item_classname* as_ ## item_classname() {
		return $self->as<roots::item_namespace ## item_classname>();
	}
}

%extend roots::sequence:: ## sequence_classname
{
	static std::string content_type()
	{
		return roots::sequence::TemplateSequence<roots::item_namespace ## item_classname>::content_type();
	}
	
	static std::string xml_tag_name()
	{
		return roots::sequence::TemplateSequence<roots::item_namespace ## item_classname>::xml_tag_name();
	}
	
	static std::string pgf_display_color()
	{
		return roots::sequence::TemplateSequence<roots::item_namespace ## item_classname>::pgf_display_color();
	}
}

%enddef


DEFINE_SEQUENCE_CLASS(SyntaxSequence, 	linguistic::syntax::, Syntax)
DEFINE_SEQUENCE_CLASS(NamedEntitySequence, 	linguistic::namedentity::, NamedEntity)
DEFINE_SEQUENCE_CLASS(PosSequence, 		linguistic::pos::, POS)
DEFINE_SEQUENCE_CLASS(WordSequence, 		linguistic::, Word)
DEFINE_SEQUENCE_CLASS(GraphemeSequence, 	linguistic::, Grapheme)
DEFINE_SEQUENCE_CLASS(FillerSequence, 	linguistic::filler::, Filler)

DEFINE_SEQUENCE_CLASS(PhonemeBlockSequence, 	phonology::, PhonemeBlock)
DEFINE_SEQUENCE_CLASS(SyllableSequence, 	phonology::syllable::, Syllable)
DEFINE_SEQUENCE_CLASS(PhonemeSequence, 	phonology::, Phoneme)
DEFINE_SEQUENCE_CLASS(ProsodicPhraseSequence, phonology::prosodicphrase::, ProsodicPhrase)

DEFINE_SEQUENCE_CLASS(SegmentSequence, 	acoustic::, Segment)
DEFINE_SEQUENCE_CLASS(AllophoneSequence, 	acoustic::, Allophone)
DEFINE_SEQUENCE_CLASS(NssSequence, 		acoustic::, NonSpeechSound)
DEFINE_SEQUENCE_CLASS(AtomSequence, 	acoustic::, Atom)

DEFINE_SEQUENCE_CLASS(VoidSequence, 	 	          , Void)
DEFINE_SEQUENCE_CLASS(SymbolSequence, 	 	          , Symbol)
DEFINE_SEQUENCE_CLASS(NumberSequence, 	 	          , Number)
DEFINE_SEQUENCE_CLASS(EmbeddingSequence, 	 	          , Embedding)


%extend roots::sequence::Sequence {
     %template(as_character_sequence)    as<roots::sequence::GraphemeSequence>;
};
