//------------------------------------------------------------------------------
//--roots::Utterance------------------------------------------------------------
//------------------------------------------------------------------------------

%rename(Utterance) roots::Utterance;

%include "roots/Utterance.h"

%ignore import_seglab;
%ignore import_syllable;
%ignore import_htklab;

/* ------------------------------------------------------------------------- */

%extend roots::Utterance
{
  std::string roots::Utteranceto_pgf(const std::vector<std::string> &seqLabels, const int level=0, const bool draw_relations=false)
  {
    std::vector<std::string> localSequencesOrder;
    std::vector<std::string>::const_iterator iter;

    for (iter = seqLabels.begin(); iter != seqLabels.end(); iter++)
      {
	localSequencesOrder.push_back(*iter);
      }

    return $self->to_pgf(localSequencesOrder,level,draw_relations);
  }

  std::string roots::Utteranceto_pgf(
		       const std::vector<std::string> & seqLabels,
		       const std::vector<std::string> & hiddensSeqLabels,
		       const std::vector<int> & displayLevels,
		       bool draw_relations=false)
  {
    std::string str;
    std::vector<std::string> localSequencesOrder;
    std::vector<std::string> localHiddensSeqLabels;

    std::vector<std::string>::const_iterator iter;
    for (iter = seqLabels.begin();
	 iter != seqLabels.end();
	 iter++)
      { localSequencesOrder.push_back(*iter); }
    for (iter = hiddensSeqLabels.begin();
	 iter != hiddensSeqLabels.end();
	 iter++)
      { localHiddensSeqLabels.push_back(*iter); }

    RootsDisplay rd;
    return rd.to_pgf(*$self,
					 localSequencesOrder,
					 localHiddensSeqLabels,
					 displayLevels,
					 draw_relations);
  }
}

%include "roots/UtteranceHelper.h"
