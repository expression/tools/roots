%ignore boost::hash<icu::UnicodeString>;
%include "roots/common.h"


%include "roots/roots.h"

namespace roots
{
	class Relation;
	class BaseItem;
	
	namespace sequence
	{
		class Sequence;
	}
}


namespace std 
{
	%template(vector_bool) std::vector<bool>;
	%template(vector_int) std::vector<int>;
	%template(vector_uint) std::vector<unsigned int>;
	%template(vector_double) std::vector<double>;
	%template(vector_float) std::vector<float>;
	%template(vector_uchar) std::vector<UChar>;
	%template(vector_string) std::vector<std::string>;
	%template(map_string_string) std::map<std::string, std::string >;
	%template(map_string_vector_string) std::map<std::string, std::vector<std::string> >;

	%template(vector_unistr) std::vector<UnicodeString>;
	%template(map_unistr_unistr) std::map<UnicodeString, UnicodeString >;
	%template(map_unistr_vectorunistr) std::map<UnicodeString, std::vector<UnicodeString> >;
 }


%define PRINT_CLASS_COMMENT(str)
//------------------------------------------------------------------------------
//--str-----------------------------------------------------------------
//------------------------------------------------------------------------------
%enddef
 
%define RENAME_CLASS(from,to)
	%rename(to) from;
%enddef
 
// 	%rename("cpp_namespace"##"_%s") roots::cpp_namespace::classname;
// 	%rename("command:perl ./rename-class.pl <<<") cpp_namespace::classname;
// 	%rename("%(regex:/::/_/g)s") vector_p_ ## cpp_namespace ## _ ## classname;
// 	%rename("%(regex:/::/_/g)s") cpp_namespace::classname;
 

%define DEFINE_CLASS(classname,header_dir,cpp_namespace,swig_namespace...)
	PRINT_CLASS_COMMENT(cpp_namespace::classname)
#if ("swig_namespace" != "")
	RENAME_CLASS(cpp_namespace::classname,swig_namespace ## _ ## classname)
#endif
	%include header_dir/classname.h
%enddef

%define DEFINE_ITEM_CLASS(classname,header_dir,cpp_namespace,swig_namespace...)
	PRINT_CLASS_COMMENT(cpp_namespace::classname)
#if ("swig_namespace" != "")	
	RENAME_CLASS(cpp_namespace::classname,swig_namespace ## _ ## classname);	
#endif	
	#if SWIGPERL == 1
	//#warning WE ARE IN PERL
 	DEFINE_VECTOR_P_ITEM(classname,cpp_namespace,swig_namespace)
	#endif

	%include header_dir/classname.h
	%extend roots::BaseItem
	{
#if ("swig_namespace" != "")
		virtual cpp_namespace::classname* as_ ## swig_namespace ## _ ## classname() {
#else
		virtual cpp_namespace::classname* as_ ## classname() {
#endif
			return $self->as<cpp_namespace::classname>();
		}
	}

	#if SWIGPYTHON == 1
	//#warning WE ARE IN PYTHON
	       DEFINE_VECTOR_P_ITEM(classname,cpp_namespace,swig_namespace)
	#endif
%enddef



//------------------------------------------------------------------------------
// General classes
// Warning: Position of the include instructions should be consistent with class dependencies,
// ie, class A should not be included after classes B if class B uses instances of class A
//------------------------------------------------------------------------------


%include "roots/Align.h"
		 
namespace roots {
  %template(align_char_t) Align<char>;
  %template(align_uchar_t) Align<UChar>;
		 
}
%include "roots/Align/Levenshtein.h"


%ignore roots::Base::set_description(std::string const *);
%include "roots/Base.h"
%include "roots/BaseLink.h"
%include "roots/UtteranceLinkedObject.h"
%include "relation.i"
namespace std 
{
	%template(vector_relation) vector<roots::Relation*>;
}

//DEFINE_VECTOR_P_ITEM(Relation,roots,roots)

%include "roots/RelationLinkedObject.h"
%include "roots/SequenceLinkedObject.h"
   
DEFINE_ITEM_CLASS(BaseItem,roots,roots)

// namespace roots {
// 	%template(TAlignment) Align< BaseItem * >;
// }


namespace roots{
    %template(align_base) Align<roots::Base*>;
}

DEFINE_CLASS(Alignment,roots/Align,roots::align,"")
// %rename (Alignment) Align< BaseItem * >;

%include "roots/RootsException.h"

%include "roots/Matrix.h"
%include "roots/Matrix/SparseMatrix.h"

%include "roots/Tree.h"
%ignore roots::tree::TreeNode::get_children() const;
%include "roots/Tree/TreeNode.h"


DEFINE_ITEM_CLASS(Void,roots,roots)

%include "roots/RootsStream.h"

#ifdef USE_STREAM_XML
%feature("notabstract") RootsStreamXML;
%include "roots/RootsStreamXML.h"
#endif

%include "roots/RootsStreamJson.h"

// RootsDisplay

%include "roots/RootsDisplay.h"
%ignore roots::RootsDisplay::to_pgf;

%extend roots::RootsDisplay
{
  std::string to_pgf(Utterance * utt,
			const std::vector<std::string> &sequencesOrder,
			const int level=0,
			const bool draw_relations=false)
  {
    std::string str;
    std::vector<std::string> localSequencesOrder;
    std::vector<std::string>::const_iterator iter;
    
    for (iter = sequencesOrder.begin(); iter != sequencesOrder.end(); iter++)
      {
	localSequencesOrder.push_back(*iter);
      }
    
    return $self->to_pgf(*utt,localSequencesOrder,level,draw_relations);
  }
  
  std::string to_pgf(Utterance * utt,
		       const std::vector<std::string> &sequencesOrder, 
		       const std::vector<std::string> &hiddensSeqLabels, 
		       const std::vector<int> &displayLevels,
		       const bool draw_relations=false)
  {
    std::string str;
    std::vector<std::string> localSequencesOrder;
    std::vector<std::string> localHiddensSeqLabels;
 
    std::vector<std::string>::const_iterator iter;   
    for (iter = sequencesOrder.begin();
	 iter != sequencesOrder.end(); 
	 iter++)
      { localSequencesOrder.push_back(*iter); }
    for (iter = hiddensSeqLabels.begin();
	 iter != hiddensSeqLabels.end(); 
	 iter++)
      { localHiddensSeqLabels.push_back(*iter); }
    
    return $self->to_pgf(*utt, 
		  localSequencesOrder,
		  localHiddensSeqLabels,
		  displayLevels,
		  draw_relations);
  }


}



//------------------------------------------------------------------------------
// file
//------------------------------------------------------------------------------
// Must be declared befor Accoustic items 

%include "file.i"



//------------------------------------------------------------------------------
// Generic items
//------------------------------------------------------------------------------


DEFINE_ITEM_CLASS(Symbol,roots,roots)
//%rename(ItemSymbol) roots::Symbol;
DEFINE_ITEM_CLASS(Number,roots,roots)
DEFINE_ITEM_CLASS(Embedding,roots,roots)



//------------------------------------------------------------------------------
// linguistic
//------------------------------------------------------------------------------


DEFINE_ITEM_CLASS(Filler,roots/Linguistic,roots::linguistic::filler,linguistic_filler)
%extend roots::linguistic::filler::Filler
{
    %template(as_nsa)    as<roots::linguistic::filler::Nsa>;
    %template(as_word)    as<roots::linguistic::filler::Word>;
}

DEFINE_ITEM_CLASS(Nsa,roots/Linguistic/Filler,roots::linguistic::filler,linguistic_filler)
DEFINE_ITEM_CLASS(Word,roots/Linguistic/Filler,roots::linguistic::filler,linguistic_filler)

DEFINE_ITEM_CLASS(NamedEntity,roots/Linguistic,roots::linguistic::namedentity,linguistic)
%rename(linguistic_namedentity_Category) roots::linguistic::namedentity::Category;

DEFINE_ITEM_CLASS(Location,roots/Linguistic/NamedEntity,roots::linguistic::namedentity,linguistic_namedentity)
DEFINE_ITEM_CLASS(Address,roots/Linguistic/NamedEntity/Location,roots::linguistic::namedentity::location,linguistic_namedentity_location)

// TODO
//DEFINE_ITEM_CLASS(Number,roots/Linguistic/NamedEntity,roots::linguistic::namedentity,linguistic_namedentity)

DEFINE_ITEM_CLASS(Organization,roots/Linguistic/NamedEntity,roots::linguistic::namedentity,linguistic_namedentity)
DEFINE_ITEM_CLASS(EthnicGroup,roots/Linguistic/NamedEntity/Organization,roots::linguistic::namedentity::organization,linguistic_namedentity_organization)
DEFINE_ITEM_CLASS(Other,roots/Linguistic/NamedEntity/Organization,roots::linguistic::namedentity::organization,linguistic_namedentity_organization)
DEFINE_ITEM_CLASS(Period,roots/Linguistic/NamedEntity,roots::linguistic::namedentity,linguistic_namedentity)
DEFINE_ITEM_CLASS(PeriodTime,roots/Linguistic/NamedEntity/Period,roots::linguistic::namedentity::period,linguistic_namedentity)
DEFINE_ITEM_CLASS(Person,roots/Linguistic/NamedEntity,roots::linguistic::namedentity,linguistic_namedentity)
DEFINE_ITEM_CLASS(Time,roots/Linguistic/NamedEntity,roots::linguistic::namedentity,linguistic_namedentity)

// POS

%rename(linguistic_pos_LEVEL_LABEL) roots::linguistic::pos::LEVEL_LABEL;
%rename(linguistic_pos_LEVEL_CLASS_FINE) roots::linguistic::pos::LEVEL_CLASS_FINE;
%rename(linguistic_pos_LEVEL_CLASS_COARSE) roots::linguistic::pos::LEVEL_CLASS_COARSE;
%rename(linguistic_pos_Degree) roots::linguistic::pos::Degree;
%rename(linguistic_pos_Gender) roots::linguistic::pos::Gender;
%rename(linguistic_pos_Number) roots::linguistic::pos::Number;
%rename(linguistic_pos_ConjunctionCategory) roots::linguistic::pos::ConjunctionCategory;
%rename(linguistic_pos_Category) roots::linguistic::pos::Category;
%rename(linguistic_pos_PunctuationCategory) roots::linguistic::pos::PunctuationCategory;
%rename(linguistic_pos_Mood) roots::linguistic::pos::Mood;
%rename(linguistic_pos_Tense) roots::linguistic::pos::Tense;
%rename(linguistic_pos_Aspect) roots::linguistic::pos::Aspect;
%rename(linguistic_pos_Person) roots::linguistic::pos::Person;
DEFINE_ITEM_CLASS(POS,roots/Linguistic,roots::linguistic::pos,linguistic)

DEFINE_ITEM_CLASS(Adjective,roots/Linguistic/POS,roots::linguistic::pos,linguistic_pos)
DEFINE_ITEM_CLASS(Adverb,roots/Linguistic/POS,roots::linguistic::pos,linguistic_pos)
DEFINE_ITEM_CLASS(Conjunction,roots/Linguistic/POS,roots::linguistic::pos,linguistic_pos)

DEFINE_ITEM_CLASS(Determiner,roots/Linguistic/POS,roots::linguistic::pos,linguistic_pos)
DEFINE_ITEM_CLASS(Article,roots/Linguistic/POS/Determiner,roots::linguistic::pos,linguistic_pos)
DEFINE_ITEM_CLASS(Demonstrative,roots/Linguistic/POS/Determiner,roots::linguistic::pos,linguistic_pos)
DEFINE_ITEM_CLASS(Indefinite,roots/Linguistic/POS/Determiner,roots::linguistic::pos,linguistic_pos)
DEFINE_ITEM_CLASS(Interrogative,roots/Linguistic/POS/Determiner,roots::linguistic::pos,linguistic_pos)
DEFINE_ITEM_CLASS(Numeral,roots/Linguistic/POS/Determiner,roots::linguistic::pos,linguistic_pos)
DEFINE_ITEM_CLASS(Possessive,roots/Linguistic/POS/Determiner,roots::linguistic::pos,linguistic_pos)

DEFINE_ITEM_CLASS(Interjection,roots/Linguistic/POS,roots::linguistic::pos,linguistic_pos)
DEFINE_ITEM_CLASS(Noun,roots/Linguistic/POS,roots::linguistic::pos,linguistic_pos)
DEFINE_ITEM_CLASS(Preposition,roots/Linguistic/POS,roots::linguistic::pos,linguistic_pos)
DEFINE_ITEM_CLASS(Pronoun,roots/Linguistic/POS,roots::linguistic::pos,linguistic_pos)
DEFINE_ITEM_CLASS(Punctuation,roots/Linguistic/POS,roots::linguistic::pos,linguistic_pos)
DEFINE_ITEM_CLASS(Verb,roots/Linguistic/POS,roots::linguistic::pos,linguistic_pos)
DEFINE_ITEM_CLASS(ForeignWord,roots/Linguistic/POS,roots::linguistic::pos,linguistic_pos)
DEFINE_ITEM_CLASS(Prefix,roots/Linguistic/POS,roots::linguistic::pos,linguistic_pos)
DEFINE_ITEM_CLASS(Particle,roots/Linguistic/POS,roots::linguistic::pos,linguistic_pos)

DEFINE_ITEM_CLASS(Symbol,roots/Linguistic/POS,roots::linguistic::pos,linguistic_pos)
%rename("linguistic_pos_Symbol") roots::linguistic::pos::Symbol;

// Syntax

%ignore roots::linguistic::syntax::SyntaxNode::get_word_index_array() const;
DEFINE_CLASS(SyntaxNode,roots/Linguistic/Syntax,roots::linguistic::syntax,linguistic_syntax)
%template(syntax_tree_t) roots::tree::Tree< roots::linguistic::syntax::SyntaxNode >;
DEFINE_ITEM_CLASS(Syntax,roots/Linguistic,roots::linguistic::syntax,linguistic_syntax)

// Remaining items

DEFINE_ITEM_CLASS(Word,roots/Linguistic,roots::linguistic,linguistic)

%ignore roots::linguistic::Grapheme::Grapheme(UChar const &);
DEFINE_ITEM_CLASS(Grapheme,roots/Linguistic,roots::linguistic,linguistic)



//------------------------------------------------------------------------------
// phonology
//------------------------------------------------------------------------------


%{
#include <boost/assign/list_of.hpp> // for 'map_list_of()'
#include <boost/assign/list_inserter.hpp>
%}

%include roots/Phonology/IpaConstants.h
%rename(phonology_ipa_CATEGORIES) roots::phonology::ipa::CATEGORIES;
%rename(phonology_ipa_CATEGORIES_LENGTH) roots::phonology::ipa::CATEGORIES_LENGTH;

%rename(phonology_ipa_LEVEL_LABEL) roots::phonology::ipa::LEVEL_LABEL;
%rename(phonology_ipa_LEVEL_CLASS_FINE) roots::phonology::ipa::LEVEL_CLASS_FINE;
%rename(phonology_ipa_LEVEL_CLASS_COARSE) roots::phonology::ipa::LEVEL_CLASS_COARSE;

%rename(phonology_ipa_Place) roots::phonology::ipa::Place;
%rename(phonology_ipa_Manner) roots::phonology::ipa::Manner;
%rename(phonology_ipa_Aperture) roots::phonology::ipa::Aperture;
%rename(phonology_ipa_Shape) roots::phonology::ipa::Shape;
%rename(phonology_ipa_Diacritic) roots::phonology::ipa::Diacritic;
%rename(phonology_ipa_Category) roots::phonology::ipa::Category;
%rename(phonology_ipa_Classification) roots::phonology::ipa::Classification;

namespace roots { namespace phonology { namespace ipa { class Alphabet; } } }
namespace roots { namespace phonology { namespace ipa { class Ipa; } } }
namespace roots { namespace phonology { class Phoneme; } }
DEFINE_ITEM_CLASS(Ipa,roots/Phonology,roots::phonology::ipa,phonology_ipa)
DEFINE_CLASS(IpaException,roots/Phonology,roots::phonology::ipa,phonology_ipa)
DEFINE_ITEM_CLASS(Phoneme,roots/Phonology,roots::phonology,phonology)

DEFINE_CLASS(Alphabet,roots/Phonology/Ipa,roots::phonology::ipa,phonology_ipa)



%include roots/Phonology/Ipa/MappedAlphabet.h

namespace roots { namespace phonology { namespace ipa { class IrisaAlphabet; } } }
%template(IrisaAlphabet_t) roots::phonology::ipa::MappedAlphabet<roots::phonology::ipa::IrisaAlphabet>;
DEFINE_CLASS(IrisaAlphabet,roots/Phonology/Ipa/Alphabet,roots::phonology::ipa,phonology_ipa)

namespace roots { namespace phonology { namespace ipa { class LiaphonAlphabet; } } }
%template(LiaphonAlphabet_t) roots::phonology::ipa::MappedAlphabet<roots::phonology::ipa::LiaphonAlphabet>;
DEFINE_CLASS(LiaphonAlphabet,roots/Phonology/Ipa/Alphabet,roots::phonology::ipa,phonology_ipa)

namespace roots { namespace phonology { namespace ipa { class SampaAlphabet; } } }
%template(SampaAlphabet_t) roots::phonology::ipa::MappedAlphabet<roots::phonology::ipa::SampaAlphabet>;
DEFINE_CLASS(SampaAlphabet,roots/Phonology/Ipa/Alphabet,roots::phonology::ipa,phonology_ipa)

namespace roots { namespace phonology { namespace ipa { class BdlexSampaAlphabet; } } }
%template(BdlexSampaAlphabet_t) roots::phonology::ipa::MappedAlphabet<roots::phonology::ipa::BdlexSampaAlphabet>;
DEFINE_CLASS(BdlexSampaAlphabet,roots/Phonology/Ipa/Alphabet,roots::phonology::ipa,phonology_ipa)

namespace roots { namespace phonology { namespace ipa { class ArpabetAlphabet; } } }
%template(ArpabetAlphabet_t) roots::phonology::ipa::MappedAlphabet<roots::phonology::ipa::ArpabetAlphabet>;
DEFINE_CLASS(ArpabetAlphabet,roots/Phonology/Ipa/Alphabet,roots::phonology::ipa,phonology_ipa)

namespace roots { namespace phonology { namespace ipa { class CmudictAlphabet; } } }
DEFINE_CLASS(CmudictAlphabet,roots/Phonology/Ipa/Alphabet,roots::phonology::ipa,phonology_ipa)

namespace roots { namespace phonology { namespace ipa { class PinyinAlphabet; } } }
%template(PinyinAlphabet_t) roots::phonology::ipa::MappedAlphabet<roots::phonology::ipa::PinyinAlphabet>;
DEFINE_CLASS(PinyinAlphabet,roots/Phonology/Ipa/Alphabet,roots::phonology::ipa,phonology_ipa)

namespace roots { namespace phonology { namespace ipa { class LoadableAlphabet; } } }
%template(LoadableAlphabet_t) roots::phonology::ipa::MappedAlphabet<roots::phonology::ipa::LoadableAlphabet>;
DEFINE_CLASS(LoadableAlphabet,roots/Phonology/Ipa/Alphabet,roots::phonology::ipa,phonology_ipa)


// Alphabet converters

%include roots/Phonology/Ipa/AlphabetConverter.h

// Non speech sound alphabets

%rename(phonology_nsa_Nsa) roots::phonology::nsa::Nsa;
%rename(phonology_nsa_CATEGORIES) roots::phonology::nsa::CATEGORIES;
%rename(phonology_nsa_CATEGORIES_LENGTH) roots::phonology::nsa::CATEGORIES_LENGTH;

%rename(phonology_nsa_LEVEL_LABEL) roots::phonology::nsa::LEVEL_LABEL;
%rename(phonology_nsa_LEVEL_CLASS_FINE) roots::phonology::nsa::LEVEL_CLASS_FINE;
%rename(phonology_nsa_LEVEL_CLASS_COARSE) roots::phonology::nsa::LEVEL_CLASS_COARSE;

%rename(phonology_nsa_Origin) roots::phonology::nsa::Origin;
%rename(phonology_nsa_Category) roots::phonology::nsa::Category;
%rename(phonology_nsa_Duration) roots::phonology::nsa::Duration;

%include "roots/Phonology/Nsa.h"

DEFINE_CLASS(NsAlphabet,roots/Phonology/Nsa,roots::phonology::nsa,phonology_nsa)
DEFINE_CLASS(NsaCommon,roots/Phonology/Nsa,roots::phonology::nsa,phonology_nsa)
DEFINE_CLASS(NsaException,roots/Phonology,roots::phonology::nsa,phonology_nsa)
DEFINE_CLASS(CpromNsAlphabet,roots/Phonology/Nsa/NsAlphabet,roots::phonology::nsa,phonology_nsa)
DEFINE_CLASS(IrisaNsAlphabet,roots/Phonology/Nsa/NsAlphabet,roots::phonology::nsa,phonology_nsa)
DEFINE_CLASS(SampaNsAlphabet,roots/Phonology/Nsa/NsAlphabet,roots::phonology::nsa,phonology_nsa)
DEFINE_CLASS(CmudictNsAlphabet,roots/Phonology/Nsa/NsAlphabet,roots::phonology::nsa,phonology_nsa)
DEFINE_CLASS(ArpabetNsAlphabet,roots/Phonology/Nsa/NsAlphabet,roots::phonology::nsa,phonology_nsa)
DEFINE_CLASS(LiaphonNsAlphabet,roots/Phonology/Nsa/NsAlphabet,roots::phonology::nsa,phonology_nsa)
DEFINE_CLASS(TranscriberNsAlphabet,roots/Phonology/Nsa/NsAlphabet,roots::phonology::nsa,phonology_nsa)
DEFINE_CLASS(BuckeyeNsAlphabet,roots/Phonology/Nsa/NsAlphabet,roots::phonology::nsa,phonology_nsa)

// Non speech sound alphabet converters

%include "roots/Phonology/Nsa/NsAlphabetConverter.h"

// Remaining phonological items

DEFINE_ITEM_CLASS(PhonemeBlock,roots/Phonology,roots::phonology,phonology)
DEFINE_ITEM_CLASS(Syllable,roots/Phonology,roots::phonology::syllable,phonology)
DEFINE_CLASS(ProsodicPhraseNode,roots/Phonology/ProsodicPhrase,roots::phonology::prosodicphrase,phonology_prosodicphrase)

%template(prosodic_phrase_t) roots::tree::Tree< roots::phonology::prosodicphrase::ProsodicPhraseNode >;
DEFINE_ITEM_CLASS(ProsodicPhrase,roots/Phonology,roots::phonology::prosodicphrase,phonology_prosodicphrase)

//------------------------------------------------------------------------------
// acoustic
//------------------------------------------------------------------------------


DEFINE_ITEM_CLASS(Allophone,roots/Acoustic,roots::acoustic,acoustic)

DEFINE_ITEM_CLASS(NonSpeechSound,roots/Acoustic,roots::acoustic,acoustic)
	
DEFINE_ITEM_CLASS(Segment,roots/Acoustic,roots::acoustic,acoustic)
%extend roots::acoustic::Segment
  {
    %template(as_signal_segment)    as<roots::acoustic::SignalSegment>;
       %template(as_time_segment)    as<roots::acoustic::TimeSegment>;
    %template(as_spectral_segment)    as<roots::acoustic::SpectralSegment>;
    %template(as_f0_segment)    as<roots::acoustic::F0Segment>;
    %template(as_pitchmark_segment)    as<roots::acoustic::PitchmarkSegment>;
  }

//DEFINE_ITEM_CLASS(Segment,roots/Acoustic,roots::acoustic,acoustic)
DEFINE_ITEM_CLASS(TimeSegment,roots/Acoustic/Segment,roots::acoustic,acoustic)
DEFINE_ITEM_CLASS(SignalSegment,roots/Acoustic/Segment,roots::acoustic,acoustic)
DEFINE_ITEM_CLASS(SpectralSegment,roots/Acoustic/Segment,roots::acoustic,acoustic)
DEFINE_ITEM_CLASS(F0Segment,roots/Acoustic/Segment,roots::acoustic,acoustic)
DEFINE_ITEM_CLASS(PitchmarkSegment,roots/Acoustic/Segment,roots::acoustic,acoustic)

DEFINE_ITEM_CLASS(Atom,roots/Acoustic,roots::acoustic,acoustic)
	
//------------------------------------------------------------------------------
// sequence
//------------------------------------------------------------------------------


%include "sequence.i"
//DEFINE_VECTOR_P_ITEM(Sequence,roots::sequence,roots)
namespace std 
{
	%template(vector_sequence) vector<roots::sequence::Sequence*>;
}


//------------------------------------------------------------------------------
// utterance
//------------------------------------------------------------------------------


%include "utterance.i"
DEFINE_VECTOR_P_ITEM(Utterance,roots,roots)


//------------------------------------------------------------------------------
// corpus
//------------------------------------------------------------------------------


%include "corpus.i"


//------------------------------------------------------------------------------
// GLOBAL
//------------------------------------------------------------------------------


#ifndef SWIGJAVA
%init 
%{
    ROOTS_INIT();
%}
#endif




