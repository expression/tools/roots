%module roots

%feature("ref")   ""
%feature("unref") ""

%{
#include <unicode/unistr.h>     
#include "roots/common.h"
#include "roots/roots.h"
#include <set> 
using namespace roots;
 using namespace icu;
%}


%include "std_common.i"
%include "std_except.i"
%include "std_string.i"
%include "std_vector.i"
%include "std_map.i"
%include "exception.i"
   
%ignore operator=;
%ignore operator<<;
%rename(get_last) last;    

%ignore inflate_object;

// ==================================================================================
// Specific perl
// ==================================================================================


namespace std 
{
  %template(vector_uint32) vector<uint32_t>;
}


//------------------------------- icu::UChar ------------------------------

%typemap(typecheck) const UChar & = const std::string &;
%typemap(typecheck) UChar  = std::string;

%typemap(out) UChar, const UChar {
  icu::UnicodeString us($1);
  std::string s;
  us.toUTF8String(s);
  if (argvi >= items) EXTEND(sp, 1);
  sv_setpvn($result = sv_newmortal(), s.data(), s.size());
  SvUTF8_on($result);
  ++argvi;
 }
 
%typemap(out) UChar &, const UChar &, UChar *, const UChar * {
  icu::UnicodeString us($1);
  std::string s;
  us.toUTF8String(s);
  if (argvi >= items) EXTEND(sp, 1);
  sv_setpvn($result = sv_newmortal(), s.data(), s.size());
  SvUTF8_on($result);
  ++argvi;
 }

%typemap(in) UChar (UChar temp), const UChar (UChar temp) {
  STRLEN len;
  char *p = SvPVutf8($input, len);
  temp = UnicodeString(p, (int) len, "utf-8")[0];
  $1 = temp;
}

%typemap(in) UChar & (UChar temp), const UChar & (UChar temp), UChar * (UChar temp), const UChar * (UChar temp) {
  STRLEN len;
  char *p = SvPVutf8($input, len);
  temp = UnicodeString(p, (int) len, "utf-8")[0];
  $1 = &temp;
}

%typemap(typecheck, precedence=1003) std::vector< UChar > const *, std::vector< UChar > const & {
  UnicodeString *temp;
  // Transform SV *$input to AV *tempav
  AV * tempav = (AV*)SvRV($input);
  size_t len = av_len(tempav) + 1;
  $1 = 1;
  for (size_t i = 0; i < (len==0?0:1) ; i++) {
    // Get element i in tempav (type is SV*)
    SV **tv = av_fetch(tempav, i, 0);
    if (SWIG_ConvertPtr(*tv, (void **) &temp, $descriptor(UnicodeString), 0) == -1) {
      $1 = 0;
    }
    else {
      $1 = 1;
    }
  }
 }

%typemap(out) std::vector<UChar> *, std::vector<UChar> &{
  int len = ($1)->size();
  SV **svs = new SV*[len];
  icu::UnicodeString us;
  for (int x = 0; x < len; x++) {
    std::string s;
    SV* sv = sv_newmortal();
    us = icu::UnicodeString($1->at(x));
    us.toUTF8String(s);
    //cout << "converting uchar " << x << " = " << s << endl;
    sv_setpvn(sv, s.data(), s.size());
    SvUTF8_on(sv);
    svs[x] = sv;
  }
  AV *myav = av_make(len, svs);
  delete[] svs;
  $result = newRV_noinc((SV*) myav);
  sv_2mortal($result);
  argvi++;
}


//------------------------------- icu::UnicodeString ------------------------------

%typemap(out) UnicodeString {
  std::string s;
  $1.toUTF8String(s);
  if (argvi >= items) EXTEND(sp, 1);
  sv_setpvn($result = sv_newmortal(), s.data(), s.size());
  SvUTF8_on($result);
  ++argvi;
 }

%typemap(out) UnicodeString &, UnicodeString * {
    std::string s;
    $1->toUTF8String(s);
    if (argvi >= items) EXTEND(sp, 1);
    sv_setpvn($result = sv_newmortal(), s.data(), s.size());
    SvUTF8_on($result);
    ++argvi;
 }

%typemap(in) UnicodeString  (UnicodeString temp) {
  STRLEN len;
  char *p = SvPVutf8($input, len);
  temp = UnicodeString(p, (int) len, "utf-8");
  $1 = temp;
}

%typemap(in) UnicodeString & (UnicodeString temp), UnicodeString * (UnicodeString temp) {
  STRLEN len;
  char *p = SvPVutf8($input, len);
  temp = UnicodeString(p, (int) len, "utf-8");
  $1 = &temp;
}

// Copy the typecheck code for "std::string".  
%typemap(typecheck) const UnicodeString & = const std::string &;
%typemap(typecheck) UnicodeString  = std::string;

%typemap(typecheck, precedence=1003) std::vector< UnicodeString > const & {
  UnicodeString *temp;
  // Transform SV *$input to AV *tempav
  AV * tempav = (AV*)SvRV($input);
  size_t len = av_len(tempav) + 1;
  $1 = 1;
  for (size_t i = 0; i < (len==0?0:1) ; i++) {
    // Get element i in tempav (type is SV*)
    SV **tv = av_fetch(tempav, i, 0);
    if (SWIG_ConvertPtr(*tv, (void **) &temp, $descriptor(UnicodeString), 0) == -1) {
      $1 = 0;
    }
    else {
      $1 = 1;
    }
  }
 }

%typemap(out) std::vector<UnicodeString> *, std::vector<UnicodeString> &{
  int len = ($1)->size();
  SV **svs = new SV*[len];
  for (int x = 0; x < len; x++) {
    std::string s;
    SV* sv = sv_newmortal();
	($1->at(x)).toUTF8String(s);
    sv_setpvn(sv, s.data(), s.size());
    SvUTF8_on(sv);
    svs[x] = sv;
  }
  AV *myav = av_make(len, svs);
  delete[] svs;
  $result = newRV_noinc((SV*) myav);
  sv_2mortal($result);
  argvi++;
}

//--------------------------------- std::string --------------------------------

%typemap(in) const std::string & (std::string temp), std::string & (std::string temp) {
  STRLEN len;
  char *p = SvPV($input, len);
  temp.assign(p, len);
  $1 = &temp;
}

%typemap(in) std::string (std::string temp) {
  STRLEN len;
  char *p = SvPV($input, len);
  temp.assign(p, len);
  $1 = temp;
}

%typemap(out) std::string {
  if (argvi >= items) EXTEND(sp, 1);
  sv_setpvn($result = sv_newmortal(), $1.data(), $1.size());
  SvUTF8_on($result);
  ++argvi;
 }

%typemap(out) std::string & {
  if (argvi >= items) EXTEND(sp, 1);
  sv_setpvn($result = sv_newmortal(), $1->data(), $1->size());
  SvUTF8_on($result);
  ++argvi;
 }

%typemap(freearg,noblock=1) const std::string & (std::string temp), std::string & {
	// Nothing to free
}

%typemap(typecheck, precedence=1003) std::vector< std::string > const & {
  std::string *temp;
  // Transform SV *$input to AV *tempav
  AV * tempav = (AV*)SvRV($input);
  size_t len = av_len(tempav) + 1;
  $1 = 1;
  for (size_t i = 0; i < (len==0?0:1) ; i++) {
    // Get element i in tempav (type is SV*)
    SV **tv = av_fetch(tempav, i, 0);
    if (SWIG_ConvertPtr(*tv, (void **) &temp, $descriptor(std::string), 0) == -1) {
      $1 = 0;
    }
    else {
      $1 = 1;
    }
  }
 }

%typemap(out) std::vector<std::string> *, std::vector<std::string> &, std::vector<std::string>, const std::vector<std::string>  {
  int len = ($1)->size();
  SV **svs = new SV*[len];
  for (int x = 0; x < len; x++) {
    SV* sv = sv_newmortal();
    sv_setpvn(sv, ($1->at(x)).data(), ($1->at(x)).size());
    SvUTF8_on(sv);
    svs[x] = sv;
  }
  AV *myav = av_make(len, svs);
  delete[] svs;
  $result = newRV_noinc((SV*) myav);
  sv_2mortal($result);
  argvi++;
}


%typemap(out) 
  const std::map<std::string, std::string>&, 
  std::map<std::string, std::string>&, 
  const std::map<std::string, std::string>*, 
  std::map<std::string, std::string>*
{
  HV * myhv = newHV();

  for(std::map<std::string, std::string>::const_iterator it= $1->begin(); it!=$1->end(); ++it)
  {
    SV* sv = newSVpvn(it->second.data(), it->second.size());
    SvUTF8_on(sv);

    hv_store(myhv, it->first.data(), it->first.size(), sv, 0);
  }

  $result = newRV_noinc((SV*) myhv);
  sv_2mortal($result);
  argvi++;
}

%typemap(in,noblock=1) 
  const std::map<std::string, std::string>& (void *argp=0, int res=0, $1_ltype tempmap=0), 
  std::map<std::string, std::string>& (void *argp=0, int res=0, $1_ltype tempmap=0), 
  const std::map<std::string, std::string>* (void *argp=0, int res=0, $1_ltype tempmap=0), 
  std::map<std::string, std::string>* (void *argp=0, int res=0, $1_ltype tempmap=0) 
{
  res = SWIG_ConvertPtr($input, &argp, $descriptor, %convertptr_flags);
  if (!SWIG_IsOK(res)) {
    if (SvROK($input) && SvTYPE(SvRV($input)) == SVt_PVHV) {
      //      fprintf(stderr, "Convert HV to map\n");
      tempmap = new $1_basetype;
      HV *hv = (HV*)SvRV($input);
      HE *hentry;
      hv_iterinit(hv);
      while ((hentry = hv_iternext(hv))) {
        std::string *val=0;
        // TODO: handle errors here
        SWIG_AsPtr_std_string SWIG_PERL_CALL_ARGS_2(HeVAL(hentry), &val);
        //fprintf(stderr, "%s => %s\n", HeKEY(hentry), val->c_str());
        (*tempmap)[HeKEY(hentry)] = val->c_str();
        delete val;
      }
      argp = tempmap;
    }
    else {
      %argument_fail(res, "$type", $symname, $argnum); 
    }
  }
  if (!argp) { %argument_nullref("$type", $symname, $argnum); }
  $1 = %reinterpret_cast(argp, $ltype);
}

%typemap(freearg,noblock=1) const std::map<std::string, std::string>& {
  delete tempmap$argnum;
}


%typemap(typecheck,precedence=1000) 
const std::map< std::string, std::string >&,
std::map< std::string, std::string >&
{
  void * argp = NULL;
  int res = 0;
  res = SWIG_ConvertPtr($input, &argp, $descriptor, %convertptr_flags);
  if (!SWIG_IsOK(res))
    {
      if (SvROK($input) && SvTYPE(SvRV($input)) == SVt_PVHV) 
	{
	  // Transform SV *$input to HV *temphv
	  HV * hash = (HV*)SvRV($input);
	  // Pick one pair if one exists
	  size_t n_elem = hv_iterinit(hash);
	  // An empty hash is OK
	  $1 = 1;
	  if (n_elem > 0) 
	    {
	      HE *pair = hv_iternext(hash);
	      // According to Perl, the key is necessarily a string.
	      // So, just check the type of the value
	      // TODO !
	    }
	}
      else
	{ $1 = 0; }
    }
  else 
    { $1 = 0; }
}


//-------------------------------------------------------------------------

%typemap(out) std::vector<roots::sequence::Sequence*> *,std::vector<roots::sequence::Sequence*> & {
  int len = ($1)->size();
  SV **svs = new SV*[len];
  for (int x = 0; x < len; x++) {
    SV* sv = sv_newmortal();
    sv_setref_pv(sv, "roots::Sequence", ($1)->at(x));
    svs[x] = sv;
  }
  AV *myav = av_make(len, svs);
  delete[] svs;
  $result = newRV_noinc((SV*) myav);
  sv_2mortal($result);
  argvi++;
}

//-------------------------------------------------------------------------

%typemap(out) std::vector<roots::Relation*> *, std::vector<roots::Relation*> & {
  int len = ($1)->size();
  SV **svs = new SV*[len];
  for (int x = 0; x < len; x++) {
    SV* sv = sv_newmortal();
    sv_setref_pv(sv, "roots::Relation", ($1)->at(x));
    svs[x] = sv;
  } 
  AV *myav = av_make(len, svs);
  delete[] svs;
  $result = newRV_noinc((SV*) myav);
  sv_2mortal($result);
  argvi++;
}


//-------------------------------------------------------------------------

%typemap(in) roots::Utterance (roots::Utterance *temp) {
  if (SWIG_ConvertPtr($input, (void **) &temp, $descriptor(roots::Utterance *), 0) == -1) {
    croak("An unknown error occured when converting roots::Utterance Perl reference to C++ object!");
  }
  else {
    $1 = *temp;
  }
 }

%typemap(typecheck, precedence=1002) std::vector< roots::Utterance > const & {
  roots::Utterance *temp;
  // Transform SV *$input to AV *tempav
  AV * tempav = (AV*)SvRV($input);
  size_t len = av_len(tempav) + 1;
  $1 = 1;
  for (size_t i = 0; i < (len==0?0:1) ; i++) {
    // Get element i in tempav (type is SV*)
    SV **tv = av_fetch(tempav, i, 0);
    if (SWIG_ConvertPtr(*tv, (void **) &temp, $descriptor(roots::Utterance *), 0) == -1) {
      $1 = 0;
    }
    else {
      $1 = 1;
    }
  }
 }

%typemap(in) const std::vector<roots::Utterance> & (roots::Utterance *temp) {
  // Transform SV *$input to AV *tempav
  AV * tempav = (AV*)SvRV($input);
  // Prepare the vector
    $1 = new std::vector<roots::Utterance>();
  size_t len = av_len(tempav) + 1;
  for (size_t i = 0; i < len ; i++) {
    // Get element i in tempav (type is SV*)
    SV *tv = av_shift(tempav);
    if (SWIG_ConvertPtr(tv, (void **) &temp, $descriptor(roots::Utterance *), 0) == -1) {
      croak("An unknown error occured while checking value types: a reference to roots::Utterance is expected.");
    }
    else {
      $1->push_back(*temp);
    }
    // Do not free temp
  }
 }

%typemap(freearg) const std::vector<roots::Utterance> & {
  delete $1;
 }

%typemap(typecheck, precedence=1001) std::map< std::string, roots::Utterance > const & {
  roots::Utterance *temp;
  // Transform SV *$input to HV *temphv
  HV * hash = (HV*)SvRV($input);
  // Pick one pair if one exists
  size_t n_elem = hv_iterinit(hash);
  // An empty hash is OK
  $1 = 1;
  // Otherwise...
  if (n_elem > 0) {
    HE *pair = hv_iternext(hash);
    // According to Perl, the key is necessarily a string.
    // So, just check the type of the value
    SV *value = hv_iterval(hash, pair);
    if (SWIG_ConvertPtr(value, (void **) &temp, $descriptor(roots::Utterance *), 0) == -1) {
      $1 = 0;
    }
    else {
      $1 = 1;
    }
  }
 }

%typemap(out) 
  std::map< std::string, roots::Utterance > &, 
  std::map< std::string, roots::Utterance > *,
  std::map< std::string, roots::Utterance > const &, 
  std::map< std::string, roots::Utterance > const *
{
  HV * myhv = newHV();

  for(std::map<std::string, roots::Utterance>::const_iterator it= $1->begin(); it!=$1->end(); ++it)
  {
    SV* sv = newRV_inc();
    sv_setref_pv(sv, $descriptor(roots::Utterance), it->second);

    hv_store(myhv, it->first.data(), it->first.size(), sv, 0);
  }

  $result = newRV_noinc((SV*) myhv);
  sv_2mortal($result);
  argvi++;
}


%typemap(in) 
  std::map< std::string, roots::Utterance > & (roots::Utterance *cpp_value), 
  std::map< std::string, roots::Utterance > * (roots::Utterance *cpp_value),
  std::map< std::string, roots::Utterance > const & (roots::Utterance *cpp_value), 
  std::map< std::string, roots::Utterance > const * (roots::Utterance *cpp_value)
{
  // Transform SV *$input to HV *temphv
  HV * hash = (HV*)SvRV($input);
  // Pick one pair if one exists
  size_t n_elem = hv_iterinit(hash);
  // An empty hash is OK
  $1 = new std::map<std::string, Utterance>();
  // Otherwise...
  size_t i_elem = 0;
  SV *perl_value;
  char *perl_key;
  I32 perl_key_len;
  std::string cpp_key;
  for (i_elem = 0; i_elem < n_elem; i_elem++) {
    perl_value = hv_iternextsv(hash, &perl_key, &perl_key_len);
    if (SWIG_ConvertPtr(perl_value, (void **) &cpp_value, $descriptor(roots::Utterance *), 0) == -1) {
      // Map values are not consistent
      croak("An error occured while converting Perl reference of roots::Utterance to C++ object. Probably, the type of values is not consistent.");
    }
    else {
      // Build a string for the key
      cpp_key = std::string(perl_key, (int) perl_key_len);
      // Cast of value as roots::Utterance already done
      // Insert the couple (key, value) into the map
      (*$1)[cpp_key] = *cpp_value;
      // Do not free cpp_value
    }
  }
 }

%typemap(freearg) std::map< std::string, roots::Utterance > const & {
  delete $1;
 }


%typemap(typecheck, precedence=1000) std::map< std::string, std::vector< roots::Utterance > > const & {
  roots::Utterance *temp;
  // Transform SV *$input to HV *temphv
  HV * hash = (HV*)SvRV($input);
  // Pick one pair if one exists
  size_t n_elem = hv_iterinit(hash);
  // An empty hash is OK
  $1 = 1;
  // Otherwise...
  if (n_elem > 0) {
    HE *pair = hv_iternext(hash);
    // According to Perl, the key is necessarily a string.
    // So, just check the type of the value
    SV *value = hv_iterval(hash, pair);
    // Is value a vector of utterances? (just copied code from above)
    // Transform SV *$input to AV *tempav
    //  SV * tempsv = SvRV(value);
    if (SvROK(value) && SvTYPE(SvRV(value)) == SVt_PVAV) {
      AV * tempav = (AV*)SvRV(value);
      size_t len = av_len(tempav) + 1;
      // OK unless one element is not an utterance
      $1 = 1;
      for (size_t i = 0; i < (len==0?0:1) ; i++) {
  // Get element i in tempav (type is SV*)
  SV **tv = av_fetch(tempav, i, 0);
  if (SWIG_ConvertPtr(*tv, (void **) &temp, $descriptor(roots::Utterance *), 0) == -1) {
    $1 = 0;
  }
  else {
    $1 = 1;
  }
      }
    }
    else {
      $1 = 0;
    }
  }
 }

%typemap(in) std::map< std::string, std::vector< roots::Utterance > > const & (roots::Utterance *cpp_value) {
  // Transform SV *$input to HV *temphv
  HV * hash = (HV*)SvRV($input);
  // Pick one pair if one exists
  size_t n_elem = hv_iterinit(hash);
  // An empty hash is OK
  $1 = new std::map<std::string, std::vector<Utterance> >();
  // Otherwise...
  size_t i_elem = 0;
  SV *perl_value;
  char *perl_key;
  I32 perl_key_len;
  std::string cpp_key;
  for (i_elem = 0; i_elem < n_elem; i_elem++) {
    perl_value = hv_iternextsv(hash, &perl_key, &perl_key_len);
    // Build a string for the key
    cpp_key = std::string(perl_key, (int) perl_key_len);
    // Transform SV *$input to AV *tempav
    AV * tempav = (AV*)SvRV(perl_value);
    // Prepare the vector
    (*$1)[cpp_key] = std::vector<roots::Utterance>();
    size_t len = av_len(tempav) + 1;
    for (size_t i = 0; i < len ; i++) {
      // Get element i in tempav (type is SV*)
      SV *tv = av_shift(tempav);
      if (SWIG_ConvertPtr(tv, (void **) &cpp_value, $descriptor(roots::Utterance *), 0) == -1) {
  croak("An unknown error occured while checking value types: a reference to roots::Utterance is expected.");
      }
      else {
  (*$1)[cpp_key].push_back(*cpp_value);
  // Do not free cpp_value
      }
    }
  }
 }

%typemap(freearg) std::map< std::string, std::vector< roots::Utterance > > const & {
  delete $1;
 }


//-------------------------------------------------------------------------

%define DEFINE_VECTOR_P_ITEM(classname,cpp_namespace,swig_namespace...)
#if ("swig_namespace" != "")
%template(vector_p_ ## swig_namespace ## _ ## classname) std::vector<cpp_namespace::classname*>;
#else
%template(vector_p_ ## classname) std::vector<cpp_namespace::classname*>;
#endif
  
//%typemap(typecheck, precedence=1010)
//  cpp_namespace::classname*,
//  cpp_namespace::classname&,
//  const cpp_namespace::classname*,
//  const cpp_namespace::classname&
//  {
//    cpp_namespace::classname *temp;
//
//    if (SWIG_ConvertPtr($input, (void **) &temp, $descriptor(cpp_namespace::classname *), 0) == -1) {
//      $1 = 0;
//    }
//    else {
//      $1 = 1;
//    }
//  }


  %typemap(out) std::vector<cpp_namespace::classname*> *, std::vector<cpp_namespace::classname*> & {
    int len = ($1)->size();
    SV **svs = new SV*[len];
    for (int x = 0; x < len; x++) {
      SV* sv = sv_newmortal();
      sv_setref_pv(sv, $descriptor(cpp_namespace::classname *), ($1)->at(x));
      svs[x] = sv;
    }
    AV *myav = av_make(len, svs);
    delete[] svs;
    $result = newRV_noinc((SV*) myav);
    sv_2mortal($result);
    argvi++;
  }
  
%enddef

//-------------------------------------------------------------------------







%exception
{
	try { $function }
    catch (roots::RootsException &err)
	{
        std::clog<<err.what()<<std::endl;
        croak("An exception occured!");
	}
    catch (std::exception &err)
	{
        std::clog<<err.what()<<std::endl;
		croak("An exception occured!");
	}
	catch(...)
	{
        std::stringstream ss;
		ss << "An unknown error occured in "<< __FILE__ <<"(" <<__LINE__<<")!";
        std::clog<<ss.str()<<std::endl;
        croak("An exception occured!");
	}
}

%init %{
	ROOTS_INIT();
%}

%include "roots.i"
