%module roots

%feature("ref")   ""
%feature("unref") ""

%{
#include <unicode/unistr.h>     
#include "roots/common.h"    
#include "roots/roots.h"
#include <set> 
using namespace roots;
 using namespace icu;
%}


%include "std_common.i"
//%include "std_except.i"
%include "std_string.i"
%include "std_vector.i"
%include "std_map.i"
%include "exception.i"
   
%ignore operator=;
%ignore operator<<;
%rename(get_last) last;    

%ignore inflate_object;

// ==================================================================================
// Specific python
// ==================================================================================

%ignore *::operator=;
%ignore operator=;
%rename open fopen;


%include "exception.i"

%exception {
  try {
    $action
  } catch (const std::exception& e) {
    SWIG_exception(SWIG_RuntimeError, e.what());
  }
}


//------------------------------ std::string -----------------------------------


%typemap(in) const std::string & (std::string temp), std::string & (std::string temp) { 
  if (PyUnicode_Check($input)) { 
    temp = std::string(PyUnicode_AsUTF8($input)); 
    $1 = &temp; 
  } else { 
    SWIG_exception(SWIG_TypeError, "string expected"); 
  } 
} 

%typemap(in) std::string (std::string temp) { 
  if (PyUnicode_Check($input)) { 
    temp = std::string(PyUnicode_AsUTF8($input)); 
    $1 = temp; 
  } else { 
    SWIG_exception(SWIG_TypeError, "string expected"); 
  } 
}

%typemap(out) std::string &, std::string * { 
  $result = PyUnicode_FromStringAndSize($1->data(), $1->size());
} 

%typemap(out) std::string { 
  $result = PyUnicode_FromStringAndSize($1.data(), $1.size());
} 

%typemap(freearg,noblock=1) const std::string & (std::string temp), std::string & {
  // Nothing to free
}


//------------------------------- icu::UChar -----------------------------------

%typemap(typecheck) const UChar & = const std::string &;
%typemap(typecheck) UChar  = std::string;

%typemap(out) UChar, UChar & {
  icu::UnicodeString us($1);
  std::string s;
  us.toUTF8String(s);
  
  $result = PyUnicode_FromStringAndSize(s.data(), s.size());
 }


%typemap(in) UChar & (UChar temp) {
  if (PyUnicode_Check($input)) { 
    temp = UnicodeString(PyUnicode_AsUTF8($input))[0]; 
    $1 = &temp; 
  } else { 
    SWIG_exception(SWIG_TypeError, "UChar expected"); 
  } 
}

%typemap(in) UChar (UChar temp) {
  if (PyUnicode_Check($input)) { 
    temp = UnicodeString(PyUnicode_AsUTF8($input))[0]; 
    $1 = temp; 
  } else { 
    SWIG_exception(SWIG_TypeError, "UChar expected"); 
  } 
}


/*
%typemap(typecheck, precedence=1003) std::vector< UChar > & {
  UnicodeString *temp;

  // Transform SV *$input to AV *tempav
  AV * tempav = (AV*)SvRV($input);
  size_t len = av_len(tempav) + 1;
  $1 = 1;
  for (size_t i = 0; i < (len==0?0:1) ; i++) {
    // Get element i in tempav (type is SV*)
    SV **tv = av_fetch(tempav, i, 0);
    if (SWIG_ConvertPtr(*tv, (void **) &temp, $descriptor(UnicodeString), 0) == -1) {
      $1 = 0;
    }
    else {
      $1 = 1;
    }
  }
 }
 */

%typemap(out) std::vector<UChar> *, std::vector<UChar> &{
  icu::UnicodeString us;
  int len = ($1)->size();
  PyObject *list = PyList_New(len);

  for (int x = 0; x < len; x++) {
    std::string s;
    us = icu::UnicodeString($1->at(x));
    us.toUTF8String(s);

    PyList_SetItem(list, x, PyUnicode_FromStringAndSize(s.data(), s.size()));
  }

  $result = list;
}


//------------------------------- icu::UnicodeString ---------------------------

%typemap(out) UnicodeString {
  std::string s;
  $1.toUTF8String(s);
  $result = PyUnicode_FromStringAndSize(s.data(), s.size());
 }

%typemap(out) UnicodeString &, UnicodeString * {
    std::string s;
    $1->toUTF8String(s);
    $result = PyUnicode_FromStringAndSize(s.data(), s.size());  
 }

%typemap(in) UnicodeString  (UnicodeString temp) {
  if (PyUnicode_Check($input)) { 
    temp = UnicodeString(PyUnicode_AsUTF8($input)); 
    $1 = temp; 
  } else { 
    SWIG_exception(SWIG_TypeError, "String expected"); 
  } 
}

%typemap(in) UnicodeString & (UnicodeString temp), UnicodeString * (UnicodeString temp) {
  if (PyUnicode_Check($input)) { 
    temp = UnicodeString(PyUnicode_AsUTF8($input)); 
    $1 = &temp; 
  } else { 
    SWIG_exception(SWIG_TypeError, "String expected"); 
  } 
}

// Copy the typecheck code for "std::string".  
%typemap(typecheck) const UnicodeString & = const std::string &;
%typemap(typecheck) UnicodeString  = std::string;

%typemap(typecheck, precedence=1003) std::vector< UnicodeString > const & {
  UnicodeString *temp;
  // Transform SV *$input to AV *tempav
  AV * tempav = (AV*)SvRV($input);
  size_t len = av_len(tempav) + 1;
  $1 = 1;
  for (size_t i = 0; i < (len==0?0:1) ; i++) {
    // Get element i in tempav (type is SV*)
    SV **tv = av_fetch(tempav, i, 0);
    if (SWIG_ConvertPtr(*tv, (void **) &temp, $descriptor(UnicodeString), 0) == -1) {
      $1 = 0;
    }
    else {
      $1 = 1;
    }
  }
 }

%typemap(out) std::vector<UnicodeString> *, std::vector<UnicodeString> &{
  int len = ($1)->size();
  PyObject *list = PyList_New(len);

  for (int x = 0; x < len; x++) {
    std::string s;
    ($1->at(x)).toUTF8String(s);

    PyList_SetItem(list, x, PyUnicode_FromStringAndSize(s.data(), s.size()));
  }

  $result = list;
}

//------------------------------------------------------------------------------




%define DEFINE_VECTOR_P_ITEM(classname,cpp_namespace,swig_namespace...)
#if ("swig_namespace" != "")
%template(vector_p_ ## swig_namespace ## _ ## classname) std::vector<cpp_namespace::classname*>;
#else
%template(vector_p_ ## classname) std::vector<cpp_namespace::classname*>;
#endif
  
  //#warning cpp_namespace::classname*
  %typemap(in) std::vector<cpp_namespace::classname*> * (std::vector<cpp_namespace::classname*> temp), 
    std::vector<cpp_namespace::classname*> & (std::vector<cpp_namespace::classname*> temp) 
  {
    temp = std::vector<cpp_namespace::classname*>();
  
    int len = PySequence_Size($input);
    
    cpp_namespace::classname *p;

    for (int x = 0; x < len; x++) {

      PyObject *obj = PySequence_GetItem($input, x); 

      if (SWIG_ConvertPtr(obj, (void **) &p, $descriptor(cpp_namespace::classname *), 0) == -1) {
        SWIG_exception(SWIG_TypeError, "wrong type"); 
      } 
      
      temp.push_back(p);
    }

    $1 = &temp;
  }

  %typemap(out) std::vector<cpp_namespace::classname*> *, std::vector<cpp_namespace::classname*> &
  {
    int len = ($1)->size();
    PyObject *list = PyList_New(len);

    for (int x = 0; x < len; x++) {
      PyList_SetItem(list, x,  SWIG_NewPointerObj(SWIG_as_voidptr(($1)->at(x)), $descriptor(cpp_namespace::classname *) , 0));
    }

    $result = list;
  }


%enddef


%include "roots.i"
