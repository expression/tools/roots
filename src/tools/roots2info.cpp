/*
    This file is part of Roots.

    Roots is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Roots is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Roots.  If not, see <http://www.gnu.org/licenses/>.

    Copyright (c) 2013, 2014 IRISA
*/


#include <boost/program_options.hpp>
#include "../roots/roots.h"

#include <iostream>
#include <fstream>
#include <iomanip>
#include <string>
#include <unicode/regex.h>


#define VERSION_STRING "Version 0.1"


namespace po = boost::program_options;


void display_command_line(const char *programName,po::options_description &arguments, std::vector<po::options_description* > &optionsDescVector, std::ostream &out=std::cout)
{
	std::vector< boost::shared_ptr< po::option_description > >::const_iterator iter;

	out << "Syntax: " << std::endl;
	out << "  " << programName << " [options] ";
	for(iter  = arguments.options().begin(); iter != arguments.options().end(); ++iter)
	{
		out << (*iter)->long_name() << " ";
	}
	out << std::endl;
	out << std::endl;

	out << "Arguments: " << std::endl;
	for(iter  = arguments.options().begin(); iter != arguments.options().end(); ++iter)
	{
		out << "  " << (*iter)->long_name() << "\t" << (*iter)->description() << std::endl;
	}
	out << std::endl;

	std::vector< po::options_description* >::const_iterator iter2;
	for(iter2= optionsDescVector.begin(); iter2 != optionsDescVector.end(); ++iter2)
	{
		out << (*(*iter2)) << std::endl << std::endl;
	}

}

int main(int argc, char **argv)
{
    std::string inputFile;
    roots::RootsStream *rootsStream = NULL;

	ROOTS_INIT();
	ROOTS_SET_LOGGER_LEVEL(ROOTS_LOGGER_ERROR_LEVEL);

	// Declare the supported options.
	std::vector<po::options_description* > optionsDescVector;
	po::options_description generalOptions("Options");
	optionsDescVector.push_back(&generalOptions);
	generalOptions.add_options()
		("help,h", "produce help message")
    ;

	po::options_description arguments("Arguments");
	arguments.add_options()
		("input-file", po::value< std::string >()->required(), "input file (.xml|.json|.roots)")
    ;

    po::options_description cmdline_options;
	cmdline_options.add(arguments).add(generalOptions);

	po::positional_options_description p;
	p.add("input-file", 1);

	try{
		po::variables_map vm;
		po::store(po::command_line_parser(argc, argv).
				  options(cmdline_options).positional(p).run(), vm);

		if (vm.count("help")) {
			display_command_line(argv[0], arguments, optionsDescVector, std::cout);
			return EXIT_SUCCESS;
		}

		if (vm.count("verbose")) {
			    ROOTS_SET_LOGGER_LEVEL(ROOTS_LOGGER_DEBUG_LEVEL);
		}

		po::notify(vm);

		inputFile = vm["input-file"].as< std::string >();

		if (endsWith(inputFile,".roots"))
		{
			std::cerr << "Still not implemented" << std::endl;
			exit(EXIT_FAILURE);
		}
		else
		{
#ifdef USE_STREAM_XML
			if(endsWith(inputFile,".xml"))
			{
                rootsStream = new roots::RootsStreamXML();
			}
			else
			{
#endif
				if(endsWith(inputFile,".json"))
				{
                    rootsStream = new roots::RootsStreamJson();
				}
				else
				{
					std::cerr << "error: unsupported format of input file!" << std::endl;
					return EXIT_FAILURE;
				}
#ifdef USE_STREAM_XML
			}
#endif
		}
	}
    catch(po::error& e)
	{
		std::cerr << "error: " << e.what() << std::endl  << std::endl;
		display_command_line(argv[0], arguments, optionsDescVector, std::cerr);
		return EXIT_FAILURE;
	}


	try
	{

		rootsStream->load(inputFile);
        roots::Corpus *corpus = new roots::Corpus();
		corpus->inflate(rootsStream);

        roots::Utterance *r = corpus->get_utterance(0);

        // Dump sequence labels
        std::vector<std::string> sequenceLabelsInput = *(r->get_all_sequence_labels());
        std::cout << "Sequence labels:" << std::endl;
        std::vector<std::string>::const_iterator iter;
		for(iter = sequenceLabelsInput.begin();iter!=sequenceLabelsInput.end();iter++)
		{
            std::cout << "\t- " <<(*iter) << std::endl;
		}

        // Dump existing relations
        std::vector<std::string> relations = *(r->get_all_relation_labels());
        std::cout << "Existing relation labels:" << std::endl;
        for (iter = relations.begin(); iter != relations.end(); iter++)
        {
            std::cout << "\t- " << (*iter) << std::endl;
        }

		delete r;
		delete corpus;
		delete rootsStream;
    }
    catch(roots::RootsException e)
    {
		std::cerr << "error: " << e.what() << std::endl;
		exit(EXIT_FAILURE);
	}
    catch(std::exception e)
    {
		std::cerr << "error: " << e.what() << std::endl;
		exit(EXIT_FAILURE);
	}


	ROOTS_TERMINATE();

    return 0;
}
