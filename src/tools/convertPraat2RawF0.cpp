/*
    This file is part of Roots.

    Roots is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Roots is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Roots.  If not, see <http://www.gnu.org/licenses/>.

    Copyright (c) 2013, 2014 IRISA
*/


#include <boost/program_options.hpp>
#include "../roots/roots.h"
//#include "roots-utils.h"


#include <iostream>
#include <fstream>
#include <iomanip>
#include <string>
#include <unicode/regex.h>


#define VERSION_STRING "Version 0.1"


namespace po = boost::program_options;


/*
 * export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:../../lib:. 
 * 
 * ../../../../bin/splitTextGrid --praatfile /commun/bdd/cprom/txt/cnf-be.Pitch --audiofile /commun/bdd/cprom/cprom-wav/cnf-be.wav --tiername ss --outdir ./testsplit --prefix cprom_cnf-be /commun/bdd/cprom/txt/cnf-be.TextGrid
 * 
 */

void display_command_line(const char *programName,po::options_description &arguments, std::vector<po::options_description* > &optionsDescVector, std::ostream &out=std::cout)
{
	std::vector< boost::shared_ptr< po::option_description > >::const_iterator iter;

	out << "Syntax: " << std::endl;
	out << "  " << programName << " [options] ";
	for(iter  = arguments.options().begin(); iter != arguments.options().end(); ++iter)
	{
		out << (*iter)->long_name() << " ";
	}
	out << std::endl;
	out << std::endl;

	out << "Arguments: " << std::endl;
	for(iter  = arguments.options().begin(); iter != arguments.options().end(); ++iter)
	{
		out << "  " << (*iter)->long_name() << "\t" << (*iter)->description() << std::endl;
	}
	out << std::endl;

	std::vector< po::options_description* >::const_iterator iter2;
	for(iter2= optionsDescVector.begin(); iter2 != optionsDescVector.end(); ++iter2)
	{
		out << (*(*iter2)) << std::endl << std::endl;
	}

}



int main(int argc, char **argv)
{
	std::string inputfile = "";
	std::string outdir = "./";
	std::ofstream outfile;
	std::stringstream ss;
	
	ROOTS_INIT();
	ROOTS_SET_LOGGER_LEVEL(ROOTS_LOGGER_ERROR_LEVEL);
	
	// Declare the supported options.
	std::vector<po::options_description* > optionsDescVector;
	po::options_description generalOptions("Options");
	optionsDescVector.push_back(&generalOptions);
	generalOptions.add_options()
	    ("outdir", po::value<std::string>()->default_value("./"), "output directory for resulting files")
		("verbose,v", "verbose mode")
	    ("help", "produce help message")
    ;

	po::options_description arguments("Arguments");
	arguments.add_options()
		("input-file", po::value< std::string >()->required(), "praat pitch file to split")
    ;   	

    po::options_description cmdline_options;
	cmdline_options.add(arguments).add(generalOptions);

	po::positional_options_description p;
	p.add("input-file", 1);

	try{
		po::variables_map vm;
		po::store(po::command_line_parser(argc, argv).
				  options(cmdline_options).positional(p).run(), vm);

		if (vm.count("help")) {
			display_command_line(argv[0], arguments, optionsDescVector, std::cout);
			return EXIT_SUCCESS;
		}

		if (vm.count("verbose")) {
			    ROOTS_SET_LOGGER_LEVEL(ROOTS_LOGGER_DEBUG_LEVEL);			
		}

		po::notify(vm);
   
		outdir = vm["outdir"].as< std::string >();
		inputfile = vm["input-file"].as< std::string >();	   	  
	}catch(po::error& e)
	{
		std::cerr << "error: " << e.what() << std::endl  << std::endl;
		display_command_line(argv[0], arguments, optionsDescVector, std::cerr);
		return EXIT_FAILURE;
	}
	
	// Load PraatPitch file
	roots::file::praat::PraatPitchFile *praatStructure = NULL;
	roots::file::praat::PraatPitch praatPitchFile(inputfile);
	
	praatStructure = praatPitchFile.load();
	
	std::string outf0filename;
	
	int slashIndex = inputfile.find_last_of("/");
	if(slashIndex!=-1)
	{	
		outf0filename = inputfile.substr(slashIndex+1, inputfile.length()-slashIndex+1);
	}
	else
	{
		outf0filename = inputfile;
	}	
	findAndReplace(outf0filename, ".Pitch", ".f0");
	outf0filename = outdir + "/" + outf0filename;

	ss << outf0filename;
	outfile.open(ss.str().c_str());
	if(outfile.fail())
	{		
		std::stringstream ss2;
		ss2 << "can't open file \"" << ss.str() << "\" in write mode! \n";
		ROOTS_LOGGER_ERROR(ss2.str().c_str());
        throw roots::RootsException(__FILE__, __LINE__, ss2.str());
	}
	
	
	//outfile.imbue(this->locale);
	outfile.setf(std::ios::fixed,std::ios::floatfield);

	double time = praatStructure->x1;
	double f0Value = 0.0;
	
	for(unsigned int itemIndex=0; itemIndex < praatStructure->frames.size(); ++itemIndex)
	{
		f0Value = praatStructure->frames[itemIndex]->candidate[0]->frequency;
		
		outfile << time ;
		outfile << " ";
		outfile << f0Value;
		outfile << std::endl;
		
		time += praatStructure->dx;		
	}

	outfile.close();

	roots::file::praat::PraatPitch::delete_structure(&praatStructure);

	ROOTS_TERMINATE();

    return 0;
}
