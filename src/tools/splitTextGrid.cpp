/*
    This file is part of Roots.

    Roots is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Roots is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Roots.  If not, see <http://www.gnu.org/licenses/>.

    Copyright (c) 2013, 2014 IRISA
*/


/*
 *  splitTextGrid.cpp
 *
 *  Created on: 07 déc. 2012
 *  Author: damien.lolive@irisa.fr
 */
#include <boost/program_options.hpp>
#include "../roots/roots.h"
#include "../roots/utils.h"

#include <iostream>
#include <fstream>
#include <iomanip>
#include <string>
#include <unicode/regex.h>


#define VERSION_STRING "Version 0.1"


namespace po = boost::program_options;

using namespace icu;

/*
 * export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:../../lib:. 
 * 
 * ../../../../bin/splitTextGrid --praatfile /commun/bdd/cprom/txt/cnf-be.Pitch --audiofile /commun/bdd/cprom/cprom-wav/cnf-be.wav --tiername ss --outdir ./testsplit --prefix cprom_cnf-be_0000 /commun/bdd/cprom/txt/cnf-be.TextGrid
 * 
 */

void display_command_line(const char *programName,po::options_description &arguments, std::vector<po::options_description* > &optionsDescVector, std::ostream &out=std::cout)
{
	std::vector< boost::shared_ptr< po::option_description > >::const_iterator iter;

	out << "Syntax: " << std::endl;
	out << "  " << programName << " [options] ";
	for(iter  = arguments.options().begin(); iter != arguments.options().end(); ++iter)
	{
		out << (*iter)->long_name() << " ";
	}
	out << std::endl;
	out << std::endl;

	out << "Arguments: " << std::endl;
	for(iter  = arguments.options().begin(); iter != arguments.options().end(); ++iter)
	{
		out << "  " << (*iter)->long_name() << "\t" << (*iter)->description() << std::endl;
	}
	out << std::endl;

	std::vector< po::options_description* >::const_iterator iter2;
	for(iter2= optionsDescVector.begin(); iter2 != optionsDescVector.end(); ++iter2)
	{
		out << (*(*iter2)) << std::endl << std::endl;
	}

}

int main(int argc, char **argv)
{
    std::string inputfile = "";
	std::string audiofile = "";
	std::string praatfile = "";
	std::string tiername = "";
	std::string outdir = "./";
	std::string prefix = "file";

	ROOTS_INIT();
	ROOTS_SET_LOGGER_LEVEL(ROOTS_LOGGER_ERROR_LEVEL);
	
	// Declare the supported options.
	std::vector<po::options_description* > optionsDescVector;
	po::options_description generalOptions("Options");
	optionsDescVector.push_back(&generalOptions);
	generalOptions.add_options()
	    ("audiofile", po::value<std::string>()->default_value(""), "audio file split")
	    ("praatfile", po::value<std::string>()->default_value(""), "praat pitch file to split")
	    ("tiername", po::value<std::string>()->default_value(""), "tier name used for splitting")
	    ("outdir", po::value<std::string>()->default_value("./"), "output directory for resulting files")
	    ("prefix", po::value<std::string>()->default_value(""), "prefix for the output corpus files")
		("verbose,v", "verbose mode")
		("help,h", "produce help message")    
		;

	po::options_description arguments("Arguments");
	arguments.add_options()
		("input-file", po::value< std::string >()->required(), "input file (.xml|.json|.roots)")
		;   	

    po::options_description cmdline_options;
	cmdline_options.add(arguments).add(generalOptions);

	po::positional_options_description p;
	p.add("input-file", 1);

	try{
		po::variables_map vm;
		po::store(po::command_line_parser(argc, argv).
				  options(cmdline_options).positional(p).run(), vm);

		if (vm.count("help")) {
			display_command_line(argv[0], arguments, optionsDescVector, std::cout);
			return EXIT_SUCCESS;
		}

		if (vm.count("verbose")) {
			ROOTS_SET_LOGGER_LEVEL(ROOTS_LOGGER_DEBUG_LEVEL);			
		}

		po::notify(vm);
	
		audiofile = vm["audiofile"].as< std::string >();
		praatfile = vm["praatfile"].as< std::string >();
		tiername = vm["tiername"].as< std::string >();
		outdir = vm["outdir"].as< std::string >();
		prefix = vm["prefix"].as< std::string >();		
		inputfile = vm["input-file"].as< std::string >();
	}catch(po::error& e)
	{
		std::cerr << "error: " << e.what() << std::endl  << std::endl;
		display_command_line(argv[0], arguments, optionsDescVector, std::cerr);
		return EXIT_FAILURE;
	}

	try
	{	
	
		int tierindex = 0;
	
		// Load TextGrid file
		roots::file::praat::TextGrid textGridFile(inputfile);
		roots::file::praat::TextGridFile *tgridStructure = textGridFile.load();
	
	
		// Load PraatPitch file
		roots::file::praat::PraatPitchFile *praatStructure = NULL;
		roots::file::praat::PraatPitch praatPitchFile(praatfile);
		if(praatfile != "")
		{		
			praatStructure = praatPitchFile.load();
		}
	
		// Load Audio file
		roots::file::audio::Audio audioFile;
		if(audiofile != "")
		{		
			audioFile.set_file_name(audiofile);
			audioFile.open();
			audioFile.load(0,-1);
			//audioFile.export_to_file("./test.wav");
		}
	
		// Init tiername if == ""
		if(tiername == "")
		{
			tiername = unistr2stdstr(*(tgridStructure->items[0]->name));
			tierindex = 0;
		}
		else
		{
			tierindex = 0;
			while((tierindex < (int)tgridStructure->items.size()) 
				  && (tiername != unistr2stdstr(*(tgridStructure->items[tierindex]->name))))
			{	
				++tierindex;
			}
			if(tierindex >= (int)tgridStructure->items.size())
			{
				std::cerr << "tier name not found in structure!" << std::endl;
				return EXIT_FAILURE;
			}
		}


		// for each item in the tier
		for(unsigned int elementIndex=0; elementIndex < tgridStructure->items[tierindex]->elements.size(); ++elementIndex)
		{		
			double xmin = tgridStructure->items[tierindex]->elements[elementIndex]->xmin;
			double xmax = tgridStructure->items[tierindex]->elements[elementIndex]->xmax;		
			//cout << "      text = " << *(tgridStructure->items[tierindex]->elements[elementIndex]->text) << endl;
	
			// TODO
			// if text == "" ou "\\s*" then skip
			if((*(tgridStructure->items[tierindex]->elements[elementIndex]->text) != UnicodeString("_"))
			   && (*(tgridStructure->items[tierindex]->elements[elementIndex]->text) != UnicodeString("")))
			{
				std::cout << "xmin="<<xmin<<"/xmax="<<xmax<<std::endl;
		
				std::string filenamePrefix;
				std::stringstream ss;
				ss << outdir << "/" << prefix << "_t" << elementIndex;
				filenamePrefix = ss.str();
			
				// -> extract all info for textgrid
				std::string outtextgridfilename;
				outtextgridfilename = filenamePrefix + ".TextGrid";
			
				roots::file::praat::TextGridFile *newtgridstructure = roots::file::praat::TextGrid::extract_info_between_start_and_end(tgridStructure, xmin, xmax, true);
				textGridFile.save(newtgridstructure, outtextgridfilename, std::locale("en_GB.utf8"));
				roots::file::praat::TextGrid::delete_structure(&newtgridstructure);


				if(praatfile != "")
				{
					// -> extract all info for praat pitch
					std::string outpraatfilename;
					outpraatfilename = filenamePrefix + ".Pitch";

					roots::file::praat::PraatPitchFile *newpraatstructure = roots::file::praat::PraatPitch::extract_info_between_start_and_end(praatStructure, xmin, xmax, true);
					praatPitchFile.save(newpraatstructure, outpraatfilename, std::locale("en_GB.utf8"));
					roots::file::praat::PraatPitch::delete_structure(&newpraatstructure);
				}

				if(audiofile != "")
				{
					// -> split wav
					std::string outaudiofilename;
					outaudiofilename = filenamePrefix + ".wav";
				
					// Convertir xmin et xmax into sample indexes
					int startIndex = xmin*audioFile.get_format_rate(); 
					int endIndex = xmax*audioFile.get_format_rate();
					//cout << "xmin="<<xmin<<"/xmax="<<xmax<<"/startIndex="<<startIndex<<"/endIndex="<<endIndex<<endl;

					audioFile.export_to_file(outaudiofilename, startIndex, endIndex - startIndex + 1);
				}
			}
			else
			{
				std::cout << "skipping segment" << std::endl;
			}
		}
	

		roots::file::praat::TextGrid::delete_structure(&tgridStructure);
		if(praatfile != "")
			roots::file::praat::PraatPitch::delete_structure(&praatStructure);

    }catch(roots::RootsException e){
		std::cerr << "error: " << e.what() << std::endl;
		exit(EXIT_FAILURE);
	}catch(std::exception e){
		std::cerr << "error: " << e.what() << std::endl;
		exit(EXIT_FAILURE);
	}

	ROOTS_TERMINATE();

    return 0;
}
