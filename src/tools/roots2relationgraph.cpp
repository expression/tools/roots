/*
    This file is part of Roots.

    Roots is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Roots is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Roots.  If not, see <http://www.gnu.org/licenses/>.

    Copyright (c) 2013, 2014 IRISA
*/


#include <boost/program_options.hpp>
#include "../roots/roots.h"

#include <iostream>
#include <fstream>
#include <iomanip>
#include <string>
#include <unicode/regex.h>


#define VERSION_STRING "Version 0.1"


namespace po = boost::program_options;


/*
 * export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:../../lib:. 
 * ./bin/roots2relationgraph "../../script/TestFile/podalydes_ad_0204_t167_lin_m1.xml"
 */

void display_command_line(const char *programName,po::options_description &arguments, std::vector<po::options_description* > &optionsDescVector, std::ostream &out=std::cout)
{
	std::vector< boost::shared_ptr< po::option_description > >::const_iterator iter;

	out << "Syntax: " << std::endl;
	out << "  " << programName << " [options] ";
	for(iter  = arguments.options().begin(); iter != arguments.options().end(); ++iter)
	{
		out << (*iter)->long_name() << " ";
	}
	out << std::endl;
	out << std::endl;

	out << "Arguments: " << std::endl;
	for(iter  = arguments.options().begin(); iter != arguments.options().end(); ++iter)
	{
		out << "  " << (*iter)->long_name() << "\t" << (*iter)->description() << std::endl;
	}
	out << std::endl;

	std::vector< po::options_description* >::const_iterator iter2;
	for(iter2= optionsDescVector.begin(); iter2 != optionsDescVector.end(); ++iter2)
	{
		out << (*(*iter2)) << std::endl << std::endl;
	}

}

int main(int argc, char **argv)
{
	std::string inputFile;
    roots::Utterance *r = NULL;
	roots::Corpus *corpus = NULL;
    roots::RootsStream *rootsStream = NULL;
	int utteranceIndex = 0;
	std::vector<std::string> layers;

	ROOTS_INIT();
	ROOTS_SET_LOGGER_LEVEL(ROOTS_LOGGER_ERROR_LEVEL);

	
	// Declare the supported options.
	std::vector<po::options_description* > optionsDescVector;
	po::options_description generalOptions("Options");
	optionsDescVector.push_back(&generalOptions);
	generalOptions.add_options()
		("index,i", po::value<int>()->default_value(0), "utterance index in the file")
		("layer,l", po::value<std::vector<std::string> >()->default_value(std::vector<std::string>(), ""), "utterance layer in the file")
		("verbose,v", "verbose mode")
		("help", "produce help message")    
		;

	po::options_description arguments("Arguments");
	arguments.add_options()
		("input-file", po::value< std::string >()->required(), "input file (.xml|.json|.roots)")
		;   	

    po::options_description cmdline_options;
	cmdline_options.add(arguments).add(generalOptions);

	po::positional_options_description p;
	p.add("input-file", 1);

	try{
		po::variables_map vm;
		po::store(po::command_line_parser(argc, argv).
				  options(cmdline_options).positional(p).run(), vm);

		if (vm.count("help")) {
			display_command_line(argv[0], arguments, optionsDescVector, std::cout);
			return EXIT_SUCCESS;
		}

		if (vm.count("verbose")) {
			ROOTS_SET_LOGGER_LEVEL(ROOTS_LOGGER_DEBUG_LEVEL);			
		}

		po::notify(vm);
	
		inputFile = vm["input-file"].as< std::string >();	   	  
		utteranceIndex = vm["index"].as< int >();
		std::vector<std::string> stringLayers = vm["layer"].as< std::vector<std::string> >();	   	  
		for(std::vector<std::string>::const_iterator iter=stringLayers.begin(); iter!=stringLayers.end(); ++iter)
		{
			layers.push_back((*iter).c_str());
		}

		if (endsWith(inputFile,".roots"))
		{
			//rootsStream = new RootsStreamBinary();
			std::cerr << "Still not implemented" << std::endl;
			exit(EXIT_FAILURE);
		}
		else
		{
#ifdef USE_STREAM_XML
			if(endsWith(inputFile,".xml"))
			{
                rootsStream = new roots::RootsStreamXML();
			}
			else
			{
#endif
				if(endsWith(inputFile,".json"))
				{
                    rootsStream = new roots::RootsStreamJson();
				}
				else
				{
					std::cerr << "error: unsupported format of ouput file!" << std::endl;
					return EXIT_FAILURE;
				}
#ifdef USE_STREAM_XML
			}
#endif
		}
	}catch(po::error& e)
	{
		std::cerr << "error: " << e.what() << std::endl  << std::endl;
		display_command_line(argv[0], arguments, optionsDescVector, std::cerr);
		return EXIT_FAILURE;
	}

	try
	{	
		rootsStream->load(inputFile);
		corpus = new roots::Corpus();
		corpus->inflate(rootsStream);

		if(layers.size() != 0)
		{
			r = corpus->get_utterance(layers, utteranceIndex);
		}
		else
		{
			r = corpus->get_utterance(utteranceIndex);
		}


		std::cout << r->relation_graph_to_dot() << std::endl;

		delete r;	
		delete corpus;
		delete rootsStream;

    }catch(roots::RootsException e){
		std::cerr << "error: " << e.what() << std::endl;
		exit(EXIT_FAILURE);
	}catch(std::exception e){
		std::cerr << "error: " << e.what() << std::endl;
		exit(EXIT_FAILURE);
	}

	ROOTS_TERMINATE();

	return 0;
}
