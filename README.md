# Compilation & installation
## Prerequisites

prerequisite packages for compiling roots are:

* Core
    * cmake (>= 2.8)
    * g++ (>= 4.9)
    * libicu-dev (>= 55.1)
    * libboost-regex-dev (>= 1.55)
    * libboost-program-options-dev (>= 1.55)
    * libboost-filesystem-dev (>= 1.55)
    * libsox-dev (>= 14.4)
    * libjsoncpp-dev (>= 0.6)
    * autoconf
    * libtool    
    * pkg-config
* Wrapper
    * swig (>= 3.0)
    * python3 (>= 3.3)
    * python3-dev
    * perl (>= 5.16)
* XML support (deprecated)
    * libxerces-c-dev (>= 3.1.1) if using XML 
* Documentation
    * doxygen (and graphviz, optionnaly)

### Ubuntu command line prerequisite packages installation

The installation of these packages is provided by the  following under Ubuntu:
```sh
sudo apt-get install libicu-dev libxerces-c-dev libboost-regex-dev libboost-program-options-dev libboost-filesystem-dev libsox-dev libjsoncpp-dev cmake swig doxygen python3-dev liberror-perl libswitch-perl
```

### MacOS command line prerequisite packages installation
Under OSX, it is easier if you have installed [HomeBrew](http://brew.sh/) .  Then, launch the following command line:
```sh
brew install icu4c xerces-c jsoncpp boost sox cmake swig2 doxygen
```

Then, for some packages, you may have to force links to /usr/local (usually because brew says that those packages are keg-only formulas)
```sh
brew link --force icu4c
```

## Building Roots-cpp library and tools

The following command have to be launched from the root
directory of the roots-cpp source dir (let's call this directory <roots-cpp-dir>).

- To build all the library and the wrapper:
```sh
$ cd <roots-cpp-dir>
$ cmake CMakeLists.txt
$ make roots 			# Make the Roots library
$ make tools 			# Make the tools
$ make plroots			# Make the Perl wrapper
$ make pyroots 			# Make the Python wrapper
$ make roots-install
$ make tools-install
$ make perl-install
$ make python-install
```

or simply, to make and install everything:
```sh
$ cd <roots-cpp-dir>
$ cmake CMakeLists.txt
$ make
$ make install
```

Adapt the prefix directory to where you want to install the library using:
```sh
$ cmake CMakeLists.txt -DCMAKE_INSTALL_PREFIX:PATH=<prefix> -DLOCAL_INSTALL=1
```

Particularly under OSX, you may want to change your compiler. It is possible by using a couple of CMake options like in the following :
```sh
cmake CMakeLists.txt -DCMAKE_C_COMPILER=/usr/bin/gcc -DCMAKE_CXX_COMPILER=/usr/bin/g++
```

The LOCAL_INSTALL option **must be set to 1** to install the wrappers in the "<prefix>/lib" directory.

Other options:

* To activate the XML support, you should add the USE_STREAM_XML option: -DUSE_STREAM_XML=1

* To build the tests: -DBUILD_TESTS=1

## Building documentation

In the roots-cpp directory, launch:
```sh
$ make doc
```
The documentation is then accessible from the doc directory.

# Using roots

## Environment configuration

By default, the prefix used is **/usr/local**. Therefore, you should check that all variables
(indicated next) are consequently set. In case of **local installation**, you should adapt the variables  :

```sh
export PREFIX=/local/slemaguer/env/local # By default /usr/local

# Main path
export PATH=$PREFIX/bin/:$PATH

# Library pathes
export LIBRARY_PATH=$PREFIX/lib:$LIBRARY_PATH
export LD_LIBRARY_PATH=$LIBRARY_PATH
export DYLD_LIBRARY_PATH=$LD_LIBRARY_PATH

# Include pathes
export CPATH=$CPATH:$PREFIX/include
export C_INCLUDE_PATH=$PREFIX/include:$C_INCLUDE_PATH
export CPLUS_INCLUDE_PATH=$PREFIX/include:$CPLUS_INCLUDE_PATH

# Pkg-config part
export PKG_CONFIG_PATH=$PKG_CONFIG_PATH:$PREFIX/lib/pkgconfig

# Wrapper extension
export PYTHONPATH=$PYTHONPATH:$PREFIX/python
export PERL5LIB=$PERL5LIB:$PREFIX/perl
```

## Script examples with roots

Examples of scripts using Roots are given within the samples directory. Detailed explanations are given within the documentation (see doc)

## Using the tools given with roots

The roots2pgf converter can be launched with :
```sh
<install-dir-roots>/roots2pgf  \
        --sequence "Segment"   \
        --sequence "Allophone" \
        --sequence "NonSpeechSound"  \
        "./testfiles/test.json"
```

## Using ROOTS in your software

### C++
You need to respect the following template :

```cpp
/** Set of includes */
#include <roots/roots.h>

/* Some code .... */

int main(int /*argc*/, char ** /*argv*/)
{
    // Initialisation of roots
    ROOTS_INIT();
    ROOTS_SET_LOGGER_LEVEL(ROOTS_LOGGER_ERROR_LEVEL); // Error level => should be adapted !

    /**********************/
    /* Do your duty here! */
    /**********************/

    // Termination roots (free the memory !)
    ROOTS_TERMINATE();

    return EXIT_SUCCESS;
}
```

# How to cite

```bibtex
@inproceedings{chevelu:hal-00974628,
        AUTHOR = {Chevelu, Jonathan and Lecorv{\'e}, Gw{\'e}nol{\'e} and Lolive, Damien},
        TITLE = {{ROOTS: a toolkit for easy, fast and consistent processing of large sequential annotated data collections}},
        BOOKTITLE = {{Language Resources and Evaluation Conference (LREC)}},
        YEAR = {2014},
        ADDRESS = {Reykjavik, Iceland},
        URL = {http://hal.inria.fr/hal-00974628}
}
```