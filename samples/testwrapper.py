#!/usr/bin/python3

import argparse;
import sys;
import os;
import re;

sys.path.append(os.path.dirname(os.path.realpath(__file__)) + "/../../../lib")

import roots;



################# Main program ################
def main():
  
  parser = argparse.ArgumentParser(description='Sample script using Roots'' Python wrapper.')
  parser.add_argument('corpusfile', type=str, nargs=1, help='name of the input corpus file')
  parser.add_argument('-v', '--version', action='version', version='%(prog)s 1.0')
  
  args = parser.parse_args()

  corpusFilename = args.corpusfile[0]
     
  corpus = roots.Corpus()
  corpus.load(corpusFilename)

  # filenames = corpus.get_filenames()
  # print(filenames)
  # print()
  print(roots.phonology_ipa_ArpabetAlphabet.get_instance().is_label("toto"))
  print("After test")

  seg = roots.acoustic_TimeSegment(0.0, 1.0)
  print("timesegment: "+seg.to_string())

  print(roots.phonology_ipa_SampaAlphabet.get_instance().is_label("l"))
  try:
    ipas = roots.phonology_ipa_SampaAlphabet.get_instance().extract_ipas("V1")
    testph = roots.phonology_Phoneme(ipas, roots.phonology_ipa_SampaAlphabet.get_instance())
    testSymb = roots.phonology_ipa_ArpabetAlphabet.get_instance().approximate_phoneme(testph)
    print(testph.to_string()+" -> "+testSymb)
    #ipas = roots.phonology_ipa_ArpabetAlphabet.get_instance().extract_ipas("R")
    #ipas = roots.phonology_ipa_SampaAlphabet.get_instance().extract_ipas("l")
    #testph = roots.phonology_Phoneme(ipas, roots.phonology_ipa_SampaAlphabet.get_instance())
    #testSymb =roots.phonology_ipa_ArpabetAlphabet.get_instance().approximate_phoneme(testph)
    #testSymb2 = "" #roots.phonology_ipa_ArpabetAlphabet.get_instance().convert_ipas(ipas)
    #print(testph.to_string()+" -> "+testSymb+" / "+testSymb2)
  except RuntimeError as e:
    print("RuntimeError:"+str(e))
    

  nbutts = corpus.count_utterances()
  utts = corpus.get_utterances(0, nbutts)
  for utt in utts:
    print("--------------------------------------------")
    lseqs = utt.get_all_sequence_labels()
    for lseq in lseqs:
      print("sequence label: " + lseq)
    # print(utt.get_sequence("Word EN").to_string())
    seqs = utt.get_all_sequences()
    for seq in seqs:
      print("sequence: " + seq.to_string())

    print()
    phSeq = utt.get_sequence("Phoneme FR").as_phoneme_sequence()

    item = phSeq.get_item(1)
    print("item: " + item.to_string())
    ipa = item.get_ipa(0)
    print("ipa: " + ipa.to_string())
    uni = item.get_ipa_unicode(0)
    print("UChar value: " + uni)

    print("is_unicode: " + str(roots.phonology_ipa_Ipa.is_unicode(uni)))
    print("is_unicode: " + str(roots.phonology_ipa_Ipa.is_unicode("a")))
    print("is_unicode: " + str(roots.phonology_ipa_Ipa.is_unicode("/")))
  print("--------------------------------------------")

  fchunk = roots.FileChunk()
  fchunk.add_filename("layer1", "./toto.json")
  fchunk.add_filename("layer2", "./toto2.json")
  fnames = fchunk.get_filenames()
  print(fnames)
  for f in fnames:
    print(f + ": " + fnames[f])
  
########################################################################         

if __name__ == "__main__":
    main() 
