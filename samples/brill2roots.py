#!/usr/bin/python3

import argparse;
import sys;
import re;
import os;

sys.path.append(os.path.dirname(os.path.realpath(__file__)) + "/../../../lib")

import roots;

def load_sentence(line_str, seq_name):
  seq = roots.WordSequence(seq_name)  
  
  line_str.rstrip()
  
  for word in line_str.split():
    word_roots = roots.linguistic_Word(word)
    seq.add(word_roots)
    word_roots.destroy()

  return seq


def new_pos_from_brill(postag):
	pos = None

	if not re.match('W?RB$', postag) is None:
		pos = roots.linguistic_pos_Adverb()
	elif postag == "RBS":
		pos = roots.linguistic_pos_Adverb(roots.POS_DEGREE_SUPERLATIVE)
	elif postag == "RBR":
		pos = roots.linguistic_pos_Adverb(roots.POS_DEGREE_COMPARATIVE)
	elif postag == "JJ":
		pos = roots.linguistic_pos_Adjective()
	elif postag == "JJS":
		pos = roots.linguistic_pos_Adjective(roots.POS_DEGREE_SUPERLATIVE)
	elif postag == "JJR":
		pos = roots.linguistic_pos_Adjective(roots.POS_DEGREE_COMPARATIVE)
	elif postag == "CC":
		pos = roots.linguistic_pos_Conjunction(roots.POS_CONJUNCTION_CATEGORY_COORDINATING)
	elif postag == "PRP":
		pos = roots.linguistic_pos_Pronoun()
	elif postag == "WP":
		pos = roots.linguistic_pos_Pronoun()
	elif not re.match('^[WP]P\$$', postag) is None:
		pos = roots.linguistic_pos_Pronoun(roots.POS_CATEGORY_POSSESSIVE)
	elif not re.match('^[WP]?DT$', postag) is None:
		pos = roots.linguistic_pos_Determiner()
	elif postag == "FW":
		pos = roots.linguistic_pos_ForeignWord()
	elif postag == "UH":
		pos = roots.linguistic_pos_Interjection()
	elif postag == "NN":
		pos = roots.linguistic_pos_Noun(roots.POS_GENDER_UNKNOWN, roots.POS_NUMBER_SINGULAR, 0)
	elif postag == "NNS":
		pos = roots.linguistic_pos_Noun(roots.POS_GENDER_UNKNOWN, roots.POS_NUMBER_PLURAL, 0)
	elif postag == "NNP":
		pos = roots.linguistic_pos_Noun(roots.POS_GENDER_UNKNOWN, roots.POS_NUMBER_SINGULAR, 1)
	elif postag == "NNPS":
		pos = roots.linguistic_pos_Noun(roots.POS_GENDER_UNKNOWN, roots.POS_NUMBER_PLURAL, 1)
	elif postag == "VB":
		pos = roots.linguistic_pos_Verb()
	elif postag == "IN":
		pos = roots.linguistic_pos_Preposition()
	elif postag == "TO":
		pos = roots.linguistic_pos_Preposition()
	elif postag == "VB":
		pos = roots.linguistic_pos_Verb()
	elif postag == "VBD":
		pos = roots.linguistic_pos_Verb()
		pos.set_tense(roots.POS_TENSE_PAST)
	elif postag == "VBG":
		pos = roots.linguistic_pos_Verb()
		pos.set_mood(roots.POS_MOOD_GERUND)
	elif postag == "VBN":
		pos = roots.linguistic_pos_Verb()
		pos.set_mood(roots.POS_MOOD_PARTICIPLE)
		pos.set_tense(roots.POS_TENSE_PAST)
	elif postag == "VBP":
		pos = roots.linguistic_pos_Verb()
		pos.set_tense(roots.POS_TENSE_PRESENT)
	elif postag == "VBZ":
		pos = roots.linguistic_pos_Verb()
		pos.set_tense(roots.POS_TENSE_PRESENT)
		pos.set_person(roots.POS_PERSON_THIRD)
		pos.set_number(roots.POS_NUMBER_SINGULAR)
	elif postag == ".":
		pos = roots.linguistic_pos_Punctuation(roots.POS_PUNCTUATION_CATEGORY_FINAL)
	elif not re.match('^[,:]$', postag) is None:
		pos = roots.linguistic_pos_Punctuation(roots.POS_PUNCTUATION_CATEGORY_PAUSE)
	else:
		raise Exception("error: unknown postag value <%s>\n" % postag)
	
	return pos

def read_brill_tagging(line_brill):
	
	seq_words = roots.WordSequence("Word POS EN") 	# Words as tokenized by the POS tagger
	seq_pos = roots.PosSequence("POS EN") 			# POS tags
	rel = roots.Relation(seq_words, seq_pos)
	
	line_brill.rstrip()
	word_pos_list = line_brill.split()

	for pair_str in word_pos_list:
		items = pair_str.split("/")
		word_str = items[0]
		pos_str = items[1]

		#print("{0} -- {1}".format(word_str, pos_str) )

		word_roots = roots.linguistic_Word(word_str)
		pos_roots = new_pos_from_brill(pos_str)
		i = seq_words.add(word_roots)
		j = seq_pos.add(pos_roots)
		rel.link(i, j)
		word_roots.destroy()
		pos_roots.destroy()
	return (seq_words, seq_pos, rel)



################# Main program ################
def main():
  
  parser = argparse.ArgumentParser(description='Sample script using Roots'' Python wrapper.')
  parser.add_argument('rawfile', type=str, nargs=1, help='Input 1: English raw sentences')
  parser.add_argument('posfile', type=str, nargs=1, help='Input 2: English POS-tagged sentences')
  parser.add_argument('corpusfile', type=str, nargs=1, help='name of the output corpus file')
  parser.add_argument('-v', '--version', action='version', version='%(prog)s 1.0')
  
  args = parser.parse_args()

  rawFilename = args.rawfile[0]
  posFilename = args.posfile[0]
  corpusFilename = args.corpusfile[0]
 
  txtlines = None
  phnlines = None
  
  try:
    with open(rawFilename, 'r') as f:
        rawlines = f.readlines()

    with open(posFilename, 'r') as f:
        poslines = f.readlines()
  except:
    print("Error: can\'t open one of input files!")
    return
    
  corpus = roots.Corpus()

  for index in range(0, len(rawlines)):
    print(rawlines[index])
    print(poslines[index])
    
    seq_word_raw_en = load_sentence(rawlines[index], "Word EN")
    (seq_word_pos_en, seq_pos_en, rel_word2pos) = read_brill_tagging(poslines[index])
    rel_word2word = roots.Relation(seq_word_raw_en, seq_word_pos_en, seq_word_raw_en.make_mapping(seq_word_pos_en))
	
    utt = roots.Utterance()
    utt.add_sequence(seq_word_raw_en)
    utt.add_sequence(seq_word_pos_en)
    utt.add_sequence(seq_pos_en)
    utt.add_relation(rel_word2pos)
    utt.add_relation(rel_word2word)

    corpus.add_utterance(utt)
    utt.destroy()

  print()

  corpus.save(corpusFilename)
  
########################################################################         

if __name__ == "__main__":
    main() 
