#!/usr/bin/perl

BEGIN {
	use File::Basename;
	use Cwd 'abs_path';
	our $sandbox = abs_path(dirname(__FILE__));
	$sandbox =~ s/^(.*)\/script\/?.*/$1/;
	unless ($ENV{LD_LIBRARY_PATH} =~ m#$sandbox/lib#) {
		$ENV{LD_LIBRARY_PATH} = $ENV{LD_LIBRARY_PATH}.":$sandbox/lib";
		$ENV{PERL5LIB} = $ENV{PERL5LIB}.":$sandbox/ext/perl";
#		exec($^X, $0, @ARGV);
#		exit;
	}
}

use strict;
use warnings;
use utf8;
binmode(STDOUT, ":utf8");
use open qw(:std :utf8);

use Getopt::Long qw(:config no_ignore_case);
use File::Temp qw/tempdir tempfile/;
use IO::File;
use Cwd qw/realpath/;
use Data::Dumper;

use roots;

$|++; # Auto-flush


#############################################################
my $BDLEX = roots::phonology_ipa_BdlexSampaAlphabet::get_instance();
my $LIAPHON = roots::phonology_ipa_LiaphonAlphabet::get_instance();
my $SAMPA = roots::phonology_ipa_SampaAlphabet::get_instance();
my $ARPABET = roots::phonology_ipa_ArpabetAlphabet::get_instance();
my $IPA = new roots::phonology_ipa_Ipa();

#my $SOURCE = $ARPABET;
#my $SOURCE = $LIAPHON;
#my $SOURCE = $SAMPA;
my $SOURCE = $IPA;
my $TARGET = $LIAPHON;
#my $TARGET = $ARPABET;
#my $TARGET = $SAMPA;

if (defined $ARGV[0] && $ARGV[0] ne "") {
	foreach my $sym ($ARGV[0]) {
		my @array = @{$SOURCE->extract_ipas($sym)};
		print "${sym} --[".""."]-> ".join(" ", map { $_->to_string(1) } @array)."\n";
		# my @array = shift($BDLEX->extract_ipas("E"));
		my $str = $TARGET->approximate_ipas(\@array, " ");
		
		@array = ();
		foreach my $s (split(/ /, $str)) {
			push(@array, @{$TARGET->extract_ipas($s)});
		}
		print join(" ", map { $_->to_string(1) } @array)." --[".$TARGET->get_name()."]-> $str\n";
	}
}
else {
	print " -> ".$TARGET->get_name()."\n";
	print "---------------------------------\n";
	
	my @symbols = @{$SOURCE->get_alphabet_set()};
	foreach my $sym (@symbols) {
		#print STDERR "Trying $sym\n";
	        #print STDERR $SOURCE->extract_ipas($sym)."\n";
		my @array = @{$SOURCE->extract_ipas($sym)};
		
		my $srcipastr = join(" ", map { $_->to_string(0) } @array);

		my $str = $TARGET->approximate_ipas(\@array, " ");
		
		@array = ();
		foreach my $s (split(/ /, $str)) {
			push(@array, @{$TARGET->extract_ipas($s)});
		}
		my $tgtipastr = join(" ", map { $_->to_string() } @array);
		#if($tgtipastr ne $srcipastr)
		{
				print "${sym} (".$srcipastr.") --> ";
				print " $str (".$tgtipastr.")\n";
		}
	}
}

# my $ipa1 = shift @{$ARPABET->extract_ipas("UH")};
# my $ipa2 = shift @{$LIAPHON->extract_ipas("au")};
# print $ipa1->to_string(1)."\n";
# print $ipa2->to_string(1)."\n";
# print $ipa1->compute_distance($ipa2)."\n";

