#!/usr/bin/perl

# Simply to find the library, this can be removed if reference in environment variable PERL5LIB
use File::Basename;
use lib dirname( __FILE__ )."/..";
BEGIN { if(-l __FILE__){unshift @INC, dirname( __FILE__ )."/".dirname( readlink __FILE__ )."/..";} }

use strict;
use utf8;

use roots;

my $file_en = shift(@ARGV); # Input 1: English sentences
my $file_fr = shift(@ARGV); # Input 2: French sentences
my $file_alg = shift(@ARGV); # Input 3: Giza alignment file
my $file_corpus = shift(@ARGV); # Output: Roots corpus

# Functions

sub load_sentence {
	my $line_str = shift; # String of the sentence (words are separated by blanks)
	my $seq_name = shift; # Name of the sequence to be built and returned
	
	my $seq = new roots::WordSequence($seq_name);
	
	chomp $line_str;
	my @words = split(/\s+/, $line_str);
	foreach my $word_str (@words) {
		my $word_roots = new roots::linguistic_Word($word_str);
		$seq->add($word_roots);
		$word_roots->destroy();
	}
	
	return $seq;
}

sub read_giza_alignment {
	my $seq_en = shift;
	my $seq_fr = shift;
	my $line_giza = shift;
	
	my $rel = new roots::Relation($seq_en, $seq_fr);
	
	chomp $line_giza;
	my @alignments = split(/\s+/, $line_giza);
	foreach my $pair_str (@alignments) {
		my ($i,$j) = $pair_str =~ /^(\d+)-(\d+)$/;
		$rel->link($i, $j);
	}
	
	return $rel;
}

# Functions

my $corpus = new roots::Corpus();

open(E, $file_en) or die("Unable to read English sentences in \"$file_en\".");
open(F, $file_fr) or die("Unable to read French sentences in \"$file_fr\".");
open(A, $file_alg) or die("Unable to read sentence alignments in \"$file_alg\".");
# The 3 files are supposed to contain the same number of lines
while(my $line_en = <E>) {
	my $line_fr = <F>;
	my $line_alg = <A>;
	
	my $seq_word_en = load_sentence($line_en, "Word EN");
	my $seq_word_fr = load_sentence($line_fr, "Word FR");
	my $rel_en2fr = read_giza_alignment($seq_word_en, $seq_word_fr, $line_alg);
	
	my $utt = new roots::Utterance();
	$utt->add_sequence($seq_word_en);
	$utt->add_sequence($seq_word_fr);
	$utt->add_relation($rel_en2fr);
	$corpus->add_utterance($utt);
	$utt->destroy();
}
close(E);
close(F);
close(A);

$corpus->save($file_corpus);
