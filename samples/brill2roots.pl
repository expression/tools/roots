#!/usr/bin/perl

# Simply to find the library, this can be removed if reference in environment variable PERL5LIB
use File::Basename;
use lib dirname( __FILE__ )."/..";
BEGIN { if(-l __FILE__){unshift @INC, dirname( __FILE__ )."/".dirname( readlink __FILE__ )."/..";} }

use strict;
use utf8;

use roots;

my $file_raw = shift(@ARGV); # Input 1: English raw sentences
my $file_pos = shift(@ARGV); # Input 2: English POS-tagged sentences
my $file_corpus = shift(@ARGV); # Output: Roots corpus

# Functions

sub load_sentence {
	my $line_str = shift; # String of the sentence (words are separated by blanks)
	my $seq_name = shift; # Name of the sequence to be built and returned
	
	my $seq = new roots::WordSequence($seq_name);
	
	chomp $line_str;
	my @words = split(/\s+/, $line_str);
	foreach my $word_str (@words) {
		my $word_roots = new roots::linguistic_Word($word_str);
		$seq->add($word_roots);
		$word_roots->destroy();
	}
	
	return $seq;
}

sub new_pos_from_brill($)
{
    my $postag = shift;
    my $pos;

	if($postag =~ /^W?RB$/) {
		$pos = new roots::linguistic_pos_Adverb();
	}
	elsif($postag eq "RBS") {
		$pos = new roots::linguistic_pos_Adverb($roots::POS_DEGREE_SUPERLATIVE);
	}
	elsif($postag eq "RBR") {
		$pos = new roots::linguistic_pos_Adverb($roots::POS_DEGREE_COMPARATIVE);
	}
	elsif ($postag eq "JJ") {
		$pos = new roots::linguistic_pos_Adjective();
	}
	elsif ($postag eq "JJS") {
		$pos = new roots::linguistic_pos_Adjective($roots::POS_DEGREE_SUPERLATIVE);
	}
	elsif ($postag eq "JJR") {
		$pos = new roots::linguistic_pos_Adjective($roots::POS_DEGREE_COMPARATIVE);
	}
	elsif ($postag eq "CC") {
		$pos = new roots::linguistic_pos_Conjunction($roots::POS_CONJUNCTION_CATEGORY_COORDINATING);
	}
	elsif ($postag eq "PRP") {
		$pos = new roots::linguistic_pos_Pronoun();
	}
	elsif ($postag eq "WP") {
		$pos = new roots::linguistic_pos_Pronoun();
	}
	elsif ($postag =~ /^[WP]P\$$/) {
		$pos = new roots::linguistic_pos_Pronoun($roots::POS_CATEGORY_POSSESSIVE);
	}
	elsif ($postag =~ /^[WP]?DT$/) {
		$pos = new roots::linguistic_pos_Determiner();
	}
	elsif ($postag eq "FW") {
		$pos = new roots::linguistic_pos_ForeignWord();
	} 
	elsif ($postag eq "UH") {
		$pos = new roots::linguistic_pos_Interjection();
	}
	elsif ($postag eq "NN") {
		$pos = new roots::linguistic_pos_Noun($roots::POS_GENDER_UNKNOWN, $roots::POS_NUMBER_SINGULAR, 0);
	}
	elsif ($postag eq "NNS") {
		$pos = new roots::linguistic_pos_Noun($roots::POS_GENDER_UNKNOWN, $roots::POS_NUMBER_PLURAL, 0);
	}
	elsif ($postag eq "NNP") {
		$pos = new roots::linguistic_pos_Noun($roots::POS_GENDER_UNKNOWN, $roots::POS_NUMBER_SINGULAR, 1);
	}
	elsif ($postag eq "NNPS") {
		$pos = new roots::linguistic_pos_Noun($roots::POS_GENDER_UNKNOWN, $roots::POS_NUMBER_PLURAL, 1);
	}elsif ($postag eq "VB") {
		$pos = new roots::linguistic_pos_Verb();
	}
	elsif ($postag eq "IN") {
		$pos = new roots::linguistic_pos_Preposition();
	}
	elsif ($postag eq "TO") {
		$pos = new roots::linguistic_pos_Preposition();
	}
	elsif ($postag eq "VB") {
		$pos = new roots::linguistic_pos_Verb();
	}
	elsif ($postag eq "VBD") {
		$pos = new roots::linguistic_pos_Verb();
		$pos->set_tense($roots::POS_TENSE_PAST);
	}
	elsif ($postag eq "VBG") {
		$pos = new roots::linguistic_pos_Verb();
		$pos->set_mood($roots::POS_MOOD_GERUND);
	}
	elsif ($postag eq "VBN") {
		$pos = new roots::linguistic_pos_Verb();
		$pos->set_mood($roots::POS_MOOD_PARTICIPLE);
		$pos->set_tense($roots::POS_TENSE_PAST);
	}
	elsif ($postag eq "VBP") {
		$pos = new roots::linguistic_pos_Verb();
		$pos->set_tense($roots::POS_TENSE_PRESENT);
	}
	elsif ($postag eq "VBZ") {
		$pos = new roots::linguistic_pos_Verb();
		$pos->set_tense($roots::POS_TENSE_PRESENT);
		$pos->set_person($roots::POS_PERSON_THIRD);
		$pos->set_number($roots::POS_NUMBER_SINGULAR);
	}
	elsif ($postag eq ".") {
		$pos = new roots::linguistic_pos_Punctuation($roots::POS_PUNCTUATION_CATEGORY_FINAL);
	}
	elsif ($postag =~ /^[,:]$/) {
		$pos = new roots::linguistic_pos_Punctuation($roots::POS_PUNCTUATION_CATEGORY_PAUSE);
	}
	else  {
		die "error: unknown postag value <$postag>, $?\n";
	}
	
    return $pos;
}

sub read_brill_tagging {
	my $line_brill = shift;
	
	my $seq_words = new roots::WordSequence("Word POS EN"); # Words as tokenized by the POS tagger
	my $seq_pos = new roots::PosSequence("POS EN"); # POS tags
	my $rel = new roots::Relation($seq_words, $seq_pos);
	
	chomp $line_brill;
	my @word_pos_list = split(/\s+/, $line_brill);
	foreach my $pair_str (@word_pos_list) {
		my ($word_str,$pos_str) = $pair_str =~ /^(.+)\/(.+?)$/;
		my $word_roots = new roots::linguistic_Word($word_str);
		my $pos_roots = new_pos_from_brill($pos_str);
		my $i = $seq_words->add($word_roots);
		my $j = $seq_pos->add($pos_roots);
		$rel->link($i, $j);
		$word_roots->destroy();
		$pos_roots->destroy();
	}
	
	return ($seq_words, $seq_pos, $rel);
}

# Main

my $corpus = new roots::Corpus();

open(R, $file_raw) or die("Unable to read raw text sentences in \"$file_raw\".");
open(P, $file_pos) or die("Unable to read POS-tagged sentences in \"$file_pos\".");
# The 2 files are supposed to contain the same number of lines
while(my $line_raw = <R>) {
	my $line_pos = <P>;
	my $seq_word_raw_en = load_sentence($line_raw, "Word EN"); # Raw words
	my ($seq_word_pos_en, $seq_pos_en, $rel_word2pos) = read_brill_tagging($line_pos); 
	my $rel_word2word = new roots::Relation($seq_word_raw_en, $seq_word_pos_en, $seq_word_raw_en->make_mapping($seq_word_pos_en));
	
	my $utt = new roots::Utterance();
	$utt->add_sequence($seq_word_raw_en);
	$utt->add_sequence($seq_word_pos_en);
	$utt->add_sequence($seq_pos_en);
	$utt->add_relation($rel_word2pos);
	$utt->add_relation($rel_word2word);
	$corpus->add_utterance($utt);
	$utt->destroy();
}
close(R);
close(P);

$corpus->save($file_corpus);
