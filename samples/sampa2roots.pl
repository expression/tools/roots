#!/usr/bin/perl

# Simply to find the library, this can be removed if reference in environment variable PERL5LIB
use File::Basename;
use lib dirname( __FILE__ )."/..";
BEGIN { if(-l __FILE__){unshift @INC, dirname( __FILE__ )."/".dirname( readlink __FILE__ )."/..";} }

use strict;
use utf8;

use roots;

my $file_txt = shift(@ARGV); # Input 1: text sentence
my $file_phn = shift(@ARGV); # Input 2: phn sentence (no NSS)
my $file_corpus = shift(@ARGV); # Output: Roots corpus

# Functions

sub load_sentence {
    my $line_str = shift; # String of the sentence (words are separated by blanks)
    my $seq_name = shift; # Name of the sequence to be built and returned

    my $seq = new roots::WordSequence($seq_name);

    chomp $line_str;
    my @words = split(/\s+/, $line_str);
    foreach my $word_str (@words) {
	my $word_roots = new roots::linguistic_Word($word_str);
	$seq->add($word_roots);
	$word_roots->destroy();
    }

    return $seq;
}

sub read_phn {
    my $utt = shift;
    my $seq_txt = shift;
    my $line_phn = shift;

    my $seq_phn = new roots::PhonemeSequence("Phoneme FR");
    my $rel = new roots::Relation($seq_phn, $seq_txt);

    my @elems = split(/\s+/,$line_phn);

    my $sampaAlphabet = roots::phonology_ipa_SampaAlphabet::get_instance();
    my $wordId = 0;
    foreach my $elem (@elems)
    {
	if($elem eq "|") # New word separator
	{ $wordId ++; }
	else # It is a phoneme
	{
	    my @ipas = @{$sampaAlphabet->extract_ipas($elem)};
	    my $phn = new roots::phonology_Phoneme(\@ipas, $sampaAlphabet);
	    map { $_->destroy() } @ipas;
	    my $phnId = $seq_phn->add($phn);

	    # link it
	    $rel->link($phnId, $wordId);
	}
    }
    $utt->add_sequence($seq_phn);
    $utt->add_relation($rel);
}

# Main

my $corpus = new roots::Corpus();

open(T, $file_txt) or die("Unable to read English sentences in \"$file_txt\".");
open(P, $file_phn) or die("Unable to read French sentences in \"$file_phn\".");

# The 3 files are supposed to contain the same number of lines
while(my $line_T = <T>) {
    my $line_P = <P>;

    my $seq_word_fr = load_sentence($line_T, "Word FR");

    my $utt = new roots::Utterance();
    $utt->add_sequence($seq_word_fr);

    read_phn($utt, $seq_word_fr, $line_P);

    $corpus->add_utterance($utt);
    $utt->destroy();
}
close(T);
close(P);

$corpus->save($file_corpus);
