#!/usr/bin/python3

import argparse;
import sys;
import os;

sys.path.append(os.path.dirname(os.path.realpath(__file__)) + "/../../../lib")

import roots;

def load_sentence(line_str, seq_name):
  seq = roots.WordSequence(seq_name)  
  
  line_str.rstrip()
  
  for word in line_str.split():
    word_roots = roots.linguistic_Word(word)
    seq.add(word_roots)
    word_roots.destroy()

  return seq


def read_phn(utt, seq_txt, line_phn):
  seq_phn = roots.PhonemeSequence("Phoneme FR")
  rel = roots.Relation(seq_phn, seq_txt)

  elems = line_phn.split()

  sampaAlphabet = roots.phonology_ipa_SampaAlphabet.get_instance()

  wordId = 0

  for elem in elems:
    if elem == "|":
      wordId += 1
    else:
      ipas = sampaAlphabet.extract_ipas(elem)
      phn = roots.phonology_Phoneme(ipas, sampaAlphabet)
      for ipa in ipas:
        ipa.destroy()
      phnId = seq_phn.add(phn)
      rel.link(phnId, wordId)

  utt.add_sequence(seq_phn)
  utt.add_relation(rel)

################# Main program ################
def main():
  
  parser = argparse.ArgumentParser(description='Sample script using Roots'' Python wrapper.')
  parser.add_argument('txtfile', type=str, nargs=1, help='name of the input text file')
  parser.add_argument('phnfile', type=str, nargs=1, help='name of the input phn file (no NSS)')
  parser.add_argument('corpusfile', type=str, nargs=1, help='name of the output corpus file')
  parser.add_argument('-v', '--version', action='version', version='%(prog)s 1.0')

  args = parser.parse_args()

  txtFilename = args.txtfile[0]
  phnFilename = args.phnfile[0]
  corpusFilename = args.corpusfile[0]
 
  txtlines = None
  phnlines = None
  
  try:
    with open(txtFilename, 'r') as ftxt:
        txtlines = ftxt.readlines()

    with open(phnFilename, 'r') as fphn:
        phnlines = fphn.readlines()
  except:
    print("Error: can\'t open one of input files!")
    return
    
  corpus = roots.Corpus()

  for index in range(0, len(txtlines)):
    print(txtlines[index])
    print(phnlines[index])
    
    seq_word_fr = load_sentence(txtlines[index], "Word FR")

    utt = roots.Utterance()
    utt.add_sequence(seq_word_fr)

    read_phn(utt, seq_word_fr, phnlines[index])

    corpus.add_utterance(utt)
    utt.destroy()

  print()

  corpus.save(corpusFilename)
  
########################################################################         

if __name__ == "__main__":
    main() 
