#!/bin/bash

echo -n "Building phonetic layer ." && \
perl sampa2roots.pl data/sample1.fr data/sample1.fr.sampa data/sample1_phonetics.json && \
echo "..  [OK]";

echo -n "Building translation layer ." && \
perl giza2roots.pl data/sample1.en data/sample1.fr data/sample1.giza data/sample1_translation.json && \
echo ".. [OK]";

echo -n "Building linguistic layer ." && \
perl brill2roots.pl data/sample1.en data/sample1.en.brill data/sample1_linguistics.json && \
echo "..  [OK]";

echo -n "Building merged corpus ." && \
../script/merge-corpora.pl \
-l "Phonologic" data/sample1_phonetics.json \
-l "Linguistic" data/sample1_linguistics.json \
-l "Translation" data/sample1_translation.json \
data/sample1_corpus.json  && \
echo "..     [OK]";


echo -n "Building PDF ." && \
../script/roots2pdf.pl data/sample1_phonetics.json ./demo/ -l -u -1 && \
../script/roots2pdf.pl data/sample1_translation.json ./demo/ -l -u -1 && \
../script/roots2pdf.pl data/sample1_linguistics.json ./demo/ -l -u -1 && \
../script/roots2pdf.pl data/sample1_corpus.json ./demo/ -l -s "POS EN/0" -s "Word POS EN/0" -s "Word EN/0" -s "Word FR/0" -s "Phoneme FR/0" -u -1 && \
echo "..               [OK]";

#okular ./demo/sample1_phonetics.json_0.pdf 2> /dev/null &
#okular ./demo/sample1_linguistics.json_0.pdf 2> /dev/null &
#okular ./demo/sample1_translation.json_0.pdf  2> /dev/null &
#okular ./demo/sample1_corpus.json_0.pdf 2> /dev/null &

