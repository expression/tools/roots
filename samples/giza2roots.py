#!/usr/bin/python3

import argparse;
import sys;
import os;
import re;

sys.path.append(os.path.dirname(os.path.realpath(__file__)) + "/../../../lib")

import roots;

def load_sentence(line_str, seq_name):
  seq = roots.WordSequence(seq_name)  
  
  line_str.rstrip()
  
  for word in line_str.split():
    word_roots = roots.linguistic_Word(word)
    seq.add(word_roots)
    word_roots.destroy()

  return seq

def read_giza_alignment(seq_en, seq_fr, line_giza):
	
	rel = roots.Relation(seq_en, seq_fr)
	line_giza.rstrip()
	
	alignments = line_giza.split()
	for pair_str in alignments:
		items = pair_str.split("-")
		i = items[0]
		j = items[1]
		rel.link(int(i), int(j))
	
	return rel


################# Main program ################
def main():
  
  parser = argparse.ArgumentParser(description='Sample script using Roots'' Python wrapper.')
  parser.add_argument('enfile', type=str, nargs=1, help='Input 1: English sentences')
  parser.add_argument('frfile', type=str, nargs=1, help='Input 2: French sentences')
  parser.add_argument('alfile', type=str, nargs=1, help='Input 3: Giza alignment file')
  parser.add_argument('corpusfile', type=str, nargs=1, help='name of the output corpus file')
  parser.add_argument('-v', '--version', action='version', version='%(prog)s 1.0')
  
  args = parser.parse_args()

  enFilename = args.enfile[0]
  frFilename = args.frfile[0]
  alFilename = args.alfile[0]
  corpusFilename = args.corpusfile[0]
 
  txtlines = None
  phnlines = None
  
  try:
    with open(enFilename, 'r') as f:
        enlines = f.readlines()

    with open(frFilename, 'r') as f:
        frlines = f.readlines()

    with open(alFilename, 'r') as f:
        allines = f.readlines()
  except:
    print("Error: can\'t open one of input files!")
    return
    
  corpus = roots.Corpus()

  for index in range(0, len(enlines)):
    print(enlines[index])
    print(frlines[index])
    print(allines[index])

    seq_word_en = load_sentence(enlines[index], "Word EN")
    seq_word_fr = load_sentence(frlines[index], "Word FR")
    rel_en2fr = read_giza_alignment(seq_word_en, seq_word_fr, allines[index])

    utt = roots.Utterance()
    utt.add_sequence(seq_word_en)
    utt.add_sequence(seq_word_fr)
    utt.add_relation(rel_en2fr)
    corpus.add_utterance(utt)
    utt.destroy()

  print()

  corpus.save(corpusFilename)
  
########################################################################         

if __name__ == "__main__":
    main() 
