#!/usr/bin/perl

# Simply to find the library, this can be removed if reference in environment variable PERL5LIB
use File::Basename;
use lib dirname( __FILE__ )."/..";
BEGIN { if(-l __FILE__){unshift @INC, dirname( __FILE__ )."/".dirname( readlink __FILE__ )."/..";} }

use strict;
use utf8;

use roots;


my $symbol = new roots::Symbol("titi");
print "--> ".$symbol->to_string(0)."\n";

#my $symbol2 = new roots::linguistic_pos_Symbol();
#print "--> ".$symbol2->to_string(0)."\n";

my $ipa = new roots::phonology_ipa_Ipa();
my $str = "r.ɔ";
print STDERR $str."\n";
my @ipas = @{$ipa->extract_ipas($str)};
print STDERR "nb ipas: ".($#ipas+1)."\n";
# die;

my $corpusFilename = shift(@ARGV); # Output: Roots corpus

my $corpus = new roots::Corpus();
$corpus->load($corpusFilename);

my $nbutts = $corpus->count_utterances();
my $utts = $corpus->get_utterances(0, $nbutts);
foreach my $utt (@{$utts})
{
  print "--------------------------------------------\n";
  my $lseqs = $utt->get_all_sequence_labels();
  foreach my $lseq (@{$lseqs})
  {
    print "sequence label: " . $lseq . "\n";
  }

  my $seqs = $utt->get_all_sequences();
  foreach my $seq (@{$seqs})
  {
    print "sequence: " . $seq->to_string() . "\n";
  }

  print "\n";
  my $phSeq = $utt->get_sequence("Phoneme FR")->as_phoneme_sequence();

  my $item = $phSeq->get_item(1);
  print "item: " . $item->to_string() . "\n";
  my $ipa = $item->get_ipa(0);
  print "ipa: " . $ipa->to_string() . "\n";
  my $uni = $item->get_ipa_unicode(0);
  print "UChar value: " . $uni . "\n";

  print "is_unicode: " . roots::phonology_ipa_Ipa::is_unicode($uni) . "\n";
  print "is_unicode: " . roots::phonology_ipa_Ipa::is_unicode("a") . "\n";
  print "is_unicode: " . roots::phonology_ipa_Ipa::is_unicode("/") . "\n";
}
print "--------------------------------------------\n";

my $fchunk = new roots::FileChunk();
$fchunk->add_filename("layer1", "./toto.json");
$fchunk->add_filename("layer2", "./toto2.json");
my $fnames = $fchunk->get_filenames();
foreach my $f (keys %{$fnames})
{
  print $f . ": " . $fnames->{$f} . "\n";
}

