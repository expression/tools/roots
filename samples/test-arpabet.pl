#!/usr/bin/perl

BEGIN {
	use File::Basename;
	use Cwd 'abs_path';
	our $sandbox = abs_path(dirname(__FILE__));
	$sandbox =~ s/^(.*)\/script\/?.*/$1/;
	unless ($ENV{LD_LIBRARY_PATH} =~ m#$sandbox/lib#) {
		$ENV{LD_LIBRARY_PATH} = $ENV{LD_LIBRARY_PATH}.":$sandbox/lib";
		$ENV{PERL5LIB} = $ENV{PERL5LIB}.":$sandbox/ext/perl";
#		exec($^X, $0, @ARGV);
#		exit;
	}
}

use strict;
use warnings;
use utf8;
binmode(STDOUT, ":utf8");
use open qw(:std :utf8);

use Getopt::Long qw(:config no_ignore_case);
use File::Temp qw/tempdir tempfile/;
use IO::File;
use Cwd qw/realpath/;
use Data::Dumper;

use roots;

$|++; # Auto-flush


#############################################################
my $ARPABET = roots::phonology_ipa_ArpabetAlphabet::get_instance();

my $SOURCE = $ARPABET;

my @symbols = ("EY1", "N", "D", "R", "AO0", "K", "L", "IH2", "Z");
#my @symbols = ("AY1", "ER2", "EYN1", "AO0", "AO", "IH2");

foreach my $sym (@symbols) {
    my @array = @{$SOURCE->extract_ipas($sym)};
    print "${sym} --[".""."]-> ".join(" ", map { $_->to_string(0) } @array)."\n";
    my $all = new roots::phonology_Phoneme(@array, $SOURCE);   
    print "${sym} --[".""."]-> ".$all->to_string(0)."\n";
    #print "is_stressed: ".$all->is_stressed()."\n";
}

# my $ipa1 = shift @{$ARPABET->extract_ipas("UH")};
# my $ipa2 = shift @{$LIAPHON->extract_ipas("au")};
# print $ipa1->to_string(1)."\n";
# print $ipa2->to_string(1)."\n";
# print $ipa1->compute_distance($ipa2)."\n";

