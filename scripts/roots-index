#!/usr/bin/perl

use strict;
use warnings;

use utf8;
binmode(STDOUT, ":utf8");
use open qw(:std :utf8);

use Getopt::Long qw(:config no_ignore_case);

use File::Spec qw(abs2rel);

use Data::Dumper;

use roots;

$|++; # autoflush

my $OPT_HELP = 0;
my $OPT_VERBOSE = 0;
my @OPT_KEY_NAMES = ();
my $OPT_KEY_SEPARATOR = "::";
my $OPT_ID_SEPARATOR = " ";
my $OPT_COLUMN_SEPARATOR = "\t";

#############################################################
sub usage()
{
print <<EOF;
________________________________________________
Usage:
	perl $0 [options] --key <seq_name_1> [ --key <seq_name_2> [...] <corpus>

Synopsis:
	Index a corpus according one or several key sequences, ie, list the the ID of utterance matching each key.

Options:
	-h, --help            Print this message ;-).
	-k, --key=str         Name of a sequence to be used as a key.
	-K, --key-sep=str     Separator between multiple concatenated keys. Default is "$OPT_KEY_SEPARATOR".
	-I, --id-sep=str      Separator between multiple IDs sharing a same key. Default is "$OPT_ID_SEPARATOR".
	-C, --column-sep=str  Separator between a key and its associated IDs. Default is "$OPT_COLUMN_SEPARATOR".
	-v, --verbose         Print debug information .

________________________________________________
EOF
}

#############################################################


GetOptions (
    'help|h' => \$OPT_HELP,
    'verbose|v' => \$OPT_VERBOSE,
    'key|k=s' => \@OPT_KEY_NAMES,
	'key-sep|K=s' => \$OPT_KEY_SEPARATOR,
	'id-sep|I=s' => \$OPT_ID_SEPARATOR,
	'column-sep|C=s' => \$OPT_COLUMN_SEPARATOR
    );


die usage if(@ARGV != 1) or (@OPT_KEY_NAMES == 0);

if ($OPT_HELP) {
	usage();
	exit(0);
}

if(!$OPT_VERBOSE) {
	roots::ROOTS_SET_LOGGER_LEVEL($roots::ROOTS_LOGGER_ERROR_LEVEL);
}

#############################################################

sub push_in_hash {
	my $p_hash = shift;
	my $k = shift;
	my $v = shift;
	if (!defined(${$p_hash}{$k})) {
		my @array = ();
		${$p_hash}{$k} = \@array;
	}
	push(@{${$p_hash}{$k}}, $v);
}

sub compute_key {
	my $utt = shift;
	my @key_parts;
	foreach my $seq_name (@OPT_KEY_NAMES) {
		if ($utt->is_valid_sequence($seq_name)) {
			push(@key_parts, $utt->get_sequence($seq_name)->to_string());
		}
		else {
			die("Invalid sequence \"$seq_name\" for the following utterance:\n".$utt->to_string());
		}
	}
	return join($OPT_KEY_SEPARATOR, @key_parts);
}

sub save_index {
	my $p_hash = shift;
	
	foreach my $key (sort(keys(%{$p_hash}))) {
		print "$key$OPT_COLUMN_SEPARATOR".join($OPT_ID_SEPARATOR, @{$$p_hash{$key}})."\n";
	}
}

#############################################################

my $f = shift(@ARGV);
my $corpus = new roots::Corpus($f);
my %key2id = ();

my $n_utt = $corpus->count_utterances();
my $key = "";
for (my $i_utt = 0; $i_utt < $n_utt; $i_utt++) {
	my $utt = $corpus->get_utterance($i_utt);
	$key = compute_key($utt);
	push_in_hash(\%key2id, $key, $i_utt);
	$utt->destroy();
	if ($OPT_VERBOSE && $i_utt % 100 == 0) {
		print STDERR "$i_utt / $n_utt utterances...\r";
	}
}

save_index(\%key2id);
if ($OPT_VERBOSE) { print STDERR "Indexing finished.\n"; }
