package roots_perl_utils;

use strict;
use warnings;
use Error qw(:try);

use roots;
use utf8;

use Data::Dumper; #DEBUG

use List::Util qw(first max maxstr min minstr reduce shuffle sum);

our @ISA= qw( Exporter );
our @EXPORT = qw(process_corpus);
our @EXPORT_OK = qw(process_corpus);

sub process_corpus
{

    my $corpus = shift;
    my $ref_function = shift;
    my $blockSize = -1;
    $blockSize = shift if @_; # use -1 tuo use default
    my $headUtt = -1;
    $headUtt = shift if @_; # use -1 tuo use default
    my $parallel = 0;
    $parallel = shift if @_; # use -1 tuo use default
    my $verbose = 0;
    $verbose = shift if @_;

    my $nbUtt = int($corpus->count_utterances());
    $nbUtt = min ($nbUtt, $headUtt) if($headUtt > -1);
    $blockSize = $nbUtt if ($blockSize < 0);
 
    if($parallel)
    {
	for(my $i = 0; $i < int($nbUtt/$blockSize); $i ++)
	{
	    my $currentI = int($i*$blockSize);
	    my @utts = @{$corpus->get_utterances($currentI, $blockSize)};

	    foreach my $utt (@utts)
	    {
		# $utt->set_use_cached_relation(1); # should be done inside $ref_function
		if($verbose && ($currentI % ($nbUtt/100 +1) == 0))
		{ print STDERR "\rLooking ".($currentI)."/".($nbUtt-1)."  ";}
		$ref_function->($utt);
		# $utt->destroy(); # should be done inside $ref_function
		$currentI ++;
	    }

# for // perl
=head
		my $maxProcs = 5;
	    my $pl = Parallel::Loops->new($maxProcs);
	    $pl->share( \%resAll );
	    $pl->foreach(\@utts, sub
			 {
			     my $utt = $_;
			     if($currentI % ($nbUtt/100 +1) == 0)
			     { print STDERR "\rLooking ".($currentI)."/".($nbUtt-1);}
			     get_words($utt, $phn1, $phn2, $level, \%resAll);
			     $utt->destroy();
			     $currentI ++;
			 }
		);
=cut
	}

	if($nbUtt%$blockSize != 0)
	{
	    print STDERR "\r End Looking ".((int($nbUtt/$blockSize))*$blockSize)."/".($nbUtt-1) if($verbose);
	    my @utts = @{$corpus->get_utterances((int($nbUtt/$blockSize))*$blockSize, $nbUtt%$blockSize )};
	    foreach my $utt (@utts)
	    {
		# $utt->set_use_cached_relation(1); # should be done inside $ref_function
		$ref_function->($utt);
		# $utt->destroy(); # should be done inside $ref_function
	    }
	}
    }
    else
    {
	for(my $i = 0; $i < $nbUtt; $i ++)
	{ 
	    if($verbose && ($i % ($nbUtt/100 +1) == 0))
	    { print STDERR "\rLooking ".($i)."/".($nbUtt-1)."  ";}

	    my $utt = $corpus->get_utterance($i);
	    $ref_function->($utt);
	}
    }
    print STDERR "\n" if($verbose);
}
