#!/usr/bin/perl

# Do not forget to modify LD_LIBRARY_PATH with:
#   export LD_LIBRARY_PATH=..:$LD_LIBRARY_PATH
# You need also libroots.so accessible by the library path
#

BEGIN {
    use File::Basename;
    if(-l __FILE__){
        unshift @INC, dirname( readlink __FILE__ )."/..";
        unshift @INC, dirname( __FILE__ )."/".dirname( readlink __FILE__
)."/..";}
    else
    {use lib dirname( __FILE__ )."/..";}
}

use strict;
use warnings;

use utf8;
binmode(STDOUT, ":utf8");
use open qw(:std :utf8);

use Getopt::Long;

use File::Find qw(finddepth);

use Data::Dumper;

use roots;

#############################################################


my $OPT_HELP = 0;
my $OPT_STDOUT = 0;
my $OPT_VERBOSE = 0;

sub usage()
{
print <<EOF;
________________________________________________
Usage:
  perl $0 [options] <layer_1> [ <layer_2> [...] ] <output_corpus>

Synopsis:
  Merge roots files into one chunk and create the associated corpus.

Options:
  -h, --help          Print this message ;-).
  -s, --stdout        Display in STDOUT instead of printing into output_corpus.
  -v, --verbose       Print debug information.

________________________________________________
EOF
}


GetOptions (
	'help|h' => \$OPT_HELP,
	'stdout|s' => \$OPT_STDOUT,
	'verbose|v' => \$OPT_VERBOSE,
	);

die usage if(@ARGV<1);

if ($OPT_HELP) {
	usage();
	exit(0);
}

if(!$OPT_VERBOSE) {
	roots::ROOTS_SET_LOGGER_LEVEL($roots::ROOTS_LOGGER_ERROR_LEVEL);
}

#############################################################
# MAIN
#############################################################

my $merged_corpus = new roots::Corpus();

my %layers;
my $id = 0;

my $output_corpus = pop @ARGV;
my ($out_bn, $out_dn, $out_ext) = fileparse($output_corpus, qr/\.[^.]*/);

# Relative dir name according to the dirname of the output corpus
my $delta = $out_dn;
# Test if layer files are in subdirectories of the output corpus
my $subdir = 1;
foreach my $f (@ARGV) 
{
    chomp $f;
	my ($f_bn, $f_dn, $f_ext) = fileparse($f, qr/\.[^.]*/);
	if ($f_dn !~ /^($out_dn)(.*)$/) { $subdir = 0; last; }
}
# List relative path for each layer
foreach my $f (@ARGV) 
{
    chomp $f;
    if($OPT_VERBOSE){print STDERR "$f\n";}
    if ($subdir) {
		my ($f_bn, $f_dn, $f_ext) = fileparse($f, qr/\.[^.]*/);
		$f_dn =~ s:^($out_dn)(.*)$:./:;
		$layers{$id} = "$f_dn/$f_bn$f_ext";
	}
	else {
		$layers{$id} = $f;
		$delta = "";
	}
	print "$layers{$id}\n";
    $id += 1;
}
$merged_corpus->new_file_chunk(\%layers, $delta);
$merged_corpus->update_counts();
my $ostream = new roots::RootsStreamJson();
$merged_corpus->deflate($ostream);
if ($OPT_STDOUT) {
	print $ostream->to_string();
}
else {
	open(F, "> $output_corpus") or die("Unable to open file $output_corpus (write mode).\n");
	print F $ostream->to_string();
	close(F);
}
